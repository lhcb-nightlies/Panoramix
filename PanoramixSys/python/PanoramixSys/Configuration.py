"""
High level configuration for Panoramix
"""
__version__ = "$Id: Configuration.py,v 1.64 2010-06-08 10:13:04 truf Exp $"
__author__  = "Thomas Ruf <Thomas.Ruf@cern.ch>"

# options common to all Panoramix use cases
#============================================================================
from Gaudi.Configuration import *

# Disable the error when asking for unknown configurables 
import Gaudi.Configurables 
Gaudi.Configurables.ignoreMissingConfigurables = True 

from Configurables import ( LHCbConfigurableUser, GaudiSequencer, ProcessPhase, DstConf, RecSysConf,CondDB,OTRawBankDecoder,DecodeVeloRawBuffer,
                            RootHistCnv__PersSvc, EventClockSvc, OnXSvc, L0Conf,AnalysisConf, PhysConf,LumiAlgsConf,LHCbApp,
                    MagneticFieldSvc, TrackSys,TESCheck,EventNodeKiller,GlobalRecoConf,UpdateManagerSvc, ParticlesAndVerticesMapper,
                    UnpackParticlesAndVertices,SimConf,DecodeRawEvent,CaloRecoConf,HCRawBankDecoder,TCKANNSvc)

from GaudiConf.IOHelper import IOHelper
from LumiAlgs.LumiIntegratorConf import LumiIntegratorConf

_used = filter(None, [DstConf, L0Conf, RecSysConf,AnalysisConf, PhysConf,TrackSys,MagneticFieldSvc, CondDB])

class PanoramixSys(LHCbConfigurableUser):
    ## Possible used Configurables
    __used_configurables__ = _used 

    # Steering options
    __slots__ = {
       "outputlevel":        4     # set Output Level: 2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL    
       ,"Sim":               False # read SIM files, requires precaution, no raw data present
       ,"Full_rec":          False # run full Brunel reconstruction sequence
       ,"Hlt":               False # emulate HLT
       ,"L0":                False # emulate L0
       ,"Phys":              False # import common DaVinci sequence 
       ,"User_opts":         ''    # additional user option 
       ,"User_file":         []    # data files
       ,"xml_catalogue":     ''    # user defined xml cataloque
       ,"expertTracking":    ''    # expert tracking options
       ,"User_config":       ''    # read in from user config file
       ,"qt":                True  # use QT for graphics
       ,"om":                False # use OpenMotif for graphics
       ,"User_opts_first":   ''    # additional user options which go in front of all other options
       ,"withMC":            False  # set to False for real data
       ,"DataType":          "2010" # MC09, 2010
       ,"Cosmics":           False # set to True to run reco sequence for cosmics
                                   # PatSeeding, CaloCosmicsTrackAlg, and simple Muontrack finding
       ,"SingleBeam":        False # set to True to run reco sequence with VeloOpen and fieldOff
                                   #  and standalone Muontrack finding
       ,"FirstCollisions":   False # set to True to run reco sequence with VeloOpen and field ON and standalone Muontrack finding
       ,"Mdf": False               # set True if input file == raw mdf file 
       ,"Online":  False           # set True if running in online mode
       ,"FEST":  False             # set True if running with FEST data
       ,"BareConfig":  0           # 0: default config, 1: start without analysis,phys and hlt and reco sequence
       ,"Oracle":  False           # use Oracle database instead of sqldddb
       ,"latestTag":  False        # use latestTag for given Datatype
       ,"simulation": False        # simulation data
       ,"AutoPilot": False         # run in autopilot mode
       ,"Upgrade": False           # reduced functionality for upgrade configurations 
        }

    def getProp(self,name):
        if hasattr(self,name):
            return getattr(self,name)
        else:
            return self.getDefaultProperties()[name]

    def setProp(self,name,value):
        return setattr(self,name,value)
        
    def applyConf(self):
        if self.getProp("outputlevel") < 4 : 
          for c in self.__slots__ :
           print 'conf received: ',c,':',self.getProp(c)
# standard stuff         
        appConf = ApplicationMgr()
# ROOT persistency for histograms
# The below must come before POOL (that uses ROOT to do the IO).
# It permits to initialize ROOT in graphic mode (whilst
# POOL initialize it in batch mode).
        appConf.HistogramPersistency = "ROOT"
        RootHistCnv__PersSvc('RootHistCnv').ForceAlphaIds = True
        RootHistSvc('RootHistSvc').OutputFile = 'Panohisto.root'
        appConf.ExtSvc += ["RootSvc"]
# for backward compatibility with units in opts files
        import os.path, GaudiKernel.ProcessJobOptions
        GaudiKernel.ProcessJobOptions._parser._parse_units(os.path.expandvars("$STDOPTS/units.opts"))
        appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc',"LoKiSvc","TCKANNSvc"]

# FSR
   # TES setup
        from Configurables import Gaudi__RootCnvSvc
        rootSvc = Gaudi__RootCnvSvc( "RootCnvSvc", EnableIncident = 1 )
        EventPersistencySvc().CnvServices += [ rootSvc ]
        rootSvc.VetoBranches = ["*"]
        rootSvc.CacheBranches = []
        fileSvc = Gaudi__RootCnvSvc( "FileRecordCnvSvc" )
        fileSvc.ShareFiles = "YES"
        FileRecordDataSvc( ForceLeaves        = True,
                           EnableFaultHandler = True,
                           RootCLID           = 1, 
                           PersistencySvc     = "PersistencySvc/FileRecordPersistencySvc")
        PersistencySvc("FileRecordPersistencySvc").CnvServices += [ fileSvc ] 
        appConf.ExtSvc += [ rootSvc,fileSvc]
# for particle property service 
        import PartProp.Service, PartProp.decorators
# support for decoding raw data including tae slots
        if not self.getProp("Sim") :  
          if self.getProp("BareConfig") == 1: 
           DecodeRawEvent().DataOnDemand=True
           importOptions( "$L0TCK/L0DUConfig.opts" )
          else :                              
# does not work anymore waiting for Rob: importOptions('$PANORAMIXROOT/options/tae_ondemand.py')
           DecodeRawEvent().DataOnDemand=True
           
           importOptions( "$L0TCK/L0DUConfig.opts" )
          L0Conf().FullL0MuonDecoding = True
          DecodeVeloRawBuffer().RawEventLocations = ['Velo/RawEvent', 'Other/RawEvent', 'Tracker/RawEvent','DAQ/RawEvent']
          OTRawBankDecoder().RawEventLocations    = ['OT/RawEvent'  , 'Other/RawEvent', 'Tracker/RawEvent','DAQ/RawEvent']
          dondsvc = DataOnDemandSvc()
          dondsvc.AlgMap['/Event/Raw/HC/Digits'] =  HCRawBankDecoder() 
# only take OT hits in time Window           
#          OTRawBankDecoder().TimeWindow = ( -8.0, 56.0 ) 
# some new ST configuration (September 2010), don't know yet when to use what:
          from STTools import STOfflineConf
          STOfflineConf.DefaultConf().configureTools()
#
# Get the event time (for CondDb) from ODIN
          EventClockSvc().EventTimeDecoder = "OdinTimeDecoder"
# user opts first
        if self.getProp("User_opts_first") : importOptions(self.getProp('User_opts_first')) 
# of course, visualization is needed        
        importOptions('$PANORAMIXROOT/options/PanoramixVis.py')
# default input files
        use_default = False
        ufile = self.getProp("User_file")
        if len(ufile) == 0: 
           use_default = True
           self.setProp("User_file",['default'])
        if len(ufile)  > 0:
         if ufile[0].lower() == 'default' : use_default = True
        if use_default :  
          if self.getProp("DataType") == "MC09":
           ufile = ['$PANORAMIXDATA/Bs2JpsiPhi_MC09.dst']
          else :
           ufile = ['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst']
          if self.getProp("xml_catalogue") == '' and self.getProp("xml_catalogue").lower() != 'none' :
             FileCatalog().Catalogs = ["xmlcatalog_file:$PANORAMIXDATA/MyCatalog.xml"]
          self.setProp("User_file",ufile)
# reading mdf file
        elif (ufile[0].find('mdf')>0 or ufile[0].find('raw')>0) and ufile[0].lower() != 'none' : 
          self.setProp("Mdf",True)
          self.setProp("withMC",False)
# connect to buffer manager
        elif ufile[0] == 'online' : 
          if self.getProp("FEST"): importOptions('$PANORAMIXROOT/options/PanoramixFest.py')
          else :                   importOptions('$PANORAMIXROOT/options/PanoramixOnline.py')
          CondDB().IgnoreHeartBeat         = True 
          CondDB().UseDBSnapshot           = True
          CondDB().EnableRunChangeHandler  = True
          MagneticFieldSvc().UseSetCurrent = True
          self.setProp("Online",True)
          self.setProp("withMC",False)
	  # no MC however use SIMCONDB for FEST
          if self.getProp("FEST") : 
            DstConf().SimType   = 'Full' 
# file catalog:
        if self.getProp("xml_catalogue") != '' and  self.getProp("xml_catalogue")  != 'none' :
             FileCatalog().Catalogs = ["xmlcatalog_file:"+self.getProp("xml_catalogue")]
# foresee output file
        if self.getProp("User_file") : 
         if use_default : 
          fname = 'Default'
         else: 
          fname =  self.getProp("User_file")[0]
         ll = max( fname.rfind('/'), fname.rfind('\\'))
         lm = fname[ll+1:].find('.') + ll+1 
         outputName = 'Sel_'+fname[ll+1:lm]+'.dst'
# don't know why suddenly this PFN: came in, TR 15/10/2010
         outputName = outputName.replace('PFN:','')
         #dstWriter = OutputStream("PanoDstWriter")
         #dstWriter.ItemList = ["/Event/DAQ/RawEvent#1","/Event/DAQ/ODIN#1","/Event#999"]
         #dstWriter.Preload  = False
         dstWriter = InputCopyStream("PanoDstWriter")
         dstWriter.Output   = "DATAFILE='PFN:" + outputName +  " 'SVC='Gaudi::RootCnvSvc' OPT='RECREATE'"
         appConf.OutStream.append( "PanoDstWriter" )
# add reconstruction functionality
# the fitter should work in any case
        from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter,ConfiguredHltFitter
        from Configurables import ToolSvc
# needed for turboTracks
        ConfiguredMasterFitter( "HltFitter", SimplifiedGeometry = True, LiteClusters = True, MSRossiAndGreisen = True )
        ConfiguredMasterFitter("TrackMasterFitter")
#
        if self.getProp("BareConfig") != 1 and self.getProp("User_file")[0].lower() != 'none' :
         recsys = RecSysConf()
         recsys.setProp('OutputLevel',self.getProp('outputlevel'))
         recsys.setProp('DataType',self.getProp('DataType'))
         recsys.setProp('Detectors',LHCbApp().getProp('Detectors'))
         if self.getProp('Upgrade') or LHCbApp().getProp('DataType') == '2015' or LHCbApp().getProp('DataType') == '2016' :
          from Configurables import RichRecSysConf
          RichRecSysConf('RichOfflineRec').Radiators = [ "Rich1Gas", "Rich2Gas" ]
          #recsys.setProp('RecoSequence',['Decoding', 'VELO', 'TT', 'IT', 'OT', 'Tr', 'Vertex', 'CALO', 'MUON', 'PROTO', 'SUMMARY'])
         # earlyData does not exist anymore in Rec v11r5
         # if self.getProp('DataType') == '2011' or self.getProp('DataType') == '2010' : RecSysConf().SpecialData = ['earlyData']
         if self.getProp('latestTag'):
# use latest tag for real data
              CondDB().LatestGlobalTagByDataType =  [self.getProp('DataType')]
# with single beam:
         if self.getProp("SingleBeam") or self.getProp("FirstCollisions"):
            if self.getProp("FirstCollisions"):
             RecSysConf().SpecialData = ['veloOpen']
             # if Oracle, assume user knows what to do
             #if not self.getProp("Oracle"):  
             #   MagneticFieldSvc().ScaleFactor      = 1
             #   UpdateManagerSvc().ConditionsOverride +=["Conditions/Online/LHCb/Magnet/Measured := double Current =5849.9936523438 ; int Polarity = -1;"] 
            else:
             RecSysConf().SpecialData = ['veloOpen', 'fieldOff']
             MagneticFieldSvc().UseConstantField = 0
             TrackSys().setSpecialDataOption("fieldOff",True)
            # MagneticFieldSvc().UseConstantField = True
            # MagneticFieldSvc().UseConditions    = False
            TrackSys.TrackPatRecAlgorithms  = ["Velo","Forward","TsaSeed","Match","Downstream","VeloTT"]
            # TrackSys().TrackPatRecAlgorithms  = ["Velo","Forward","PatSeed","Match","Downstream","VeloTT"]
            # TrackSys().ExpertTracking = ["noDrifttimes"] 
            # Select 'all' tracks to be made into ProtoParticles, in early data ...
            GlobalRecoConf().TrackTypes = [ "Long","Upstream","Downstream","Ttrack","Velo","VeloR" ]
            GlobalRecoConf().TrackCuts  = { }

            self.setProp("withMC",False)
            # configure tools for muon rec
            from Configurables import MuonNNetRec,MuonCombRec
            muonNNet = MuonNNetRec()
            muonNNet.TracksOutputLocation = '/Event/Rec/Muon/Track'
            muonNNet.ClusterTool = "MuonClusterRec"
            muonComb = MuonCombRec()
            muonComb.ClusterTool = "MuonClusterRec"
            muonComb.AddXTalk = False
            muonComb.TracksOutputLocation = '/Event/Rec/Muon/Track'            
# Set main sequence
         brunelSeq = GaudiSequencer("PanoRecoSequencer")
         brunelSeq.MeasureTime = True
         RecSysConf().RichSequences = ["RICH"]  # use good old RICH decoding
         DataOnDemandSvc().AlgMap['/Event/Rec/Rich/RecoEvent/Offline/Pixels'] = "RecoRICHSeq"
         DataOnDemandSvc().AlgMap['/Event/Rec/Rich/RecoEvent/OfflineLong/Pixels'] = "RecoRICHSeq"
         DataOnDemandSvc().AlgMap['/Event/Rec/Rich/RecoEvent/OfflineVeloTT/Pixels'] = "RecoRICHSeq"
         DataOnDemandSvc().AlgMap['/Event/Rec/Rich/RecoEvent/OfflineKsTrack/Pixels'] = "RecoRICHSeq"
         appConf.TopAlg += [ brunelSeq ]
# define sequence for converting HltInfo to tracks
         from Configurables import HltTrackConverter 
         hlttrackconv1 = HltTrackConverter("HltTrackConverter_hlt1")
         hlttrackconv2 = HltTrackConverter("HltTrackConverter_hlt2")
         hlttrackconv1.ReadHltLinesFrom1stEvent = True
         hlttrackconv1.SelReportsLocation = "Hlt1/SelReports"
         hlttrackconv1.HltObjectSummary = "Hlt1/SelReports/Candidates"
         hlttrackconv1.TrackDestination = "Rec/Track/ConvertedHlt1Tracks"
         hlttrackconv2.ReadHltLinesFrom1stEvent = True
         hlttrackconv2.SelReportsLocation = "Hlt2/SelReports"
         hlttrackconv2.TrackDestination = "Rec/Track/ConvertedHlt2Tracks"
         hlttrackconv2.HltObjectSummary = "Hlt2/SelReports/Candidates"
         hltSeq = GaudiSequencer("Pano_HltTrackConverter")
         hltSeq.Members+=[hlttrackconv1,hlttrackconv2]
         appConf.TopAlg += [hltSeq]
# clean up only for DST files, don't know what to do for files specified in user options
         ndst = -1
         if len(ufile)>0: ndst = ufile[0].lower().find('.dst')	 
         if ndst > 0 or use_default :
              cleanSeq = GaudiSequencer("CleanUpSequence")
              brunelSeq.Members += [cleanSeq]
              eventNodeKiller = EventNodeKiller()
              eventNodeKiller.Nodes = [ "Rec", "pRec", "Raw"]
              if self.getProp("withMC"):
               EvtCheck = TESCheck('EvtCheck')
               EvtCheck.Inputs       = ["Link/Rec/Track/Best"]
               eventNodeKiller.Nodes = [ "Rec", "pRec", "Raw", "Link/Rec" ]
               cleanSeq.Members += [EvtCheck]
              cleanSeq.Members += [eventNodeKiller]
         brunelSeq.Members += ["ProcessPhase/Init", "ProcessPhase/Reco"]
         ProcessPhase("Init").MeasureTime = True
         ProcessPhase("Reco").MeasureTime = True
         ProcessPhase("Init").DetectorList += ["Brunel", "Calo"]
# Convert Calo 'packed' banks to 'short' banks if needed
         GaudiSequencer("InitCaloSeq").Members += ["GaudiSequencer/CaloBanksHandler"]
         importOptions("$CALODAQROOT/options/CaloBankHandler.opts")
         from Configurables import TrackToDST
         trackfilter = TrackToDST()
         outputDSTSeq = GaudiSequencer("OutputDSTSeq")
         outputDSTSeq.Members += [ trackfilter ]
         appConf.TopAlg += [outputDSTSeq]
# instantiate OnX service        
        onxSvc  = OnXSvc()
# re-run L0
        if self.getProp('L0') and not self.getProp('Hlt')       : 
         importOptions('$L0DUROOT/options/ReplaceL0DUBankWithEmulated.opts')
# for running Hlt:
        if self.getProp("Hlt") :    
         print 'running of HLT not anymore supported !'
         # hltconf.HltType = 'Hlt1+Hlt2'
         # Default options to rerun L0          
         if self.getProp('L0') :  
           ## hltconf.replaceL0BanksWithEmulated = True
            print 'running of L0 not anymore supported !'
            #l0seq = GaudiSequencer("seqL0")
            #appConf.TopAlg += [ l0seq ]
            #L0Conf().setProp( "L0Sequencer", l0seq )
            #L0Conf().setProp( "ReplaceL0BanksWithEmulated", True ) 
         #hlt = GaudiSequencer('Hlt')
         #hlt.MeasureTime = True
         #appConf.TopAlg += [ hlt ]
         # for 2d vertex report
         #HltVertexReportsMaker().VertexSelections += ["PV2D"]
# with Cosmics or TED data:
        if self.getProp("Cosmics") : 
         importOptions('$PANORAMIXROOT/options/cosmic.py')  
         # self.setProp("withMC",False)           
         # CondDB().LocalTags["SIMCOND"] = ["velo-open"]
        SimConf().EnableUnpack = True
        SimConf().Detectors.append('UT')
        SimConf().Detectors.append('FT')
        SimConf().Detectors.append('VP')
# unpack packed Rec
        DstConf().EnableUnpack = []
        if (len(ufile) > 0 and not self.getProp("Mdf")) or use_default :
           if ufile[0] != 'online' and not self.getProp("Full_rec") and not self.getProp("Cosmics") and not self.getProp("SingleBeam"): 
             DstConf().EnableUnpack = ["Reconstruction","Stripping"]    
# some bricolage for reading my microDST
             pvmapper   = ParticlesAndVerticesMapper("UnpackPsAndVsMapper")  
             pvunpacker = UnpackParticlesAndVertices()
             pvunpacker.InputStream='/Initial/'
             appConf.TopAlg += [pvunpacker]                                         
# for physics selections and standard particles:
        if self.getProp("BareConfig") == 0 :
         analysis = AnalysisConf() 
         phys     = PhysConf() 
         dt = self.getProp("DataType") 
         analysis.DataType = dt
         phys.DataType = dt               
         if dt.find('MC')>-1 : 
             analysis.Simulation = True          
             phys.Simulation = True
         else : 
             analysis.Simulation = self.getProp("simulation")         
             phys.Simulation     = self.getProp("simulation")     
         physSeq = GaudiSequencer('PhysSeq') 
         physSeq.OutputLevel = self.getProp('outputlevel')
         physSeq.Members+=[phys.initSequence(),analysis.initSequence()]     
         appConf.TopAlg+=[physSeq]     
         # importOptions('$PANORAMIXROOT/options/Panoramix_DaVinci.py') 
# Rich now handled using configurables, but does not work yet really
        #richConf = RichRecQCConf()
        #richConf.DataType = dt   
# use qt :
        if self.getProp("qt") :
          onxSvc.Toolkit = "Qt"
          # onxSvc.Toolkit = "Gtk"
          # onxSvc.Toolkit = "Xt"
        else : 
          onxSvc.Toolkit = "NATIVE"    
# for MC data         
        if self.getProp("withMC") :     
         # provide support for unpacking MC data
         dondsvc = DataOnDemandSvc()
         # create muon linker tables
         dondsvc.AlgMap['/Event/Link/Raw/Muon/Coords'] =  'MuonCoord2MCParticleLink' 
         dondsvc.AlgMap['/Event/MC/Particles'] =  'UnpackMCParticle'
         dondsvc.AlgMap['/Event/MC/Vertices']   = 'UnpackMCVertex'
         DstConf().SimType   = 'Full'  
        elif not self.getProp("FEST"): 
            DstConf().SimType   = 'None'  

# provide hook for interactive user event sequence, only execute on demand
        eventSeq = GaudiSequencer("PanoEventSequence")
        appConf.TopAlg += [ eventSeq  ]

# making printout less verbose
        if len(ufile) > 0: 
            if ufile[0] != 'online' : EventSelector().PrintFreq = 100

################################################################################
# Lumi setup
#
    def lumi(self):
        """
        read FSR and accumulate event and luminosity data
        calculate normalization - toolname:
        """
        seq = []
        self.setOtherProps(LumiAlgsConf(),["DataType","InputType"])
        # add touch-and-count sequence
        lumiSeq = GaudiSequencer("LumiSeq")
        LumiAlgsConf().LumiSequencer = lumiSeq
        seq += [ lumiSeq ]
        # add integrator for normalization
        self.setOtherProps(LumiIntegratorConf(),["InputType"])
        lumiInt = GaudiSequencer("IntegratorSeq")
        LumiIntegratorConf().LumiSequencer = lumiInt
        seq += [ lumiInt ]
        return seq
        
################################################################################


