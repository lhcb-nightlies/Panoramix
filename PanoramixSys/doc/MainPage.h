/** \mainpage notitle
 *  \anchor Panoramixdoxygenmain
 *
 * This is the code reference manual for the Panoramix Visualization Application  classes.

 * These pages have been generated directly from the code and reflect the exact
 * state of the software for this version of the PanoramixSys packages. More
 * information is available from the
 * <a href="http://cern.ch/lhcb-comp/Frameworks/Visualization/">web pages</a>
 * of the Panoramix project
 *
 * \sa
 * \li \ref physsysdoxygenmain "PhysSys documentation (LHCb physics classes)"
 * \li \ref recsysdoxygenmain "RecSys documentation (LHCb reconstruction classes)"
 * \li \ref lbcomdoxygenmain   "LbcomSys documentation (LHCb shared components)"
 * \li \ref lhcbdoxygenmain    "LHCbSys documentation (LHCb core classes)"
 * \li \ref gaudidoxygenmain   "Gaudi documentation"
 * \li \ref externaldocs       "Related external libraries"

 */


