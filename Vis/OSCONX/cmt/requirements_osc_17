package OSCONX
version v16r13

branches cmt doc

#//////////////////////////////////////////////////
#//////////////////////////////////////////////////
#//////////////////////////////////////////////////
#
# The environment variable OSC_release_area must point
# to an OpenScientist RTK installation
#
#//////////////////////////////////////////////////
#//////////////////////////////////////////////////
#//////////////////////////////////////////////////

use LCG_Settings v*

set OSC_release_area  "${OSC_release_area}"

#macro OSC_native_version "16.10.0.1/"
macro OSC_native_version "17.0"

macro OSC_home "$(OSC_release_area)/osc_vis/$(OSC_native_version)/$(LCG_system)" \
      WIN32   "%OSC_release_area%\osc_vis\$(OSC_native_version)\$(LCG_system)"

include_path none
include_dirs $(OSC_home)/Resources/CoinGL/include $(OSC_home)/Resources/HEPVis/include $(OSC_home)/Resources/Slash $(OSC_home)/Resources/expat/include $(OSC_home)/Resources/Lib/include $(OSC_home)/Resources/OnX/include $(OSC_home)/Resources/inlib $(OSC_home)/Resources/exlib $(OSC_home)/Resources/backcomp

# WIN32 : have -D instead of /D for gccxml.
macro OSCONX_cppflags "" WIN32 "-DCOIN_DLL"

macro OSCLIB "$(OSC_home)/lib" WIN32 "$(OSC_home)\lib"
macro OSCBIN "$(OSC_home)/bin" WIN32 "$(OSC_home)\bin"

macro OSCONX_linkopts "-L$(OSCLIB) -Wl,--no-as-needed -lOnXCore -lLibXML -lourex_expat -lLibZip -lz -lLibUtils -ldl  -Wl,--as-needed " \
# -lz causes link problems on Ubuntu, don't use it at all
#macro OSCONX_linkopts "-L$(OSCLIB) -Wl,--no-as-needed -lOnXCore -lLibXML -losc_expat -lLibZip -lz -lLibUtils -ldl  -Wl,--as-needed " \
      slc3            "-L$(OSCLIB) -lOnXCore -lLibXML -losc_expat -lLibZip -lz -lLibUtils -ldl "\
      WIN32           '/libpath:"$(OSCLIB)" OnXCore.lib LibXML.lib osc_expat.lib LibZip.lib LibUtils.lib'

macro_remove  OSCONX_linkopts "" slc4-ia32 " -lz"
macro_remove  OSCONX_linkopts "" slc4_amd64_gcc34 " -lz"
macro_remove  OSCONX_linkopts "" x86_64-slc5-gcc43-opt " -lz"
macro_remove  OSCONX_linkopts "" x86_64-slc5-gcc46-opt " -lz"
macro_remove  OSCONX_linkopts "" x86_64-slc5-icc11-opt " -lz"

macro OSCIV_linkopts "-L$(OSCLIB) -Wl,--no-as-needed -lourex_HEPVis -lourex_freetype -lourex_CoinGL -lLibUtils -lGLU -lGL -ldl -L/usr/X11R6/lib -lX11 -Wl,--as-needed" \
            slc3     "-L$(OSCLIB) -lHEPVisPlotter -lHEPVisDetector -lHEPVisGeometry -lHEPVisUtils -losc_dvi2bitmap -losc_freetype2 -losc_Coin -lLibUtils -lGLU -lGL -ldl -L/usr/X11R6/lib -lX11 " \
            slc4-amd64     "-L$(OSCLIB)  -Wl,--no-as-needed -lHEPVisPlotter -lHEPVisDetector -lHEPVisGeometry -lHEPVisUtils -losc_dvi2bitmap -losc_freetype2 -losc_Coin -lLibUtils  -lGLU -lGL -ldl -L/usr/X11R6/lib64 -lX11 -Wl,--as-needed " \
            WIN32    '/libpath:"$(OSCLIB)" HEPVisPlotter.lib HEPVisDetector.lib HEPVisGeometry.lib HEPVisUtils.lib osc_dvi2bitmap.lib osc_freetype2.lib osc_Coin.lib LibUtils.lib glu32.lib opengl32.lib gdi32.lib user32.lib'

# Used in RootSvc :
macro OSCONXXt_linkopts "-L$(OSCLIB) -Wl,--no-as-needed -lOnXXtCore -lCoinXt -Wl,--as-needed" WIN32 ""

#//////////////////////////////////////////////////
#/// Run time /////////////////////////////////////
#//////////////////////////////////////////////////

set OSC_HOME_DIR "$(OSC_home)"

# Waiting Qt from LCG :
path_remove LD_LIBRARY_PATH "/usr/local/Qt/4.1.4/lib" CERN "/afs/cern.ch/sw/contrib/Qt/4.1.4/slc3_ia32_gcc323/lib" WIN32 ""
path_append LD_LIBRARY_PATH "/usr/local/Qt/4.1.4/lib" CERN "/afs/cern.ch/sw/contrib/Qt/4.1.4/slc3_ia32_gcc323/lib" WIN32 ""

# SDL (that will never come from LCG) :
#path_remove LD_LIBRARY_PATH "/afs/cern.ch/sw/contrib/SDL/1.2.11/slc3_ia32_gcc323/lib" WIN32 ""
#path_append LD_LIBRARY_PATH "/afs/cern.ch/sw/contrib/SDL/1.2.11/slc3_ia32_gcc323/lib" WIN32 ""

path_remove LD_LIBRARY_PATH "$(OSCBIN)" WIN32 ""
path_append LD_LIBRARY_PATH "$(OSCBIN)" WIN32 ""
path_remove LD_LIBRARY_PATH "$(OSCLIB)" WIN32 ""
path_append LD_LIBRARY_PATH "$(OSCLIB)" WIN32 ""

path_remove DYLD_LIBRARY_PATH "" Darwin "$(OSCLIB)"
path_append DYLD_LIBRARY_PATH "" Darwin "$(OSCLIB)"

path_remove DYLD_LIBRARY_PATH "" Darwin "$(OSCBIN)"
path_append DYLD_LIBRARY_PATH "" Darwin "$(OSCBIN)"

path_remove PATH "$(OSCBIN)" WIN32 "$(OSCBIN)"
path_prepend PATH "$(OSCBIN)" WIN32 "$(OSCBIN)"

set COIN_DONT_INFORM_INDIRECT_RENDERING "1"

path_remove PYTHONPATH "/OpenScientist/" WIN32 "\OpenScientist\"
path_append PYTHONPATH "${OSC_home}/Resources/CoinPython/scripts" \
                 WIN32 "${OSC_home}\Resources\CoinPython\scripts"
path_append PYTHONPATH "${OSC_home}/Resources/HEPVis/scripts/Python" \
                 WIN32 "${OSC_home}\Resources\HEPVis\scripts\Python"
path_append PYTHONPATH "${OSC_home}/Resources/OnX/scripts/Python" \
                 WIN32 "${OSC_home}\Resources\OnX\scripts\Python"
path_append PYTHONPATH "${OSCBIN}"

macro X_linkopts " -lXm -lXt -lXext -lX11 -lm " \
                 target-slc4&target-i386 " -L/usr/X11R6/lib -lXm -lXt -lXext -lX11 -lm " \
		 target-slc4&target-x86_64 " -L/usr/X11R6/lib64 -lXm -lXt -lXext -lX11 -lm "
