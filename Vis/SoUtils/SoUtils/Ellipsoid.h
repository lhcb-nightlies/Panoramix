// $Id: Ellipsoid.h,v 1.4 2008-07-28 08:11:22 truf Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.3  2007/03/19 15:23:33  ranjard
// v3r2 - fix for LHCb v22r2
//
// Revision 1.2  2006/03/09 16:48:15  odescham
// v2r1 - migrated to LHCb v20r0 - to be completed
//
// Revision 1.1.1.1  2004/09/08 15:52:31  ibelyaev
// New package: code moved from Vis/SoCalo
// 
// ============================================================================
#ifndef SOUTILS_ELLIPSOID_H 
#define SOUTILS_ELLIPSOID_H 1
// ============================================================================
// Include files
// ============================================================================
// STD & STL 
// ============================================================================
#include <vector>
//From GaudiKernel
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/Transform3DTypes.h"
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/StatusCode.h"
// ============================================================================
/// forward declarations
// ============================================================================
class SoEllipsoid       ; ///< from HEPVis package  
class SoEllipticalPrism ; ///< from HepVis package 
// ============================================================================


namespace SoUtils
{
  /** @fn ellipsoid 
   *  create SoEllipsoid Inventor node from the 
   *  (3x3) covarinace matrix and position
   *  values of error codes from "eigensystem" method 
   *  @param center position of ellipsoid center 
   *  @param cov    "covarinace" matrix (3D) for the ellipsoid 
   *  @param node   (return) pointer to created SoEllipsoid Inventor node
   *  @return status code 
   */
  StatusCode ellipsoid
  ( const Gaudi::XYZPoint&   center ,  
    const Gaudi::SymMatrix3x3& cov    ,
    SoEllipsoid*&       node   );

}


// ============================================================================
// The END 
// ============================================================================
#endif // SOUTILS_ELLIPSOID_H
// ============================================================================
