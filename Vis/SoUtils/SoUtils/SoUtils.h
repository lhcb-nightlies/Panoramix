// $Id: SoUtils.h,v 1.2 2008-07-28 08:11:22 truf Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.1.1.1  2004/09/08 15:52:31  ibelyaev
// New package: code moved from Vis/SoCalo
// 
// ============================================================================
#ifndef SOUTILS_SOUTILS_H 
#define SOUTILS_SOUTILS_H 1
// ============================================================================
// Include files
// ============================================================================
// SoUtils 
// ============================================================================
#include "SoUtils/EigenSystems.h"
#include "SoUtils/Ellipsoid.h"
#include "SoUtils/EllipticalPrism.h"
// ============================================================================


/** @namespace  SoUtils SoUtils.h SoUtils/SoUtils.h
 *
 *  Simple namespace with collectgion of useful 
 *  utilities for visualization
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date   2004-09-08
 */
namespace  SoUtils { } 

// ============================================================================
// The END 
// ============================================================================
#endif // SOUTILS_SOUTILS_H
// ============================================================================
