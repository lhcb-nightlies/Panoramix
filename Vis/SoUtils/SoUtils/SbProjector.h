#ifndef SoUtils_SbProjector_h
#define SoUtils_SbProjector_h 

#include <Inventor/SbString.h>
#include <Inventor/SbLinear.h>

/*!
 * class SbProjector
 *
 * To handle special (non-linear) projections (rz, phiz, etc...)
*/
namespace SoUtils {

class SbProjector {
public:
  SbProjector(const SbString&);
  SbProjector(const SbProjector&);
  virtual ~SbProjector();
  bool project(int,SbVec3f*) const;
  bool isRZ() const;
  bool isZR() const;
protected:
  bool projectRZ(int,SbVec3f*) const;
  bool projectPHIZ(int,SbVec3f*) const;
  bool projectZR(int,SbVec3f*) const;
  bool projectNZR(int,SbVec3f*) const;
  bool projectYZR(int,SbVec3f*) const;
  bool projectNormZ(int,SbVec3f*) const;
  bool projectZPHI(int,SbVec3f*) const;
  bool projectPHIETA(int,SbVec3f*) const;
private:
  int fProjection;
};

}

#endif
