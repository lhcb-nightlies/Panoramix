#ifndef SoUtils_Win32_h
#define SoUtils_Win32_h

// The below are used both in Gaudi and Windows includes !
// ( Who will have the final word : Pere or Bill ? )

#ifdef WIN32
#define IID IIID
#define MSG MESS
#undef ERROR
#undef CONST
#undef PLANES
#endif


#endif
