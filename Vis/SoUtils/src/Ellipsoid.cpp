// $Id: Ellipsoid.cpp,v 1.9 2009-06-04 11:10:04 jonrob Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.8  2009/06/04 11:08:04  jonrob
// ! 2009-06-04 - Chris Jones
//  - Fix rotations in Ellipsoid.cpp (I think....)
//
// Revision 1.7  2008/07/28 08:11:22  truf
// just to please cmt or cvs or whatever
//
// Revision 1.6  2006/03/29 08:37:15  gybarran
// *** empty log message ***
//
// Revision 1.5  2006/03/09 16:48:15  odescham
// v2r1 - migrated to LHCb v20r0 - to be completed
//
// Revision 1.4  2005/12/13 14:05:22  gybarran
// *** empty log message ***
//
// Revision 1.3  2005/04/15 16:09:17  ranjard
// v1r1 - adapt to CLHEP 1.9.1.2
//
// Revision 1.2  2004/09/10 14:06:58  ibelyaev
//  fix a stupid bug!
//
// ============================================================================
// Include files
// ============================================================================
// HepVis
// ============================================================================
#include <Inventor/nodes/SoSphere.h>
#include <HEPVis/nodes/SoEllipsoid.h>
// ============================================================================
// local
// ============================================================================
#include "SoUtils/Win32.h"
#include "SoUtils/EigenSystems.h"
#include "SoUtils/Ellipsoid.h"
// ============================================================================

// ============================================================================
/** @file
 *
 *  Implementation file for SoUtils::ellipsoid
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 22/11/2001
 */
// ============================================================================

// ============================================================================
/** create SoEllipsoid Inventor node from the
 *  covarinace matrix and position
 *  values of error codes from "eigensystem" method
 *  @param center positinoi of ellipsoid center
 *  @param cov    "covarinace" matrix for the ellipsoid
 *  @param node   (return) pointer to created SoEllipsoid Inventor node
 *  @return status code
 */
// ============================================================================
StatusCode SoUtils::ellipsoid
( const Gaudi::XYZPoint&   center ,
  const Gaudi::SymMatrix3x3& cov  ,
  SoEllipsoid*&       node   )
{
  // reset the output
  node = 0 ;
  //  if( 3 != cov.num_row()    ) { return StatusCode::FAILURE ; } ///< RETURN !
  //
  Gaudi::Vector3              evals  ( 0, 0, 0 ) ;
  typedef std::vector<Gaudi::Vector3> Vectors ;
  Vectors evects           ;
  StatusCode  sc = SoUtils::eigensystem( cov , evals , evects );
  if( sc.isFailure       () ) { return sc                  ; } ///< RETURN !
  if( 3 != evects.size   () ) { return StatusCode::FAILURE ; } ///< RETURN !
  //  if( 3 != evals.num_row () ) { return StatusCode::FAILURE ; } ///< RETURN !
  //

  SoEllipsoid* ell = new SoEllipsoid();
  ell->center.
    setValue( (float)center.x() , (float)center.y() , (float)center.z() );
  ell->eigenvalues.
    setValue( (float)sqrt( evals( 0 )  ) ,
              (float)sqrt( evals( 1 )  ) ,
              (float)sqrt( evals( 2 )  ) ) ;
  //
  const Gaudi::XYZVector vx ( evects[0](0) , evects[0](1) , evects[0](2) ) ;
  const Gaudi::XYZVector vy ( evects[1](0) , evects[1](1) , evects[1](2) ) ;
  //
  const Gaudi::XYZVector vz = vx.Cross( vy );
  const Gaudi::Transform3D rot( Gaudi::Rotation3D( vx.X(), vy.X(), vz.X(),
                                                   vx.Y(), vy.Y(), vz.Y(),
                                                   vx.Z(), vy.Z(), vz.Z() ) );
  //
  ROOT::Math::AxisAngle axisangle;
  rot.GetRotation( axisangle );
  ell->rotation.setValue ( SbVec3f( (float)axisangle.Axis().x(),
                                    (float)axisangle.Axis().y(),
                                    (float)axisangle.Axis().z() ),
                           (float)axisangle.Angle() );
  //
  node = ell ;
  //
  return StatusCode::SUCCESS ;
}

// ============================================================================
// The End
// ============================================================================
