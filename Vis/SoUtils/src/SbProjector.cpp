// this :
#include <SoUtils/SbProjector.h>

#include <math.h>

// LHCbMath
#include "LHCbMath/LHCbMath.h"

bool arctan(const double aX, const double aY,double& aValue) 
{
  //if((aX==0)&&(aY==0)) 
  if ( LHCb::Math::equal_to_double(aX,0) && LHCb::Math::equal_to_double(aY,0) )
  { 
    aValue = 0;
    return false;
  } else 
  {
    aValue = ::atan2(aX,aY); // return value in [-PI,PI]
    return true;
  }
}

//////////////////////////////////////////////////////////////////////////////
SoUtils::SbProjector::SbProjector(
 const SbString& aProjection
)
:fProjection(0)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
       if(aProjection=="RZ") fProjection = 1;
  else if(aProjection=="PHIZ") fProjection = 2;
  else if(aProjection=="ZR") fProjection = 3;
  else if(aProjection=="ZPHI") fProjection = 4;
  else if(aProjection=="-ZR") fProjection = 5;
  else if(aProjection=="-ZRY") fProjection = 7;
  else if(aProjection=="PHIETA") fProjection = 6;
  
}
//////////////////////////////////////////////////////////////////////////////
SoUtils::SbProjector::SbProjector(
 const SoUtils::SbProjector& aProjector
)
:fProjection(aProjector.fProjection)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
SoUtils::SbProjector::~SbProjector(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::isRZ(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (fProjection==1 ? true : false);
}
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::isZR(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (fProjection==3 || fProjection==5 || fProjection==7 ? true : false);
}
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::project(
 int aNumber
,SbVec3f* aPoints
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(fProjection==1) 
    return projectRZ(aNumber,aPoints);
  else if(fProjection==2) 
    return projectPHIZ(aNumber,aPoints);
  else if(fProjection==3) 
    return projectZR(aNumber,aPoints);
  else if(fProjection==4) 
    return projectZPHI(aNumber,aPoints);
  else if(fProjection==5) 
    return projectNZR(aNumber,aPoints);
  else if(fProjection==7) 
    return projectYZR(aNumber,aPoints);
  else
    return true; // Do nothing. It is ok.
}
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectRZ(
 int aNumber
,SbVec3f* aPoints
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  for(int index=0;index<aNumber;index++) {
    SbVec3f& point = aPoints[index];
    float x = point[0];
    float y = point[1];
    float z = point[2];
    float r = (float)::sqrt(x*x+y*y);
    //printf("debug : SbProjector::projectRZ : %d (%d) : %g %g\n",
    // index,aNumber,r,z);
    point[0] = r;
    point[1] = z;
    point[2] = 0;
  }
  return true;
}
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectPHIZ(
 int aNumber
,SbVec3f* aPoints
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  for(int index=0;index<aNumber;index++) {
    SbVec3f& point = aPoints[index];
    float x = point[0];
    float y = point[1];
    float z = point[2];
    double v;
    if(!arctan(y,x,v)) return false;
    point[0] = (float)v;
    point[1] = z;
    point[2] = 0;
  }
  return true;
}
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectZR(
 int aNumber
,SbVec3f* aPoints
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  for(int index=0;index<aNumber;index++) {
    SbVec3f& point = aPoints[index];
    float x = point[0];
    float y = point[1];
    float z = point[2];
    float r = (float)::sqrt(x*x+y*y);
    //printf("debug : SbProjector::projectRZ : %d (%d) : %g %g\n",
    // index,aNumber,r,z);
    point[0] = z;
    point[1] = r;
    point[2] = 0;
  }
  return true;
}
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectNZR(
 int aNumber
,SbVec3f* aPoints
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  for(int index=0;index<aNumber;index++) {
    SbVec3f& point = aPoints[index];
    float x = point[0];
    float y = point[1];
    float z = point[2];
    float r = (float)::sqrt(x*x+y*y);
    if (x<0) {r=-r;}
    point[0] = z;
    point[1] = r;
    point[2] = 0;
  }
  return true;
}
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectYZR(
 int aNumber
,SbVec3f* aPoints
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  for(int index=0;index<aNumber;index++) {
    SbVec3f& point = aPoints[index];
    float x = point[0];
    float y = point[1];
    float z = point[2];
    float r = (float)::sqrt(x*x+y*y);
    if (y<0) {r=-r;}
    point[0] = z;
    point[1] = r;
    point[2] = 0;
  }
  return true;
}
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectPHIETA(
 int aNumber
,SbVec3f* aPoints
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  for(int index=0;index<aNumber;index++) {
    SbVec3f& point = aPoints[index];
    float x = point[0];
    float y = point[1];
    float eta = (float)::tan(x*x+y*y);
    double phi;
    if(!arctan(y,x,phi)) return false;
    //printf("debug : SbProjector::projectPHIETA : %d (%d) : %g %g\n",
    // index,aNumber,eta,phi);
    point[0] = eta;
    point[1] = (float)phi;
    point[2] = 0;
  }
  return true;
}
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectZPHI(
 int aNumber
,SbVec3f* aPoints
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  for(int index=0;index<aNumber;index++) {
    SbVec3f& point = aPoints[index];
    float x = point[0];
    float y = point[1];
    float z = point[2];
    double v;
    if(!arctan(y,x,v)) return false;
    point[0] = z;
    point[1] = (float)v;
    point[2] = 0;
  }
  return true;
}
/*
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectRHOZ(
 const SbVec3f& aIn
,SbVec3f& aOut
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  aOut.clear();
  if(!aIn.size()) return true;
  else if( (This->specialProjection.specialProjection==OSpecialProjectionRHOZ) && 
           (ONodeGetProjected(a_node)==1) )
    {
      double   phi1from, phi1to;
      phi1from = This->specialProjection.rhoz_phi1 / 180. * M_PI;
      phi1to   = phi1from + M_PI;
      if(AllocatePoints(a_number)==0) return 0;
      for (index=0;index<a_number;index++)
        {
          double r,x,y,phi;
          x      = a_xs[index];
          y      = a_ys[index];
          r      = sqrt ( x * x + y * y);
          Class.ys[index] = r / (1 +  This->specialProjection.fisheye_a * r);
          Class.xs[index] = a_zs[index] / This->specialProjection.xcompress;
          phi    = CMathGetArcTangent (y,x);
          if(phi < 0) phi = M_PI - phi;
          if(phi < phi1from || phi >= phi1to) Class.ys[index] *= -1;
          Class.zs[index] = 0.;
        }
      if(a_pxs!=NULL) *a_pxs = Class.xs;
      if(a_pys!=NULL) *a_pys = Class.ys;
      if(a_pzs!=NULL) *a_pzs = Class.zs;
      return          a_number;
    }
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectTHETAPHI(
 const SbVec3f& aIn
,SbVec3f& aOut
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  aOut.clear();
  if(!aIn.size()) return true;
  else if( (This->specialProjection.specialProjection==OSpecialProjectionTHETAPHI) && 
           (ONodeGetProjected(a_node)==1) )
    {
      if(AllocatePoints(a_number)==0) return 0;
      for (index=0;index<a_number;index++) 
        {
          double x,y,z;
          x      = a_xs[index];
          y      = a_ys[index];
          z      = a_zs[index];
          Class.xs[index] = CMathGetArcTangent (y, sqrt( x * x + z * z));
          Class.ys[index] = CMathGetArcTangent (y,x);
          Class.zs[index] = 0.;
        }
      if(a_pxs!=NULL) *a_pxs = Class.xs;
      if(a_pys!=NULL) *a_pys = Class.ys;
      if(a_pzs!=NULL) *a_pzs = Class.zs;
      return          a_number;
    }
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectPHITHETA(
 const SbVec3f& aIn
,SbVec3f& aOut
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  else if( (This->specialProjection.specialProjection==OSpecialProjectionPHITHETA) && 
           (ONodeGetProjected(a_node)==1) )
    {
      double r_scale, z_scale; 
      if(AllocatePoints(a_number)==0) return 0;
      r_scale = 1. + This->specialProjection.v_rho_max * This->specialProjection.fisheye_a;
      z_scale = 1. + This->specialProjection.v_z_max   * This->specialProjection.fisheye_a;
      for (index=0;index<a_number;index++)
        {
          double x,y,z,r,phi;
          x      = a_xs[index];
          y      = a_ys[index];
          z      = a_zs[index];
          if (This->specialProjection.fisheye_a > 0.0)
            {
              r   = sqrt ( x * x + y * y);
              r  /= 1 + This->specialProjection.fisheye_a * r;
              r  *= r_scale;
              phi = CMathGetArcTangent (y,x);
              Class.xs[index] = r * sin(phi) / This->specialProjection.xcompress;
              Class.ys[index] = r * cos(phi);
              Class.zs[index] = z / (1 +  This->specialProjection.fisheye_a * z) * z_scale;
            }
          else
            {
              Class.xs[index] = x / This->specialProjection.xcompress;
              Class.ys[index] = y;
            }
          Class.zs[index] = z;
        }
      if(a_pxs!=NULL) *a_pxs = Class.xs;
      if(a_pys!=NULL) *a_pys = Class.ys;
      if(a_pzs!=NULL) *a_pzs = Class.zs;
      return          a_number;
    }
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectPHIR(
 const SbVec3f& aIn
,SbVec3f& aOut
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  else if( (This->specialProjection.specialProjection==OSpecialProjectionPHIR) && 
           (ONodeGetProjected(a_node)==1) )
    {
      double v_left,v_right,v_top,v_bottom, scale; 
      if(AllocatePoints(a_number)==0) return 0;
      OCameraGetViewLimits(This,&v_left,&v_right,&v_top,&v_bottom);
      scale = 0.9 * (v_top - v_bottom) / M_PI / 2; 
      for (index=0;index<a_number;index++)
        {
          double x,y,z;
          x      = a_xs[index];
          y      = a_ys[index];
          z      = a_zs[index];
          Class.ys[index] = CMathGetArcTangent (y,x) * scale;
          Class.xs[index] = sqrt ( x*x + y*y + z*z ) / This->specialProjection.xcompress;
          Class.zs[index] = 0.;
        }
      if(a_pxs!=NULL) *a_pxs = Class.xs;
      if(a_pys!=NULL) *a_pys = Class.ys;
      if(a_pzs!=NULL) *a_pzs = Class.zs;
      return          a_number;
    }
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectPHIRHO(
 const SbVec3f& aIn
,SbVec3f& aOut
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  else if( (This->specialProjection.specialProjection==OSpecialProjectionPHIRHO) && 
           (ONodeGetProjected(a_node)==1) )
    {
      double v_left,v_right,v_top,v_bottom, scale; 
      if(AllocatePoints(a_number)==0) return 0;
      OCameraGetViewLimits(This,&v_left,&v_right,&v_top,&v_bottom);
      scale = 0.9 * (v_top - v_bottom) / M_PI / 2; 
      for (index=0;index<a_number;index++)
        {
          double x,y;
          x      = a_xs[index];
          y      = a_ys[index];
          Class.ys[index] = CMathGetArcTangent (y,x) * scale;
          Class.xs[index] = sqrt (x * x + y * y) / This->specialProjection.xcompress;
          Class.zs[index] = 0.;
        }
      if(a_pxs!=NULL) *a_pxs = Class.xs;
      if(a_pys!=NULL) *a_pys = Class.ys;
      if(a_pzs!=NULL) *a_pzs = Class.zs;
      return          a_number;
    }
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectVPLOT(
 const SbVec3f& aIn
,SbVec3f& aOut
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  else if( (This->specialProjection.specialProjection==OSpecialProjectionVPLOT) && 
           (ONodeGetProjected(a_node)==1) )
    {
      double v_left,v_right,v_top,v_bottom, scale; 

      if(AllocatePoints(2*a_number)==0) return 0;
      OCameraGetViewLimits(This,&v_left,&v_right,&v_top,&v_bottom);
      scale = 0.9 * (v_top - v_bottom) / M_PI / 2; 
      for (index=0;index<a_number;index++)
        {
          double r,x,y,z,phi,theta,rmax,d_k;
          x      = a_xs[index];
          y      = a_ys[index];
          z      = a_zs[index];
          r      = sqrt (x * x + y * y);
          phi    = CMathGetArcTangent (y,x);
          theta  = CMathGetArcTangent (r,z);
          rmax   = MINIMUM(This->specialProjection.v_rho_max/sin(theta),
                           This->specialProjection.v_z_max/fabs(cos(theta)));
          d_k    = This->specialProjection.v_k * (rmax - r);
          Class.xs[index]          = (theta + d_k) * scale;
          Class.ys[index]          = phi           * scale;
          Class.zs[index]          = 0.;
          Class.xs[index+a_number] = (theta - d_k) * scale; 
          Class.ys[index+a_number] = Class.ys[index]; 
          Class.zs[index+a_number] = Class.zs[index]; 
        }
      if(a_pxs!=NULL) *a_pxs = Class.xs;
      if(a_pys!=NULL) *a_pys = Class.ys;
      if(a_pzs!=NULL) *a_pzs = Class.zs;
      return          2*a_number;
    }
//////////////////////////////////////////////////////////////////////////////
bool SoUtils::SbProjector::projectFISHEYE(
 const SbVec3f& aIn
,SbVec3f& aOut
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  else if( (This->specialProjection.specialProjection==OSpecialProjectionFISHEYE) && 
           (ONodeGetProjected(a_node)==1) )
    {
      double r_scale, z_scale; 
      if(AllocatePoints(a_number)==0) return 0;
      r_scale = 1. + This->specialProjection.v_rho_max * This->specialProjection.fisheye_a;
      z_scale = 1. + This->specialProjection.v_z_max   * This->specialProjection.fisheye_a;
      for (index=0;index<a_number;index++)
        {
          double x,y,z,r,phi;
          x      = a_xs[index];
          y      = a_ys[index];
          z      = a_zs[index];
          if (This->specialProjection.fisheye_a > 0.0)
            {
              r   = sqrt ( x * x + y * y);
              r  /= 1 + This->specialProjection.fisheye_a * r;
              r  *= r_scale;
              phi = CMathGetArcTangent (y,x);
              Class.xs[index] = r * sin(phi) / This->specialProjection.xcompress;
              Class.ys[index] = r * cos(phi);
              Class.zs[index] = z / (1 +  This->specialProjection.fisheye_a * z) * z_scale;
            }
          else
            {
              Class.xs[index] = x / This->specialProjection.xcompress;
              Class.ys[index] = y;
            }
          Class.zs[index] = z;
        }
      if(a_pxs!=NULL) *a_pxs = Class.xs;
      if(a_pys!=NULL) *a_pys = Class.ys;
      if(a_pzs!=NULL) *a_pzs = Class.zs;
      return          a_number;
    }
  else
    {
      if(a_pxs!=NULL) *a_pxs = a_xs;
      if(a_pys!=NULL) *a_pys = a_ys;
      if(a_pzs!=NULL) *a_pzs = a_zs;
      return          a_number;
    }
}
*/
