// $Id: EllipticalPrism.cpp,v 1.7 2008-07-28 09:20:46 truf Exp $
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.5  2006/03/29 08:37:15  gybarran
// *** empty log message ***
//
// Revision 1.4  2006/03/09 16:48:15  odescham
// v2r1 - migrated to LHCb v20r0 - to be completed
//
// Revision 1.3  2005/12/13 14:05:22  gybarran
// *** empty log message ***
//
// Revision 1.2  2005/04/15 16:09:17  ranjard
// v1r1 - adapt to CLHEP 1.9.1.2
//
// Revision 1.1.1.1  2004/09/08 15:52:31  ibelyaev
// New package: code moved from Vis/SoCalo
//
// ============================================================================
// Include files
// ============================================================================
// HepVis
// ============================================================================
#include <Inventor/nodes/SoSphere.h>
#include <HEPVis/nodes/SoEllipticalPrism.h>
// ============================================================================
// local
// ============================================================================
#include "SoUtils/Win32.h"
#include "SoUtils/EigenSystems.h"
#include "SoUtils/EllipticalPrism.h"
// ============================================================================

// ============================================================================
/** @file
 *
 *  Implementation file for SoUtils::ellipticalprism
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 22/11/2001
 */
// ============================================================================

// ============================================================================
/** create SoEllipticalPrism Inventor node from the
 *  covarinace matrix and position
 *  values of error codes from "eigensystem" method
 *  @param center position of elliptical prism
 *  @param cov    "covarinace" matrix (2D) for the ellipsoid
 *  @param extent extent for elliptical prism
 *  @param node   (return) pointer to created SoEllipsoid Inventor node
 *  @return status code
 */
// ============================================================================
StatusCode SoUtils::ellipticalprism
( const Gaudi::XYZPoint&     center ,
  const Gaudi::SymMatrix2x2&   cov    ,
  const double          extent ,
  SoEllipticalPrism*&   node   )
{
  /// reset the output
  node = 0 ;

  if( cov == 0  ) {
    /// return StatusCode::FAILURE;
  }
  Gaudi::Vector3              evals  ( 0 , 0 ,0 ) ;
  std::vector<Gaudi::Vector3> evects           ;
  //OD StatusCode sc = SoUtils::eigensystem( cov , evals , evects );
  //if( sc.isFailure       () ) { return sc                  ; } ///< RETURN !
  if( 2 != evects.size   () ) { return StatusCode::FAILURE ; } ///< RETURN !
  //  if( 2 != evals.num_row () ) { return StatusCode::FAILURE ; } ///< RETURN !
  ///
  SoEllipticalPrism* ell = new SoEllipticalPrism();
  ell->center.
    setValue( (float)center.x() , (float)center.y() , (float)center.z() );
  ell->eigenvalues.
    setValue( (float)sqrt( evals( 1 )  ) ,
              (float)sqrt( evals( 2 )  ) ) ;
  ///

  Gaudi::Rotation3D rot;
  Gaudi::XYZVector vx ( evects[0](1) , evects[0](2) , 0 ) ;
  Gaudi::XYZVector vy ( evects[1](1) , evects[1](2) , 0 ) ;
  ///
  //OD  rot.rotateAxes ( vx , vy , vx.cross( vy ) );
  Gaudi::XYZVector axis  ;
  double      angle = 0;
  //OD rot.getAngleAxis( angle , axis );
  ell->rotation.setValue
    (SbVec3f((float)axis.x(),(float)axis.y(),(float)axis.z()),(float)angle);
  ell->extent.setValue( (float)extent );
  ///
  node = ell ;
  ///
  return StatusCode::SUCCESS ;
}

// ============================================================================
// The End
// ============================================================================
