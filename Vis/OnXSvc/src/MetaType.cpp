
// this :
#include <OnXSvc/MetaType.h>

// Lib :
#include <Lib/Property.h>
#include <Lib/Variable.h>

// Gaudi :
#include <GaudiIntrospection/IIntrospectionSvc.h>
#include <GaudiIntrospection/MetaClass.h>
#include <GaudiIntrospection/MetaField.h>

//////////////////////////////////////////////////////////////////////////////
MetaType::MetaType(
 IPrinter& aPrinter
,IIntrospectionSvc* aIntrospectionSvc
)
:BaseType(aPrinter)
,fIntrospectionSvc(aIntrospectionSvc)
,fIterator(0)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
bool MetaType::setName(
 const std::string& aName
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  fType = "";
  if(!fIntrospectionSvc) return false;
  if(!fIntrospectionSvc->existsClass(aName)) return false;
  fType = aName;
  return true;
}
//////////////////////////////////////////////////////////////////////////////
const std::string& MetaType::name(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fType;
}
//////////////////////////////////////////////////////////////////////////////
void MetaType::setIterator(
 Lib::IIterator* aIterator
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  fIterator = aIterator;
}
//////////////////////////////////////////////////////////////////////////////
Lib::IIterator* MetaType::iterator(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fIterator;
}
//////////////////////////////////////////////////////////////////////////////
Lib::Property* MetaType::getProperties(
 int& aNumber
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  aNumber = 0;

  //printf("debug : getProps : 000\n");

  MetaClass& metaClass = const_cast<MetaClass&>(fIntrospectionSvc->getClass(fType));
  std::vector<MetaField*> fields = metaClass.fields();

  unsigned int number = fields.size();
  //printf("debug : getProps : 001 : %d\n",number);

  if(number<=0) return 0;
  Lib::Property* list = new Lib::Property[number];
  if(!list) return 0;
  
  for( unsigned int i = 0; i < number; i++ ) {
    list[i].set(fields[i]->name(),Lib::Property::DOUBLE);
    //printf("debug :  available property \"%s\"\n",fields[i]->name().c_str());
  }

  aNumber = number;
  return list;
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable MetaType::value(
 Lib::Identifier aIdentifier
,const std::string& aName
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MetaClass& metaClass = const_cast<MetaClass&>(fIntrospectionSvc->getClass(fType));
  std::vector<MetaField*> fields = metaClass.fields();
  for( unsigned int i = 0; i < fields.size(); i++ ) {
    if(aName==fields[i]->name()) {
      double value = 0;
      return Lib::Variable(printer(),fields[i]->get(aIdentifier,value));
    }
  }
  return Lib::Variable(printer());
}
