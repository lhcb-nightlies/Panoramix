// $Id: SoConversionSvc.cpp,v 1.14 2009-01-27 08:38:28 cattanem Exp $ 
// ============================================================================

// this :
#include <OnXSvc/SoConversionSvc.h>

//#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/SvcFactory.h>
#include <GaudiKernel/DataObject.h>
#include <GaudiKernel/IDataProviderSvc.h>

#include <DetDesc/IDetectorElement.h>
#include <DetDesc/ILVolume.h>
#include <DetDesc/CLIDDetectorElement.h>
#include <DetDesc/CLIDLVolume.h>

#include <AIDA/IHistogram.h>

#include <OnXSvc/ClassID.h>

DECLARE_SERVICE_FACTORY( SoConversionSvc )

//////////////////////////////////////////////////////////////////////////////
SoConversionSvc::SoConversionSvc(
 const std::string& aName
,ISvcLocator* aSvcLoc
)
:ConversionSvc(aName, aSvcLoc, So_TechnologyType) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{ 
}
//////////////////////////////////////////////////////////////////////////////
SoConversionSvc::~SoConversionSvc(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{ 
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoConversionSvc::queryInterface(
 const InterfaceID& aRIID
,void** aPPVIF
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(ISoConversionSvc::interfaceID() == aRIID) {
    *aPPVIF = static_cast<ISoConversionSvc*> (this);
    addRef();
    return StatusCode::SUCCESS;
  } else if(IConversionSvc::interfaceID() == aRIID) {
    *aPPVIF = static_cast<IConversionSvc*> (this);
    addRef();
    return StatusCode::SUCCESS;
  } else {
    return ConversionSvc::queryInterface(aRIID, aPPVIF);
  }
}

//////////////////////////////////////////////////////////////////////////////
StatusCode SoConversionSvc::initialize(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  StatusCode status = ConversionSvc::initialize();
  if( status.isFailure() ) return status;

  MsgStream log(msgSvc(), name());
  log << MSG::INFO << this->name() << " initialized successfully" << endmsg;
  
  return status;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoConversionSvc::createRep ( 
 DataObject* aObject
,IOpaqueAddress*& aAddress
) 
//////////////////////////////////////////////////////////////////////////////
/** Convert the transient object to the requested representation.
 *  e.g. conversion to persistent objects.
 *  @param     Object  Pointer to location of the object 
 *  @param     Address  Reference to location of pointer with the 
 *  object address.
 *  @return    Status code indicating success or failure
 */
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  aAddress = 0;

  /// check the arguments 
  if(!aObject) {
    MsgStream log( msgSvc() , name() );
    log << MSG::ERROR 
	<< " DataObject* points to NULL! " << endmsg ;
    return StatusCode::FAILURE ;
  }

  /// DetectorElement? 
  if(dynamic_cast<const IDetectorElement*>(aObject)) {
    IConverter* cnv = converter( CLID_DetectorElement );
    if(!cnv) {
      MsgStream log( msgSvc() , name() );
      log << MSG::ERROR
          << " Converter is not found for Object/CLID='" 
          << System::typeinfoName( typeid( *aObject ) ) << "'/'"
          << aObject->clID()                            << "'"   << endmsg ;
      return StatusCode::FAILURE ;
    }
    return cnv->createRep( aObject ,  aAddress ) ;
  }

  /// Logical volume?
  if(dynamic_cast<const ILVolume*>(aObject)) {
    IConverter* cnv = converter( CLID_LVolume );
    if(!cnv) {
      MsgStream log( msgSvc() , name() );
      log << MSG::ERROR
          << " Converter is not found for Object/CLID='" 
          << System::typeinfoName( typeid( *aObject ) ) << "'/'"
          << aObject->clID()                            << "'"   << endmsg ;
      return StatusCode::FAILURE ;
    }
    return cnv->createRep( aObject ,  aAddress ) ;
  }

  /// Histogram ?
  if(dynamic_cast<const AIDA::IHistogram*>(aObject)) {
    IConverter* cnv = converter( CLID_Histogram );
    if(!cnv) {
      MsgStream log( msgSvc() , name() );
      log << MSG::ERROR
          << " Histogram converter is not found for Object/CLID='" 
          << System::typeinfoName( typeid( *aObject ) ) << "'/'"
          << aObject->clID()                            << "'"   << endmsg ;
      return StatusCode::FAILURE ;
    }
    return cnv->createRep( aObject ,  aAddress ) ;
  }

  /// Other objects :
  IConverter* cnv = converter( aObject->clID()       ) ; ///< other object
  if(!cnv) {
    MsgStream log( msgSvc() , name() );
    log << MSG::INFO
	<< " Converter is not found for Object/CLID='" 
        << System::typeinfoName( typeid( *aObject ) ) << "'/'"
        << aObject->clID()                            << "'"   
        << endmsg ;
        
    // does not work anymore with gcc62 log << MSG::ALWAYS << aObject->fillStream(log.stream()) << endmsg;    
    
    return StatusCode::SUCCESS ;
  }
  return cnv->createRep( aObject ,  aAddress ) ;

}

