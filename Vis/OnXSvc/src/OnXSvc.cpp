// this :
#include <OnXSvc/OnXSvc.h>

// Slash :
#include <Slash/Core/IWriter.h>
#include <Slash/Data/IProcessor.h>
#include <Slash/Core/IManager.h>
#include <Slash/UI/IUI_Manager.h>

#ifdef OSC_VERSION_LT_16_10
#else
#include <Slash/Tools/osc_env.h>
#endif

// Lib :
#include <Lib/smanip.h>
#include <Lib/Printer.h>
#include <Lib/Manager.h>
#include <Lib/Arguments.h>

// OnX :

// Gaudi :
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/IAppMgrUI.h>
#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiKernel/IDataManagerSvc.h>
#include <GaudiKernel/IRegistry.h>
#include <GaudiKernel/IHistogramSvc.h>
#include <GaudiKernel/IEvtSelector.h>
#include <GaudiKernel/SvcFactory.h>
#include <GaudiKernel/SmartIF.h>
#include <GaudiKernel/SmartDataPtr.h>
#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/System.h>

// AIDA :
#include <AIDA/IHistogram.h>

#include <XmlTools/IXmlSvc.h>

// VisSvc :
#include <VisSvc/IVisualizationSvc.h>

// OnXSvc :
#include <OnXSvc/ISoConversionSvc.h>
#include <OnXSvc/ISvcLocatorManager.h>

#include <OnXSvc/Helpers.h>

#ifdef OSC_VERSION_LT_16_10
// Xlib.h define CPP "Status", then clash with IInterface.h !
#include <OnX/Core/Main.h>
#endif

DECLARE_SERVICE_FACTORY( OnXSvc )

class SvcLocatorManager
  :public virtual Slash::Core::IManager
  ,public virtual ISvcLocatorManager {
public: //IManager
  virtual std::string name() const { return fName;  }
  virtual void* cast(const std::string& aString) const {
    if(aString=="ISvcLocatorManager") {
      return ((ISvcLocatorManager*)this);
    } else if(aString=="Slash::Core::IManager") {
      return ((Slash::Core::IManager*)this);
    } else {
      return 0;
    }
  }
public: //ISvcLocatorManager
  virtual ISvcLocator* serviceLocator() const {
    return fSvcLocator;
  }
  virtual IService* service(const std::string& aName) const {
    IService* svc;
    fSvcLocator->service(aName,svc);
    return svc;
  }
public:
  SvcLocatorManager(ISvcLocator* aSvcLocator)
    :fName("SvcLocatorManager"),fSvcLocator(aSvcLocator){}
  virtual ~SvcLocatorManager(){}
private:
  std::string fName;
  ISvcLocator* fSvcLocator;
};

class Printer : public virtual Slash::Core::IWriter {
public:
  virtual void disable() {}
  virtual void enable() {}
  virtual bool enabled() const {  return true;}
  virtual bool write(const std::string& aString) {
    fLog << aString;
    return true;
  }
  virtual bool flush() {
    return true;
  }
public:
  Printer(MsgStream& aLog):fLog(aLog){}
  virtual ~Printer(){}
private:
  MsgStream& fLog;
};

//////////////////////////////////////////////////////////////////////////////
OnXSvc::OnXSvc(
               const std::string& aName
               ,ISvcLocator* aSvcLoc
               )
  :Service(aName,aSvcLoc)
  ,fAppMgrUI(0)
  ,fSoCnvSvc(0)
  ,fEventDataSvc(0)
  ,fDetectorDataSvc(0)
  ,fHistogramSvc(0)
  ,fThreaded(false)
  ,fToolkit("NATIVE")
  ,fFile("$ONXFILE")
  ,fOutputToTerminal(false)
  ,fLog(NULL)
#ifdef OSC_VERSION_LT_16_10
  ,fOnXMain(0)
#else
  ,fOwnSession(false)
#endif
  ,fSession(0)
  ,fTypeManager(0)
  ,fMetaType(0)
  ,fLibPrinter(0)
  ,fGaudiPrinter(0)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  declareProperty("Threaded",fThreaded);
  declareProperty("Toolkit",fToolkit);
  declareProperty("File",fFile);
  declareProperty("OutputToTerminal",fOutputToTerminal);
  // Create a default printer. The printer() method may be called
  // before the service is initialized (through the declaration of types).
  fLibPrinter = new Lib::Printer();
}
//////////////////////////////////////////////////////////////////////////////
OnXSvc::~OnXSvc(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  delete fLog;
  fLog = 0;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode OnXSvc::queryInterface(
                                  const InterfaceID& aRIID
                                  ,void** aPPVIF
                                  )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if (aRIID==IRunable::interfaceID())   {
    *aPPVIF = static_cast<IRunable*>(this);
    addRef();
    return StatusCode::SUCCESS;
  } else if(aRIID==IUserInterfaceSvc::interfaceID()) {
    *aPPVIF = static_cast<IUserInterfaceSvc*>(this);
    addRef();
    return StatusCode::SUCCESS;
  } else {
    return Service::queryInterface(aRIID, aPPVIF);
  }
}
//////////////////////////////////////////////////////////////////////////////
StatusCode OnXSvc::initialize(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  StatusCode status = Service::initialize();
  if( status.isFailure() ) return status;

  fGaudiPrinter = new Printer(msgStream());

  msgStream() << MSG::DEBUG << "OnXSvc::initialize " << endmsg;

  setProperties();

  if(!serviceLocator()) {
    msgStream() << MSG::ERROR << "Service locator not found " << endmsg;
    return StatusCode::FAILURE;
  }

  if(fAppMgrUI) {
    fAppMgrUI->release();
    fAppMgrUI = 0;
  }
  status = serviceLocator()->service("ApplicationMgr",fAppMgrUI);
  if( status.isFailure() || !fAppMgrUI) {
    msgStream() << MSG::ERROR << "ApplicationMgr not found " << endmsg;
    return StatusCode::FAILURE;
  }
  fAppMgrUI->addRef();

  // Dynamic load OnX GUI driver :
  if(fToolkit=="NONE") {
    msgStream() << MSG::INFO << "GUI toolkit NONE." << endmsg;
  } else {

#ifdef OSC_VERSION_LT_16_10
#else
    if(!Slash::osc::set_env(std::cout)) {
      msgStream() << MSG::ERROR << " unable to set OSC environment." << endmsg;
      return StatusCode::FAILURE;
    }
#endif

    Lib::Arguments gaudi_args(System::cmdLineArgs());

    //args relevant to OnX::Main :
    Lib::Arguments args;
    {std::string key = "-debug_check_class";
      if(gaudi_args.isAnArgument(key)) {
        std::string value;
        gaudi_args.find(key,value);
        args.add(key,value);
      }}
    {std::string key = "-Qt";
      if(gaudi_args.isAnArgument(key)) {
        std::string value;
        gaudi_args.find(key,value);
        args.add(key,value);
      }}

#ifdef OSC_VERSION_LT_16_10
    fOnXMain = new OnX::Main(false,false,args.tovector());
    if(!fOnXMain->isValid()) {
      msgStream() << MSG::ERROR << "Can't create a valid OnX::Main" << endmsg;
      delete fOnXMain;
      fOnXMain = 0;
      return StatusCode::FAILURE;
    }

    // Do the below also if fThreaded ?
    // Have a property to control the below ?
    if(!fOnXMain->loadInterpreter("Python")) {
      // Not fatal.
      msgStream() << MSG::WARNING << "Can't load the OnX Python module." << endmsg;
    }

    Slash::Core::ISession* session = fOnXMain->session();

    if(!session) {
      msgStream() << MSG::ERROR << "OnX::Main don't have a ISession." << endmsg;
      delete fOnXMain;
      fOnXMain = 0;
      return StatusCode::FAILURE;
    }
#else
    // We pass an empty -onx_file to not create the GUI
    // with the below Slash_create_session. We want to do that
    // in order to enforce first the loading of the OnX Python module.
    args.add("-onx_file","");

    Slash::Core::ISession* session = Slash_create_session(args.tovector());
    if(!session) {
      msgStream() << MSG::ERROR << "OnX::Main don't have a ISession." << endmsg;
      return StatusCode::FAILURE;
    }

    // then we load the OnX Python module.
    Slash::UI::IScriptManager* sm = Slash::scriptManager(*session);
    if(!sm) {
      msgStream() << MSG::ERROR << "Can't find the script manager." << endmsg;
      return StatusCode::FAILURE;
    }
    Slash::UI::IInterpreter* interp = sm->findInterpreter("Python");
    if(!interp) {
      msgStream() << MSG::ERROR << "Can't find Python IInterpreter." << endmsg;
      return StatusCode::FAILURE;
    }
    if(!interp->load(false)){
      // Not fatal.
      msgStream() << MSG::WARNING << "Can't load the OnX Python module." << endmsg;
    }
#endif

    if(fOutputToTerminal) session->setParameter("session.output","terminal");

    session->setParameter("UI_Manager.thread",fThreaded?"true":"false");

    fOwnSession = true;

    setSession(session);

    std::string driver;
    if(fToolkit=="NATIVE") {
      driver = ""; //Let OnX decide. Decided also from program (argc,argv).
    } else {
      driver = fToolkit;
    }

    msgStream() << MSG::INFO << "GUI toolkit " << driver << "." << endmsg;
    msgStream() << MSG::INFO << "OnX GUI file " << fFile << "." << endmsg;

#ifdef OSC_VERSION_LT_16_10
    if(fOnXMain->createUI(driver,fFile,true)) {
      msgStream() << MSG::DEBUG << "GUI created." << endmsg;
    } else {
      msgStream() << MSG::ERROR << "GUI creation failed." << endmsg;
      delete fOnXMain;
      fOnXMain = 0;
      fSession = 0;
      return StatusCode::FAILURE;
    }
#else
    // then we create the GUI :
    Slash::UI::IUI_Manager* uim = Slash::uiManager(*session);
    if(!uim) {
      msgStream() << MSG::ERROR << "UI_Manager not found." << endmsg;
      delete fSession;
      fSession = 0;
      return StatusCode::FAILURE;
    }

    std::vector<std::string> ui_args;
    if(uim->create(driver,fFile,ui_args,true)) {
      msgStream() << MSG::DEBUG << "GUI created." << endmsg;
    } else {
      msgStream() << MSG::ERROR << "GUI creation failed." << endmsg;
      delete fSession;
      fSession = 0;
      return StatusCode::FAILURE;
    }
#endif

  }

  return status;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode OnXSvc::finalize(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(fAppMgrUI) {
    fAppMgrUI->release();
    fAppMgrUI = 0;
  }
  if(fSoCnvSvc) {
    fSoCnvSvc->release();
    fSoCnvSvc = 0;
  }
  if(fEventDataSvc) {
    fEventDataSvc->release();
    fEventDataSvc = 0;
  }
  if(fDetectorDataSvc) {
    fDetectorDataSvc->release();
    fDetectorDataSvc = 0;
  }
  if(fHistogramSvc) {
    fHistogramSvc->release();
    fHistogramSvc = 0;
  }

  msgStream() << MSG::DEBUG << "OnXSvc finalizing..." << endmsg;

#ifdef OSC_VERSION_LT_16_10
  delete fOnXMain;
  fOnXMain = 0;
#else
  if(fOwnSession)  delete fSession;
  fSession = 0;
#endif

  delete fGaudiPrinter;
  fGaudiPrinter = 0;
  delete fLibPrinter;
  fLibPrinter = 0;

  msgStream() << MSG::DEBUG << "OnXSvc finalized successfully" << endmsg;

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode OnXSvc::run(
)
//////////////////////////////////////////////////////////////////////////////
// Called only in "GaudiMain" mode.
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!fSession) {
    msgStream() << MSG::ERROR << "No ISession given " << endmsg;
    return StatusCode::FAILURE;
  }
  Slash::UI::IUI* ui = findUI();
  if(!ui) return StatusCode::FAILURE;
  msgStream() << MSG::DEBUG << "Enter in GUI toolkit mainloop... " << endmsg;
  ui->steer();
  msgStream() << MSG::DEBUG << "GUI toolkit mainloop exited. " << endmsg;
  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
SoPage* OnXSvc::currentSoPage(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!fSession) {
    msgStream() << MSG::WARNING << "No ISession given " << endmsg;
    return 0;
  }
  Slash::UI::IUI* ui = findUI();
  if(!ui) return 0;
  return ui_SoPage(*ui);
}
//////////////////////////////////////////////////////////////////////////////
SoRegion* OnXSvc::currentSoRegion(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  SoPage* soPage = currentSoPage();
  if(!soPage) {
    msgStream() << MSG::WARNING << "SoPage not found " << endmsg;
    return 0;
  }
  return soPage->currentRegion();
}
//////////////////////////////////////////////////////////////////////////////
Slash::UI::IWidget* OnXSvc::currentWidget(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!fSession) {
    msgStream() << MSG::INFO << "No ISession given " << endmsg;
    return 0;
  }
  Slash::UI::IUI* ui = findUI();
  if(!ui) return 0;
  return ui->currentWidget();
}
//////////////////////////////////////////////////////////////////////////////
Slash::Data::IProcessor* OnXSvc::typeManager(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fTypeManager;
}
//////////////////////////////////////////////////////////////////////////////
Slash::Data::IAccessor* OnXSvc::metaType(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fMetaType;
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
void OnXSvc::setSession(
                        Slash::Core::ISession* aSession
                        )
//////////////////////////////////////////////////////////////////////////////
// Used when the service is started in NONE mode.
// Should be executed once.
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(fSession) return; //done.
  if(!aSession) return;
  fSession = aSession;

  fSession->addManager(new SvcLocatorManager(serviceLocator()));

  fTypeManager =
    Lib_findManager(*fSession,"AccessorManager",Slash::Data::IProcessor);

  if(fTypeManager) {
    std::vector<Slash::Data::IAccessor*>::const_iterator it;
    for(it=fTypes.begin(); it!=fTypes.end(); it++) {
      fTypeManager->addAccessor(*it);
    }
    fTypes.clear();

    /*
      IIntrospectionSvc* introspectionSvc = 0;
      serviceLocator()->service("IntrospectionSvc",introspectionSvc);
      if(!introspectionSvc) {
      msgStream() << MSG::INFO << "IntrospectionSvc not found " << endmsg;
      }

      if(introspectionSvc) {
      fMetaType = new MetaType(fSession->printer(),introspectionSvc);
      fTypeManager->addType(fMetaType);
      }
    */
  }

}

Slash::Core::ISession* OnXSvc::session() {return fSession;}

void OnXSvc::nextEvent() {
  if(!fAppMgrUI) return;
  fAppMgrUI->nextEvent(1);
}

//////////////////////////////////////////////////////////////////////////////
void OnXSvc::eventInfo(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!fSession) {
    msgStream() << MSG::INFO << "No ISession given " << endmsg;
    return;
  }

  if(!fEventDataSvc) {
    StatusCode status = service("EventDataSvc",fEventDataSvc,true);
    if(status.isFailure() || !fEventDataSvc) {
      msgStream() << MSG::INFO << "EventDataSvc not found " << endmsg;
      return;
    }
    fEventDataSvc->addRef();
  }

  //SmartDataPtr<DataObject> smartDataObject(fEventDataSvc,"/Event");
  //if(smartDataObject) {}

  DataObject* dataObject;
  StatusCode sc = fEventDataSvc->retrieveObject("/Event", dataObject);
  if(!sc.isSuccess()) {
    msgStream() << MSG::ERROR << "Unable to retrieve /Event " << endmsg;
    return;
  }
  if(!dataObject) {
    msgStream() << MSG::ERROR << "Unable to retrieve /Event " << endmsg;
    return;
  }

  //Event* event = dynamic_cast<Event*>(dataObject);
  //if(!event) return;
  //fSession->out().println("run %d event %d ",event->run(),event->event());
}
//////////////////////////////////////////////////////////////////////////////
StatusCode OnXSvc::visualize(
                             const DataObject& aDataObject
                             )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  msgStream() << MSG::DEBUG << " visualize DataObject... " << endmsg;

  if(!fSoCnvSvc) {
    StatusCode status = service("SoConversionSvc",fSoCnvSvc,true);
    if(status.isFailure() || !fSoCnvSvc) {
      msgStream() << MSG::ERROR << "SoConversionSvc not found " << endmsg;
      return StatusCode::FAILURE;
    }
    fSoCnvSvc->addRef();
  }

  try {
    IOpaqueAddress* addr = 0;
    fCuts = "";
    StatusCode sc = fSoCnvSvc->createRep ((DataObject*)&aDataObject, addr);
    fCuts = "";
    if (sc.isSuccess())
      sc = fSoCnvSvc->fillRepRefs(addr,(DataObject*)&aDataObject);
    if (sc.isFailure()) {
      msgStream() << MSG::WARNING
                  << "Error creating graphical representations"
                  << endmsg;
      return sc;
    }
  } catch (GaudiException e) {
    fCuts = "";
    msgStream() << MSG::ERROR
                << "Unable to create graphical representation, received exception :";
    e.printOut(msgStream());
    msgStream() << endmsg;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode OnXSvc::visualize(
                             const AIDA::IHistogram& aHistogram
                             )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  const DataObject* dataObject = dynamic_cast<const DataObject*>(&aHistogram);
  if(!dataObject) {
    msgStream() << MSG::ERROR << "histogram not a DataObject." << endmsg;
    return StatusCode::FAILURE;
  }

  return visualize(*dataObject);
}
//////////////////////////////////////////////////////////////////////////////
IDataProviderSvc* OnXSvc::dataProvider(const std::string& aPath)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(aPath.find_first_of("E", 1) == 1) {
    if(!fEventDataSvc) {
      StatusCode status = service("EventDataSvc",fEventDataSvc,true);
      if(status.isFailure() || !fEventDataSvc) {
        msgStream() << MSG::WARNING << "EventDataSvc not found " << endmsg;
        return 0;
      }
      fEventDataSvc->addRef();
    }
    return fEventDataSvc;
  } else if( aPath.find_first_of("d", 1) == 1) {
    if(!fDetectorDataSvc) {
      StatusCode status = service("DetectorDataSvc",fDetectorDataSvc,true);
      if(status.isFailure() || !fDetectorDataSvc) {
        msgStream() << MSG::WARNING << "DetectorDataSvc not found " << endmsg;
        return 0;
      }
      fDetectorDataSvc->addRef();
    }
    return fDetectorDataSvc;
  } else if(aPath.find_first_of("s",1)==1) {
    if(!fHistogramSvc) {
      StatusCode status = service("HistogramDataSvc",fHistogramSvc,true);
      if(status.isFailure() || !fHistogramSvc) {
        msgStream() << MSG::WARNING << "HistogramDataSvc not found " << endmsg;
        return 0;
      }
      fHistogramSvc->addRef();
    }
    return fHistogramSvc;
  } else {
    msgStream() << MSG::WARNING
                << "Data service not found for \"" << aPath << "\"."
                << endmsg;
    return 0;
  }
}
//////////////////////////////////////////////////////////////////////////////
StatusCode OnXSvc::visualize(
                             const std::string& aWhat
                             )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!fSoCnvSvc) {
    StatusCode status = service("SoConversionSvc",fSoCnvSvc,true);
    if(status.isFailure() || !fSoCnvSvc) {
      msgStream() << MSG::ERROR << "SoConversionSvc not found " << endmsg;
      return StatusCode::FAILURE;
    }
    fSoCnvSvc->addRef();
  }

  StatusCode sc = StatusCode::FAILURE;

  std::string path = aWhat;
  std::string cuts;

  std::string::size_type sp;
  if((sp=aWhat.find(' '))!=std::string::npos){
    path = aWhat.substr(0,sp);
    cuts = aWhat.substr(sp+1,aWhat.size()-(sp+1));
  }
  msgStream() << MSG::DEBUG << "Path \"" << path << "\"" << endmsg;
  msgStream() << MSG::DEBUG << "Cuts \"" << cuts << "\"" << endmsg;

  IDataProviderSvc* dataSvc = dataProvider(path);
  if(!dataSvc) return StatusCode::FAILURE;

  DataObject* dataObject = 0;
  sc = dataSvc->retrieveObject(path, dataObject);
  if(!sc.isSuccess()) {
    msgStream() << MSG::ERROR << "Unable to retrieve " << path << endmsg;
    return sc;
  }
  if(!dataObject) {
    msgStream() << MSG::ERROR << "Unable to retrieve " << path << endmsg;
    return StatusCode::FAILURE;
  }

  try {
    IOpaqueAddress* addr = 0;
    fCuts = cuts;
    sc = fSoCnvSvc->createRep(dataObject,addr);
    fCuts = "";
    if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,dataObject);
    if (sc.isFailure()) {
      msgStream() << MSG::WARNING
                  << "Error creating graphical representations" << endmsg;
      return sc;
    }
  } catch (GaudiException e) {
    fCuts = "";
    msgStream() << MSG::ERROR << "Unable to convert : "
                << path << endmsg;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
std::vector<std::string> OnXSvc::dataChildren(const std::string& aPath)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  std::vector<std::string> text;

  IDataProviderSvc* dataSvc = dataProvider(aPath);
  if(!dataSvc) return text;
  SmartIF<IDataManagerSvc> dataManagerSvc(dataSvc);
  if(dataManagerSvc==0) {
    msgStream() << MSG::ERROR
                << "Unable to get DataManager interface."
                << endmsg;
    return text;
  }

  SmartDataPtr<DataObject> dataObject(dataSvc,aPath);
  if(dataObject==0) {
    msgStream() << MSG::ERROR
                << "Can't get \"" << aPath << "\"."
                << endmsg;
    return text;
  }

  std::vector<IRegistry*> children;
  dataManagerSvc->objectLeaves(dataObject->registry(),children);
  std::vector<IRegistry*>::iterator it;
  for (it = children.begin();it != children.end();it++) {
    text.push_back((*it)->identifier());
  }

  return text;
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
bool OnXSvc::ls (
                 IDataProviderSvc* aDataSvc
                 ,SmartIF<IDataManagerSvc>& aDataManagerSvc
                 ,SmartDataPtr<DataObject>& aDataObject
                 ,MsgStream& aLog
                 ,int aWantedDepth
                 ,int& aDepth //default = -1 = no limits.
                 )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if((aWantedDepth!=-1)&&(aDepth>=aWantedDepth)) return true;

  std::vector<IRegistry*> children;
  aDataManagerSvc->objectLeaves(aDataObject->registry(),children);
  std::vector<IRegistry*>::iterator it;
  for (it = children.begin();it != children.end();it++) {
    try {

      aLog << MSG::DEBUG << (*it)->identifier() << endmsg;
      SmartDataPtr<DataObject> obj(aDataSvc,(*it));

      if(obj) { // To compell loading of things in the sub directory.
        aDepth++;
        if(!ls(aDataSvc,aDataManagerSvc,obj,aLog,aWantedDepth,aDepth))
          return false;
        aDepth--;
      } else {
        aLog << MSG::ERROR
             << "Unable to load \"" << (*it)->identifier() << "\"." << endmsg;
      }

    } catch (GaudiException e) {
      aLog << MSG::ERROR
           << "Unable to load \"" << (*it)->identifier() << "\"." << endmsg;
      return false;
    }

  }

  return true; //continue;
}
//////////////////////////////////////////////////////////////////////////////
void OnXSvc::ls (
                 const std::string& aPath
                 ,int aDepth //default = -1 = no limits.
                 )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  IDataProviderSvc* dataSvc = dataProvider(aPath);
  if(!dataSvc) return;
  SmartIF<IDataManagerSvc> dataManagerSvc(dataSvc);
  if (dataManagerSvc==0) {
    msgStream() << MSG::ERROR
                << "Unable to get DataManager interface "
                << endmsg;
    return;
  }

  SmartDataPtr<DataObject> dataObject(dataSvc,aPath);
  if(dataObject==0) {
    msgStream() << MSG::ERROR
                << "Can't get \"" << aPath << "\"."
                << endmsg;
    return;
  }

  msgStream() << MSG::DEBUG << aPath << endmsg;

  int depth = 0;

  ls(dataSvc,dataManagerSvc,dataObject,msgStream(),aDepth,depth);
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
void OnXSvc::clearDetectorStore(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!fDetectorDataSvc) {
    StatusCode status = service("DetectorDataSvc",fDetectorDataSvc,true);
    if(status.isFailure() || !fDetectorDataSvc) {
      msgStream() << MSG::WARNING << "DetectorDataSvc not found " << endmsg;
      return;
    }
    fDetectorDataSvc->addRef();
  }

  SmartIF<IDataManagerSvc> dm(fDetectorDataSvc);
  if(dm==0) {
    msgStream() << MSG::ERROR << "No DataManagerSvc. Store not cleared." << endmsg;
    return;
  }

  IXmlSvc* xs = 0;
  service ("XmlCnvSvc", xs);
  if(!xs) {
    msgStream() << MSG::ERROR << "No XmlCnvSvc. Store not cleared." << endmsg;
    return;
  }

  IVisualizationSvc* vs = 0;
  service ("VisualizationSvc", vs);
  if(!vs) {
    msgStream() << MSG::WARNING
                << "No VisualizationSvc. Visualization attributes not reloaded."
                << endmsg;
    return;
  }

  StatusCode sc = dm->clearStore();
  if (!sc.isSuccess()) {
    msgStream() << MSG::ERROR << "Unable to clear store !!!" << endmsg;
    return;
  }
  xs->clearCache();
  vs->reload();
}
//////////////////////////////////////////////////////////////////////////////
StatusCode OnXSvc::changeGeometry(const std::string& file)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Change some properties
  IProperty* pdm = 0;
  service ("DetectorDataSvc", pdm);
  if(!pdm) {
    msgStream() << MSG::ERROR
                << "No DetectorDataSvc found (Looking for IProperty Interface)."
                << endmsg;
    return StatusCode::FAILURE;
  }
  pdm->setProperty("DetDbLocation", file);

  // Now Reinitialize services
  IService* dm = 0;
  service ("DetectorDataSvc", dm);
  if(!dm) {
    msgStream() << MSG::ERROR << "No DetectorDataSvc found." << endmsg;
    return StatusCode::FAILURE;
  }

  IService* xs = 0;
  service ("XmlCnvSvc", xs);
  if(!xs) {
    msgStream() << MSG::ERROR << "No XmlCnvSvc found." << endmsg;
    return StatusCode::FAILURE;
  }

  StatusCode sc = dm->reinitialize();
  if (!sc.isSuccess()) {
    msgStream() << MSG::ERROR << "Unable to reinitialize DetectorDataSvc !" << endmsg;
    return sc;
  }
  sc = xs->reinitialize();
  if (!sc.isSuccess()) {
    msgStream() << MSG::ERROR << "Unable to reinitialize XmlCnvSvc !" << endmsg;
    return sc;
  }
  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode OnXSvc::changeColors(const std::string& file)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Change some properties
  IProperty* pvs = 0;
  service ("VisualizationSvc", pvs);
  if(!pvs) {
    msgStream() << MSG::ERROR
                << "No VisualizationSvc found (Looking for IProperty Interface)."
                << endmsg;
    return StatusCode::FAILURE;
  }
  pvs->setProperty("ColorDbLocation", file);

  // Now Reinitialize services
  IService* vs = 0;
  service ("VisualizationSvc", vs);
  if(!vs) {
    msgStream() << MSG::WARNING << "No VisualizationSvc found." << endmsg;
    return StatusCode::FAILURE;
  }

  StatusCode sc = vs->reinitialize();
  if (!sc.isSuccess()) {
    msgStream() << MSG::ERROR << "Unable to reinitialize VisualizationSvc !" << endmsg;
    return sc;
  }
  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
void OnXSvc::openEventFile(
                           const std::string& aFile
                           )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(aFile=="") return;

  msgStream() << MSG::INFO << "Try to open " << aFile << "..." << endmsg;

  IEvtSelector* eventSelector = 0;
  StatusCode status = service("EventSelector",eventSelector,true);
  if(status.isFailure() || !eventSelector) {
    msgStream() << MSG::ERROR << "EventSelector not found " << endmsg;
    return;
    //return StatusCode::FAILURE;
  }
  eventSelector->addRef();

  SmartIF<IProperty> prp(eventSelector);
  std::string prop =
    std::string("[\"DATAFILE='") + aFile + "' TYP='ROOT' OPT='READ'\"]";

  prp->setProperty("Input",prop);
  SmartIF<IService> prp2(eventSelector);
  prp2->reinitialize();

  eventSelector->release();

  IService* eventLoopMgr = 0;
  status = service("EventLoopMgr",eventLoopMgr,true);
  if(status.isFailure() || !eventLoopMgr) {
    msgStream() << MSG::ERROR << "EventLoopMgr not found " << endmsg;
    return;
  }
  eventLoopMgr->addRef();
  eventLoopMgr->reinitialize();
  eventLoopMgr->release();
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
bool OnXSvc::visitToXML(
                        IDataProviderSvc* aDataSvc
                        ,SmartIF<IDataManagerSvc>& aDataManagerSvc
                        ,SmartDataPtr<DataObject>& aDataObject
                        ,std::string& aOut
                        )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  std::vector<IRegistry*> children;
  aDataManagerSvc->objectLeaves(aDataObject->registry(), children);
  std::vector<IRegistry*>::iterator it;
  for(it=children.begin();it != children.end();it++) {

    std::string path = (*it)->identifier();
    std::string name = path;
    {std::string::size_type pos = path.find_last_of("/");
      if(pos!=std::string::npos) name = path.substr(pos+1,path.size()-pos);}

    aOut +=  "<treeItem><label>" + name + "</label>";

    try {

      SmartDataPtr<DataObject> obj(aDataSvc,(*it));
      if(obj) { // To compell loading of things in the sub directory.
        if(!visitToXML(aDataSvc,aDataManagerSvc,obj,aOut)) {
          aOut = "";
          return false;
        }
      } else {
        msgStream() << MSG::ERROR
                    << "Unable to load \"" << (*it)->identifier() << "\"."
                    << endmsg;
      }

    } catch (GaudiException e) {
      msgStream() << MSG::ERROR
                  << "Unable to load \"" << (*it)->identifier() << "\"."
                  << endmsg;
      aOut = "";
      return false;
    }

    aOut += "</treeItem>";
  }

  return true;
}
//////////////////////////////////////////////////////////////////////////////
bool OnXSvc::writeToString(
                           const std::string& aPath
                           ,std::string& aOut
                           )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  aOut = "";
  if(!fSession) return false;

  IDataProviderSvc* dataSvc = dataProvider(aPath);
  if(!dataSvc) return false;
  SmartIF<IDataManagerSvc> dataManagerSvc(dataSvc);
  if (dataManagerSvc==0) {
    msgStream() << MSG::ERROR
                << "Unable to get DataManager interface "
                << endmsg;
    return false;
  }

  SmartDataPtr<DataObject> dataObject(dataSvc,aPath);
  if(dataObject==0) {
    msgStream() << MSG::ERROR
                << "Can't get \"" << aPath << "\"."
                << endmsg;
    return false;
  }

  aOut = "<tree>";

  std::string name = aPath;
  {std::string::size_type pos = aPath.find_last_of("/");
    if(pos!=std::string::npos) name = aPath.substr(pos+1,aPath.size()-pos);}
  aOut +=  "<treeItem><label>" + name + "</label>";

  if(!visitToXML(dataSvc,dataManagerSvc,dataObject,aOut)) return false;

  aOut += "</treeItem>";
  aOut += "</tree>";
  return true;
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
void OnXSvc::addType(
                     Slash::Data::IAccessor* aType
                     )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!aType) return;
  if(!fSession) {
    // Session not yet set, store types in a temporary list.
    fTypes.push_back(aType);
  } else {
    if(!fTypeManager) return; // Should never happen.
    fTypeManager->addAccessor(aType);
  }
}
//////////////////////////////////////////////////////////////////////////////
IService* OnXSvc::getService(
                             const std::string& aName
                             )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!serviceLocator()) {
    msgStream() << MSG::WARNING << "service locator not found " << endmsg;
    return 0;
  }
  IService* svc = 0;
  StatusCode status = serviceLocator()->service(aName,svc);
  if(status.isFailure() || !svc) {
    msgStream() << MSG::WARNING << aName << " not found " << endmsg;
    return 0;
  }
  return svc;
}
//////////////////////////////////////////////////////////////////////////////
const std::string& OnXSvc::cuts(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fCuts;
}
//////////////////////////////////////////////////////////////////////////////
Slash::Core::IWriter& OnXSvc::printer(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fGaudiPrinter ? *fGaudiPrinter : *fLibPrinter;
}

//////////////////////////////////////////////////////////////////////////////
std::string OnXSvc::torgb(
                          const std::string& aColor
                          )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  double r,g,b;
  if(!Lib::smanip::torgb(aColor,r,g,b)) return "";
  std::string s;
  Lib::smanip::printf(s,128,"%g %g %g",r,g,b);
  return s;
}
//////////////////////////////////////////////////////////////////////////////
void* OnXSvc::topointer(
                        const std::string& aValue
                        ) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  void* p;
  Lib::smanip::topointer(aValue,p);
  return p;
}
//////////////////////////////////////////////////////////////////////////////
IAppMgrUI* OnXSvc::appMgr(
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fAppMgrUI;
}
//////////////////////////////////////////////////////////////////////////////
Slash::UI::IUI* OnXSvc::findUI(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!fSession) {
    msgStream() << MSG::WARNING << "No ISession given " << endmsg;
    return 0;
  }
  Slash::UI::IUI_Manager* uiManager =
    Lib_findManager(*fSession,"UI_Manager",Slash::UI::IUI_Manager);
  if(!uiManager) {
    msgStream() << MSG::WARNING << "UI manager not found " << endmsg;
    return 0;
  }
  Slash::UI::IUI* ui = uiManager->find("default");
  if(!ui) {
    msgStream() << MSG::WARNING << "UI not found " << endmsg;
    return 0;
  }
  return ui;
}

//////////////////////////////////////////////////////////////////////////////
/// Visualization not done by using the Gaudi convertion system //////////////
//////////////////////////////////////////////////////////////////////////////

// Inventor :
#include <Inventor/SbPlane.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoNormalBinding.h>

// HEPVis :
#include <HEPVis/SbStyle.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>
#include <HEPVis/misc/SoTools.h>

// SoUtils :
#include <SoUtils/SbProjector.h>

//////////////////////////////////////////////////////////////////////////////
SbMarkerStyle SoTools_markerStyle (
                                   const std::string& aString
                                   )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  SbStyle sbStyle;
  std::string s = "markerStyle " + aString;
  sbStyle.setFromString(s.c_str());
  return sbStyle.getMarkerStyle();
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
StatusCode OnXSvc::visualize(
                             const Gaudi::XYZPoint& aData
                             )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  msgStream() << MSG::DEBUG << " visualize Gaudi::XYZPoint... " << endmsg;

  SoRegion* region = currentSoRegion();
  if(!region) {
    msgStream() << MSG::ERROR << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE;
  }

  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 0.0;
  std::string value;
  if(fSession->parameterValue("modeling.color",value))
    Lib::smanip::torgb(value,r,g,b);
  if(fSession->parameterValue("modeling.highlightColor",value))
    Lib::smanip::torgb(value,hr,hg,hb);
  // Non linear projections :
  fSession->parameterValue("modeling.projection",value);
  SoUtils::SbProjector projector(value.c_str());

  SbMarkerStyle mStyle = SbMarkerPlus;
  if(fSession->parameterValue("modeling.markerStyle",value))
    mStyle = SoTools_markerStyle(value);
  int mSize = 5;
  if(fSession->parameterValue("modeling.markerSize",value))
    if(!Lib::smanip::toint(value,mSize)) mSize = 5;

  SoSeparator* separator = new SoSeparator;

  // Material :
  SoHighlightMaterial* highlightMaterial = new SoHighlightMaterial;
  highlightMaterial->diffuseColor.setValue
    (SbColor(float(r),float(g),float(b)));
  highlightMaterial->highlightColor.setValue
    (SbColor(float(hr),float(hg),float(hb)));
  //highlightMaterial->transparency.setValue((float)transparency);
  separator->addChild(highlightMaterial);

  SbVec3f point;
  point.setValue((float)aData.x(), (float)aData.y(), (float)aData.z());
  projector.project(1,&point);

  SoTools::addPointsToNode(separator,1,&point,mStyle,mSize);

  //  Send scene graph to the viewing region
  // (in the "dynamic" sub-scene graph) :
  region_addToDynamicScene(*region,separator);

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode OnXSvc::visualize(
                             const std::vector<Gaudi::XYZPoint>& aData
                             ,RepType aType
                             )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  msgStream() << MSG::DEBUG << " visualize std::vector<Gaudi::XYZPoint>... " << endmsg;

  if(!aData.size()) return StatusCode::SUCCESS;
  if((aType==POLYGON)&&(aData.size()<=2)) return StatusCode::FAILURE;

  SoRegion* region = currentSoRegion();
  if(!region) {
    msgStream() << MSG::ERROR << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE;
  }

  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 0.0;
  std::string value;
  if(fSession->parameterValue("modeling.color",value))
    Lib::smanip::torgb(value,r,g,b);
  if(fSession->parameterValue("modeling.highlightColor",value))
    Lib::smanip::torgb(value,hr,hg,hb);
  // Non linear projections :
  fSession->parameterValue("modeling.projection",value);
  SoUtils::SbProjector projector(value.c_str());

  int pointn = aData.size();
  SbVec3f* points = new SbVec3f[pointn];
  for(int index=0;index<pointn;index++) {
    points[index].setValue
      ((float)aData[index].x(),(float)aData[index].y(),(float)aData[index].z());
  }
  projector.project(pointn,points);

  SoSeparator* separator = new SoSeparator;

  // Material :
  SoHighlightMaterial* highlightMaterial = new SoHighlightMaterial;
  highlightMaterial->diffuseColor.setValue
    (SbColor((float)r,(float)g,(float)b));
  highlightMaterial->highlightColor.setValue
    (SbColor((float)hr,(float)hg,(float)hb));
  //highlightMaterial->transparency.setValue((float)transparency);
  separator->addChild(highlightMaterial);

  switch(aType) {
  case POINTS: {
    SbMarkerStyle mStyle = SbMarkerPlus;
    if(fSession->parameterValue("modeling.markerStyle",value))
      mStyle = SoTools_markerStyle(value);
    int mSize = 5;
    if(fSession->parameterValue("modeling.markerSize",value))
      if(!Lib::smanip::toint(value,mSize)) mSize = 5;
    SoTools::addPointsToNode(separator,pointn,points,mStyle,mSize);
  }break;
  case LINES: {
    SoLightModel* lightModel = new SoLightModel;
    lightModel->model = SoLightModel::BASE_COLOR;
    separator->addChild(lightModel);

    SoDrawStyle* drawStyle = new SoDrawStyle;
    drawStyle->style.setValue(SoDrawStyle::LINES);
    drawStyle->linePattern.setValue(0xFFFF);
    separator->addChild(drawStyle);

    SoTools::addLinesToNode(separator,pointn,points);
  }break;
  case SEGMENTS: {
    SoLightModel* lightModel = new SoLightModel;
    lightModel->model = SoLightModel::BASE_COLOR;
    separator->addChild(lightModel);

    SoDrawStyle* drawStyle = new SoDrawStyle;
    drawStyle->style.setValue(SoDrawStyle::LINES);
    drawStyle->linePattern.setValue(0xFFFF);
    separator->addChild(drawStyle);

    SoTools::addSegmentsToNode(separator,pointn,points);
  }break;
  case POLYGON: {
    SoLightModel* lightModel = new SoLightModel;
    lightModel->model = SoLightModel::PHONG;
    separator->addChild(lightModel);

    SoDrawStyle* drawStyle = new SoDrawStyle;
    drawStyle->style.setValue(SoDrawStyle::FILLED);
    separator->addChild(drawStyle);

    SoNormalBinding* normalBinding = new SoNormalBinding;
    normalBinding->value=SoNormalBinding::PER_FACE;
    separator->addChild(normalBinding);

    // It is assumed that the points are in the same plane.
    //WARNING : get the normal from the three first points.
    // It assumes that the three first points can form a plane...
    SbPlane plane(points[0],points[1],points[2]);
    SbVec3f normal = plane.getNormal();
    if(normal.length()<=0) normal.setValue(0,0,1); //Anomaly. Continue anyway.
    SoTools::addPolygonToNode(separator,pointn,points,normal);
  }break;
  }

  //  Send scene graph to the viewing region
  // (in the "dynamic" sub-scene graph) :
  region_addToDynamicScene(*region,separator);

  delete [] points;

  return StatusCode::SUCCESS;
}

#include <Inventor/nodes/SoShape.h>
#include <Inventor/actions/SoSearchAction.h>
#include <HEPVis/nodes/SoSceneGraph.h>

//////////////////////////////////////////////////////////////////////////////
std::vector<std::string> OnXSvc::getHighlightedSoShapeNames()
//////////////////////////////////////////////////////////////////////////////
// From OnX/source/Callbacks/OnX::SceneGraphAccessor::getSceneGraphs
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  std::vector<std::string> names;

  SoRegion* region = currentSoRegion();
  if(!region) {
    msgStream() << MSG::WARNING << " can't get viewing region." << endmsg;
    return names;
  }

  //typedef std::pair<std::string,void*> Loc;
  //std::vector<Loc> locs;

  SoNode* node = region->findNode("part","topSeparator");
  if(!node) {
    msgStream() << MSG::WARNING << " part not found." << endmsg;
    return names;
  }

  SoPathList* list = SoTools::getSceneGraphs(node);
  if(!list) return names;

  unsigned int number = list->getLength();
  for(unsigned int index=0;index<number;index++) {
    SoFullPath* path = (SoFullPath*)(*list)[index];
    SoNode* sep_sceneGraph = path->getTail();
    //NOTE : sep_sceneGraph should be now a SoSceneGraph
    //       on which the "pick string ID" had been deposited.

    // highlighted ?
    bool highlighted = false;
    {SoSearchAction searchAction;
      searchAction.setFind(SoSearchAction::TYPE);
      searchAction.setType(SoHighlightMaterial::getClassTypeId());
      searchAction.apply(sep_sceneGraph);
      SoPath* path = searchAction.getPath();
      if(path) {
        SoHighlightMaterial* material = (SoHighlightMaterial*)path->getTail();
        if(material->getSavedMaterial()) highlighted = true;
      }}

    if(highlighted) {
      if(sep_sceneGraph->isOfType(SoSceneGraph::getClassTypeId())==TRUE) {
        SoSceneGraph* sg = (SoSceneGraph*)sep_sceneGraph;
        names.push_back(sg->getString());
      } else {
        //NOTE : old logic. We should not fall on this case anymore.
        //       But well, we keep the code just in case.
        SoSearchAction searchAction;
        searchAction.setFind(SoSearchAction::TYPE);
        searchAction.setType(SoShape::getClassTypeId());
        searchAction.apply(sep_sceneGraph);
        SoPath* path = searchAction.getPath();
        std::string spath =
          path ? path->getTail()->getName().getString():"nil";
        //printf("debug : %s\n",spath.c_str());
        names.push_back(spath);
      }
    }

  }

  delete list;

  return names;
}
//////////////////////////////////////////////////////////////////////////////
std::vector<ContainedObject*> OnXSvc::getHighlightedContainedObject(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  std::vector<ContainedObject*> objs;

  std::vector<std::string> names = getHighlightedSoShapeNames();
  std::vector<std::string>::const_iterator it;
  for(it=names.begin();it!=names.end(); it++) {

    std::vector<std::string> fields;
    Lib::smanip::words(*it,"/",fields);
    if(fields.size()<=1) {
      msgStream() << MSG::ERROR << " bad syntax in " << (*it) << "." << endmsg;
      objs.clear();
      break;
    }

    //std::string sclass = fields[0];

    std::string sid = fields[1];
    void* id = 0;
    if(!Lib::smanip::topointer(sid,id)) {
      msgStream() << MSG::ERROR << " bad syntax in " << sid << "." << endmsg;
      objs.clear();
      break;
    }

    //WARNING : it assumes that the deposited address is a ContainedObject* !

    objs.push_back((ContainedObject*)id);

  }

  return objs;
}
