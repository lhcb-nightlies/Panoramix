#ifndef ISoConversionSvc_h
#define ISoConversionSvc_h

#include <GaudiKernel/IConversionSvc.h>

static const InterfaceID IID_ISoConversionSvc(91, 1, 0); 

class ISoConversionSvc : virtual public IConversionSvc {
public:
  virtual ~ISoConversionSvc(){}
public:
  static const InterfaceID& interfaceID() { return IID_ISoConversionSvc; }
};			

#endif
