// $Id: SoConversionSvc.h,v 1.5 2008-07-23 14:04:22 truf Exp $ 
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.4  2006/12/21 10:17:21  gybarran
// *** empty log message ***
//
// Revision 1.3  2004/08/26 08:58:45  ranjard
// v7r0 - use Gaudi v15r1
//
// Revision 1.2  2004/03/18 15:31:05  gybarran
// *** empty log message ***
//
// Revision 1.1  2002/05/31 12:41:30  barrand
// G.Barrand : toward Panoramix 4
//
// Revision 1.3  2001/12/12 15:41:54  ibelyaev
//  minor changes to visualize arbitrary DetElem
// 
// ============================================================================
#ifndef SoConversionSvc_h
#define SoConversionSvc_h

// Inheritance :
#include <GaudiKernel/ConversionSvc.h>
#include <OnXSvc/ISoConversionSvc.h>

template <class T> class SvcFactory; 

class SoConversionSvc : 
public ConversionSvc, public ISoConversionSvc {
  friend class SvcFactory<SoConversionSvc>;
public: //IInterface
  StatusCode queryInterface(const InterfaceID&,void**);
public: //IService
  virtual StatusCode initialize();
public:
  
  /** Convert the transient object to the requested representation.
   *  e.g. conversion to persistent objects.
   *  @return    Status code indicating success or failure
   *  @param     Object     Pointer to location of the object 
   *  @param     Address Reference to location of pointer with the 
   *  object address.
   */
  virtual StatusCode createRep   
  ( DataObject*      Object , 
    IOpaqueAddress*& Address ) ;
  
  virtual StatusCode fillRepRefs
  ( IOpaqueAddress*  /* Address */ ,
    DataObject*      /* Object  */ )
  { return StatusCode::SUCCESS; }
  
  virtual StatusCode UpdateRepRefs
  ( IOpaqueAddress*  /* Address */  ,
    DataObject*      /* Object  */  ) 
  { return StatusCode::SUCCESS; }
  
  virtual StatusCode updateRepRefs
  ( IOpaqueAddress*  /* pAddress */ ,
    DataObject*      /* pObject  */ )
  { return StatusCode::SUCCESS; }
  
protected:
  SoConversionSvc(const std::string&,ISvcLocator*);
  virtual ~SoConversionSvc();
};

#endif

