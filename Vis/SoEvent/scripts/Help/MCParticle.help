  The "MCparticle editor" dialog box permits to work on the MCParticle objects.
 
  Choose an "Action". Give input to the corresponding "input fields"
 and click on "Ok" or "Apply" to execute the action. "Cancel" discards
 the panel. Note that this dialog box is a non blocking one (modeless),
 you can put it aside and continue to work with the main panel or other
 dislog boxes.
 
 "Container" field:         Source of MCParticles, default: MC/Particles
 "Decay Descriptor" field:  Input for the MCDecayFinder. More information
 can be found at the end or at 
 http://lhcb-release-area.web.cern.ch/LHCb-release-area/LHCB/doc/html/class_m_c_decay_finder.html
  
 "decay length gt" : filter particles with a decay length greater than xx [mm].
 "origin vx x lt"  : filter particles with an origin vertex in x less than xx [mm].
 "energy cut"  : filter particles with an energy bigger than xx [MeV].

 "Automatic zoom": 
   xy-proj: display particles in xy projection. Center display on first mother particle. 
   zx-proj: display particles in zx projection. Z-axis scaled by 1:8. 
   First mother particle left. 
   no: take geometry of existing page
 
 "Current color": 
   default: use internal color scheme,
     particles with b-quarks: white 
     particles with c-quarks: magenta 
     KS: cyan
     K+/-  :  red
     pi+/- : green
     mu+/- : blue
     e+/-  : darkred
     gamma : yellow
     else : grey
   otherwise choose preferred color

 "Line Width" : specify line width
 "Text":  yes / no, display particle names
 "Text size": text size
 "Text position": position of text in mm along flight direction
  overwritten by default color scheme. Medium == text in the middle    
 
 Action:
  The "visualize" action displays the filtered MCParticles with the display options.
  The "dump" action permits to dump infos about the selected objects.
  "setting": Take options, do nothing else.
   

http://lhcb-release-area.web.cern.ch/LHCb-release-area/LHCB/doc/html/class_m_c_decay_finder.html
The decay is given thru the property 'Decay' as a string which describe what you are looking for. 
The syntax of this expression has been designed to be as powerful as possible. 
It can express inclusive decays as well as (almost) any set of decay.

The description is made of 'particle' and 'decay'. A 'particle' is one of:

a particle name as known by the ParticlePropertySvc,
a set of 'particle's expressed like this: "{A, B, C}",
a set of 'particle's or their charge conjugate, expressed as "[A,B,C]cc",
a 'particle' with it's oscillation flag set ("[A]os"),
a 'wildcard', any particle with some quarks inside is expressed as "<Xq>" 
with up to tree quarks in place of "q". Quarks are u,d,c,s,t,b,u~,d~,... 
A simple '?' is also a wildcard matching any single particle, a 'decay' is one of:
an optional "(" and a 'particle' and the matching ")" if needed,
a "(", a 'particle', one of "->" or "=>", a blank separated list of decay, 
an optional "..." and a ")", e.g. "(B0 -> pi+ pi-)",
a "pp" one of "->" or "=>" and a blank separated list of decay. 
Note: the "()" are not needed around the first decay so the previous exemple could 
simply be written "B0 -> pi+ pi-" but they are mandatory for subdecay 
like "B0 -> (J/psi(1S) -> mu+ mu-) KS0". Note: If you want to find a stable particle 
you must explicily request that it decay to nothing but the arrow has to be dropped. 
So this looks like "B0 -> D_s- (K+)", here the D_s- can decay to whatever but the K+ must not decay. 
NB: Right now secondary interaction are seen as decay! Note: "->" means "decay to" 
and "=>" means "decay to .... with some resonnances in the middle". 
So if you want to catch B0 to D_s- K+ with any D*_s in beetween you simply 
write "B0 => D_s- K+ ...". NB: The "..." is here to catch any number of particles of any 
kind so you get the products of the D*_s-. Note: "pp" stands for "proton - proton collision". 
The decay products will be matched against the particles with no mother. An implicit "..." is appended. 
Only the boolean information for that kind of find can be used, 
the returned particle is set (MCParticle *)1 in case of match NULL otherwise. 
Note: alternate decays can be specified by puting them in a comma seperated list 
surrounded by braces (e.g. "B0 -> pi+ pi- , B0 -> K+ pi-"). 
Note: the charge conjugate operator can also be applied to a set of decay 
(e.g. "[B0 -> K+ pi-]cc" == "B0 -> K+ pi- , B0~ -> K- pi+" (Please note the B0 turned into a B0~)). 
Note: alternate set of daughters can also be specified by putting the sets in a 
comma seperated list surrounded by braces (e.g. "B0 -> {pi+ pi-, K+ pi-, K- pi+, K+ K-}"). 
If one of the alternate set has only one daughter, DO NOT put it first in the list has 
it will not be able to parse the decay correctly (it will fail indeed). 
Note: you can also use the charge conjugate operator on the daughters set. 
Note: you can even mix the []cc and the {}.
Extracting particles from the decay:

It is possible to extract all particle in the decay tree which match a given id 
by preceeding the description with the list of particle you want to be extracted 
(cc, os, nos operator applies here too) terminated by a collon (e.g. "pi+ : B0 -> pi+ pi-").
Precise particle inside the decay tree can also be flagged for extraction by preceeding 
them with a '^' (like "B0 -> (^J/psi(1S) -> mu+ mu-) KS0"). You can then retrieve these 
particles with the MCDecayFinder::members method.
