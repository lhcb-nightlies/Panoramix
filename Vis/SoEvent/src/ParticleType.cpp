// this :
#include "ParticleType.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Variable.h"
#include "Lib/Out.h"

// Gaudi
#include "Kernel/IParticlePropertySvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "Kernel/ParticleProperty.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/IToolSvc.h"
#include "GaudiKernel/IRegistry.h"

// MC relations:
#include "Linker/LinkedTo.h"

#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ISoConversionSvc.h"

//////////////////////////////////////////////////////////////////////////////
ParticleType::ParticleType( IUserInterfaceSvc* aUISvc
                           ,ISoConversionSvc* aSoCnvSvc
                           ,IDataProviderSvc* aDataProviderSvc
                           ,LHCb::IParticlePropertySvc* aParticlePropertySvc
                           ,IToolSvc* aToolSvc
                           ,MsgStream& aMsgStream )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  :SoEvent::Type<LHCb::Particle>( LHCb::Particles::classID(),
                                  "Particle",
                                  "", //No location. Set in iterator method.
                                  aUISvc,
                                  aSoCnvSvc,
                                  aDataProviderSvc )
  ,fParticlePropertySvc(aParticlePropertySvc)
  ,m_trackType ( new TrackType(aUISvc,aSoCnvSvc,aDataProviderSvc,aToolSvc,aMsgStream) )
{
  addProperty("key",Lib::Property::INTEGER);
  addProperty("particle",Lib::Property::STRING);
  addProperty("pid",Lib::Property::INTEGER);
  addProperty("energy",Lib::Property::DOUBLE,0,"(MeV)");
  addProperty("pt",Lib::Property::DOUBLE,0,"(MeV)");
  addProperty("mass",Lib::Property::DOUBLE,0,"(MeV)");
  addProperty("charge",Lib::Property::DOUBLE);
  addProperty("address",Lib::Property::POINTER);
  addProperty("cl",Lib::Property::POINTER);
}
//////////////////////////////////////////////////////////////////////////////
ParticleType::~ParticleType() { delete m_trackType; }
//////////////////////////////////////////////////////////////////////////////
Lib::Variable ParticleType::value(
                                  Lib::Identifier aIdentifier
                                  ,const std::string& aName
                                  ,void*
                                  )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::Particle* obj = (LHCb::Particle*)aIdentifier;
  if(!obj) return Lib::Variable(printer());
  if(aName=="address") {
    return Lib::Variable(printer(),(void*)obj);
  } else if(aName=="key") {
    return Lib::Variable(printer(),obj->key());
  } else if(aName=="energy") {
    return Lib::Variable(printer(),obj->momentum().e());
  } else if(aName=="cl") {
    return Lib::Variable(printer(),obj->confLevel());
  } else if(aName=="pt") {
    const double px = obj->momentum().px();
    const double py = obj->momentum().py();
    return Lib::Variable(printer(),sqrt(px*px+py*py));
  } else if(aName=="mass") {
    double mass = obj->measuredMass();
    return Lib::Variable(printer(),mass);
  } else if(aName=="particle") {
    if(fParticlePropertySvc) {
     const LHCb::ParticleProperty* pp = fParticlePropertySvc->find
        (obj->particleID());
      if(pp) return Lib::Variable(printer(),pp->particle());
    }
    return Lib::Variable(printer(),std::string("nil"));
  } else if(aName=="charge") {
    if(fParticlePropertySvc) {
     const LHCb::ParticleProperty* pp = fParticlePropertySvc->find
        (obj->particleID());
      if(pp) return Lib::Variable(printer(),pp->charge());
    }
    return Lib::Variable(printer(),0.);
  } else if(aName=="pid") {
    return Lib::Variable(printer(),(int)(obj->particleID().pid()));
  } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
Lib::IIterator* ParticleType::iterator( )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  setLocationFromSession();
  return SoEvent::Type<LHCb::Particle>::iterator();
}
//////////////////////////////////////////////////////////////////////////////
void ParticleType::visualize( Lib::Identifier aIdentifier
                              ,void* aData )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if ( !aIdentifier || !fUISvc || !fUISvc->session() ) return;

  // cast to particle
  LHCb::Particle* object = (LHCb::Particle*)aIdentifier;
  if ( !object ) return;

  std::string value;
  fUISvc->session()->parameterValue("modeling.what",value);
  if ( value=="MCParticle" )
  {
    visualizeMCParticle(*object);
  }
  else if ( value=="Particle" )
  {
    visualizeDaughters(*object);
  }
  else if ( value=="RichRecoCKRings"  || value=="RichRecoPixels"  ||
            value=="RichRecoSegments" || value=="RichRecoPhotons" ||
            value=="RichMCPixels"     || value=="RichMCSegments"  ||
            value=="RichMCPhotons"    || value=="RichMCCKRings"    )
  {
    if ( object->proto() && object->proto()->charge() != 0 )
    {
      LHCb::Track * track = const_cast<LHCb::Track*>(object->proto()->track());
      m_trackType->visualize( track, aData );
    }
  }
  else if ( value=="Track")
  {
    if ( object->proto() && object->proto()->charge() != 0 )
    {
      LHCb::Track * track = const_cast<LHCb::Track*>(object->proto()->track());
      m_trackType->visualize( track, aData );
    }
  }
  else
  {
    this->SoEvent::Type<LHCb::Particle>::visualize(aIdentifier,aData);
  }
}
//////////////////////////////////////////////////////////////////////////////
void ParticleType::visualizeMCParticle( LHCb::Particle& aParticle )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Get location :
  const ObjectContainerBase* parentname = aParticle.parent();
  const std::string & namenew = parentname->registry()->identifier(); //finds name and directory of particle

  LinkedTo<LHCb::MCParticle> particleLink(fDataProviderSvc, 0, namenew);

  if ( !particleLink.notFound() )
  {
    LHCb::MCParticles objs;
    LHCb::MCParticle* obj = particleLink.first(aParticle.key());
    while ( 0 != obj )
    {
      objs.add(obj);
      obj = particleLink.next();
    }
    // draw
    drawContainer(objs);
  }
}
//////////////////////////////////////////////////////////////////////////////
void ParticleType::visualizeDaughters( LHCb::Particle& p )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  const LHCb::Particle::ConstVector pv = p.daughtersVector();
  // loop on daughters
  LHCb::Particles objs;
  for ( LHCb::Particle::ConstVector::const_iterator ip = pv.begin();
        ip!=pv.end();++ip)
  {
    LHCb::Particle* obj = const_cast<LHCb::Particle*>(*ip);
    objs.add(obj);
  // draw the particles
    drawContainer(objs);
  }
}
//////////////////////////////////////////////////////////////////////////////
