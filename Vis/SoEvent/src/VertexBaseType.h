#ifndef SoEvent_VertexBaseType_h
#define SoEvent_VertexBaseType_h

// Inheritance :
#include "Type.h"

#include "Event/VertexBase.h" // The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;

class VertexBaseType : public SoEvent::Type<LHCb::VertexBase> {
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public:
  VertexBaseType(IUserInterfaceSvc*,
         ISoConversionSvc*,
         IDataProviderSvc*);
  Lib::IIterator* iterator();
private:
};

#endif
