#ifndef SoEvent_SoTrackCnv_h
#define SoEvent_SoTrackCnv_h

#include "SoEventConverter.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Event/TrackTypes.h"

template <class T> class CnvFactory;

class SoTrackCnv : public SoEventConverter {
  //friend class CnvFactory<SoTrackCnv>;
public:
  SoTrackCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
