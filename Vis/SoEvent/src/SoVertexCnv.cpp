// To fix clashes between Gaudi and Windows :
#include "OnXSvc/Win32.h"

// this :
#include "SoVertexCnv.h"

// Inventor :
#include "Inventor/nodes/SoSeparator.h"
#include "Inventor/nodes/SoLightModel.h"
#include "Inventor/nodes/SoDrawStyle.h"
#include "Inventor/nodes/SoCoordinate3.h"
#include "Inventor/nodes/SoSphere.h"
#include "Inventor/nodes/SoTransform.h"
#include "Inventor/nodes/SoLineSet.h"
#include "HEPVis/nodes/SoTextTTF.h"
#include "HEPVis/nodes/SoEllipsoid.h"

#include "HEPVis/nodes/SoSceneGraph.h"
#include "HEPVis/nodes/SoMarkerSet.h"
typedef HEPVis_SoMarkerSet SoMarkerSet;
#include "HEPVis/misc/SoStyleCache.h"

// HEPVis :
#include "HEPVis/nodes/SoHighlightMaterial.h"

// Lib :
#include "Lib/smanip.h"
#include "Lib/Interfaces/ISession.h"

// Gaudi :
#include "GaudiKernel/MsgStream.h"
#include "Kernel/IParticlePropertySvc.h"
#include "GaudiKernel/CnvFactory.h"
#include "Kernel/ParticleProperty.h"

// LHCb :
#include "Event/Particle.h"

// SoUtils :
#include "SoUtils/SbProjector.h"

// OnXSvc :
#include "OnXSvc/Filter.h"
#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ClassID.h"
#include "OnXSvc/Helpers.h"

DECLARE_CONVERTER_FACTORY( SoVertexCnv )

//////////////////////////////////////////////////////////////////////////////
SoVertexCnv::SoVertexCnv(
                         ISvcLocator* aSvcLoc
                         )
  :SoEventConverter(aSvcLoc,SoVertexCnv::classID())
                        //////////////////////////////////////////////////////////////////////////////
                        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoVertexCnv::createRep(
                                  DataObject* aObject
                                  ,IOpaqueAddress*& aAddr
                                  )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log(msgSvc(), "SoVertexCnv");
  //log << MSG::INFO << "SoVertexCnv createReps" << endmsg;

  if(!fUISvc) {
    log << MSG::INFO << " UI service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  if(!fParticlePropertySvc) {
    log << MSG::INFO << " ParticleProperty service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  ISession* session = fUISvc->session();
  if(!session) {
    log << MSG::INFO << " can't get ISession." << endmsg;
    return StatusCode::FAILURE;
  }

  SoRegion* region = 0;
  if(aAddr) {
    // Optimization.
    // If having a not null aAddr, we expect a SoRegion.
    // See SoEvent/Type.h/SoEvent::Type<>::beginVisualize.
    region = (SoRegion*)aAddr;
  } else {
    region = fUISvc->currentSoRegion();
  }
  if(!region) {
    log << MSG::INFO << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE;
  }

  if(!aObject) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE;
  }

  LHCb::Vertices* vertices = dynamic_cast<LHCb::Vertices*>(aObject);
  if( !vertices ) {
    log << MSG::INFO << " bad object type." << endmsg;
    return StatusCode::FAILURE;
  }

  bool deleteVector = false;
  // Filter :
  Filter<LHCb::Vertex> filter(*fUISvc,log);
  const std::string& cuts = fUISvc->cuts();
  if(cuts!="") {
    log << MSG::INFO << " cuts \"" << cuts << "\"" << endmsg;
    vertices = filter.collect(*vertices,"Vertex",cuts);
    if(!vertices) return StatusCode::SUCCESS;
    //filter.dump(*mcParticles,"MCParticle");
    deleteVector = true;;
  }

  if( !vertices->size()) return StatusCode::SUCCESS;

  // Representation attributes :
  // Get color (default is grey (valid on black or white background) ):
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 0.0;
  std::string value;
  if(session->parameterValue("modeling.color",value))
    Lib::smanip::torgb(value,r,g,b);
  if(session->parameterValue("modeling.highlightColor",value))
    Lib::smanip::torgb(value,hr,hg,hb);
  // Non linear projections :
  session->parameterValue("modeling.projection",value);
  SoUtils::SbProjector projector(value.c_str());

  // One scene graph per object ?
  int multiNodeLimit = 1000;
  if(session->parameterValue("modeling.multiNodeLimit",value))
    if(!Lib::smanip::toint(value,multiNodeLimit)) multiNodeLimit = 1000;


  // One scene graph per object :

  // Representation attributes :
  bool showText = false;
  session->parameterValue("modeling.showText",value);
  Lib::smanip::tobool(value,showText);
  int sizeText = 10; //SoTextTTF default.
  session->parameterValue("modeling.sizeText",value);
  if(!Lib::smanip::toint(value,sizeText)) sizeText = 10;
  session->parameterValue("modeling.posText",value);
  std::string posTextAtb = value=="" ? "medium" : value;

  SoStyleCache* styleCache = region->styleCache();
  SoLightModel* lightModel = styleCache->getLightModelBaseColor();
  SoDrawStyle* drawStyle = styleCache->getLineStyle(SbLinePattern_solid);
  SoMaterial* highlightMaterial =
    styleCache->getHighlightMaterial(float(r),float(g),float(b),
                                     float(hr),float(hg),float(hb),
                                     0,
                                     projector.isZR()?TRUE:FALSE);

  LHCb::Vertices::iterator it;
  for(it = vertices->begin(); it != vertices->end(); it++) {
    LHCb::Vertex* Vertex  = (*it);
    //if(!(*it)->pointOnRec()) continue;

    // Build name :
    char sid[64];
    ::sprintf(sid,"Vertex/0x%lx",(unsigned long)Vertex);

    SoSceneGraph* separator = new SoSceneGraph;
    separator->setString(sid);

    // Material :
    separator->addChild(highlightMaterial);

    //const Vertex* Vertex = particle->endVertex();

    separator->addChild(lightModel);
    separator->addChild(drawStyle);

    const Gaudi::XYZPoint& pos = Vertex->position();
    const Gaudi::SymMatrix3x3& poserr = Vertex->covMatrix();

    double ex = 3.*sqrt(poserr(0,0));
    double ey = 3.*sqrt(poserr(1,1));
    double ez = 3.*sqrt(poserr(2,2));

    if(! projector.isZR()) {
      // ellipsoid
      SoEllipsoid* ell = new SoEllipsoid();
      ell->center.setValue( float(pos.x()) , float(pos.y()) , float(pos.z()) );
      ell->eigenvalues.setValue( float(ex) , float(ey), float(ez) );
      separator->addChild(ell);

    }
    else {
      SbVec3f pointall[3];
      int pointn = 2;

      // here we should project an ellipse for the time being we average
      // the x/y errors and project a circle

      double radi = sqrt(pos.x() * pos.x() + pos.y() * pos.y());
      double erav = 0.5 * (ex + ey);
      double xmax = pos.x() * (radi + erav)/radi;
      double ymax = pos.y() * (radi + erav)/radi;
      double xmin = pos.x() * (radi - erav)/radi;
      double ymin = pos.y() * (radi - erav)/radi;
      pointall[0].setValue(float(xmax), float(ymax), float(pos.z()));
      pointall[1].setValue(float(xmin), float(ymin), float(pos.z()));
      if(erav > radi) {
        pointn = 3;
        pointall[2].setValue(0., 0., float(pos.z()));
      }
      // Add one So per particle
      SoCoordinate3* coordinate3x = new SoCoordinate3;
      projector.project(3,pointall);
      coordinate3x->point.setValues(0,3,pointall);
      SoLineSet* lineSetx = new SoLineSet;
      lineSetx->numVertices.setValues(0,1,&pointn);
      separator->addChild(coordinate3x);
      separator->addChild(lineSetx);
      pointall[0].setValue(float(pos.x()), float(pos.y()), float(pos.z() - ez));
      pointall[1].setValue(float(pos.x()), float(pos.y()), float(pos.z() + ez));
      pointall[2].setValue(float(pos.x()), float(pos.y()), float(pos.z()));
      // Add one So per particle
      SoCoordinate3* coordinate3z = new SoCoordinate3;
      projector.project(3,pointall);
      coordinate3z->point.setValues(0,3,pointall);
      SoLineSet* lineSetz = new SoLineSet;
      lineSetz->numVertices.setValues(0,1,&pointn);
      separator->addChild(coordinate3z);
      separator->addChild(lineSetz);
    }
    SoCoordinate3* coordinate3 = new SoCoordinate3;
    // 1 points per Vertex
    SbVec3f pointe;
    pointe.setValue(float(pos.x()), float(pos.y()), float(pos.z()));
    projector.project(1,&pointe);
    coordinate3->point.setValues(0,1,&pointe);
    separator->addChild(coordinate3);
    // marker
    SoMarkerSet* markerSet = new SoMarkerSet;
    markerSet->numPoints = 1;
    markerSet->markerIndex = SoMarkerSet::CROSS_5_5;
    separator->addChild(markerSet);
    SbVec3f textPoint = pointe;

    // Text :
    if(showText) {
      SoSeparator* sepText = new SoSeparator;
      separator->addChild(sepText);
      SoTransform* tsf = new SoTransform;
      tsf->translation.setValue(textPoint);
      sepText->addChild(tsf);
      SoTextTTF* text = new SoTextTTF();
      text->size.setValue(sizeText);
      std::string mcname("Vertex");
      text->string.set1Value(0,mcname.c_str());
      sepText->addChild(text);
    }

    //  Send scene graph to the viewing region
    // (in the "dynamic" sub-scene graph) :
    region_addToDynamicScene(*region,separator);

  }//end loop

  if(deleteVector) {
    // We have first to empty the vector :
    while(vertices->size()) {
      vertices->remove(*(vertices->begin()));
    }
    // Then we can delete it :
    delete vertices;
  }

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoVertexCnv::classID(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return LHCb::Vertices::classID();
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoVertexCnv::storageType(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return So_TechnologyType;
}
