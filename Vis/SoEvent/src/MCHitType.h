#ifndef SoEvent_MCHitType_h
#define SoEvent_MCHitType_h

// Inheritance :
#include "OnX/Core/BaseType.h"

#include "Lib/Interfaces/IIterator.h"
#include "Kernel/IParticlePropertySvc.h"


class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IParticlePropertySvc;

class MCHitType : public OnX::BaseType {
public: //Lib::IType
  virtual std::string name() const;
  virtual Lib::IIterator* iterator();
  virtual void setIterator(Lib::IIterator*);  
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public: //OnX::IType
  virtual void visualize(Lib::Identifier,void*);
public:
  MCHitType(IUserInterfaceSvc*,ISoConversionSvc*,IDataProviderSvc*,
	    LHCb::IParticlePropertySvc*,const std::string&);
private:
  std::string fType;
  std::string fDetector;
  Lib::IIterator* fIterator;
  IUserInterfaceSvc* fUISvc;
  ISoConversionSvc* fSoCnvSvc;
  IDataProviderSvc* fDataProviderSvc;
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
};

#endif
