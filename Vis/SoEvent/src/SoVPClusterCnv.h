#ifndef SoEvent_SoVPClusterCnv_h
#define SoEvent_SoVPClusterCnv_h

#include "SoEventConverter.h"

template <class T> class CnvFactory;

class SoVPClusterCnv : public SoEventConverter {
  //friend class CnvFactory<SoVPClusterCnv>;
public:
  SoVPClusterCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
