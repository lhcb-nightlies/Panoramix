#ifndef SoEvent_SoOnStoredTrackCnv_h
#define SoEvent_SoOnStoredTrackCnv_h

#include "SoEventConverter.h"

// Gaudi :
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/CnvFactory.h"
#include "GaudiKernel/KeyedContainer.h"

#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/ClassID.h"

template <class T> class CnvFactory;

template <class ObjectOnStoredTrack, class Object>
class SoOnStoredTrackCnv : public SoEventConverter {
  //friend class CnvFactory< SoOnStoredTrackCnv<ObjectOnStoredTrack,Object> >;
  typedef KeyedContainer< ObjectOnStoredTrack, Containers::HashMap> CollectionOnStoredTrack;
public:
  SoOnStoredTrackCnv(ISvcLocator* aSvcLoc)
    :SoEventConverter(aSvcLoc,SoOnStoredTrackCnv<ObjectOnStoredTrack,Object>::classID()) {}
  virtual StatusCode createRep(DataObject* aObject,IOpaqueAddress*&){
    MsgStream log(msgSvc(), "SoOnStoredTrackCnv");
    //log << MSG::INFO << "SoOnStoredTrack::createRep." << endmsg;
    if(!aObject) {
      log << MSG::INFO << " NULL object." << endmsg;
      return StatusCode::FAILURE; 
    }
    CollectionOnStoredTrack* collectionOnStoredTrack = 
      dynamic_cast<CollectionOnStoredTrack*>(aObject);
    if(!collectionOnStoredTrack) {
      log << MSG::INFO << " bad object type." << endmsg;
      return StatusCode::FAILURE; 
    }
    if(!collectionOnStoredTrack->size()) {
      log << MSG::INFO << " collection is empty." << endmsg;
      return StatusCode::SUCCESS; 
    }
    typedef KeyedContainer< Object, Containers::HashMap> Collection;
    /* Does not work (some exception when filling the collection)
    Collection* collection = new Collection;
    CollectionOnStoredTrack::iterator it;
    for(it = collectionOnStoredTrack->begin(); 
	it != collectionOnStoredTrack->end(); it++) {
      ObjectOnStoredTrack* objectOnStoredTrack  = (*it);
      Object* object = objectOnStoredTrack->cluster();
      if(!object) continue;
      collection->add(object);
    }
    // Convert it :
    IOpaqueAddress* addr = 0;
    StatusCode sc = fSoCnvSvc->createRep(collection, addr);
    if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,collection);
    // Delete the collection :
    while(collection->size()) collection->remove(*(collection->begin()));
    delete collection;
    */
    typename CollectionOnStoredTrack::iterator it;
    for(it = collectionOnStoredTrack->begin(); 
	it != collectionOnStoredTrack->end(); it++) {
      ObjectOnStoredTrack* objectOnStoredTrack  = (*it);
      Object* object = objectOnStoredTrack->cluster();
      if(!object) continue;
      //
      Collection* collection = new Collection;
      collection->add(object);
      // Convert it :
      IOpaqueAddress* addr = 0;
      StatusCode sc = fSoCnvSvc->createRep(collection, addr);
      if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,collection);
      // Delete the collection :
      while(collection->size()) collection->remove(*(collection->begin()));
      delete collection;
    }

    return StatusCode::SUCCESS;
  }
public:
  static const CLID& classID() { return CollectionOnStoredTrack::classID(); }
  static const unsigned char storageType() { return So_TechnologyType;}
};

//== OC : As OTCluster has no longer a 'cluster' method, cut and paste class for it !

template <class ObjectOnStoredTrack, class Object>
class SoOTOnStoredTrackCnv : public SoEventConverter {
  friend class CnvFactory< SoOTOnStoredTrackCnv<ObjectOnStoredTrack,Object> >;
  typedef KeyedContainer< ObjectOnStoredTrack, Containers::HashMap> CollectionOnStoredTrack;
public:
  SoOTOnStoredTrackCnv(ISvcLocator* aSvcLoc)
    :SoEventConverter(aSvcLoc,SoOTOnStoredTrackCnv<ObjectOnStoredTrack,Object>::classID()) {}
  virtual StatusCode createRep(DataObject* aObject,IOpaqueAddress*&){
    MsgStream log(msgSvc(), "SoOTOnStoredTrackCnv");
    //log << MSG::INFO << "SoOTOnStoredTrack::createRep." << endmsg;
    if(!aObject) {
      log << MSG::INFO << " NULL object." << endmsg;
      return StatusCode::FAILURE; 
    }
    CollectionOnStoredTrack* collectionOnStoredTrack = 
      dynamic_cast<CollectionOnStoredTrack*>(aObject);
    if(!collectionOnStoredTrack) {
      log << MSG::INFO << " bad object type." << endmsg;
      return StatusCode::FAILURE; 
    }
    if(!collectionOnStoredTrack->size()) {
      log << MSG::INFO << " collection is empty." << endmsg;
      return StatusCode::SUCCESS; 
    }
    typedef KeyedContainer< Object, Containers::HashMap> Collection;
    /* Does not work (some exception when filling the collection)
    Collection* collection = new Collection;
    CollectionOnStoredTrack::iterator it;
    for(it = collectionOnStoredTrack->begin(); 
	it != collectionOnStoredTrack->end(); it++) {
      ObjectOnStoredTrack* objectOnStoredTrack  = (*it);
      Object* object = objectOnStoredTrack->time();
      if(!object) continue;
      collection->add(object);
    }
    // Convert it :
    IOpaqueAddress* addr = 0;
    StatusCode sc = fSoCnvSvc->createRep(collection, addr);
    if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,collection);
    // Delete the collection :
    while(collection->size()) collection->remove(*(collection->begin()));
    delete collection;
    */
    typename CollectionOnStoredTrack::iterator it;
    for(it = collectionOnStoredTrack->begin(); 
	it != collectionOnStoredTrack->end(); it++) {
      ObjectOnStoredTrack* objectOnStoredTrack  = (*it);
      Object* object = objectOnStoredTrack->time();      //*** Main change !
      if(!object) continue;
      //
      Collection* collection = new Collection;
      collection->add(object);
      // Convert it :
      IOpaqueAddress* addr = 0;
      StatusCode sc = fSoCnvSvc->createRep(collection, addr);
      if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,collection);
      // Delete the collection :
      while(collection->size()) collection->remove(*(collection->begin()));
      delete collection;
    }

    return StatusCode::SUCCESS;
  }
public:
  static const CLID& classID() { return CollectionOnStoredTrack::classID(); }
  static const unsigned char storageType() { return So_TechnologyType;}
};
#endif
