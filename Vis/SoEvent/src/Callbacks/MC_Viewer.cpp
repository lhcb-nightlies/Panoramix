//
// A compiled version of scripts/Python/MC_Viewer.py
// in order to help debugging things.
//   G.Barrand
//

//
//  All functions here should be OnX callbacks, that is to say
// functions with signature :
//   extern "C" {
//     void callback_without_arguments(IUI&);
//     void callback_with_arguments(IUI&,const std::vector<std::string>&);
//   }
//


// OnX :
#include <OnX/Interfaces/IUI.h>

// Lib :
#include <Lib/Interfaces/ISession.h>
#include <Lib/Interfaces/IPrinter.h>
#include <Lib/Manager.h>
#include <Lib/Out.h>
#include <Lib/smanip.h>
#include <Lib/sout.h>

// Gaudi :
#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/IService.h>
#include <GaudiKernel/IDataManagerSvc.h>
#include <GaudiKernel/IRegistry.h>

#include <Kernel/ParticleProperty.h>

#include <OnX/Helpers/Inventor.h>

#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoTransform.h>

#include <HEPVis/nodekits/SoPage.h>
#include <HEPVis/nodekits/SoRegion.h>
#include <HEPVis/nodekits/SoDisplayRegion.h>

// OnXSvc :
#include <OnXSvc/ISvcLocatorManager.h>

// Event :
#include <Kernel/IMCDecayFinder.h>
#include <Kernel/IDebugTool.h>

#include <math.h>

#include "Helpers.h"

inline void set_color(
 ISession& a_session
,LHCb::MCParticle& a_d
,double a_tscale
) 
{
  LHCb::IParticlePropertySvc* ppSvc = find_particlePropertySvc(a_session);
  std::string partname = ppSvc->find(a_d.particleID())->particle();

  double tpos = 0.1;
  a_session.setParameter("modeling.color","grey");
  if(a_d.particleID().hasBottom()) {
    a_session.setParameter("modeling.color","white");
  } else if(a_d.particleID().hasCharm()) {
    a_session.setParameter("modeling.color","magenta");
  } else if(partname == "KS0" ) {
    a_session.setParameter("modeling.color","cyan");
    tpos = 2.;
  } else if(partname == "K+" || partname == "K-" ) {
    a_session.setParameter("modeling.color","red");
    tpos = 1.;
  } else if(partname == "pi+" || partname == "pi-") {
    a_session.setParameter("modeling.color","green");
    tpos = 0.5;
  } else if(partname == "mu+" || partname == "mu-") {
    a_session.setParameter("modeling.color","blue");
    tpos = 3.;
  } else if(partname == "e+" || partname == "e-") {
    a_session.setParameter("modeling.color","darkred");
    tpos =3.;
  } else if(partname == "gamma" ) {
    a_session.setParameter("modeling.color","yellow");
    tpos = 4.;
  }
  std::string textpos;
  Lib::smanip::printf(textpos,32,"%g",a_tscale * tpos);
  a_session.setParameter("modeling.posText",textpos);
}

inline bool filter(
 ISession& a_session
,LHCb::MCParticle& a_d
,std::string a_kind
,double a_stable
,double a_energy
,double a_ovx
) {

  double e = a_d.momentum().E();
  int ch = a_d.particleID().threeCharge();
  Gaudi::XYZPoint eVx = a_d.endVertices()[0]->position();
  Gaudi::XYZPoint oVx = a_d.primaryVertex()->position();
  double declength = ::sqrt( 
        (eVx.x()-oVx.x())*(eVx.x()-oVx.x())+
        (eVx.y()-oVx.y())*(eVx.y()-oVx.y())+
	(eVx.z()-oVx.z())*(eVx.z()-oVx.z()) );

  LHCb::IParticlePropertySvc* ppSvc = find_particlePropertySvc(a_session);

  std::string partname = ppSvc->find(a_d.particleID())->particle();
  bool filter1 = 
    ((ch == 0)&&(a_kind=="neutral"))||((a_kind!="neutral")&&(a_kind!="charged"));
  bool filter2 = e > a_energy;
  bool filter3 = declength > a_stable;
  bool veto1 = (partname=="KS0")||(partname=="KL0")||(partname=="Lambda");
  std::string mothname;
  if(a_d.mother()) {
    mothname = ppSvc->find(a_d.mother()->particleID())->particle();
  }
  bool veto2 = (mothname=="KS0")||(mothname=="KL0")||(mothname=="Lambda");
  bool filter4 = (a_d.originVertex()->position().z() < a_ovx)|| veto1 || veto2;
  return filter1 && filter2 && filter3 && filter4;
}

inline std::pair<double,std::string> dump(
 ISession& a_session
,LHCb::MCParticle& a_d
) {
  LHCb::IParticlePropertySvc* ppSvc = find_particlePropertySvc(a_session);

  //double e = a_d.momentum().E();
  //double pt = a_d.pt();
  //double ch = a_d.particleID().threeCharge()/3.;
  Gaudi::XYZPoint eVx = a_d.endVertices()[0]->position();
  Gaudi::XYZPoint oVx = a_d.originVertex()->position();
  int pid = a_d.particleID().pid();
  int pidmk = -1;
  std::string pidmn;
  if(a_d.mother()) {
    pidmk = a_d.mother()->key();
    pidmn = ppSvc->find(a_d.mother()->particleID())->particle();
  }
  /*
  double declength = ::sqrt( 
        (eVx.x()-oVx.x())*(eVx.x()-oVx.x())+
        (eVx.y()-oVx.y())*(eVx.y()-oVx.y())+
        (eVx.z()-oVx.z())*(eVx.z()-oVx.z()) );
  */
  std::string partname = ppSvc->find(a_d.particleID())->particle();

  std::string printout;
  std::string s;
  Lib::smanip::printf(s,32,"%4i",a_d.key()); printout += s;
  Lib::smanip::printf(s,32,"%12s",partname.c_str()); printout += s;
  Lib::smanip::printf(s,32,"%9i",pid); printout += s;
  //printout = '%4i'%(a_d.key())+'%12s'%(partname)+'%9i'%(pid)+'%10.2f'%(e)+
  //'%10.2f'%(pt)+'%5i'%(ch)+'%9.2f'%(a_d.virtualMass())+'%10.2f'%(declength)+'%5i'%(a_d.endVertices().size())+'%10i'%(pidmk)+'%12s'%(pidmn)
      //return [ ,printout]
  return std::pair<double,std::string>(a_d.originVertex()->position().z(),printout);                                  
}

extern "C" {

//////////////////////////////////////////////////////////////////////////////
void MC_Viewer_ok(
 IUI& aUI
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  ISession& session = aUI.session();

  //std::string what;
  //if(!aUI.parameterValue("optionTree.value",what)) return;

  // Dialog inputs : 
  std::string container;
  if(!aUI.parameterValue("SoEvent_MCParticle_input_container.value",container)) return;
  double stable;
  if(!ui_parameterValue(aUI,"SoEvent_MCParticle_input_stable.value",stable)) return;
  double ovx;
  if(!ui_parameterValue(aUI,"SoEvent_MCParticle_input_ovx.value",ovx)) return;
  std::string kind;
  if(!aUI.parameterValue("SoEvent_MCParticle_input_kind.value",kind)) return;
  std::string decaystr;
  if(!aUI.parameterValue("SoEvent_MCParticle_input_decay.value",decaystr)) return;
  double energy;
  if(!ui_parameterValue(aUI,"SoEvent_MCParticle_input_energy.value",energy)) return;
  std::string azoom;
  if(!aUI.parameterValue("SoEvent_MCParticle_input_azoom.value",azoom)) return;
  std::string scolor;
  if(!aUI.parameterValue("SoEvent_MCParticle_input_color.value",scolor)) return;
  std::string linewidth;
  if(!aUI.parameterValue("SoEvent_MCParticle_input_linewidth.value",linewidth)) return;
  std::string text;
  if(!aUI.parameterValue("SoEvent_MCParticle_input_text.value",text)) return;
  std::string textsize;
  if(!aUI.parameterValue("SoEvent_MCParticle_input_textsize.value",textsize)) return;
  std::string textpos;
  if(!aUI.parameterValue("SoEvent_MCParticle_input_textpos.value",textpos)) return;
  int maxDepth;
  if(!ui_parameterValue(aUI,"SoEvent_MCParticle_input_maxdepth.value",maxDepth)) return;
  std::string action;
  if(!aUI.parameterValue("SoEvent_MCParticle_input_action.value",action)) return;

  double tscale = 1.;
  if(textpos!="medium") {
    double tscale;
    Lib::smanip::todouble(textpos,tscale);
  }
  session.setParameter("modeling.lineWidth",linewidth);
  session.setParameter("modeling.sizeText",textsize);
  session.setParameter("modeling.posText",textpos);

  if(text=="yes") {
    session.setParameter("modeling.showText","true");
  } else {
    session.setParameter("modeling.showText","false");
  }
 
  if(scolor!="default") {
    session.setParameter("modeling.color",scolor);
  }

  IDataProviderSvc* dataSvc = find_eventDataSvc(session);
  if(!dataSvc) {
    Lib::Out out(session.printer());
    out << "MC_Viewer_ok :"
        << " unable to find the event data service."
        << Lib::endl;
    return;
  }

  DataObject* dataObject = 0;
 {StatusCode sc = dataSvc->retrieveObject(container, dataObject);
  if(sc.isFailure() || !dataObject){
    Lib::Out out(session.printer());
    out << "MC_Viewer_ok :"
        << "Can't get " << container
        << Lib::endl;
    return;
  }}
  LHCb::MCParticles* mc = dynamic_cast<LHCb::MCParticles*>(dataObject);
  if(!mc) {
    Lib::Out out(session.printer());
    out << "MC_Viewer_ok :"
        << " data object not an LHCb::MCParticles."
        << Lib::endl;
    return;
  }

  IUserInterfaceSvc* uiSvc = find_uiSvc(session);
  if(!uiSvc) {
    Lib::Out out(session.printer());
    out << "MC_Viewer_ok :"
        << " user interface service not found."
        << Lib::endl;
    return;
  }

  ISoConversionSvc* soCnvSvc = find_soCnvSvc(session);
  if(!soCnvSvc) return;

  IToolSvc* toolSvc = find_toolSvc(session);
  if(!toolSvc) {
    Lib::Out out(session.printer());
    out << "MC_Viewer_ok :"
        << " unable to find the tool service."
        << Lib::endl;
    return;
  }

  IMCDecayFinder* mcDecayFinder = 0;
 {StatusCode sc = toolSvc->retrieveTool("MCDecayFinder",mcDecayFinder);
  if(sc.isFailure() || !mcDecayFinder ){
    Lib::Out out(session.printer());
    out << "MC_Viewer_ok :"
        << " unable to find the MCDecayFinder tool."
        << Lib::endl;
    return;
  }}

  IDebugTool* debugTool = 0;
 {StatusCode sc = toolSvc->retrieveTool("DebugTool",debugTool);
  if(sc.isFailure() || !debugTool ){
    Lib::Out out(session.printer());
    out << "MC_Viewer_ok :"
        << " unable to find the DebugTool tool."
        << Lib::endl;
    return;
  }}

  if(action ==  "dump") {
    std::string printout;
    std::string s;
    Lib::smanip::printf(s,32,"%4s","key"); printout += s;
    Lib::smanip::printf(s,32,"%12s","Part.Name"); printout += s;
    Lib::smanip::printf(s,32,"%7s","pid"); printout += s;
    //print '%4s'%('key'),'%12s'%('Part.Name'),'%7s'%('pid'),'%12s'%('energy(MeV)'),'%7s'%('pt(MeV)'),'%4s'%('ch'),'%8s'%('Mass(MeV)'),'%8s'%('tau(mm)'),'%3s'%('nr eVx'),'%14s'%('mother')
    Lib::Out out(session.printer());
    out << printout << Lib::endl;
  }

  std::vector<LHCb::MCParticle*> decaylist;

  std::vector< std::pair<double,std::string> > sorted_list;

  if(decaystr!="") {
    mcDecayFinder->setDecay(decaystr);
    if(mcDecayFinder->hasDecay(*mc)) {
      const LHCb::MCParticle* part = 0;
      while(mcDecayFinder->findDecay(*mc,part)) {
        if(!part) {
          Lib::Out out(session.printer());
          out << "MC_Viewer_ok :"
              << " null decay part in container !"
              << Lib::endl;
          return;
        }
        LHCb::MCParticle* obj = mc->object(part->key());
        if(!part) {
          Lib::Out out(session.printer());
          out << "MC_Viewer_ok :"
              << " null object from part->key !"
              << Lib::endl;
          return;
        }
        decaylist.push_back(obj);
      }
      std::vector<LHCb::MCParticle*>::iterator it;
      for(it=decaylist.begin();it!=decaylist.end();it++) {
	std::vector<LHCb::MCParticle*> daughters;
        mcDecayFinder->descendants(*it,daughters);

        std::vector< std::pair<double,std::string> > dlist;
       {std::vector<LHCb::MCParticle*>::iterator it;
        for(it=daughters.begin();it!=daughters.end();it++) {     
          if(filter(session,*(*it),kind,stable,energy,ovx)) {
            if(action=="visualize") {
              if(scolor=="default") set_color(session,*(*it),tscale);
              KO_visualize<LHCb::MCParticle>(*soCnvSvc,*(*it));
            } else if(action=="dump") {
              dlist.push_back(dump(session,*(*it)));
	      //dlist.sort()   
            }
          }
        }}
       {std::vector< std::pair<double,std::string> >::iterator it;
        for(it=dlist.begin();it!=dlist.end();it++) {
           sorted_list.push_back(*it);
        }}

      }

    } else {
      Lib::Out out(session.printer());
      out << "MC_Viewer_ok :"
          << " no decay found for descriptor " << decaystr
          << Lib::endl;
    }

  } else {
    std::vector<LHCb::MCParticle*>::iterator it;
    for(it=mc->begin();it!=mc->end();it++) {
      if(filter(session,*(*it),kind,stable,energy,ovx)) {
        if(action=="visualize") {
          if(scolor=="default") set_color(session,*(*it),tscale);
          KO_visualize<LHCb::MCParticle>(*soCnvSvc,*(*it));
        } else if(action=="dump") {
          //sorted_list.push_back(dump(d))
        }
      }
    }
    //sorted_list.sort()   
  }

  if(action == "printTree") {
    std::vector<LHCb::MCParticle*>::iterator it;
    for(it=decaylist.begin();it!=decaylist.end();it++) {
      debugTool->printTree(*it,maxDepth);
    }
  }

  if(action == "dump") {
    Lib::Out out(session.printer());
    std::vector< std::pair<double,std::string> >::iterator it;
    for(it=sorted_list.begin();it!=sorted_list.end();it++) {
      out << (*it).second << Lib::endl;
    }
  }

  if((action ==  "visualize") && (azoom != "no") && (decaylist.size()>0) ) {
    aUI.executeScript("DLD","Panoramix layout_rulers");
    SoPage* soPage = ui_SoPage(aUI);
    if(!soPage) return;
    SoRegion* soRegion = ui_SoRegion(aUI);
    if(!soRegion) return;

    Gaudi::XYZPoint oVx = decaylist[0]->originVertex()->position();
    Gaudi::XYZPoint eVx = decaylist[0]->endVertices()[0]->position();
    double height = 5. * 
      ::sqrt( (eVx.x()-oVx.x())*(eVx.x()-oVx.x()) + (eVx.y()-oVx.y())*(eVx.y()-oVx.y()));

    SoCamera* soCamera = soRegion->getCamera();

    if( azoom == "xy-proj" ) {
      soCamera->position.setValue(SbVec3f(oVx.x(),oVx.y(),oVx.z()));
      if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
        ((SoOrthographicCamera*)soCamera)->height.setValue(height);
      }
      soCamera->orientation.setValue(SbRotation(SbVec3f(0,1,0),3.14));
      soCamera->nearDistance.setValue(-5000);
      soCamera->farDistance.setValue(5000);
      if(soRegion->isOfType(SoDisplayRegion::getClassTypeId())) {
        SoDisplayRegion* displayRegion = (SoDisplayRegion*)soRegion;
        SoTransform* tsf = displayRegion->getTransform();
        tsf->scaleFactor.setValue(SbVec3f(1,1,1));  
      }
    }
    if( azoom == "zx-proj" ) {
      double height = fabs(eVx.z()-oVx.z()) * 5.;

      soCamera->position.setValue(SbVec3f(oVx.x(),oVx.y(),oVx.z()+height*0.7));
      if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
        ((SoOrthographicCamera*)soCamera)->height.setValue(height);
      }
      soCamera->orientation.setValue(SbRotation
        (SbVec3f(-0.57735, -0.57735, -0.57735),2.0943));
      soCamera->nearDistance.setValue(-5000);
      soCamera->farDistance.setValue(5000);
      if(soRegion->isOfType(SoDisplayRegion::getClassTypeId())) {
        SoDisplayRegion* displayRegion = (SoDisplayRegion*)soRegion;
        SoTransform* tsf = displayRegion->getTransform();
        tsf->scaleFactor.setValue(SbVec3f(5,5,1));  
      }
    }
  }

}

}
