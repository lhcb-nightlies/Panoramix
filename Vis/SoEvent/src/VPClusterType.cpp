// this :
#include "VPClusterType.h"
#include "SoEventSvc.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Variable.h"
#include "Lib/Out.h"

// Gaudi :
#include "GaudiKernel/IDataProviderSvc.h"       
#include "GaudiKernel/SmartDataPtr.h"           

#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ISoConversionSvc.h"

#include <Linker/LinkedTo.h>
// Event model :
#include <Event/MCParticle.h>
#include "VPDet/DeVP.h"


//////////////////////////////////////////////////////////////////////////////
VPClusterType::VPClusterType(
                                 IUserInterfaceSvc* aUISvc
                                 ,ISoConversionSvc* aSoCnvSvc
                                 ,IDataProviderSvc* aDataProviderSvc
                                 ,IDataProviderSvc* aDetectorDataSvc
                                 )
  :SoEvent::Type<LHCb::VPCluster>(
                                    LHCb::VPClusters::classID(),
                                    "VPCluster",
                                    LHCb::VPClusterLocation::Default,
                                    aUISvc,aSoCnvSvc,aDataProviderSvc)
  ,fDetectorDataSvc(aDetectorDataSvc)
{
  addProperty("key",Lib::Property::INTEGER);
  addProperty("station", Lib::Property::INTEGER);
  addProperty("sensor",  Lib::Property::INTEGER);
  addProperty("chip",    Lib::Property::INTEGER);
  addProperty("column",  Lib::Property::INTEGER);
  addProperty("row",     Lib::Property::INTEGER);
  addProperty("address", Lib::Property::POINTER);
}
//////////////////////////////////////////////////////////////////////////////
DeVP * VPClusterType::deVP() const
{
  static bool alreadyTried = false;
  static DeVP * fVP(NULL);
  if ( !fVP && fDetectorDataSvc && !alreadyTried )
  {
    alreadyTried = true;
    fVP = (DeVP*)SmartDataPtr<DeVP>(fDetectorDataSvc,
                                          DeVPLocation::Default );
    if( fVP==0 )
    {
      Lib::Out out(printer());
      out << "ERROR : VPClusterType : "
          << "Unable to retrieve VP detector element at " << DeVPLocation::Default
          << Lib::endl;
    }
  }
  return fVP;
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable VPClusterType::value(
 Lib::Identifier aIdentifier
,const std::string& aName
,void*
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::VPCluster* obj = (LHCb::VPCluster*)aIdentifier;
  LHCb::VPChannelID chid = obj->channelID();
  if(aName=="address") {
    return Lib::Variable(printer(),(void*)obj);
  } else if(aName=="key") {
    return Lib::Variable(printer(),(int) (obj->key() ) );
  } else if(aName=="station") {
    return Lib::Variable(printer(),int(chid.station()) );
  } else if(aName=="sensor") {
    return Lib::Variable(printer(),int(chid.sensor()) );
  } else if(aName=="chip") {
    return Lib::Variable(printer(),int(chid.chip()) );
  } else if(aName=="column") {
    return Lib::Variable(printer(),int(chid.col()) );
  } else if(aName=="row") {
    return Lib::Variable(printer(),int(chid.row()) );
  } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
void VPClusterType::visualize(
 Lib::Identifier aIdentifier
,void* aData
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{

  if(!aIdentifier) return;
  if(!fUISvc) return;
  if(!fUISvc->session()) return;
  std::string value;
  fUISvc->session()->parameterValue("modeling.what",value);
  if(value=="MCParticle") {

      LHCb::VPCluster* object = (LHCb::VPCluster*)aIdentifier;
      visualizeMCParticle(*object);

  } else {

      this->SoEvent::Type<LHCb::VPCluster>::visualize(aIdentifier,aData);

  }
}
//////////////////////////////////////////////////////////////////////////////
void VPClusterType::visualizeMCParticle(
    LHCb::VPCluster& aVPCluster
)
//////////////////////////////////////////////////////////////////////////////

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
     LinkedTo<LHCb::MCParticle,LHCb::VPCluster> VPClusterLink (fDataProviderSvc,0,LHCb::VPClusterLocation::Default);
     LHCb::MCParticle * part  ;  
     part = VPClusterLink.first(&aVPCluster);
     while ( NULL != part) {
        LHCb::MCParticles* objs = new LHCb::MCParticles;
        objs->add(part);
        IOpaqueAddress* addr = 0;
        StatusCode sc = fSoCnvSvc->createRep(objs, addr);
        if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,objs);
        std::string value;
        fUISvc->session()->parameterValue("DEBUG",value);
        if (value=="True"){
         Lib::Out out(printer());
         out <<  "INFO : VPCluster to MCParticle: "
             <<  &aVPCluster   << "  " <<  part    << Lib::endl;
        }
        objs->remove(part);
        delete objs;
        part = VPClusterLink.next();
    }
}

