// this :
#include "VertexBaseType.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Variable.h"
#include "Lib/Out.h"

#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ISoConversionSvc.h"

//////////////////////////////////////////////////////////////////////////////
VertexBaseType::VertexBaseType(
 IUserInterfaceSvc* aUISvc
,ISoConversionSvc* aSoCnvSvc
,IDataProviderSvc* aDataProviderSvc
)
:SoEvent::Type<LHCb::VertexBase>(
  LHCb::VertexBases::classID(),
  "VertexBase",
  "", //No location. Set in iterator method.
  aUISvc,aSoCnvSvc,aDataProviderSvc)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty("key",Lib::Property::INTEGER);
  addProperty("x",Lib::Property::DOUBLE);
  addProperty("y",Lib::Property::DOUBLE);
  addProperty("z",Lib::Property::DOUBLE);
  addProperty("chi2",Lib::Property::DOUBLE);
  addProperty("address",Lib::Property::POINTER);
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable VertexBaseType::value(
 Lib::Identifier aIdentifier
,const std::string& aName
,void*
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::VertexBase* obj = (LHCb::VertexBase*)aIdentifier;
  
   if(aName=="address") {
    return Lib::Variable(printer(),(void*)obj);
 } else if(aName=="key") {
    return Lib::Variable(printer(),obj->key());
 } else if(aName=="x") {
    return Lib::Variable(printer(),obj->position().x() );
 } else if(aName=="y") {
    return Lib::Variable(printer(),obj->position().y() );
 } else if(aName=="z") {
    return Lib::Variable(printer(),obj->position().z() );
 } else if(aName=="chi2") {
    return Lib::Variable(printer(),obj->chi2PerDoF() );
 } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
Lib::IIterator* VertexBaseType::iterator(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  setLocationFromSession();
  return SoEvent::Type<LHCb::VertexBase>::iterator();
}
