#ifndef SoEvent_SoSTClusterCnv_h
#define SoEvent_SoSTClusterCnv_h

#include "SoEventConverter.h"

template <class T> class CnvFactory;

class SoSTClusterCnv : public SoEventConverter {
  //friend class CnvFactory<SoSTClusterCnv>;
public:
  SoSTClusterCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
