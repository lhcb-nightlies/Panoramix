// To fix clashes between Gaudi and Windows :
#include "OnXSvc/Win32.h"

// this :
#include "SoSTClusterCnv.h"

// Inventor :
#include "Inventor/nodes/SoSeparator.h"
#include "Inventor/nodes/SoLightModel.h"
#include "Inventor/nodes/SoDrawStyle.h"
#include "Inventor/nodes/SoCoordinate3.h"
#include "Inventor/nodes/SoTransform.h"
#include "Inventor/nodes/SoIndexedLineSet.h"

// HEPVis :
#include "HEPVis/nodes/SoSceneGraph.h"
#include "HEPVis/nodes/SoHighlightMaterial.h"
#include "HEPVis/misc/SoStyleCache.h"

// Lib :
#include "Lib/smanip.h"
#include "Lib/Interfaces/ISession.h"

// Gaudi :
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/MsgStream.h"
#include "Kernel/IParticlePropertySvc.h"
#include "GaudiKernel/CnvFactory.h"
#include "Kernel/ParticleProperty.h"
#include "GaudiKernel/IToolSvc.h"
// LHCb :
#include "Event/STCluster.h"
#include "STDet/DeSTDetector.h"
#include "STDet/DeSTSector.h"
#include "Kernel/Trajectory.h"

#include "TrackInterfaces/ISTClusterPosition.h"
#include "Event/STMeasurement.h"
// OnXSvc :
#include "OnXSvc/Filter.h"
#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ClassID.h"
#include "OnXSvc/Helpers.h"

DECLARE_CONVERTER_FACTORY(SoSTClusterCnv)

//////////////////////////////////////////////////////////////////////////////
SoSTClusterCnv::SoSTClusterCnv(
 ISvcLocator* aSvcLoc
) 
:SoEventConverter(aSvcLoc,SoSTClusterCnv::classID()) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoSTClusterCnv::createRep(
 DataObject* aObject
,IOpaqueAddress*& aAddr
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log(msgSvc(), "SoSTClusterCnv");
  log << MSG::INFO << "STCluster createReps" << endmsg;

  if(!fUISvc) {
    log << MSG::INFO << " UI service not found" << endmsg;
    return StatusCode::SUCCESS; 
  }

  if(!fParticlePropertySvc) {
    log << MSG::INFO << " ParticleProperty service not found" << endmsg;
    return StatusCode::SUCCESS; 
  }

  ISession* session = fUISvc->session();
  if(!session) {
    log << MSG::INFO << " can't get ISession." << endmsg;
    return StatusCode::FAILURE; 
  }

  SoRegion* region = 0;
  if(aAddr) {
    // Optimization.
    // If having a not null aAddr, we expect a SoRegion.
    // See SoEvent/Type.h/SoEvent::Type<>::beginVisualize.
    region = (SoRegion*)aAddr; 
  } else {
    region = fUISvc->currentSoRegion();
  }
  if(!region) {
    log << MSG::INFO << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE; 
  }

  if(!aObject) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE; 
  }

  LHCb::STClusters* clusters = dynamic_cast<LHCb::STClusters*>(aObject);
  if(!clusters) {
    log << MSG::INFO << " bad object type." << endmsg;
    return StatusCode::FAILURE; 
  }

  bool deleteVector = false;
  // Filter :
  Filter<LHCb::STCluster> filter(*fUISvc,log);
  const std::string& cuts = fUISvc->cuts();
  if(cuts!="") {
    log << MSG::INFO << " cuts \"" << cuts << "\"" << endmsg;
    clusters = filter.collect(*clusters,"STCluster",cuts);
    if(!clusters) return StatusCode::SUCCESS; 
    //filter.dump(*clusters,"STCluster");
    deleteVector = true;;
  }

  if(!clusters->size()) {
    log << MSG::INFO << " collection is empty." << endmsg;
    return StatusCode::SUCCESS; 
  }

  // get geometry

  DeSTDetector* itracker=0; 
  DeSTDetector* ttracker=0; 
  DeSTDetector* utracker=0; 
  IToolSvc* toolSvc = 0;
  StatusCode sc = service( "ToolSvc", toolSvc, true );
  if( sc.isFailure() ) {
    log << MSG::FATAL << "Unable to retrieve ToolSvc " << endmsg;
  }

  // get position tool
  ISTClusterPosition* ttPositionTool;
  ISTClusterPosition* itPositionTool;
  ISTClusterPosition* utPositionTool;
 
  utracker=SmartDataPtr<DeSTDetector>(fDetectorDataSvc,DeSTDetLocation::location("UT"));
  itracker=SmartDataPtr<DeSTDetector>(fDetectorDataSvc,DeSTDetLocation::location("IT"));
  ttracker=SmartDataPtr<DeSTDetector>(fDetectorDataSvc,DeSTDetLocation::location("TT"));
  if (!utracker){
   sc = toolSvc->retrieveTool("STOfflinePosition/ITClusterPosition", itPositionTool);
   if ( !itracker ) {
    log << MSG::ERROR << "Unable to retrieve IT detector element"
        << " from xml." << endmsg;
    return StatusCode::FAILURE;
   }
   if (sc.isFailure()){
    log << MSG::FATAL << "Unable to retrieve ITPosition Tool " << endmsg; 
    return sc;
   }
   sc = toolSvc->retrieveTool("STOfflinePosition", ttPositionTool);
   if ( !ttracker ) {
    log << MSG::ERROR << "Unable to retrieve TT detector element"
        << " from xml." << endmsg;
    return StatusCode::FAILURE;
   }
   if (sc.isFailure()){
    log << MSG::FATAL << "Unable to retrieve TTPosition Tool " << endmsg; 
    return sc;
   }
  }else{
   sc = toolSvc->retrieveTool("STOfflinePosition/UTClusterPosition", utPositionTool);
   if (sc.isFailure()){
    log << MSG::FATAL << "Unable to retrieve UTPosition Tool " << endmsg; 
    return sc;
   } 
  }

  // Representation attributes :
  // Get color (default is grey (valid on black or white background) ):
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 0.0;
  std::string value;
  if(session->parameterValue("modeling.color",value))
    Lib::smanip::torgb(value,r,g,b);
  if(session->parameterValue("modeling.highlightColor",value))
    Lib::smanip::torgb(value,hr,hg,hb);
  double lineWidth = 0;
  if(session->parameterValue("modeling.lineWidth",value))
    if(!Lib::smanip::todouble(value,lineWidth)) lineWidth = 0;

  SoStyleCache* styleCache = region->styleCache();
  SoLightModel* lightModel = styleCache->getLightModelBaseColor();
  SoDrawStyle* drawStyle = 
    styleCache->getLineStyle(SbLinePattern_solid,float(lineWidth));
  SoMaterial* highlightMaterial = 
    styleCache->getHighlightMaterial(float(r),float(g),float(b),
                                     float(hr),float(hg),float(hb),0,TRUE);

  SoSeparator* separator = new SoSeparator;

  SoCoordinate3* coordinate3 = new SoCoordinate3;
  separator->addChild(coordinate3);
  int icoord = 0;
  int32_t coordIndex[3];
  SbBool empty = TRUE;

  // One scene graph per STCluster :

  LHCb::STClusters::iterator it;
  for(it = clusters->begin(); it != clusters->end(); it++) {
    LHCb::STCluster* cluster  = (*it);
    LHCb::STChannelID chan = cluster->channelID();

    // Build name :
    char sid[64];
    ::sprintf(sid,"STCluster/0x%lx",(unsigned long)cluster);

    SoSceneGraph* sep = new SoSceneGraph;
    sep->setString(sid);

    separator->addChild(sep);
    
    sep->addChild(highlightMaterial);
  
    sep->addChild(lightModel);
    sep->addChild(drawStyle);
  
    // find the sector
    // only online info
    //    DeSTSector* sector = tracker->findSector(chan);  
    //    std::auto_ptr<LHCb::Trajectory> traj_online  = sector->trajectory(chan,cluster->interStripFraction());

    // another possibility
    DeSTDetector* tdet;
    ISTClusterPosition* icluspos;
    const LHCb::STCluster* arg1 = dynamic_cast<const LHCb::STCluster*>(cluster);
    if  (arg1->isUT() ){
     tdet = utracker; 
     icluspos = utPositionTool ;
     log << MSG::INFO << "STCluster : UT" << endmsg;
    }else{
     tdet = arg1->isIT()?itracker:ttracker; 
     log << MSG::INFO << "STCluster : "<< arg1->isTT() << endmsg;
     icluspos = arg1->isIT()?itPositionTool:ttPositionTool ;
    }
    log << MSG::INFO << "STCluster clusterposition " << arg1  << "  " << tdet << "  " << icluspos << endmsg;
    LHCb::STMeasurement meas = LHCb::STMeasurement( *arg1, *tdet, *icluspos );
    log << MSG::INFO << "STCluster measurement done " << endmsg;
    const LHCb::Trajectory& traj  = meas.trajectory();

    // endpoints of trajectory
    Gaudi::XYZPoint start = traj.beginPoint();
    Gaudi::XYZPoint stop = traj.endPoint();

    int pointn = 2;
    SbVec3f points[2];
    points[0].setValue(float(start.x()), float(start.y()), float(start.z()));
    points[1].setValue(float(stop.x()), float(stop.y()), float(stop.z()));
    coordIndex[0] = icoord + 0;
    coordIndex[1] = icoord + 1;
    coordIndex[2] = SO_END_LINE_INDEX;

    coordinate3->point.setValues(icoord,pointn,points);
    icoord += pointn;

    SoIndexedLineSet* lineSet = new SoIndexedLineSet;
    lineSet->coordIndex.setValues(0,pointn+1,coordIndex);
    sep->addChild(lineSet);

    empty = FALSE;
  }
  
  if(deleteVector) {
    // We have first to empty the vector :
    while(clusters->size()) {
      clusters->remove(*(clusters->begin()));
    }
    // Then we can delete it :
    delete clusters;
  }

  if(empty==TRUE) {
    separator->unref();
  } else {
    //  Send scene graph to the viewing region 
    // (in the "dynamic" sub-scene graph) :
    region_addToDynamicScene(*region,separator);
  }

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoSTClusterCnv::classID(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{ 
  return LHCb::STClusters::classID();
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoSTClusterCnv::storageType(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{ 
  return So_TechnologyType; 
}
