// this :
#include "RecVertexType.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Variable.h"
#include "Lib/Out.h"

#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ISoConversionSvc.h"

//////////////////////////////////////////////////////////////////////////////
RecVertexType::RecVertexType(
 IUserInterfaceSvc* aUISvc
,ISoConversionSvc* aSoCnvSvc
,IDataProviderSvc* aDataProviderSvc
,LHCb::IParticlePropertySvc* aParticlePropertySvc
)
:SoEvent::Type<LHCb::RecVertex>(
  LHCb::RecVertices::classID(),
  "RecVertex",
  "", //No location. Set in iterator method.
  aUISvc,aSoCnvSvc,aDataProviderSvc)
,fParticlePropertySvc(aParticlePropertySvc)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty("key",Lib::Property::INTEGER);
  addProperty("ntracks",Lib::Property::INTEGER);
  addProperty("x",Lib::Property::DOUBLE);
  addProperty("y",Lib::Property::DOUBLE);
  addProperty("z",Lib::Property::DOUBLE);
  addProperty("chi2",Lib::Property::DOUBLE);
  addProperty("address",Lib::Property::POINTER);
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable RecVertexType::value(
 Lib::Identifier aIdentifier
,const std::string& aName
,void*
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::RecVertex* obj = (LHCb::RecVertex*)aIdentifier;
  
   if(aName=="address") {
    return Lib::Variable(printer(),(void*)obj);
 } else if(aName=="key") {
    return Lib::Variable(printer(),obj->key());
 } else if(aName=="ntracks") {
    int temp = (obj->tracks()).size();
    return Lib::Variable(printer(),temp );
 } else if(aName=="x") {
    return Lib::Variable(printer(),obj->position().x() );
 } else if(aName=="y") {
    return Lib::Variable(printer(),obj->position().y() );
 } else if(aName=="z") {
    return Lib::Variable(printer(),obj->position().z() );
 } else if(aName=="chi2") {
    return Lib::Variable(printer(),obj->chi2PerDoF() );
 } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
Lib::IIterator* RecVertexType::iterator(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  setLocationFromSession();
  return SoEvent::Type<LHCb::RecVertex>::iterator();
}
