#ifndef SoEvent_SoOTClusterCnv_h
#define SoEvent_SoOTClusterCnv_h

#include "SoEventConverter.h"

template <class T> class CnvFactory;

class SoOTClusterCnv : public SoEventConverter {
  //friend class CnvFactory<SoOTClusterCnv>;
public:
  SoOTClusterCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
