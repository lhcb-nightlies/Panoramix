// this :
#include "MuonCoordType.h"

// Lib :
#include "Lib/Interfaces/IIterator.h"
#include "Lib/Interfaces/ISession.h"
#include "Lib/Variable.h"

#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ISoConversionSvc.h"

#include <Linker/LinkedTo.h>
// Event model :
#include <Event/MCParticle.h>

//////////////////////////////////////////////////////////////////////////////
MuonCoordType::MuonCoordType(
 IUserInterfaceSvc* aUISvc
,ISoConversionSvc* aSoCnvSvc
,IDataProviderSvc* aDataProviderSvc
)
:SoEvent::Type<LHCb::MuonCoord>(
  LHCb::MuonCoords::classID(),
  "MuonCoord",
  LHCb::MuonCoordLocation::MuonCoords,
  aUISvc,aSoCnvSvc,aDataProviderSvc)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty("key",Lib::Property::INTEGER);
  addProperty("station",Lib::Property::INTEGER);
  addProperty("region",Lib::Property::INTEGER);
  addProperty("quarter",Lib::Property::INTEGER);
  addProperty("address",Lib::Property::POINTER);
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable MuonCoordType::value(
 Lib::Identifier aIdentifier
,const std::string& aName
,void*
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::MuonCoord* obj = (LHCb::MuonCoord*)aIdentifier;
  if(aName=="address") {
    return Lib::Variable(printer(),(void*)obj);
  } else if(aName=="station") {
     LHCb::MuonTileID tile = obj->key();
    return Lib::Variable(printer(),(int)tile.station());
  } else if(aName=="region") {
     LHCb::MuonTileID tile = obj->key();
    return Lib::Variable(printer(),(int)tile.region());
  } else if(aName=="quarter") {
     LHCb::MuonTileID tile = obj->key();
    return Lib::Variable(printer(),(int)tile.quarter());
  } else if(aName=="key") {
    return Lib::Variable(printer(),(int) (obj->key().key() ) );
  } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
void MuonCoordType::visualize(
 Lib::Identifier aIdentifier
,void* aData
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{

  if(!aIdentifier) return;
  if(!fUISvc) return;
  if(!fUISvc->session()) return;
  std::string value;
  fUISvc->session()->parameterValue("modeling.what",value);
  if(value=="MCParticle") {

      LHCb::MuonCoord* object = (LHCb::MuonCoord*)aIdentifier;
      visualizeMCParticle(*object);

  } else {

      this->SoEvent::Type<LHCb::MuonCoord>::visualize(aIdentifier,aData);

  }
}
//////////////////////////////////////////////////////////////////////////////
void MuonCoordType::visualizeMCParticle(
    LHCb::MuonCoord& aMuonCoord
)
//////////////////////////////////////////////////////////////////////////////

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
     LinkedTo<LHCb::MCParticle,LHCb::MuonDigit> MuonCoordLink (fDataProviderSvc,0,LHCb::MuonDigitLocation::MuonDigit);
     const std::vector< LHCb::MuonTileID >   digits = (&aMuonCoord)->LHCb::MuonCoord::digitTile();
     for (std::vector< LHCb::MuonTileID >::const_iterator idigit = digits.begin();idigit != digits.end();idigit++){
      LHCb::MCParticle * part  = MuonCoordLink.first((*idigit));
      while ( NULL != part) {
        LHCb::MCParticles* objs = new LHCb::MCParticles;
        objs->add(part);
        // Convert it :
        IOpaqueAddress* addr = 0;
        StatusCode sc = fSoCnvSvc->createRep(objs, addr);
        if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,objs);
        objs->remove(part);
        delete objs;
        part = MuonCoordLink.next();
      }
     }
}




