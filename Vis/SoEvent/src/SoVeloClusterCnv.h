#ifndef SoEvent_SoVeloClusterCnv_h
#define SoEvent_SoVeloClusterCnv_h

#include "SoEventConverter.h"

template <class T> class CnvFactory;

class VeloPointMaker;
class VeloPoint;
class DeVelo;
class IToolSvc;
class IVeloClusterPosition;

#include "Event/VeloCluster.h"
#include <vector>

class SoVeloClusterCnv : public SoEventConverter {
  //friend class CnvFactory<SoVeloClusterCnv>;
public:
  StatusCode initialize();
  SoVeloClusterCnv(ISvcLocator*);
  virtual ~SoVeloClusterCnv();
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
private:
  StatusCode clustersPoints(LHCb::VeloClusters*,std::vector< std::vector<VeloPoint*> >&);
private:
  DeVelo* m_velo;
  VeloPointMaker* m_pointMaker;
  IToolSvc* fToolSvc;
  IVeloClusterPosition* fVeloPositionTool;
};

#endif
