#ifndef SoEvent_TrackType_h
#define SoEvent_TrackType_h

// Inheritance :
#include "Type.h"

#include "Event/Track.h" //The data class.
#include "Event/RichRecTrack.h"

// Tool interfaces
#include "MCInterfaces/IRichRecMCTruthTool.h"
#include "TrackInterfaces/IHitExpectation.h"
#include "TrackInterfaces/IVeloExpectation.h"

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IToolSvc;
class ISvcLocator;
class MsgStream;

class TrackType : public SoEvent::Type<LHCb::Track>
{
public: //Lib::IType
  virtual Lib::IIterator* iterator();
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public: //OnX::IType
  virtual void visualize(Lib::Identifier,void*);
public:
  TrackType(IUserInterfaceSvc*,
            ISoConversionSvc*,
            IDataProviderSvc*,
            IToolSvc*,
            MsgStream&);
private:
  void visualizeMeasurements(LHCb::Track&) const; ///< Visualize the associated Measurements
  void visualizeMCParticle(LHCb::Track&) const;   ///< Visualize the associated MCParticle
  void visualizeChargeConjugate(LHCb::Track&) const;   ///< Visualize the associated charge conjugate trajectory
private:
  /// Visualize the associated RICH reconstructed CK rings
  void visualizeRICHRecoRings(LHCb::Track&) const;
  /// Visualize the associated RICH reconstructed radiator segments
  void visualizeRICHRecoSegments(LHCb::Track&) const;
  /// Visualize the associated RICH reconstructed hits (those with a reconstructed photon candidate)
  void visualizeRICHRecoPixels(LHCb::Track&) const;
  /// Visualize the associated RICH rconstructed photon candidates
  void visualizeRICHRecoPhotons(LHCb::Track&) const;
  /// Visualize the pixels that are MC associated to this track
  void visualizeRICHMCPixels(LHCb::Track&) const;
  /// Visualize the MC segments that are associated to this track
  void visualizeRICHMCSegments(LHCb::Track&) const;
  /// Visualize the MC photons that are associated to this track
  void visualizeRICHMCPhotons(LHCb::Track&) const;
  /// Visualize the MC CK rings that are associated to this track
  void visualizeRICHMCCKRings(LHCb::Track&) const;
  /// Access on-demand the RICH MC truth tool
  const Rich::Rec::MC::IMCTruthTool * richMCTruth() const;
  /// Get the RichRecTracks
  LHCb::RichRecTracks * richRecTracks() const;
  /// Get the RichRecPixels
  LHCb::RichRecPixels * richRecPixels() const;
  /// Get the RichRecTrack associated to the given Track
  LHCb::RichRecTrack * richRecTrack( LHCb::Track & aTrack ) const;
  /// Print a warning method if MC associations that require extended RICH MC info are requested
  void richExtendedMCMessage() const;
private:
  /// Draw the data container
  template < typename TYPE >
  inline void drawContainer( TYPE & objs ) const
  {
    if ( !objs.empty() )
    {
      IOpaqueAddress * addr = 0;
      StatusCode sc = fSoCnvSvc->createRep(&objs,addr);
      if (sc.isSuccess()) sc = fSoCnvSvc->fillRepRefs(addr,&objs);
      if ( sc.isFailure() ) error() << "Cannot visualize " 
                                    << System::typeinfoName(typeid(objs)) << endmsg;
      objs.clear();
    }
  }
private:
  /// Access to Message stream
  MsgStream & msgStream() const  { return m_msgStream;                 }
  /// Debug message
  MsgStream & always()    const  { return msgStream() << MSG::ALWAYS;  }
  /// Debug message
  MsgStream & verbose()   const  { return msgStream() << MSG::VERBOSE; }
  /// Debug message
  MsgStream & debug()     const  { return msgStream() << MSG::DEBUG;   }
  /// Info message
  MsgStream & info()      const  { return msgStream() << MSG::INFO;    }
  /// Warning message
  MsgStream & error()     const  { return msgStream() << MSG::WARNING; }
  /// Error message
  MsgStream & warning()   const  { return msgStream() << MSG::ERROR;   }
private:
  IToolSvc* m_toolSvc;     ///< Tool service
  MsgStream & m_msgStream; ///< Message stream object
  /// Use RICH tool to get MC associations for RICH data
  mutable const Rich::Rec::MC::IMCTruthTool * m_truth;
  // rich data locations
  std::string m_hypoRingLoc, m_richTrackLoc, m_richPixelLoc;
};

#endif
