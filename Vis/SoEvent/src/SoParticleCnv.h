#ifndef SoEvent_SoParticleCnv_h
#define SoEvent_SoParticleCnv_h

#include "SoEventConverter.h"

template <class T> class CnvFactory;

class SoParticleCnv : public SoEventConverter {
  //friend class CnvFactory<SoParticleCnv>;
public:
  SoParticleCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
