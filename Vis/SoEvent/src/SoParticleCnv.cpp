// To fix clashes between Gaudi and Windows :
#include "OnXSvc/Win32.h"

// this :
#include "SoParticleCnv.h"

// Inventor :
#include "Inventor/nodes/SoSeparator.h"
#include "Inventor/nodes/SoLightModel.h"
#include "Inventor/nodes/SoDrawStyle.h"
#include "Inventor/nodes/SoCoordinate3.h"
#include "Inventor/nodes/SoLineSet.h"
#include "Inventor/nodes/SoTransform.h"
#include "HEPVis/nodes/SoTextTTF.h"

// HEPVis :
#include "HEPVis/nodes/SoSceneGraph.h"
#include "HEPVis/nodes/SoHighlightMaterial.h"
#include "HEPVis/misc/SoStyleCache.h"

// Lib :
#include "Lib/smanip.h"
#include "Lib/Interfaces/ISession.h"

// Gaudi :
#include "Kernel/IParticlePropertySvc.h"   
#include "Kernel/ParticleProperty.h"
#include "GaudiKernel/MsgStream.h"              
#include "GaudiKernel/CnvFactory.h"

// Tools
#include "GaudiKernel/IToolSvc.h"

// LHCb :
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/Vertex.h"

// SoUtils :
#include "SoUtils/SbProjector.h"

// OnXSvc :
#include "OnXSvc/Filter.h"
#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ClassID.h"
#include "OnXSvc/Helpers.h"

// Phys
#include "Kernel/IParticleTransporter.h"

DECLARE_CONVERTER_FACTORY(SoParticleCnv)

//////////////////////////////////////////////////////////////////////////////
SoParticleCnv::SoParticleCnv(
 ISvcLocator* aSvcLoc
)
:SoEventConverter(aSvcLoc,SoParticleCnv::classID())
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoParticleCnv::createRep(
 DataObject* aObject
,IOpaqueAddress*& aAddr
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log(msgSvc(), "SoParticleCnv");
  log << MSG::INFO << "SoParticleCnv createReps ####" << endmsg;

  if(!fUISvc) {
    log << MSG::INFO << " UI service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  if(!fParticlePropertySvc) {
    log << MSG::INFO << " ParticleProperty service not found" << endmsg;
    return StatusCode::SUCCESS;
  }


  // get extrapolator
  IToolSvc* toolSvc = 0;
  StatusCode sc = service( "ToolSvc", toolSvc, true );
  if( sc.isFailure() ) {
    log << MSG::FATAL << "Unable to retrieve ToolSvc " << endmsg;
    return StatusCode::FAILURE;
  }
  IParticleTransporter* pTransporter = 0;
  sc = toolSvc->retrieveTool("ParticleTransporter",pTransporter);
  if (sc.isFailure()){
    log << MSG::FATAL << "Unable to retrieve ParticleTransporter Tool " << endmsg;
    return StatusCode::FAILURE;
  }

  ISession* session = fUISvc->session();
  if(!session) {
    log << MSG::INFO << " can't get ISession." << endmsg;
    return StatusCode::FAILURE;
  }

  SoRegion* region = 0;
  if(aAddr) {
    // Optimization.
    // If having a not null aAddr, we expect a SoRegion.
    // See SoEvent/Type.h/SoEvent::Type<>::beginVisualize.
    region = (SoRegion*)aAddr; 
  } else {
    region = fUISvc->currentSoRegion();
  }
  if(!region) {
    log << MSG::INFO << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE;
  }

  if(!aObject) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE;
  }

  LHCb::Particles* particles = dynamic_cast<LHCb::Particles*>(aObject);
  if(!particles) {
    log << MSG::INFO << " bad object type." << endmsg;
    return StatusCode::FAILURE;
  }

  bool deleteVector = false;
  // Filter :
  Filter<LHCb::Particle> filter(*fUISvc,log);
  const std::string& cuts = fUISvc->cuts();
  if(cuts!="") {
    log << MSG::INFO << " cuts \"" << cuts << "\"" << endmsg;
    particles = filter.collect(*particles,"Particle",cuts);
    if(!particles) return StatusCode::SUCCESS;
    //filter.dump(*particles,"Particle");
    deleteVector = true;;
  }

  if(!particles->size()) {
    log << MSG::INFO << " collection is empty." << endmsg;
    return StatusCode::SUCCESS;
  }

  // Representation attributes :
  // Get color (default is grey (valid on black or white background) ):
  std::string value;
  double r = 0.5, g = 0.5, b = 0.5;
  if(session->parameterValue("modeling.color",value))
    Lib::smanip::torgb(value,r,g,b);
  double hr = 1.0, hg = 1.0, hb = 0.0;
  if(session->parameterValue("modeling.highlightColor",value))
    Lib::smanip::torgb(value,hr,hg,hb);
  double lineWidth = 0;
  if(session->parameterValue("modeling.lineWidth",value))
    if(!Lib::smanip::todouble(value,lineWidth)) lineWidth = 0;
  // Non linear projections :
  session->parameterValue("modeling.projection",value);
  // user defined range
  bool userTrackRange = false;
  if(session->parameterValue("modeling.userTrackRange",value))
    if(!Lib::smanip::tobool(value,userTrackRange)) userTrackRange = false;
  double userbz = -1000.;
  if(session->parameterValue("modeling.trackStartz",value))
    if(!Lib::smanip::todouble(value,userbz)) userbz = -1000.;
  double userez = 10000.;
  if(session->parameterValue("modeling.trackEndz",value))
    if(!Lib::smanip::todouble(value,userez)) userez = 10000.;

  // draw from reference point to eVX
  bool force = false;
  if(session->parameterValue("modeling.partforce",value)){
   if (value=="true") force = true;
   log << MSG::DEBUG << "modeling.partforce" << value  << endmsg;
  }
  SoUtils::SbProjector projector(value.c_str());
  SoStyleCache* styleCache = region->styleCache();
  SoLightModel* lightModel = styleCache->getLightModelBaseColor();
  SoDrawStyle* drawStyle = 
    styleCache->getLineStyle(SbLinePattern_solid,float(lineWidth));
  SoMaterial* highlightMaterial = 
    styleCache->getHighlightMaterial(float(r),float(g),float(b),
                                     float(hr),float(hg),float(hb),0,TRUE);

  // Representation attributes :
  bool showText = false;
  session->parameterValue("modeling.showText",value);
  Lib::smanip::tobool(value,showText);
  int sizeText = 10; //SoTextTTF default
  session->parameterValue("modeling.sizeText",value);
  if(!Lib::smanip::toint(value,sizeText)) sizeText = 10;
  session->parameterValue("modeling.posText",value);
  std::string posTextAtb = value=="" ? "medium" : value;
  // draw from start to end without margin
  bool startend = false;
  if (session->parameterValue("modeling.partstartend",value)){
   Lib::smanip::tobool(value,startend);
  }
  LHCb::Particles::iterator it;
  for(it = particles->begin(); it != particles->end(); it++) {
      const LHCb::Particle* particle  = (*it);
      //if(!(*it)->pointOnTrack()) continue;

      // Build name :
      char sid[64];
      ::sprintf(sid,"Particle/0x%lx",(unsigned long)particle);

      SoSceneGraph* separator = new SoSceneGraph;
      separator->setString(sid);

      // Material :
      separator->addChild(highlightMaterial);

      const int nStep = 50;
      double zMax = 12000.;
      
      if ( particle->particleID().abspid() == 13 ) {
          zMax = 22000.;
         }
      if ( userTrackRange ) { zMax = userez;} 
      SbVec3f* points = new SbVec3f[nStep];
      double zMargin = 100.;
      if (startend)  zMargin = 0.;
      double zMin = particle->referencePoint().z() - zMargin ;
      if ( userTrackRange ) { zMin = userbz;} 
      const LHCb::Vertex* decayVertex = particle->endVertex();
      int step = 0;
      if ( decayVertex != 0 and !userTrackRange ) {
       const Gaudi::XYZPoint& end = decayVertex->position();
       zMax  = end.z() + zMargin ; 
       if (force){
        const Gaudi::XYZPoint& tmppos = particle->referencePoint();
        points[0].setValue(float(tmppos.x()), float(tmppos.y()), float(tmppos.z()));
        points[1].setValue(float(end.x()),    float(end.y()),    float(end.z()));
        log << MSG::DEBUG << " soparticle with force  " << tmppos.z() << "  "<< end.z() << endmsg;
        }
      }
    if (force && decayVertex != 0 ){
       step = 2;
    } else {  
     double diff = (zMax-zMin)/(double)(nStep-1);
     for ( step = 0 ; step < nStep ; step++ ) {
      const double z = zMin + diff * (double)step;
      if ( pTransporter == 0 ) break;
      LHCb::Particle transParticle;
      StatusCode sctrans = pTransporter->transport(particle,z,transParticle);
      if ( !sctrans.isSuccess() ) {
       log << MSG::INFO << " trans failed" << endmsg;
       break;
      }
      const Gaudi::XYZPoint& tmppos = transParticle.referencePoint();
      points[step].setValue(float(tmppos.x()), float(tmppos.y()), float(tmppos.z()));
      if (step == 0 and !userTrackRange ) { 
        const Gaudi::XYZPoint& tmppos= particle->referencePoint();
        points[step].setValue(float(tmppos.x()), float(tmppos.y()), float(tmppos.z()));
      }
      float xx,yy,zz;
      points[step].getValue(xx,yy,zz);
      log << MSG::INFO 
        << " trans seting point at z=" << z
        << ": x:" << xx
        << " y:" << yy << " z:" << zz << std::endl;
      }
      log << MSG::INFO << " end trans loop" << endmsg;
     }  
      log << MSG::DEBUG << " soparticle with force ? " << force << endmsg;
      separator->addChild(lightModel);
      separator->addChild(drawStyle);

      SoCoordinate3* coordinate3 = new SoCoordinate3;
      projector.project(step,points);
      coordinate3->point.setValues(0,step,points);
      separator->addChild(coordinate3);

      SoLineSet* lineSet = new SoLineSet;
      int32_t vertices;
      vertices = step;
      lineSet->numVertices.setValues(0,1,&vertices);
      separator->addChild(lineSet);

      SbVec3f textPoint;
      if(posTextAtb=="medium") {
         textPoint = (points[0] + points[1])/2;
      } else {
         double posText;
         if(Lib::smanip::todouble(posTextAtb,posText)) {
            SbVec3f direction = points[1]-points[0];
            direction.normalize();
            textPoint = points[0] + float(posText) * direction;
         } else {
           textPoint = (points[0] + points[1])/2;
         }
      }
      delete [] points;

      // Text :
      if(showText) {
       SoSeparator* sepText = new SoSeparator;
       separator->addChild(sepText);

       SoTransform* tsf = new SoTransform;
       tsf->translation.setValue(textPoint);
       sepText->addChild(tsf);

       SoTextTTF* text = new SoTextTTF();
       text->size.setValue(sizeText);

       const LHCb::ParticleProperty* pp = fParticlePropertySvc->find(particle->particleID());
       std::string mcname = pp ? pp->particle() : "nil";
       text->string.set1Value(0,mcname.c_str());
       sepText->addChild(text);
      }
      
      //  Send scene graph to the viewing region
      // (in the "dynamic" sub-scene graph) :
      region_addToDynamicScene(*region,separator);

    }//end loop

  if(deleteVector) {
    // We have first to empty the vector :
    while(particles->size()) {
      particles->remove(*(particles->begin()));
    }
    // Then we can delete it :
    delete particles;
  }

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoParticleCnv::classID(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return LHCb::Particles::classID();
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoParticleCnv::storageType(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return So_TechnologyType;
}
