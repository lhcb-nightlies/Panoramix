#ifndef SoEvent_VertexType_h
#define SoEvent_VertexType_h

// Inheritance :
#include "Type.h"
#include "Kernel/IParticlePropertySvc.h"

#include "Event/Vertex.h" // The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IParticlePropertySvc;

class VertexType : public SoEvent::Type<LHCb::Vertex> {
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public:
  VertexType(IUserInterfaceSvc*,
         ISoConversionSvc*,
         IDataProviderSvc*,
         LHCb::IParticlePropertySvc*);
  Lib::IIterator* iterator();
private:
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
};

#endif
