#ifndef SoEvent_FTClusterType_h
#define SoEvent_FTClusterType_h

// Inheritance :
#include "Type.h"

//#include "Event/FTRawCluster.h" //The data class.
#include "Event/FTLiteCluster.h" //The data class.
#include "Event/FTCluster.h" //The data class.
typedef FastClusterContainer<LHCb::FTLiteCluster,int> FTLiteClusters;

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class DeFTDetector;

class FTClusterType : public SoEvent::Type<LHCb::FTCluster> {
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public: //OnX::IType
  virtual void visualize(Lib::Identifier,void*);
public:
  FTClusterType(IUserInterfaceSvc*,ISoConversionSvc*,IDataProviderSvc*,
          IDataProviderSvc*);
private:
  DeFTDetector * deFTDetector() const;
private:
  IDataProviderSvc* fDetectorDataSvc;
  void visualizeMCParticle(LHCb::FTCluster&);
};

#endif
