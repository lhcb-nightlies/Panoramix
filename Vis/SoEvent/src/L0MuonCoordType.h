// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.2  2010/02/18 07:17:54  truf
// improve display of backward tracks
//
// Revision 1.1  2008/09/24 15:00:34  truf
// *** empty log message ***
//
//
// ============================================================================
#ifndef SoEvent_L0MuonCoordType_h
#define SoEvent_L0MuonCoordType_h

// Inheritance :
#include "Type.h"

#include "Event/L0MuonCandidate.h" //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;

class L0MuonCoordType : public SoEvent::Type<LHCb::L0MuonCandidate> {
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public: //OnX::IType
  virtual void visualize(Lib::Identifier,void*);
public:
  L0MuonCoordType(IUserInterfaceSvc*,ISoConversionSvc*,IDataProviderSvc*);
private:
  void visualizeMCParticle(LHCb::L0MuonCandidate&);
};

#endif
