// $Id: VeloCoord.h,v 1.2 2006-05-09 18:43:24 mtobin Exp $
#ifndef VELOCOORD_H 
#define VELOCOORD_H 1

// Include files
#include "Event/VeloCluster.h"

using namespace LHCb;
/** @class VeloCoord VeloCoord.h
 *  Stores the measurement of one cluster in a sensor
 *
 *  @author Olivier Callot
 *  @date   17/04/2002
 */
class VeloCoord {
public:
  /// Standard constructor
  VeloCoord( ) : m_strip(0), m_sigma(0), m_clu(0) { };
  
  VeloCoord( double strip, double sigma, VeloCluster* clu ) {
    m_strip = strip;
    m_sigma = sigma;
    m_clu   = clu;
    m_first = 0;
    m_last  = 0;
    
  }; 

  ~VeloCoord( ) {}; ///< Destructor

  double strip()  {  return m_strip; };
  double sigma()  {  return m_sigma; };
  VeloCluster* cluster()  {  return m_clu; };

  void setFirst( int first ) { m_first = first; };
  void setLast(  int last  ) { m_last  = last;  };
  void setZ(     double z  ) { m_z     = z;     };
  void setR(     double r  ) { m_r     = r;     };
  void setZone(  int zone  ) { m_zone  = zone;  };
  int  first()               { return m_first;  };
  int  last()                { return m_last;   };
  double z()                 { return m_z;      };
  double r()                 { return m_r;      };
  int  zone()                { return m_zone;   };

protected:

private:
  double m_strip;
  double m_sigma;
  VeloCluster* m_clu;
  int    m_first;
  int    m_last;
  double m_z;
  double m_r;
  int    m_zone;
};

typedef std::vector<VeloCoord> VeloCoords;

#endif // VELOCOORD_H
