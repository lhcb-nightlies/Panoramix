// this :
#include "SoEventSvc.h"

// Gaudi :
#include "GaudiKernel/SvcFactory.h"

#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IToolSvc.h"

// OnXSvc :
#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ISoConversionSvc.h"

// SoEvent :
#include "EventType.h"
#include "MCParticleType.h"
#include "MCVertexType.h"
#include "MCHitType.h"
#include "VeloClusterType.h"
#include "VPClusterType.h"
#include "FTClusterType.h"
#include "STClusterType.h"
#include "OTTimeType.h"
#include "MuonCoordType.h"
#include "L0MuonCoordType.h"
#include "ParticleType.h"
#include "TrackType.h"
#include "RecVertexType.h"
#include "VertexBaseType.h"
#include "VertexType.h"

DECLARE_SERVICE_FACTORY( SoEventSvc )

//////////////////////////////////////////////////////////////////////////////
SoEventSvc::SoEventSvc( const std::string& aName
                        ,ISvcLocator* aSvcLoc )
  :Service(aName,aSvcLoc)
  ,m_uiSvc(0)
  ,m_soConSvc(0)
  ,m_evtDataSvc(0)
  ,m_detDataSvc(0)
  ,m_pPropSvc(0)
  ,m_toolSvc(0)
  ,m_msgStream(0) { }
//////////////////////////////////////////////////////////////////////////////
SoEventSvc::~SoEventSvc( )
{
  delete m_msgStream;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoEventSvc::initialize( )
{
  StatusCode sc = Service::initialize();
  if ( sc.isFailure() ) return sc;

  debug() << "Initialize" << endmsg;

  setProperties();

  sc = ( getService( "OnXSvc",              m_uiSvc      ) &&
         getService( "SoConversionSvc",     m_soConSvc   ) &&
         getService( "EventDataSvc",        m_evtDataSvc ) &&
         getService( "LHCb::ParticlePropertySvc", m_pPropSvc   ) &&
         getService( "DetectorDataSvc",     m_detDataSvc ) &&
         getService( "ToolSvc",             m_toolSvc    ) );
  if ( sc.isFailure() ) return sc;

  if (m_evtDataSvc) 
  {
    m_uiSvc->addType(new EventType(m_uiSvc,m_evtDataSvc));
    if (m_soConSvc) 
    {
      // MC :
      if(m_pPropSvc)
      {
        m_uiSvc->addType(new MCParticleType(m_uiSvc,m_soConSvc,m_evtDataSvc,m_pPropSvc));
        m_uiSvc->addType(new MCVertexType  (m_uiSvc,m_soConSvc,m_evtDataSvc,m_pPropSvc));
        m_uiSvc->addType(new MCHitType     (m_uiSvc,m_soConSvc,m_evtDataSvc,m_pPropSvc,"Velo"));
        m_uiSvc->addType(new MCHitType     (m_uiSvc,m_soConSvc,m_evtDataSvc,m_pPropSvc,"OT"));
      }

      // Rec :
      m_uiSvc->addType(new TrackType       (m_uiSvc,m_soConSvc,m_evtDataSvc,m_toolSvc,msgStream()));
      m_uiSvc->addType(new STClusterType   (m_uiSvc,m_soConSvc,m_evtDataSvc));
      m_uiSvc->addType(new OTTimeType      (m_uiSvc,m_soConSvc,m_evtDataSvc));
      m_uiSvc->addType(new VeloClusterType (m_uiSvc,m_soConSvc,m_evtDataSvc,m_detDataSvc));
      m_uiSvc->addType(new VPClusterType   (m_uiSvc,m_soConSvc,m_evtDataSvc,m_detDataSvc));
      m_uiSvc->addType(new FTClusterType   (m_uiSvc,m_soConSvc,m_evtDataSvc,m_detDataSvc));
      m_uiSvc->addType(new MuonCoordType   (m_uiSvc,m_soConSvc,m_evtDataSvc));
      m_uiSvc->addType(new L0MuonCoordType (m_uiSvc,m_soConSvc,m_evtDataSvc));
      m_uiSvc->addType(new VertexBaseType  (m_uiSvc,m_soConSvc,m_evtDataSvc));
      // Phys :
      if(m_pPropSvc)
      {
        m_uiSvc->addType(new RecVertexType(m_uiSvc,m_soConSvc,m_evtDataSvc,m_pPropSvc));
        m_uiSvc->addType(new VertexType   (m_uiSvc,m_soConSvc,m_evtDataSvc,m_pPropSvc));
        m_uiSvc->addType(new ParticleType (m_uiSvc,m_soConSvc,m_evtDataSvc,m_pPropSvc,m_toolSvc,msgStream()));
      }
    }
  }

  return sc;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoEventSvc::finalize( )
{
  releaseSvc(m_uiSvc);
  releaseSvc(m_soConSvc);
  releaseSvc(m_evtDataSvc);
  releaseSvc(m_detDataSvc);
  releaseSvc(m_pPropSvc);
  releaseSvc(m_toolSvc);
  debug() << "Finalized successfully" << endmsg;
  return Service::finalize();
}
