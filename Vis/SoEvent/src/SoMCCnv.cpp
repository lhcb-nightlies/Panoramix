// To fix clashes between Gaudi and Windows :
#include "OnXSvc/Win32.h"

// this :
#include "SoMCCnv.h"

// Inventor :
#include "Inventor/nodes/SoSeparator.h"
#include "Inventor/nodes/SoLightModel.h"
#include "Inventor/nodes/SoDrawStyle.h"
#include "Inventor/nodes/SoCoordinate3.h"
#include "Inventor/nodes/SoTransform.h"
#include "Inventor/nodes/SoLineSet.h"

// HEPVis :
#include "HEPVis/nodes/SoSceneGraph.h"
#include "HEPVis/nodes/SoHighlightMaterial.h"

// Lib :
#include "Lib/smanip.h"
#include "Lib/Interfaces/ISession.h"

// Gaudi :
#include "GaudiKernel/CnvFactory.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/SmartDataPtr.h"

// LHCb :
#include "Event/MCHit.h"
#include "Event/MCParticle.h"

#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ClassID.h"
#include "OnXSvc/Helpers.h"

DECLARE_CONVERTER_FACTORY(SoMCCnv)

//////////////////////////////////////////////////////////////////////////////
SoMCCnv::SoMCCnv(
 ISvcLocator* aSvcLoc
) 
:SoEventConverter(aSvcLoc,SoMCCnv::classID())
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoMCCnv::createRep(
 DataObject* aObject
,IOpaqueAddress*& aAddr
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log(msgSvc(), "SoMCCnv");
  log << MSG::INFO << "MC createReps" << " name() = " << aObject->name()
      << endmsg;

  if(!fUISvc) {
    log << MSG::INFO << " UI service not found" << endmsg;
    return StatusCode::SUCCESS; 
  }

  ISession* session = fUISvc->session();
  if(!session) {
    log << MSG::INFO << " can't get ISession." << endmsg;
    return StatusCode::FAILURE; 
  }

  SoRegion* region = 0;
  if(aAddr) {
    // Optimization.
    // If having a not null aAddr, we expect a SoRegion.
    // See SoEvent/Type.h/SoEvent::Type<>::beginVisualize.
    region = (SoRegion*)aAddr; 
  } else {
    region = fUISvc->currentSoRegion();
  }
  if(!region) {
    log << MSG::INFO << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE; 
  }

  if(!aObject) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE; 
  }

  // Get modeling parameters :
  double r = 0.5, g = 0.5, b = 0.5; // grey, visible on black or white back.
  double hr = 1.0, hg = 1.0, hb = 0.0;
  std::string value;
  if(session->parameterValue("modeling.color",value))
    Lib::smanip::torgb(value,r,g,b);
  if(session->parameterValue("modeling.highlightColor",value))
    Lib::smanip::torgb(value,hr,hg,hb);

  std::string shape = "line";
  if(!session->parameterValue("modeling.shape",shape)) shape = "line";


  // This is a generic Converter, so we need to known what you need to convert
  if( aObject->name() == "/MC") {
    // Fill Particle and Hits
    P2HMap  p2h;
    fillParticlesAndHits( aObject,  p2h );

    // Loop over all Particles in the obtained container
    int pind;
    P2HMap::iterator it;
    for ( it = p2h.begin(), pind = 0; it != p2h.end(); it++, pind++ ) {
      const LHCb::MCParticle* p = (*it).first;
      HVector& hits = (*it).second;
      const LHCb::MCVertex* ov = p->originVertex();
      if (abs(int(ov->position().z())) > 1000.) continue;  

      // Build picking string id :
      char sid[64];
      ::sprintf(sid,"MCParticle/0x%lx",(unsigned long)p);

      // Create the node for this particle
      SoSceneGraph* separator = new SoSceneGraph;
      separator->setString(sid);

      // Material :
      SoHighlightMaterial* highlightMaterial = new SoHighlightMaterial;
      highlightMaterial->diffuseColor.setValue
        (SbColor((float)r,(float)g,(float)b));
      //emissiveColor is to save lines in VRML properly.
      highlightMaterial->emissiveColor.setValue
        (SbColor((float)r,(float)g,(float)b));
      highlightMaterial->highlightColor.setValue
        (SbColor((float)hr,(float)hg,(float)hb));
      //highlightMaterial->transparency.setValue((float)transparency);
      separator->addChild(highlightMaterial);
  
      // Count # of points (start + 2* for each hit + endvertices
      int nendv   = p->endVertices().size();
      // Suspected "feature" that makes the extra endVertices useless
      if ( nendv > 1 ) nendv = 1;
      int npoints = nendv + 2*(hits.size()) + 1;
      SbVec3f* points = new SbVec3f[npoints];
      int indp = 0;
      int indv = nendv - 1;
      points[indp++].setValue ((float)ov->position().x(),
                               (float)ov->position().y(),
                               (float)ov->position().z());
      //log << "o[" << indp-1 << "] = " <<ov->position() << endmsg;
      HVector::iterator ith;
      for( ith = hits.begin(); ith != hits.end(); ith++) {
        const LHCb::MCHit* hit = (*ith);
        // Add the current hit
        points[indp++].setValue ((float)hit->entry().x(),
                                 (float)hit->entry().y(),
                                 (float)hit->entry().z());
        //log << "h[" << indp-1 << "] = " <<hit->entry() << endmsg;
        points[indp++].setValue ((float)hit->exit().x(),
                                 (float)hit->exit().y(),
                                 (float)hit->exit().z());
        //log << "h[" << indp-1 << "] = " <<hit->exit() << endmsg;
      }
      // end by adding possible end vertices if any left
      for ( ; indv >= 0; indv-- ) {
        const LHCb::MCVertex* v = p->endVertices()[indv];
        points[indp++].setValue ((float)v->position().x(),
                                 (float)v->position().y(),
                                 (float)v->position().z());
        //log << "v[" << indp-1 << "] = " <<v->position() << endmsg;
      }
      if (indp != npoints ) {
        log << MSG::ERROR
            << "Something went very wrong in the counting of points"
            << endmsg;
        log << MSG::ERROR << "Counted: " << npoints << " filled: " << indp
            << endmsg;
      }

      // Add one So per particle
      SoCoordinate3* coordinate3 = new SoCoordinate3;
      coordinate3->point.setValues(0,npoints,points);
      delete [] points;
      separator->addChild(coordinate3);
 
      SoLineSet* lineSet = new SoLineSet;
        lineSet->numVertices.setValues(0,1,&npoints);
      separator->addChild(lineSet);

      //  Send scene graph to the viewing region 
      // (in the "dynamic" sub-scene graph) :
      region_addToDynamicScene(*region,separator);
      //if( pind == 5 ) break;
    }
    log << pind << " MCParticles visualized." << endmsg;
  } else {
    log << MSG::DEBUG << "Do not know to display: " << aObject->name()
        << endmsg;
    return StatusCode::SUCCESS;
  }
  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoMCCnv::classID(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{ 
  return DataObject::classID();
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoMCCnv::storageType(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{ 
  return So_TechnologyType; 
}
//////////////////////////////////////////////////////////////////////////////
void SoMCCnv::addHitToMap(
 const LHCb::MCParticle* particle
,const LHCb::MCHit* hit
,P2HMap& p2h
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  P2HMap::iterator itmap;
  itmap = p2h.find(particle);
  if( itmap == p2h.end() ) {
    // New particle. Create a vector with a single hit
    HVector hv;
    hv.push_back(hit);
    p2h.insert(P2HMap::value_type(particle,hv));
  } else {
    // Particle exists. Add the hit in the adequate place
    HVector& hv = (*itmap).second;
    hv.push_back(hit);
    p2h.insert(P2HMap::value_type(particle,hv));
  }
  // Add also all the mothers particle if not there yet
  const LHCb::MCParticle* pp = particle;
  const LHCb::MCParticle* mp;
  for( pp = particle; 
       pp->originVertex() && (mp = pp->originVertex()->mother());
       pp = mp ) {
    if( p2h.find(mp) == p2h.end() ) {
      p2h.insert(P2HMap::value_type(mp,HVector()));
    }
  }
}
//////////////////////////////////////////////////////////////////////////////
void SoMCCnv::sortHitMap(
 P2HMap& p2h
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  P2HMap::iterator it;
  HVector::iterator ith;
  for ( it = p2h.begin(); it != p2h.end(); it++ ) {
    const LHCb::MCParticle* p = (*it).first;
    HVector&  hits = (*it).second;
    Gaudi::XYZPoint currpoint = p->originVertex()->position();
     
    unsigned int nhits = hits.size();
    HVector     currhits = hits;
    hits.erase( hits.begin(), hits.end() );
    for ( unsigned int i = 0; i < nhits; i++ ) {
      // locate the closest
      HVector::iterator itminhit;      
      double mindist = 1.0e30;
      for( ith = currhits.begin(); ith != currhits.end(); ith++) {
        double mag2 = (currpoint-(*ith)->entry()).Mag2();
        if( mag2 < mindist ) {
          mindist = mag2;
          itminhit = ith;
        }
      }
      // erase from current list and insert in the final list
      currpoint = (*itminhit)->exit();
      hits.push_back(*itminhit);
      currhits.erase(itminhit);  
    }
  }
}
//////////////////////////////////////////////////////////////////////////////
void SoMCCnv::fillParticlesAndHits(
 DataObject* obj
,P2HMap& p2h
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log(msgSvc(), "SoMCCnv");

  IDataProviderSvc* dataSvc = obj->registry()->dataSvc();
  if(!dataSvc) return;
  SmartDataPtr<DataObject> mc(dataSvc, obj->registry() );

  // Some initializations
  p2h.erase( p2h.begin(), p2h.end() );
  
  std::vector<std::string> what;
  what.push_back("Velo/Hits");
  what.push_back("PuVelo/Hits");
  what.push_back("TT/Hits");
  what.push_back("IT/Hits");
  what.push_back("OT/Hits");
  what.push_back("Muon/Hits");
  what.push_back("Spd/Hits");
  what.push_back("Prs/Hits");
  what.push_back("Ecal/Hits");
  what.push_back("Hcal/Hits");
  what.push_back("Rich/Hits");

  for(unsigned int index=0;index<what.size();index++) {
    SmartDataPtr<LHCb::MCHits> hits(mc,what[index]);
    if(!hits) {
      log << MSG::WARNING 
          << what[index] << " not found in this data file." 
          << endmsg;
    } else {
      log << MSG::INFO 
          << what[index] << " found in this data file." 
          << endmsg;
      LHCb::MCHits::iterator ith;
      for (ith = hits->begin(); ith != hits->end(); ith++) {
        addHitToMap ((*ith)->mcParticle(), *ith, p2h);
      }
    }
  }


  // Lets sort the list of hits by selecting the closest hit each time
  // Loop over all particles in the map
  sortHitMap(p2h);
}
