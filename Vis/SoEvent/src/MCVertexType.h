#ifndef SoEvent_MCVertexType_h
#define SoEvent_MCVertexType_h

// Inheritance :
#include "Type.h"

#include "Event/MCVertex.h"
#include "Kernel/IParticlePropertySvc.h"

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IParticlePropertySvc;

class MCVertexType : public SoEvent::Type<LHCb::MCVertex> {
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public: //OnX::IType
  virtual void visualize(Lib::Identifier,void*);
public:
  MCVertexType(IUserInterfaceSvc*,ISoConversionSvc*,IDataProviderSvc*,
	       LHCb::IParticlePropertySvc*);
private:
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
};

#endif
