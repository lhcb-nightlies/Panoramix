#ifndef SoEvent_SoMCHitCnv_h
#define SoEvent_SoMCHitCnv_h

#include "SoEventConverter.h"

template <class T> class CnvFactory;

class SoMCHitCnv : public SoEventConverter
{
  //friend class CnvFactory<SoMCHitCnv>;
public:
  SoMCHitCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
