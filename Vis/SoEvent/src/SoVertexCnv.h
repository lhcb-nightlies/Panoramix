#ifndef SoEvent_SoVertexCnv_h
#define SoEvent_SoVertexCnv_h

#include "SoEventConverter.h"
#include "Event/Vertex.h"

template <class T> class CnvFactory;

class SoVertexCnv : public SoEventConverter {
  //friend class CnvFactory<SoVertexCnv>;
public:
  SoVertexCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
