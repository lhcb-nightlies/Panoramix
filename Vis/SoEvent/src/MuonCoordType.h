#ifndef SoEvent_MuonCoordType_h
#define SoEvent_MuonCoordType_h

// Inheritance :
#include "Type.h"

#include "Event/MuonCoord.h" //The data class.
#include "Event/MuonDigit.h" //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;

class MuonCoordType : public SoEvent::Type<LHCb::MuonCoord> {
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public: //OnX::IType
  virtual void visualize(Lib::Identifier,void*);
public:
  MuonCoordType(IUserInterfaceSvc*,ISoConversionSvc*,IDataProviderSvc*);
private:
  void visualizeMCParticle(LHCb::MuonCoord&);
};

#endif
