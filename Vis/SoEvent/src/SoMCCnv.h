#ifndef SoEvent_SoMCCnv_h
#define SoEvent_SoMCCnv_h

#include "SoEventConverter.h"
#include <map>
#include <vector>
namespace LHCb {
  class MCParticle;
  class MCHit;
}

template <class T> class CnvFactory;

class SoMCCnv : public SoEventConverter {
  //friend class CnvFactory<SoMCCnv>;
public:
  SoMCCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
private:
  typedef std::vector<const LHCb::MCHit*>  HVector;
  typedef std::map<const LHCb::MCParticle*, HVector > P2HMap;

  void fillParticlesAndHits (DataObject*, P2HMap & );
  void addHitToMap (const LHCb::MCParticle*, const LHCb::MCHit*, P2HMap&);
  void sortHitMap (P2HMap&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
