
// this :
#include "SoEventConverter.h"

// Gaudi :
#include "GaudiKernel/ISvcLocator.h" 
#include "Kernel/IParticlePropertySvc.h"
#include "GaudiKernel/IDataProviderSvc.h"

// OnXSvc :
#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ClassID.h"

//////////////////////////////////////////////////////////////////////////////
SoEventConverter::SoEventConverter(
 ISvcLocator* aSvcLoc
,CLID aCLID
) 
:Converter(So_TechnologyType,aCLID,aSvcLoc) 
,fSoCnvSvc(0)
,fUISvc(0)
,fParticlePropertySvc(0)
,fDetectorDataSvc(0)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoEventConverter::initialize(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  StatusCode status = Converter::initialize();
  if( status.isFailure() ) return status;
  status = serviceLocator()->service("SoConversionSvc",fSoCnvSvc);
  if(status.isFailure()) return status;
  status = serviceLocator()->service("OnXSvc",fUISvc);
  if(status.isFailure()) return status;
  status = serviceLocator()->service("LHCb::ParticlePropertySvc",fParticlePropertySvc);
  if(status.isFailure()) return status;
  status = serviceLocator()->service("DetectorDataSvc",fDetectorDataSvc);
  if(status.isFailure()) return status;
  return status;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoEventConverter::finalize() 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  //debug if(fSoCnvSvc) fSoCnvSvc->release();
  //debug if(fUISvc) fUISvc->release();
  //debug if(fParticlePropertySvc) fParticlePropertySvc->release();
  return Converter::finalize();
}
//////////////////////////////////////////////////////////////////////////////
long SoEventConverter::repSvcType() const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return i_repSvcType();
}
