#ifndef SoEvent_SoVertexBaseCnv_h
#define SoEvent_SoVertexBaseCnv_h

#include "SoEventConverter.h"
#include "Event/VertexBase.h"

template <class T> class CnvFactory;

class SoVertexBaseCnv : public SoEventConverter {
  //friend class CnvFactory<SoVertexBaseCnv>;
public:
  SoVertexBaseCnv(ISvcLocator*);
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
