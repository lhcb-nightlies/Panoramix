// To fix clashes between Gaudi and Windows :
#include "OnXSvc/Win32.h"

// this :
#include "SoTrackCnv.h"

// Inventor :
#include "Inventor/nodes/SoSeparator.h"
#include "Inventor/nodes/SoLightModel.h"
#include "Inventor/nodes/SoDrawStyle.h"
#include "Inventor/nodes/SoCoordinate3.h"
#include "Inventor/nodes/SoLineSet.h"
#include "Inventor/nodes/SoCylinder.h"

// HEPVis :
#include "HEPVis/nodes/SoSceneGraph.h"
#include "HEPVis/nodes/SoArrow.h"
#include "HEPVis/nodes/SoHighlightMaterial.h"
#include "HEPVis/nodes/SoMarkerSet.h"
typedef HEPVis_SoMarkerSet SoMarkerSet;
#include "HEPVis/misc/SoStyleCache.h"

// Lib :
#include "Lib/smanip.h"
#include "Lib/Interfaces/ISession.h"

// Gaudi :
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/MsgStream.h"
#include "Kernel/IParticlePropertySvc.h"
#include "GaudiKernel/CnvFactory.h"
#include "Kernel/ParticleProperty.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/IMagneticFieldSvc.h"
// gsl
#include <gsl/gsl_sf_exp.h>

// LHCb :
#include "Event/Track.h"
#include "Event/State.h"
#include "Event/Node.h"
#include "Event/OTTime.h"
#include "Event/FTLiteCluster.h"
#include "Event/FTMeasurement.h"
#include "Event/OTMeasurement.h"
#include "Event/STMeasurement.h"
#include "Event/VeloRMeasurement.h"
#include "Event/VeloPhiMeasurement.h"
#include "Event/STLiteMeasurement.h"
#include "Event/VeloLiteRMeasurement.h"
#include "Event/VeloLitePhiMeasurement.h"
#include "Event/VPMeasurement.h"
#include "Event/STCluster.h"
#include "Event/STLiteCluster.h"
#include "GaudiKernel/IToolSvc.h"
#include "TrackInterfaces/ITrackExtrapolator.h"
#include "Kernel/ITrajPoca.h"
#include "TrackKernel/StateTraj.h"
#include "TrackKernel/TrackTraj.h"

#include "GaudiKernel/Point3DTypes.h"

// SoUtils :
#include "SoUtils/SbProjector.h"

// OnXSvc :
#include "OnXSvc/Filter.h"
#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ClassID.h"
#include "OnXSvc/Helpers.h"


DECLARE_CONVERTER_FACTORY(SoTrackCnv)

int curvePointNumber = 10;
static void statePointAndNormal(const LHCb::State&,SbVec3f&,SbVec3f&);
SbBool send2scene = FALSE;
SbBool sendsep2scene = FALSE;

#define TOLERANCE (1.0*Gaudi::Units::mm)

//////////////////////////////////////////////////////////////////////////////
SoTrackCnv::SoTrackCnv(
                       ISvcLocator* aSvcLoc
                       )
  :SoEventConverter(aSvcLoc,SoTrackCnv::classID())
                      //////////////////////////////////////////////////////////////////////////////
                      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoTrackCnv::createRep(
                                 DataObject* aObject
                                 ,IOpaqueAddress*& aAddr
                                 )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log(msgSvc(), "SoTrackCnv");
  //log << MSG::INFO << "SoTrack::createRep." << endmsg;

  if(!fUISvc) {
    log << MSG::INFO << " UI service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  if(!fParticlePropertySvc) {
    log << MSG::INFO << " ParticleProperty service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  ISession* session = fUISvc->session();
  if(!session) {
    log << MSG::ERROR << " can't get ISession." << endmsg;
    return StatusCode::FAILURE;
  }

  SoRegion* region = 0;
  if(aAddr) {
    // Optimization.
    // If having a not null aAddr, we expect a SoRegion.
    // See SoEvent/Type.h/SoEvent::Type<>::beginVisualize.
    region = (SoRegion*)aAddr;
  } else {
    region = fUISvc->currentSoRegion();
  }
  if(!region) {
    log << MSG::INFO << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE;
  }

  if(!aObject) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE;
  }

  IToolSvc* toolSvc = 0;
  StatusCode sc = service( "ToolSvc", toolSvc, true );
  if( sc.isFailure() ) {
    log << MSG::FATAL << "Unable to retrieve ToolSvc " << endmsg;
  }
  // get extrapolator
  ITrackExtrapolator* extrapolator;
  //  sc = toolSvc->retrieveTool("TrackHerabExtrapolator", extrapolator);
  sc = toolSvc->retrieveTool("TrackMasterExtrapolator", extrapolator);
  if (sc.isFailure()){
    log << MSG::FATAL << "Unable to retrieve Extrapolator Tool " << endmsg;
    return sc;
  }
  // get linear extrapolator
  ITrackExtrapolator* linextrapolator;
  sc = toolSvc->retrieveTool("TrackLinearExtrapolator", linextrapolator);
  if (sc.isFailure()){
    log << MSG::FATAL << "Unable to retrieve linear Extrapolator Tool " << endmsg;
    return sc;
  }
  // get poca tool
  ITrajPoca* poca ;
  sc = toolSvc->retrieveTool("TrajPoca",poca);
  if (sc.isFailure()){
    log << MSG::FATAL << "Unable to retrieve Poca Tool " << endmsg;
    return sc;
  }
  // magnetic field service
  IMagneticFieldSvc* magneticFieldSvc = 0;
  service("MagneticFieldSvc",magneticFieldSvc,true);
  if(!magneticFieldSvc) {
    log << MSG::INFO << " MagneticFieldSvc not found " << endmsg;
  }

  LHCb::Tracks* tracks = dynamic_cast<LHCb::Tracks*>(aObject);
  if(!tracks) {
    log << MSG::INFO << " bad object type." << endmsg;
    return StatusCode::FAILURE;
  }

  bool deleteVector = false;
  // Filter :
  Filter<LHCb::Track> filter(*fUISvc,log);
  const std::string& cuts = fUISvc->cuts();
  if(cuts!="") {
    log << MSG::INFO << " cuts \"" << cuts << "\"" << endmsg;
    tracks = filter.collect(*tracks,"Track",cuts);
    if(!tracks) return StatusCode::SUCCESS;
    //filter.dump(*tracks,"Track");
    deleteVector = true;;
  }


  if (tracks->empty()) {
    log << MSG::DEBUG << " collection is empty." << endmsg;
    return StatusCode::SUCCESS;
  }

  // Representation attributes :
  // Get color (default is grey (valid on black or white background) ):
  std::string value;
  double r = 0.5, g = 0.5, b = 0.5;
  if(session->parameterValue("modeling.color",value))
    Lib::smanip::torgb(value,r,g,b);
  double hr = 1.0, hg = 1.0, hb = 0.0;
  if(session->parameterValue("modeling.highlightColor",value))
    Lib::smanip::torgb(value,hr,hg,hb);
  double lineWidth = 0;
  if(session->parameterValue("modeling.lineWidth",value))
    if(!Lib::smanip::todouble(value,lineWidth)) lineWidth = 0;
  // precision :
  session->parameterValue("modeling.precision",value);
  int temp;
  Lib::smanip::toint(value,temp);
  if (temp>0){curvePointNumber = temp;}
  // Non linear projections :
  session->parameterValue("modeling.projection",value);
  std::string projection = value;
  std::string proj_opt   = value;
  if (projection=="-ZR" || projection=="-ZRY" ) {projection="ZR";}
  SoUtils::SbProjector projector(value.c_str());

  // No Markers for states :
  session->parameterValue("modeling.noStates",value);
  bool noStates = false;
  if (value == "true"){  noStates = true;}
  
  SoStyleCache* styleCache = region->styleCache();
  SoLightModel* lightModel = styleCache->getLightModelBaseColor();
  SoDrawStyle* drawStyle =
    styleCache->getLineStyle(SbLinePattern_solid,float(lineWidth));

  bool showNormals = false;
  if(session->parameterValue("modeling.showNormals",value))
    if(!Lib::smanip::tobool(value,showNormals)) showNormals = false;

  bool userTrackRange = false;
  if(session->parameterValue("modeling.userTrackRange",value))
    if(!Lib::smanip::tobool(value,userTrackRange)) userTrackRange = false;
  double userbz = -1000.;
  if(session->parameterValue("modeling.trackStartz",value))
    if(!Lib::smanip::todouble(value,userbz)) userbz = -1000.;
  double userez = 10000.;
  if(session->parameterValue("modeling.trackEndz",value))
    if(!Lib::smanip::todouble(value,userez)) userez = 10000.;

  LHCb::Tracks::iterator it;
  for(it = tracks->begin(); it != tracks->end(); it++) {
    const LHCb::Track* track  = (*it);

    const std::vector< LHCb::State * >& states = track->states();
    int number = states.size();
    if(number<=0) continue;

    std::vector<const LHCb::Measurement*> measurements(track->measurements()) ;
    int numberm = measurements.size();
    std::vector<const LHCb::Node*> nodes(track->nodes().begin(),track->nodes().end()) ;

    std::string modeling;
    fUISvc->session()->parameterValue("modeling.what",modeling);
    if ( modeling != "this" ){
      // Loop over all nodes on the track
      for ( unsigned int in = 0; in < nodes.size(); in++) {
        // check if node has connected measurement
        if (!nodes[in]->hasMeasurement()) {continue;}
        // get chi2sq and translate to probability for later use
        //double test1 = nodes[in]->residual();
        //double test2 = nodes[in]->errMeasure();

        double chi =  nodes[in]->residual()/nodes[in]->errResidual();
        double abs_chi = fabs(chi);
        // fix colours for measurement display
        double M_r = 0.0, M_g = 1.0, M_b = 0.0;
        if (abs_chi>2) {
          M_r = 1.0;
          M_g = 0.65;
          M_b = 0.0;
        }
        if (abs_chi>4) {
          M_r = 1.0;
          M_g = 0.0;
          M_b = 0.0;
        }
        if (abs_chi>9) {
          chi = 9.*chi/abs_chi;
          M_r = 1.0;
          M_g = 0.0;
          M_b = 1.0;
        }

        // Define separator visualization object
        SoSceneGraph* separator = new SoSceneGraph;
        send2scene = FALSE;
        //separator->setString(sid); //done in the below

        SoHighlightMaterial* highlightMaterial = new SoHighlightMaterial;
        highlightMaterial->diffuseColor.setValue(SbColor(float(M_r),float(M_g),float(M_b)));
        //emissiveColor is to save lines in VRML properly.
        highlightMaterial->emissiveColor.setValue(SbColor(float(M_r),float(M_g),float(M_b)));
        highlightMaterial->highlightColor.setValue(SbColor(float(M_r),float(M_g),float(M_b)));
        separator->addChild(highlightMaterial);
        separator->addChild(lightModel);
        separator->addChild(drawStyle);

        const LHCb::Measurement* meas = &(nodes[in]->measurement());
        // Draw the OTClusterOnTracks
        if( (modeling == "OTMeasurements" || modeling == "Measurements")
            && (meas->type())==LHCb::Measurement::OT ) {

          const LHCb::OTMeasurement* otMeas = dynamic_cast<const LHCb::OTMeasurement*>(meas);

          LHCb::OTChannelID channel = otMeas->channel();

          // Build name :
          char sid[64];
          ::sprintf(sid,"OTTime/0x%lx",(unsigned long)channel);
          separator->setString(sid);

          // Get the x,y,z on the trajectory which is closest to the track
          LHCb::State cstate = nodes[in]->state() ;

          Gaudi::XYZVector distance;
          Gaudi::XYZVector bfield;
          magneticFieldSvc->fieldVector( cstate.position(), bfield );
          LHCb::StateTraj stateTraj = LHCb::StateTraj( nodes[in]->refVector(), bfield );
          double s1 = 0.0;
          double s2 = otMeas->trajectory().arclength( stateTraj.position(s1) );
          poca->minimize(stateTraj, s1, otMeas->trajectory(), s2, distance,20*Gaudi::Units::mm);
          Gaudi::XYZPoint mpoint = otMeas->trajectory().position(s2);

          // Define the center of the cross as a 3D point
          SbVec3f point(float(mpoint.x()), float(mpoint.y()), float(mpoint.z()));
          SoCoordinate3* coordinate3 = new SoCoordinate3;
          projector.project(1,&point);
          coordinate3->point.setValues(0,1,&point);
          separator->addChild(coordinate3);

          // Define cross as marker
          SoMarkerSet* markerSet = new SoMarkerSet;
          markerSet->numPoints = 1;
          markerSet->markerIndex = SoMarkerSet::CROSS_9_9;
          separator->addChild(markerSet);

          // Draw a circle around the cross. The radius corresponds to the
          // drift-time in the cluster (corrected for propagation along wire).
          int pointn = 16;
          double dangle = 2*M_PI/(pointn-1);
          SbVec3f* points = new SbVec3f[pointn];
          double r = otMeas->driftRadiusWithErrorFromY( mpoint.y() ).val ;
          Gaudi::XYZVector direct = otMeas->trajectory().direction(s2);
          Gaudi::Rotation3D rot(Gaudi::AxisAngle(direct,0.));
          for(int index=0;index<pointn;index++) {
            double angCircle = dangle * index;
            Gaudi::XYZVector v1(r * cos(angCircle),0.,r * sin(angCircle));
            // now rotate in space that it becomes perpendicular to the trajectory
            Gaudi::XYZVector v2 = rot*v1;
            points[index].setValue(float(v2.x()+mpoint.x()), float(v2.y()+mpoint.y()), float(v2.z()+mpoint.z()) );
          }
          SoCoordinate3* circleCoordinate3 = new SoCoordinate3;
          projector.project(pointn,points);
          circleCoordinate3->point.setValues(0,pointn,points);
          SoLineSet* lineSet = new SoLineSet;
          lineSet->numVertices.setValues(0,1,&pointn);
          separator->addChild(circleCoordinate3);
          separator->addChild(lineSet);

          // draw line along distance indicating chi2 contribution
          Gaudi::XYZPoint epoint = stateTraj.position(s1);
          double scale = 5.*chi ;
          Gaudi::XYZVector dir = -fabs(scale)*distance.unit();
          // special for OT, we need to take into account ambiguity & driftdistance,
          // as measTraj is the wire...
          if ( otMeas->ambiguity()*distance.x()>0 && chi<0 ) {
            dir *= -1.; // I think/hope I got this one right
          }
          points[0].setValue(float(epoint.x()), float(epoint.y()), float(epoint.z()));
          points[1].setValue(float(epoint.x()+dir.x()),float(epoint.y()+dir.y()),float(epoint.z()+dir.z()));

          SoCoordinate3* lineCoordinate3 = new SoCoordinate3;
          projector.project(pointn,points);
          lineCoordinate3->point.setValues(0,pointn,points);
          delete [] points;
          pointn = 2;
          SoLineSet* linec2Set = new SoLineSet;
          linec2Set->numVertices.setValues(0,1,&pointn);
          separator->addChild(lineCoordinate3);
          separator->addChild(linec2Set);

          // make another cross at the start of the chi2 line
          SbVec3f pointe(float(epoint.x()), float(epoint.y()), float(epoint.z()));
          SoCoordinate3* ecoordinate3 = new SoCoordinate3;
          projector.project(1,&pointe);
          ecoordinate3->point.setValues(0,1,&pointe);
          separator->addChild(ecoordinate3);

          // Define cross as marker
          SoMarkerSet* markerSete = new SoMarkerSet;
          markerSete->numPoints = 1;
          markerSete->markerIndex = SoMarkerSet::CROSS_5_5;
          separator->addChild(markerSete);

          //  Send scene graph to the viewing region
          // (in the "dynamic" sub-scene graph) :
          region_addToDynamicScene(*region,separator);
          send2scene = TRUE; 
        } 
        else if ( (
                   (modeling == "ITMeasurements" ) ||
                   (modeling == "TTMeasurements" ) ||
                   (modeling == "STMeasurements" ) ||
                   (modeling == "Measurements" )
                   ) && (
                         (meas->type()==LHCb::Measurement::IT) ||
                         (meas->type()==LHCb::Measurement::ITLite) ||
                         (meas->type()==LHCb::Measurement::UT) ||
                         (meas->type()==LHCb::Measurement::TT) || 
                         (meas->type()==LHCb::Measurement::TTLite)
                         )
                   ) 
        {
          const LHCb::Measurement* stMeas = dynamic_cast<const LHCb::Measurement*>(meas);

          if( meas->type()==LHCb::Measurement::ITLite || meas->type()==LHCb::Measurement::TTLite ){
           // don't know what to do
           const LHCb::STLiteMeasurement* stMeas = dynamic_cast<const LHCb::STLiteMeasurement*>(meas);
           const LHCb::STLiteCluster cluster = stMeas->cluster();
          // Build name :
           char sid[64];
           ::sprintf(sid,"STLiteCluster/0x%lx",(unsigned long)&cluster);
           separator->setString(sid);          
        }else{ 
           // Get the geometry of the STCluster
           const LHCb::STMeasurement* stMeas = dynamic_cast<const LHCb::STMeasurement*>(meas);
           const LHCb::STCluster* cluster = stMeas->cluster();
          // Build name :
           char sid[64];
           ::sprintf(sid,"STCluster/0x%lx",(unsigned long)cluster);
           separator->setString(sid);
        }

          // Get the x,y,z on the trajectory which is closest to the track
          LHCb::State cstate = nodes[in]->state() ;

          Gaudi::XYZVector distance;
          Gaudi::XYZVector bfield;
          magneticFieldSvc->fieldVector( cstate.position(), bfield );
          LHCb::StateTraj stateTraj = LHCb::StateTraj( nodes[in]->refVector(), bfield );
          double s1 = 0.0;
          double s2 = stMeas->trajectory().arclength( stateTraj.position(s1) );
          poca->minimize(stateTraj, s1, stMeas->trajectory(), s2, distance,20*Gaudi::Units::mm);
          Gaudi::XYZPoint mpoint = stMeas->trajectory().position(s2);

          // draw line along distance indicating chi2 contribution
          int pointn = 2;
          SbVec3f* points = new SbVec3f[pointn];
          // draw line along distance indicating chi2 contribution
          Gaudi::XYZPoint epoint = stateTraj.position(s1);
          double scale = 1.*chi ;
          Gaudi::XYZVector dir = -fabs(scale)*distance.unit();
          points[0].setValue(float(epoint.x()), float(epoint.y()), float(epoint.z()));
          points[1].setValue(float(epoint.x()+dir.x()),float(epoint.y()+dir.y()),float(epoint.z()+dir.z()));

          SoCoordinate3* lineCoordinate3 = new SoCoordinate3;
          projector.project(pointn,points);
          lineCoordinate3->point.setValues(0,pointn,points);
          delete [] points;
          SoLineSet* linec2Set = new SoLineSet;
          linec2Set->numVertices.setValues(0,1,&pointn);
          separator->addChild(lineCoordinate3);
          separator->addChild(linec2Set);

          // Define the center of the cross as a 3D point
          SbVec3f point(float(mpoint.x()), float(mpoint.y()), float(mpoint.z()));
          SoCoordinate3* coordinate3 = new SoCoordinate3;
          projector.project(1,&point);
          coordinate3->point.setValues(0,1,&point);
          separator->addChild(coordinate3);

          // Define cross as marker
          SoMarkerSet* markerSet = new SoMarkerSet;
          markerSet->numPoints = 1;
          markerSet->markerIndex = SoMarkerSet::CROSS_9_9;
          separator->addChild(markerSet);

          // make another cross at the start of the chi2 line
          SbVec3f pointe(float(epoint.x()), float(epoint.y()), float(epoint.z()));
          SoCoordinate3* ecoordinate3 = new SoCoordinate3;
          projector.project(1,&pointe);
          ecoordinate3->point.setValues(0,1,&pointe);
          separator->addChild(ecoordinate3);

          // Define cross as marker
          SoMarkerSet* markerSete = new SoMarkerSet;
          markerSete->numPoints = 1;
          markerSete->markerIndex = SoMarkerSet::CROSS_5_5;
          separator->addChild(markerSete);

          region_addToDynamicScene(*region,separator);
          send2scene = TRUE; 
        }  
        else if (
                   (modeling == "FTMeasurements" || modeling == "Measurements" )
                    && meas->type()==LHCb::Measurement::FT                   ) 
        {
          const LHCb::FTMeasurement* ftMeas = dynamic_cast<const LHCb::FTMeasurement*>(meas);

          // Get the geometry of the FTCluster
          const LHCb::FTLiteCluster cluster = ftMeas->cluster();

          // Build name :
          char sid[64];
          ::sprintf(sid,"FTCluster/0x%lx",(unsigned long)&cluster);
          separator->setString(sid);

          // Get the x,y,z on the trajectory which is closest to the track
          LHCb::State cstate = nodes[in]->state() ;

          Gaudi::XYZVector distance;
          Gaudi::XYZVector bfield;
          magneticFieldSvc->fieldVector( cstate.position(), bfield );
          LHCb::StateTraj stateTraj = LHCb::StateTraj( nodes[in]->refVector(), bfield );
          double s1 = 0.0;
          double s2 = ftMeas->trajectory().arclength( stateTraj.position(s1) );
          poca->minimize(stateTraj, s1, ftMeas->trajectory(), s2, distance,20*Gaudi::Units::mm);
          Gaudi::XYZPoint mpoint = ftMeas->trajectory().position(s2);

          // draw line along distance indicating chi2 contribution
          int pointn = 2;
          SbVec3f* points = new SbVec3f[pointn];
          // draw line along distance indicating chi2 contribution
          Gaudi::XYZPoint epoint = stateTraj.position(s1);
          double scale = 1.*chi ;
          Gaudi::XYZVector dir = -fabs(scale)*distance.unit();
          points[0].setValue(float(epoint.x()), float(epoint.y()), float(epoint.z()));
          points[1].setValue(float(epoint.x()+dir.x()),float(epoint.y()+dir.y()),float(epoint.z()+dir.z()));

          SoCoordinate3* lineCoordinate3 = new SoCoordinate3;
          projector.project(pointn,points);
          lineCoordinate3->point.setValues(0,pointn,points);
          delete [] points;
          SoLineSet* linec2Set = new SoLineSet;
          linec2Set->numVertices.setValues(0,1,&pointn);
          separator->addChild(lineCoordinate3);
          separator->addChild(linec2Set);

          // Define the center of the cross as a 3D point
          SbVec3f point(float(mpoint.x()), float(mpoint.y()), float(mpoint.z()));
          SoCoordinate3* coordinate3 = new SoCoordinate3;
          projector.project(1,&point);
          coordinate3->point.setValues(0,1,&point);
          separator->addChild(coordinate3);

          // Define cross as marker
          SoMarkerSet* markerSet = new SoMarkerSet;
          markerSet->numPoints = 1;
          markerSet->markerIndex = SoMarkerSet::CROSS_9_9;
          separator->addChild(markerSet);

          // make another cross at the start of the chi2 line
          SbVec3f pointe(float(epoint.x()), float(epoint.y()), float(epoint.z()));
          SoCoordinate3* ecoordinate3 = new SoCoordinate3;
          projector.project(1,&pointe);
          ecoordinate3->point.setValues(0,1,&pointe);
          separator->addChild(ecoordinate3);

          // Define cross as marker
          SoMarkerSet* markerSete = new SoMarkerSet;
          markerSete->numPoints = 1;
          markerSete->markerIndex = SoMarkerSet::CROSS_5_5;
          separator->addChild(markerSete);

          region_addToDynamicScene(*region,separator);
          send2scene = TRUE; 
        } else if( (modeling == "VeloMeasurements" || modeling == "Measurements") && (
                   (meas->type()==LHCb::Measurement::VeloR) || (meas->type()==LHCb::Measurement::VeloPhi) 
                   || (meas->type()==LHCb::Measurement::VeloLiteR) || (meas->type()==LHCb::Measurement::VeloLitePhi)  )) {

          const LHCb::Measurement* veloMeas = dynamic_cast<const LHCb::Measurement*>(meas);

          // Build picking string id :
          char sid[64];
          if( (meas->type())==LHCb::Measurement::VeloR) {
            const LHCb::VeloRMeasurement* veloRMeas = dynamic_cast<const LHCb::VeloRMeasurement*>(meas);
            ::sprintf(sid,"VeloCluster/0x%lx",(unsigned long)(veloRMeas->cluster() ));
          }else if( (meas->type())==LHCb::Measurement::VeloPhi){
            const LHCb::VeloPhiMeasurement* veloPhiMeas = dynamic_cast<const LHCb::VeloPhiMeasurement*>(meas);
            ::sprintf(sid,"VeloCluster/0x%lx",(unsigned long)(veloPhiMeas->cluster() ));
          }else if( (meas->type())==LHCb::Measurement::VeloLiteR) {
            const LHCb::VeloLiteRMeasurement* veloRMeas = dynamic_cast<const LHCb::VeloLiteRMeasurement*>(meas);
            ::sprintf(sid,"VeloLiteCluster/0x%lx",(unsigned long)(veloRMeas->cluster() ));
          }else if( (meas->type())==LHCb::Measurement::VeloLitePhi) {
            const LHCb::VeloLitePhiMeasurement* veloPhiMeas = dynamic_cast<const LHCb::VeloLitePhiMeasurement*>(meas);
            ::sprintf(sid,"VeloLiteCluster/0x%lx",(unsigned long)(veloPhiMeas->cluster() ));
          }
          separator->setString(sid);

          // Get the x,y,z on the trajectory which is closest to the track
          LHCb::State cstate;
          if (track->checkType(LHCb::Track::Velo)){
            linextrapolator->propagate(*(track), veloMeas->z(),cstate);
          } else {
            extrapolator->propagate(*(track), veloMeas->z(),cstate);
          }
          Gaudi::XYZVector distance;
          Gaudi::XYZVector bfield;
          magneticFieldSvc->fieldVector( cstate.position(), bfield );
          LHCb::StateTraj stateTraj = LHCb::StateTraj( nodes[in]->refVector(), bfield );
          double s1 = 0.0;
          double s2 = veloMeas->trajectory().arclength( stateTraj.position(s1) );
          poca->minimize(stateTraj, s1, veloMeas->trajectory(), s2, distance,20*Gaudi::Units::mm);
          Gaudi::XYZPoint mpoint = veloMeas->trajectory().position(s2);

          // draw line along distance indicating chi2 contribution
          int pointn = 2;
          SbVec3f* points = new SbVec3f[pointn];
          Gaudi::XYZPoint epoint = stateTraj.position(s1);
          double scale = 1.*chi ;
          Gaudi::XYZVector dir = -fabs(scale)*distance.unit();
          points[0].setValue(float(epoint.x()), float(epoint.y()), float(epoint.z()));
          points[1].setValue(float(epoint.x()+dir.x()),float(epoint.y()+dir.y()),float(epoint.z()+dir.z()));

          SoCoordinate3* lineCoordinate3 = new SoCoordinate3;
          projector.project(pointn,points);
          lineCoordinate3->point.setValues(0,pointn,points);
          delete [] points;
          SoLineSet* linec2Set = new SoLineSet;
          linec2Set->numVertices.setValues(0,1,&pointn);
          separator->addChild(lineCoordinate3);
          separator->addChild(linec2Set);

          // Define the center of the cross as a 3D point
          SbVec3f point(float(mpoint.x()), float(mpoint.y()), float(mpoint.z()));
          SoCoordinate3* coordinate3 = new SoCoordinate3;
          projector.project(1,&point);
          coordinate3->point.setValues(0,1,&point);
          separator->addChild(coordinate3);

          // Define cross as marker
          SoMarkerSet* markerSet = new SoMarkerSet;
          markerSet->numPoints = 1;
          markerSet->markerIndex = SoMarkerSet::CROSS_9_9;
          separator->addChild(markerSet);

          // make another cross at the start of the chi2 line
          SbVec3f pointe(float(epoint.x()), float(epoint.y()), float(epoint.z()));
          SoCoordinate3* ecoordinate3 = new SoCoordinate3;
          projector.project(1,&pointe);
          ecoordinate3->point.setValues(0,1,&pointe);
          separator->addChild(ecoordinate3);

          // Define cross as marker
          SoMarkerSet* markerSete = new SoMarkerSet;
          markerSete->numPoints = 1;
          markerSete->markerIndex = SoMarkerSet::CROSS_5_5;
          separator->addChild(markerSete);

          region_addToDynamicScene(*region,separator);
          send2scene = TRUE; 
        } else if( (modeling == "VPMeasurements" || modeling == "Measurements") && (
                                                                                      (meas->type()==LHCb::Measurement::VP)  )) {
          const LHCb::VPMeasurement* vpMeas = dynamic_cast<const LHCb::VPMeasurement*>(meas);

          // Build picking string id :
          char sid[64];
          ::sprintf(sid,"VPCluster/0x%lx",(unsigned long)(vpMeas->cluster() ));
          separator->setString(sid);

          // Get the x,y,z on the trajectory which is closest to the track
          LHCb::State cstate;
          if (track->checkType(LHCb::Track::Velo)){
            linextrapolator->propagate(*(track), vpMeas->z(),cstate);
          } else {
            extrapolator->propagate(*(track), vpMeas->z(),cstate);
          }
          Gaudi::XYZVector distance;
          Gaudi::XYZVector bfield;
          magneticFieldSvc->fieldVector( cstate.position(), bfield );
          LHCb::StateTraj stateTraj = LHCb::StateTraj( nodes[in]->refVector(), bfield );
          double s1 = 0.0;
          double s2 = vpMeas->trajectory().arclength( stateTraj.position(s1) );
          poca->minimize(stateTraj, s1, vpMeas->trajectory(), s2, distance,20*Gaudi::Units::mm);
          Gaudi::XYZPoint mpoint = vpMeas->trajectory().position(s2);

          // draw line along distance indicating chi2 contribution
          int pointn = 2;
          SbVec3f* points = new SbVec3f[pointn];
          Gaudi::XYZPoint epoint = stateTraj.position(s1);
          double scale = 1.*chi ;
          Gaudi::XYZVector dir = -fabs(scale)*distance.unit();
          points[0].setValue(float(epoint.x()), float(epoint.y()), float(epoint.z()));
          points[1].setValue(float(epoint.x()+dir.x()),float(epoint.y()+dir.y()),float(epoint.z()+dir.z()));

          SoCoordinate3* lineCoordinate3 = new SoCoordinate3;
          projector.project(pointn,points);
          lineCoordinate3->point.setValues(0,pointn,points);
          delete [] points;
          SoLineSet* linec2Set = new SoLineSet;
          linec2Set->numVertices.setValues(0,1,&pointn);
          separator->addChild(lineCoordinate3);
          separator->addChild(linec2Set);

          // Define the center of the cross as a 3D point
          SbVec3f point(float(mpoint.x()), float(mpoint.y()), float(mpoint.z()));
          SoCoordinate3* coordinate3 = new SoCoordinate3;
          projector.project(1,&point);
          coordinate3->point.setValues(0,1,&point);
          separator->addChild(coordinate3);

          // Define cross as marker
          SoMarkerSet* markerSet = new SoMarkerSet;
          markerSet->numPoints = 1;
          markerSet->markerIndex = SoMarkerSet::CROSS_9_9;
          separator->addChild(markerSet);

          // make another cross at the start of the chi2 line
          SbVec3f pointe(float(epoint.x()), float(epoint.y()), float(epoint.z()));
          SoCoordinate3* ecoordinate3 = new SoCoordinate3;
          projector.project(1,&pointe);
          ecoordinate3->point.setValues(0,1,&pointe);
          separator->addChild(ecoordinate3);

          // Define cross as marker
          SoMarkerSet* markerSete = new SoMarkerSet;
          markerSete->numPoints = 1;
          markerSete->markerIndex = SoMarkerSet::CROSS_5_5;
          separator->addChild(markerSete);

          region_addToDynamicScene(*region,separator);
          send2scene = TRUE; 
        } // end of measurement IF THEN
       if (!send2scene)  
         separator->unref();
      }
    }
    // Build picking string id :
    char sid[64];
    ::sprintf(sid,"Track/0x%lx",(unsigned long)track);

    int pointn = (number+1)*curvePointNumber;
    SbVec3f* points = new SbVec3f[pointn];
    int pointi = 0;

    // Build the scene graph :
    SoSceneGraph* separator = new SoSceneGraph;
    send2scene = FALSE; 
    separator->setString(sid);

//TR      SoSeparator* sepEnd = new SoSeparator;
//TR      sendsep2scene = FALSE; 
    // Material :
    SoHighlightMaterial* highlightMaterial = new SoHighlightMaterial;
    highlightMaterial->diffuseColor.setValue(SbColor(float(r),float(g),float(b)));
    //emissiveColor is to save lines in VRML properly.
    highlightMaterial->emissiveColor.setValue(SbColor(float(r),float(g),float(b)));
    highlightMaterial->highlightColor.setValue(SbColor(float(hr),float(hg),float(hb)));
    //highlightMaterial->transparency.setValue((float)transparency);
    separator->addChild(highlightMaterial);

    SbVec3f point,normal;
    for(int count=0;count<number;count++) {
      const LHCb::State& state = *(states[count]);
      if(projection =="ZR" && state.z()>1000.) continue;
      // temporary hack
      if(track->checkFlag(LHCb::Track::Backward) && !(userTrackRange) &&
         track->checkType(LHCb::Track::Velo)     && fabs(state.z())>800.)  continue;
      if(track->checkType(LHCb::Track::Velo)     && !(userTrackRange) && state.z()>1000.) continue;
      statePointAndNormal(state,point,normal);
      // make a marker for every state
      SbVec3f pointst;
      if ( noStates != true ) {
       pointst.setValue(point);
       SoCoordinate3* coordinate3 = new SoCoordinate3;
       projector.project(1,&pointst);
       coordinate3->point.setValues(0,1,&pointst);
       separator->addChild(coordinate3);
       SoMarkerSet* markerSet = new SoMarkerSet;
       markerSet->numPoints = 1;
       markerSet->markerIndex = SoMarkerSet::DIAMOND_FILLED_5_5;
       separator->addChild(markerSet);
      }
      if( count < number - 1 ||  userTrackRange ){

        SbVec3f end,endNormal;
        if (count == number - 1) {
          LHCb::State cstate;
          if (userez < 12000.){
            extrapolator->propagate(*(track), userez,cstate);
          } else{
            linextrapolator->propagate(*(track), userez,cstate);
          }
          statePointAndNormal(cstate,end,endNormal);
        } else {
          statePointAndNormal(*(states[count+1]),end,endNormal);
        }
        // default LHCb display
        // Use extrapolators to make the curve.
        int index;
        double bz = point[2];
        double ez = end[2];
        if ( count == 0        &&  userTrackRange ) { bz = userbz;}
        if ( ez > userez &&  userTrackRange ) { ez = userez;}
        double dz = (ez - bz) / (curvePointNumber-1);
        Gaudi::XYZPoint pos;
        LHCb::Track& thetrack  = *(*it);
        LHCb::TrackTraj traj( thetrack );
        for ( index=0; index<curvePointNumber; ++index) {
          pos = traj.position( bz ) ;
          double rp = pos.rho();
          if( projection!="ZR" || userTrackRange || (projection=="ZR" && rp<45. && bz<1000.) ||
              (projection=="ZR" && track->checkType(LHCb::Track::Downstream) && bz<1000.)  ) {
            if( !(userTrackRange && ( pos.z() < userbz || pos.z() > userez )) ) {
                 points[pointi] = SbVec3f(float(pos.x()),float(pos.y()),float(pos.z()));
                ++pointi;
            }
          }
          bz+=dz;
        }
       if(showNormals) { //Draw the end normal :
//TR          log << MSG::ERROR << " who asked for this ? " << endmsg;
//TR          SoArrow* arrowBegin = new SoArrow;
//TR          arrowBegin->tail = point;
//TR          arrowBegin->tip = point + normal * 1000;
//TR         sepEnd->addChild(arrowBegin);
//TR          SoArrow* arrow = new SoArrow;
//TR          arrow->tail = end;
//TR          arrow->tip = end + endNormal * 1000;
//TR          sepEnd->addChild(arrow);
        }
      } else if(number==1) {
        points[pointi] = point;
        pointi++;
      }
    }

    /* From XmlDDDB/DDDB/Ecal/Installation/Ecal.xml
       ### Z Geometry ###
       Ecal starts at Z=12490*mm from the interaction point with a total
       space along Z of 835*mm; Ecal Modules geometry centers are offset
       to the center of the total ECAL space.
    */

    if( (number < 2 || track->checkType(LHCb::Track::Velo) ||
         track->checkType(LHCb::Track::Upstream) ||
         (track->checkType(LHCb::Track::Long) && projection !="ZR")) && userTrackRange==false  ) {
      // some ugly logic to make something reasonable if there is only one "measured" state
      double zlast = 12490;
      if (numberm == 0 && number == 1) zlast = 900.;
      if (track->checkType(LHCb::Track::Muon) ) zlast = 20000.;
      if ( track->checkType(LHCb::Track::Upstream) ) zlast = 5000.;
      if ( track->checkFlag(LHCb::Track::Backward) ) zlast = -600.;
      if ( (track->checkType(LHCb::Track::Velo) || track->checkType(LHCb::Track::VeloR) ) && numberm > 0 ) {
        zlast = measurements[numberm-1]->z() + 10.;
        if (track->checkFlag(LHCb::Track::Backward) ) {
          zlast = measurements[numberm-1]->z() - 10.;
        }
     }

     double u = (zlast - point[2]) / normal[2];
     double x = normal[0] * u + point[0];
     double y = normal[1] * u + point[1];

     if ( projection == "ZR" || track->checkType(LHCb::Track::Velo) || track->checkType(LHCb::Track::VeloR) ) {
        double rsq  = x*x+y*y;
        double rmsq;
        if (track->checkType(LHCb::Track::Velo) || track->checkType(LHCb::Track::VeloR) ){
          rmsq = 45.*45.;
        }else{
          rmsq = 60.*60.;
        }
        if(rsq>rmsq){
          double a1 = normal[0]*normal[0] + normal[1]*normal[1] ;
          double a2 = 2.*(normal[0]*point[0] + normal[1]*point[1]) ;
          double a3 = (point[0]*point[0] + point[1]*point[1]) ;
          u = 1./(2*a1)*(-a2+sqrt(a2*a2-4*a1*(a3-rmsq)));
          double u2 = 1./(2*a1)*(-a2-sqrt(a2*a2-4*a1*(a3-rmsq)));
          if (u2<u && track->checkFlag(LHCb::Track::Backward) ){u=u2;}
          if (u2>u && !(track->checkFlag(LHCb::Track::Backward)) ){u=u2;}
          zlast = u*normal[2]+ point[2];
          x = normal[0] * u + point[0];
          y = normal[1] * u + point[1];
        }
      }
      if ( track->checkHistory(LHCb::Track::TrackMatching) ){
        SbVec3f del = points[pointi-1]- points[pointi-2];
        points[pointi] = points[pointi-1] + 10.0F*del;
        pointi++;
      } else if ( fabs(zlast) > fabs(point[2]) ) {
        points[pointi].setValue(float(x),float(y),float(zlast));
        pointi++;
      }
    }
    // all points defined
//TR    if(showNormals)  {
//TR        separator->addChild(sepEnd);
//TR        sendsep2scene = TRUE;}
    SoCoordinate3* coordinate3 = new SoCoordinate3;
    // remove track segments in overlap for rz (x) projection
    // not quite save, in case first point in one half  second point outside
    int newpointi = 0;
    if ( projection == "ZR" && proj_opt == "-ZR"){
      for ( int n=0; n<pointi; ++n) {
        if (points[n][0]*points[0][0] > 0) {
          newpointi+=1;
        }
        else {break;}
      }

      if (pointi != newpointi ){
        if (newpointi < pointi / 2  &&  !(track->checkFlag(LHCb::Track::Backward)) ) {
          SbVec3f* npoints = new SbVec3f[pointi-newpointi];
          for ( int n=0; n<pointi-newpointi; ++n) {
            npoints[n] = points[n+newpointi];
          }
          projector.project(pointi-newpointi,npoints);
          coordinate3->point.setValues(0,pointi-newpointi,npoints) ;
          SoLineSet* lineSet = new SoLineSet;
          int32_t vertices = pointi-newpointi;
          lineSet->numVertices.setValues(0,1,&vertices);
          separator->addChild(lightModel);
          separator->addChild(drawStyle);
          separator->addChild(coordinate3);
          separator->addChild(lineSet);
          region_addToDynamicScene(*region,separator);
          send2scene = TRUE;  
          pointi=1;
        }
        else {
          pointi=newpointi-1;
        }
      }
    }
    if (pointi>1){
      projector.project(pointi,points);
      coordinate3->point.setValues(0,pointi,points);

      SoLineSet* lineSet = new SoLineSet;
      int32_t vertices = pointi;
      lineSet->numVertices.setValues(0,1,&vertices);

      separator->addChild(lightModel);
      separator->addChild(drawStyle);

      separator->addChild(coordinate3);
      separator->addChild(lineSet);

      //  Send scene graph to the viewing region
      // (in the "dynamic" sub-scene graph) :
      region_addToDynamicScene(*region,separator);
      send2scene = TRUE; 
    }
    if (!send2scene)  {
       separator->unref();
    }
    delete [] points;
    if(deleteVector) {
      // We have first to empty the vector :
      while(tracks->size()) {
        tracks->remove(*(tracks->begin()));
      }
      // Then we can delete it :
      delete tracks;
    }

  }
  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoTrackCnv::classID(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return LHCb::Tracks::classID();
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoTrackCnv::storageType(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return So_TechnologyType;
}
//////////////////////////////////////////////////////////////////////////////
void statePointAndNormal(
                         const LHCb::State& aState
                         ,SbVec3f& aPoint
                         ,SbVec3f& aNormal
                         )
//////////////////////////////////////////////////////////////////////////////
//   x = nx * u + x0
//   y = ny * u + y0
//   z = nz * u + z0
//
//   tx = (x-x0)/(z-z0) = nx/nz
//   ty = (y-y0)/(z-z0) = ny/nz
//
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{

  double tx = 0;
  double ty = 0;
  aPoint.setValue(float(aState.x()),float(aState.y()),float(aState.z()));
  tx = aState.tx();
  ty = aState.ty();

  aNormal.setValue(float(tx),float(ty),1.);
  aNormal.normalize();
}
