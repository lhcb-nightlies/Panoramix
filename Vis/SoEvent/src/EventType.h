#ifndef SoEvent_EventType_h
#define SoEvent_EventType_h

// Inheritance :
#include "Lib/BaseType.h"
#include "Lib/Interfaces/IIterator.h"

class IUserInterfaceSvc;
class IDataProviderSvc;
class DataObject;

class EventType : public Lib::BaseType {
public:
  EventType(IUserInterfaceSvc*,IDataProviderSvc*);
public: //Lib::IType
  virtual std::string name() const;
  virtual Lib::IIterator* iterator();
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
private:
  DataObject* getObject(const std::string&);
private:
  std::string fType;
  IUserInterfaceSvc* fUISvc;
  IDataProviderSvc* fDataProviderSvc;
};

#endif
