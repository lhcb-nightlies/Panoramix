#ifndef SoEvent_RecVertexType_h
#define SoEvent_RecVertexType_h

// Inheritance :
#include "Type.h"
#include "Kernel/IParticlePropertySvc.h"

#include "Event/RecVertex.h" // The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IParticlePropertySvc;

class RecVertexType : public SoEvent::Type<LHCb::RecVertex> {
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
public:
  RecVertexType(IUserInterfaceSvc*,
         ISoConversionSvc*,
         IDataProviderSvc*,
         LHCb::IParticlePropertySvc*);
  Lib::IIterator* iterator();
private:
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
};

#endif
