// To fix clashes between Gaudi and Windows :
#include <OnXSvc/Win32.h>

// this :
#include "SoHepMCEventCnv.h"

// Lib :
#include <Lib/smanip.h>
#include <Lib/Interfaces/ISession.h>

// Gaudi :
#include <GaudiKernel/ISvcLocator.h>
#include <Kernel/IParticlePropertySvc.h>
#include <GaudiKernel/Converter.h>
#include <GaudiKernel/CnvFactory.h>
#include <GaudiKernel/MsgStream.h>
#include <Kernel/ParticleProperty.h>

// OnXSvc :
#include <OnXSvc/IUserInterfaceSvc.h>
#include <OnXSvc/ISoConversionSvc.h>
#include <OnXSvc/ClassID.h>

#include "Types.h"

// HepMC :
#include <Event/HepMCEvent.h>
#include <HepMC/GenEvent.h>

DECLARE_CONVERTER_FACTORY( SoHepMCEventCnv )

//////////////////////////////////////////////////////////////////////////////
SoHepMCEventCnv::SoHepMCEventCnv(
                                 ISvcLocator* aSvcLoc
                                 )
  :Converter(So_TechnologyType,SoHepMCEventCnv::classID(),aSvcLoc)
  ,fSoCnvSvc(0)
  ,fUISvc(0)
  ,fParticlePropertySvc(0)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoHepMCEventCnv::initialize(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  StatusCode status = Converter::initialize();
  if( status.isFailure() ) return status;
  status = serviceLocator()->service("SoConversionSvc",fSoCnvSvc);
  if(status.isFailure()) return status;
  status = serviceLocator()->service("OnXSvc",fUISvc);
  if(status.isFailure()) return status;
  status =
    serviceLocator()->service("ParticlePropertySvc",fParticlePropertySvc);
  if(status.isFailure()) return status;
  return status;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoHepMCEventCnv::finalize()
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  //debug if(fSoCnvSvc) fSoCnvSvc->release();
  //debug if(fUISvc) fUISvc->release();
  //debug if(fParticlePropertySvc) fParticlePropertySvc->release();
  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
long SoHepMCEventCnv::repSvcType() const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return i_repSvcType();
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoHepMCEventCnv::createRep(
                                      DataObject* aObject
                                      ,IOpaqueAddress*&
                                      )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log(msgSvc(), "SoHepMCEventCnv");
  log << MSG::INFO << "SoHEPMcEventCnv createReps" << endmsg;

  if(!fUISvc) {
    log << MSG::INFO << " UI service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  if(!fParticlePropertySvc) {
    log << MSG::INFO << " ParticleProperty service not found" << endmsg;
    return StatusCode::SUCCESS;
  }

  ISession* session = fUISvc->session();
  if(!session) {
    log << MSG::INFO << " can't get ISession." << endmsg;
    return StatusCode::FAILURE;
  }

  SoRegion* soRegion = fUISvc->currentSoRegion();
  if(!soRegion) {
    log << MSG::INFO << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE;
  }

  if(!aObject) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE;
  }

  LHCb::HepMCEvents* mcEvents = dynamic_cast<LHCb::HepMCEvents*>(aObject);
  if(!mcEvents) {
    log << MSG::INFO << " bad object type." << endmsg;
    return StatusCode::FAILURE;
  }

  log << MSG::INFO << " number of events : " << mcEvents->size() << endmsg;

  LHCb::HepMCEvents::iterator it;
  for(it = mcEvents->begin(); it != mcEvents->end(); it++) {
    HepMC::GenEvent* genEvent = (*it)->pGenEvt();

    log << MSG::INFO
        << " number of particles : " << genEvent->particles_size()
        << " number of vertices : " << genEvent->vertices_size()
        << endmsg;
    for ( HepMC::GenEvent::particle_const_iterator p
            = genEvent->particles_begin();
          p != genEvent->particles_end(); ++p ) {


      HepMC::GenParticle* genParticle = (*p);
      GenParticleType::represent(genParticle,
                                 session,soRegion,
                                 fParticlePropertySvc);

    }

  }

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoHepMCEventCnv::classID(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return LHCb::HepMCEvents::classID();
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoHepMCEventCnv::storageType(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return So_TechnologyType;
}
