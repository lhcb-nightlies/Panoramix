// this :
#include "SoHepMCSvc.h"

// Gaudi :
#include <GaudiKernel/SvcFactory.h>
#include <GaudiKernel/MsgStream.h>

#include <GaudiKernel/IDataProviderSvc.h>
#include <Kernel/IParticlePropertySvc.h>

// OnXSvc :
#include <OnXSvc/IUserInterfaceSvc.h>

#include "Types.h"

DECLARE_SERVICE_FACTORY( SoHepMCSvc )

//////////////////////////////////////////////////////////////////////////////
SoHepMCSvc::SoHepMCSvc(
 const std::string& aName
,ISvcLocator* aSvcLoc
) 
:Service(aName,aSvcLoc)
,m_uiSvc(0)
,m_eventDataSvc(0)
,m_particlePropertySvc(0)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
SoHepMCSvc::~SoHepMCSvc(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{ 
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoHepMCSvc::initialize(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  StatusCode status = Service::initialize();
  if( status.isFailure() ) return status;

  MsgStream log(msgSvc(), Service::name());

  log << MSG::INFO << " SoHepMCSvc::initialize " << endmsg;

  setProperties();

  if(!serviceLocator()) {
    log << MSG::INFO << " service locator not found " << endmsg;
    return StatusCode::FAILURE;
  }

  // Get the Detector data service :
  
  if(m_uiSvc) {
    m_uiSvc->release();
    m_uiSvc = 0;
  }
  status = service("OnXSvc",m_uiSvc,true);
  if(status.isFailure()  || !m_uiSvc) {
    log << MSG::INFO << " OnXSvc not found " << endmsg;
    return StatusCode::FAILURE;
  }
  m_uiSvc->addRef();

  if(m_eventDataSvc) {
    m_eventDataSvc->release();
    m_eventDataSvc = 0;
  }
  status = service("EventDataSvc" , m_eventDataSvc , true );
  if(status.isFailure() || !m_eventDataSvc) {
    log << MSG::INFO << " EventDataSvc not found " << endmsg;
  }
  m_eventDataSvc->addRef();

  if(m_particlePropertySvc) {
    m_particlePropertySvc->release();
    m_particlePropertySvc = 0;
  }
  status = service("ParticlePropertySvc" , m_particlePropertySvc , true );
  if(status.isFailure() || !m_particlePropertySvc) {
    log << MSG::INFO << " ParticlePropertySvc not found " << endmsg;
  }
  m_particlePropertySvc->addRef();
  
  
  if(m_eventDataSvc) {
    if(m_particlePropertySvc) {
      m_uiSvc->addType(new GenParticleType(m_particlePropertySvc,
					   m_eventDataSvc,
					   m_uiSvc));
    }
  }
  
  return status;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoHepMCSvc::finalize(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(m_uiSvc) {
    m_uiSvc->release();
    m_uiSvc = 0;
  }
  if(m_eventDataSvc) {
    m_eventDataSvc->release();
    m_eventDataSvc = 0;
  }
  if(m_particlePropertySvc) {
    m_particlePropertySvc->release();
    m_particlePropertySvc = 0;
  }

  MsgStream log(msgSvc(), Service::name());
  log << MSG::INFO << "SoHepMCSvc finalized successfully" << endmsg;
  return StatusCode::SUCCESS;
}
