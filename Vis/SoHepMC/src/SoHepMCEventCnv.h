#ifndef SoHepMC_SoHepMCEventCnv_h
#define SoHepMC_SoHepMCEventCnv_h

#include <GaudiKernel/Converter.h>
#include "Kernel/IParticlePropertySvc.h"

template <class T> class CnvFactory;

class ISoConversionSvc;
class IUserInterfaceSvc;
class IParticlePropertySvc;

class SoHepMCEventCnv : public Converter {
  friend class CnvFactory<SoHepMCEventCnv>;
public:
  SoHepMCEventCnv(ISvcLocator*);
  virtual StatusCode initialize();
  virtual StatusCode finalize();
  virtual long repSvcType() const; 
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
private:
  ISoConversionSvc* fSoCnvSvc;
  IUserInterfaceSvc* fUISvc;
  LHCb::IParticlePropertySvc* fParticlePropertySvc;
};

#endif
