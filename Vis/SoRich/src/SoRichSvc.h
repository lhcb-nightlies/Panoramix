#ifndef SoRich_SoRichSvc_h
#define SoRich_SoRichSvc_h

// Inheritance :
#include "GaudiKernel/Service.h"
#include "GaudiKernel/MsgStream.h"

// forward declaration
template <class SVC> class SvcFactory;

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IToolSvc;

class SoRichSvc : public Service
{
  friend class SvcFactory<SoRichSvc>;
public:
  virtual StatusCode initialize();
  virtual StatusCode finalize();
  virtual ~SoRichSvc();
protected:
  SoRichSvc(const std::string& name,ISvcLocator* SvcLoc);
private:
  SoRichSvc();
  SoRichSvc( const SoRichSvc& );
  SoRichSvc& operator=( const SoRichSvc& );
private:
  inline MsgStream & msgStream() 
  {
    if ( !m_msgStream ) { m_msgStream = new MsgStream(msgSvc(), Service::name()); }
    return *m_msgStream; 
  }
  MsgStream & debug()       { return msgStream() << MSG::DEBUG;   }
  MsgStream & info()        { return msgStream() << MSG::INFO;    }
  MsgStream & error()       { return msgStream() << MSG::WARNING; }
  MsgStream & warning()     { return msgStream() << MSG::ERROR;   }
private:
  template < class SVC >
  void releaseSvc( SVC *& svc ) { if (svc) { svc->release(); svc=NULL; } }
  template < class SVC >
  StatusCode getService( const std::string & name, SVC *& svc )
  {
    releaseSvc(svc);
    const StatusCode sc = service(name,svc,true);
    if ( sc.isFailure() || !svc ) 
    {
      error() << name << " not found" << endmsg;
    } else { svc->addRef(); }
    return sc;
  }
private:
  IUserInterfaceSvc* m_uiSvc;
  ISoConversionSvc* m_soConSvc;
  IDataProviderSvc* m_evtDataSvc;
  IToolSvc* m_toolSvc;
  MsgStream* m_msgStream;
};

#endif
