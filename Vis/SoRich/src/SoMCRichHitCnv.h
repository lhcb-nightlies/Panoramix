
/** @file SoMCRichHitCnv.h
 *
 *  Header file for RICH "So" visualisation converter : SoMCRichHitCnv
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#ifndef     SORICH_SOMCRICHHITCNV_H
#define     SORICH_SOMCRICHHITCNV_H 1

// STD and STL
#include <vector>
#include <string>

// SoRich
#include "SoRichBaseCnv.h"

// forward decalarations
template <class CNV>
class CnvFactory;

/**  @class SoMCRichHitCnv  SoMCRichHitCnv.h
 *
 *   Converter for visualization of MCRichHit objects
 *
 *   @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *   @date    09/05/2004
 */

class SoMCRichHitCnv : public SoRichBaseCnv {

  friend class CnvFactory<SoMCRichHitCnv>;

public:

  /// standard initialization method
  virtual StatusCode initialize ();

  /// standard finalization  method
  virtual StatusCode finalize   ();

  /// Returns the representation type
  virtual long repSvcType() const;

  // the only one essential method
  virtual StatusCode createRep( DataObject* /* Object */ ,
                                IOpaqueAddress*&  /* Address */ );

  /// Class ID for created object == class ID for this specific converter
  static const CLID&  classID();

  /// storage Type
  static unsigned char storageType () ;

protected:

  /// standard constructor
  SoMCRichHitCnv( ISvcLocator* svcLoc );

  /// virtual destructor
  virtual ~SoMCRichHitCnv();

private:

  /// default constructor is disabled
  SoMCRichHitCnv           (                       ) ;

  /// copy constructor is disabled
  SoMCRichHitCnv           ( const SoMCRichHitCnv& ) ;

  /// assignment is disabled
  SoMCRichHitCnv& operator=( const SoMCRichHitCnv& ) ;

private:

};


// ============================================================================
#endif  //  SORICH_SOMCRICHHITCNV_H
// ============================================================================













