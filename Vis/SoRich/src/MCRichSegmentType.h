#ifndef SoRich_MCRichSegmentType_h
#define SoRich_MCRichSegmentType_h

// Inheritance :
#include <OnXSvc/KeyedType.h>

#include <Event/MCRichSegment.h> //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class SoSeparator;
class MCRichSegmentRep;

class MCRichSegmentType 
:public OnXSvc::KeyedType<LHCb::MCRichSegment> 
{
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
  virtual void beginVisualize();
  virtual void visualize(Lib::Identifier aIdentifier,void*);
  virtual void endVisualize();
public:
  MCRichSegmentType(IUserInterfaceSvc*,
                  ISoConversionSvc*,
                  IDataProviderSvc*);
private:
  SoSeparator* m_separator;
  MCRichSegmentRep* m_rep;
  bool m_empty;
};

#endif
