#ifndef SoRich_MCRichHitType_h
#define SoRich_MCRichHitType_h

// Inheritance :
#include <OnXSvc/Type.h>

#include <Event/MCRichHit.h> //The data class.

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class SoSeparator;
class MCRichHitRep;

class MCRichHitType : public OnXSvc::Type<LHCb::MCRichHit> {
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
  virtual void beginVisualize();
  virtual void visualize(Lib::Identifier aIdentifier,void*);
  virtual void endVisualize();
public:
  MCRichHitType(IUserInterfaceSvc*,
                  ISoConversionSvc*,
                  IDataProviderSvc*);
private:
  SoSeparator* m_separator;
  MCRichHitRep* m_rep;
  bool m_empty;
};

#endif
