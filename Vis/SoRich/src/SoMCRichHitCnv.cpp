
/** @file SoMCRichHitCnv.cpp
 *
 *  Implementation file for RICH "So" visual converter : SoMCRichHitCnv
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#include <OnXSvc/Win32.h>

// this :
#include "SoMCRichHitCnv.h"

// GaudiKernel
#include <GaudiKernel/IRegistry.h>
#include <GaudiKernel/CnvFactory.h>
#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiKernel/SmartDataPtr.h>
#include <GaudiKernel/DataObject.h>
#include <GaudiKernel/IToolSvc.h>

// OnXSvc
#include <OnXSvc/IUserInterfaceSvc.h>
#include <OnXSvc/ClassID.h>
#include <OnXSvc/Helpers.h>

// Lib :
#include <Lib/smanip.h>
#include <Lib/Interfaces/ISession.h>

// Inventor :
#include <Inventor/nodes/SoSeparator.h>

// HEPVis :
#include <HEPVis/nodekits/SoRegion.h>

// RichEvent
#include <Event/MCRichHit.h>

// Local
#include "MCRichHitRep.h"

// namespaces
using namespace LHCb;

// ============================================================================
// mandatory factory business
// ============================================================================
DECLARE_CONVERTER_FACTORY( SoMCRichHitCnv )

// ============================================================================
// standard constructor
// ============================================================================
SoMCRichHitCnv::SoMCRichHitCnv( ISvcLocator* svcLoc )
  : SoRichBaseCnv ( classID(), svcLoc )
{
  setName ( "SoMCRichHitCnv" );
}

// ============================================================================
// standard desctructor
// ============================================================================
SoMCRichHitCnv::~SoMCRichHitCnv(){}

// ============================================================================
// initialize
// ============================================================================
StatusCode SoMCRichHitCnv::initialize()
{
  StatusCode sc = SoRichBaseCnv::initialize();
  if ( sc.isFailure() )
    return Error("initialize: Could not initialize base class!");

  return StatusCode::SUCCESS;
}

// ============================================================================
// finalize
// ============================================================================
StatusCode SoMCRichHitCnv::finalize()
{
  return SoRichBaseCnv::finalize();
}

long SoMCRichHitCnv::repSvcType() const
{
  return i_repSvcType();
}

// ============================================================================
// Class ID for created object == class ID for this specific converter
// ============================================================================
const CLID& SoMCRichHitCnv::classID()
{
  return MCRichHits::classID();
}

// ============================================================================
// storage Type
// ============================================================================
unsigned char SoMCRichHitCnv::storageType()
{
  return So_TechnologyType;
}

// ============================================================================
// the only one essential method
// ============================================================================
StatusCode SoMCRichHitCnv::createRep( DataObject*         object     ,
                                      IOpaqueAddress*& /* Address */ )
{

  // Preliminary checks
  if ( 0 == object  ) return Error("createRep: DataObject* points to NULL");
  if ( 0 == uiSvc() ) return Error("createRep: IUserInterfaceSvc* points to NULL" );


  SoRegion* soRegion = uiSvc()->currentSoRegion();
  if(soRegion==0)
  { return Error("createRep: SoRegion* points to NULL" );}

  ISession* session = uiSvc()->session();
  if(session==0)
  { return Error("createRep: ISession* points to NULL" );}

  const MCRichHits* hits = dynamic_cast<const MCRichHits*>( object );
  if ( 0 == hits  )
  { return Error( "createRep: Wrong input data type '" +
                  System::typeinfoName(typeid(object)) + "'" ); }
  if ( hits->empty() )
  { return Debug( "Container " + object->registry()->identifier() +
                  " is empty" ); }

  // Find first non-null digit
  typedef MCRichHits::const_iterator RiHitIt;
  RiHitIt ihit;
  for ( ihit = hits->begin(); ihit != hits->end(); ++ihit ) { if(*ihit) break; }
  if( hits->end() == ihit )
  { return Warning("Container " + object->registry()->identifier() +
                   " contains NULL pointers !" , StatusCode::SUCCESS ) ;}

  // Colours
  double r, g, b, hr, hg, hb;
  std::string color1, color2;
  if ( !session->parameterValue( "modeling.color", color1 ) ) color1 = "red";
  Lib::smanip::torgb(color1,r,g,b);
  SbColor color(r,g,b);
  if ( !session->parameterValue( "modeling.highlightColor", color2 ) ) color2 = "red";
  Lib::smanip::torgb(color2,hr,hg,hb);
  SbColor hcolor(hr,hg,hb);
  std::string shape;
  if ( !session->parameterValue("modeling.MCRichHitMode",shape) ) shape = "marker";

  // create visualisation object
  Debug( "Visualizer for MCRichHits : Style= " +shape +
         " : colors="+color1+" "+color2 );
  MCRichHitRep vis( shape,
                    MCRichHitRep::Qualities( color, hcolor,
                                             SoMarkerSet::CIRCLE_FILLED_5_5),
                    soRegion->styleCache());

  SoSeparator* separator = vis.begin();

  bool empty = true;
  for(ihit=hits->begin();ihit!=hits->end();++ihit) {
    const LHCb::MCRichHit* hit = *ihit;
    if(!hit) continue;
    vis.represent(hit,separator);
    empty = false;
  }

  if(empty) {
    separator->unref();
  } else {
    soRegion->resetUndo();
    region_addToDynamicScene(*soRegion,separator);
  }

  return StatusCode::SUCCESS;
}
