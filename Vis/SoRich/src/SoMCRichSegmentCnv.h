
/** @file SoMCRichSegmentCnv.h
 *
 *  Header file for RICH "So" visualisation converter : SoMCRichSegmentCnv
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

#ifndef     SORICH_SOMCRICHSEGMENTCNV_H
#define     SORICH_SOMCRICHSEGMENTCNV_H 1

// SoRich
#include "SoRichBaseCnv.h"

// forward decalarations
template <class CNV> class CnvFactory;

/**  @class SoMCRichSegmentCnv  SoMCRichSegmentCnv.h
 *
 *   Converter for visualization of MCRichSegment objects
 *
 *   @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *   @date    09/05/2004
 */

class SoMCRichSegmentCnv : public SoRichBaseCnv {

  friend class CnvFactory<SoMCRichSegmentCnv>;

public:

  /// standard initialization method
  virtual StatusCode initialize ();

  /// standard finalization  method
  virtual StatusCode finalize   ();

  /// Returns the representation type
  virtual long repSvcType() const;

  /// the only one essential method
  virtual StatusCode createRep( DataObject* /* Object */ ,
                                IOpaqueAddress*&  /* Address */ );

  /// Class ID for created object == class ID for this specific converter
  static const CLID&  classID();

  /// storage Type
  static unsigned char storageType () ;

protected:

  /// standard constructor
  SoMCRichSegmentCnv( ISvcLocator* svcLoc );

  /// virtual destructor
  virtual ~SoMCRichSegmentCnv();

private:

  /// default constructor is disabled
  SoMCRichSegmentCnv           (                       ) ;

  /// copy constructor is disabled
  SoMCRichSegmentCnv           ( const SoMCRichSegmentCnv& ) ;

  /// assignment is disabled
  SoMCRichSegmentCnv& operator=( const SoMCRichSegmentCnv& ) ;

private:

};


// ============================================================================
#endif  //  SORICH_SOMCRICHSEGMENTCNV_H
// ============================================================================













