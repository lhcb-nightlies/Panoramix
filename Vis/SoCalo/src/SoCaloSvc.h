// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.5  2008/07/28 08:13:33  truf
// just to please cmt/cvs, no changes
//
// Revision 1.4  2007/02/02 15:48:29  ranjard
// v8r0 - fix to use new PluginManager
//
// Revision 1.3  2006/12/21 10:21:20  gybarran
// G.Barrand : revisit the addRef/release of svcs
//
// Revision 1.2  2002/09/11 07:00:58  barrand
// G.Barrand : update according new event model and OnX/v11
//
// Revision 1.1.1.1  2001/10/17 18:26:06  ibelyaev
// New Package: Visualization of Calorimeter objects 
//
// ============================================================================
#ifndef SOCALO_SOCALOSVC_H 
#define SOCALO_SOCALOSVC_H 1

// Inheritance :
#include <GaudiKernel/Service.h>

// Include files
// from STD & STL
#include <string>
// from Gaudi
// forward declaration 
template <class SVC> class SvcFactory;

class IUserInterfaceSvc;

/** @class SoCaloSvc SoCaloSvc.h
 *  
 *   Auxillary class (probably temporary) for declaring 
 *  "Calorimeter" visualization Types to the system 
 *  
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru 
 *  @date   09/10/2001
 */

class SoCaloSvc : public Service
{
  friend class SvcFactory<SoCaloSvc>; 

public:
  
  /** Standard initialization method
   *  just declare all available calormeter visualisation types 
   *  @return status code 
   */
  virtual StatusCode initialize();
  
  /** standard finaliation method
   *  @return statsu code 
   */
  virtual StatusCode finalize  ();
  
  virtual ~SoCaloSvc( ); ///< Destructor
  
  protected:
  
  /** Standard constructor
   *  @param name name of the service 
   *  @param SvcLoc pointer to Service Locator
   */
  SoCaloSvc( const std::string& name, ISvcLocator* SvcLoc );
  
private:
  
  SoCaloSvc();
  SoCaloSvc( const SoCaloSvc& );
  SoCaloSvc& operator=( const SoCaloSvc& );

private:
  
  IUserInterfaceSvc* m_uiSvc;
};

// ============================================================================
#endif // SOCALOSVC_H
// ============================================================================
