// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.3  2009/05/26 08:32:17  gybarran
// *** empty log message ***
//
// Revision 1.2  2008/10/07 08:22:06  truf
// *** empty log message ***
//
// Revision 1.1  2008/09/24 15:12:38  truf
// *** empty log message ***
//
// 
// ============================================================================
// Inventor
#include  "Inventor/nodes/SoSeparator.h"
#include <Inventor/nodes/SoLightModel.h>
// HEPVis :
#include  "HEPVis/nodekits/SoRegion.h"
#include <HEPVis/nodes/SoHighlightMaterial.h>
#include <HEPVis/misc/SoStyleCache.h>
// OnXSvc
#include  "OnXSvc/Win32.h"
// STD & STL
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
// GaudiKernel
#include  "GaudiKernel/CnvFactory.h" 
#include  "GaudiKernel/IRegistry.h"
#include  "GaudiKernel/DataObject.h"
#include  "GaudiKernel/IDataProviderSvc.h"
#include  "GaudiKernel/SmartDataPtr.h"
// OnXSvc
#include  "OnXSvc/IUserInterfaceSvc.h"
#include  "OnXSvc/ClassID.h"
#include  "OnXSvc/Helpers.h"
// CaloEvent 
#include  "Event/L0CaloCandidate.h"
#include  "Kernel/CaloCellID.h"
// CaloDet 
#include  "CaloDet/DeCalorimeter.h"
// Local 
#include  "SoL0CaloDigitCnv.h" 
#include  "L0CaloDigitRep.h" 

#include <Lib/Interfaces/ISession.h>
#include <Lib/smanip.h>

/** @file SoL0CaloDigitCnv.cpp
 * 
 *  implementation of SoL0CaloDigitCnv class 
 *  
 *  @author Fernando Rodrigues
 *  @date   13/08/2008
 *
 *  @file based on SoCaloDigitCnv.cpp
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru 
 *  @date   18/04/2001 
 */

// ============================================================================
/// mandatory factory business 
// ============================================================================
DECLARE_CONVERTER_FACTORY(SoL0CaloDigitCnv)

// ============================================================================
/// standard constructor 
// ============================================================================
SoL0CaloDigitCnv::SoL0CaloDigitCnv( ISvcLocator* svcLoc )
  : SoCaloBaseCnv( classID() , svcLoc ) 
  , m_calos()
{  
  setName      ( "SoL0CaloDigitCnv"  ); 
  setDetSvcName( "DetectorDataSvc" ); 
}

// ============================================================================
/// standard desctructor 
// ============================================================================
SoL0CaloDigitCnv::~SoL0CaloDigitCnv(){}

// ============================================================================
/// initialize 
// ============================================================================
StatusCode SoL0CaloDigitCnv::initialize()
{
  StatusCode sc = SoCaloBaseCnv::initialize(); 
  if( sc.isFailure() ) 
    { return Error("initialize: could not initialize base class!"); } 
  return StatusCode::SUCCESS;
}

// ============================================================================
/// finalize 
// ============================================================================
StatusCode SoL0CaloDigitCnv::finalize()
{ return SoCaloBaseCnv::finalize(); }

long SoL0CaloDigitCnv::repSvcType() const
{ return i_repSvcType();}

// ============================================================================
/// Class ID for created object == class ID for this specific converter 
// ============================================================================
const CLID&         SoL0CaloDigitCnv::classID  () { 
  //return LHCb::L0CaloCandidate::classID();
  return LHCb::L0CaloCandidates::classID();
} 

// ============================================================================
/// storage Type 
// ============================================================================
unsigned char SoL0CaloDigitCnv::storageType () 
{ return So_TechnologyType; }  

// ============================================================================
/// the only one essential method 
// ============================================================================
StatusCode SoL0CaloDigitCnv::createRep( DataObject*         object     ,
				        IOpaqueAddress*& /* Address */ )
{

  // Preliminary checks
  if ( 0 == object  ) return Error("createRep: DataObject* points to NULL"        );
  if ( 0 == uiSvc() ) return Error("createRep: IUserInterfaceSvc* points to NULL" );
  SoRegion* soRegion = uiSvc()->currentSoRegion();
  if ( soRegion==0  ) return Error("createRep: SoRegion* points to NULL" );
  ISession* session = uiSvc()->session();
  if ( session==0   ) return Error("createRep: ISession* points to NULL" );

  ///
  const LHCb::L0CaloCandidates* digits = dynamic_cast<const LHCb::L0CaloCandidates*> ( object ); 
  if ( 0 == digits  ) 
     return Error("createRep: Wrong input data type '" + System::typeinfoName(typeid(object)) + "'" ); 
  if (digits->empty()) 
     return Print( "Container " + object->registry()->identifier() + " is empty", MSG::INFO, StatusCode::SUCCESS );
  if (digits->size() == 0) 
     return Print("container " + object->registry()->identifier() + " is empty", MSG::INFO , StatusCode::SUCCESS ) ;

  /// Find first non/null digit 
  typedef LHCb::L0CaloCandidates::const_iterator DigIt;  //ObjectVector <<< see Event/L0CaloCandidate.h
  DigIt idig;
  // Verify if contains only NULLS.
  for(idig=digits->begin();idig!=digits->end();++idig) {if(*idig) break;}
  if ( digits->end() == idig ) 
     { return Warning("container " + object->registry()->identifier() + 
                      " contains only NULLs" , StatusCode::SUCCESS ) ;}
  // Interact over the container, if it's not contains NULLs
  for (idig=digits->begin();idig!=digits->end();++idig) {
      /// locate calorimeter 
      const unsigned int calo = (*idig)->id().calo();
      const DeCalorimeter* decalo = calorimeter( calo ) ;
      if( 0 == decalo ) { return Error("could not locate calorimeter" ); } 
      // Build the data-accessor name :
      std::string da_name = "none"; //Should be Ecal, Hcal, Prs, Spd.
      if ( CaloCellCode::CaloNumFromName( "Ecal" ) == int(calo)       ) { da_name = "Ecal"; }
      else if ( CaloCellCode::CaloNumFromName ( "Prs" ) == int(calo)  ) { da_name = "Prs";  }
      else if ( CaloCellCode::CaloNumFromName ( "Spd" ) == int(calo)  ) { da_name = "Spd";  }
      else if ( CaloCellCode::CaloNumFromName ( "Hcal" ) == int(calo) ) { da_name = "Hcal"; }
      da_name = "L0Calo"+da_name+"Digits";
      
      /// create "visualizator" 
      L0CaloDigitRep vis( decalo , da_name);
      
      /// Linear or logarithm ? 
      {
        bool logVis = false ;
        Lib::smanip::tobool( attribute( "CaloLogVis" ) , logVis );
        vis.setLogVis( logVis );
      }
      /// Scale 
      {
        double scale = 5  ;
        Lib::smanip::todouble( attribute( "CaloScale" ) , scale );
        scale *= Gaudi::Units::cm/Gaudi::Units::GeV ;
        vis.setEScale( scale );    
      }
      
      // Representation attributes :
      double r = 0.5, g = 0.5, b = 0.5;
      double hr = 1.0, hg = 1.0, hb = 1.0;
      Lib::smanip::torgb(attribute("modeling.color"),r,g,b);
      Lib::smanip::torgb(attribute("modeling.highlightColor"),hr,hg,hb);
      bool modeling = modelingSolid();
      vis.setSolid(modeling);
      
      SoStyleCache* soStyleCache = soRegion->styleCache(); 
      SoLightModel* lightModel = 0;
      if(modeling) {
        lightModel = soStyleCache->getLightModelPhong();
      } else { //SoLightModel::BASE_COLOR);
        lightModel = soStyleCache->getLightModelBaseColor();
      }
      // Material :
      SoMaterial* highlightMaterial = 
        soStyleCache->getHighlightMaterial(float(r),float(g),float(b),
                                           float(hr),float(hg),float(hb));

      const LHCb::L0CaloCandidate* dig = *idig;
      if( 0 == dig                )  { continue ; }

      SoSeparator* separator = vis(dig);
      if( separator ) { 

	//  Send scene graph to the viewing region 
	// (in the "dynamic" sub-scene graph) :

        separator->insertChild(highlightMaterial,0);
        separator->insertChild(lightModel,0);

        region_addToDynamicScene(*soRegion,separator);
      }
      else { Error("createRep: SoSeparator* points to NULL!, skip it!") ; } 

  }
  ///
  return StatusCode::SUCCESS;
  ///
} 

// ============================================================================
/// locate the calorimeter //////
// ============================================================================
StatusCode SoL0CaloDigitCnv::locateCalo( const unsigned int calo )
{
  ///
  if( ( calo < m_calos.size() ) && 
      ( 0 != m_calos[calo]    )    ) { return StatusCode::SUCCESS; }   
  ///
  if( 0 == detSvc() ) { return Error("IDataProviderSvc* points to NULL!"); } 
  ///
  const std::string caloName( "/dd/Structure/LHCb/DownstreamRegion/" + 
                              CaloCellCode::CaloNameFromNum( calo ) ); 
  ///
  SmartDataPtr<DeCalorimeter> det( detSvc() , caloName );
  if( !det ) { return Error("could not locate \""+caloName +"\"") ; }
  ///
  while( !( calo < m_calos.size() ) ){  m_calos.push_back( 0 ); } 
  ///
  m_calos[calo] = det; 
  ///
  return StatusCode::SUCCESS;
  ///
}

// ============================================================================
/// calorimeter 
// ============================================================================
const DeCalorimeter* SoL0CaloDigitCnv::calorimeter( const unsigned int calo )  
{
  ///
  StatusCode sc = locateCalo( calo ); 
  if( sc.isFailure() ) { return 0 ; } 
  ///
  return *( m_calos.begin() + calo );
  ///  
}
std::string
SoL0CaloDigitCnv::attribute ( const std::string& att ) const
{
  if( 0 == uiSvc() ) return "";
  std::string value;
  uiSvc()->session()->parameterValue( att,value );
  return value;
}
bool 
SoL0CaloDigitCnv::modelingSolid ( ) const
{
  if( 0 == uiSvc() ) return true;
  std::string value;
  if(!uiSvc()->session()->parameterValue( "modeling.modeling",value )) return true;
  return (value=="solid" ? true : false); 
}

// ============================================================================
/// the end 
// ============================================================================
