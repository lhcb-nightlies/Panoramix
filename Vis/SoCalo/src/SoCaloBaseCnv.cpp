// ============================================================================
// include
//
#include <OnXSvc/Win32.h>
//
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/Stat.h"
#include "GaudiKernel/System.h"
#include "GaudiKernel/IChronoStatSvc.h"
//
#include "OnXSvc/ISoConversionSvc.h"
#include "OnXSvc/IUserInterfaceSvc.h"
#include "OnXSvc/ClassID.h"
//
#include "SoCaloBaseCnv.h"


/** @file SoCaloBaseCnv.cpp
 *
 *  implementation of class SoCaloBaseCnv
 *
 *   @author  Vanya Belyaev
 *   @date    18/04/2001
 */

/// standard constructor
SoCaloBaseCnv::SoCaloBaseCnv ( const CLID& clid ,
                               ISvcLocator* svcLoc )
///
  : Converter( So_TechnologyType , clid , svcLoc )
  , m_soSvc     ( 0 )
  , m_uiSvc     ( 0 )
  , m_chronoSvc ( 0 )
  , m_name      ()
  , m_detSvc    ( 0 )
  , m_detSvcName()
                             ///
{ m_name = System::typeinfoName( typeid( *this ) ); }
/// standard (virtual) destructor
SoCaloBaseCnv::~SoCaloBaseCnv(){}
/// initialize //////////
StatusCode SoCaloBaseCnv::initialize()
{
  /// try to initialize base class
  {
    StatusCode sc = Converter::initialize();
    if( sc.isFailure() )
    { return Error(std::string("initialize(): could not initialize")
                   +" base class Converter ", sc); }
  }
  /// locate needed services:
  {
    if( 0 != m_chronoSvc ) { m_chronoSvc->release() ; m_chronoSvc = 0 ; }
    const std::string tmp("ChronoStatSvc");
    StatusCode sc = serviceLocator()->service( tmp , m_chronoSvc );
    if( sc.isFailure() )
    { return Error("initialize(): could not locate "+tmp, sc); }
    else if( 0 == m_chronoSvc )
    { return Error("initialize(): could not locate "+tmp    ); }
    else
    { m_chronoSvc->addRef() ; }
  }
  /// locate needed services:
  {
    if( 0 != m_soSvc     ) { m_soSvc->release() ; m_soSvc     = 0 ; }
    const std::string tmp("SoConversionSvc");
    StatusCode sc = serviceLocator()->service( tmp , m_soSvc );
    if     ( sc.isFailure() )
    { return Error("initialize(): could not locate "+tmp, sc); }
    else if( 0 == m_soSvc )
    { return Error("initialize(): could not locate "+tmp    ); }
    else
    { m_soSvc->addRef() ; }
    ///
  }
  /// locate needed services:
  {
    if( 0 != m_uiSvc     ) { m_uiSvc->release() ; m_uiSvc     = 0 ; }
    const std::string tmp("OnXSvc");
    StatusCode sc = serviceLocator()->service( tmp , m_uiSvc );
    if     ( sc.isFailure() )
    { return Error("initialize(): could not locate "+tmp, sc); }
    else if( 0 == m_uiSvc )
    { return Error("initialize(): could not locate "+tmp    ); }
    else
    { m_uiSvc->addRef() ; }
    ///
  }
  if( !m_detSvcName.empty() )
  {
    if( 0 != m_detSvc    ) { m_detSvc->release() ; m_detSvc    = 0 ; }
    const std::string tmp( m_detSvcName );
    StatusCode sc = serviceLocator()->service( tmp , m_detSvc );
    if     ( sc.isFailure() )
    { return Error("initialize(): could not locate "+tmp, sc); }
    else if( 0 == m_detSvc )
    { return Error("initialize(): could not locate "+tmp    ); }
    else
    { m_detSvc->addRef() ; }
    ///
  }
  ///
  return StatusCode::SUCCESS;
  ///
}

/// finalize
StatusCode SoCaloBaseCnv::finalize()
{
  /// release used services
  if( 0 != m_detSvc    ) { m_detSvc->release() ; m_detSvc    = 0 ; }
  if( 0 != m_soSvc     ) { m_soSvc->release() ; m_soSvc     = 0 ; }
  if( 0 != m_uiSvc     ) { m_uiSvc->release() ; m_uiSvc     = 0 ; }
  if( 0 != m_chronoSvc ) { m_chronoSvc->release() ; m_chronoSvc = 0 ; }
  /// try to finalize the base class
  {
    StatusCode sc = Converter::finalize();
    if( sc.isFailure() )
    { return Error("finalize(): could not finalize base class Converter ",
                   sc); }
  }
  ///
  return StatusCode::SUCCESS;
  ///
}
/// main method  //////////////////////////////////////////////////////////////
StatusCode SoCaloBaseCnv::createRep( DataObject*            Object    ,
                                     IOpaqueAddress*&    /* Address*/ )
{
  if      ( 0  == Object  )
  { return Error("createRep(): DataObject* points to NULL!"); }
  else if ( 0  == soSvc() )
  { return Error("createRep(): ISoConversionSvc* points to NULL!"); }
  ///
  return StatusCode::SUCCESS;
  ///
}
/// exception print and count
StatusCode  SoCaloBaseCnv::Exception( const std::string    & Message ,
                                      const GaudiException & Excp    ,
                                      const MSG::Level     & level   ,
                                      const StatusCode     & Status )
{
  Stat stat( chronoSvc() , Excp.tag() );
  MsgStream log( msgSvc() , name() + ":"+Excp.tag() );
  log << level << Message << ":" << Excp << endmsg ;
  return  Status;
}
/// print and count exception
StatusCode SoCaloBaseCnv::Exception( const std::string    & Message ,
                                     const std::exception & Excp    ,
                                     const MSG::Level     & level   ,
                                     const StatusCode     & Status )
{
  Stat stat( chronoSvc() , "std::exception" );
  MsgStream log( msgSvc() , name() + ":std::exception" );
  log << level << Message << ":" << Excp.what() << endmsg ;
  return  Status;
}
/// print and count exception
StatusCode SoCaloBaseCnv::Exception( const std::string    & Message ,
                                     const MSG::Level     & level   ,
                                     const StatusCode     & Status )
{
  Stat stat( chronoSvc() , "*UNKNOWN* exception" );
  MsgStream log( msgSvc() , name() + ":UNKNOWN exception" );
  log << level << Message << ": UNKNOWN exception"  << endmsg ;
  return  Status;
}
/// print and count the error
StatusCode  SoCaloBaseCnv::Error( const std::string& Message ,
                                  const StatusCode & Status )
{
  Stat stat( chronoSvc() ,  name() + ":Error" );
  return  Print( Message , MSG::ERROR  , Status  ) ;
}
/// print and count the warning
StatusCode SoCaloBaseCnv::Warning( const std::string& Message ,
                                   const StatusCode & Status )
{
  Stat stat( chronoSvc() ,  name() + ":Warning" );
  return  Print( Message , MSG::WARNING , Status ) ;
}
/// print teh message /////////
StatusCode SoCaloBaseCnv::Print( const std::string& Message ,
                                 const MSG::Level & level   ,
                                 const StatusCode & Status )
{ MsgStream log( msgSvc() , name() );
  log << level << Message << endmsg ; return  Status; }
///////////////////////////////










