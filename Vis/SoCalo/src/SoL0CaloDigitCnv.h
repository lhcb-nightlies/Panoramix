// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.1  2008/09/24 15:12:38  truf
// *** empty log message ***
//
// 
// ============================================================================
#ifndef     SOCALO_SOL0CALODIGITCNV_H 
#define     SOCALO_SOL0CALODIGITCNV_H 1 
// STD and STL 
#include <vector>
#include <string> 
// Calo 
#include "SoCaloBaseCnv.h"

// forward decalarations 
template <class CNV>
class CnvFactory;
class DeCalorimeter; 

/**  @class SoL0CaloDigitCnv  SoL0CaloDigitCnv.h 
 *     
 *   Converter for visualization of  
 *   container of L0CaloDigit objects
 *   
 *   @author  Fernando Rodrigues flfr@if.ufrj.br
 *   @date    13/08/2008
 *
 *   @class based on SoCaloDigitCnv
 *   @author  Vanya Belyaev 
 *   @date    18/04/2001 
 */

class SoL0CaloDigitCnv : public SoCaloBaseCnv 
{
  ///
  //friend class CnvFactory<SoL0CaloDigitCnv>; 
  ///
public:
  ///
  /// standard initialization method 
  virtual StatusCode initialize ();
  /// standard finalization  method 
  virtual StatusCode finalize   ();

  virtual long repSvcType() const; 

  /// the only one essential method 
  virtual StatusCode createRep( DataObject* /* Object */ , 
                                IOpaqueAddress*&  /* Address */ );
  /// Class ID for created object == class ID for this specific converter
  static const CLID&         classID     ();
  /// storage Type 
  static unsigned char storageType () ; 
  ///
  //protected:
  ///
  /// standard constructor 
  SoL0CaloDigitCnv( ISvcLocator* svcLoc ); 
  /// virtual destructor   
  virtual ~SoL0CaloDigitCnv();
  /// helpful methos to save typing 
  StatusCode  locateCalo( const unsigned int calo ); 
  /// accessor to calorimeter 
  const DeCalorimeter* calorimeter( const unsigned int calo );
  ///
private: 
  ///
  /// default constructor is disabled 
  SoL0CaloDigitCnv           (                       ) ;
  /// copy constructor is disabled 
  SoL0CaloDigitCnv           ( const SoL0CaloDigitCnv& ) ;
  /// assignment is disabled 
  SoL0CaloDigitCnv& operator=( const SoL0CaloDigitCnv& ) ; 
  ///
  std::string attribute ( const std::string& att ) const;
  bool modelingSolid ( ) const;
private:
  ///
  /// located calorimeter objects 
  std::vector<const DeCalorimeter*>   m_calos; 
  ///
};



// ============================================================================
#endif  //  SOCALO_SOL0CALODIGITCNV_H 
// ============================================================================
