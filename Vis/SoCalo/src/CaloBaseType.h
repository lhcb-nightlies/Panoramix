// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.8  2008/02/05 13:56:58  gybarran
// G.Barrand : rm OSC_VERSION_16_0 logic
//
// Revision 1.7  2006/12/07 15:05:18  gybarran
// G.Barrand : use SoStyleCache
//
// Revision 1.6  2006/12/07 10:01:07  gybarran
// G.Barrand : handle modeling solid, wire_frame
//
// Revision 1.5  2006/12/07 09:00:35  gybarran
// G.Barrand : optimize iterator and getting of the currentRegion
//
// Revision 1.4  2006/10/24 12:51:08  gybarran
// G.Barrand : prepare OSC-16
//
// Revision 1.3  2006/03/29 20:01:13  gybarran
// *** empty log message ***
//
// Revision 1.2  2002/09/11 07:00:57  barrand
// G.Barrand : update according new event model and OnX/v11
//
// Revision 1.1.1.1  2001/10/17 18:26:06  ibelyaev
// New Package: Visualization of Calorimeter objects 
//
// ============================================================================
#ifndef SOCALO_CALOBASETYPE_H 
#define SOCALO_CALOBASETYPE_H 1
// Include files
#include <string>
#include <exception>
// Gaudi 
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/MsgStream.h"
//
// forward declarations 
class ISvcLocator;
class IDataProviderSvc;
class IMessageSvc;
class IChronoStatSvc;
class IUserInterfaceSvc;
class SoRegion;
class SoSeparator;
class SoStyleCache;
//
class GaudiException;
class DeCalorimeter;

/** @class CaloBaseType CaloBaseType.h
 *  
 *  The base class for all "Lib"-like objects used 
 *     for Calo visualization
 *
 *  @author Ivan Belyaev
 *  @date   09/10/2001
 */

class CaloBaseType
{
public:
  
  /** initialize the base type
   *  @return status code 
   */ 
  virtual StatusCode initialize();
  
  /** finalize the base type
   *  @return status code 
   */ 
  virtual StatusCode finalize  ();
  
  /** return name/type of the object
   *  @return name/type of object 
   */
  virtual std::string name() const ;
protected:
  
  /** Standard constructor
   *  @param TypeName name of type 
   *  @param SvcLoc pointer to Service locator 
   */
  CaloBaseType( const std::string& TypeName , 
                ISvcLocator*       SvcLoc   ) ; 
  
  virtual ~CaloBaseType(); ///< Destructor
  
protected:
  
  /** accessor to the "Lib-type" of the object 
   */
  const std::string& myType    () const { return m_myType    ; }

  /** accessor to Service Locator 
   */
  ISvcLocator*       svcLoc    () const { return m_svcLoc    ; }

  /** accessor to Detector Data Provider  
   */
  IDataProviderSvc*  detSvc    () const { return m_detSvc    ; }

  /** accessor to Event Data Provider  
   */
  IDataProviderSvc*  evtSvc    () const { return m_evtSvc    ; }

  /** accessor to Message Service 
   */
  IMessageSvc*       msgSvc    () const { return m_msgSvc    ; }

  /** accessor to Chrono & Stat Service 
   */
  IChronoStatSvc*    chronoSvc () const { return m_chronoSvc ; }
  
  /** accessor to User Interface 
   */
  IUserInterfaceSvc* uiSvc     () const { return m_uiSvc     ; }
  
  /** accessor to data address
   */ 
  const std::string& data      () const { return m_data      ; }
  
  /** get (string) value of attribute 
   *  @param att attribute name 
   *  @return (string) value of attribute 
   */
  std::string attribute ( const std::string& att ) const;
  bool modelingSolid ( ) const;
  
  /** get current drawing region
   *  @return pointer to current region
   */
  SoRegion*  region() const;
  
  /** get parent node/separator
   *  @param  Node  node/separator region name 
   *  @return parent node/separator 
   */
  SoSeparator*   parent( const std::string& Node ) const;
  
  /** locate the calorimeter detector 
   *  @param Calo index of calorimeter detector 
   *  @return status code
   */
  StatusCode locateCalo( const unsigned int Calo ) const;
  
  /** accessor to calorimeter detector
   *  @param Calo calorimeter index
   *  @return pointer to calorimeter device 
   */
  const DeCalorimeter* calo( const unsigned int Calo ) const ;
   
  /** print and count error message
   *  @param Message error message to be printed 
   *  @param sc   status code 
   *  @return status code 
   */
  StatusCode 
  Error      ( const std::string    & Message , 
               const StatusCode     & sc  = StatusCode::FAILURE ) const ;
  
  /** print and count warning  message
   *  @param msg warning message to be printed 
   *  @param sc   status code 
   *  @return status code 
   */
  StatusCode 
  Warning   ( const std::string    & msg                       ,  
              const StatusCode     & sc  = StatusCode::FAILURE ) const ;
  
  /** print the message
   *  @param msg   message to be printed 
   *  @param lvl  print level  
   *  @param sc   status code 
   *  @return status code 
   */
  StatusCode 
  Print     ( const std::string    & msg                       ,  
              const MSG::Level     & lvl = MSG::INFO           ,
              const StatusCode     & sc  = StatusCode::FAILURE ) const ;
  
  /** exception print and count  
   *  @param msg  message to be printed 
   *  @param exc  previous exception 
   *  @param lvl  print level  
   *  @param sc   status code 
   *  @return status code 
   */
  StatusCode 
  Exception ( const std::string    & msg                        ,   
              const GaudiException & exc                        , 
              const MSG::Level     & lvl = MSG::FATAL           ,
              const StatusCode     & sc  = StatusCode::FAILURE ) const ;

  /** exception print and count  
   *  @param msg  message to be printed 
   *  @param exc  previous exception 
   *  @param lvl  print level  
   *  @param sc   status code 
   *  @return status code 
   */
  StatusCode 
  Exception ( const std::string    & msg                        ,  
              const std::exception & exc                        , 
              const MSG::Level     & lvl = MSG::FATAL           ,
              const StatusCode     & sc  = StatusCode::FAILURE ) const ;
  
  /** exception print and count  
   *  @param msg  message to be printed 
   *  @param lvl  print level  
   *  @param sc   status code 
   *  @return status code 
   */  
  StatusCode 
  Exception ( const std::string    & msg                        ,  
              const MSG::Level     & lvl = MSG::FATAL           ,
              const StatusCode     & sc  = StatusCode::FAILURE ) const ;

protected:
  
  void setMyType    ( const std::string& NewType   ) 
  { m_myType        = NewType    ; } ;
  void setDetSvc    ( const std::string& NewDet    ) 
  { m_detSvcName    = NewDet     ; } ;
  void setEvtSvc    ( const std::string& NewEvt    ) 
  { m_evtSvcName    = NewEvt     ; } ;
  void setMsgSvc    ( const std::string& NewMsg    ) 
  { m_msgSvcName    = NewMsg     ; } ;
  void setChronoSvc ( const std::string& NewChrono ) 
  { m_chronoSvcName = NewChrono  ; } ;
  void setData      ( const std::string& NewData   )
  { m_data          = NewData    ; } ;

  SoStyleCache* styleCache();

private:
  
  CaloBaseType();
  CaloBaseType( const CaloBaseType& );
  CaloBaseType& operator=( const CaloBaseType& );
  
private:
  
  std::string         m_myType        ;
  ISvcLocator*        m_svcLoc        ;
  IDataProviderSvc*   m_detSvc        ;
  IDataProviderSvc*   m_evtSvc        ;
  IMessageSvc*        m_msgSvc        ;
  IChronoStatSvc*     m_chronoSvc     ;
  IUserInterfaceSvc*  m_uiSvc         ;
  
  std::string         m_detSvcName    ;
  std::string         m_evtSvcName    ;
  std::string         m_msgSvcName    ;
  std::string         m_chronoSvcName ;
  std::string         m_uiSvcName     ;

  std::string         m_data          ;
  
  mutable std::vector<const DeCalorimeter*>  m_calos;

  StatusCode          m_init          ;

  SoStyleCache*       m_soStyleCache  ;  
protected:
  SoSeparator*        m_parentNode    ;  
};

// ============================================================================
#endif // SOCALO_CALOBASETYPE_H
// ============================================================================
