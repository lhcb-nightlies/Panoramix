// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.18  2009/05/26 08:32:17  gybarran
// *** empty log message ***
//
// Revision 1.17  2008/07/28 08:13:33  truf
// just to please cmt/cvs, no changes
//
// Revision 1.16  2007/02/02 15:48:29  ranjard
// v8r0 - fix to use new PluginManager
//
// Revision 1.15  2006/12/07 15:09:51  gybarran
// G.Barrand : use SoStyleCache
//
// Revision 1.14  2006/12/07 10:01:07  gybarran
// G.Barrand : handle modeling solid, wire_frame
//
// Revision 1.13  2006/12/07 09:17:38  gybarran
// G.Barrand : optimize : rm Nodes loop ; loop in one pass
//
// Revision 1.12  2006/12/07 09:01:32  gybarran
// G.Barrand : handle coloring and other vis attributes. Have correct data-accessor name for picking
//
// Revision 1.11  2006/03/31 15:38:54  gybarran
// *** empty log message ***
//
// Revision 1.10  2006/03/29 20:01:14  gybarran
// *** empty log message ***
//
// Revision 1.9  2006/03/09 16:30:27  odescham
// v6r2 - migration to LHCb v20r0
//
// Revision 1.8  2006/02/03 09:08:46  gybarran
// G.Barrand : migrate osc_15 : use DYNAMIC_SCENE
//
// Revision 1.7  2004/04/07 08:57:40  gybarran
// *** empty log message ***
//
// Revision 1.6  2004/02/05 09:04:43  gybarran
// modifs for GAUDI_v14r1, LHCB_v15r1
//
// Revision 1.5  2003/11/21 14:17:54  barrand
// *** empty log message ***
//
// Revision 1.4  2002/09/11 07:00:58  barrand
// G.Barrand : update according new event model and OnX/v11
//
// Revision 1.3  2001/12/09 14:36:22  ibelyaev
//  update for newer version of Gaudi
//
// Revision 1.2  2001/11/26 10:11:13  ibelyaev
//  major redesign and update
//
// Revision 1.1  2001/10/21 16:26:48  ibelyaev
// Visualization for CaloClusters is added
// 
// ============================================================================
// include files
// Inventor 
#include "Inventor/nodes/SoSeparator.h"
#include <Inventor/nodes/SoLightModel.h>
// HEPVis :
#include  "HEPVis/nodekits/SoRegion.h"
#include <HEPVis/nodes/SoHighlightMaterial.h>
#include <HEPVis/misc/SoStyleCache.h>
// mandatory for visualization!!!
#include "OnXSvc/Win32.h"
// STD & STL
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
// GaudiKernel
#include  "GaudiKernel/CnvFactory.h" 
#include  "GaudiKernel/DataObject.h"
#include  "GaudiKernel/IRegistry.h"
#include  "GaudiKernel/SmartDataPtr.h"
#include  "GaudiKernel/IDataProviderSvc.h"
#include  "GaudiKernel/SmartRef.h"
// OnXSvc
#include  "OnXSvc/IUserInterfaceSvc.h"
#include  "OnXSvc/ClassID.h"
#include  "OnXSvc/Helpers.h"
// Event 
#include  "Event/CaloCluster.h"
// CaloDet 
#include  "CaloDet/DeCalorimeter.h"
// Local 
#include  "SoCaloClusterCnv.h" 
#include  "CaloClusterRep.h" 

#include <Lib/Interfaces/ISession.h>
#include <Lib/smanip.h>

/** @file SoCaloClusterCnv.cpp
 * 
 *  implementation of SoCaloClusterCnv class 
 *  
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru 
 *  @date   20/10/2001 
 */

// ============================================================================
/// mandatory factory business 
// ============================================================================

DECLARE_CONVERTER_FACTORY(SoCaloClusterCnv)

// ============================================================================
/// standard constructor 
// ============================================================================
SoCaloClusterCnv::SoCaloClusterCnv( ISvcLocator* svcLoc )
  : SoCaloBaseCnv( classID() , svcLoc ) 
  , m_calos()
{  
  setName      ( "SoCaloClusterCnv"  ); 
  setDetSvcName( "DetectorDataSvc"   ); 
}

// ============================================================================
/// destructor 
// ============================================================================
SoCaloClusterCnv::~SoCaloClusterCnv(){}

// ============================================================================
/// initialize
// ============================================================================
StatusCode SoCaloClusterCnv::initialize()
{
  StatusCode sc = SoCaloBaseCnv::initialize(); 
  if( sc.isFailure() ) 
    { return Error("initialize: could not initialize base class!"); } 
  return StatusCode::SUCCESS;
}

// ============================================================================
/// finalize
// ============================================================================
StatusCode SoCaloClusterCnv::finalize()
{ return SoCaloBaseCnv::finalize(); }

long SoCaloClusterCnv::repSvcType() const
{ return i_repSvcType();}

/// ============================================================================
/// Class ID for created object == class ID for this specific converter 
// ============================================================================
const CLID&         SoCaloClusterCnv::classID  () 
{ return LHCb::CaloClusters::classID(); }

// ============================================================================
/// storage Type 
// ============================================================================
unsigned char SoCaloClusterCnv::storageType () 
{ return So_TechnologyType; }

// ============================================================================
// create the representation
// ============================================================================
StatusCode SoCaloClusterCnv::createRep( DataObject*         object     ,
                                        IOpaqueAddress*& /* Address */ )
{
  ///
  typedef LHCb::CaloClusters::const_iterator ClusIt;
  ///
  if( 0 == object  ) 
    { return Error("createRep: DataObject* points to NULL"       );}
  if( 0 == uiSvc() ) 
    { return Error("createRep: IUserInterfaceSvc* points to NULL" );}
  SoRegion* soRegion = uiSvc()->currentSoRegion();
  if(soRegion==0) 
    { return Error("createRep: SoRegion* points to NULL" );}

  const LHCb::CaloClusters* clusters = 
    dynamic_cast<const LHCb::CaloClusters*> ( object ); 
  if( 0 == clusters  ) 
    { return Error("createRep: wrong cast to KeyedContainer<LHCb::CaloCluster, Containers::HashMap>!"); } 
  ///
  
  if( clusters->size() == 0 ) 
  { return Print("container " + object->registry()->identifier() + 
                 " is empty", MSG::INFO , StatusCode::SUCCESS ) ;}


  ///
  /// locate the calorimeter (needed for digits)
  const DeCalorimeter* decalo = 0 ;
  for( ClusIt it = clusters->begin() ; clusters->end() != it ; ++it )
    {
      const LHCb::CaloCluster* cl = *it ;
      if( 0 == cl                )  { continue ; }
      std::cout << " cluster " << *cl << std::endl;
      
      for( LHCb::CaloCluster::Digits::const_iterator idig = 
             cl->entries().begin() ; cl->entries().end() != idig ; ++idig )
        {
          const LHCb::CaloDigit* digit = idig->digit() ;
          if( 0 == digit ) { continue ; }
          decalo = calorimeter( digit->cellID().calo() ) ;
          break;
        }
      if( 0 != decalo ) { break; }
    }
  if( 0 == decalo ) 
    { return Error( "could not locate the calorimeter!");}

  // Build the data-accessor name :
  std::string da_name = "none"; //Should be EcalClusters, HcalClusters.
 {std::vector<std::string> words;
  Lib::smanip::words(object->registry()->identifier(),"/",words);
  if(words.size()>=1) da_name = words[words.size()-1];}

  /// create the visualizator
  CaloClusterRep vis( decalo , da_name );

  ///
  /// E or Et ?
  {
    bool etVis = false ;
    Lib::smanip::tobool         ( attribute( "CaloEtVis"         ) , etVis         );
    vis.setEtVis         ( etVis         );
  }
  /// Linear or logarithm ? 
  {
    bool logVis = false ;
    Lib::smanip::tobool         ( attribute( "CaloLogVis"        ) , logVis        );
    vis.setLogVis        ( logVis        );
  }
  /// Scale 
  {
    double scale = 5  ;
    Lib::smanip::todouble       ( attribute( "CaloScale"         ) , scale         );
    scale *= Gaudi::Units::cm/Gaudi::Units::GeV ;
    vis.setEScale        ( scale         );    
  }
  /// visualize the digits? 
  {  
    bool visDigits = true ;
    Lib::smanip::tobool         ( attribute( "CaloVisDigits"     ) , visDigits     );
    vis.setVisDigits     ( visDigits     );
  }
  /// visualize shared digits ?
  {  
    bool visShared = false ;
    Lib::smanip::tobool         ( attribute( "CaloVisShared"     ) , visShared     );
    vis.setVisShared     ( visShared     );
  } 
  /// visualize the structure ?
  {  
    bool visStructure = false ;
    Lib::smanip::tobool         ( attribute( "CaloVisStructure"  ) , visStructure  );
    vis.setVisStructure  ( visStructure  );
  }
  /// visualize the covariance ?
  {  
    bool visCovariance = false ;
    Lib::smanip::tobool         ( attribute( "CaloVisCovariance" ) , visCovariance );
    vis.setVisCovariance ( visCovariance );
  }
  /// visualize the cluster spread ? 
  {  
    bool visSpread = false ;
    Lib::smanip::tobool         ( attribute( "CaloVisSpread"     ) , visSpread     );
    vis.setVisSpread     ( visSpread     );
  }
  /// visualize the spread prism ? 
  {  
    bool visPrism = false ;
    Lib::smanip::tobool         ( attribute( "CaloVisPrism"      ) , visPrism      );
    vis.setVisPrism      ( visPrism      );
  }

  // Representation attributes :
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 1.0;
  Lib::smanip::torgb(attribute("modeling.color"),r,g,b);
  Lib::smanip::torgb(attribute("modeling.highlightColor"),hr,hg,hb);
  bool modeling = modelingSolid();
  vis.setSolid(modeling);

  SoStyleCache* soStyleCache = soRegion->styleCache(); 
  SoLightModel* lightModel = 0;
  if(modeling) {
    lightModel = soStyleCache->getLightModelPhong();
  } else { //SoLightModel::BASE_COLOR);
    lightModel = soStyleCache->getLightModelBaseColor();
  }
  // Material :
  SoMaterial* highlightMaterial = 
    soStyleCache->getHighlightMaterial(float(r),float(g),float(b),
                                       float(hr),float(hg),float(hb));

  for( ClusIt it = clusters->begin() ; clusters->end() != it ; ++it ) {
      const LHCb::CaloCluster* cl = *it;
      if( 0 == cl                )  { continue ; }

      SoSeparator* separator = vis(cl);
      if( separator ) { 
	//  Send scene graph to the viewing region 
	// (in the "dynamic" sub-scene graph) :

        separator->insertChild(highlightMaterial,0);
        separator->insertChild(lightModel,0);

        region_addToDynamicScene(*soRegion,separator);
      }
      else { 
	Error("createRep: SoSeparator* points to NULL! skip it!");
      }
    }
  ///
  return StatusCode::SUCCESS;
}

// ============================================================================
/// locate the calorimeter //////
// ============================================================================
StatusCode SoCaloClusterCnv::locateCalo( const unsigned int calo )
{
  ///
  if( ( calo < m_calos.size() ) && 
      ( 0 != m_calos[calo]    )    ) { return StatusCode::SUCCESS; }   
  ///
  if( 0 == detSvc() ) { return Error("IDataProviderSvc* points to NULL!"); } 
  ///
  const std::string caloName( "/dd/Structure/LHCb/DownstreamRegion/" + 
                              CaloCellCode::CaloNameFromNum( calo ) ); 
  ///
  SmartDataPtr<DeCalorimeter> det( detSvc() , caloName );
  if( !det ) { return Error("could not locate "+caloName ) ; }
  ///
  while( !( calo < m_calos.size() ) ){  m_calos.push_back( 0 ); } 
  ///
  m_calos[calo] = det; 
  ///
  return StatusCode::SUCCESS;
  ///
}

// ============================================================================
/// calorimeter 
// ============================================================================
const DeCalorimeter* SoCaloClusterCnv::calorimeter( const unsigned int calo )  
{
  ///
  StatusCode sc = locateCalo( calo ); 
  if( sc.isFailure() ) { return 0 ; } 
  ///
  return *( m_calos.begin() + calo );
  ///  
}

std::string
SoCaloClusterCnv::attribute ( const std::string& att ) const
{
  if( 0 == uiSvc() ) return "";
  std::string value;
  uiSvc()->session()->parameterValue( att,value );
  return value;
}
bool 
SoCaloClusterCnv::modelingSolid ( ) const
{
  if( 0 == uiSvc() ) return true;
  std::string value;
  if(!uiSvc()->session()->parameterValue( "modeling.modeling",value )) 
    return true;
  return (value=="solid" ? true : false); 
}

// ============================================================================
/// the end 
// ============================================================================



