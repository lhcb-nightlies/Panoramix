// $Id: L0CaloDigitRep.cpp,v 1.2 2009-05-26 08:32:17 gybarran Exp $ 
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.1  2008/09/24 15:12:37  truf
// *** empty log message ***
//
// 
// ============================================================================
#define    SOCALO_L0CALODIGITREP_CPP 1 
//
#include <cmath> 
//
// GaudiKernel
#include "GaudiKernel/Point3DTypes.h"
// Inventor
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoCoordinate3.h>
//#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoRotation.h>
//
#include <HEPVis/nodes/SoSceneGraph.h>
#include <HEPVis/nodes/SoTextTTF.h>
#include <HEPVis/SbPolyhedron.h>
#include <HEPVis/nodes/SoPolyhedron.h>
//
#include <OnXSvc/Win32.h>
// Event 
#include "Event/CaloDigit.h" 
#include "Event/CaloClusterEntry.h"
// CaloDet
#include "CaloDet/DeCalorimeter.h" 
//local 
#include "L0CaloDigitRep.h" 
///

#ifdef WIN32
#include <sstream>
#endif

// ============================================================================
/** @file L0CaloDigitRep.cpp
 *  implementation of class L0CaloDigitRep 
 *
 *  @author Fernando Rodrigues flfr@if.ufrj.br 
 *  @date   14/08/2008 
 *
 *  Based on file CaloDigitRep.cpp
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru 
 *  @date   19/04/2001 
 */
// ============================================================================


// ============================================================================
/** standard constructor
 *  @param DeCalo  pointer to DeCalorimeter object 
 *                 (source of geometry  information)
 *  @param Type    "Object Type" for picking 
 *  @param EScale  drawing scale
 *  @param EtVis   flag for drawing the transverse energy 
 *  @param LogVis  flag for logarithmic drawing
 *  @param TxtVis  flag for drawing the text 
 *  @param TxtSize text size 
 *  @param TxtPos  text position
 */ 
// ============================================================================
L0CaloDigitRep::L0CaloDigitRep( const DeCalorimeter*   DeCalo  ,
                                const std::string&     Type    ,
			        const double           EScale  , 
			        const bool             EtVis   , 
			        const bool             LogVis  , 
			        const bool             TxtVis  , 
			        const double           TxtSize , 
			        const double           TxtPos  ) 
  : m_type    ( Type    )  ///< "Type" for picking
  , m_deCalo  ( DeCalo  )  ///< DeCalorimeter object 
  , m_eScale  ( EScale  )  ///< scale for drawing 
  , m_txtSize ( TxtSize )  ///< text size 
  , m_txtPos  ( TxtPos  )  ///< text position 
  , m_etVis   ( EtVis   )  ///< flag for visualisation of E or Et 
  , m_logVis  ( LogVis  )  ///< linear or logarithmic scale 
  , m_txtVis  ( TxtVis  )  ///< flag for text visualization 
  , m_solid   ( true    )
{}

// ============================================================================
/** destructor 
 */
// ============================================================================
L0CaloDigitRep::~L0CaloDigitRep(){}

// ============================================================================
// ============================================================================
SoSeparator* L0CaloDigitRep::operator() ( 
 const LHCb::L0CaloCandidate* digit 
,const std::string& pickName
) const 
{
  ///  if detector or digit is NULL  drawing is impossible 
  if(  0 == deCalo() || 0 == digit  ) { return 0 ; }  ///< RETURN !!!  
  ///
  double et  = 0 ; 
  /// use transverse energy (if requested) 
  
  et = digit->et() ;  
  /// use logarithm (if requested), need to tune constants !!!
  if( logVis() ) 
    {
      if( et <= 0      ) { return 0 ; }  ///< drawing is impossible 
      et = log10 ( et ) ;
    }
  /// scale the "energy" 
  m_size = et * eScale() ; 
  /// create the box  
  const double cellsize = 
    ( deCalo()->cellSize( digit->id() ) ) * 90 * Gaudi::Units::perCent;  
  //SoCube* box = new SoCube();
  //box->width  = cellsize      ;
  //box->height = cellsize      ; 
  //box->depth  = abs( size() ) ; 
  // G.Barrand : Instead of an SoCube, have 
  //  an SoPolyhedron (to master solid/wireframe) :
  SoPolyhedron* box = 
    new SoPolyhedron(SbPolyhedronBox(cellsize/2.,
				     cellsize/2.,
				     fabs(size())/2.));
  box->solid.setValue(m_solid?TRUE:FALSE);
  
  /// position of the box 
  const double x( deCalo()->cellX( digit->id() )              ) ; 
  const double y( deCalo()->cellY( digit->id() )              ) ; 
  const double z( deCalo()->cellZ( digit->id() ) + size()/2.0 ) ; 
  
  /// create the translation and rotation 
  SoTranslation* translation = new SoTranslation;
  translation->translation   =     SbVec3f( (float)x , (float)y , (float)z );
  /// SoRotation*    rotation    = new SoRotation;
  /// rotation->rotation         =     SbRotation( SbVec3f( 0 , 0 , 1 ) , 0 );
  /// create separaror and decorate it 
  SoSceneGraph* separator     = new SoSceneGraph();

  // Build picking string id :
  if( !type().empty() ) 
    {
      char* sid = new char[type().size()+32];
      ::sprintf(sid,"%s/0x%lx",m_type.c_str(),(unsigned long)digit);
      separator->setString(sid);
      delete [] sid;
    } 
  else if(pickName!="") // A ClusterRep may force its name on a digit.
    {
      separator->setString(pickName.c_str());
    }

  separator->addChild( translation ) ;
  /// separator->addChild( rotation    ) ; 
  separator->addChild( box         ) ;  
  ///
  if( txtVis() )
    { 
      ///
      SoLightModel* lightModel   = new SoLightModel;
      lightModel->model   = SoLightModel::BASE_COLOR;
      separator->addChild( lightModel);
      ///
      SoDrawStyle*  drawStyle    = new SoDrawStyle;
      drawStyle->style.setValue( SoDrawStyle::LINES );
      drawStyle->linePattern.setValue( 0xFFFF );
      separator->addChild( drawStyle );
      /// text = id! 
#if defined (__GNUC__) && ( __GNUC__ <= 2 )
      std::ostrstream str;
#else
      std::ostringstream str;
#endif
      str << digit->id() << digit->et()/Gaudi::Units::GeV << "GeV";
#if defined (__GNUC__) && ( __GNUC__ < 3 )
      str.freeze(); // What is the equivalent in 3.2 ?
#endif
      const std::string tmp( str.str() );
      SbVec3f txtPoint( (float)x , (float)y , (float)(z - et/2 + txtPos()) ); 
      ////
      SoSeparator* sepText = new SoSeparator;
      SoTransform* tsf = new SoTransform;
      tsf->translation.setValue(txtPoint);
      ///
      SoTextTTF* text = new SoTextTTF();
      text->size.setValue( (int) txtSize() ); //Default is 10.
      text->string.set1Value(0,tmp.c_str());
      ///
      sepText->addChild(tsf);
      sepText->addChild(text);
      /// add text to separator 
      separator->addChild(sepText);
    }
  ///
  return separator;
  ///
}

// ============================================================================

