// ============================================================================
// CVS tag $Name: not supported by cvs2svn $ 
// ============================================================================
// $Log: not supported by cvs2svn $
// Revision 1.26  2009/05/26 08:02:27  gybarran
// *** empty log message ***
//
// Revision 1.25  2008/07/28 08:13:33  truf
// just to please cmt/cvs, no changes
//
// Revision 1.24  2008/02/22 13:25:01  gybarran
// G.Barrnd : Linux : rm some warnings
//
// Revision 1.23  2007/02/02 15:48:29  ranjard
// v8r0 - fix to use new PluginManager
//
// Revision 1.22  2007/01/17 07:07:48  truf
// *** empty log message ***
//
// Revision 1.21  2006/12/07 15:09:52  gybarran
// G.Barrand : use SoStyleCache
//
// Revision 1.20  2006/12/07 10:01:07  gybarran
// G.Barrand : handle modeling solid, wire_frame
//
// Revision 1.19  2006/12/07 09:17:38  gybarran
// G.Barrand : optimize : rm Nodes loop ; loop in one pass
//
// Revision 1.18  2006/12/07 09:01:32  gybarran
// G.Barrand : handle coloring and other vis attributes. Have correct data-accessor name for picking
//
// Revision 1.17  2006/03/31 15:38:54  gybarran
// *** empty log message ***
//
// Revision 1.16  2006/03/29 20:01:14  gybarran
// *** empty log message ***
//
// Revision 1.15  2006/03/09 16:30:27  odescham
// v6r2 - migration to LHCb v20r0
//
// Revision 1.14  2006/02/03 09:08:46  gybarran
// G.Barrand : migrate osc_15 : use DYNAMIC_SCENE
//
// Revision 1.13  2004/04/07 08:57:41  gybarran
// *** empty log message ***
//
// Revision 1.12  2004/02/05 09:04:43  gybarran
// modifs for GAUDI_v14r1, LHCB_v15r1
//
 // Revision 1.11  2003/11/21 14:17:55  barrand
// *** empty log message ***
//
// Revision 1.10  2003/06/24 14:05:40  barrand
// *** empty log message ***
//
// Revision 1.9  2002/09/11 07:56:35  barrand
// *** empty log message ***
//
// Revision 1.8  2002/09/11 07:00:58  barrand
// G.Barrand : update according new event model and OnX/v11
//
// Revision 1.7  2001/12/09 14:36:22  ibelyaev
//  update for newer version of Gaudi
//
// Revision 1.6  2001/10/21 16:26:48  ibelyaev
// Visualization for CaloClusters is added
//
// Revision 1.5  2001/10/19 07:27:47  barrand
// Deposit an id on the objects rep in order to have feedback picking
//
// Revision 1.4  2001/10/18 13:45:51  ibelyaev
//  update for Win2K
//
// Revision 1.3  2001/10/18 13:09:03  ibelyaev
//  update for new access to So_TechnologyType variable
//
// Revision 1.2  2001/10/18 13:03:57  ibelyaev
//  code update for Win2K
//
// Revision 1.1.1.1  2001/10/17 18:26:07  ibelyaev
// New Package: Visualization of Calorimeter objects 
// 
// ============================================================================
// Inventor
#include  "Inventor/nodes/SoSeparator.h"
#include <Inventor/nodes/SoLightModel.h>
// HEPVis :
#include  "HEPVis/nodekits/SoRegion.h"
#include <HEPVis/nodes/SoHighlightMaterial.h>
#include <HEPVis/misc/SoStyleCache.h>
// OnXSvc
#include  "OnXSvc/Win32.h"
// STD & STL
#include <string>
#include <vector>
#include <algorithm>
#include <functional>
// GaudiKernel
#include  "GaudiKernel/CnvFactory.h" 
#include  "GaudiKernel/IRegistry.h"
#include  "GaudiKernel/DataObject.h"
#include  "GaudiKernel/IDataProviderSvc.h"
#include  "GaudiKernel/SmartDataPtr.h"
// OnXSvc
#include  "OnXSvc/IUserInterfaceSvc.h"
#include  "OnXSvc/ClassID.h"
#include  "OnXSvc/Helpers.h"
// CaloEvent 
#include  "Event/CaloDigit.h"
#include  "Kernel/CaloCellID.h"
// CaloDet 
#include  "CaloDet/DeCalorimeter.h"
// Local 
#include  "SoCaloDigitCnv.h" 
#include  "CaloDigitRep.h" 

#include <Lib/Interfaces/ISession.h>
#include <Lib/smanip.h>

/** @file SoCaloDigitCnv.cpp
 * 
 *  implementation of SoCaloDigitCnv class 
 *  
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru 
 *  @date   18/04/2001 
 */

// ============================================================================
/// mandatory factory business 
// ============================================================================
DECLARE_CONVERTER_FACTORY(SoCaloDigitCnv)

// ============================================================================
/// standard constructor 
// ============================================================================
SoCaloDigitCnv::SoCaloDigitCnv( ISvcLocator* svcLoc )
  : SoCaloBaseCnv( classID() , svcLoc ) 
  , m_calos()
{  
  setName      ( "SoCaloDigitCnv"  ); 
  setDetSvcName( "DetectorDataSvc" ); 
}

// ============================================================================
/// standard desctructor 
// ============================================================================
SoCaloDigitCnv::~SoCaloDigitCnv(){}

// ============================================================================
/// initialize 
// ============================================================================
StatusCode SoCaloDigitCnv::initialize()
{
  StatusCode sc = SoCaloBaseCnv::initialize(); 
  if( sc.isFailure() ) 
    { return Error("initialize: could not initialize base class!"); } 
  return StatusCode::SUCCESS;
}

// ============================================================================
/// finalize 
// ============================================================================
StatusCode SoCaloDigitCnv::finalize()
{ return SoCaloBaseCnv::finalize(); } 

long SoCaloDigitCnv::repSvcType() const
{ return i_repSvcType();}

// ============================================================================
/// Class ID for created object == class ID for this specific converter 
// ============================================================================
const CLID&         SoCaloDigitCnv::classID  () { 
  return LHCb::CaloDigits::classID();
} 

// ============================================================================
/// storage Type 
// ============================================================================
unsigned char SoCaloDigitCnv::storageType () 
{ return So_TechnologyType; }  

// ============================================================================
/// the only one essential method 
// ============================================================================
StatusCode SoCaloDigitCnv::createRep( DataObject*         object     ,
				      IOpaqueAddress*& /* Address */ )
{
  ///
  typedef LHCb::CaloDigits::const_iterator DigIt;
  ///
  if( 0 == object  ) 
    { return Error("createRep: DataObject* points to NULL"       );}
  if( 0 == uiSvc() ) 
    { return Error("createRep: IUserInterfaceSvc* points to NULL" );}
  SoRegion* soRegion = uiSvc()->currentSoRegion();
  if(soRegion==0) 
    { return Error("createRep: SoRegion* points to NULL" );}
  const LHCb::CaloDigits* digits = 
    dynamic_cast<const LHCb::CaloDigits*> ( object ); 
  if( 0 == digits  ) 
    { return Error("createRep: wrong cast to KeyedContainer<LHCb::CaloDigit,Containers::HashMap>!"); } 
  ///
  if( digits->size() == 0) 
  { return Print("container " + object->registry()->identifier() +
                 " is empty", MSG::INFO , StatusCode::SUCCESS ) ;}

  /// find corresponding de-calorimeter 
  DigIt idig;
  for(idig=digits->begin();idig!=digits->end();++idig) {if(*idig) break;}
  if( digits->end() == idig ) 
  { return Warning("container " + object->registry()->identifier() + 
                   " contains only NULLs" , StatusCode::SUCCESS ) ;}

  /// locate calorimeter 
  const unsigned int calo = (*idig)->cellID().calo();
  const DeCalorimeter* decalo = calorimeter( calo ) ;
  if( 0 == decalo ) { return Error("could not locate calorimeter" ); } 
  // Build the data-accessor name :
  std::string da_name = "none"; //Should be Ecal, Hcal, Prs, Spd.
 
  if ( CaloCellCode::CaloNumFromName( "Ecal" ) == int(calo) )  {
  // we are in Ecal
    da_name = "Ecal";
  }
  else if ( CaloCellCode::CaloNumFromName ( "Prs" ) == int(calo) ) {  
    // we are in Prs 
    da_name = "Prs";
  }
  else if ( CaloCellCode::CaloNumFromName ( "Spd" ) == int(calo) ) {  
    // we are in Spd 
    da_name = "Spd";
  }
  else if ( CaloCellCode::CaloNumFromName ( "Hcal" ) == int(calo) ) {  
    // we are in Hcal 
    da_name = "Hcal";
  }
  da_name = da_name+"Digits";
  

  
  /// create "visualizator" 
  CaloDigitRep vis( decalo , da_name);

  /// E or Et ?
  {
    bool etVis = false ;
    Lib::smanip::tobool( attribute( "CaloEtVis" ) , etVis );
    vis.setEtVis( etVis );
  }
  /// Linear or logarithm ? 
  {
    bool logVis = false ;
    Lib::smanip::tobool( attribute( "CaloLogVis" ) , logVis );
    vis.setLogVis( logVis );
  }
  /// Scale 
  {
    double scale = 5  ;
    Lib::smanip::todouble( attribute( "CaloScale" ) , scale );
    scale *= Gaudi::Units::cm/Gaudi::Units::GeV ;
    vis.setEScale( scale );    
  }

  // Representation attributes :
  double r = 0.5, g = 0.5, b = 0.5;
  double hr = 1.0, hg = 1.0, hb = 1.0;
  Lib::smanip::torgb(attribute("modeling.color"),r,g,b);
  Lib::smanip::torgb(attribute("modeling.highlightColor"),hr,hg,hb);
  bool modeling = modelingSolid();
  vis.setSolid(modeling);

  SoStyleCache* soStyleCache = soRegion->styleCache(); 
  SoLightModel* lightModel = 0;
  if(modeling) {
    lightModel = soStyleCache->getLightModelPhong();
  } else { //SoLightModel::BASE_COLOR);
    lightModel = soStyleCache->getLightModelBaseColor();
  }
  // Material :
  SoMaterial* highlightMaterial = 
    soStyleCache->getHighlightMaterial(float(r),float(g),float(b),
                                       float(hr),float(hg),float(hb));

 {for(DigIt idig=digits->begin();idig!=digits->end();++idig) {
      const LHCb::CaloDigit* dig = *idig;
      if( 0 == dig                )  { continue ; }

      SoSeparator* separator = vis(dig);
      if( separator ) { 

        separator->insertChild(highlightMaterial,0);
        separator->insertChild(lightModel,0);

        region_addToDynamicScene(*soRegion,separator);
      }
      else { Error("createRep: SoSeparator* points to NULL!, skip it!") ; } 

   }}
  ///
  return StatusCode::SUCCESS;
  ///
}

// ============================================================================
/// locate the calorimeter //////
// ============================================================================
StatusCode SoCaloDigitCnv::locateCalo( const unsigned int calo )
{
  ///
  if( ( calo < m_calos.size() ) && 
      ( 0 != m_calos[calo]    )    ) { return StatusCode::SUCCESS; }   
  ///
  if( 0 == detSvc() ) { return Error("IDataProviderSvc* points to NULL!"); } 
  ///
  const std::string caloName( "/dd/Structure/LHCb/DownstreamRegion/" + 
                              CaloCellCode::CaloNameFromNum( calo ) ); 
  ///
  SmartDataPtr<DeCalorimeter> det( detSvc() , caloName );
  if( !det ) { return Error("could not locate \""+caloName +"\"") ; }
  ///
  while( !( calo < m_calos.size() ) ){  m_calos.push_back( 0 ); } 
  ///
  m_calos[calo] = det; 
  ///
  return StatusCode::SUCCESS;
  ///
}

// ============================================================================
/// calorimeter 
// ============================================================================
const DeCalorimeter* SoCaloDigitCnv::calorimeter( const unsigned int calo )  
{
  ///
  StatusCode sc = locateCalo( calo ); 
  if( sc.isFailure() ) { return 0 ; } 
  ///
  return *( m_calos.begin() + calo );
  ///  
}
std::string
SoCaloDigitCnv::attribute ( const std::string& att ) const
{
  if( 0 == uiSvc() ) return "";
  std::string value;
  uiSvc()->session()->parameterValue( att,value );
  return value;
}
bool 
SoCaloDigitCnv::modelingSolid ( ) const
{
  if( 0 == uiSvc() ) return true;
  std::string value;
  if(!uiSvc()->session()->parameterValue( "modeling.modeling",value )) return true;
  return (value=="solid" ? true : false); 
}

// ============================================================================
/// the end 
// ============================================================================
