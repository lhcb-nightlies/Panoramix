import os,getpass

host  = os.environ['HOST']
local = host.find('pctommy')>-1
if local : 
  path = '/media/Work'
else :
  path = '/tmp/'+getpass.getuser()

prodids = [5799,5800,5801]

allfiles = os.listdir(path)
allfiles.sort()
for prod in prodids:
  filesize = 0
  files = []
  for f in allfiles : 
   islast   = allfiles.index(f) == len(allfiles) - 1
   goodfile = f.find('_2.dst')>-1 and f.find('0000'+str(prod))>-1 
   if goodfile or islast: 
    if goodfile:
     files.append(path+'/'+f)
     lrun = f.split('_')[1]
     nrf = len(files)-1
     filesize+=os.path.getsize(files[nrf])
    if filesize > 2E09 or islast : 
     nw = files[0].replace('_2.dst','_'+lrun+'_2.dst')    
     filesize = 0
     sfiles = '"'
     for ff in files: sfiles+=ff+' '
     sfiles+='"'
     infiles = sfiles.replace(' "','"')
     os.system('python $GAUDIPYTHONTUTORIAL/MergeFiles.py '+infiles+' '+nw)
     files = []
