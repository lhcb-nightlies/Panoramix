#! /usr/bin/env python

import GaudiPython
appMgr     = GaudiPython.AppMgr()
tistostool = appMgr.toolsvc().create('TriggerTisTos',interface='ITriggerTisTos')

evt = appMgr.evtsvc()
# define a helper class:                  
class irange(object) :                    
   def __init__(self, b, e ):             
     self.begin, self.end  = b, e         
   def __iter__(self):                    
     it = self.begin                      
     while it != self.end :               
       yield it.__deref__()               
       it.__postinc__(1)                  

def info(name):
    """ returns a function to retrieve the info from the object
    """
    varname = name.split(",")[0]
    id = HLTSUM.confInt("InfoID/"+varname)
    def fun(x):
        return x.info(id,-1)
    return fun

def candidates(selection):
    """ return the candidates of a selection (tracks or vertices)
    """
    if (not HLTSUM.hasSelection(selection)): return None
    type = HLTSUM.confString(selection+"/SelectionType")
    if (type == "Track"):
        return HLTSUM.selectionTracks(selection)
    elif (type == "Vertex"):
        return HLTSUM.selectionVertices(selection)
    return None



def l0():
    """ how to get the l0 decision?
        how to get the l0 decision per channel (i.e hadron high threshold)
        hot to get the l0 decision per condition (i.e SPD multiplicity)
    """
    l0 = evt["Trig/L0/L0DUReport"]
    print " L0 decision ",l0.decision()
                 

def l0_values():
 L0DUReport = evt['Trig/L0/L0DUReport']
 if not L0DUReport : return                                
 L0DUConfig = L0DUReport.configuration()                                 
 if not L0DUConfig : return                                
 cond = L0DUConfig.conditions()                                          
 datas      = L0DUConfig.data()                                          
 for x in irange(datas.begin(),datas.end()) :                            
   idata  = x.second                                                      
   name   = idata.name()                                                   
   digit  = idata.digit()                                                
   print  'L0DU Data : ',name,'has value (digit unit)',digit             
   
def l0_candidates():
    """ hot to get the l0 candidates?
    """
    calos = evt["Trig/L0/FullCalo"]
    if (calos):
        hadcalos = filter(lambda calo: (calo.type() == 2),calos)
        print " L0 hadron calo candidates ",len(hadcalos)
        
    muons = evt["Trig/L0/MuonCtrl"]
    if (muons):
        print " L0 muon candidates ",muons.size()

def hlt_decisions(l=1) :
  print '%40s %20s %20s'%('  Selection name ', 'Decision', 'from TisTosTool')
  location = 'Hlt'+str(l)+'/DecReports'
  for s in evt[location].decisionNames() : 
    print '%40s %20s %20s'%(s,str(evt[location].decReport(s).decision()),
           str(tistostool.selectionTisTos(s).decision()))

def tistos_selections() :
 for s in tistostool.triggerSelectionNames('*'): print s

def hlt():
    """ how to get the hlt decision?
        how to get the decision of a selection?
        how to get the selections that an event has passed?
    """
    print " HLT decision ", evt['Hlt/DecReports'].decReport('Hlt1Global').decision()
    names = ["L0HadronsDecision","Hlt1HadronSingleDecision","Hlt1HadronDiDecision"]
    for name in names:
        print " \t",name," ? ",evt['Hlt/DecReports'].decReport(name).decision()

    names =  evt['Hlt/SelReports'].selectionNames()
    print " HLT selections passed:"
    for name in names: print " \t",name

def hlt_selection_configuration(selection):
    """ how to get the filters applied in one selection?
        how to get the input selection used for a selection?
        @ param name of the selection, i.e Hlt1HadronSingleDecision 
    """
    filters = HLTSUM.confStringVector(selection+"/Filters")
    print " HLT ",selection ," Filters : "
    for filter in filters: print " \t",filter
    inputs = HLTSUM.confStringVector(selection+"/InputSelections")
    print " HLT ",selection ," Inputs : "
    for input in inputs: print " \t",input

def hlt_candidates_info(selection = "Hlt1HadronSingleDecision"):
    """ how to get the info of the candidates of a selection?
        i.e what is the PT value of the tracks of the HadPreTrigger selection?
        @ param name of the selection, i.e Hlt1HadronSingleDecision
    """
    print " HLT ",selection," ? ",evt['Hlt/DecReports'].decReport(selection).decision()
    if (not HLTSUM.hasSelection(selection)): return
    objs = candidates(selection)
    if (not objs): return
    print " HLT ",selection," candidates ",objs.size()
    filters = HLTSUM.confStringVector(selection+"/Filters")
    for filter in filters:
        xfun = info(filter)
        vars = map(xfun,objs)
        s = ""
        for var in vars: s = s+" "+str(var)
        print "\t",filter," : ",s

def hlt_vertex_reports() :
 sels =  evt['Hlt/VertexReports'].selectionNames()
 for s in sels:
     for v in evt['Hlt/VertexReports'].vertexReport(s):
        print s, v.target()

def hlt_selection_report():
  for s in evt['Hlt/SelReports'].selectionNames() : 
    print '### selection name ', s
    selectionRep = evt['Hlt/SelReports'].selReport(s) 
    print selectionRep
    if selectionRep :                    
      for candidate in selectionRep.substructure():
       print candidate.target()

def run(n=10):
    """ run 10 events and print info about L0 and HLT
    @param n number of events to run
    """
    for i in range(n):
        print " *** L0 and HLT info *** "
        appMgr.run(1)
        l0()
        # l0_candidates()
        hlt()
        hlt_candidates_info("Hlt1HadronSingleDecision")
        hlt_candidates_info("Hlt1HadronDiDecision")
    print " *** configuration  *** "
    hlt_selection_configuration("Hlt1HadronSingleDecision")
    hlt_selection_configuration("Hlt1HadronDiDecision")


