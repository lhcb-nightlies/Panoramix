from Gaudi.Configuration import * 

from Configurables import GaudiSequencer,  MuonPIDChecker, CombineParticles, FilterDesktop,DecodeRawEvent


debug = False
simulation = False

#-------------------------------------
# -- change the default tracking cuts:
# ------------------------------------
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Types = ["Long","Downstream"]

# Data on Demand:
#----------------
# support for decoding raw data
DecodeRawEvent().DataOnDemand=True
importOptions( "$L0TCK/L0DUConfig.opts" )
from Configurables import ( DataOnDemandSvc ) 
DataOnDemandSvc().Prefix = '/Event/PID/'

from Configurables import MuonRec
MuonRec().RootInTES = '/Event/PID'
#MuonRec().RootOnTES = '/Event/PID'
DataOnDemandSvc().AlgMap["/Event/PID/Raw/Muon/Coords"] = MuonRec()
MuonRec().OutputLevel = WARNING

MySeq = GaudiSequencer("MySequence")
MySeq.IgnoreFilterPassed = True
# Fix to Raw event location
myCopySeq  = GaudiSequencer('MyCopySeq')
MySeq.Members += [ myCopySeq ]
#----------------------------------
# Configure MuonIDAlg
#----------------------------------
from MuonID import ConfiguredMuonIDs
cm = ConfiguredMuonIDs.ConfiguredMuonIDs(data="2011")
MySeq.Members += [ cm.getMuonIDSeq() ]
# If muon PID has rerun, need to re make the Combined DLLS...
from Configurables import ( ChargedProtoParticleAddMuonInfo,
                                ChargedProtoCombineDLLsAlg )
from Configurables import MuonIDAlg
MuonIDAlg().AllMuonTracks = True
MuonIDAlg().OutputLevel = WARNING
MuonIDAlg().RootInTES = '/Event/PID/'

MySeq.Members += [ ChargedProtoParticleAddMuonInfo("CProtoPAddNewMuon"),
                   ChargedProtoCombineDLLsAlg("CProtoPCombDLLNewMuon") ]
ChargedProtoParticleAddMuonInfo("CProtoPAddNewMuon").RootInTES = '/Event/PID/'
ChargedProtoCombineDLLsAlg("CProtoPCombDLLNewMuon").RootInTES = '/Event/PID/'
# Configure the various selections and the corresponding MuonIDNtMaker ntuples

newSel = FilterDesktop("Lam0LLLine2V0ForPIDSel")
newSel.Inputs = ["Phys/Lam0LLLine2V0ForPID/Particles"]
newSel.Code = "ALL"
newSel.RootInTES = '/Event/PID/'
newSel.OutputLevel = WARNING
if debug:
  newSel.OutputLevel = 1
    
MySeq.Members += [ newSel ]

########################################################################
#
# Standard DaVinci configuration
#
from Configurables import DaVinci
DaVinci().PrintFreq = 1000
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"    # Histogram file
DaVinci().TupleFile = "DVNtuples.root"         # Ntuple

if debug:
  DaVinci().PrintFreq = 1
  DaVinci().EvtMax = 1000

DaVinci().DataType  = '2011'
DaVinci().InputType = 'MDST'

# Stripping 17 validation
DaVinci().DDDBtag   = "head-20110914"
DaVinci().CondDBtag = "head-20110914"

MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"

MySeq.MeasureTime = True
DaVinci().UserAlgorithms = [ MySeq ] # Shouldn't be DaVinci().MoniSequence ?
DaVinci().Input = ["DATAFILE='PFN:00012413_00000001_1.pid.mdst' TYP='POOL_ROOTTREE' OPT='READ'"]

import GaudiPython
from GaudiPython.GaudiAlgs import GaudiAlgo
class myRawCopy(GaudiAlgo):
 def execute(self):
  evt = appMgr.evtsvc()
  rc = evt.registerObject('/Event/PID/DAQ/RawEvent',evt['/Event/Muon/RawEvent'])
  return rc

appMgr = GaudiPython.AppMgr()
seq    = appMgr.algorithm('GaudiSequencer/MyCopySeq',True)
seq.Members += [myRawCopy('myRawcopy').name()]

#evt = appMgr.evtsvc()
#appMgr.run(1)
#print evt['PID/Raw/Muon/Coords'].size()
