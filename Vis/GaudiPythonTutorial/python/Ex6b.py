from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"
# use random misaligned velo 
importOptions('VeloAlignment_0.py')
 
appConf = ApplicationMgr(AppName = 'Ex6b', OutputLevel = INFO) 

import GaudiPython

appMgr = GaudiPython.AppMgr()
tsvc   = appMgr.toolsvc()

import GaudiPython
appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
det    = appMgr.detSvc() 
lhcb   = det['/dd/Structure/LHCb']
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst']) 

extrap   = appMgr.toolsvc().create('TrackMasterExtrapolator', interface='ITrackExtrapolator')

State = GaudiPython.gbl.LHCb.State 
s_origin = State()
# create a state ot origin with slow tx=0.1, ty=-0.1 and p = 5000 MeV/c
s_origin.setState(0.,0.,0.,0.1,-0.1,1./5000.)
s_extrap = s_origin.clone()
# extrapolate to z=12m
result = extrap.propagate(s_extrap,12000.) 
print 'extrapolated state: x,y,z   = %8.2f, %8.2f, %8.2f'%(s_extrap.x(),s_extrap.y(),s_extrap.z())
print 'extrapolated state: tx,ty,p = %8.2f, %8.2f, %8.2f'%(s_extrap.tx(),s_extrap.ty(),s_extrap.p())

# study residual field in Velo region:
magSvc = appMgr.service('MagneticFieldSvc','ILHCbMagnetSvc')
magSvc.fieldGrid().setScaleFactor(1.)

def curv(z0,z1,p):
 s_origin.setState(0.,0.,z0,0.0,0.0,1./p)
 s_extrap = s_origin.clone()
 result = extrap.propagate(s_extrap,z1)
 print 'extrapolated state: x,y,z   = %8.2f, %8.2f, %8.2f'%(s_extrap.x(),s_extrap.y(),s_extrap.z())


