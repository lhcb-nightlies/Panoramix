# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2008"

#importOptions('$L0DUOPTS/L0DUFromRaw.opts')

# define a helper class:
class irange(object) :
   def __init__(self, b, e ):
     self.begin, self.end  = b, e
   def __iter__(self):
     it = self.begin
     while it != self.end :
       yield it.__deref__()
       it.__postinc__(1)

appConf = ApplicationMgr( OutputLevel = INFO, AppName = 'readtest')
#appConf.HistogramPersistency = "HBOOK"
HistogramPersistencySvc().OutputFile  = "test.root"

EventSelector().PrintFreq  = 50

import GaudiPython
gbl=GaudiPython.gbl
from gaudigadgets import *

#dictionary for private data to be saved on a DST
GaudiPython.gbl.gSystem.Load('enclose') # Need to load the just created dictionary without autoloading
MyClass = gbl.Enclose(gbl.MyClass)
Vector  = gbl.Enclose('std::vector<double>')

appMgr = GaudiPython.AppMgr() 
sel    = appMgr.evtsel()
sel.open(['PFN:L0yes.raw'])
#sel.open(['PFN:/afs/cern.ch/lhcb/group/trigger/vol3/dijkstra/Selections/Bd2MuMuKst-lum5.dst'])
evt = appMgr.evtsvc()

first = True
n = 10
while n>0 :
 n-=1 
 appMgr.run(1)
 # check L0
 L0DUReport = evt['Trig/L0/L0DUReport']
 if not L0DUReport : break  # probably end of input
 L0DUConfig = L0DUReport.configuration()
 cond = L0DUConfig.conditions()
 if first : 
   first = False
   for x in irange(cond.begin(),cond.end()) : print x.first, x.second
 print L0DUReport
 
 datas      = L0DUConfig.data()   
 for x in irange(datas.begin(),datas.end()) :
   idata = x.second
   print idata.summary() 
  # or to get the actual value :
   name = idata.name()
   digit  = idata.digit()
   print  'L0DU Data : ',name,'has value (digit unit)',digit
 
 decisions = L0DUReport.channelsDecisionSummaries()
 for d in irange(decisions.begin(),decisions.end()) :                 
   print  'L0DU decision',d   

 channels = L0DUReport.configuration().channels()
 for c in irange(channels.begin(),channels.end()) :
  print '%15s : %2i '%(c.first,L0DUReport.channelDecision(c.second.id() ) )
 
 
 if evt['MyObject'] : 
  print evt['MyObject'].s,evt['MyObject'].i,evt['MyObject'].d
  for d in evt['MyVector'] :
   print 'collision x1Bjorken',d
