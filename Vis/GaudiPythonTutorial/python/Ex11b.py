from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType            = "2011"
# container name
candidates = 'Dimuon/Phys/BetaSBu2JpsiKPrescaledLine/Particles' 

appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'Ex11b')
EventSelector().PrintFreq = 100

import GaudiPython
appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/Bu2JpsiK_00011917_00000048_1.dst'])
evt = appMgr.evtsvc()

h_bmass = TH1F('h_bmass','Mass of B candidate',100,5200.,5500.)

while 0 < 1:
 appMgr.run(1)
# check if there are still valid events 
 if not evt['Rec/Header'] : break
 cont = evt[candidates]
 if cont : 
  for b in cont : success = h_bmass.Fill(b.momentum().mass())
h_bmass.Draw()

Hbookfname = "myoutput.hbook"
from ROOT import TFile,gROOT
f=TFile('temp.root','recreate')
for h in gROOT.GetList() : 
 h.Write()
f.Close()
import os
os.system("root -b -q -l  \'root2hbook.c(\"temp.root\")\'")
os.system("paw -b temp.kumac")
os.system("rm temp.root")
os.system("rm temp.kumac")
os.system("mv temp.hbook "+Hbookfname)

