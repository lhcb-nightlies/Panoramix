# Only works on windows, requires wmi module

signal = False
dst    = True
newdst = False
rdst   = False

# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"

appConf = ApplicationMgr( OutputLevel = INFO, AppName = 'dst_sizes' )
  
import GaudiPython
from gaudigadgets import *

appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
if signal: 
 sel.open(['PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-lumi2/00001620/DST/0000/00001620_00000001_5.dst'
          ,'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-lumi2/00001620/DST/0000/00001620_00000001_5.dst'
          ,'PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-lumi2/00001620/DST/0000/00001620_00000001_5.dst'])
elif dst:
 # official DST
 sel.open(['/media/Data/SEMILEPTONIC.DST/MCLamc/DOWN/00013195_00000010_1.allstreams.dst'])
elif newdst:
 # newDST
 sel.open(['PFN:castor:/castor/cern.ch/user/l/lshchuts/stripping/newDST_v31/1.dst'])
elif rdst:
 # rDST
 sel.open(['PFN:castor:/castor/cern.ch/grid/lhcb/production/DC06/phys-v4-lumi2/00001896/RDST/0000/00001896_00000004_1.rdst'])

evt = appMgr.evtsvc()

appMgr.run(1)

try: 
 allnodes = nodes(evt,True)
except:
# just try it again
 allnodes = nodes(evt,True)
 
import wmi,time
c = wmi.WMI()
process = c.Win32_Process (name='python.exe')[0]
def diff_process_param(n) :
  new_param = {}
  process   =  c.Win32_Process (name='python.exe')[n] 
  new_param['rio']   = process.Properties_('ReadTransferCount').Value
  new_param['ctime'] = time.clock()
  new_param['ktime'] = process.Properties_('KernelModeTime').Value
  new_param['utime'] = process.Properties_('UserModeTime').Value 
  for x in process_param : 
   temp             = new_param[x]
   new_param[x]     = int(temp) - int(process_param[x])
   process_param[x] = temp
  return new_param  
  
  
process_param      = {'rio':0,'ctime':0,'ktime':0,'utime':0}
sizes = {}
allnodes.append('Zero')
for x in allnodes : 
 sel.rewind()
 nevents = 0
 for n in range(1000): 
   appMgr.run(1)
   nevents +=1
   if x != 'Zero': test = evt[x]
 diff_param = diff_process_param(0)
 sizes[x] = diff_param['rio']/float(nevents)

# printout
print ' container name ',  'size in kb'

x = 'Zero'
print ' %30s %4.2F'%(x,sizes[x]) 

items = sizes.values()
items.sort()

f = open('out.txt','w')

for i in items:
 for x in sizes:
  if i == sizes[x] : 
   if x != 'Zero': 
     xx = x.replace('/Event/','')
     print ' %40s %4.2F'%(xx,(sizes[x]-sizes['Zero'])/1000.) 

for i in items:
 for x in sizes:
  if i == sizes[x] : 
   if x != 'Zero': 
     xx = x.replace('/Event/','')
     if xx.find('MC')>-1 or xx.find('Link')>-1 or xx.find('Gen')>-1 : continue
     out = ' %80s %4.2F \n'%(xx,(sizes[x]-sizes['Zero'])/1000.) 
     f.write(out)
     print out

total = 0
for x in sizes:
 if x.find('MC')==-1 and x.find('Link')==-1 and x.find('Gen')==-1 : total+=sizes[x]
print total  
f.close()
