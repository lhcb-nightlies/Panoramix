# SetupProject LHCbDirac
# lhcb-proxy-init
from DIRAC.Interfaces.API.Dirac import Dirac
from LHCbDIRAC.BookkeepingSystem.Client.BookkeepingClient import BookkeepingClient
from DIRAC import gConfig
from DIRAC.Core.DISET.RPCClient import RPCClient
from DIRAC.Core.Utilities.List import sortList
import re,os
debug = False

All = ["CERN_M-DST","CNAF_M-DST","GRIDKA_M-DST","IN2P3_M-DST","NIKHEF_M-DST","PIC_M-DST","RAL_MC-DST"]


host  = os.environ['HOST']
if host.find('pctommy')>-1: 
  prefix = '~/castorfs/cern.ch/grid/lhcb/data/2010/'
  lscommand = 'ls'
else :
  prefix = '/castor/cern.ch/grid/lhcb/data/2010/'
  lscommand = 'nsls'

dirac = Dirac()
getlistofruns = False
runs = []

def rawData():
# get list of runs, do not know any other way at the moment:
 pfn = prefix+"RAW/FULL/LHCb/COLLISION10/"
 if getlistofruns:
  os.system(lscommand+" -l "+pfn+"  > listOfRuns.txt" )
 f=open("listOfRuns.txt")
 allruns = f.readlines()
 f.close()
 for l in allruns:
  a = l.find('\n')
  b = l.rfind(' ')
  if a>0 and b>0 :
   runs.append(l[b+1:a])

 bk = BookkeepingClient()
 out=open("runinfo.txt",'w')
 shortinfo=[]
 for run  in runs:
  res = bk.getRunInformations(int(run))
  out.write(  "============== " + run + " ==================================\n")
  if res['OK']:
    val = res['Value']
    out.write(  "Run  Informations: \n")
    out.write(  "Run Start:".ljust(50) + str(val['RunStart'])+"\n")
    out.write(  "Run End:".ljust(50)   + str(val['RunEnd'])  +"\n")
    out.write(  "  Configuration Name:".ljust(50)+str(val['Configuration Name'])+"\n")
    out.write(  "  Configuration Version:".ljust(50)+str(val['Configuration Version'])+"\n")
    out.write(  "  FillNumber:".ljust(50)+str(val['FillNumber'])+"\n")
    out.write(  "  Data taking description: ".ljust(50)+str(val['DataTakingDescription'])+"\n")
    out.write(  "  Processing pass: ".ljust(50)+str(val['ProcessingPass'])+"\n")
    out.write(  "  Stream: ".ljust(50)+str(val['Stream'])+"\n")
    out.write(  "  FullStat: ".ljust(50)+str(val['FullStat']).ljust(50)+" Total: ".ljust(10)+str(sum(val['FullStat']))+"\n")
    out.write(  "  Number of events: ".ljust(50) +str(val['Number of events']).ljust(50)+" Total:".ljust(10)+str(sum(val['Number of events']))+"\n")
    out.write(  "  Number of file: ".ljust(50)+str(val['Number of file']).ljust(50) + " Total: ".ljust(10)+str(sum(val['Number of file']))+"\n")
    out.write(  "  File size: ".ljust(50) + str(val['File size']).ljust(50)+ " Total: ".ljust(10) +str(sum(val['File size']))+"\n")
    shortinfo.append([int(run),sum(val['Number of events']),sum(val['File size']),float(sum(val['File size'])) / float(sum(val['Number of events']))] )
    out.write(  "shortinfo: ".ljust(50) + run + str(sum(val['Number of events'])) + " " + str(sum(val['File size'])) + "\n")
    print  "shortinfo: ".ljust(50), run, str(sum(val['Number of events'])) + " " + str(sum(val['File size'])),float(sum(val['File size'])) / float(sum(val['Number of events']))/1000.
  else:
   out.write(  "Failed to retrieve run files: %s \n" % res['Message'] )
 totalevents = 0
 totalbytes  = 0 
 for s in shortinfo:
  totalevents+=s[1]
  totalbytes+=s[2]
 print "total event statistics ",totalevents," total bytes ",totalbytes

###  useful dirac commands:
# dirac-dms-storage-usage-summary --Sites="LCG.CERN.ch"
#DIRAC SE             Size (TB)            Files
#--------------------------------------------------
#CERN-FAILOVER        0.1                  691
#CERN-HIST            0.9                  292195
#CERN-RAW             172.0                127255
#CERN-RDST            62.7                 63885
#CERN-USER            93.5                 615761
#CERN-disk            76.3                 290164
#CERN-tape            12.9                 56823
#CERN_M-DST           242.0                339683
#CERN_MC_M-DST        351.7                115759


######  get statistics from Castor direct
def strippedDST():
 result = {}
 os.system(lscommand+" "+prefix+" > streams.txt") 
 f=open("streams.txt")
 allstreams = f.readlines()
 f.close()
 streams = []
 for line in allstreams:
  for s in line.split(' '):
     if s.find('DST') > -1: streams.append(s.replace('\n',''))
 for s in streams: 
  os.system(lscommand+" "+prefix+s+" > temp.txt") 
  f=open("temp.txt")
  alllines = f.readlines()
  f.close()
  result[s] = {}  
  for line in alllines: 
    prodid = line.split(' ')[0].replace('\n','')
    os.system(lscommand+" -l "+prefix+s+"/"+prodid+"/0000/ > temp.txt") 
    f=open("temp.txt")
    alllines = f.readlines()
    f.close()
    nbytes = 0
    for line in alllines: 
      tmp = line.split(' ')  
      if len(tmp)>3:
       xx = 0
       for n in range(4,6):
         if tmp[n] != "" :
           xx = int(tmp[n])
           break 
       nbytes += xx

    res = bk.getProductionInformations_new(long(prodid))
    nevents = res['Value']['Number of events'][1][1]
    result[s][prodid] = [nbytes,nevents]
 return result
#>>> analysis(res)
#EW.DST   30.1345550293
#CHARM.DST   33.2997888569
#BHADRON.DST   17.3775427552
#SEMILEPTONIC.DST   8.2486982409
#DIMUON.DST   25.9257616729
#DIPHOTONDIMUON.DST   0.097661155734
#RADIATIVE.DST   1.19273626795
#CALIBRATION.DST   10.4276460639
#MINIBIAS.DST   15.5171737637
#HADRONIC.DST   52.1092564385
#DIELECTRON.DST   22.8018846057
#V0.DST   5.9463088429
#SDST   12.0073606723
#DST   23.9008501554
#RDST   0.00278766055
#total  258.990012182


# taken from dirac-dms-storage-usage-summary 
def strippedDSTDirac():
 rpc = RPCClient('DataManagement/StorageUsage')
 bk = BookkeepingClient()
 result = {}
 os.system(lscommand+' '+prefix+' > streams.txt')
 f=open("streams.txt")
 allstreams = f.readlines()
 f.close()
 streams = []
 for line in allstreams:
  for s in line.split(' '):
   if s.find('DST') > -1: streams.append(s.replace('\n',''))
 for s in streams: 
   os.system(lscommand+" "+prefix+s+" > temp.txt")
   f=open("temp.txt")
   alllines = f.readlines()
   f.close()
   result[s] = {}
   for line in alllines:
    prodid = line.split(' ')[0].replace('\n','')
    directory = "/lhcb/data/2010/"+s
    ses = ["CERN_M-DST"]
    if debug: print 'test',directory,"",prodid,ses
    res = rpc.getStorageSummary(directory,"",prodid,ses)
    print '-'*50
    if len(res['Value']) > 0:
     files = res['Value'][ses[0]]['Files']
     size  = float(res['Value'][ses[0]]['Size'])/1E12
    else:
     files = 0
     size = 0
    res = bk.getProductionInformations_new(long(prodid))
    nevents = 0.0001
    if res.has_key('Value') and files > 0 :
     for x in res['Value']['Number of events']:
        if x[0].find('DST') > -1:
          nevents = x[1]
          break
    print s,prodid,'%5.2F TB'%(size),files,int(nevents), '%6.3F kB/event'%(float(size*1E9)/float(nevents))
    result[s][prodid] = [size,nevents]
 return result

def analysis(res,scale=1.):
 total = 0
 for r in res.keys():
   totsum = 0
   toteve = 0 
   for prodid in res[r]:
     totsum+=res[r][prodid][0]
     toteve+=res[r][prodid][1]
   total+=totsum
   print r.ljust(30),'%5.2F TB'%(float(totsum)  / scale ),'%10.2E'%(toteve),'%5.2F kB/event'%(float(totsum*1E9)/float(toteve)  / scale)
 print 'total '.ljust(30),'%5.2F'%(float(total)  / scale)


# other useful information:
# dirac-dms-show-se-status
# dirac-dms-storage-usage-summary



