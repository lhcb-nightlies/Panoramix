from ROOT import TFile,TH1F,TH2F,TCanvas,TBrowser,gStyle,TText,TMath,TF1,gROOT,MakeNullPointer,gSystem
f_2008   = False

# get the basic configuration from here
from LHCbConfig import *
if not f_2008:
    lhcbApp.DataType = "DC06"
else:
    lhcbApp.DataType = "2008"
    lhcbApp.Simulation = True     

appConf = ApplicationMgr( OutputLevel = INFO, AppName = 'TestChi2Match' )
EventSelector().PrintFreq  = 100

import GaudiPython
import GaudiKernel.SystemOfUnits as units

gbl = GaudiPython.gbl
ParticleID     = gbl.LHCb.ParticleID
MCTrackInfo    = gbl.MCTrackInfo
MCParticle     = gbl.LHCb.MCParticle
Track          = gbl.LHCb.Track
part           = MakeNullPointer(MCParticle)
             
def chi2Match(type,p):  
 pmom = p.momentum()
 sx = pmom.x()/pmom.z()
 sy = pmom.y()/pmom.z()
 chi2min = 999999.
 tmatch  = None
 for t in evt['Rec/Track/Best']: 
   if type == "Long" and t.type() != t.Long : continue
   if t.type() != t.Long and t.type() != t.Velo and t.type() != t.Downstream : continue  
   fstate = t.firstState()
# ignore correlations 
   chi2x = (fstate.tx()-sx)*(fstate.tx()-sx)/fstate.errTx2()    
   chi2y = (fstate.ty()-sy)*(fstate.ty()-sy)/fstate.errTy2()    
   chi2 = chi2x+chi2y 
   if chi2 < chi2min : 
       chi2min = chi2
       tmatch = t
 return tmatch,chi2min  

def chi2Link(p,t):  
 pmom = p.momentum()
 sx = pmom.x()/pmom.z()
 sy = pmom.y()/pmom.z()
 fstate = t.firstState()
# ignore correlations 
 chi2x = (fstate.tx()-sx)*(fstate.tx()-sx)/fstate.errTx2()    
 chi2y = (fstate.ty()-sy)*(fstate.ty()-sy)/fstate.errTy2()    
 chi2 = chi2x+chi2y 
 return chi2 
   
h_chi2_v = TH2F('h_chi2_v','chi2 from match Velo / truth ',100,0.,100.,110,-10.,100.)
h_chi2_l = TH2F('h_chi2_l','chi2 from match Long / truth ',100,0.,100.,110,-10.,100.)

appMgr = GaudiPython.AppMgr()
sel  = appMgr.evtsel()
evt  = appMgr.evtsvc()
msg  = appMgr.service('MessageSvc', 'IMessageSvc')
dps  = appMgr.service('EventDataSvc', 'IDataProviderSvc')
from LinkerInstances.eventassoc import linkedFrom, linkedTo  

MCDecayFinder  = appMgr.toolsvc().create('MCDecayFinder', interface='IMCDecayFinder')
MCDecayFinder.setDecay( "[ B_s0  -> ( phi(1020) -> K+  K-  ) gamma]cc" )

files = []
if f_2008: file  = '/castor/cern.ch/grid/lhcb/MC/2008/DST/00003989/0000/00003989_00000XXX_5.dst'
else : file  = '/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00002019/DST/0000/00002019_00000XXX_5.dst'
for n in range(1,10) : 
  ff = file.replace('XXX','%(X)03d'%{'X':n})
  x  = os.system('nsls '+ff)
  if x == 0 :  files.append(ff)
sel.open(files)

while 0 < 1:
  appMgr.run(1)
# check if there are still valid events 
  if not evt['Rec/Header'] : break
  mc        = evt['MC/Particles']
  if not MCDecayFinder.hasDecay(mc) : continue
  decaylist = []
  trackinfo = MCTrackInfo(dps, msg)
  mc2track  = linkedFrom(Track,MCParticle,'Rec/Track/Best')   
  cont = evt['Rec/Track/Best']

  while MCDecayFinder.findDecay(mc,part)>0 :
   pclone = part.clone()
   GaudiPython.setOwnership(pclone,True)
   decaylist.append(pclone)
  for decay in decaylist :
   daughters = GaudiPython.gbl.std.vector('const LHCb::MCParticle*')()
   MCDecayFinder.descendants(decay,daughters)
# find the two kaons 
   kaons = [] 
   for dpart in daughters :
    if dpart.particleID().abspid() != 321 : continue 
    mother = dpart.mother()
    if mother.particleID().pid() != 333  : continue
    kaons.append(dpart)
   if len(kaons) != 2 : 
    continue
#  
   for k in kaons : 
    # match with linker table
    trv,chi2v = chi2Match('Velo',k)  
    trl,chi2l = chi2Match('Long',k)  
    vmatch = False
    lmatch = False
    for t in mc2track.range(k) :    
      if t.type() != t.Long and t.type() != t.Velo and t.type() != t.Downstream : continue     
      vmatch = True
      chi2link = chi2Link(k,t)  
      sc = h_chi2_v.Fill(chi2v,chi2link)
      if t.type() == t.Long : 
       lmatch = True
       sc = h_chi2_l.Fill(chi2l,chi2link) 
    if vmatch == False : h_chi2_v.Fill(chi2v,-5.)
    if lmatch == False and vmatch == True : h_chi2_v.Fill(chi2l,-5.)
            
      

