# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2008"
lhcbApp.Simulation = True

privateData = False
L0          = False 
unpacktest  = False
rDST        = False
Hlt         = True


if Hlt: 
 from HltConf.Configuration import *
 hltconf = HltConf()
 hltconf.hltType = 'Hlt1+PA'
 appConf.TopAlg += [ GaudiSequencer('Hlt') ]


rawwriter  = OutputStream('RawWriter', Preload = False)
rawwriter.ItemList = ["/Event#1","/Event/DAQ#1","/Event/DAQ/RawEvent#1","/Event/DAQ/ODIN#1",
                      "/Event/pRec#999","/Event/Rec#999"]
if  unpacktest : 
 rawwriter.ItemList = ["/Event#1","/Event/DAQ#1","/Event/DAQ/RawEvent#1","/Event/DAQ/ODIN#1","/Event/Rec#999"]
if  rDST : 
 rawwriter.ItemList = ["/Event#1","/Event/Rec#999","/Event/pRec#999"] 
                       
rawwriter.OptItemList = ["/Event/MyVector#1","/Event/MyObject#1"]

if L0:  
  rawwriter.Output   = "DATAFILE='PFN:L0yes.dst' TYP='POOL_ROOTTREE' OPT='REC' "
else:  
  rawwriter.Output   = "DATAFILE='PFN:Phys.dst' TYP='POOL_ROOTTREE' OPT='REC' "

appConf    = ApplicationMgr( OutputLevel = INFO, AppName = 'RemoveMC') 
appConf.OutStream = [rawwriter]

EventSelector().PrintFreq  = 50

import GaudiPython
from GaudiPython import gbl

from gaudigadgets import *

# dictionary for private data is genereated by createPrivateDict.py
gbl.gSystem.Load('enclose') # Need to load the just created dictionary without autoloading
MyClass = gbl.Enclose(gbl.MyClass)
Vector  = gbl.Enclose(gbl.std.vector('double'))

def add_priv_data():
#---Add MyClass object------
  o = MyClass()
  o.i, o.d, o.s = evt['Gen/Header'].evtNumber(),  evt['Gen/Header'].luminosity(), evt['Gen/Header'].applicationName()
  GaudiPython.setOwnership(o, False)  # Neeed to give up ownership
  evt.registerObject('/Event/MyObject', o)
#---Add vector-----
  v = Vector()
  for x in evt['Gen/Header'].collisions() :  
   e = x.target().x1Bjorken()
   v.push_back(e)
  GaudiPython.setOwnership(v, False)
  evt.registerObject('/Event/MyVector', v)
  return True

def test():
#---Add vector-----
  v = Vector()
  for x in range(5) :  
   v.push_back(x)
  return True

files = []
file  = "DATA='/castor/cern.ch/grid/lhcb/MC/2008/DST/00003984/0000/00003984_00000XXX_5.dst'  TYP='POOL_ROOTTREE' OPT='READ'"
for n in range(1,10) : 
  ff = file.replace('XXX','%(X)03d'%{'X':n})
  files.append(ff)
EventSelector(Input = files)

appMgr = GaudiPython.AppMgr()
evt  = appMgr.evtsvc()

appMgr.algorithm('RawWriter').Enable = False  # stop automatic execution of RawWriter

while 1>0 : 
 appMgr.run(1)
 if not evt['Rec/Header'] : break  # probably end of input
 if privateData : rc = add_priv_data() 
 # check L0
 L0dir = evt['Trig/L0/L0DUReport']
 if L0dir.decision() > 0 : 
  ss = gaudigadgets.nodes(evt,True,'pRec')
  ss = gaudigadgets.nodes(evt,True,'Rec')
  if unpacktest : gaudigadgets.unpackAll()
  rc = appMgr.algorithm('RawWriter').execute() # output event                                      
