from ROOT import TH1F, TCanvas    

from LHCbConfig import *

lhcbApp.DataType = "2010"
lhcbApp.DDDBtag   =  "head-20100119"
lhcbApp.CondDBtag =  "sim-20100222-vc-md100"
lhcbApp.Simulation = True

appConf = ApplicationMgr(OutputLevel = INFO) 
EventSelector().PrintFreq = 10000

from Configurables import HltConf,L0Conf

ApplicationMgr().ExtSvc += [ "LoKiSvc" ]
hltconf = HltConf()
hltconf.L0TCK = '0x1810'
hltconf.ThresholdSettings =  'Physics_MinBiasL0_PassThroughHlt_Apr10'

hlt = GaudiSequencer('Hlt')
appConf.TopAlg += [hlt]
DataOnDemandSvc().AlgMap["Hlt/DecReports"] = HltDecReportsDecoder(OutputLevel = 4)
DataOnDemandSvc().AlgMap["Hlt/SelReports"] = HltSelReportsDecoder(OutputLevel = 4)
DataOnDemandSvc().AlgMap["Hlt/VertexReports"] =HltVertexReportsDecoder( OutputLevel = 4)


import GaudiPython

appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
evt    = appMgr.evtsvc()

sel.open(['/castor/cern.ch/grid/lhcb/MC/2010/DST/00005934/0000/00005934_00000001_1.dst',
          '/castor/cern.ch/grid/lhcb/MC/2010/DST/00005949/0000/00005949_00000001_1.dst'])

appMgr.algorithm('Hlt').Enable = False

import GaudiPython
appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
evt = appMgr.evtsvc()
h  = TH1F('h',' no. of tracks',100,0.,500.)
hp = TH1F("hp","Momentum of Tracks",100,0.,50000.)

for n in range(500):
  appMgr.run(1)
  odin = evt['DAQ/ODIN']
  odin.setTriggerType(odin.LumiTrigger)
  appMgr.algorithm('Hlt').execute()
  HltReport = evt['Hlt/DecReports']
  if not HltReport.decReport('Hlt1MBMicroBiasRZVeloDecision').decision() : continue
  
  pv = evt['/Event/Rec/Vertex/Primary']
  tracks = evt['/Event/Rec/Track/Best']
  rc = h.Fill(tracks.size())
  for t in tracks :    
    hp.Fill(t.p())
    
hp.Draw()
