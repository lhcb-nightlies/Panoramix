# get the basic configuration from here
from LHCbConfig import *
import os
# argument one: list of files "file1 file2 ..."
if len(os.sys.argv) > 2 : 
 fn         = os.sys.argv[1]
 if fn.find('.py')>-1: 
    importOptions(fn)
 else:  files = fn.split(' ')
 outname    = os.sys.argv[2]
else :
 print 'use default file name'
 stop  

appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'MergeFiles')
lhcbApp.DataType    = "2010"
InputCopyStream().Output   = "DATAFILE='PFN:"+outname+"' TYP='POOL_ROOTTREE' OPT='REC' " 

appConf.OutStream = [InputCopyStream()]
EventSelector().PrintFreq = 1000
import GaudiPython
import LHCbMath.Types
LHCbMath = LHCbMath.Types.Gaudi
TrackTraj = GaudiPython.gbl.LHCb.TrackTraj
import GaudiKernel.SystemOfUnits as units
from ROOT import Double
dis   = LHCbMath.XYZVector()

appMgr = GaudiPython.AppMgr()
evt    = appMgr.evtsvc()
sel    = appMgr.evtsel()
if fn.find('.py')==-1: sel.open(files)
appMgr.algorithm('InputCopyStream').Enable = False
poca  = appMgr.toolsvc().create('TrajPoca', interface='ITrajPoca')

def myPVs():
   hardPVs = []
   for pv in evt['Rec/Vertex/Primary']:
     tracks = pv.tracks()
     if tracks.size()>8: hardPVs.append(pv.key())
   return hardPVs

def pos():
 interesting = False 
 minip = 999.
 for amuon in evt['Dimuon/Phys/StdLooseMuons/Particles']:   
   tr = amuon.proto().track()
   traj     =  TrackTraj( tr )
   for kpv in myPVs() :
    pv = evt['Rec/Vertex/Primary'][kpv]
    pvpos =  pv.position()
    s1 = Double(0.1)
    a  = Double(0.0005)
    success = poca.minimize(traj,s1,pvpos,dis,a)   
    ip = dis.r()
    if ip<minip : minip = ip
 if minip > 0.5: interesting = True
 return interesting

while 1>0:
  appMgr.run(1)
  if not evt['DAQ/RawEvent'] : break
  if pos():
   appMgr.algorithm('InputCopyStream').execute()
