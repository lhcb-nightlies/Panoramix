from processing import Process, Queue, Pool
import popen2,os,time

from random import *
from ROOT import *

#------The function to run in each 'process'
def g(n) :
  print 'doing the next iteration to calc something [%i]'% n
  child_stdin, child_stdout = os.popen4('python rrandom.py '+str(n))
  return child_stdout.readlines()

#------Use up to 4 processes
p = Pool(4)

workitems = range(20)
result = p.map_async(g, workitems)

print result.get(timeout=1000)

