from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2008"

appConf = ApplicationMgr(OutputLevel = INFO , AppName = 'Ex10')

EventSelector(
    Input = ["DATA='castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS/24080/024080_0000054568.raw' SVC='LHCb::MDFSelector'"] )

import GaudiPython
appMgr = GaudiPython.AppMgr()

evt = appMgr.evtsvc()
appMgr.run(1)
print '+++++++', evt['Raw/Hcal/Digits'].size()

