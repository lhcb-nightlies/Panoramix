import os,getpass
files = []

host  = os.environ['HOST']
local = host.find('pctommy')>-1
if local : 
  path = '/media/Work'
else :
  path = '/castor/cern.ch/user/t/truf/2009'

prodids = [5731,5730,5727]

if local:
 allfiles = os.listdir(path)
 for f in allfiles :
  for prod in prodids:
   if f.find('_2.dst')>-1 and f.find('0000'+str(prod))>-1  : files.append(path+'/'+f)


for f in files: 
 nw = f.replace('_2.dst','_2_nolumi.dst')
 os.system('python stripOffLumi.py '+f+' '+nw)
