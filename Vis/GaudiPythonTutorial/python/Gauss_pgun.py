nevents = 10

from Gaudi.Configuration import *
import GaudiKernel.SystemOfUnits as units
from Configurables import Gauss,ParticleGun,LHCbApp, UpdateManagerSvc, MomentumRange,EvtGenDecay
from Gauss.Configuration import *
LHCbApp().DDDBtag   = "head-20101206"
LHCbApp().CondDBtag = "sim-20101210-vc-md100"

UpdateManagerSvc().ConditionsOverride += ["Conditions/Online/Velo/MotionSystem := double ResolPosRC =-5.0 ; double ResolPosLA = 5.0 ;"]

gauss = Gauss()
gauss.Production = 'PGUN'
gauss.DataType   = "MC10"

#--Generator phase, set random numbers
GaussGen = GenInit("GaussGen")
GaussGen.FirstEventNumber = 1
GaussGen.RunNumber        = 13
#--Number of events
LHCbApp().EvtMax = 10

mypgun = ParticleGun("ParticleGun")

Generator  = GaudiSequencer('Generator')
mypgun.ParticleGunTool = "MomentumRange"
mypgun.NumberOfParticlesTool = "FlatNParticles"
mypgun.addTool(MomentumRange())
mypgun.MomentumRange.PdgCodes = [ 443 ]

EvtGenDecay().UserDecayFile = "$DECFILESROOT/dkfiles/Jpsi_mumu,20GeV-140GeV=TrkAcc.dec"

mypgun.MomentumRange.MomentumMin =  10.0*units.GeV
mypgun.MomentumRange.MomentumMax = 200.0*units.GeV
mypgun.EventType = 59010141
mypgun.MomentumRange.ThetaMin = 0.015*units.rad
mypgun.MomentumRange.ThetaMax = 0.300*units.rad

mypgun.GenHeaderLocation = ''


appConf = ApplicationMgr( OutputLevel = INFO, AppName = 'pgun' )

FileCatalog().Catalogs = [ "xmlcatalog_file:Gauss_Catalog.xml" ]
HistogramPersistencySvc().OutputFile = 'Gauss.root'

import GaudiPython

appMgr = GaudiPython.AppMgr()
appMgr.initialize()

# stop Geant4 for the moment
#appMgr.algorithm('Simulation').Enable = False
appMgr.algorithm('GaussTape').Enable = False

# generate n events
for events in range(nevents):
 appMgr.run(1)  
evt=appMgr.evtsvc()
