from Gaudi.Configuration import *
from Configurables import Moore
max_Events = 10000
debug      = False
from GaudiPython.Parallel import Task, WorkManager

Moore().DataType     = "MC09"
Moore().Simulation   = True
Moore().inputFiles   = []
Moore().HltType      = 'Hlt1'

from GaudiPython import AppMgr
import GaudiPython
appMgr = AppMgr()
stringv  = GaudiPython.gbl.std.vector('string')

class MyTask(Task):
  
  def initializeLocal(self):
   print 'initializeLocal'
  def initializeRemote(self):        
   print 'initializeRemote'

  def process(self, file) :
   appMgr = AppMgr()
   sel = appMgr.evtsel()
   fname = stringv()
   fname.push_back("DATA='"+file+"' SVC='LHCb::MDFSelector'")
   sel.properties()['Input'].property().setValue(fname)
   sel.properties()['PrintFreq'].property().setValue(5000)

   sel.reinitialize()
   evt = appMgr.evtsvc()
   nevents = 0
   while nevents < max_Events:
    appMgr.run(1)
    nevents+=1
# check if there are still valid events
    if not evt['DAQ/ODIN'] : break
   print 'Finishing.... ', file
   appMgr.stop()

if __name__ == '__main__' :
 files  = ['PFN:/home/truf/data/052808_0000000003.raw',
           'PFN:/home/truf/data/052808_0000000001.raw',
           'PFN:/home/truf/data/052808_0000000004.raw',
           'PFN:/home/truf/data/052808_0000000005.raw',
           'PFN:/home/truf/data/052808_0000000006.raw',
           'PFN:/home/truf/data/052808_0000000007.raw'
]
 
 task = MyTask()
 if not debug:
  wmgr = WorkManager(ncpus = 6)
  wmgr.process(task,files[:6]) 
 else:
  task.initializeLocal()
  task.initializeRemote()
  task.process(files[0])
 print 'finished'

# 6 cores
#job count | % of all jobs | job time sum | time per job | job server
#       6 |        100.00 |      592.378 |      98.730 | None
#Time elapsed since server creation 104.528590
# 574 Hz

# 4 cores
#job count | % of all jobs | job time sum | time per job | job server
#       4 |        100.00 |      336.574 |      84.143 | None
#Time elapsed since server creation 85.604600
# 470 Hz

# 2 cores
#Job execution statistics:
#job count | % of all jobs | job time sum | time per job | job server
#       2 |        100.00 |      134.678 |      67.339 | None
#Time elapsed since server creation 67.411041
# 300 Hz
