# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType            = "2010"
lhcbApp.Simulation          = True 

appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'Ex16')
 
import GaudiPython
from gaudigadgets import irange as irange

appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/MC2010_BsJpsiPhi__00008919_00000068_1.dst'])
evt = appMgr.evtsvc()

appMgr.run(1)
GenEvtContainer = evt['Gen/HepMCEvents']
for genEvt in GenEvtContainer:
  pGenEvt = genEvt.pGenEvt()
  for particle in irange(pGenEvt.particles_begin(),pGenEvt.particles_end()):
     if particle.production_vertex() :
      vtx = particle.production_vertex()
      print "PDG ID %i production vertex z=%4.2F  part out: %3i" %(particle.pdg_id(), vtx.position().z(),vtx.particles_out_size())
     else:
      print "PDG ID %i                       " %(particle.pdg_id())

GenEvtContainer = evt['Gen/HepMCEvents']
for genEvt in GenEvtContainer:
  pGenEvt = genEvt.pGenEvt()
  for genp in irange(pGenEvt.particles_begin(),pGenEvt.particles_end()):
        if( not genp.end_vertex() ): continue
        vtx = genp.end_vertex()
        print 'Pos vtx = %4.2F  part in: %3i  part out: %3i'%(vtx.position().z(),vtx.particles_in_size(),vtx.particles_out_size())
## following does not work anymore, heavy crash    ?    
        ##for dau in irange( vtx.particles_out_const_begin(), vtx.particles_out_const_end() ):
        ##  print 'daughter', dau.momentum().eta()

