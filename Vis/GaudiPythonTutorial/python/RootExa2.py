from ROOT import *

fh = gROOT.FindObjectAny
hstore =[] # you are the owner
# booking
for n in range(21) :
 hstore.append(TH1F('sensor'+str(n),' ADC counts',100,0.,255.))

Landau = TRandom().Landau 

# filling 
for n in range(21) :
 for hits in range(100) : 
   adc = Landau(25.,3.)
   rc = fh('sensor'+str(n)).Fill(adc)

# drawing
fh('sensor'+str(10)).Draw()

# saving
f=TFile('histos.root','recreate')
for h in hstore : 
  rc = h.Write()
f.Close()

# retrieving
f=TFile('histos.root')
fh = f.FindObjectAny
# drawing
fh('sensor'+str(10)).Draw()



