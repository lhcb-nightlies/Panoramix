# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2010"
appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'Ex15')
 
importOptions('$PANORAMIXROOT/options/PanoramixVis.py')
from PanoramixSys.Configuration import *

lhcbApp.DataType            = "2010"
lhcbApp.Simulation          = False
# container name
candidates = 'Dimuon/Phys/Bs2MuMuLinesWideMassLine/Particles'

import GaudiPython
import gaudigadgets

appMgr = GaudiPython.AppMgr()
appMgr.initialize()
sel    = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst']) 
evt = appMgr.evtsvc()

appMgr.run(1)
evt.dump()
from panoramixmodule import *
# display a track
aTrack = evt['Rec/Track/Best'][12]
Object_visualize(aTrack)
import sys
sys.exit(0)
# go to GUI
#toui()

