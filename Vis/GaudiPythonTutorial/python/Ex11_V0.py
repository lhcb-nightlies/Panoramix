MC = False
outputname = 'privateV0'
if MC: outputname = outputname+'_MC'

#
from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
appConf = ApplicationMgr(OutputLevel = INFO, AppName = 'V0_Stripping')
EventSelector().PrintFreq = 5000

lhcbApp.DataType            = "2009"
if MC : 
   lhcbApp.Simulation = True 
   lhcbApp.DDDBtag    =  "head-20100119"
   lhcbApp.CondDBtag  =  "MC-20100205-vc15mm-md100"
AnalysisConf().DataType     = lhcbApp.DataType
PhysConf().DataType         = lhcbApp.DataType
PhysConf().InputType        = 'DST'
appConf.TopAlg+=[PhysConf().initSequence(),AnalysisConf().initSequence()]
output          = OutputStream("myDstWriter")
output.Output   =   "DATAFILE='PFN:"+outputname+".dst' TYP='POOL_ROOTTREE' OPT='REC'"
output.ItemList = ["/Event/Rec#999","/Event/Rec/Header#1","/Event/Rec/Status#1","/Event/DAQ#999","/Event/Strip#999","/Event/Phys#999"]
appConf.OutStream.append("myDstWriter")   

from StrippingSelections.StrippingV0 import StrippingV0Conf 
from PhysSelPython.Wrappers import SelectionSequence 
from StrippingConf.Configuration     import StrippingConf
from StrippingSelections import StreamV0
v0           = StrippingV0Conf( Monitor = True )
strip        = StrippingConf()
stream       = StreamV0.stream
stream.Lines = v0.lines()
strip.appendStream ( stream )
appConf.TopAlg+=[strip.sequence()]


import gaudigadgets
from LoKiPhys.decorators import TRTYPE
def check_part():
  all = gaudigadgets.nodes(evt,True)
  for node in all:
    if node.find('/Particles')>-1:
      print node
      for p in evt[node]:
       pr = p.proto()
       if not pr : continue
       t = TRTYPE ( p )
       print pr.track().key(),pr.key(),t

striplines = {}
h_mass     = {}
for l in stream.Lines : 
  nm = l.name() 
  striplines[ nm ] = l.outputLocation()+'/Particles'
for nm in striplines : 
  if nm.lower().find('lambda') == -1 :
    h_mass[ nm ] = TH1F('V0mass_'+nm,'Mass of Ks candidate '+nm,100,0.4,0.6)
  else : 
    h_mass[ nm ] = TH1F('V0mass_'+nm,'Mass of Lambda candidate '+nm,100,1.06,1.2)

import GaudiPython

appMgr = GaudiPython.AppMgr()
sel = appMgr.evtsel()
path = '/media/Work/'
listOfFiles =  [ 
path+'00005844/0000/00005844_00000003_1.dst',
path+'00005844/0000/00005844_00000002_1.dst',
path+'00005842/0000/00005842_00000175_1.dst',
path+'00005842/0000/00005842_00000163_1.dst',
path+'00005842/0000/00005842_00000142_1.dst',
path+'00005842/0000/00005842_00000183_1.dst',
path+'00005842/0000/00005842_00000171_1.dst',
path+'00005842/0000/00005842_00000195_1.dst',
path+'00005842/0000/00005842_00000153_1.dst',
path+'00005844/0000/00005844_00000003_1.dst',
path+'00005842/0000/00005842_00000196_1.dst',
path+'00005842/0000/00005842_00000156_1.dst',
path+'00005842/0000/00005842_00000137_1.dst',
path+'00005842/0000/00005842_00000140_1.dst',
path+'00005842/0000/00005842_00000194_1.dst',
path+'00005842/0000/00005842_00000160_1.dst',
path+'00005842/0000/00005842_00000161_1.dst',
path+'00005842/0000/00005842_00000151_1.dst',
path+'00005842/0000/00005842_00000164_1.dst',
path+'00005842/0000/00005842_00000174_1.dst',
path+'00005842/0000/00005842_00000159_1.dst',
path+'00005842/0000/00005842_00000176_1.dst',
path+'00005842/0000/00005842_00000170_1.dst',
path+'00005842/0000/00005842_00000158_1.dst',
path+'00005842/0000/00005842_00000162_1.dst',
path+'00005842/0000/00005842_00000152_1.dst',
path+'00005842/0000/00005842_00000185_1.dst',
path+'00005842/0000/00005842_00000181_1.dst',
path+'00005842/0000/00005842_00000197_1.dst',
path+'00005842/0000/00005842_00000141_1.dst',
path+'00005842/0000/00005842_00000168_1.dst',
path+'00005842/0000/00005842_00000154_1.dst',
path+'00005842/0000/00005842_00000173_1.dst',
path+'00005842/0000/00005842_00000172_1.dst',
path+'00005842/0000/00005842_00000146_1.dst',
path+'00005842/0000/00005842_00000190_1.dst',
path+'00005842/0000/00005842_00000193_1.dst',
path+'00005842/0000/00005842_00000135_1.dst',
path+'00005842/0000/00005842_00000167_1.dst',
path+'00005842/0000/00005842_00000165_1.dst']

listOfFiles.sort()
mcfiles = []
for n in range(5,37) : 
   mcfiles.append(path+'mschille_171_'+str(n)+'.dst')

if not MC: sel.open(listOfFiles)
else     : sel.open(mcfiles)

evt = appMgr.evtsvc()

appMgr.algorithm('myDstWriter').Enable = False


runstat = {}
while 1>0 :
 appMgr.run(1)
# check if there are still valid events 
 if not evt['Rec/Header'] : break
 run = evt['DAQ/ODIN'].runNumber()
 if runstat.has_key(run) : runstat[run]=+1
 else                    : runstat[run]= 1
 found = False
 for line in striplines:
   cont = evt[striplines[line]]
   if cont  : 
     for b in cont :
      success = h_mass[line].Fill(b.momentum().mass()/units.GeV)
      found = True
 if found : appMgr.algorithm('myDstWriter').execute() 

cv0 = TCanvas('v0','Mass distributions',1200,900)
n = len(striplines)
cv0.Divide(2,3)
print n
i=1
for line in striplines:
 cv0.cd(i)
 h_mass[line].Draw()
 i+=1
