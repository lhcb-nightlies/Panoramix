# SetupProject LHCbDirac
# lhcb-proxy-init
from DIRAC.Interfaces.API.Dirac import Dirac
dirac = Dirac()
import os
if len(os.sys.argv) > 1 : 
  lfn = os.sys.argv[1]
else: 
  lfn =  'LFN:/lhcb/data/2010/DST/00007943/0000/00007943_00000021_1.dst'

#lfn = "LFN:/lhcb/MC/2009/XDST/00005723/0000/00005723_00000001_1.xdst"
#lfn =  "LFN:/lhcb/MC/2009/XDST/00005723/0000/00005723_00000002_1.xdst"
#450 GeV new geometry
#lfn = 'LFN:/lhcb/MC/2009/XDST/00005725/0000/00005725_00000001_1.xdst'

l_local_file = lfn.rfind('/')+1
local_file = lfn[l_local_file:]

# also works
## os.system('dirac-dms-get-file  '+lfn+' '+local_file)

# sc = os.popen4('dirac-dms-lfn-replicas '+lfn)
result = dirac.getReplicas(lfn,printOutput=True)
print result
if result['OK'] : 
 txt  = result['Value']['Successful']
 dict = txt[lfn.replace('LFN:','')]
 dest1 = dict.keys()[0]
 srm = dict.values()[0]
 sc = os.system('lcg-cp '+srm+' '+local_file)
# to be tested
# sc = os.system('lcg-cp --vo LHCb '+srm+' '+local_file)
 
 print sc
