import os,time
from Gaudi.Configuration import *
from Brunel.Configuration import *
from GaudiKernel.SystemOfUnits import mm

import os
if os.environ.has_key('myProdFile'):
 datasetName  = os.environ['myProdFile']
else:
 datasetName  = 'gauss'

from Configurables import UpdateManagerSvc,DDDBConf,CondDB
LHCbApp().DDDBtag   = "head-20101206"
LHCbApp().CondDBtag = "sim-20101210-vc-md100"
DDDBConf(DataType = "2010")
CondDB().UseLatestTags = ["2010"]
LHCbApp().EvtMax    = 1000
# settings for intermediate energy 1.35 TeV early 2011
UpdateManagerSvc().ConditionsOverride += ["Conditions/Online/Velo/MotionSystem := double ResolPosRC =-5.0 ; double ResolPosLA = 5.0 ;"]

Brunel().WithMC       = True  # set to False if real data or to ignore MC truth
Brunel().InputType    = "DIGI"# or "MDF" or "ETC" or "RDST" or "DST"
Brunel().OutputType   = "DST" # or "RDST" or "NONE"
Brunel().DatasetName  = datasetName     # string used to build I/O file names
Brunel().DataType     = "2010"
Brunel().SpecialData += ["veloOpen","earlyData","microBiasTrigger"]
Brunel().WithMC       = True   #  
Brunel().Simulation   = True   #  

# special for Velo tracks
from Configurables import TrackPrepareVelo, TrackBuildCloneTable, TrackCloneCleaner,TrackSys,TrackAssociator
# TrackSys().FastVelo = True
TrackPrepareVelo().bestLocation = ""
cloneTable = TrackBuildCloneTable("FindTrackClones_Velo")
cloneTable.maxDz   = 500*mm
cloneTable.zStates = [ 0*mm, 990*mm]
cloneTable.klCut   = 5e3
cloneTable.inputLocation = "Rec/Track/PreparedVelo"
cloneCleaner = TrackCloneCleaner("FlagTrackClones_Velo")
cloneCleaner.CloneCut = 5e3
cloneCleaner.inputLocation = "Rec/Track/PreparedVelo"
GaudiSequencer("TrackClonesSeq").Members  += [ cloneTable, cloneCleaner ]
TrackAssociator()
#

#-- File catalogs. First one is read-write
BrunelRoot = os.environ["BRUNELROOT"]
FileCatalog().Catalogs = [ "xmlcatalog_file:MyCatalog.xml",
                           "xmlcatalog_file:" + BrunelRoot + "/job/NewCatalog.xml" ]

EventSelector().Input = ["DATAFILE='PFN:" + Brunel().DatasetName + ".digi' TYP='POOL_ROOTTREE' OPT='READ'"]

OutputStream("DstWriter").Output = "DATAFILE='PFN:" + Brunel().DatasetName  + ".dst' TYP='POOL_ROOTTREE' OPT='REC'"
HistogramPersistencySvc().OutputFile = Brunel().DatasetName  + '_Brunel.root'
OutputStream("DstWriter").ItemList+= ["/Event/Rec/Track/PreparedVelo#999"] 
OutputStream("DstWriter").ItemList+= ["/Event/Link/Rec/Track/Velo#999"] 
OutputStream("DstWriter").ItemList+= ["/Event/Link/Rec/Track/VeloInv#999"] 

appConf = ApplicationMgr( OutputLevel = INFO, AppName = 'myBrunel' )

import GaudiPython

appMgr = GaudiPython.AppMgr()
evt = appMgr.evtSvc()

while 1>0:
 appMgr.run(1)
 if not evt['MC/Header'] : break

