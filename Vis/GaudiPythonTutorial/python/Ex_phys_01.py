########################################################################
from Gaudi.Configuration import *
########################################################################
#
# Standard configuration
#
from Configurables import DaVinci
DaVinci().DataType     = "MC09"               
DaVinci().Simulation   = True
DaVinci().HistogramFile = "DVHistos_1.root"    # Histogram file
DaVinci().TupleFile = "DVNtuples.root"         # Ntuple
# DaVinci().MainOptions  = "" # None
########################################################################
# some input files
EventSelector().Input   = [
"DATAFILE='castor:/castor/cern.ch/grid/lhcb/MC/MC09/DST/00004879/0000/00004879_00000001_1.dst' TYP='POOL_ROOTTREE' OPT='READ'"]

from GaudiPython import AppMgr
appMgr = AppMgr()
evt=appMgr.evtsvc()
#
appMgr.run(1)
evt.dump()
