import GaudiPython
print 'import pyMon'

appMgr = GaudiPython.AppMgr()
class MyAlg(GaudiPython.PyAlgorithm):
 def initialize(self):
## book AIDA histogram
     print 'execute initialize'
     return True
 def execute(self):
    print 'execute event loop'
    print GaudiPython.AppMgr().evtsvc()['Rec/Header']
    return True
 def finalize(self):
    print 'execute finalize'
    return True
