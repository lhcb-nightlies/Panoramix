import os,sys
from ROOT import *
import GaudiPython
appMgr = GaudiPython.AppMgr()

if len(sys.argv) > 1 : 
 fn = os.path.expandvars(sys.argv[1])
else:
 print 'missing file name, stop'
 sys.exit()    

xx=TFile()
# f.Open('root://eoslhcb.cern.ch//eos/lhcb/LHCb/Collision12/DIMUON.DST/00020198/0002/00020198_00026690_1.dimuon.dst')
# f.Open('rfio:/castor/cern.ch//user/t/truf/SEMILEPTONIC_s17b.DST/DOWN/microDOWN_250.mdst')
if not fn.find('castor')<0:
 f=xx.Open('rfio:'+fn)
elif not fn.find('eoslhcb')<0:
 if fn.find('root')<0: f=xx.Open('root:'+fn)
 else:                 f=xx.Open(fn)
else:
 f=TFile(fn)
print fn
key = 'Event'
ev  = f.Get(key)
if ev: 
 ev.GetEntry(0)
 lf = ev.GetLeaf('_Event_Rec_Header.LHCb::ProcessHeader.m_condDBTags.first')
 lf.PrintValue(7)
 lf = ev.GetLeaf('_Event_Rec_Header.LHCb::ProcessHeader.m_condDBTags.second')
 lf.PrintValue(7)
else: 
# maybe old POOL produced DST, try 
 key = '_Event_Rec_Header'
 ev  = f.Get(key)
 if ev: 
  ev.GetEntry(0)
  br = ev.GetBranch('RecHeader')
  lf = br.FindBranch('m_condDBTags.first')
  lf.PrintValue(7)
  lf = br.FindBranch('m_condDBTags.second')
  lf.PrintValue(7)
 else: 
  print 'ExtractDataBaseTags: event key not found'

