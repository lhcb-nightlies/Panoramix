# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2008"
lhcbApp.DataType = "DC06"

# Default options to rerun L0 
from Configurables import L0DUAlg,bankKiller,L0DUFromRawAlg,L0MuonCandidatesFromRaw,L0CaloCandidatesFromRaw

myL0DUseq = GaudiSequencer("seqL0DU")
appConf.TopAlg += [ myL0DUseq ]
removeL0DUBank = bankKiller('RemoveL0DUBank')
removeL0DUBank.BankTypes =["L0DU"]
##myL0DUseq.Members+=[ removeL0DUBank,"L0MuonAlg/L0Muon","L0CaloAlg/L0Calo","PuVetoAlg/L0PuVeto"]
##myL0DUseq.Members+=[ L0DUFromRawAlg(),removeL0DUBank]

myL0Muon = L0MuonCandidatesFromRaw('L0MuonFromRaw')
myL0Muon.WriteL0ProcData = True
myL0DUseq.Members+=[ myL0Muon,"L0CaloAlg/L0Calo","PuVetoAlg/L0PuVeto",removeL0DUBank]

def doMyChanges():
 del DataOnDemandSvc().AlgMap['Trig/L0/MuonCtrl']
appendPostConfigAction(doMyChanges)

l0du     =  L0DUAlg('L0DU')
l0du.TCK = '0xFFF8'
myL0DUseq.Members+=[ l0du ]
   
import GaudiPython
from GaudiPython import gbl

files = []
file  = '/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00002019/DST/0000/00002019_00000XXX_5.dst'
if lhcbApp.DataType == '2008' : 
 file  = '/castor/cern.ch/grid/lhcb/MC/2008/DST/00003991/0000/00003991_00000XXX_5.dst'
import os
for n in range(1,5) : 
  ff = file.replace('XXX','%(X)03d'%{'X':n})
  x  = os.system('nsls '+ff)
  if x == 0 :  files.append("DATA='"+ff+"'  TYP='POOL_ROOTTREE' OPT='READ'")
EventSelector(Input = files)

appMgr = GaudiPython.AppMgr()
evt  = appMgr.evtsvc()

for n in range(3) : 
 appMgr.run(1)
 if not evt['Rec/Header']  : break  # probably end of input
 # check L0
 L0dir = evt['Trig/L0/L0DUReport']
 print L0dir
