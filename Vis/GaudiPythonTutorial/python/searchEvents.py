import sys,os
castor = True
Lambda = [
[89535,78950579],
[89535,79723476]
]

def check():
 rc = False
 evtr = evt['DAQ/ODIN'].eventNumber()
 runr = evt['DAQ/ODIN'].runNumber()
 for l in Lambda:
   if l[0]==runr and l[1]==evtr : 
        rc = True 
        break 
 return rc

if not castor:
 path = '/media/Work/'
 files = []
 dirs = ['00005842/0000/','00005843/0000/','00005844/0000/','00005845/0000/']
 for dd in dirs:
   for ff in os.listdir(path+dd):
     files.append( path+dd+ff)
else: 
 files = [ '/castor/cern.ch/grid/lhcb/LHCb/Collision11/SEMILEPTONIC.DST/00016781/0000/00016781_00000018_1.semileptonic.dst',
           '/castor/cern.ch/grid/lhcb/LHCb/Collision11/SEMILEPTONIC.DST/00016781/0000/00016781_00000016_1.semileptonic.dst']

# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"

mywriter          = InputCopyStream('DstWriter')
mywriter.Output   = "DATAFILE='PFN:selEvents.dst' 'SVC='Gaudi::RootCnvSvc' OPT='REC' "


appConf    = ApplicationMgr( OutputLevel = INFO, AppName = 'stripEvents') 
appConf.OutStream = [mywriter]
EventSelector().PrintFreq = 10000
    
import GaudiPython
appMgr = GaudiPython.AppMgr()
appMgr.initialize()
sel    = appMgr.evtsel()
evt    = appMgr.evtsvc()
sel.open(files) 

appMgr.algorithm('DstWriter').Enable = False

while 1>0:
 appMgr.run(1)
 if not evt['Rec/Header'] : break 
 if check() : 
     rc = appMgr.algorithm('DstWriter').execute() # output event             

