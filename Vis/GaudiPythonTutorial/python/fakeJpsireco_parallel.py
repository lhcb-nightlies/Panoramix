# setenv PYTHONPATH /afs/cern.ch/sw/lcg/external/processing/0.51/${CMTCONFIG}/lib/python2.5/site-packages:$PYTHONPATH
from ROOT import TH1F, TH2F, TBrowser, TCanvas, MakeNullPointer,gStyle,gROOT
import math
gStyle.SetPalette(1)     

#dataset = 'BsJpsiPhi'
dataset = 'inclJpsi'

if dataset == 'inclJpsi' : 
 f_2008   = False
else: 
 f_2008   = True
debug    = True

# get the basic configuration from here
from Gaudi.Configuration import *
from GaudiPython.Parallel import Task, WorkManager
import GaudiKernel.SystemOfUnits as units
from GaudiPython import AppMgr,gbl
import GaudiPython
from LHCbConfig import *

if not f_2008:
    lhcbApp.DataType = "DC06"
else:
    lhcbApp.DataType = "2008"
    lhcbApp.Simulation = True     

ParticleID = gbl.LHCb.ParticleID
part       = MakeNullPointer(gbl.LHCb.MCParticle)
mysqrt     = gbl.Math.sqrt
MCParticle = gbl.LHCb.MCParticle
Track      = gbl.LHCb.Track
LorentzVector  = gbl.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')
stableParticles = [13]

class MyTask(Task):
  
  def initializeLocal(self):
    name  = 'J/psi'
    self.output = {
     name+'_mass' :  TH1F(name+'_mass','mass of '+name,400,2.5,3.5),
     name+'_eff'  :  TH1F(name+'_eff','efficiency of '+name,400,2.5,3.5),
     'h_gen'      :  TH1F('h_gen','nr of generated Jpsi',1,0.,1.),
     'h_etapt'    :  TH2F('h_etapt','pt vs. eta for reco Jpsi',100,1.,6.,100,0.0,10.0),
     'h_etapt_gen':  TH2F('h_etapt_gen','pt vs. eta for generated Jpsi',100,1.,6.,100,0.0,10.0)
     }

  def initializeRemote(self):        
    #--- Get the basic configuration for each remote worker
    #appConf.OutputLevel = INFO
    #appConf.AppName     = 'fakeJpsireco'
    EventSelector().PrintFreq = 100
 
  def process(self, file) :
   print '+++++ Start with file ',file
   appMgr = AppMgr()
   sel  = appMgr.evtsel()
   sel.open(file)
   evt  = appMgr.evtsvc()
   from LinkerInstances.eventassoc import linkedFrom, linkedTo  
   partsvc = appMgr.ppSvc()

   MCDecayFinder  = appMgr.toolsvc().create('MCDecayFinder', interface='IMCDecayFinder')
   MCDecayFinder.setDecay('J/psi(1S)')
   MCDebugTool             = appMgr.toolsvc().create('PrintMCDecayTreeTool', interface='IPrintMCDecayTreeTool')

   nevents = 0
   while 1>0: 
    appMgr.run(1)
    if not evt['Rec/Header'] : break
    nevents+=1
    if debug and nevents > 10: break 
# look at MC truth
    mc = evt['MC/Particles']
    if not MCDecayFinder.hasDecay(mc) : continue
    lfrommc = linkedFrom(Track,MCParticle,'Rec/Track/Best')
    decaylist = []
    while MCDecayFinder.findDecay(mc,part)>0 :
     print 'decayfinder'
     pclone = part.clone()
     GaudiPython.setOwnership(pclone,True)
     decaylist.append(pclone)
    for decay in decaylist :
      print 'decay',len(decaylist)
      pid = decay.particleID().pid()
      self.output['h_gen'].Fill(1) 
      mc_eta = decay.momentum().eta()
      mc_pt  = decay.pt()
      self.output['h_etapt_gen'].Fill(mc_eta,mc_pt/units.GeV)
      daughters = GaudiPython.gbl.std.vector('const LHCb::MCParticle*')()
      MCDecayFinder.descendants(decay,daughters)
      recoparticle = []
      if debug : MCDebugTool.printTree(decay,5)
      for dpart in daughters : 
# check if e, muon, pion, kaon, proton 
       stable = False
       for s in stableParticles : 
        if dpart.particleID().abspid() == s : stable = True
       if not stable : continue   
# check if origin is close to beam line and inside Velo
       oVx = dpart.originVertex()
       pos = oVx.position()
       if pos.rho()    > 10. : continue
       if abs(pos.z()) > 40. : continue
# another check don't accept MCParticles with endVertices before T
       if dpart.endVertices().size() > 0 :
        eVx = dpart.endVertices()[0].position()
       if eVx.z() < 7000. : continue
# check if reconstructed
       tcand = []   
       for tr in lfrommc.range(dpart): 
# check if long track
        if tr.type() == tr.Long : 
# additional problem, decays in flight: also MC decay products are matched with reco track 
# avoid adding twice the same track   
         exist = False
         trkey = tr.key()
         for t in tcand : 
          if t.key() == trkey : 
            exist = True
            break
         if not exist : 
          tcand.append(tr)     
       if len(tcand) == 0 : continue
# if more than one candidate, choose closest to MC truth     
       if len(tcand) > 1 : 
         pmom = dpart.momentum().r()
         diff = [999999.,-1]
         for tr in tcand: 
          delta = abs(pmom - tr.p())
          if delta < diff[0] : 
            diff[0] = delta 
            diff[1] = tr   
         if diff[1] == -1 : print 'this is impossible, BIG ERROR!'
         tcand[0] = diff[1]
# fake PID:
       pid = dpart.particleID().abspid()
       m   = partsvc.find(ParticleID(pid)).mass()
       p   = tcand[0].momentum() 
       rsq = m*m + p.Mag2()
       E   = mysqrt(rsq)
       lv = LorentzVector(p.x(),p.y(),p.z(),E)
       fourmomenta = [lv,dpart.momentum()]
       recoparticle.append(fourmomenta)  
      print 'until here 3 ?',len(recoparticle)
      if len(recoparticle) < 2 : continue
# ignore B field in Velo close to IP, add up 4 momenta 
      bmom   = recoparticle[0][0] 
      bmommc = recoparticle[0][1] 
      for t in range(1,len(recoparticle)): 
       bmom  += recoparticle[t][0]
       bmommc+= recoparticle[t][1]
# fill histogram with B mass
       self.output[name+'_mass'].Fill(bmom.mass()/units.GeV) 
       delmass = abs(bmommc.mass() - decay.momentum().mass())
       if delmass < 50.*units.MeV :  
        self.output[name+'_eff'].Fill(bmom.mass()/units.GeV) 
        self.output['h_etapt'].Fill(mc_eta,mc_pt/units.GeV)
   appMgr.stop()

  def finalize(self): 
    f=TFile('DarkMatter.root','recreate')
    for h in self.output.keys():
     histo = self.output[h]
     histo.Write()
     print histo.GetTitle(),':',histo.GetEntries()
    f.Close()



if __name__ == '__main__' :
 nmax = 100
 if debug: nmax = 5

 if dataset == 'BsJpsiPhi' : 
  if f_2008: 
   files = []
   file  = '/castor/cern.ch/grid/lhcb/MC/2008/DST/00003783/0000/00003783_00000XXX_5.dst'
   for n in range(1,nmax) :
    ff = file.replace('XXX','%(X)03d'%{'X':n})
    x  = os.system('nsls '+ff)
    if x == 0 :  files.append(ff)
 if dataset == 'inclJpsi' : 
   files = []
   file  = '/castor/cern.ch/grid/lhcb/production/DC06/v1r0/00002070/DST/0000/00002070_0000XXXX_2.dst'
   for n in range(1,nmax) :
    ff = file.replace('XXXX','%(X)04d'%{'X':n})
    x  = os.system('nsls '+ff)
    if x == 0 :  files.append(ff)  #------Use up to 8 processes 


 task = MyTask()
 if not debug : 
   wmgr = WorkManager(ncpus = 8)
   wmgr.process(task,files) 
 else : 
    task.initializeLocal()
    task.initializeRemote()
    task.process(files[0])
    task.finalize()
 histos = task.output

from ROOT import TText   
tc = TCanvas('mass','masses',700,400)
histos['J/psi_mass'].Draw()
  
tceff = TCanvas('eff','masses with missing m < 50 MeV',700,400)
h = 'J/psi_eff'
histos[h].Draw()
nentries = histos[h].GetEntries()
if nentries > 25 : histos[h].Fit('gaus')
# generated J/Psi 
gen = histos['h_gen'].GetEntries()
eff = 100.*float(nentries)/float(gen) 
txt = 'Reconstructed J/Psi = %3.2E %s'%(eff,'%')
y = 0.5 * histos[h].GetMaximum()
x = 2.6
tx  = TText(x,y,txt)
tx.DrawText(x,y,txt)

eff_etapt = histos['h_etapt'].Clone()
eff_etapt.Divide(histos['h_etapt_gen'])



