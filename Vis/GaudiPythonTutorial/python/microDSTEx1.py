# Imports and typedefs
from ROOT import TCanvas, TH1F, TH2F
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "MC09"

# basic input
selection_name = 'Bs2JpsiPhi'
input_name    = 'micro.'+selection_name+'Seq.mdst'

appConf = ApplicationMgr( OutputLevel = INFO )
EventSelector().PrintFreq = 10000
candidates = 'Phys/SelBs2JpsiPhi/Particles'

import GaudiPython
import GaudiKernel.SystemOfUnits as units
locationRoot = '/Event/MicroDST/'

appMgr = GaudiPython.AppMgr()
sel   = appMgr.evtSel()
evt   = appMgr.evtSvc()
tools = appMgr.toolsvc()

partSvc = appMgr.ppSvc()

h_bmass=TH1F('h_bmass','B mass',100,5.1,5.6)
h_b          = TH1F('h_b','B multiplicity',10,-0.5,9.5)

properTimeFitter = tools.create('PropertimeFitter',interface='ILifetimeFitter')

sel.open('$GAUDIPYTHONTUTORIALROOT/python/'+input_name)

def my_loop():
 while 0<1 :
  appMgr.run(1)
  if not evt[locationRoot+'Rec/Header'] : break
  mybees = evt[locationRoot+candidates]
  if mybees  :
   h_b.Fill(evt[locationRoot+candidates].size())
   for b in mybees :
    rc=h_bmass.Fill(b.measuredMass()/units.GeV)

import time
start =  time.clock()
my_loop()  
end   =  time.clock()

from gaudigadgets import panorewind
panorewind()
nevents = 0
while 0<1 :
  appMgr.run(1)
  if not evt[locationRoot+'Rec/Header']  : break
  nevents+=1
  
print 'time used per event', (end - start)/float(nevents)
h_bmass.Draw() 

_ = raw_input('press enter to continue...')

h_bmass_vs_pt= TH2F('h_bmass_vs_pt','B mass vs pt',100,0.,10.,100,5.0,5.4)

def my_loop2(nevents):
 for n in range(nevents) :
  appMgr.run(1)
  if not evt[locationRoot+'Rec/Header']  : break
  mybees = evt[locationRoot+candidates]
  if mybees  :
   for b in mybees :
    rc=h_bmass_vs_pt.Fill(b.pt()/units.GeV,b.measuredMass()/units.GeV)

print h_b.GetEntries(),h_b.GetMean()
panorewind()
start =  time.clock()
my_loop2(nevents)  
end   =  time.clock()
print 'time used per event', (end - start)/float(nevents)
h_bmass_vs_pt.Draw('box') 

for n in range(10):
 panorewind()
 start =  time.clock()
 my_loop2(nevents)  
 end   =  time.clock()
 print 'time used per event', (end - start)/float(nevents)

