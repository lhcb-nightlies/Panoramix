import os
filename = None
if len(os.sys.argv) > 2 : 
 filename  = os.sys.argv[1]
 outname   = os.sys.argv[2]
else :
 print 'use default file name'

if filename: 
  files = [filename]
else: 
  files = ['/media/Work/00005731_00000042_1.dst']
  outname = 'test.dst'

raw = files[0].find('.mdf')>0 or files[0].find('.raw')>0

# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2009"
EventSelector().PrintFreq = 10000

# remove Velo non zero suppressed banks
from Configurables import bankKiller

removeVeloFullBank = bankKiller('RemoveVeloFullBank')
removeVeloFullBank.BankTypes =["VeloFull","L0PUFull"]
appConf.TopAlg += [ removeVeloFullBank ]

output = OutputStream("myDstWriter")
output.Output = "DATAFILE='PFN:"+outname+"' TYP='POOL_ROOTTREE' OPT='REC'"
strip = files[0].find('V0')>-1
if not raw and strip : output.ItemList = ["/Event/pRec#999","/Event/Rec#999","/Event/DAQ#999","/Event/Strip#999"]
if not raw and not strip : output.ItemList = ["/Event/pRec#999","/Event/Rec#999","/Event/DAQ#999"]


else: output.ItemList = ["/Event/DAQ#999"]
appConf.OutStream.append("myDstWriter")   
appConf.OutputLevel = INFO
appConf.AppName     = 'StripOfVeloFull'

import GaudiPython
from GaudiPython import gbl

#files = []
#path = '/media/Work'
#allfiles = os.listdir(path)
#for f in allfiles :
#  if f.find('.dst')>-1 : files.append(path+'/'+f)

appMgr = GaudiPython.AppMgr()
evt    = appMgr.evtsvc()
sel    = appMgr.evtsel()
appMgr.initialize()

sel.open(files) 

appMgr.algorithm('myDstWriter').Enable              = False
appMgr.algorithm('RemoveVeloFullBank').Enable       = False

n = 0
while 1>0 : 
 appMgr.run(1)
 if not evt['Rec/Header']  : break  # probably end of input
 try:
  notlumiExclusive = evt['DAQ/RawEvent'].banks(53)[0].data()[1]&0x4
 except:
  notlumiExclusive = False
 if not notlumiExclusive: continue

 ss = gaudigadgets.nodes(evt,True)
 appMgr.algorithm('RemoveVeloFullBank').execute()
 appMgr.algorithm('myDstWriter').execute()
 n+=1
