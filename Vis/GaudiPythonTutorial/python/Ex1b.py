# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"

appConf = ApplicationMgr(OutputLevel = INFO)

import GaudiPython
# load some additional gadgets
import gaudigadgets

appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst']) 
evt = appMgr.evtsvc()
# read one event
appMgr.run(1)

# read all from Rec
recnodes = gaudigadgets.nodes(evt,True,'Rec')
print recnodes
# dump TES
evt.dump()
# inspect Primary vertex:
print 'number of primary vertices:',evt['Rec/Vertex/Primary'].size()
print evt['Rec/Vertex/Primary'][0]
print evt['Rec/Header']





