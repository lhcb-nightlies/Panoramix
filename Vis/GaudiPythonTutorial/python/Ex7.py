from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"
    
appConf = ApplicationMgr(AppName = 'Ex7',OutputLevel = INFO) 
appConf.ExtSvc += ['TransportSvc']

import GaudiPython

appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
det    = appMgr.detSvc() 
lhcb   = det['/dd/Structure/LHCb']
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst']) 
appMgr.run(1)

tranSvc = appMgr.service('TransportSvc','ITransportSvc')
gbl = GaudiPython.gbl
XYZPoint    = LHCbMath.XYZPoint
XYZVector   = LHCbMath.XYZVector

a = XYZPoint(0,0,0)
b = XYZPoint(500.,350.,2000.)
radlength = tranSvc.distanceInRadUnits(a,b)
print 'radiation length between point a and b = %3.4F'%(radlength)
