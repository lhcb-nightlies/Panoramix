from ROOT import TH1F, TBrowser, TCanvas
from LHCbConfig import *
lhcbApp.DataType   = "2010"
lhcbApp.Simulation = True

appConf = ApplicationMgr(OutputLevel = INFO, AppName     = 'Ex3d')
 
import GaudiPython
# load some additional gadget
from gaudigadgets import panorewind

appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/MC2010_BsJpsiPhi__00008919_00000068_1.dst']) 
evt = appMgr.evtsvc()

hpt = TH1F('hpt','pt of bees',100,0.,10000.)  
for n in range(20) :
    appMgr.run(1)  
    mc  = evt['MC/Particles']
    for mcp in mc :
        if mcp.particleID().hasBottom() : 
            result = hpt.Fill( mcp.pt() )
hpt.Draw()    # plot Root histogram

_ = raw_input('rewind and start again, press enter to continue...')

panorewind()  # start again from first event
hpt2 = TH1F('hpt2','pt of charm',100,0.,10000.)
for n in range(20) :
    appMgr.run(1)
    mc  = evt['MC/Particles']
    for mcp in mc :
        if mcp.particleID().hasCharm() :
            result = hpt2.Fill( mcp.pt() )
hpt2.SetLineColor(2)
hpt2.Draw('same')




