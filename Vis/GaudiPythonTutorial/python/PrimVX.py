bincl = False

from ROOT import TH1F, TH2F, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"

appConf = ApplicationMgr( OutputLevel = 4, AppName = 'PrimVX' )
EventSelector().PrintFreq = 1000

from Configurables import PatPV3D,PVOfflineTool

PVAside = PatPV3D("PVAside")
PVCside = PatPV3D("PVCside")
PVAside.addTool( PVOfflineTool )
PVCside.addTool( PVOfflineTool )

PVAside.PVOfflineTool.InputTracks = ["Rec/Track/Aside"]
PVAside.PVOfflineTool.PVFitterName = "LSAdaptPV3DFitter"
PVAside.PVOfflineTool.PVSeedingName = "PVSeed3DTool"
PVAside.OutputVerticesName = "Rec/Vertices/Aside"
PVCside.PVOfflineTool.InputTracks = ["Rec/Track/Cside"]
PVCside.PVOfflineTool.PVFitterName = "LSAdaptPV3DFitter"
PVCside.PVOfflineTool.PVSeedingName = "PVSeed3DTool"
PVCside.OutputVerticesName = "Rec/Vertices/Cside"

# velo tracking
from TrackSys.Configuration import *
from Configurables import (DecodeVeloRawBuffer, Tf__PatVeloRTracking, Tf__PatVeloSpaceTool,
                            Tf__PatVeloSpaceTracking, Tf__PatVeloGeneralTracking,
                            Tf__PatVeloTrackTool)                         

## Start TransportSvc, needed by track fit
appConf.ExtSvc.append("TransportSvc")

DecodeVeloRawBuffer  = DecodeVeloRawBuffer()  
DecodeVeloRawBuffer.DecodeToVeloClusters     = True
patVeloRTracking     = Tf__PatVeloRTracking("PatVeloRTracking")
patVeloSpaceTracking = Tf__PatVeloSpaceTracking("PatVeloSpaceTracking")
patVeloSpaceTracking.addTool( Tf__PatVeloSpaceTool(), name="PatVeloSpaceTool" )
patVeloSpaceTracking.PatVeloSpaceTool.MarkClustersUsed = True

mySeq = GaudiSequencer('mySeq')
mySeq.Members += [DecodeVeloRawBuffer,patVeloRTracking,patVeloSpaceTracking,"PatPVOffline/PatPVOffline",PVAside,PVCside]

appConf.TopAlg += ["GaudiSequencer/mySeq"]

import GaudiPython
newcont = GaudiPython.makeClass('KeyedContainer<LHCb::Track,Containers::KeyedObjectManager<Containers::hashmap> >')
import GaudiKernel.SystemOfUnits as units

appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
files = ['$PANORAMIXDATA/Bu2JpsiK_00011917_00000048_1.dst']
sel.open(files)

evt = appMgr.evtsvc()

appMgr.start()
appMgr.algorithm('PVAside').Enable = False
appMgr.algorithm('PVCside').Enable = False

pva  = appMgr.toolsvc().create('PVOfflineTool', interface='IPVOfflineTool')

h_dist_x  =   TH2F('h_dist_x', ' delta x', 100,-0.3,0.3,80,-20.,20.)      
h_dist_y  =   TH2F('h_dist_y', ' delta y', 100,-0.3,0.3,80,-20.,20.)      
h_dist_z  =   TH2F('h_dist_z', ' delta z', 100,-1.,1.,80,-20.,20.)      

x=1  
while x>0 : 
 appMgr.run(1)
 if not evt['Rec/Header'] : break 
 Aside = newcont()
 GaudiPython.setOwnership(Aside,False)
 Cside = newcont()
 GaudiPython.setOwnership(Cside,False)
 for t in evt['Rec/Track/Velo']:
   s = t.states()
   tnew = t.clone()
   GaudiPython.setOwnership(tnew,False)
   if s[1].x() > 0 : 
     Aside.add(tnew)
   # copy to A side
   else : 
     Cside.add(tnew)
   # copy to C side
 evt.registerObject('/Event/Rec/Track/Aside',Aside)
 evt.registerObject('/Event/Rec/Track/Cside',Cside)
 appMgr.algorithm('PVAside').execute()
 appMgr.algorithm('PVCside').execute()
 for pvA in evt['Rec/Vertices/Aside']:
   posA = pvA.position()
   for pvC in  evt['Rec/Vertices/Cside']: 
     posC = pvC.position()
     if abs(posA.z()-posC.z()) < 0.5 : 
       z = (posA.z()+posC.z())/2.
       sc = h_dist_x.Fill(posA.x()-posC.x(),z)
       sc = h_dist_y.Fill(posA.y()-posC.y(),z)
       sc = h_dist_z.Fill(posA.z()-posC.z(),z)
   
# analysis part
hprojx = h_dist_x.ProjectionX()
hprojy = h_dist_y.ProjectionX()
hprojz = h_dist_z.ProjectionX()
meanx_funz = h_dist_x.ProfileY()   
meany_funz = h_dist_y.ProfileY()      
meanz_funz = h_dist_z.ProfileY()      
meanx_funz.Fit('pol1')
meany_funz.Fit('pol1')
meanz_funz.Fit('pol1')
hprojx.Fit('gaus')
hprojy.Fit('gaus')
hprojz.Fit('gaus')

fx=meanx_funz.GetFunction('pol1') 
fy=meany_funz.GetFunction('pol1')   
fz=meanz_funz.GetFunction('pol1')   

delx =  fx.GetParameter(0)/units.micrometer   
slx  =  fx.GetParameter(1)/units.micrometer   
dely =  fy.GetParameter(0)/units.micrometer
sly  =  fy.GetParameter(1)/units.micrometer
delz =  fz.GetParameter(0)/units.micrometer
slz  =  fz.GetParameter(1)/units.micrometer

edelx =  fx.GetParError(0)/units.micrometer
eslx  =  fx.GetParError(1)/units.micrometer  
edely =  fy.GetParError(0)/units.micrometer   
esly  =  fy.GetParError(1)/units.micrometer  
edelz =  fz.GetParError(0)/units.micrometer   
eslz  =  fz.GetParError(1)/units.micrometer  

print 'distance in x: %5.3F +/- %5.3F'%(delx,edelx) 
print 'distance in y: %5.3F +/- %5.3F'%(dely,edely) 
print 'distance in z: %5.3F +/- %5.3F'%(delz,edelz) 
print 'slope in x: %5.3F +/- %5.3F'%(slx,eslx) 
print 'slope in y: %5.3F +/- %5.3F'%(sly,esly) 
print 'slope in z: %5.3F +/- %5.3F'%(slz,eslz) 

tc = TCanvas('tc','Alignment of Velo Halves',900,900)
meanx_funz.SetMinimum(-0.015)
meany_funz.SetMinimum(-0.015)
meanx_funz.SetMaximum(0.015)
meany_funz.SetMaximum(0.015)

tc.Divide(2,3)    
tc.cd(1)
hprojx.Draw()
tc.cd(2)
hprojy.Draw()
tc.cd(3)
hprojz.Draw()
tc.cd(4)
meanx_funz.Draw()
tc.cd(5)
meany_funz.Draw()
tc.cd(6)
meanz_funz.Draw()
