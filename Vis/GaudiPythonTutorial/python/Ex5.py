import GaudiPython
LHCbMath    = GaudiPython.gbl.Math
XYZPoint    = LHCbMath.XYZPoint
XYZVector   = LHCbMath.XYZVector
# Instantiation of the objects:
aPoint   = XYZPoint(0.,0.,10.)
aVector  = XYZVector(1.,1.,5.)

print aVector.x(), aVector.y(), aVector.z()

