# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2010"
lhcbApp.Simulation = True
lhcbApp.DDDBtag   =  "head-20101206"
lhcbApp.CondDBtag =  "sim-20101210-vc-md100" 

appConf = ApplicationMgr( OutputLevel = INFO, AppName = 'TGhost')

import GaudiPython
appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/MC2010_BsJpsiPhi__00008919_00000068_1.dst'])
evt = appMgr.evtsvc()
tsvc  = appMgr.toolsvc()
longGhost = tsvc.create('LongGhostClassification',interface='ITrackGhostClassification')
print longGhost.type()
info  = GaudiPython.gbl.LHCb.GhostTrackInfo()

from LinkerInstances.eventassoc import *  
MCParticle = GaudiPython.gbl.LHCb.MCParticle
Track     = GaudiPython.gbl.LHCb.Track

appMgr.run(1)

####MC relation
ltrack2part = linkedTo(MCParticle,Track,'Rec/Track/Best')

aTrack = evt['Rec/Track/Best'][12]
longGhost.info(aTrack,info)

