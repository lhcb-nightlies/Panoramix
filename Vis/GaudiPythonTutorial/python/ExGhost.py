# setenv PYTHONPATH /afs/cern.ch/sw/lcg/external/processing/0.51/${CMTCONFIG}/lib/python2.5/site-packages:$PYTHONPATH
from ROOT import TFile,TH1F,TH2F,TCanvas,TBrowser,gStyle,TText,TMath,TF1,gROOT,MakeNullPointer,gSystem
f_2008   = False
debug    = False

# get the basic configuration from here
from LHCbConfig import *
if not f_2008:
    lhcbApp.DataType = "DC06"
else:
    lhcbApp.DataType = "2008"
    lhcbApp.Simulation = True     

appConf = ApplicationMgr( OutputLevel = INFO, AppName = 'ExGhost' )

# new way to run Hlt
from Configurables import GaudiSequencer, HltConf 
ApplicationMgr().ExtSvc += [ "LoKiSvc" ]
HltConf().hltType = 'Hlt1'
# Default options to rerun L0          
if lhcbApp.DataType == 'DC06' :  
 l0seq = GaudiSequencer("seqL0")
 appConf.TopAlg += [ l0seq ]
 L0Conf().setProp( "L0Sequencer", l0seq )
 L0Conf().setProp( "ReplaceL0BanksWithEmulated", True ) 
appConf.TopAlg += [ GaudiSequencer('Hlt') ]


EventSelector().PrintFreq  = 100

import GaudiPython
import GaudiKernel.SystemOfUnits as units

gbl = GaudiPython.gbl
MCParticle   = gbl.LHCb.MCParticle
Track        = gbl.LHCb.Track
VeloCluster  = gbl.LHCb.VeloCluster
XYZPoint     = LHCbMath.XYZPoint

import gaudigadgets
enums = gaudigadgets.getEnumNames('LHCb::Track')

## trackcontainer = 'Rec/Track/Velo'
trackcontainer = 'Hlt1/Track/Forward'

h_rz     = TH1F('h_rz','max fraction of rz clusters',100,-0.1,1.1)
h_rznoe  = TH1F('h_rznoe','max fraction of rz clusters, no electrons',100,-0.1,1.1)
h_rznoe1 = TH1F('h_rznoe1','max fraction of rz clusters, no electrons, 1 ghost',100,-0.1,1.1)
h_3d     = TH1F('h_3d','max fraction of 3d clusters',100,-0.1,1.1)
h_3dxy   = TH2F('h_3dxy','tx / ty of velo ghosts',100,-0.4,0.4,100,-0.4,0.4)
h_xy     = TH2F('h_xy','tx / ty all tracks',100,-0.4,0.4,100,-0.4,0.4)

myHistos = {}
for h in gROOT.GetList() : 
    myHistos[h.GetName()] = h

def findGhost(evt,ppSvc,linkedTo,htemp):
 mc = evt['MC/Particles']
 vc = evt['Raw/Velo/Clusters']
 tr = evt[trackcontainer]
 if not tr : return False 
 clu2mc  = linkedTo(MCParticle,VeloCluster,'Raw/Velo/Clusters')   
 nghost = 0
 for t in tr : 
  if t.checkFlag(t.Backward) : continue
  ss = 0
  ssrz = 0
  match = {}
  matchrz = {}
  for l in t.lhcbIDs():
   if not l.isVelo() : continue
   ss +=1
   lv = l.veloID()
   cl = vc.containedObject(lv.channelID())
   for m in clu2mc.range(cl) :
     if match.has_key(m.key()) :   match[m.key()]+=1
     else                      :   match[m.key()] =1  
     if cl.isRType()   :  
      ssrz+=1
      if matchrz.has_key(m.key()) :   matchrz[m.key()]+=1
      else                      :   matchrz[m.key()] =1     
  mlist = match.values()
  mlist.sort(reverse=True)
  maxl = 0
  if len(mlist)>0 : maxl = mlist[0]
  mlistrz = matchrz.values()
  mlistrz.sort(reverse=True)
  maxrz = 0
  if len(mlistrz)>0 : maxrz = mlistrz[0]
  fst = t.firstState()
  htemp['h_3d'].Fill(float(maxl)/float(ss))
  test1 = 0
  if maxl > 3 : htemp['h_xy'].Fill(fst.tx(),fst.ty())
  r3d = float(maxl)/float(ss)
  rrz = float(maxrz)/float(ssrz)     
  if r3d < 0.7 : 
   htemp['h_rz'].Fill(rrz)
   for m in matchrz :
    if mc[m].particleID().abspid() == 11 : test1 = 11
   if test1 != 11 : htemp['h_rznoe'].Fill(rrz)
   htemp['h_3dxy'].Fill(fst.tx(),fst.ty())   
   nghost+=1
 if nghost==1 and test1 != 11 : htemp['h_rznoe1'].Fill(rrz) 
 return

def processFile(file) :
 print '+++++ Start with file ',file
 htemp = {}
 for h in myHistos.keys() : 
    htemp[h] = myHistos[h].Clone()
     
 appMgr = GaudiPython.AppMgr()
 ppSvc = appMgr.ppSvc()
 sel   = appMgr.evtsel()
 sel.open(file)
 evt  = appMgr.evtsvc()
 from LinkerInstances.eventassoc import linkedFrom, linkedTo  

 while 0 < 1:
  appMgr.run(1)  
# check if there are still valid events 
  if not evt['Rec/Header'] : break
  findGhost(evt,ppSvc,linkedTo,htemp)
 appMgr.stop()
 return htemp

if __name__ == '__main__' :
 files = []    
 file  = '/castor/cern.ch/grid/lhcb/production/DC06/L0-v1-lumi2/00001959/DST/0000/00001959_00000XXX_1.dst'
 nfiles = 10
 for n in range(2,nfiles) : 
   ff = file.replace('XXX','%(X)03d'%{'X':n})
   x  = os.system('nsls '+ff)
   if x == 0 :  files.append(ff)
   
  #------Use up to 8 processes
 from processing import Pool
 n = 8
 pool = Pool(n)
 result = pool.map_async(processFile, files)
 for hlist in result.get(timeout=10000) : 
    for h in hlist :
     if myHistos[h] : myHistos[h].Add(hlist[h])
     else                     : myHistos[h] = hlist[h]
         
f=TFile('Ghosts.root','recreate')
for h in myHistos :
  sc = myHistos[h].Write()
f.Close()


