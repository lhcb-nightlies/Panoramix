from ROOT import *

gsl = Math.GSLRandomEngine()
gsl.Initialize()

h_r1 = TH1F('h_r1','r1',100,0.,5.)
h_r2 = TH1F('h_r2','r2',100,0.,5.)

norm = 1E5
for n in range(norm) : 
 sc = h_r1.Fill(gsl.Exponential(1.))
 sc = h_r2.Fill(gsl.Exponential(0.5))

h_r = h_r1.Clone('h_r')
sc  = h_r.Add(h_r2)

h_ratio = h_r2.Clone('h_ratio')
sc  = h_ratio.Divide(h_r1)



