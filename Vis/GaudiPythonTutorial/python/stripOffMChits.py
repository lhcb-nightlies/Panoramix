import os,pickle
filename = None
if len(os.sys.argv) > 2 : 
 filename  = os.sys.argv[1]
 outname   = os.sys.argv[2]
else :
 print 'use default file name'

if filename: 
  files = [filename]
else: 
#manuel files:
 files = []
 temp = '/castor/cern.ch/user/m/mschille/171/SUBJOBID/outputdata/Brunel.dst'
 for n in range(311): 
  files.append(temp.replace('SUBJOBID',str(n)))  
 outname = 'Brunel_311.dst'
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2010"
lhcbApp.Simulation = True
lhcbApp.DDDBtag   =  "head-20100119"
lhcbApp.CondDBtag =  "MC-20100205-vc15mm-md100"
 
EventSelector().PrintFreq = 10000

output = OutputStream("myDstWriter")
output.Output = "DATAFILE='PFN:"+outname+"' TYP='POOL_ROOTTREE' OPT='REC'"
#output.ItemList = ["/Event/Link#999","/Event/Rec#999","/Event/DAQ#999","/Event/MC#999","/Event/Gen#999"]
output.ItemList = ["/Event/Rec#999","/Event/DAQ#999","/Event/MC#999"]
appConf.OutStream.append("myDstWriter")   
appConf.OutputLevel = INFO
appConf.AppName     = 'StripOfMCHits'

from Configurables import EventNodeKiller 
evtkiller = EventNodeKiller('MChits')
evtkiller.Nodes        = ['/Event/MC/Rich','/Event/MC/Velo','/Event/MC/IT','/Event/MC/OT',
                         '/Event/MC/TT','/Event/MC/PuVeto','/Event/MC/Spd','/Event/MC/Prs',
                         '/Event/MC/Ecal','/Event/MC/Hcal','/Event/MC/Muon','/Event/Hlt','/Event/Trig','/Event/DAQ/Status']
appConf.TopAlg += [evtkiller]

from Configurables import HltConf,L0Conf
# fix Hlt interference
from Configurables import ( DataOnDemandSvc,RawBankToSTClusterAlg,
                            RawBankToSTLiteClusterAlg,TrackSys )

def doMyChanges():
 DataOnDemandSvc().AlgMap["Raw/Velo/LiteClusters"] = "DecodeVeloRawBuffer/createVeloLiteClusters"
 DataOnDemandSvc().AlgMap["Raw/TT/LiteClusters"]   = "RawBankToSTLiteClusterAlg/createTTLiteClusters"
 DataOnDemandSvc().AlgMap["Raw/IT/LiteClusters"]   = "RawBankToSTLiteClusterAlg/createITLiteClusters"
 DataOnDemandSvc().AlgMap["Raw/OT/Times"]          = "OTTimeCreator"
 DataOnDemandSvc().AlgMap["DAQ/ODIN"]              = "createODIN"
 RawBankToSTLiteClusterAlg("createITLiteClusters").DetType  = "IT"
 DataOnDemandSvc().AlgMap["Raw/Spd/Digits"]        = "CaloDigitsFromRaw/SpdFromRaw"
 DataOnDemandSvc().AlgMap["Raw/Prs/Digits"]        = "CaloDigitsFromRaw/PrsFromRaw"
 DataOnDemandSvc().AlgMap["Raw/Ecal/Digits"]        = "CaloZSupAlg/EcalZSup"
 DataOnDemandSvc().AlgMap["Raw/Hcal/Digits"]        = "CaloZSupAlg/HcalZSup"
 DataOnDemandSvc().AlgMap["Trig/L0/FullCalo"] = "L0CaloCandidatesFromRaw/L0CaloFromRaw"
 DataOnDemandSvc().AlgMap["Trig/L0/Calo"] = "L0CaloCandidatesFromRaw/L0CaloFromRaw"
 # some problems with this one: DataOnDemandSvc().AlgMap["Trig/L0/MuonCtrl"] = "L0MuonCandidatesFromRaw/L0MuonFromRaw"
 DataOnDemandSvc().AlgMap["Trig/L0/L0DUReport"] = "L0DUFromRawAlg"

ApplicationMgr().ExtSvc += [ "LoKiSvc" ]
hltconf = HltConf()
hltconf.L0TCK = '0x1309'
hltconf.ThresholdSettings =  'Physics_MinBiasL0_PassThroughHlt_09Dec09'
hlt = GaudiSequencer('Hlt')
appConf.TopAlg += [hlt]
L0Conf().TCK = '0x1309'
appendPostConfigAction(doMyChanges)
DataOnDemandSvc().AlgMap["Hlt/DecReports"] = HltDecReportsDecoder(OutputLevel = 4)
DataOnDemandSvc().AlgMap["Hlt/SelReports"] = HltSelReportsDecoder(OutputLevel = 4)
DataOnDemandSvc().AlgMap["Hlt/VertexReports"] =HltVertexReportsDecoder( OutputLevel = 4)

import GaudiPython,gaudigadgets
from GaudiPython import gbl

# keep track of MC events
try:
 f = file('MCinfo_5879.pkl')
 mcinfo = pickle.load(f)
 f.close()
except : 
 mcinfo = {}

appMgr = GaudiPython.AppMgr()
evt    = appMgr.evtsvc()
sel    = appMgr.evtsel()
appMgr.initialize()

sel.open(files) 

appMgr.algorithm('myDstWriter').Enable         = False
appMgr.algorithm('MChits').Enable              = False
appMgr.service('IODataManager').UseGFAL = False

n = 0
while 1>0 : 
 appMgr.run(1)
 if not evt['DAQ/ODIN'] : break  # probably end of input
 check = str(evt['DAQ/ODIN'].runNumber())+'|'+str(evt['DAQ/ODIN'].eventNumber())
 if mcinfo.has_key(check):
   print ' duplicated event', check
   continue
 mcinfo[check] = 1 
 #try:
 # allnodes = gaudigadgets.nodes(evt,True)
 #except:
 # # stupid gaudi
 # allnodes = gaudigadgets.nodes(evt,True)
 appMgr.algorithm('MChits').execute()
 appMgr.algorithm('myDstWriter').execute()
 n+=1

f = file('MCinfo_5879.pkl','w')
mcinfo = pickle.dump(mcinfo,f)
f.close()
