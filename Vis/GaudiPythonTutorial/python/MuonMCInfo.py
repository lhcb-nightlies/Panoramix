# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2008"
    
appConf = ApplicationMgr(OutputLevel = INFO) 

from Configurables import Gaudi__MultiFileCatalog
fc = Gaudi__MultiFileCatalog("FileCatalog")
name = os.path.expandvars("$GAUDIPYTHONTUTORIAL/MyCatalog_2008.xml")
if lhcbApp.DataType == 'DC06' :
  name = os.path.expandvars("$GAUDIPYTHONTUTORIAL/MyCatalog_20000_0.xml")
fc.Catalogs = ["xmlcatalog_file:"+name]

DataOnDemandSvc().AlgMap['/Event/Link/Raw/Muon/Coords'] =  'MuonCoord2MCParticleLink'

import GaudiPython
appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
# DC06 Dst
if lhcbApp.DataType == 'DC06' :
  sel.open(['PFN:/afs/cern.ch/lhcb/group/tracking/vol1/00001276_00000002_5.digi'])
else :  
 sel.open(['PFN:/castor/cern.ch/user/t/truf/MC2008/00003402_00000001_1.digi',
           'PFN:/castor/cern.ch/user/t/truf/MC2008/00003402_00000004_1.digi'])

MCMuonDigitInfo = GaudiPython.gbl.LHCb.MCMuonDigitInfo
MuonDigit       = GaudiPython.gbl.LHCb.MuonDigit
MCParticle      = GaudiPython.gbl.LHCb.MCParticle
MuonCoord       = GaudiPython.gbl.LHCb.MuonCoord

from LinkerInstances.eventassoc import *

def muondigithistory(mc):
 if mc.isGeantHit():		    print 'IsGeant'         
 if mc.isXTalkHit():		    print 'IsXtalk'       
 if mc.isBackgroundHit():       print 'IsBackg'        
 if mc.isChamberNoiseHit():     print 'IsChamberNoise' 
 if mc.isElNoiseHit():		    print 'ElNoiseHit'
 if mc.isFlatSpilloverHit(): 	print 'FlatSpillover'
 if mc.isMachineBkgHit():	    print 'MachineBkg'

evt = appMgr.evtsvc()
det = appMgr.detsvc()
# read one event
appMgr.run(1)
lcoord2part     = linkedTo(MCParticle,MuonCoord,'Raw/Muon/Coords')
if lcoord2part.notFound():  lcoord2part  = linkedTo(MCParticle,MuonDigit,'Raw/Muon/Digits')
if lcoord2part.notFound():  print 'no Linker tables available !'
lfrom = linkedFrom(MuonDigit,MCParticle,'Raw/Muon/Digits')

l_mcpart = {}
muonCoords = evt['Raw/Muon/Coords']
# take first muonCoord as an example
obj = muonCoords.containedObjects()[0]
# find all mcparticles linked with this object
l_mcpart[obj]=[] 
for digit in obj.digitTile():
     for mcp in lcoord2part.range(MuonDigit(digit)) :
      l_mcpart[obj].append(mcp)
# print info about muon tile and its related MCParticles
print l_mcpart
    
# Vector of digit
digits = obj.digitTile()
MCInfo = evt['MC/Muon/DigitsInfo']
mci    = MCMuonDigitInfo()
for d in digits :
  info = MCInfo.link(d.key())
  mci.setDigitInfo(info)
# print more info about MC history
muondigithistory(mci)    

# example for pass by reference
from ROOT import Long
ix = Long()
iy = Long()
mudet = det['/dd/Structure/LHCb/DownstreamRegion/Muon']
mudet.Pos2ChamberNumber(1000.,200.,12000.,ix,iy)
print ix,iy



