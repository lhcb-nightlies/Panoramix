# wants to be the ROOT of everything
import ROOT,os,sys,subprocess
# check for Panoversion
panovers = '0'
if os.environ.has_key('PANORAMIXROOT'):
 tmp = os.environ['PANORAMIXROOT']
 if tmp.find('_')<0:
  panovers = 'dev'
 else:
  panovers = os.environ['PANORAMIXROOT'].split('_')[1].split('/')[0]
#---Enable Tab completion-----------------------------------------
try:
  import rlcompleter, readline
  readline.parse_and_bind( 'tab: complete' )
  readline.parse_and_bind( 'set show-all-if-ambiguous On' )
except:
  pass
#
# fighting against the framework, undo recent changes which came with Gaudi v21r5
def _my_Action_() :    
 from GaudiPython.Bindings import AppMgr 
 gaudi = AppMgr()  
 gaudi.exit() 
import atexit
atexit.register ( _my_Action_ )

def addDBTags(fn):
 f = open('test.log','w')
 p = subprocess.Popen(['ex',os.environ['GAUDIPYTHONTUTORIAL']+'/ExtractDataBaseTags.py',fn],executable='python',env=os.environ,stdout=f,stderr=f)
 p.communicate() 
 f.close()
 keys = {'first':'m_condDBTags.first','second':'m_condDBTags.second'}
 tags = {}
 f = open('test.log')
 for l in f.readlines():
   if l.find('$$')<0: continue
   l = l.replace('$$','')
   t = l.split(':')
   k     = t[0].replace(' ','')
   tg = t[1].replace(' ','').replace('\n','')
   if tags.has_key(k):  tags[k].append(tg)
   else:                tags[k] = [tg]
 print '>>> Found database tags: ',tags 
 upgrade = False
 VP      = False
 ltg = [] 
 stg = [] 
 dt = ''
 for tg in  tags['DDDB']:
   if tg.find('dddb')==0 or tg.find('head')==0: 
     dt = tg.split('-')[1][0:4]
     lhcbApp.DataType = dt
     lhcbApp.DDDBtag = tg
   else:
     ltg.append(tg)
   if not tg.lower().find('upgrade')<0: upgrade = True
   if not tg.find('FT')<0: upgrade = True
   if not tg.find('UT')<0: upgrade = True
   if not tg.find('VP')<0: VP = True
 if tags.has_key('SIMCOND'):
  for tg in  tags['SIMCOND']:
   if tg.find('sim-20')==0: 
    CondDB().Tags['SIMCOND'] = tg
    lhcbApp.Simulation = True
   else:
     stg.append(tg)   
 if upgrade : 
   lhcbApp.DataType  = 'Upgrade'
   if VP : lhcbApp.setProp("Detectors",['VP','UT','FT','Rich1Pmt', 'Rich2Pmt', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'Magnet' ])
   else  : lhcbApp.setProp("Detectors",['VL','UT','FT','Rich1Pmt', 'Rich2Pmt', 'Spd', 'Prs', 'Ecal', 'Hcal', 'Muon', 'Magnet' ])
   dt =  'Upgrade'
   CondDB().Upgrade  = True
 CondDB().LocalTags = {'DDDB':ltg,'SIMCOND':stg}
 if tags.has_key('DQFLAGS'): 
  CondDB().Tags['DQFLAGS'] = tags['DQFLAGS'][0]
 if tags.has_key('LHCBCOND'):
  lhcbApp.CondDBtag = tags['LHCBCOND'][0]
  lhcbApp.DataType = tags['LHCBCOND'][0].split('-')[1][0:4]
  CondDB().Tags['LHCBCOND'] = tags['LHCBCOND'][0]
 os.system('rm test.log')
 return dt
# basic LHCb configurations for python users
from Gaudi.Configuration import *
# for backward compatibility with units in opts files
import os.path, GaudiKernel.ProcessJobOptions
GaudiKernel.ProcessJobOptions._parser._parse_units(os.path.expandvars("$STDOPTS/units.opts"))
#
import GaudiKernel.SystemOfUnits as units

from Configurables import LHCbApp,CondDB,DDDBConf,PhysConf,L0Conf,EventClockSvc,DstConf,AnalysisConf,DecodeRawEvent
from Configurables import LoKiSvc
LoKiSvc().Welcome = False


if 0>1:
# hope all these is now done by new Decoder ????
# Get the event time (for CondDb) from ODIN
 EventClockSvc().EventTimeDecoder = "OdinTimeDecoder"
 smallRawEventLocation = "/Event/Trigger/RawEvent"
 from Configurables import (OdinTimeDecoder,ODINDecodeTool,createODIN,ANNDispatchSvc)
 OdinTimeDecoder(OutputLevel = 3).addTool( ODINDecodeTool, 'ODINDecodeTool' )
 OdinTimeDecoder().ODINDecodeTool.RawEventLocations += [smallRawEventLocation]
 # until Hlt decoding is improved to accept list of containers, use only setting valid for new persistency model
 from Configurables import (HltDecReportsDecoder, HltSelReportsDecoder)
 ApplicationMgr().ExtSvc += [ANNDispatchSvc()]
 ANNDispatchSvc().RawEventLocation = smallRawEventLocation
 decReportsDecoder = HltDecReportsDecoder( InputRawEventLocation = smallRawEventLocation,
                                               OutputHltDecReportsLocation = "/Event/Hlt/DecReports")
 selReportsDecoder = HltSelReportsDecoder( InputRawEventLocation = smallRawEventLocation,
                                              OutputHltSelReportsLocation = "/Event/Hlt/SelReports",
                                              HltDecReportsLocation = "/Event/Hlt/DecReports")
 #support for Hlt decoding
 from Configurables import  HltDecReportsDecoder,HltSelReportsDecoder,HltVertexReportsDecoder
 DataOnDemandSvc().AlgMap["Hlt/DecReports"] = HltDecReportsDecoder( OutputLevel = 4)
 DataOnDemandSvc().AlgMap["Hlt/SelReports"] = HltSelReportsDecoder( OutputLevel = 4)
 DataOnDemandSvc().AlgMap["Hlt/VertexReports"] = HltVertexReportsDecoder( OutputLevel = 4)
 # L0 for TISTOS
 from Configurables import L0DecReportsMaker, L0SelReportsMaker
 DataOnDemandSvc().AlgMap["HltLikeL0/DecReports"] = L0DecReportsMaker()
 DataOnDemandSvc().AlgMap["HltLikeL0/SelReports"] = L0SelReportsMaker()

 from Configurables import (L0DUFromRawAlg,L0DUFromRawTool,L0MuonOutputs,
                           L0MuonCandidatesFromRaw,L0MuonOutputs,L0CaloCandidatesFromRaw,DecodePileUpData)
 L0Conf().FullL0MuonDecoding       = True
 L0Conf().EnableL0DecodingOnDemand = True

lhcbApp  = LHCbApp()
if panovers != '0': import PanoramixSys.Fixes_Gaudi
# very slow, therefore comment
# import PanoramixSys.Fixes_LoKi

# use default tag == latest tag
# for simulation, tag should be the same as used in Gauss

# no tags made automatically
#lhcbApp.DDDBtag   = "default"
#lhcbApp.CondDBtag = "default"

appConf = ApplicationMgr()
appConf.ExtSvc += ['DataOnDemandSvc','LHCb::ParticlePropertySvc']
# for particle property service 
import PartProp.Service, PartProp.decorators

DstConf().EnableUnpack = ["Reconstruction","Stripping"]

# provide support for unpacking MC data
# not done centrally
DataOnDemandSvc().AlgMap['/Event/MC/Particles'] =  'UnpackMCParticle' 
DataOnDemandSvc().AlgMap['/Event/MC/Vertices']   = 'UnpackMCVertex'
# support for decoding raw data
DecodeRawEvent().DataOnDemand=True
importOptions( "$L0TCK/L0DUConfig.opts" )
# provide support for ondemand creation of standard particles
import CommonParticles.StandardBasic
import CommonParticles.StandardIntermediate
# persistency service for IO of MDF files
EventPersistencySvc().CnvServices += ["LHCb::RawDataCnvSvc"]
from Configurables import Gaudi__RootCnvSvc
rootSvc = Gaudi__RootCnvSvc( "RootCnvSvc", EnableIncident = 1 )
rootSvc.VetoBranches = ["*"]
rootSvc.CacheBranches = []
fileSvc = Gaudi__RootCnvSvc( "FileRecordCnvSvc" )
fileSvc.ShareFiles = "YES"
FileRecordDataSvc( ForceLeaves        = True,
                   EnableFaultHandler = True,
                   RootCLID           = 1, 
                   PersistencySvc     = "PersistencySvc/FileRecordPersistencySvc")
PersistencySvc("FileRecordPersistencySvc").CnvServices += [ fileSvc ] 
appConf.ExtSvc += [ rootSvc,fileSvc]

# ROOT persistency for histograms
importOptions('$STDOPTS/RootHist.opts')
from Configurables import RootHistCnv__PersSvc
RootHistCnv__PersSvc('RootHistCnv').ForceAlphaIds = True
# should be provided by the user script, otherwise big confusion between Gaudi and ROOT
### RootHistSvc('RootHistSvc').OutputFile = 'histo.root'

# Use TimingAuditor for timing, suppress printout from SequencerTimerTool
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ] 
SequencerTimerTool().OutputLevel = INFO

# other useful decorators
import gaudigadgets
from GaudiPython import gbl

import LHCbMath.Types
LHCbMath = LHCbMath.Types.Gaudi


# some recent annoyance with ROOT, causes long wait when typing wrong command
try:
 if os.uname()[3].find('Ubuntu')>-1:
   sys.excepthook = sys.__excepthook__
except:
 rc = 'OK' 

# interactive users will probably never run with files on the GRID
# disabling avoids error message about not able to load GFale
from Configurables import Gaudi__IODataManager
iodatamanager = Gaudi__IODataManager()
iodatamanager.setProp('UseGFAL',False)

# some ROOT specialities
# f.MakeProject("Rene","*","recreate++") will make a dictionary of all LHCb C++ objects
# Event.MakeClass("Frank")  same thing ?
# gROOT.Time() measure time

# LoKi wants to be last
appConf.ExtSvc += [ 'LoKiSvc' ]

