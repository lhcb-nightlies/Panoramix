from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType            = "2010"
lhcbApp.Simulation          = True
# container name
candidates = 'AllStreams/Phys/Bs2JpsiPhiDetachedLine/Particles' 
appConf = ApplicationMgr(OutputLevel = INFO, AppName  = 'Ex11d')

EventSelector().PrintFreq = 100

import GaudiPython
from LinkerInstances.eventassoc import *
gbl = GaudiPython.gbl
Track       = gbl.LHCb.Track
Particle    = gbl.LHCb.Particle
MCParticle  = gbl.LHCb.MCParticle

appMgr = GaudiPython.AppMgr()

sel = appMgr.evtsel()
sel.open(['$PANORAMIXDATA/MC2010_BsJpsiPhi__00008919_00000068_1.dst'])

evt = appMgr.evtsvc()

h_bmass = TH1F('h_bmass','Mass of B candidate',100,5200.,5500.)

proto2mc = linkedTo(MCParticle,Track,'Rec/Track/Best')

while 0 < 1:
 appMgr.run(1)
# check if there are still valid events 
 if evt['Rec/Header'] == None : break
 cont = evt[candidates]
 if cont != None : 
  for b in cont : success = h_bmass.Fill(b.momentum().mass())
h_bmass.Draw()
if h_bmass.GetEntries() < 1 : 
 print 'ERROR ! No b candidates found.'

