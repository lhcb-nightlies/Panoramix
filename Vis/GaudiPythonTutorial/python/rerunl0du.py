#!/usr/bin/env python
# -*- coding: latin1 -*-

import GaudiPython
L0DUElementaryCondition = GaudiPython.gbl.LHCb.L0DUElementaryCondition

_debug_=True

from gaudigadgets import irange

def l0_conditions():
 l0dureport = evt['/Event/Trig/L0/L0DUReport']
 l0duconfig = l0dureport.configuration()
 l0duconditions = l0duconfig.conditions()
 for x in irange(l0duconditions.begin(),l0duconditions.end()) :                            
   idata  = x.second                                                      
   name   = idata.name()                                                   
   digit  = idata.threshold()                                                
   print  'L0DU Configuration : ',name,'has value (digit unit)',digit  

 for x in irange(l0duconditions.begin(),l0duconditions.end()) :
  print x[0], '\n', l0duconditions[x[0]]
  print l0duconditions[x.second.name()]


def rerunl0du(evt,modifiedConditionsMap=None):
    """ Modify some elementary conditions thresholds and recompute the L0DU decision.
    Thresholds to be applied are specified in the input dictionnary :
     - modifiedConditionsMap keys   : elementary condition names
     - modifiedConditionsMap values : elementary condition new thresholds
    """
    if modifiedConditionsMap is None:
        modifiedConditionsMap={}
        
    l0dureport = evt['/Event/Trig/L0/L0DUReport']
    l0duconfig = l0dureport.configuration()
    l0duconditions = l0duconfig.conditions()

    l0duconfig.resetEmulated()
    # Modify some elementary conditions
    for name,threshold in modifiedConditionsMap.items():
        if (_debug_): print 'Condition ',name
        elementaryCondition = l0duconditions[name]
        if (_debug_): print '\tbefore change',elementaryCondition.emulatedValue()
        elementaryCondition.setThreshold(threshold)
        if (_debug_): print '\tafter change ',elementaryCondition.emulatedValue()

    # debug print out    
    if (_debug_):    
        for l0ducondition in l0duconditions:
            name=l0ducondition[0]
            el_cond=l0ducondition[1]
            print 'name=%25s value, digits =%8s,%4s %2s threshold=%4s ? %1s' \
                  % (name,el_cond.data().value(),el_cond.data().digit(),el_cond.comparator(),el_cond.threshold(),el_cond.emulatedValue())

    emulatedDecision = l0duconfig.emulatedDecision()
    originalDecision = l0dureport.decision()   
    if (_debug_): print l0dureport
    l0dureport.setConfiguration(l0duconfig)
    l0dureport.setDecision(l0duconfig.emulatedDecision() )
    overwrittenDecision = l0dureport.decision()   
    if (_debug_): print l0dureport
    if (_debug_): print 'L0DU decision : original %d -VS- emulated %d overwritten %d' % (originalDecision,emulatedDecision,overwrittenDecision)
    
    return originalDecision,emulatedDecision

if __name__=='__main__':

    from LHCbConfig import *
    #lhcbApp.DataType = "2008"
    lhcbApp.DataType = "DC06"
    
    EvtMax = 10
    
    EventSelector().PrintFreq = 1
    
    files =[]
    file  = '/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00002019/DST/0000/00002019_00000XXX_5.dst'
    if lhcbApp.DataType == '2008' :
      file  = '/castor/cern.ch/grid/lhcb/MC/2008/DST/00003991/0000/00003991_00000XXX_5.dst'
    import os
    for n in range(1,5) :
      ff = file.replace('XXX','%(X)03d'%{'X':n})
      x  = os.system('nsls '+ff)
      if x == 0 :  files.append("DATA='"+ff+"'  TYP='POOL_ROOTTREE' OPT='READ'")
    EventSelector(Input = files)
    
    # Define the new L0DU conditions
    modifiedConditionsMap={'DiMuon,HighPt':21 , \
                           'Muon,LowPt':13}

    # Event loop
    appMgr = GaudiPython.AppMgr() 
    evt=appMgr.evtsvc()
    counters={'tot':0,'original':0,'emulated':0}
    for ievt in range(EvtMax):
        appMgr.run(1)
        originalDecision,emulatedDecision = rerunl0du(evt,modifiedConditionsMap)
        counters['tot']+=1
        counters['original']+=originalDecision
        counters['emulated']+=emulatedDecision


    # Print out counters
    print 'RerunL0DU : Modified L0DU conditions :'
    for name,threshold in modifiedConditionsMap.items():
        print 'RerunL0DU :\tname= %30s => threshold=%5s' % (name,threshold)
    print 'RerunL0DU : Total Number of events processed = %5d ' % (counters['tot'])
    if counters['tot']>0:
        print 'RerunL0DU :\toriginaly L0 accepted = %5d (%5.2f %%) ' % (counters['original'],counters['original']*100./counters['tot'])  
        print 'RerunL0DU :\twith new cuts         = %5d (%5.2f %%) ' % (counters['emulated'],counters['emulated']*100./counters['tot'])  
