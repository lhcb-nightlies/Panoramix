from ROOT import TH1F, TBrowser, TCanvas,Double
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2011"

import GaudiKernel.SystemOfUnits as units

appConf = ApplicationMgr(AppName = 'Ex7d',OutputLevel = INFO)
appConf.ExtSvc += ['MagneticFieldSvc']

import GaudiPython

appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
det    = appMgr.detSvc() 
lhcb   = det['/dd/Structure/LHCb']
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst']) 
appMgr.run(1)
magSvc      = appMgr.service('MagneticFieldSvc','IMagneticFieldSvc')
bintegrator = appMgr.toolsvc().create('BIntegrator',interface='IBIntegrator')
zCenter = Double(0.)

XYZPoint    = LHCbMath.XYZPoint
XYZVector   = LHCbMath.XYZVector

nbins = 10000
za    = 2000.
zb    = 8000.

h_fieldy = TH1F('fieldy','field y component x=y=0',nbins,za,zb)
h_inty   = TH1F('inty','integrated field y component x=y=0',nbins,za,zb)
a   = XYZPoint(0.,0.,0.)
b   = XYZPoint(0.,0.,0.)
v   = XYZVector()
Bdl = XYZVector()

dz = (zb-za)/float(nbins)
for n in range(nbins):
  z = za+n*dz
  a.SetZ(z)
  magSvc.fieldVector(a,v)
  print z
  h_fieldy.Fill(z,v.y()/units.gauss)
  a.SetZ(z-dz/2.)
  b.SetZ(z+dz/2.)
  result = bintegrator.calculateBdlAndCenter(a,b,0.,0.,zCenter,Bdl)
  h_inty.Fill(z,Bdl.y()/units.gauss)

appMgr.stop() 
h_fieldy.Draw()

