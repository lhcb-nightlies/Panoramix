import time
start = time.time()
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "DC06"

importOptions('$PANORAMIXROOT/options/Panoramix_DaVinci.py')
from Configurables import CombineParticles,FilterDesktop
importOptions('$STRIPPINGSELECTIONSROOT/options/StrippingBs2JpsiPhi.py')
appConf.TopAlg+=[
 FilterDesktop("StripStdUnbiasedPhi2KK"),
 CombineParticles("StripBs2JpsiPhi")]

appConf = ApplicationMgr( OutputLevel = 3, AppName = 'Ex2_multicore')

EventSelector().PrintFreq  = 100

#--- modules to be reused in all processes---------------
import GaudiPython
from processing import Pool
from ROOT import TH1F,TFile,gROOT 

h_bmass = TH1F('h_bmass','Mass of B candidate',100,5200.,5500.)

histos = {}
for h in gROOT.GetList() : 
  histos[h.GetName()] = h

def processFile(file) :
  htemp = {}
  for h in histos :
    htemp[h] = histos[h].Clone()
  appMgr = GaudiPython.AppMgr()
  sel = appMgr.evtsel()
  sel.open([file])
  evt = appMgr.evtsvc()
  while 0 < 1:
   appMgr.run(1)
# check if there are still valid events
   if not evt['Rec/Header'] : break
   cont = evt['Phys/StripBs2JpsiPhi/Particles']
   if cont :
     for b in cont : 
      success = htemp['h_bmass'].Fill(b.momentum().mass())
  print 'Finishing.... ', file,' ',htemp['h_bmass'].GetMean() ,htemp['h_bmass'].GetEntries() 
# collect all histograms
  appMgr.stop()
  return htemp

#--- In the master process only....

if __name__ == '__main__' :
 files = []
 file  = 'PFN:/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00001758/DST/0000/00001758_00000XXX_5.dst' 
 for n in range(1,50) : 
  ff = file.replace('XXX','%(X)03d'%{'X':n})
  x  = os.system('nsls '+ff.replace('PFN:',''))
  if x == 0 :  files.append(ff)
      
  #------Use up to 8 processes
 n = 4
 pool = Pool(n)

 result = pool.map_async(processFile, files[:32])
  
 for hlist in result.get(timeout=10000) : 
   for h in hlist :  
       histos[h].Add(hlist[h])
   
 histos['h_bmass'].Draw()
 print histos['h_bmass'].GetEntries()
 end = time.time()
 print 'total time', end - start
