from ROOT import TH1F, TBrowser, TCanvas
# get the basic configuration from here
from LHCbConfig import *
lhcbApp.DataType = "2009"
    
appConf = ApplicationMgr(AppName = 'Ex7b', OutputLevel = INFO) 

import GaudiPython

appMgr = GaudiPython.AppMgr()
sel    = appMgr.evtsel()
det    = appMgr.detSvc() 
lhcb   = det['/dd/Structure/LHCb']
sel.open(['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst']) 
appMgr.run(1)
partSvc    = appMgr.ppSvc()
ParticleID = GaudiPython.gbl.LHCb.ParticleID

print 'Name of B meson', partSvc.find(ParticleID(521)).name()
print 'Mass of B meson', partSvc.find(ParticleID(521)).mass()
print 'Lifetime of B meson',partSvc.find(ParticleID(521)).lifetime()
print partSvc.find(ParticleID(521))
