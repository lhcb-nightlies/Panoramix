# get the basic configuration from here
from LHCbConfig import *
# lhcbApp.DataType = "2008"
lhcbApp.DataType = "DC06"

# Default options to rerun L0 
from Configurables import L0DUAlg,L0DUFromRawAlg

myL0DUseq = GaudiSequencer("seqL0DU")
appConf.TopAlg += [ myL0DUseq ]

l0decode = L0DUFromRawAlg()
l0decode.ProcessorDataOnTES = True
l0decode.L0DUReportOnTES    = False

l0DU = L0DUAlg('L0DU')
l0DU.DataLocations = ["Trig/L0/L0DUData"]
l0DU.ReportLocation = "Trig/L0/L0DUReport"
l0DU.StoreInBuffer = False
l0DU.WriteOnTES = True
l0DU.OutputLevel = 2

l0DU.TCK = '0xFFF8'
myL0DUseq.Members+=[ l0decode, l0DU ]
   
#def doMyChanges():
# del DataOnDemandSvc().AlgMap['Trig/L0/L0DUReport']
#appendPostConfigAction(doMyChanges)
   
   
import GaudiPython
from GaudiPython import gbl

files = []
file  = '/castor/cern.ch/grid/lhcb/production/DC06/phys-v2-lumi2/00002019/DST/0000/00002019_00000XXX_5.dst'
if lhcbApp.DataType == '2008' : 
 file  = '/castor/cern.ch/grid/lhcb/MC/2008/DST/00003991/0000/00003991_00000XXX_5.dst'
import os
for n in range(1,5) : 
  ff = file.replace('XXX','%(X)03d'%{'X':n})
  x  = os.system('nsls '+ff)
  if x == 0 :  files.append("DATA='"+ff+"'  TYP='POOL_ROOTTREE' OPT='READ'")
EventSelector(Input = files)

appMgr = GaudiPython.AppMgr()
evt  = appMgr.evtsvc()

for n in range(3) : 
 appMgr.run(1)
 if not evt['Rec/Header'] : break  # probably end of input
 # check L0
 L0dir = evt['Trig/L0/L0DUReport']
 print L0dir
