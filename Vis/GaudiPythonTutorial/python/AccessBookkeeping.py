import os,sys 
# requires SetupProject Dirac

if len(sys.argv) < 1 :
  print 'enter prodid, try again '
  exit
  
if not os.getenv('DIRACROOT') : 
  print 'Dirac environment required. Use SetupProject Dirac'
  exit  
    
# check for proxy
rc = os.system('lhcb-proxy-info > /dev/null') 
if rc != 0 : os.system('lhcb-proxy-init') 
  
prodid = str(sys.argv[1])  
os.system('dirac-bookkeeping-production-informations '+prodid) 
