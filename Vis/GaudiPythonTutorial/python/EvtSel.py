#!/usr/bin/env python
# =============================================================================

import GaudiPython.Bindings

_EvtSel = GaudiPython.Bindings.iEventSelector

def _openNew_ ( self, stream, typ = 'POOL_ROOT', opt = 'READ', sel = None, fun = None, collection = None ):
        """
        Open the file(s) :

        >>> evtSel.open ( 'file.dst' )                   ## open one file 
        >>> evtSel.open ( [ 'file1.dst' , 'file2.dst'] ) ## list of files 
        >>> evtSel.open ( '/castor/.../file.dst' )       ## open castor file 
        >>> evtSel.open ( 'file.raw' )                   ## open RAW file 
        >>> evtSel.open ( 'file.mdf' )                   ## open MDF file 
        
        """
        
        if typ == 'ROOT' :
            self.g.declSvcType('RootEvtCnvSvc','DbEventCnvSvc')
            self.g.service('RootEvtCnvSvc').DbType  = 'ROOT'
            self.g.createSvc('RootEvtCnvSvc')
            self.g.service('EventPersistencySvc').CnvServices = ['RootEvtCnvSvc']
        elif typ == 'POOL_ROOT':
            cacsvc = self.g.service('PoolDbCacheSvc')
            if hasattr(cacsvc, 'Dlls') : cacsvc.Dlls += ['lcg_RootStorageSvc', 'lcg_XMLCatalog']
            else :                       cacsvc.Dlls = ['lcg_RootStorageSvc', 'lcg_XMLCatalog']
            cacsvc.OutputLevel = 4
            cacsvc.DomainOpts    = [ 'Domain[ROOT_All].CLASS_VERSION=2 TYP=int',
                                     'Domain[ROOT_Key].CLASS_VERSION=2 TYP=int',
                                     'Domain[ROOT_Tree].CLASS_VERSION=2 TYP=int' ]
            cacsvc.DatabaseOpts  = ['']
            cacsvc.ContainerOpts = ['']
            self.g.createSvc('PoolDbCacheSvc')
            cnvSvcs = [('PoolRootEvtCnvSvc',     'POOL_ROOT'),
                       ('PoolRootTreeEvtCnvSvc', 'POOL_ROOTTREE'),
                       ('PoolRootKeyEvtCnvSvc',  'POOL_ROOTKEY')]
            for svc in cnvSvcs :
                self.g.declSvcType(svc[0], 'PoolDbCnvSvc')
                cnvsvc = self.g.service(svc[0])
                cnvsvc.DbType = svc[1]
            self.g.service('EventPersistencySvc').CnvServices = [ svc[0] for svc in cnvSvcs ]
            for svc in cnvSvcs :
                self.g.createSvc(svc[0])
                
        self.g.service('EventDataSvc').RootCLID = 1
        
        if type(stream) != list : stream = [stream]
        fixpart = "TYP=\'%s\' OPT=\'%s\'" % ( typ, opt )
        if sel        : fixpart += " SEL=\'%s\'" % sel
        if fun        : fixpart += " FUN=\'%s\'" % fun
        if collection : fixpart += " COLLECTION=\'%s\'" % collection

        cstream = [] 
        for f in stream :

            mask = "DATAFILE=\'%s\' %s "
            ## prepend castor if needed 
            if 0 == f.find('/castor') : f = 'castor:' + f

            if 0 < f.find ( '.raw') or 0 < f.find ( '.mdf') :
                cstream += [ "DATA=\'%s\' SVC='LHCb::MDFSelector' " % f ]                
                if 'LHCb::RawDataCnvSvc' not in self.g.service('EventPersistencySvc').CnvServices :
                    self.g.service('EventPersistencySvc').CnvServices.append ( 'LHCb::RawDataCnvSvc')
                    self.g.createSvc('LHCb::RawDataCnvSvc')
            else: 
                cstream += ["DATAFILE=\'%s\' %s" % ( f, fixpart) ] 

        self.Input = cstream
        self.reinitialize()


_EvtSel._openNew_ = _openNew_
_EvtSel.open      = _openNew_
                        

# =============================================================================
# The END 
# =============================================================================
       
