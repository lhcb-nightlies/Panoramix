debug    = False
f_2008   = True
noNoise  = True


from ROOT import TH1F,TCanvas
# get the basic configuration from here
from LHCbConfig import *
if not f_2008:
    lhcbApp.DataType = "DC06"
else:
    lhcbApp.DataType = "2008"
    lhcbApp.Simulation = True 
     
appConf = ApplicationMgr( OutputLevel = 4, AppName = 'VeloCluster' )

EventSelector().PrintFreq = 1000

import GaudiPython
MCParticle   = GaudiPython.gbl.LHCb.MCParticle
VeloCluster  = GaudiPython.gbl.LHCb.VeloCluster

# lets define Algorithm for event loop

def execute(linkedTo):
  l2mc = linkedTo(MCParticle,VeloCluster,'Raw/Velo/Clusters')   
  vc   = evt['Raw/Velo/Clusters']   
  for cl in vc :
   if l2mc.range(cl).size() == 0 : continue
   sensorNr = cl.channelID().sensor()
   if hlist.has_key(sensorNr) : 
    h  = hlist[sensorNr]
    hl = hlandau[sensorNr]
    hc = hclustsize[sensorNr] 
   else :
    sensor = velo.sensor(sensorNr)
    title  = (sensor.name()).replace(velo_loc,'')
    name   = title.split('/')[2]+sensor.type()
    h      = TH1F(name, title, sensor.numberOfStrips(), 0., sensor.numberOfStrips() )  
    hl     = TH1F('L_'+name, title,  300,   0., 300. )
    hc     = TH1F('C_'+name, title,  10,   0., 10. )
    hlist[sensorNr]    = h
    hlandau[sensorNr]  = hl
    hclustsize[sensorNr]  = hc
   for i in range(cl.size()) :
    success = h.Fill(cl.strip(i))
   
   success = hl.Fill(cl.totalCharge())
   success = hc.Fill(cl.size())
  return True

# 
#/////////////////////////////////////////////////////////////////////////////
#  Do the execution
#/////////////////////////////////////////////////////////////////////////////

# helpful if you want to reload without re-executing all this 
if __name__ == '__main__' :

 appMgr = GaudiPython.AppMgr()
 from LinkerInstances.eventassoc import linkedTo
 
 # define location of geometry
 det = appMgr.detsvc()
   
 velo_loc =  '/dd/Structure/LHCb/BeforeMagnetRegion/Velo'
 velo = det[velo_loc]
 
 # open event input file
 sel = appMgr.evtsel()
 if not f_2008 : 
           sel.open(['PFN:/afs/cern.ch/lhcb/group/tracking/vol1/00001378_00000002_5.dst'])
           print 'Using DC06 data !'
 else : 
           sel.open(['PFN:/castor/cern.ch/grid/lhcb/MC/2008/DST/00003991/0000/00003991_00000001_5.dst',
           'PFN:/castor/cern.ch/grid/lhcb/MC/2008/DST/00003991/0000/00003991_00000002_5.dst',
           'PFN:/castor/cern.ch/grid/lhcb/MC/2008/DST/00003991/0000/00003991_00000003_5.dst',
           'PFN:/castor/cern.ch/grid/lhcb/MC/2008/DST/00003991/0000/00003991_00000004_5.dst'
           ])
  
 evt = appMgr.evtsvc()
  
 hlist      = {}
 hlandau    = {}
 hclustsize = {}  
 
 for n in range(2500) : 
  appMgr.run(1)
  if not evt['DAQ/ODIN'] : break
  execute(linkedTo)
   
 c1=TCanvas('c1','Velo sensors hitmaps',1400,1100)
 c1.Divide(10,9)
 
 i=0
 for h in hlist.values()  :
  i+=1
  c1.cd(i)
  h.DrawCopy()  
  
 cl=TCanvas('cl','Velo sensors Landau',1400,1100)
 cl.Divide(10,9) 
 i=0
 for h in hlandau.values()  :
  i+=1
  cl.cd(i)
  h.DrawCopy()

 cc=TCanvas('cc','Velo sensors clustersizes',1400,1100)
 cc.Divide(10,9)
 
 i=0
 for h in hclustsize.values()  :
  i+=1
  cc.cd(i)
  h.DrawCopy()  
 
 c1.Update()
 cl.Update()
 cc.Update()
 
 from ROOT import TFile, gROOT 
 f=TFile('VeloCluster.root','recreate')
 for h in gROOT.GetList() :
  h.Write()
 f.Close()
