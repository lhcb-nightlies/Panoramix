#ifndef SoStat_SoStatSvc_h
#define SoStat_SoStatSvc_h

// Gaudi :
#include <GaudiKernel/Service.h>

template <typename T> class SvcFactory;

class SoStatSvc : public Service { 
public: //IService
  virtual StatusCode initialize();
  virtual StatusCode finalize();
protected:
  // No instantiation via new, only via the factory.
  SoStatSvc(const std::string&,ISvcLocator*);
  virtual ~SoStatSvc();
  friend class SvcFactory<SoStatSvc>;
};

#endif
