// this :
#include "SoStatSvc.h"

// Gaudi :
#include <GaudiKernel/SvcFactory.h>
#include <GaudiKernel/MsgStream.h>

DECLARE_SERVICE_FACTORY( SoStatSvc )

//////////////////////////////////////////////////////////////////////////////
SoStatSvc::SoStatSvc(
 const std::string& aName
,ISvcLocator* aSvcLoc
) 
:Service(aName,aSvcLoc)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
SoStatSvc::~SoStatSvc(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{ 
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoStatSvc::initialize(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  StatusCode status = Service::initialize();
  if( status.isFailure() ) return status;
  MsgStream log(msgSvc(), Service::name());
  log << MSG::DEBUG << " SoStatSvc::initialize " << endmsg;
  setProperties();
  return status;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoStatSvc::finalize(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log(msgSvc(), Service::name());
  log << MSG::DEBUG << "SoStatSvc finalized successfully" << endmsg;
  return Service::finalize();
}
