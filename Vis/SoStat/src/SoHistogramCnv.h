#ifndef SoStat_SoHistogramCnv_h
#define SoStat_SoHistogramCnv_h

// Inheritance :
#include <GaudiKernel/Converter.h>

namespace AIDA {
  class IHistogram;
  class IHistogram1D;
  class IHistogram2D;
}

class IUserInterfaceSvc;
class IRootSvc;

template <class T> class CnvFactory;

class SoHistogramCnv : public Converter {
  friend class CnvFactory<SoHistogramCnv>;
public:
  SoHistogramCnv(ISvcLocator*);
  virtual StatusCode initialize();
  virtual StatusCode finalize();
  virtual long repSvcType() const;
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
private:
  StatusCode plotWithSoPlotter(MsgStream&,DataObject*);
  StatusCode plotWithTCanvas(MsgStream&,DataObject*);
protected:
  IUserInterfaceSvc* fUISvc;
  IRootSvc* fRootSvc;
};

// Inheritance :
#include <HEPVis/SbPlottableThings.h>

class SoHistogramRep1D : public SbPlottableBins1D {
public: //SbPlottableObject
  virtual bool isValid() const;
  virtual const char* getName();
  virtual int getDimension() const;
  virtual const char* getLabel();
  virtual void* cast(const char*) const;
  virtual void* nativeObject() const;
  virtual const char* nativeObjectClass() const;
  virtual const char* getInfos(const char*);
  virtual const char* getLegend();
public: //SbPlottableBins1D
  virtual void getBinsSumOfWeightsRange(float&,float&) const;
  virtual int getAxisNumberOfBins() const;
  virtual float getAxisMinimum() const;
  virtual float getAxisMaximum() const;
  virtual float getBinLowerEdge(int) const;
  virtual float getBinUpperEdge(int) const;
  virtual int getBinNumberOfEntries(int) const;
  virtual float getBinSumOfWeights(int) const;
  virtual float getBinBar(int) const;
public:
  SoHistogramRep1D(const std::string&,const AIDA::IHistogram1D*);
  virtual ~SoHistogramRep1D();
private:
  std::string fName;
  std::string fTitle;
  const AIDA::IHistogram1D* fHistogram;
  std::string fDataClass;
  std::string fInfos;
  std::string fLegend;
};

class SoHistogramRep2D : public SbPlottableBins2D {
public: //SbPlottableObject
  virtual bool isValid() const;
  virtual const char* getName();
  virtual int getDimension() const;
  virtual const char* getLabel();
  virtual void* cast(const char*) const;
  virtual void* nativeObject() const;
  virtual const char* nativeObjectClass() const;
  virtual const char* getInfos(const char*);
  virtual const char* getLegend();
public: //SbPlottableBins2D
  virtual void getBinsSumOfWeightsRange(float&,float&) const;
  virtual int getAxisNumberOfBinsX() const;
  virtual float getAxisMinimumX() const;
  virtual float getAxisMaximumX() const;
  virtual int getAxisNumberOfBinsY() const;
  virtual float getAxisMinimumY() const;
  virtual float getAxisMaximumY() const;
  virtual float getBinLowerEdgeX(int) const;
  virtual float getBinUpperEdgeX(int) const;
  virtual float getBinLowerEdgeY(int) const;
  virtual float getBinUpperEdgeY(int) const;
  virtual int getBinNumberOfEntries(int,int) const;
  virtual float getBinSumOfWeights(int,int) const;
  virtual float getBinBar(int,int) const;
public:
  SoHistogramRep2D(const std::string&,const AIDA::IHistogram2D*);
  virtual ~SoHistogramRep2D();
private:
  std::string fName;
  std::string fTitle;
  const AIDA::IHistogram2D* fHistogram;
  std::string fDataClass;
  std::string fInfos;
  std::string fLegend;
};

#endif
