// This :
#include "Types.h"

// Lib :
#include <Lib/Property.h>
#include <Lib/Variable.h>
#include <Lib/Manager.h>

// DetDesc :
#include <DetDesc/ILVolume.h>
#include <DetDesc/IPVolume.h>
#include <DetDesc/ISolid.h>
#include <DetDesc/IGeometryInfo.h>
#include <DetDesc/Material.h>

// OnXSvc :
#include <OnXSvc/Helpers.h>

// Inventor :
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/draggers/SoDragPointDragger.h>
#include <Inventor/nodes/SoTransform.h>

// HEPVis :
#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoSceneGraph.h>

#include <cmath>

#ifdef WIN32
#include <sstream>
#endif

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
LVolumeType::LVolumeType(
 IPrinter& aPrinter
)
:BaseType(aPrinter)
,fType("LVolume")
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty("id",Lib::Property::POINTER);
  addProperty("name",Lib::Property::STRING,70);
  addProperty("material",Lib::Property::STRING,70);
  addProperty("solid",Lib::Property::STRING,70);
}
//////////////////////////////////////////////////////////////////////////////
std::string LVolumeType::name() const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fType;
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable LVolumeType::value(
 Lib::Identifier aIdentifier
,const std::string& aName
,void*
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  const ILVolume* obj = (const ILVolume*)aIdentifier;
  if(aName=="id") {
    return Lib::Variable(printer(),(void*)obj);
  } else if(aName=="name") {
    return Lib::Variable(printer(),obj->name());
  } else if(aName=="material") {
    const Material* material = obj->material();
    return Lib::Variable(printer(),material ? material->name() : "(nil)");
  } else if(aName=="solid") {
    const ISolid* solid = obj->solid();
    if(solid) {
#if defined(__GNUC__) && (__GNUC__ <= 2)
      std::ostrstream os;
      os << solid << std::ends;
      const std::string s(os.str());
      return Lib::Variable(printer(),s);
#else
      std::ostringstream os;
      os << solid << std::ends;
      return Lib::Variable(printer(),os.str());
#endif
    } else {
      const std::string s = "(nil)";
      return Lib::Variable(printer(),s);
    }
  } else {
    return Lib::Variable(printer());
  }
}
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// Inventor :
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoIndexedFaceSet.h>
#include <Inventor/nodes/SoNormal.h>
#include <Inventor/nodes/SoNormalBinding.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLightModel.h>

// Gaudi :
#include <GaudiKernel/IMagneticFieldSvc.h>

// OnXSvc :
#include <OnXSvc/IUserInterfaceSvc.h>

// Lib :
#include <Lib/Interfaces/IIterator.h>
#include <Lib/Interfaces/ISession.h>
#include <Lib/smanip.h>

// OnX :
#include <OnX/Interfaces/IUI.h>

#define MINIMUM(a,b) ((a)<(b)?a:b)
#define MAXIMUM(a,b) ((a)>(b)?a:b)

//////////////////////////////////////////////////////////////////////////////
MagneticFieldType::MagneticFieldType(
 IUserInterfaceSvc* aUISvc
,IMagneticFieldSvc* aMagneticFieldSvc
)
:OnX::BaseType(aUISvc->printer())
,fName("MagneticField")
,fUISvc(aUISvc)
,fMagneticFieldSvc(aMagneticFieldSvc)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  addProperty("id",Lib::Property::POINTER);
}
//////////////////////////////////////////////////////////////////////////////
std::string MagneticFieldType::name() const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fName;
}
//////////////////////////////////////////////////////////////////////////////
Lib::IIterator* MagneticFieldType::iterator(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // One shoot only iterator.
  class Iterator : public Lib::IIterator {
  public: //Lib::IIterator
    virtual Lib::Identifier object() { return fMagneticFieldSvc; }
    virtual void* tag() { return 0; }
    virtual void next() {fMagneticFieldSvc = 0;}
  public:
    Iterator(IMagneticFieldSvc* aMagneticFieldSvc)
      :fMagneticFieldSvc(aMagneticFieldSvc){}
  private:
    IMagneticFieldSvc* fMagneticFieldSvc;
  };

  return new Iterator(fMagneticFieldSvc);
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable MagneticFieldType::value(
 Lib::Identifier aIdentifier
,const std::string& aName
,void*
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(aName=="id") {
    return Lib::Variable(printer(),(void*)aIdentifier);
  } else {
    return Lib::Variable(printer());
  }
}

// set layout of the scene graph :
static void alloc_sceneGraph(
 SoCoordinate3& a_coordinate3
,SoMaterial& a_material
,SoIndexedFaceSet& a_faceSet
,unsigned int a_nx
,unsigned int a_ny
) {
  int32_t coordIndex[5];
  int icoord = 0;
  int iface = 0;
  int imat = 0;

  SbVec3f line[4];

  for(unsigned int ix=0;ix<a_nx;ix++) {
    for(unsigned int iy=0;iy<a_ny;iy++) {

      a_material.diffuseColor.set1Value(imat,SbColor(0,0,0));
      a_material.transparency.set1Value(imat,0);

      coordIndex[0] = icoord + 0;
      coordIndex[1] = icoord + 1;
      coordIndex[2] = icoord + 2;
      coordIndex[3] = icoord + 3;
      coordIndex[4] = SO_END_FACE_INDEX;

      a_coordinate3.point.setValues(icoord,4,line);
      icoord += 4;   

      a_faceSet.coordIndex.setValues(iface,5,coordIndex);
      iface += 5;

      a_faceSet.materialIndex.set1Value(imat,imat);
      imat++;
    }
  }

}

static void fill_sceneGraph(
 SoCoordinate3& a_coordinate3
,SoMaterial& a_material
,unsigned int a_nx
,unsigned int a_ny
,float a_sx
,float a_sy
,const SbVec3f& a_gridStart
,const SbVec3f& a_gridX
,const SbVec3f& a_gridY
,float a_transparency
,float a_colorMin
,float a_colorMax
,float a_fieldMin
,float a_fieldMax
,IMagneticFieldSvc& a_MagneticFieldSvc
) {
  int imat = 0;
  int icoord = 0;

  float colorSlope = (a_colorMax-a_colorMin)/(a_fieldMax-a_fieldMin);
  for(unsigned int ix=0;ix<a_nx;ix++) {
    for(unsigned int iy=0;iy<a_ny;iy++) {

      SbVec3f center = a_gridStart 
        + a_gridX * (ix * a_sx + a_sx/2) 
        + a_gridY * (iy * a_sy + a_sy/2);
      Gaudi::XYZPoint point(center[0],center[1],center[2]);
      Gaudi::XYZVector field;
      a_MagneticFieldSvc.fieldVector( point, field);
      float value = (float)field.mag2();
      value = (float) std::sqrt(value);
      
      float r = 0;
      float g = 0;
      float b = 0;
      if(value<a_fieldMin) {
        r = a_colorMin;
      } else if(value>=a_fieldMax) {
        r = a_colorMax;
      } else {
        r = colorSlope * (value - a_fieldMin) + a_colorMin;
      }

      a_material.diffuseColor.set1Value(imat,SbColor(r,g,b));
      a_material.transparency.set1Value(imat,a_transparency);
      imat++;

      SbVec3f line[4];
      line[0] = a_gridStart+ a_gridX*(ix*a_sx)      +a_gridY*(iy*a_sy);
      line[1] = a_gridStart+ a_gridX*(ix*a_sx+a_sx) +a_gridY*(iy*a_sy);
      line[2] = a_gridStart+ a_gridX*(ix*a_sx+a_sx) +a_gridY*(iy*a_sy+a_sy);
      line[3] = a_gridStart+ a_gridX*(ix*a_sx)      +a_gridY*(iy*a_sy+a_sy);

      a_coordinate3.point.setValues(icoord,4,line);
      icoord += 4;   
    }
  }

}

class MagField_dragger : public SoDragPointDragger {
public:
  MagField_dragger(float a_scale,
                SoCoordinate3& a_coordinate3,
                SoMaterial& a_material,
                unsigned int a_nx,
                unsigned int a_ny,
                float a_sx,
                float a_sy,
                const SbVec3f& a_gridStart,
                const SbVec3f& a_gridX,
                const SbVec3f& a_gridY,
                float a_transparency,
                float a_colorMin,
                float a_colorMax,
                float a_fieldMin,
                float a_fieldMax,
                IMagneticFieldSvc& a_MagneticFieldSvc)
 :f_scale(a_scale),
  f_coordinate3(a_coordinate3),
  f_material(a_material),
  f_nx(a_nx),
  f_ny(a_ny),
  f_sx(a_sx),
  f_sy(a_sy),
  f_gridStart(a_gridStart),
  f_gridX(a_gridX),
  f_gridY(a_gridY),
  f_transparency(a_transparency),
  f_colorMin(a_colorMin),
  f_colorMax(a_colorMax),
  f_fieldMin(a_fieldMin),
  f_fieldMax(a_fieldMax),
  f_MagneticFieldSvc(a_MagneticFieldSvc)
  {
    SoDragger::addMotionCallback(motion_dragger_CB);
  }
protected:
  virtual ~MagField_dragger(){
    SoDragger::removeMotionCallback(motion_dragger_CB);
  }
private:
  static void motion_dragger_CB(void*,SoDragger* aDragger) {
    MagField_dragger* This = (MagField_dragger*)aDragger;
    const SbVec3f& v = This->translation.getValue();
    //printf("debug : motion_dragger_CB : %g %g %g\n",v[0],v[1],v[2]);

    //This->fCoord.point.set1Value(This->fIndex,This->fStart);
    SbVec3f gridStart = This->f_gridStart + v * This->f_scale;

    fill_sceneGraph(This->f_coordinate3,This->f_material,
                    This->f_nx,This->f_ny,This->f_sx,This->f_sy,
                    gridStart,This->f_gridX,This->f_gridY,
                    This->f_transparency,This->f_colorMin,This->f_colorMax,
                    This->f_fieldMin,This->f_fieldMax,
                    This->f_MagneticFieldSvc);
  }
private:
  float f_scale;
  SoCoordinate3& f_coordinate3;
  SoMaterial& f_material;
  unsigned int f_nx;
  unsigned int f_ny;
  float f_sx;
  float f_sy;
  SbVec3f f_gridStart;
  SbVec3f f_gridX;
  SbVec3f f_gridY;
  float f_transparency;
  float f_colorMin;
  float f_colorMax;
  float f_fieldMin;
  float f_fieldMax;
  IMagneticFieldSvc& f_MagneticFieldSvc;
};

//////////////////////////////////////////////////////////////////////////////
void MagneticFieldType::visualize(
 Lib::Identifier aIdentifier
,void*
) 
//////////////////////////////////////////////////////////////////////////////
// We subdivide a cutting plan into cells (then we have a cutting grid).
// At each center of the cell we compute the magnetic field strength.
// We colorize the cell according the field value.
// Cells with a field value below "fieldMin" are not drawn.
// See SoDet/scripts/Python/MagneticField.py for a triggering
// of the representation from python.
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(!fMagneticFieldSvc) return;
  if(!fUISvc) return;
  if(!fUISvc->session()) return;
  ISession& session = *(fUISvc->session());

  SoRegion* soRegion = fUISvc->currentSoRegion();
  if(!soRegion) return;

  std::string value;
  std::vector<double> ds;
  std::vector<int> ls;

  // Default values :
  float sx = 100;  // Size of a cell in x.
  float sy = 100;  // Size of a cell in y.
  unsigned int nx = 80; // Number of cell in x.
  unsigned int ny = 80; // Number of cell in y.
  SbVec3f gridStart(-(sx * nx)/2,-(sy * ny)/2,1000);
  SbVec3f gridX(1,0,0);
  SbVec3f gridY(0,1,0);
  float fieldMin = 0.0001F; // Below that, field is colored in grey.
  float fieldMax = 0.003F;
  float colorMin = 0.3F; // Color for fieldMin.
  float colorMax = 1;    // Color for fieldMax.
  float transparency = 0;

  double dvalue;
  if(session.parameterValue("modeling.magneticField.gridPosition",value))
    if(Lib::smanip::todoubles(value,ds) && (ds.size()==3) ) {
      gridStart.setValue((float)ds[0],(float)ds[1],(float)ds[2]);
    }
  if(session.parameterValue("modeling.magneticField.gridX",value)) 
    if(Lib::smanip::todoubles(value,ds) && (ds.size()==3) ) {
      gridX.setValue((float)ds[0],(float)ds[1],(float)ds[2]);
    }
  if(session.parameterValue("modeling.magneticField.gridY",value))
    if(Lib::smanip::todoubles(value,ds) && (ds.size()==3) ) {
      gridY.setValue((float)ds[0],(float)ds[1],(float)ds[2]);
    }
  if(session.parameterValue("modeling.magneticField.gridCells",value))
    if(Lib::smanip::toints(value,ls) && (ls.size()==2) ) {
      nx = ls[0];
      ny = ls[1];
    }
  if(session.parameterValue("modeling.magneticField.gridCellSize",value))
    if(Lib::smanip::todoubles(value,ds) && (ds.size()==2) ) {
      sx = (float)ds[0];
      sy = (float)ds[1];
    }
  if(session.parameterValue("modeling.magneticField.min",value))
    if(Lib::smanip::todouble(value,dvalue)) fieldMin = (float)dvalue;
  if(session.parameterValue("modeling.magneticField.max",value))
    if(Lib::smanip::todouble(value,dvalue)) fieldMax = (float)dvalue;
  if(session.parameterValue("modeling.magneticField.colorMin",value))
    if(Lib::smanip::todouble(value,dvalue)) colorMin = (float)dvalue;
  if(session.parameterValue("modeling.magneticField.colorMax",value))
    if(Lib::smanip::todouble(value,dvalue)) colorMax = (float)dvalue;
  if(session.parameterValue("modeling.transparency",value))
    if(Lib::smanip::todouble(value,dvalue)) transparency = (float)dvalue;

  bool editable = false; // no dragger by default.
  if(session.parameterValue("modeling.magneticField.editable",value)) {
    Lib::smanip::tobool(value,editable);
  }

  if(fieldMax<=fieldMin) return;
  if(colorMax<=colorMin) return;
  if(nx*ny==0) return;

  SoStyleCache* styleCache = soRegion->styleCache();

  // Build name (for picking) :
  char sid[64];
  ::sprintf(sid,"MagneticField/0x%lx",(unsigned long)aIdentifier);
    
  SoSeparator* separator = new SoSeparator;

  SoSceneGraph* cells = new SoSceneGraph;
  cells->setString(sid);
  separator->addChild(cells);

  cells->addChild(styleCache->getLightModelBaseColor());
  cells->addChild(styleCache->getFilled());
  cells->addChild(styleCache->getNormalBindingPerFace());
  cells->addChild(styleCache->getNormalZ());

  // add a not used material for the highlight logic.
  // (If not then a pick of the mag field will put in
  // white a sub set of the cells).
 {SoMaterial* soMaterial = new SoMaterial;
  cells->addChild(soMaterial);
  soMaterial->diffuseColor.setValue(SbColor(0,0,0));}

  SoMaterial* soMaterial = new SoMaterial;
  cells->addChild(soMaterial);

  SoMaterialBinding* soMaterialBinding = new SoMaterialBinding;
  soMaterialBinding->value.setValue(SoMaterialBinding::PER_FACE_INDEXED);
  cells->addChild(soMaterialBinding);

  SoCoordinate3* coordinate3 = new SoCoordinate3;
  cells->addChild(coordinate3);

  SoIndexedFaceSet* faceSet = new SoIndexedFaceSet;
  cells->addChild(faceSet);

  alloc_sceneGraph(*coordinate3,*soMaterial,*faceSet,nx,ny);
  fill_sceneGraph(*coordinate3,*soMaterial,
                  nx,ny,sx,sy,
                  gridStart,gridX,gridY,
                  transparency,colorMin,colorMax,
                  fieldMin,fieldMax,
                  *fMagneticFieldSvc);

  if(editable) {
    // add a dragger :
    SoSeparator* sp = new SoSeparator;
    separator->addChild(sp);

    float scale = 1000;
    SoTransform* tsf = new SoTransform();
    tsf->translation.setValue(gridStart);
    tsf->scaleFactor.setValue(scale,scale,scale);
    sp->addChild(tsf);

    MagField_dragger* dragger = 
      new MagField_dragger(scale,
                    *coordinate3,*soMaterial,
                    nx,ny,sx,sy,
                    gridStart,gridX,gridY,
                    transparency,colorMin,colorMax,
                    fieldMin,fieldMax,
                    *fMagneticFieldSvc);

    //SoTools::setDraggerColor(*dragger,SbColor_red);
    sp->addChild(dragger);
  }
  
  soRegion->resetUndo();    
  region_addToStaticScene(*soRegion,separator);

}
