
// this :
#include "SoLVolumeCnv.h"

#include <Lib/Interfaces/ISession.h>
#include <Lib/smanip.h>

#include <GaudiKernel/Converter.h>
#include <GaudiKernel/CnvFactory.h>
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/MsgStream.h>

#include <DetDesc/CLIDLVolume.h>
#include <DetDesc/LVolume.h>
#include <DetDesc/IGeometryInfo.h>
#include <DetDesc/ILVolume.h>
#include <DetDesc/ISolid.h>

#include <OnXSvc/ISoConversionSvc.h>
#include <OnXSvc/IUserInterfaceSvc.h>
#include <OnXSvc/ClassID.h>
#include <OnXSvc/Helpers.h>

#include "SoDetConverter.h"

DECLARE_CONVERTER_FACTORY(SoLVolumeCnv)

//////////////////////////////////////////////////////////////////////////////
SoLVolumeCnv::SoLVolumeCnv( ISvcLocator* aSvcLoc ) 
  :SoDetConverter(aSvcLoc, CLID_LVolume)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoLVolumeCnv::createRep(DataObject* aObject,IOpaqueAddress*&) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  MsgStream log(msgSvc(), "SoLVolumeCnv");

  if(!fUISvc) {
    log << MSG::INFO << " UI service not found" << endmsg;
    return StatusCode::SUCCESS; 
  }

  ISession* session = fUISvc->session();
  if(!session) {
    log << MSG::INFO << " can't get OnX session." << endmsg;
    return StatusCode::FAILURE; 
  }

  SoRegion* region = fUISvc->currentSoRegion();
  if(!region) {
    log << MSG::INFO << " can't get viewing region." << endmsg;
    return StatusCode::FAILURE; 
  }

  if(!aObject) {
    log << MSG::INFO << " NULL object." << endmsg;
    return StatusCode::FAILURE; 
  }

  if(!fSoCnvSvc) {
    log << MSG::INFO << " SoConversionSvc not found" << endmsg;
    return StatusCode::FAILURE; 
  }

  std::string value;
  bool opened = false;
  if(session->parameterValue("modeling.opened",value))
    Lib::smanip::tobool(value,opened);

  log << MSG::INFO << "SoLVolumeCnv::createReps() called" << endmsg;
  
  const ILVolume* lv  = dynamic_cast<const ILVolume*>(aObject);
  if(!lv) { 
    log << MSG::INFO << " DataObject has not ILVolume interface! " << endmsg;
    return StatusCode::FAILURE; 
  }

  SoNode* node = volumeToSoDetectorTreeKit(*lv,Gaudi::Transform3D(),opened);
  if(!node) { 
    log << MSG::INFO << " no representation" << endmsg;
    return StatusCode::FAILURE; 
  }

  // Send scene graph to the viewing region (in the "static" sub-scene graph) :
  region_addToStaticScene(*region,node);

  return StatusCode::SUCCESS;
}
//////////////////////////////////////////////////////////////////////////////
const CLID& SoLVolumeCnv::classID( ) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{ 
  return CLID_LVolume;
}
//////////////////////////////////////////////////////////////////////////////
unsigned char SoLVolumeCnv::storageType( ) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{ 
  return So_TechnologyType; 
}
