#ifndef SoDet_SoLVolumeCnv_h
#define SoDet_SoLVolumeCnv_h

#include "SoDetConverter.h"

template <class T> class CnvFactory;

class SoLVolumeCnv : public SoDetConverter {
  friend class CnvFactory<SoLVolumeCnv>;
public:
  SoLVolumeCnv(ISvcLocator*);
  // The next method should be replaced by one with a single arguement
  virtual StatusCode createRep(DataObject*,IOpaqueAddress*&);
public:
  static const CLID& classID();
  static unsigned char storageType();
};

#endif
