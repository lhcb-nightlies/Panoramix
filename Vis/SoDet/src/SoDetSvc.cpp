// this :
#include "SoDetSvc.h"

// Lib :
#include <Lib/Interfaces/ISession.h>
#include <Lib/Printer.h>

// Gaudi :
#include <GaudiKernel/SvcFactory.h>
#include <GaudiKernel/MsgStream.h>

#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiKernel/IMagneticFieldSvc.h>

// OnXSvc :
#include <OnXSvc/IUserInterfaceSvc.h>
#include <OnXSvc/ISoConversionSvc.h>

// SoDet :
#include "Types.h"

DECLARE_SERVICE_FACTORY(SoDetSvc)

//////////////////////////////////////////////////////////////////////////////
SoDetSvc::SoDetSvc(
 const std::string& aName
,ISvcLocator* aSvcLoc
) 
:Service(aName,aSvcLoc)
,m_uiSvc(0)
,m_magneticFieldSvc(0)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
}
//////////////////////////////////////////////////////////////////////////////
SoDetSvc::~SoDetSvc(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{ 
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoDetSvc::initialize(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  StatusCode status = Service::initialize();
  if( status.isFailure() ) return status;

  MsgStream log(msgSvc(), Service::name());

  log << MSG::DEBUG << "SoDetSvc::initialize " << endmsg;

  setProperties();

  if(!serviceLocator()) {
    log << MSG::ERROR << " service locator not found " << endmsg;
    return StatusCode::FAILURE;
  }

  // Get the Detector data service :
  
  if(m_uiSvc) {
    m_uiSvc->release();
    m_uiSvc = 0;
  }
  status = service("OnXSvc",m_uiSvc,true);
  if(status.isFailure() || !m_uiSvc) {
    log << MSG::ERROR << " OnXSvc not found " << endmsg;
    return StatusCode::FAILURE;
  }
  m_uiSvc->addRef();

  m_uiSvc->addType(new LVolumeType(m_uiSvc->printer()));

  if(m_magneticFieldSvc) {
    m_magneticFieldSvc->release();
    m_magneticFieldSvc = 0;
  }
  status = service("MagneticFieldSvc",m_magneticFieldSvc,true);
  if(status.isFailure() || !m_magneticFieldSvc) {
    log << MSG::WARNING << " MagneticFieldSvc not found " << endmsg;
  } else {
    m_magneticFieldSvc->addRef();
    m_uiSvc->addType(new MagneticFieldType(m_uiSvc,m_magneticFieldSvc));
  }

  return status;
}
//////////////////////////////////////////////////////////////////////////////
StatusCode SoDetSvc::finalize(
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(m_uiSvc) {
    m_uiSvc->release();
    m_uiSvc = 0;
  }
  if(m_magneticFieldSvc) {
    m_magneticFieldSvc->release();
    m_magneticFieldSvc = 0;
  }

  MsgStream log(msgSvc(), Service::name());
  log << MSG::DEBUG << "SoDetSvc finalized successfully" << endmsg;
  return Service::finalize();
}
