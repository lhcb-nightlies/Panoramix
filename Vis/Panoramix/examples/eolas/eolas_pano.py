import os
from ROOT import TCanvas,TPad,TText,TImage,TMarker,TPaveLabel,gROOT,kGreen,kWhite,kYellow,kCircle,kCyan,TClonesArray,TPolyLine,Double,TF1,TH1F,kFullDotLarge,TArc,TLine,TGaxis,TButton,TH1F
from panoramixmodule import *
from math import atan2,cos,sin,acos,asin,log,tan,atan
#storeTz = []
#real pos: ec 12.7 hc 13.7 m 12.1 15.3 16.7 17.6 19.0
#plot pos: ec  8k  hc 12k  m  7.5 16   17   18   19
storerealz=[0.,200.,400.,600.,800.,1000.,1400.,1800.,2200.,2600.,3000.,4000.,5000.,6000.,
7000.,8000.,9000.,10000.,11000.,12000.,12700.,13700.,15300.,16600.,17600.,19000.,20000.]
#drawn positions(27)
storeTz=[0.,200.,400.,600.,800.,1000.,1400.,1800.,2200.,2600.,3000.,3500.,4000.,4500.,
5000.,5500.,6000.,6500.,7000.,7500.,8000.,12000.,16000.,17000.,18000.,19000.,20000.]
#book once
maxArraySize=10000
h2plot = TClonesArray('TLine',maxArraySize)
h2plot1 = TClonesArray('TLine',maxArraySize)
e2plot = TClonesArray('TLine',maxArraySize)
e2plot1 = TClonesArray('TLine',maxArraySize)
m2plot = TClonesArray('TMarker',maxArraySize)
mass2plot = TClonesArray('TMarker',maxArraySize) 
t2plot = TClonesArray('TPolyLine',maxArraySize)
tx2plot = TClonesArray('TPolyLine',maxArraySize)
tmasterplot = TClonesArray('TPolyLine',maxArraySize)
storeTpt = []
storeTeta = []
storeTkey = []
storeE =[]
storeEphi =[]
storeEeta =[]
storeEass =[]
storeEid =[]
storeEcol =[]
storeErow =[]
storeH =[]
storeHphi=[]
storeHeta=[]
storeHass=[]
storeHid =[]
storeHcol =[]
storeHrow =[]
storeMeta=[]
storeMx=[]
storeMy=[]
storeMasseta=[]
storeMassx=[]
storeMassy=[]

#pad0 = TPad("pad0","pad0",0,0,1,1)
#pad0.Range(-2000,-2000,2000,2000)
#pad0.SetFillColor(1)
#eolas_container.append(pad0)

def Setup():
 c0  =TCanvas("eolas_c0","Eolas Information",500,700)
 eolas_container.append(c0)
 pad1=TPad("pad1","pad1",0.02,0.02,.98,.98)
 pad1.SetFillColor(1)
 pad1.Draw()
 pad1.cd()
 label=TPaveLabel(.2,.92,.8,.97,"Eolas Viewer","br")
 label.Draw()
 t0=TText(0,0,"a")
 t0.SetTextFont(42)
 t0.SetTextAlign(12)
 t0.SetTextSize(0.02)
 t0.SetTextColor(0)
 t0.DrawText(.55,.01,"Comments and queries to ronan.mcnulty@ucd.ie")
 eolas_container.append(pad1)


 pad2=TPad("pad2","pad2",0.02,0.64,.98,.9)
 pad3=TPad("pad3","pad3",0.02,0.22,.98,.62)
 pad4=TPad("pad4","pad4",0.02,0.02,.98,.2)
 eolas_container.append(pad2)
 eolas_container.append(pad3)
 eolas_container.append(pad4)

 pad2.SetFillColor(kGreen)
 pad3.SetFillColor(kYellow)
 pad4.SetFillColor(kCyan)
 pad2.Draw()
 pad3.Draw()
 pad4.Draw()

 pad2.cd()
 t00=TText(0,0,"a")
 t00.SetTextFont(62)
 t00.SetTextAlign(12)
 t00.SetTextSize(0.09)
 ygap=.09
 y=1-ygap
 t00.DrawText(.02,y,"Overview")
 y-=ygap
 y-=ygap
 t=TText(0,0,"a")
 t.SetTextFont(42)
 t.SetTextAlign(12)
 t.SetTextSize(0.09)
 t.DrawText(.02,y,"This viewer provides a synoptic map of physics quantities which are")
 y-=ygap
 t.DrawText(.02,y,"important for energy flow and particle identification")
 y-=ygap
 t.DrawText(.02,y,"Note, it is not a literal representation of LHCb events.")
 y-=ygap
 y-=ygap
 t.DrawText(.02,y,"The phi co-ordinate is simply the LHCb phi co-ordinate.")
 y-=ygap
 t.DrawText(.02,y,"However the radial co-ordinate, depending on context, represents:")
 y-=ygap
 t.DrawText(.05,y,"- either a transmogified LHCb z co-ordinate,")
 y-=ygap
 t.DrawText(.05,y,"- or transverse energy flow.")

 pad3.cd()
 t2=TText(0,0,"a")
 t2.SetTextFont(62)
 t2.SetTextAlign(12)
 t2.SetTextSize(0.058)
 y=1-ygap
 ygap=.052
 t2.DrawText(.02,y,"Description of the view")
 y-=ygap
 y-=ygap
 t1=TText(0,0,"a")
 t1.SetTextFont(42)
 t1.SetTextAlign(12)
 t1.SetTextSize(0.051)
 t1.DrawText(.02,y,"Extrapolated track positions are drawn as dashed lines where the radial position")
 y-=ygap
 t1.DrawText(.02,y,"is approximately LHCb z.  However the scale is distorted to aid with visualisation.")
 y-=ygap
 t1.DrawText(.02,y,"The Ecal is drawn at 8m, the Hcal at 12m, and the five muon stations")
 y-=ygap
 t1.DrawText(.02,y,"are equally spaced between 16m and 20m.")
 y-=ygap
 y-=ygap
 t1.DrawText(.02,y,"The transverse momentum of the tracks is indicated by a solid line overlaying the")
 y-=ygap
 t1.DrawText(.02,y,"extrapolated track.  The length of this line is propotional to the transverse")
 y-=ygap
 t1.DrawText(.02,y,"momentum with the scale as indicated in the key.")
 y-=ygap
 y-=ygap
 t1.DrawText(.02,y,"The transverse energy in the Ecal and Hcal are represented by bars whose length")
 y-=ygap
 t1.DrawText(.02,y,"is proportional to the energy, with the scale indicated in the key.")
 y-=ygap
 t1.DrawText(.02,y,"Calorimeter deposits that have been associated to tracks have darker shading.")
 y-=ygap
 y-=ygap
 t1.DrawText(.02,y,"Muon chamber hits are represented by open circles.")
 y-=ygap
 t1.DrawText(.02,y,"If these have been associated to tracks, the circles are filled.")



 pad4.cd()
 t3=TText(0,0,"a")
 t3.SetTextFont(62)
 ygap=.1
 t3.SetTextAlign(12)
 t3.SetTextSize(ygap)
 y=1-ygap
 t3.DrawText(.02,y,"Controlling the view")
 y-=ygap
 y-=ygap
 t4=TText(0,0,"a")
 t4.SetTextFont(42)
 t4.SetTextAlign(12)
 t4.SetTextSize(ygap)
 t4.DrawText(.02,y,"The scale for the transverse energies can be changed.")
 y-=ygap
 t4.DrawText(.02,y,"Drag the right-hand-side of the respective key to the desired scale.")
 y-=ygap
 y-=ygap
 t4.DrawText(.02,y,"The eta range can also be modified.  By default it encompasses all of LHCb.")
 y-=ygap
 t4.DrawText(.02,y,"Drag the eta bar to limit the view to objects within this acceptance.")
 y-=ygap
 y-=ygap
 t4.DrawText(.02,y,"Clicking on the print button will produce a colour scheme more suitable for printing")

 pad1.cd()

def decode():
 tracks = evt['Rec/Track/Best']
 print 'number of tracks: ', tracks.size()
#for i in range (101):
#  extrap_z=200.*i
#  storeTz.append(extrap_z)
 ii=0
 ikey=0
 for t in tracks :
  if t.type() == 3:  
     px=0.001*t.momentum().x()
     py=0.001*t.momentum().y()
     pz=0.001*t.momentum().z()
     pt=pow(px*px+py*py,.5)
     storeTpt.append(pt)
     storeTeta.append(t.momentum().eta())
     storeTkey.append(ikey)
     st=t.firstState()
     s=st.clone()
     tmasterplot[ii]=TPolyLine(27)
     tmasterplot[ii].SetPoint(0,0.,0.)
     xold=0.
     yold=0.
     i=0
     for extrap_z in storerealz:
       result = extrap.propagate(s,extrap_z)
       phi=atan2(s.y(),s.x())
       draw_z=storeTz[i]
       x3=draw_z/10.*cos(phi)
       y3=draw_z/10.*sin(phi)
       tmasterplot[ii].SetPoint(i,0.,0.)
       theta=atan2(pow(pow(s.x(),2)+pow(s.y(),2),.5),s.z())
       if theta>0.3:
         x3=xold
         y3=yold
       tmasterplot[ii].SetPoint(i,x3,y3)
       xold=x3
       yold=y3
       i+=1
     ii += 1
  ikey+=1

#calo H
 hcal = det['/dd/Structure/LHCb/DownstreamRegion/Hcal']
 hhits = evt['Raw/Hcal/Digits']
 hsize = hhits.size()
 print 'number of Hcal hits: ', hsize
 for h2 in hhits.containedObjects():
  en = h2.e()
  cid = h2.cellID()
  x = hcal.cellX(cid)
  y = hcal.cellY(cid)
  z = hcal.cellZ(cid)
  phi = atan2(y,x)
  storeHphi.append(phi)
  theta=acos(z/pow(x*x+y*y+z*z,0.5))
  # print log 

  eta=-log(tan(theta/2.))
  storeHeta.append(eta)
  storeH.append(en*sin(theta))
  storeHass.append(0)
  storeHid.append(cid.calo())
  storeHrow.append(cid.row())
  storeHcol.append(cid.col())


 proto = evt['Rec/ProtoP/Charged']
 for e2 in proto:
  for c in e2.calo():
    for d in c.digits():
      cellid=d.cellID().calo()
      cellrow=d.cellID().row()
      cellcol=d.cellID().col()
      jj=0
      for allcell in storeHid:
#        print 'all',storeEid[jj],storeEcol[jj],storeErow[jj]
        if storeHid[jj]==cellid and storeHcol[jj]==cellcol and storeHrow[jj]==cellrow:
          storeHass[jj]=1
#          print 'possible match',cellid,storeHid[jj],cellrow,cellcol
        jj+=1
        
 proto = evt['Rec/ProtoP/Charged']
 for h2 in proto:
  magicnumber=h2.CaloHcalE
  en=h2.info(magicnumber,-99)
  magictrk=h2.TrackType
  ttype=h2.info(magictrk,-99)
  jj=0
  for ee in storeH:
    if abs(ee-en) < 0.001:
      storeHass[jj]=1
    jj = jj+1

#calo E
 ecal = det['/dd/Structure/LHCb/DownstreamRegion/Ecal']
 ehits = evt['Raw/Ecal/Digits']
 print 'number of Ecal hits: ', ehits.size()
 for e2 in ehits.containedObjects():
  en = e2.e()
  cid = e2.cellID()
  x = ecal.cellX(cid)
  y = ecal.cellY(cid)
  z = ecal.cellZ(cid)
  theta=acos(z/pow(x*x+y*y+z*z,0.5))
  eta=-log(tan(theta/2.))
  phi = atan2(y,x)
  storeE.append(en*sin(theta))
  storeEass.append(0)
  storeEphi.append(phi)
  storeEeta.append(eta)
  storeEid.append(cid.calo())
  storeErow.append(cid.row())
  storeEcol.append(cid.col())
# 
 proto = evt['Rec/ProtoP/Charged']
 print 'number of proto particles: ', proto.size()
 for p in proto:
  tracktype=p.track().type()
  if tracktype==3:
    for c in p.calo():
      for d in c.digits():
        cellid=d.cellID().calo()
        cellrow=d.cellID().row()
        cellcol=d.cellID().col()
        jj=0
        for allcell in storeEid:
          # ? how to check associated ecal or hcal.  just match row+col
          # should also match detector.  ecal not stored??
          if storeEid[jj]==cellid and storeEcol[jj]==cellcol and storeErow[jj]==cellrow:
            storeEass[jj]=1
#            print 'track',e2.track().key(),'hit',storeE[jj],storeEphi[jj]
          jj+=1
#for e2 in proto:
#  magicnumber=e2.CaloEcalE
#  en=e2.info(magicnumber,-999)
#  magictrk=e2.TrackType
#  ttype=e2.info(magictrk,-999)
#  if en>-999:
#    jj=0
#    for ee in storeE:
#      if abs(ee-en) < 0.001:
#        storeEass[jj]=1
#      jj+=1
    
 # dummy = GaudiPython.gbl.MuonTrack()  
 muon = det['/dd/Structure/LHCb/DownstreamRegion/Muon']
 muonhits = evt['Raw/Muon/Coords']
 print 'number of muon hits: ', muonhits.size()
 ii=0
 x = Double(0)
 y = Double(0)
 z = Double(0)
 dx = Double(0)
 dy = Double(0)
 dz = Double(0)
 for m2 in muonhits.containedObjects():
  muon.Tile2XYZ(m2.key(),x,dx,y,dy,z,dz)
#  print m2.key().station(),x,y,z
  if m2.key().station()==0:
#    rad=1600
    rad=750
  if m2.key().station()==1:
#    rad=1680
    rad=1600
  if m2.key().station()==2:
#    rad=1760
    rad=1700
  if m2.key().station()==3:
#    rad=1840
    rad=1800
  if m2.key().station()==4:
#    rad=1920
    rad=1900
  phi=atan2(y,x)
  theta=acos(z/pow(x*x+y*y+z*z,0.5))
  eta=-log(tan(theta/2.))
  storeMeta.append(eta)
  x1=rad*cos(phi)
  y1=rad*sin(phi)
  storeMx.append(x1)
  storeMy.append(y1)
  ii += 1


# muon hits on tracks
 muonpids = evt['Rec/Muon/MuonPID']
 print 'number of muon pids: ', muonpids.size()
 if muonpids:
  ii=0
  for mm in muonpids:
   momofseed = mm.idTrack().momentum().r() / 1000.
   if mm.IsMuonLoose() and momofseed>10:
    for m1 in mm.muonTrack().lhcbIDs():
      m2=m1.muonID()
      muon.Tile2XYZ(m2,x,dx,y,dy,z,dz)
      if m2.station()==0:
        rad=750
      if m2.station()==1:
        rad=1600
      if m2.station()==2:
        rad=1700
      if m2.station()==3:
        rad=1800
      if m2.station()==4:
        rad=1900
      phi=atan2(y,x)
      theta=acos(z/pow(x*x+y*y+z*z,0.5))
      eta=-log(tan(theta/2.))
      storeMasseta.append(eta)
      x1=rad*cos(phi)
      y1=rad*sin(phi)
      storeMassx.append(x1)
      storeMassy.append(y1)
      x1=rad*cos(phi)
      y1=rad*sin(phi)
      ii += 1
 print "finished with decode"
def draw():
 c1 = gROOT.FindObjectAny('eolas_c0')
 pad1 = gROOT.FindObjectAny('pad1')
 if not pad1:
  pad1=TPad("pad1","pad1",0,0,1,1)
  pad1.Range(-2000,-2000,2000,2000)
  pad1.SetFillColor(1)
  eolas_container.append(pad1)
 pad1.Draw()
 pad1.cd()
 #Scale pt,e,h (large division = this many GeV)
 tscale= 10.
 escale= 10.
 hscale=10.
 eta_lo=1.5
 eta_hi=5.5
 pt_lo=0.
 iprint=0
 inext=0
 scalepar=gROOT.FindObject("scalepar")
 if scalepar:
  scalepar.Reset()
 else:
  scalepar=TH1F("scalepar","scalepar",10,0.,1.)
 scalepar.SetBinContent(1,tscale)
 scalepar.SetBinContent(2,escale)
 scalepar.SetBinContent(3,hscale)
 scalepar.SetBinContent(4,100.)
 scalepar.SetBinContent(5,eta_lo)
 scalepar.SetBinContent(6,eta_hi)
 scalepar.SetBinContent(7,iprint)
 scalepar.SetBinContent(8,pt_lo)
 scalepar.SetBinContent(9,inext)
 scalepar.SetBinContent(10,0)

 pad1.AddExec("ex",".x $PANORAMIXROOT/examples/eolas/clickity.C")

 for iloop in range(1,2):
  tscale=scalepar.GetBinContent(1)
  escale=scalepar.GetBinContent(2)
  hscale=scalepar.GetBinContent(3)
  iredraw=scalepar.GetBinContent(4)
  eta_lo=scalepar.GetBinContent(5)
  eta_hi=scalepar.GetBinContent(6)
  iprint=scalepar.GetBinContent(7)
  pt_lo=scalepar.GetBinContent(8)
  inext=scalepar.GetBinContent(9)
  itrackinfo=scalepar.GetBinContent(10)

  iredraw,inext = 2,0
  if iredraw>1:
    iredraw=0
    scalepar.SetBinContent(4,0.)
    # Next event??
    if inext>0:
      inext=0
      scalepar.SetBinContent(9,0.)
      ipad1.Delete()
      pad1.Delete()
      break
    # print info on one track?
    if itrackinfo>0:
      print 'Information requested for track ',itrackinfo
      itrackinfo-=100
      itrackinfo=int(itrackinfo)
      itrackkey=storeTkey[itrackinfo]
      print ' track in list ',itrackinfo,itrackkey
      print tracks[itrackkey]
      itrackinfo=0
# h should be at 13.7m.
# e should be at 12.7m
# m should be at 19,17.6,16.6,15.3,12.1
    mu_radius=1600.
    h_radius=1200.
    e_radius=800.
    style=iprint
    if style==0:
      bground=1
      base=20
      monitor=1
      muonfound=kFullDotLarge
      muonnotfound=kCircle
      muonsize=1
      xtrapline=7
      trackcolor=kWhite
      textcolor=kWhite
      ptline=1
      ecolor= kYellow
    if style==2:
      bground=1
      base=kMagenta
      monitor=1
      muonfound=kFullStar
      muonnotfound=kOpenStar
      muonsize=2
      xtrapline=7
      trackcolor=kYellow-7
      textcolor=kWhite
      ptline=1
      ecolor=kOrange+1
    if style==1:
      bground=0
      base=kWhite
      monitor=0
      muonfound=kFullDotLarge
      muonnotfound=kCircle
      muonsize=1
      xtrapline=7
      trackcolor=kBlack
      textcolor=kBlack
      ptline=1
      ecolor=kOrange

    pad1.SetFillColor(bground)
    det_all = TArc(0.,0.,2000.)
    det_all.SetFillColor(monitor*(base-8))
    det_all.Draw()
    det_mu = TArc(0.,0.,mu_radius)
    det_mu.SetFillColor(monitor*(base-7))
    det_mu.Draw()
    det_h = TArc(0.,0.,h_radius)
    det_h.SetFillColor(monitor*(base-6))
    det_h.Draw()
    det_e = TArc(0.,0.,e_radius)
    det_e.SetFillColor(monitor*(base-5))
    det_e.Draw()
    det_s = TArc(0.,0.,400.)
    det_s.SetFillColor(monitor*(base-4))
    det_s.Draw()
    tright=-1900+40.*tscale
    t_bar=TLine(-1900,-1500,tright,-1500)
    t_bar.SetLineWidth(5)
    t_bar.SetLineColor(trackcolor)
    t_bar.SetUniqueID(901)
    t_bar.Draw()
    eright=-1900+40.*escale
    e_bar=TLine(-1900,-1600,eright,-1600)
    e_bar.SetLineWidth(5)
    e_bar.SetLineColor(ecolor)
    e_bar.SetUniqueID(902)
    e_bar.Draw()
    hright=-1900+40.*hscale
    h_bar=TLine(-1900,-1700,hright,-1700)
    h_bar.SetLineWidth(5)
    h_bar.SetLineColor(7)
    h_bar.SetUniqueID(903)
    h_bar.Draw()
    eta_left=900+300*(eta_lo-2.)
    eta_right=900+300*(eta_hi-2.)
    eta_bar=TLine(eta_left,-1700,eta_right,-1700)
    eta_bar.SetLineWidth(5)
    eta_bar.SetLineColor(textcolor)
    eta_bar.SetUniqueID(904)
    eta_bar.Draw()
    ptright=1900-600*(pt_lo)
    pt_bar=TLine(1300,-1400,ptright,-1400)
    pt_bar.SetLineWidth(5)
    pt_bar.SetLineColor(trackcolor)
    pt_bar.SetUniqueID(905)
    pt_bar.Draw()
    axis=TGaxis(-1900,-1800,-1100,-1800,0,20,404)
    axis.SetLabelColor(1-monitor)
    axis.SetLineColor(1-monitor)
    axis.SetLabelSize(0.02)
    axis.SetTitle("Transverse Energy (GeV)")
    axis.SetTitleSize(0.015)
    axis.SetTitleOffset(1.3)
    axis.SetTitleColor(1-monitor)
    axis.Draw()
    axis1=TGaxis(900,-1800,1800,-1800,2,5,303)
    axis1.SetLabelColor(1-monitor)
    axis1.SetLineColor(1-monitor)
    axis1.SetLabelSize(0.02)
    axis1.SetTitle("Pseudorapidity Range")
    axis1.SetTitleSize(0.015)
    axis1.SetTitleOffset(1.3)
    axis1.SetTitleColor(1-monitor)
    axis1.Draw()
    faxis=TF1("faxis","1-x",0,1)
    axis2=TGaxis(1300,-1500,1900,-1500,"faxis",404)
    axis2.SetLabelColor(1-monitor)
    axis2.SetLineColor(1-monitor)
    axis2.SetLabelSize(0.02)
    axis2.SetTitle("Minimum Pt (GeV)")
    axis2.SetTitleSize(0.015)
    axis2.SetTitleOffset(1.3)
    axis2.SetTitleColor(1-monitor)
    axis2.Draw()
    button=TButton("Print","",.92,.88,.98,.91)
    button.SetUniqueID(906);
    button.Draw()
    button1=TButton("Next","",.92,.94,.98,.97)
    button1.SetUniqueID(907);
    button1.Draw()
    ss1='Run: '+str(evt['Rec/Header'].runNumber())
    ss2='Evt: '+str(evt['Rec/Header'].evtNumber())
    button2=TButton(ss1,"",.78,.942,.91,.97)
    button3=TButton(ss2,"",.78,.912,.91,.94)
    button2.SetFillColor(bground)
    button2.SetTextColor(textcolor)
    button3.SetFillColor(bground)
    button3.SetTextColor(textcolor)
    button2.Draw()
    button3.Draw()
    ipad1=TPad("ipad1","ipad1",0.005,0.9,.24,.995)
    ipad1.SetFillStyle(4000)
    ipad1.SetBorderMode(0)
    ipad1.SetBorderSize(0)
    ipad1.Draw()
    ipad1.cd()    
    image1=TImage.Open(os.environ['PANORAMIXROOT']+"/examples/eolas/eolaslhcb.jpg")
    image1.Draw()
    pad1.cd()
#clear
    h2plot.Delete()
    h2plot1.Delete()
    e2plot.Delete()
    e2plot1.Delete()
    m2plot.Delete()
    mass2plot.Delete()
    t2plot.Delete()
    tx2plot.Delete()
#    h2plot = TClonesArray('TLine')
#    h2plot1 = TClonesArray('TLine')
#    e2plot = TClonesArray('TLine')
#    e2plot1 = TClonesArray('TLine')
#    m2plot = TClonesArray('TMarker')
#    mass2plot = TClonesArray('TMarker') 
#    t2plot = TClonesArray('TPolyLine')
#    tx2plot = TClonesArray('TPolyLine')

    
    # calc tracks
    ii=0
    iplot=0
    for eta in storeTeta:
      pt=storeTpt[ii]
      if eta>eta_lo and eta<eta_hi and pt>pt_lo:
        x4=tmasterplot[ii].GetX()
        y4=tmasterplot[ii].GetY()
        tx2plot[iplot]=TPolyLine(27)
        tx2plot[iplot].SetUniqueID(100+iplot)
        t2plot[iplot]=TPolyLine(27)
        for i in range(27):
          tx2plot[iplot].SetPoint(i,x4[i],y4[i])
          z=storeTz[i]
          t2plot[iplot].SetPoint(i,0.,0.)
          if pt*400./tscale > z/10.:
            t2plot[iplot].SetPoint(i,x4[i],y4[i])
        iplot+=1
      else:
        tx2plot[iplot]=TPolyLine(27)
        for i in range(27):
          tx2plot[iplot].SetPoint(i,0.,0.)
        iplot+=1
      ii+=1
          
    #histo Ecal
    ii=0
    ehist=gROOT.FindObject("ehist")
    if ehist:
      ehist.Reset()
    else:
      ehist=TH1F("ehist","ehist",800,-3.142,3.142)
    ehist1=gROOT.FindObject("ehist1")
    if ehist1:
      ehist1.Reset()
    else:
      ehist1=TH1F("ehist1","ehist1",800,-3.142,3.142)
    for eta in storeEeta:
      if eta>eta_lo and eta<eta_hi:
        ee = storeE[ii]
        phi=storeEphi[ii]
        ehist.Fill(phi,ee)
        if storeEass[ii] == 1:
          ehist1.Fill(phi,ee)
      ii+=1
    #store Ecal
    z=e_radius
    iplot=0
    for i in range(1,801):
      phi=ehist.GetBinCenter(i)
      ee=ehist.GetBinContent(i)
      if ee>0:
        x1 = z*cos(phi)
        y1 = z*sin(phi)
        off = ee/1000.*400./escale
        x2 = (z+off)*cos(phi)
        y2 = (z+off)*sin(phi)
        e2plot[iplot] = TLine(x1,y1,x2,y2)
        e2plot[iplot].SetLineWidth(5)
        e2plot[iplot].SetLineColor(ecolor)
        iplot+=1

    iplot=0
    for i in range(1,801):
      phi=ehist1.GetBinCenter(i)
      ee=ehist1.GetBinContent(i)
      if ee>0:
        x1 = z*cos(phi)
        y1 = z*sin(phi)
        off = ee/1000.*400./escale
        x2 = (z+off)*cos(phi)
        y2 = (z+off)*sin(phi)
        e2plot1[iplot] = TLine(x1,y1,x2,y2)
        e2plot1[iplot].SetLineWidth(5)
        e2plot1[iplot].SetLineColor(kRed)
        iplot+=1


    #histo Hcal
    ii=0
    hhist=gROOT.FindObject("hhist")
    if hhist:
      hhist.Reset()
    else:
      hhist=TH1F("hhist","hhist",800,-3.142,3.142)
    hhist1=gROOT.FindObject("hhist1")
    if hhist1:
      hhist1.Reset()
    else:
      hhist1=TH1F("hhist1","hhist1",800,-3.142,3.142)
    for eta in storeHeta:
      if eta>eta_lo and eta<eta_hi:
        ee = storeH[ii]
        phi=storeHphi[ii]
        hhist.Fill(phi,ee)
        if storeHass[ii] == 1:
          hhist1.Fill(phi,ee)
      ii+=1
    #store Hcal
    z=h_radius
    iplot=0
    for i in range(1,801):
      phi=hhist.GetBinCenter(i)
      ee=hhist.GetBinContent(i)
      if ee>0:
        x1 = z*cos(phi)
        y1 = z*sin(phi)
        off = ee/1000.*400./hscale
        x2 = (z+off)*cos(phi)
        y2 = (z+off)*sin(phi)
        h2plot[iplot] = TLine(x1,y1,x2,y2)
        h2plot[iplot].SetLineWidth(5)
        h2plot[iplot].SetLineColor(7)
        iplot+=1

    iplot=0
    for i in range(1,801):
      phi=hhist1.GetBinCenter(i)
      ee=hhist1.GetBinContent(i)
      if ee>0:
        x1 = z*cos(phi)
        y1 = z*sin(phi)
        off = ee/1000.*400./hscale
        x2 = (z+off)*cos(phi)
        y2 = (z+off)*sin(phi)
        h2plot1[iplot] = TLine(x1,y1,x2,y2)
        h2plot1[iplot].SetLineWidth(5)
        h2plot1[iplot].SetLineColor(kBlue)
        iplot+=1


    ii=0
    iplot=0
    for eta in storeMeta:
      if eta>eta_lo and eta<eta_hi:
        x1=storeMx[ii]
        y1=storeMy[ii]
        m2plot[iplot]=TMarker(x1,y1,4)
        iplot+=1
      ii+=1

    ii=0
    iplot=0
    for eta in storeMasseta:
      if eta>eta_lo and eta<eta_hi:
        x1=storeMassx[ii]
        y1=storeMassy[ii]
        mass2plot[iplot]=TMarker(x1,y1,8)
        iplot+=1
      ii+=1
        
# colours and styles
    for t in tx2plot:
      t.SetLineWidth(1)
      t.SetLineStyle(xtrapline)
      t.SetLineColor(trackcolor)
    for t in t2plot:
      t.SetLineWidth(3)
      t.SetLineStyle(ptline)
      t.SetLineColor(trackcolor)

    for m in mass2plot:
      m.SetMarkerColor(kGreen)
      m.SetMarkerStyle(muonfound)
      m.SetMarkerSize(muonsize)
    for m in m2plot:
      m.SetMarkerColor(kGreen-6)
      m.SetMarkerSize(muonsize)
      m.SetMarkerStyle(muonnotfound)
      
    t2plot.Draw()
    tx2plot.Draw()

    h2plot.Draw()
    h2plot1.Draw()
    e2plot.Draw()
    e2plot1.Draw()
    
    m2plot.Draw()
    mass2plot.Draw()
    pad1.Update()


#//    scalepar.SetBinContent(1,tscale)
#//    scalepar.SetBinContent(2,escale)
#//    scalepar.SetBinContent(3,hscale)

def execute():
 #open main window
 # Draw everything
 gROOT.Reset()
 del storeTpt[:]    
 del storeTeta[:]   
 del storeTkey[:]   
 del storeE[:]      
 del storeEphi[:]   
 del storeEeta[:]   
 del storeEass[:]   
 del storeEid[:]    
 del storeEcol[:]   
 del storeErow[:]   
 del storeH[:]      
 del storeHphi[:]   
 del storeHeta[:]   
 del storeHass[:]   
 del storeHid[:]    
 del storeHcol[:]   
 del storeHrow[:]   
 del storeMeta[:]   
 del storeMx[:]     
 del storeMy[:]     
 del storeMasseta[:]
 del storeMassx[:]  
 del storeMassy[:]  
 if not gROOT.FindObjectAny("eolas_c1"):
  c1 = TCanvas("eolas_c1","Eolas: LHCb Transmogrified view",1000,1000)
  eolas_container.append(c1)
  pad10=TPad("pad10","pad10",0.02,0.02,.98,.98)
  eolas_container.append(pad10)
  pad10.SetFillColor(1)
  pad10.Draw()
  pad10.cd()
  t0=TText(0,0,"a")
  t0.SetTextFont(42)
  t0.SetTextAlign(12)
  t0.SetTextSize(0.04)
  t0.SetTextColor(kYellow)
  t0.DrawText(.05,.9,"Initialising....")
  
 decode()
 draw()

extrap = appMgr.toolsvc().create('TrackMasterExtrapolator',interface='ITrackExtrapolator')
if not gROOT.FindObjectAny("eolas_c0"): Setup()

