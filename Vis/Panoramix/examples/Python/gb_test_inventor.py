#
# Example script to manipulate Inventor from Python.
#
# It assumes that the GUI with one SoPage viewer is here.
#

import CoinPython as iv
import HEPVis
import OnX

def addObjects(aRegion):
  sep = aRegion.cast_SoDisplayRegion().getStaticScene()
  if sep == None: return
  soCube = iv.SoCube()
  soCube.width = 3
  soCube.height = 1
  sep.addChild(soCube)
  soCylinder = iv.SoCylinder()
  sep.addChild(soCylinder)

def setCamera(aCamera):
  aCamera.height.setValue(10)
  aCamera.position.setValue(iv.SbVec3f(0,0,10))
  aCamera.orientation.setValue(iv.SbRotation(iv.SbVec3f(0,1,0),0))

soPage = OnX.session().ui().currentPage()
soPage.title.setValue("CoinPython test")

soRegion = OnX.session().ui().currentPage().currentRegion()
soRegion.color.setValue(iv.SbColor(0.6,0.6,0.6))

tsf = soRegion.cast_SoDisplayRegion().getTransform()
tsf.scaleFactor.setValue(iv.SbVec3f(1./3.,1,1))

# A simple animation :
timeCounter = iv.SoTimeCounter()
timeCounter.min.setValue(0)
timeCounter.max.setValue(10)
timeCounter.frequency.setValue(0.2)

calculator = iv.SoCalculator()
calculator.a.connectFrom(timeCounter.output)
# Oscillate the x scale around 1 :
calculator.expression.setValue("oA=vec3f(1+0.5*cos(0.314*a),1,1)")

tsf.scaleFactor.connectFrom(calculator.oA)

#reg = pmx.Region()
#reg.setTransformScale(1/10.,1,1)

addObjects(soRegion)

#setCamera(soRegion.getCamera())

import pmx
camera = pmx.Camera() # To work on the camera of the current region.
camera.setHeight(10)
camera.setPosition(0,0,10)
camera.setOrientation(0,1,0,0)
#camera.setNearFar(0.1,100)

viewer = pmx.Viewer()
viewer.removeDecoration()
viewer.setFrame()

