from panoramixmodule import *

# ZR view : 
if not ui().findWidget('Viewer_2d') :     
 ui().createComponent('Viewer_2d','PlanePageViewer','ViewerTabStack')
 ui().setCallback('Viewer_2d','collect','DLD','OnX viewer_collect @this@')
 ui().setCallback('Viewer_2d','popup','Python','from Panoramix import sys_import;sys_import("Viewer_popup")')
 ui().setParameter('Viewer_2d.popupItems','Current region\nNo highlighted\nTrack_Clusters\nTrack_Measurements\nTrack_MCParticle\nMCParticle_Track\nParticle_MCParticle\nParticle_Daughters\nMCParticle_Clusters\nCluster_MCParticles\nMCParticle_MCHits')
 ui().setCallback('Viewer_2d','destroy','DLD','ui_remove_from_currents @this@ OnX_viewers')
 
ui().setCurrentWidget(ui().findWidget('Viewer_2d')) 
#Page().setTitle('VELO rz view')
#Page().titleVisible(True)
page = ui().currentPage()
if not page.getNumberOfRegions() == 1 :      
 page.deleteRegions()
 page.createRegions(1,1,0)

# Setup region (a page can have multiple drawing region) :
Page().setCurrentRegion(0)
Region().setBackgroundColor('black')
session().setParameter('modeling.projection','-ZR')
session().setParameter('modeling.modeling','wireFrame')
#
#     Velo has Z in [-200, 800]. 
#     Max radius of the velo is around 60.
#     After ZR projection, Z is going to be on the x axis.
#     Have a global scale in order to bring R at the
#     same scale than Z ( scaleX = 1000/60 = 100/6 = 16.66).

# for -ZR projection
Camera().setPosition(300., 0., 100.)
Camera().setHeight(1000.)
Camera().setOrientation(0., 1., 0., 0.)
Camera().setNearFar(0.,1000.)
Region().setTransformScale(1.,8.4,1.)

Viewer().removeAutoClipping()

# separate viewer for 3d
if not ui().findWidget('Viewer_3d') :     
 ui().createComponent('Viewer_3d','PageViewer','ViewerTabStack')
 ui().setCallback('Viewer_3d','collect','DLD','OnX viewer_collect @this@')
 ui().setCallback('Viewer_3d','popup','Python','from Panoramix import sys_import;sys_import("Viewer_popup")')
 ui().setParameter('Viewer_3d.popupItems','Current region\nNo highlighted\nTrack_Clusters\nTrack_Measurements\nTrack_MCParticle\nMCParticle_Track\nParticle_MCParticle\nParticle_Daughters\nMCParticle_Clusters\nCluster_MCParticles\nMCParticle_MCHits')
 ui().setCallback('Viewer_3d','destroy','DLD','ui_remove_from_currents @this@ OnX_viewers')

 ui().setCurrentWidget(ui().findWidget('Viewer_3d')) 
 Page().titleVisible(True)
 Page().setTitle('3d view')
 Page().createDisplayRegions(1,1,0)

 Page().setCurrentRegion(0)
 Region().setBackgroundColor('black')
 Camera().setPosition(1000., 4000., 8000.,)
 Camera().setHeight(15000.)
 Camera().setOrientation(-0.6, -0.6, -0.5, 2.0)
 Camera().setNearFar(-5000.,100000.)
 Region().setTransformScale(1.,1.,1.)  

