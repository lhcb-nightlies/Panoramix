from panoramixmodule import *
TrackMasterFitter    = appMgr.toolsvc().create('TrackMasterFitter',  interface='ITrackFitter')

if not ui().findWidget('Viewer_2d')  :
  sys_import('truf_Velo_RZView_setup')  
ui().setCurrentWidget(ui().findWidget('Viewer_2d'))        
Page().setCurrentRegion(0)
session().setParameter('modeling.projection','-ZR')

session().setParameter('modeling.showCurve','true')
session().setParameter('modeling.useExtrapolator','true')
tracklocation = 'Rec/Track/Best'
#  tracklocation =  'Rec/Track/Forward'
#  tracklocation =  'Rec/Track/Velo'
session().setParameter('Track.location',tracklocation)

cl = 0
if cl > 0 :
 for t in evt[tracklocation] :
  if t.type() == t.Velo and t.states().size() < 2 :
    print 'fit track ',t.key()
    tf.fit(t)

# velo veloBack veloTT unique  seed match forward isDownstream
cl = 1
if cl > 0 : 
 Style().setLineWidth(1.5) 
 Style().setColor('green')
 data_collect(da(),'Track','velo==true&&unique==true')
 data_visualize(da())
 Style().setColor('greenyellow')
 data_collect(da(),'Track','upstream==true&&unique==true')
 data_visualize(da())
 Style().setColor('blue')
 data_collect(da(),'Track','long==true&&unique==true')
 data_visualize(da())
 Style().setColor('skyblue')
 data_collect(da(),'Track','downstream==true&&unique==true')
 data_visualize(da())
cl = 1
if cl > 0 :
 Style().setColor('violet')
 data_collect(da(),'VeloCluster','isR==false')
 data_visualize(da())

 Style().setColor('white')
 data_collect(da(),'VeloCluster','isR==true')
 data_visualize(da())

cl = 0
if cl > 0 :
 session().setParameter('modeling.what','clusters')
 Style().setColor('red')
 data_collect(da(),'Track','')
 data_visualize(da())
     
 Style().setColor('red')
 uiSvc().visualize('/Event/Rec/Vertex/Primary')  

 Style().setColor('yellow')
 session().setParameter('modeling.what','no')
 Style().setLineWidth(1.) 

Style().setColor('blue')
#session().setParameter('modeling.projection','')

