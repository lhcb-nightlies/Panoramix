
from Panoramix import *

##################################################################
# Draw Velo, IT and OT for tracking event display                #
##################################################################

# Clear the page
page = ui.currentPage()
region = page.currentRegion()
region.clear()

# Set the camera and background
region_setCamera('height 9000','position 0 2000 4500','orientation -1 -1 -1 2.0943')
region.setParameter('color','white')

# Use different colors than the standard colors from color.xml
session.setParameter('modeling.useVisSvc','false')

# Draw IT, OT and Velo
session.setParameter('modeling.modeling','wireFrame')
session.setParameter('modeling.color','0 0.5 0')
for i in range(1,4) :
 s = '/dd/Structure/LHCb/IT/ITS%01d' % i
 uiSvc.visualize(s) 
uiSvc.visualize('/dd/Structure/LHCb/OT')
for i in range(0,21) :
 s = '/dd/Structure/LHCb/Velo/Station%02d' % i
 uiSvc.visualize(s+'R')
 uiSvc.visualize(s+'L')
session.setParameter('modeling.modeling','solid')
session.setParameter('modeling.color','0.9 0.9 0.9')
#uiSvc.visualize('/dd/Structure/LHCb/Pipe')


