from Panoramix import *

##################################################################
# Draw tracks and their measurements                             #
##################################################################

page = ui().currentPage()
region = page.currentRegion()
region.setParameter("dynamicScene","clear")

# Now draw the unique forward and match tracks
filter = '((forward==true)||(match==true))&&(unique==true)'
#filter = '((upstream==true)||(seed==true))&&(unique==true)'
#filter = 'unique==true'
session().setParameter('modeling.what','no')
session().setParameter('modeling.useExtrapolator','yes')
session().setParameter('modeling.showCurve','yes')

Style().setColor('blue')
data_collect('Track',filter)
data_visualize(da())

## # Draw their measurements
session().setParameter('modeling.what','VeloMeasurements')
Style().setColor('cyan')
data_collect('Track',filter)
data_visualize(da())
session().setParameter('modeling.what','STMeasurements')
Style().setColor('red')
data_collect('Track',filter)
data_visualize(da())
session.setParameter('modeling.what','OTMeasurements')
Style().setColor('magenta')
data_collect('Track',filter)
data_visualize(da())

session.setParameter('modeling.what','no')

