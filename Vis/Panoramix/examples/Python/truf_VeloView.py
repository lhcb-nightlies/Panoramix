from panoramixmodule import *

def createPage(title): 
 Page().titleVisible(True)
 Page().setTitle(title)
 Page().createDisplayRegions(1,1,0)

# Setup region (a page can have multiple drawing region) :
 Page().setCurrentRegion(0)
# Camera positioning :
 Camera().setPosition(116.95869, 197.67953, 256.32767)
 Camera().setHeight(589.)
 Camera().setOrientation(0.4615671, -0.83176726, -0.3084138,  4.2253432)
 Camera().setNearFar(-4217.9507,9060.3965)

 Viewer().setFrame()
 Style().setColor('black')
 Style().setWireFrame()
# Style().setSolid()
# Style().setTransparency(0.5)

 for i in range(21) :
    name = 'Module'
    if i<5 :
      name = 'Module0'
    j=2*i
    s = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/'+name+str(j)
    uiSvc().visualize(s)
    j+=1
    s = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/'+name+str(j)
    uiSvc().visualize(s)

#pileup
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/ModulePU01')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/ModulePU03')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ModulePU00')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ModulePU02')


def execute(check4page=True) :
 title = 'VELO hits and tracks'
 curtitle = ui().currentPage().title.getValues()[0]
 if not (title == curtitle) and  check4page :     
  createPage(title)
  onlineViews[title] = 'truf_VeloView.execute()'
 
#  filter = '(match==true)&&(unique==true)'
 filter = ''

 ui().echo(' Visualize Track '+filter+' (blue)')
 ui().echo(' with velo measurements ')
 session().setParameter('Track.location','Rec/Track/Best')

 session().setParameter('modeling.showCurve','true')
 session().setParameter('modeling.useExtrapolator','true')

 Style().setColor('blue')
 data_collect(da(),'Track',filter)
 data_visualize(da())

 Style().setColor('cyan')
 data_collect(da(),'VeloCluster','isR==true')
 data_visualize(da())

 Style().setColor('violet')
 data_collect(da(),'VeloCluster','isR==false')
 data_visualize(da())


