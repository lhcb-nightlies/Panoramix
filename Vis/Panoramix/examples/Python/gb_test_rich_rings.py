import pmx

session = pmx.session()

style = pmx.Style()
da = pmx.DA()

style.setShape("center")
style.setColor("yellow")
da.collect('RichRecRing')
da.visualize()

style.setShape("inside")
style.setColor("blue")
da.collect('RichRecRing')
da.visualize()

style.setShape("outside")
style.setColor("red")
da.collect('RichRecRing')
da.visualize()

