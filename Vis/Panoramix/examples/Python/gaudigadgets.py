import GaudiPython,ROOT
##### some useful things from Jose A. Hernando [jose.hernando@cern.ch]
def leave(datsvc,name = ''):
  """ return the list of leaves (paths) under the name-path
  param@ datsvc, the data service
  param@ name, the initial root path of the leave
  """
  rname = name[:]
  if ((name != '') and (name.find('/') == 0)): rname = name[1:]
  obj = datsvc[rname]
  if (not obj): return []
  leaves = datsvc.leaves(obj)
  # if (len(leaves) == 0): return [name,]
  names = []
  for l in leaves:
    lname = name+l.name()
    names.append(lname)
  return names


def leaves(datsvc,names = None):
  """ return the list of leaves (paths) recrusive under the names-path list
  param@ datsvc, the data service
  param@ the list of root paths to explore
  """
  if (not names): names = leave(datsvc)
  ok = True
  ttnames = names[:]
  while (ok):
    ok = False
    tnames = ttnames[:]
    ttnames = []
    for l in tnames:
      knames = leave(datsvc,l)
      if (len(knames) >0):
        for k in knames:
          if (names.count(k) == 0):
            names.append(k)
            ttnames.append(k)
            ok = True
  return names

def printAlgoList(gaudi,algoList,alreadyListed=[],level=0):
  isOk=True
  for iAlgo in algoList:
    chain=reduce(lambda a,b:a+b,map(lambda a:'     ',range(level)),'')
    if iAlgo in alreadyListed:
      print chain+'**** Algorithm repeated ****',iAlgo
      isOk=False
    else:
      alreadyListed.append(iAlgo)
      print chain+iAlgo
      try:
        algoProp=gaudi.property(iAlgo.split('/')[-1])
        alreadyListed+=printAlgoList(gaudi,algoProp.Members,
                                     alreadyListed,level+1)
      except: pass

      if not isOk: sys.exit()
    return
##### some useful things from Thomas Ruf
import httplib,urllib
import urlparse
import webbrowser
 
def httpExists(url):    
  host, path = urlparse.urlsplit(url)[1:3]
  connection = httplib.HTTPConnection(host)  ## Make HTTPConnection Object
  connection.request("HEAD", path)
  response = connection.getresponse()        ## Grab HTTPResponse Object
  if response.status == 200 : return True
  return False

def doxygen(t) :
# takes as input any class instance of the LHCb software 
  import os,sys
#  if not t.classID : 
#    print 'no class ID defined, do not know what to do.'
#    return
  cl = type(t).__name__
  hcl = 'class'
  for c in cl :
    if c.isupper() : hcl += '_' + c.lower()
    else           : hcl += c 
  hcl += '.html'
 
  packages = ['LBCOM','LHCB','GAUDI','REC','PHYS','ANALYSIS','HLT','ROOT']    
  url = ''
  for package in packages :   
    if package == 'ROOT' :
      newcl = (((((cl.replace(':','_')).replace('>','_')).replace('<','_')).replace(' ','')).replace(',','_')).replace('_ROOT__Math__','_-p1')
      temp  = 'http://root.cern.ch/root/html/'+newcl+'_.html'
      url = temp.replace('__.html','_.html')
    else :
     vers = ''
     target = os.sep + package.upper() + os.sep
     for value in os.environ.values() :
      l =  value.rfind(target)
      if l > 0 : 
        vers = value[l+len(package)+2:].split(os.sep)[0]
        break
     if vers :
      if package == 'GAUDI' :
       hcl = hcl.replace('classi','class')
       url = 'http://proj-gaudi.web.cern.ch/proj-gaudi/releases/'+package.upper()+'/'+vers+'/doc/html/'+hcl
      else :
       pn =  package.lower()
       vn =  vers.split('_')[1]
       urlbase = 'http://lhcb-release-area.web.cern.ch/LHCb-release-area/DOC/'+pn+'/releases/'+vn+'/doxygen/'
       urlclass = urlbase+'classes.html'
       if httpExists(urlclass) : 
         classname = hcl.replace('::','_1_1')  
         xx  = urllib.urlopen(urlclass)
         all = xx.readlines()
         for line in all :
           if line.find(classname)>-1 :  
             p1 = line.find(classname)
             p2 = line[:p1].rfind('href')+6    
             url = urlbase+line[p2:p1]+classname
             if httpExists(url) : break
    if url != '' : 
     if httpExists(url) : break
     url = ''
  if url :
    webbrowser.open(url,new = 1)
    print url
  else :
    print 'no package found'
##### print types of a class
def class_print(t) :
 GaudiPython.loaddict('ReflexRflx')
 Reflex = GaudiPython.gbl.Reflex
 cl = type(t).__name__
 print 'Types of '+cl+':'
 sname = Reflex.Type.ByName(cl)
 for i in range(sname.FunctionMemberSize()) :                                               
   print sname.FunctionMemberAt(i).Name()                                    

##### this one is broken
def print_out(o) :
  std = GaudiPython.gbl.std
  std.stringstream = std.basic_stringstream('char,std::char_traits<char>,std::allocator<char>')
  s = std.stringstream()
  o.fillStream(s)
  print s.str()

def getEnumNames(clname) :
  if type(clname) != str : 
    print 'getEnumNames requires class name as string'
    return {}
   ## Code Using reflex 
  if not ROOT.gROOT.GetVersion()[0]=='6':
   GaudiPython.loaddict('ReflexRflx')
   Reflex = GaudiPython.gbl.Reflex                                                
   enums = {}                                                                       
   GaudiPython.makeClass(clname)  # To make sure that the dictionary is loaded      
   cl = Reflex.Type.ByName(clname)                                                  
   if not cl.IsClass() : return enums                                               
   for i in range(cl.SubTypeSize()) :                                               
     e = cl.SubTypeAt(i)                                                            
     if not e.IsEnum() : continue                                                   
     enums[e.Name()] = {}                                                           
     for j in range(e.DataMemberSize()) :                                           
       m = e.DataMemberAt(j)                                                        
       enums[e.Name()][m.Offset()] = m.Name()
  else:
   # ROOT6 Version
   enums = {}
   from GaudiPython import gbl
   cl = gbl.TClass.GetClass(clname)
   # First iterating on all enums in the class
   for e in cl.GetListOfEnums():
     ename= e.GetName()
     enumvals = {}
     # Now iterating on all values of the enum
     for ec in e.GetConstants():
       enumvals[ec.GetValue()] = ec.GetName()
     enums[ename] = enumvals
   return enums  

def nodes(dsvc, forceload=False, node='') :
 nodelist = []
 if type(node) is str:
  root = dsvc.retrieveObject(node)
  if root : node = root.registry()
  else    : return nodelist
 if node.object() :
   if dsvc.leaves(node).size() > 0 :
#    print node.identifier() , '*'
    for l in dsvc.leaves(node) :
     if forceload : temp = dsvc[l.identifier()]
     nodelist+=nodes(dsvc,forceload,l)
   else :
#     print node.identifier()
     nodelist.append(node.identifier())
 else:
#   print node.identifier()
   nodelist.append(node.identifier())
 return nodelist 

def histo_dump(hsvc, node=None) :
 if not node :  
  root = hsvc.retrieveObject('')
  if root     : node = root.registry()
  else        : return
 if node.object() :
    if hsvc.leaves(node).size() > 0 :
       for l in hsvc.leaves(node) :
             histo_dump(hsvc,l)
    else :
       print node.identifier(),'Title:',hsvc[node.identifier()].title()

#decorate evtsvc:
def  dumpAll( self ) :
    try : 
     nodelist = nodes(self,True,'')
    except :
# for some stupid reason, try again    
     nodelist = nodes(self,True,'')
    for i in nodelist : print i
GaudiPython.Bindings.iDataSvc.dumpAll = dumpAll

# enable automatic unpacking of new DSTs, Jan 2009 
def unpackAll():     
 evt =  GaudiPython.AppMgr().evtsvc()
 packed = nodes(evt,True,'pRec')  
 for a in packed : 
   b = a.replace('Event/pRec','Event/Rec')
   touch = evt[b]
 mcpacked = nodes(evt,True,'pSim')  
 for a in mcpacked : 
   b = a.replace('Event/pSim/MC','Event/MC/')
   touch = evt[b]
   
#-----------------------------------------------------------
# for the new LHCb Particle property service
def ParticlePropertySvc(x) :
    Helper = GaudiPython.Helper 
    appMgr = GaudiPython.AppMgr() 
    svc = Helper.service( appMgr._svcloc, 'LHCb::ParticlePropertySvc' )
    return GaudiPython.InterfaceCast(GaudiPython.gbl.LHCb.IParticlePropertySvc)(svc)
GaudiPython.Bindings.AppMgr.partSvc =  ParticlePropertySvc

