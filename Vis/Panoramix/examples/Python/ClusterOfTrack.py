from panoramixmodule import *


# ZR view : 
Page().setTitle('VELO rz view')
Page().titleVisible(True)
Page().createDisplayRegions(1,1,0)

# Setup region (a page can have multiple drawing region) :
Page().setCurrentRegion(0)
Viewer().setFrame()
Region().setBackgroundColor('black')
session().setParameter('modeling.projection','ZR')
session().setParameter('modeling.modeling','wireFrame')
#
#     Velo has Z in [-200, 800]. 
#     Max radius of the velo is around 60.
#     After ZR projection, Z is going to be on the x axis.
#     Have a global scale in order to bring R at the
#     same scale than Z ( scaleX = 1000/60 = 100/6 = 16.66).
Region().setTransformScale(1., 16.8, 1.)
Camera().setPosition(300., 470., 100.)
Camera().setHeight(1000.)
Camera().setOrientation(0., 1., 0., 0.)
Camera().setNearFar(0.,120.)

ui().setParameter('viewer.autoClipping','false')

session().setParameter('modeling.showCurve','true')
session().setParameter('modeling.useExtrapolator','true')

# velo veloBack veloTT unique  seed match forward isDownstream

Style().setLineWidth(2.) 
Style().setColor('green')
data_collect('Track','velo==true')
data_visualize(da())
Style().setColor('greenyellow')
data_collect('Track','veloBack==true')
data_visualize(da())
Style().setColor('cyan')
data_collect('Track','veloTT==true')
data_visualize(da())
Style().setColor('blue')
data_collect('Track','forward==true')
data_visualize(da())

Style().setColor('white')
data_collect('VeloCluster','isR==true')
data_visualize(da())
Style().setColor('violet')
data_collect('VeloCluster','isR==false')
data_visualize(da())

session.setParameter('modeling.what','clusters')
Style().setColor('red')
data_collect('Track','')
data_visualize(da())

session.setParameter('modeling.lineWidth','0') 

Style()_setColor('gold')
session.setParameter('modeling.what','MCParticle')
data_collect('Track','')
data_visualize(da())

toui()
