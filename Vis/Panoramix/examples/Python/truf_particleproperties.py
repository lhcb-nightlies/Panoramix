from panoramixmodule import *
ParticleID = GaudiPython.gbl.LHCb.ParticleID
# Dialog inputs : 
action   = ui().parameterValue('Panoramix_particleproperties_input_action.value')
id       = int(ui().parameterValue('Panoramix_particleproperties_input_id.value'))
print id

if action == 'findByLHCbID' :
  prop = partsvc.find(ParticleID(id))
elif action ==  'findByName' :
  prop = partsvc.find(id)
else :
  print 'this should not happen'
  prop = partsvc.find(id)

if not prop :
  print 'unknown particle'
else :
  print prop  

