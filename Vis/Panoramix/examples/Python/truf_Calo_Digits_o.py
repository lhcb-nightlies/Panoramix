from panoramixmodule import *

# Camera positioning :
Camera().setPosition(0., 0., 20000.)
Camera().setHeight(12000.)
Camera().setOrientation(0.,0.,1.,0.)
Camera().setNearFar(1.,100000.)

ui().echo(' Visualize Calo Digits')
Style().dontUseVisSvc()
Style().setColor('blue')
uiSvc().visualize('/Event/Raw/Hcal/Digits')
Style().setColor('red')
uiSvc().visualize('/Event/Raw/Ecal/Digits')
Style().setColor('green')
uiSvc().visualize('/Event/Raw/Prs/Digits')
Style().setColor('yellow')
uiSvc().visualize('/Event/Raw/Spd/Digits')
data_visualize(da())
Style().useVisSvc()
