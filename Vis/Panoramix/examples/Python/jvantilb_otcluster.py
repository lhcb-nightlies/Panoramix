
from Panoramix import *

##################################################################
# Draw the OTClusters in the next event                          #
##################################################################

# Go to the next event
eraseEvent()
uiSvc.nextEvent()

# Draw the OTClusters
session.setParameter('modeling.color','red')
data_collect('OTCluster')
data_visualize()
