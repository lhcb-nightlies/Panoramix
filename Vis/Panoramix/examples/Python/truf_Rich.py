from panoramixmodule import *

def createPage(title):
    
    Page().setTitle(title)
    Page().titleVisible(True)
    onlineViews[title] = 'truf_Rich.execute('+title.replace('RICH','')+')'
    Page().createDisplayRegions(1,1,0)
    # Setup region (a page can have multiple drawing region) :
    Page().setCurrentRegion(0)
    Viewer().setFrame()
    Region().setBackgroundColor('black')
    
    if title.find('RICH1') != -1 :
        
        # Camera positioning :
        Camera().setPosition(-15.612442, 23.426102, 1475.8756)
        Camera().setHeight(3227.969)
        Camera().setOrientation(-0.010145176, -0.99955624, -0.028006708,  1.8510739)
        Camera().setNearFar(-1270.5231,1150.1384)

        Style().useVisSvc()
        Style().setSolid()

        # Temp hack for DC06 compat.
        from Configurables import LHCbApp
        if LHCbApp().isPropertySet("DataType") and LHCbApp().DataType == 'DC06':

            # draw the HPD panels
            Style().setColor('cyan')
            Style().setOpened()
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PDPanel0')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PDPanel1')
            Style().setClosed()

            # draw the mirrors
            Style().setColor('yellow')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q0A:1')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q0B:0')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q1A:5')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q1B:4')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q2A:6')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q2B:7')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q3A:2')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1Q3B:3')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q00:0')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q01:1')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q02:2')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q03:3')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q110:10')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q111:11')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q18:8')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q19:9')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q212:12')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q213:13')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q214:14')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q215:15')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q34:4')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q35:5')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q36:6')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2Q37:7')

        else :

            # draw the HPD panels
            Style().setColor('cyan')
            Style().setOpened()
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/HPDPanel0')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/HPDPanel1')
            Style().setClosed()

            # draw the mirrors
            Style().setColor('yellow')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1EnvModuleQ0')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1EnvModuleQ1')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1EnvModuleQ2')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1EnvModuleQ3')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2MasterTop')
            uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2MasterBot')

    elif title.find('RICH2') != -1 :

        # Camera positionning :
        Camera().setPosition(-3344.4697, 9908.5205, 5334.5815)
        Camera().setHeight(11398.284)
        Camera().setOrientation(-0.50448138, -0.77540749, -0.37979186,  2.2956514 )
        Camera().setNearFar(5762.4453, 14284.674)

        Style().useVisSvc()
        Style().setSolid()

        # Temp hack for DC06 compat.
        from Configurables import LHCbApp
        if LHCbApp().isPropertySet("DataType") and LHCbApp().DataType == 'DC06':

            # draw the HPD panels
            Style().setSolid()
            Style().setColor('cyan')
            Style().setOpened()
            uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/RichSystem/HPDPanel0')
            uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/RichSystem/HPDPanel1')
            Style().setClosed()

            Style().useVisSvc()
            # draw the mirrors
            Style().setColor('yellow')
            for i in range(0,40) :
                s1 = '/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2SecMirror:'+str(i)
                uiSvc().visualize(s1)
            for i in range(0,56) :
                s2 = '/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2SphMirror:'+str(i)
                uiSvc().visualize(s2)

        else :
                    
            # draw the HPD panels
            Style().setColor('cyan')
            Style().setOpened()
            uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/HPDPanel0')
            uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/HPDPanel1')
            Style().setClosed()
        
            # draw the mirrors
            Style().setColor('yellow')
            uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2Gas/Rich2SphMirrorContainer_0')
            uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2Gas/Rich2SphMirrorContainer_1')
            uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2Gas/Rich2SecMirrorContainer_0')
            uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2Gas/Rich2SecMirrorContainer_1')
        
    else :
     
        print 'truf_rich: not possible !'

def execute(flag) :
  if flag == 1 : 
    title = 'RICH1'
  if flag == 2 : 
    title = 'RICH2'
  if not ui().findWidget(title) :     
    ui().createComponent(title,'PageViewer','ViewerTabStack')
    ui().setCallback(title,'collect','DLD','OnX viewer_collect @this@')
    ui().setCallback(title,'popup','Python','from Panoramix import sys_import;sys_import("Viewer_popup")')
    ui().setParameter(title+'.popupItems','Current region\nNo highlighted\nTrack_Clusters\nTrack_Measurements\nTrack_MCParticle\nMCParticle_Track\nParticle_MCParticle\nParticle_Daughters\nMCParticle_Clusters\nCluster_MCParticles\nMCParticle_MCHits')
    ui().setCurrentWidget(ui().findWidget(title))   
    createPage(title)  	 
  ui().setCurrentWidget(ui().findWidget(title))   
