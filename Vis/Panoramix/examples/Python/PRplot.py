from panoramixmodule import *
import DetView_module,Detector 
import Onlinemodule,Calo_Viewer,Event_Tree
import os

showclusters = False
blackbackground = False
found = False
knownformats =  ["GL2PS","JPEG","PS","GIF","GL2PDF","GL2SVG","PSVec"]

MCParticle     = gaudimodule.gbl.LHCb.MCParticle
Track          = gaudimodule.gbl.LHCb.Track
Vertex         = gaudimodule.gbl.LHCb.Vertex
MCDecayFinder  = getTool('MCDecayFinder','IMCDecayFinder')
if not MCDecayFinder: MCDecayFinder  = tsvc.create('MCDecayFinder', interface='IMCDecayFinder')
# causes severe crash in MuonNeuron::MuonNeuron(MuonHit const*, MuonHit const*), Jan 20 2017 TR
# libMuonInterfacesLib.so
# muon_cosmicTool = tsvc.create('MuonCombRec',interface='IMuonTrackRec') 
# muon_recTool    = tsvc.create('MuonNNetRec',interface='IMuonTrackRec') 

# RichDigitsLocation = 'Raw/Rich/Digits'
RichDigitsLocation = 'Rec/Rich/RecoEvent/Offline/Pixels'

from ROOT import MakeNullPointer
ot = det['/dd/Structure/LHCb/AfterMagnetRegion/T/OT']
it = det['/dd/Structure/LHCb/AfterMagnetRegion/T/IT']
XYZPoint = GaudiPython.gbl.ROOT.Math.XYZPoint    
begEv  = GaudiPython.gbl.Incident('BeginEvent', 'BeginEvent' )
endEv  = GaudiPython.gbl.Incident('EndEvent', 'EndEvent' )
incSvc = appMgr.service('IncidentSvc', 'IIncidentSvc' ) 
atrack = Track()

dialog = 'SoEvent_Track_dialog'

def defaults():
  if not ui().findWidget(dialog) :  ui().showDialog( '$SOEVENTROOT/scripts/OnX/Track.onx' ,dialog )
  if ui().findWidget(dialog) : 
    ui().findWidget(dialog).hide()
  ui().synchronize()

def newPage(title,nregions):                         
 page = ui().currentPage()                              
 page.deleteRegions()                                   
 Page().titleVisible(False)                              
 Page().createDisplayRegions(1,1,0)                     
 ui().currentWidget().cast_ISoViewer().setHeadlight(1)
 
def wprint(fname,format='JPEG',quality='100',info=True):    
  ui().synchronize()
  if format.lower().find('ps')>-1 : 
    if fname.find('.')==-1: fname = fname+'.ps'        
# remove text, logo, and title 
    soPage = ui().currentPage()
    soPage.setTitleVisible(False) 
    regions2delete = []
    for n in range(soPage.getNumberOfRegions()):
      reg = soPage.getRegion(n)
      typ = reg.getTypeId()
      if typ.getName() == 'SoTextRegion' or typ.getName() == 'SoImageRegion':
        regions2delete.append(reg)
    for reg in regions2delete: soPage.deleteRegion(reg)
  if format == 'wrl' :
    Region().write_wrl()
    os.rename('out.wrl',fname+'.wrl')
  elif format == 'hiv' :
    Region().write_hiv()
    os.rename('out.hiv',fname+'.hiv')
  else : 
   widget = ui().currentWidget()
   if widget :
#   format = "GL2PS"
#   format = "JPEG90"
   # in principle is available too :
#   format = "PS"     # pixmap PS.
#   format = "GIF"    # pixmap GIF
#   format = "JPEG<quality>" # pixmap. <quality> is an int in [0,100]. 
#Exa : JPEG80
#   format = "GL2PDF"
#   format = "GL2SVG"
#   format = "PSVec"
    if format.lower().find('jp')>-1: 
     if fname.find('.')==-1: fname = fname+'.jpg'        
     rc = widget.write(fname,format,quality)
    else: 
     rc = widget.write(fname,format)
    if info : print rc,    fname, ' with format ',format,' and quality',quality

def detView(flag) : 
 Style().setWireFrame()
#  Style().setOpened()
 Style().setClosed()
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1')
 if flag > 0: uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT')
 if blackbackground :  Style().setColor('darkgrey')
 uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')
# uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/PipeInMagnet')
# uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/PipeSupportsInMagnet')
 if flag > 1: uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/IT')
 if flag > 1: uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT')
# uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/PipeInT')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2')
 Style().setColor('orange')
 if flag > 0: uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Prs')
 if flag > 0: uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Spd')
 if flag > 0: uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
 if flag > 0: uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')
# Style().setColor('darkgreen')
# if flag > 0: uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Muon')
 Style().setSolid()
 
def BeamPipe(): 
      Detector.vis_beamPipe()
 
def setColor(r,g,b) : 
  Style().setColor('%1.2f %1.2f %1.2f' % (r/256.,g/256.,b/256.))	

def search():    
 decaystr = '[B_s0]cc'
 found = False
 while not found :
  appMgr.run(1)
  cont     = evt['Rec/Track/Best']
  contmc   = evt['MC/Particles']
  decaylist = []
  MCDecayFinder.setDecay(decaystr)
  if MCDecayFinder.hasDecay(contmc) :
   part = MakeNullPointer(MCParticle)
   while MCDecayFinder.findDecay(contmc,part)>0 :
    decaylist.append(contmc[part.key()])
   for decay in decaylist :
    daughters = gaudimodule.gbl.std.vector('const LHCb::MCParticle*')()    
    MCDecayFinder.descendants(decay,daughters)
    lfrommc = linkedFrom(Track,MCParticle,'Rec/Track/Best')       
##### find the stable particles in the B decay chain
    pList = []
    for mcp in daughters : 
     if mcp.particleID().abspid() == 321 :  pList.append(mcp)     
     if mcp.particleID().abspid() == 211 :  pList.append(mcp)     
     if mcp.particleID().abspid() == 13  :  pList.append(mcp)     
    tList = []
    for mcp in pList : 
     tr = lfrommc.first(mcp)     
     if tr  : 
      if tr.type() == tr.Long or tr.type() == tr.Downstream :  tList.append(mcp)
    if len(tList) == 4 :
       print 'Bingo'
       found = True
       break      
 return found      

def disp_tracks(opt,container='Rec/Track/Best',opt2='',mucolor='magenta'):
# requires -F option, full reconstruction
  if not ui().findWidget(dialog) : defaults()
  chi2  = float(ui().parameterValue('SoEvent_Track_input_xmax.value'))
  session().setParameter('Track.location',container)
  veloclosed = False
  mo = det['/dd/Conditions/Online/Velo/MotionSystem']
  test = mo.paramAsDouble('ResolPosLA')
  if abs(test) < 3 : veloclosed = True
  assoc = ui().parameterValue('SoEvent_Track_input_associations.value')
  rc = session().setParameter('modeling.what',assoc)
  if session().parameterValue('modeling.lineWidth')=='':
    rc = session().setParameter('modeling.lineWidth','2.')  
  rc = session().setParameter('modeling.precision','50')           #??
  rc = session().setParameter('modeling.userTrackRange','false')  
  if opt2=='cosmic': 
   rc = session().setParameter('modeling.userTrackRange','true')
   rc = session().setParameter('modeling.trackEndz','22000.')
  nmuonTracks = 0
  if evt['Rec/Track/Muon']: nmuonTracks = evt['Rec/Track/Muon'].size()
  if opt2=='cosmic': 
    Style().setRGB(0.,1.,0.2)    
    data_collect(da(),'Track','')
    data_visualize(da())
    return
#ignore tracks with no states
  filter_states = 'nstate>0'
#apply minimum pt cut
  filter_pt     = 'pt>'+ui().parameterValue('SoEvent_Track_input_ptmin.value')
  if opt == 1 : filter_pt  = 'pt>200.'
#only show tracks with origin in Velo: data_collect(session,"Track","(upstream==true)"+
  xx = ui().parameterValue('SoEvent_Track_input_type.value').lower()
  if xx=='all':filter_type = 'type>-1'
  else: filter_type = xx+'==true'
  if opt == 2: filter_type = '!(type=='+str(atrack.Downstream)+'||type=='+str(atrack.Ttrack)+'||type=='+str(atrack.Muon)+')' 
#don't project what is not related
  filter_slope = '(ty<0||ty>0)'
  if opt == 1 : filter_slope = 'ty<0'
# display long tracks
  filter_track = 'type=='+str(atrack.Long)
# display good tracks
  filter_chi2  = ''
  if chi2>0: filter_chi2  = '&&chi2dof<'+str(chi2)
# test test
  #setColor(100.,100.,100.)	      
  #data_collect(da(),'Track',filter_track)
  #data_visualize(da())
#
  setColor(0.,0.,255.)	           # blue  
  data_collect(da(),'Track',filter_states+'&&'+filter_pt+'&&'+filter_type+'&&'+filter_slope+'&&'+filter_track+filter_chi2)
  data_visualize(da())
  filter_track = 'type=='+str(atrack.Velo)         
  setColor(176.,224.,230.)	       # Powder Blue
  data_collect(da(),'Track',filter_states+'&&'+filter_pt+'&&'+filter_type+'&&'+filter_slope+'&&'+filter_track+filter_chi2)
  data_visualize(da())
  filter_track = 'type=='+str(atrack.Upstream)     
  setColor(102.,205.,170.)	       # MediumAquamarine  
  data_collect(da(),'Track',filter_states+'&&'+filter_pt+'&&'+filter_type+'&&'+filter_slope+'&&'+filter_track+filter_chi2)
  data_visualize(da())
  filter_track = 'type=='+str(atrack.Downstream)
  setColor(64., 224.,208.)	       # turquoise    
  data_collect(da(),'Track',filter_states+'&&'+filter_pt+'&&'+filter_type+'&&'+filter_slope+'&&'+filter_track+filter_chi2)
  data_visualize(da())
  filter_track = 'type=='+str(atrack.Ttrack)       
  setColor(171., 130., 255.)      # MediumPurple1
  data_collect(da(),'Track',filter_states+'&&'+filter_pt+'&&'+filter_type+'&&'+filter_slope+'&&'+filter_track+filter_chi2)
  data_visualize(da())
#
  if nmuonTracks > 0 : 
    Style().setColor(mucolor)
    temp1 = session().parameterValue('modeling.userTrackRange')
    temp2 = session().parameterValue('modeling.trackEndz')
    temp3 = session().parameterValue('modeling.lineWidth')
    temp4 = session().parameterValue('modeling.trackStartz')
    rc = session().setParameter('modeling.userTrackRange','true')
    rc = session().setParameter('modeling.trackEndz','22000.')
    rc = session().setParameter('modeling.trackStartz','-200.')
    lw = str(float(session().parameterValue('modeling.lineWidth')) * 1.5)     
    rc = session().setParameter('modeling.lineWidth',lw)  
    filter_track = 'type>-1'
    if veloclosed : filter_track = 'type=='+str(atrack.Long)  
    if chi2>0: filter_chi2  = '&&chi2dof<'+str(chi2)
    else: filter_chi2  = ''
    xx = Double(-1)
    for mt in evt['Rec/Track/Muon']:
      theTrack = evt[container].containedObject(mt.key())
      if theTrack.type()!=atrack.Long : continue
      aproto = evt['Rec/ProtoP/Charged'].containedObject(mt.key())
      if mt.info(304,xx)>0: # should be IsMuon but just make sure
       try: 
        if aproto.muonPID().IsMuon():
# require IsMuon
          data_collect(da(),'Track',filter_states+'&&'+filter_pt+'&&'+filter_slope+'&&'+filter_track+filter_chi2+'&&key=='+str(mt.key()) )
          data_visualize(da())
       except: 
          # do nothing
          xx = Double(0)  
    rc = session().setParameter('modeling.userTrackRange',temp1)
    rc = session().setParameter('modeling.trackEndz',temp2)
    rc = session().setParameter('modeling.lineWidth',temp3)
    rc = session().setParameter('modeling.trackStartz',temp4)
#
def disp_velotracks(opt,container='Rec/Track/Velo'):
# requires -F option, full reconstruction
  rc = session().setParameter('modeling.what','Measurements') 
  rc = session().setParameter('modeling.lineWidth','2.')  
  rc = session().setParameter('modeling.precision','10')          
  rc = session().setParameter('modeling.userTrackRange','true')  
  rc = session().setParameter('modeling.trackEndz','20000.')
  rc = session().setParameter('modeling.trackStartz','-2000.')
  setColor(176.,224.,230.) 
  if opt != '' :  Style().setColor(opt)
  for t in evt[container] :    
     rc = Object_visualize(t)  
#
def disp_muonTracks():
  temp1 = session().parameterValue('modeling.userTrackRange')
  temp2 = session().parameterValue('modeling.trackEndz')
  temp3 = session().parameterValue('modeling.trackStartz')
  rc = session().setParameter('modeling.userTrackRange','true')  
  rc = session().setParameter('modeling.trackEndz','25000.')
  rc = session().setParameter('modeling.trackStartz','8000.')
  defMuons = evt['/Event/Rec/Muon/Track']
  if not defMuons:  
    muonTracks = muon_recTool.tracks()
    if muonTracks.size() == 0 : 
      muonTracks = muon_cosmicTool.tracks()
      if muonTracks.size() == 0 : return 
      muon_cosmicTool.copyToLHCbTracks()
    else:  muon_recTool.copyToLHCbTracks()
  setColor(50., 205., 50.)  
  for t in evt['/Event/Rec/Muon/Track']: 
    if defMuons:
      theTrack = evt['Rec/Track/Best'].containedObject(t.key())
      if theTrack.type()!=atrack.Long : continue
      aproto = evt['Rec/ProtoP/Charged'].containedObject(t.key())
      if not aproto.muonPID().IsMuon(): continue
    rc = Object_visualize(t)  
  rc = session().setParameter('modeling.userTrackRange',temp1)  
  rc = session().setParameter('modeling.trackEndz'     ,temp2)
  rc = session().setParameter('modeling.trackStartz'   ,temp3)
       
def makefour():
# make four pictures 
# Camera positionning :
 Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
 Camera().setHeight(7000.)
# full picture position 0. 0. 9000.  height 14000.
# four pictures, height 7000., 0. 0. 9000. 
 Camera().setPosition(2500.,0.,5000.)
 wprint('test1.jpg')
 Camera().setPosition(2500.,0.,13000.)
 wprint('test2.jpg')
 Camera().setPosition(-2500.,0.,5000.)
 wprint('test3.jpg')
 Camera().setPosition(-2500.,0.,13000.)
 wprint('test4.jpg')

def makeone():
# make one ps vec picture
# Camera positionning :
 Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
 Camera().setHeight(14000.)
 Camera().setPosition(0.,0.,9000.)
 wprint('event.ps','PSVec')

def xy():
# Camera positionning :
 Camera().setPosition(0.,0.,9000.)
 Camera().setOrientation(0.,0.,1.,0.)
 Camera().setHeight(86.)
 wprint('xy_close.jpg')
 wprint('xy_close.ps','PSVec')
 Camera().setHeight(4000.)
 wprint('xy_far.jpg')
 wprint('xy_far.ps','PSVec')


def Rich1_view() :
 Camera().setPosition(-3509.51, -534.733, -181.24)
 Camera().setOrientation(0.856425, 0.0300262, -0.515397, 2.98089)
 Camera().setNearFar(-999.467,14403.4)
 Camera().setHeight(3140.68)
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1')     

def Rich2_view() :
 Camera().setPosition( -1432.73, 256.688, 9300.08 )
 Camera().setOrientation(-0.627928, -0.648631, -0.430098, 1.09301)
 Camera().setNearFar( -8287.41, 11627.2 )
#focalDistance 639.79
 Camera().setHeight(9706.73)
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/HPDPanel1')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2Gas')
 
def Velo_view() :
 Camera().setPosition( -767.398, 1630.38, 850.121)
 Camera().setOrientation(-0.733913, -0.604432, -0.309894, 1.36208)
 Camera().setNearFar(-8092.5,6629.75)
#focalDistance 1702.13
 Camera().setHeight(998.966)
 for i in range(0,21):
   modL = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/Module'+'%02d'%(2*i)
   uiSvc().visualize(modL)
   modR = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/Module'+'%02d'%(2*i+1)
   uiSvc().visualize(modR)
   if i < 2:
     modL = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ModulePU'+'%02d'%(2*i)
     uiSvc().visualize(modL)
     modR = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/ModulePU'+'%02d'%(2*i+1)
     uiSvc().visualize(modR)
def Velo_viewL() :
 Camera().setPosition( -767.398, 1630.38, 850.121)
 Camera().setOrientation(-0.733913, -0.604432, -0.309894, 1.36208)
 Camera().setNearFar(-8092.5,6629.75)
#focalDistance 1702.13
 Camera().setHeight(998.966)
 for i in range(0,21):
   modL = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/Module'+'%02d'%(2*i)
   uiSvc().visualize(modL)
   if i < 2:
     modL = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/ModulePU'+'%02d'%(2*i)
     uiSvc().visualize(modL)
def Velo_viewR() :
 Camera().setPosition( -767.398, 1630.38, 850.121)
 Camera().setOrientation(-0.733913, -0.604432, -0.309894, 1.36208)
 Camera().setNearFar(-8092.5,6629.75)
#focalDistance 1702.13
 Camera().setHeight(998.966)
 for i in range(0,21):
   modR = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/Module'+'%02d'%(2*i+1)
   uiSvc().visualize(modR)
   if i < 2:
     modR = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/ModulePU'+'%02d'%(2*i+1)
     uiSvc().visualize(modR)

#
def TT_view() :
 Camera().setPosition( -810.205, 715.685, -74.1097)
 Camera().setOrientation(-0.0150305, -0.989981, -0.140397, 2.76137)
 Camera().setNearFar(-53.4584,3578.69)
#focalDistance 3269.51
 Camera().setHeight(1722.98)
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTa')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTb')

def IT_view(): 
 Camera().setPosition( -2864.73, 4185.47, 5644.47) 
 Camera().setOrientation(-0.148356, -0.89674, -0.41695, 2.42754)
 Camera().setNearFar(-3809.38,13677.3)
#focalDistance 3269.51
 Camera().setHeight(3035.11)
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/IT')

def OT_view():
 Camera().setPosition( -430., -17., 8600.)
 Camera().setOrientation(0.0780362, 0.987969, 0.133522, 4.24575)
 Camera().setNearFar(-7800.,7800.)
#focalDistance 685.157
 Camera().setHeight(5427.8)
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT')

def Calo_view():
 Camera().setPosition( 131.174, -326.557, 12129.2)
 Camera().setOrientation(0.624658, 0.766207, 0.150766, 5.59559)
 Camera().setNearFar(-10738.9,13901.8)
#focalDistance 685.157
 Camera().setHeight(5427.8)
 session().setParameter('modeling.modeling','wireFrame')  
 Calo()
 session().setParameter('modeling.modeling','solid')
 
def Muon_view():
 Camera().setPosition( -110.885, 322.793, 11959.7)
 Camera().setOrientation(0.14057, 0.969092, 0.202731, 4.25183)
 Camera().setNearFar(-11743.9,10520.3)
#focalDistance 685.157
 Camera().setHeight(12760.6)
 DetView_module.Muon()
  
def Magnet_view(flag=''):
      uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')
      Detector.vis_beamPipe()

#position -2042.03 526.773 14163.1
#orientation -0.311088 -0.950129 0.0219616 0.290018
#aspectRatio 1
#nearDistance -6854.47
#farDistance 16465.1
#focalDistance 10
#height 6663.82

def disp_CaloMuon(Et):
  rc = session().setParameter('modeling.what','no')
# muon hits
  visMuonHits(tae='')
# calo hits
  ecolor        = session().parameterValue('Panoramix_Calo_input_ecolor')
  if ecolor == '' : Calo_Viewer.defaults()
  ecolor        = session().parameterValue('Panoramix_Calo_input_ecolor')
  hcolor        = session().parameterValue('Panoramix_Calo_input_hcolor')
  scolor        = session().parameterValue('Panoramix_Calo_input_scolor')
  pcolor        = session().parameterValue('Panoramix_Calo_input_pcolor')
  ecalenergy    = session().parameterValue('Panoramix_Calo_input_ecalenergy')
  hcalenergy    = session().parameterValue('Panoramix_Calo_input_hcalenergy')
  spdcalenergy  = session().parameterValue('Panoramix_Calo_input_spdcalenergy')
  prscalenergy  = session().parameterValue('Panoramix_Calo_input_prscalenergy')
  scale         = session().parameterValue('Panoramix_Calo_input_scale')
  eoret         = session().parameterValue('Panoramix_Calo_input_eoret')
  Calo_Viewer.execute(' ',ecalenergy,hcalenergy,spdcalenergy,prscalenergy,scale,eoret,ecolor,hcolor,scolor,pcolor,'visualize') 

             
def findNiceEvent(): 
 print 'is the display zoomed for fullscreen to get best quality ?'
 found = search()    
 if found :
  ui().echo('Visualize nice Event')
  
  detView(0) 
  disp_tracks(1)  
  disp_CaloMuon(True)
#      
  if showclusters :       
   collect_scene_graphs(da(),'highlight==false')
   data_filter(da(),'name','Track*')
   session().setParameter('modeling.what','clusters')
   setColor(240., 230., 140.)	#  khaki   
   data_visualize(da())  

#  makefour()
  makeone()
  Region().eraseEvent()
  disp_tracks(0) 
  xy()

def Calo():
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
 Style().setColor('lightblue')
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Prs')
 Style().setColor('orange')
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Spd')
 Style().setColor('violet')
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')
 
def Rich(flag): 
 if flag == 1 :
  session().setParameter('modeling.modeling','solid')
  uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1EnvModuleQ0')
  uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1EnvModuleQ1')
  uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1EnvModuleQ2')
  uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1EnvModuleQ3')
  uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/HPDPanel0')
  uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/HPDPanel1')
  uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Aerogel')
 if flag == 2 :
  session().setParameter('modeling.modeling','solid')
  uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2') 

import Onlinemodule
def Muon():
 mscolors = {1:'mediumseagreen',2:'limegreen',3:'seagreen',4:'darkgreen',5:'darkolivegreen'}
 for i in range(1,6) :
   Style().setColor(mscolors[i])
   Onlinemodule.muonStationContours(i-1)  

def det3d():
 uiSvc().visualize('/dd/Structure/Infrastructure')  
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1')     
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT')
 Magnet_view()
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2') 
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/IT')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT')
 Calo()
 Muon()

def det3d_open(flag=''):
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1')     
# Velo without tank
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft');
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTa/TTaXLayer')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTb/TTbXLayer')
 if flag.find('noMagnet')==-1 :  Magnet_view()
# Rich2 without heavy support structure  
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/HPDPanel1')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2Gas')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2BeamPipe')
 Style().dontUseVisSvc() 
 session().setParameter('modeling.modeling','solid')
 Style().setColor('darkblue')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T1/X1/Q0')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T1/X1/Q1')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T2/X1/Q0')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T2/X1/Q1')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T3/X1/Q0')
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T3/X1/Q1')
 Style().useVisSvc()
 session().setParameter('modeling.modeling','wireFrame')
 # does not work well with postscript
 # session().setParameter('modeling.transparency','0.3')
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
 Style().setColor('lightblue')
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Prs')
 Style().setColor('orange')
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Spd')
 Style().setColor('violet')
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')
 session().setParameter('modeling.modeling','solid')
 #session().setParameter('modeling.transparency','1.0')
 Muon()


def Tstation(slot='',opt=0):
# OT layers
   if opt == 0 : 
    setColor(100,100,100)       
    uiSvc().visualize(slot+'Raw/OT/Times')
   else : 
    for o in evt[slot+'Raw/OT/Times'] : 
      channelx = o.channel()
      modulex  = ot.findModule(channelx)
      name = modulex.name()  
      if name.find('X1layer')>-1:
       Style().setColor('red')
      if name.find('X2layer')>-1:
       Style().setColor('magenta')
      if name.find('Ulayer')>-1 or name.find('Vlayer')>-1 :
       Style().setColor('magenta')
      Object_visualize(o)
      if name.find('X1layer')==-1 and name.find('X2layer')==-1 : continue 
      if name.find('X1layer')>-1 : Style().setColor('red')     
      if name.find('X2layer')>-1 : Style().setColor('magenta')     
      lhcbidx  = GaudiPython.gbl.LHCb.LHCbID(channelx)
      trajx    = ot.trajectory(lhcbidx,0.) 
      x =   (trajx.beginPoint().x() + trajx.endPoint().x() )/2.             
      y =   (trajx.beginPoint().y() + trajx.endPoint().y() )/2.             
      z =   (trajx.beginPoint().z() + trajx.endPoint().z() )/2.       
      aPoint = XYZPoint(x,y,z)   
      sc = session().setParameter('modeling.markerStyle','cross')  #plus, cross, star
      sc = session().setParameter('modeling.markerSize','9')  # 5,7,9
      sc = uiSvc().visualize(aPoint)  
# IT layers
    for ithit in evt['Raw/IT/Clusters'] : 
      channel = ithit.channelID()
      box     = it.findBox(channel)
      layer   = box.findLayer(channel)
      lhcbid  = GaudiPython.gbl.LHCb.LHCbID(channel)
      traj    = it.trajectory(lhcbid,ithit.interStripFraction())                        
      if layer==1 or layer==4:
       Style().setColor('orange')
      if layer==2 or layer==3:
       Style().setColor('yellow')
      Object_visualize(o)
      if layer.sinAngle() != 0 : 
        Style().setColor('yellow')     
      else : 
        Style().setColor('orange')     
      x =   (traj.beginPoint().x() + traj.endPoint().x() )/2.             
      y =   (traj.beginPoint().y() + traj.endPoint().y() )/2.             
      z =   (traj.beginPoint().z() + traj.endPoint().z() )/2.       
      aPoint = XYZPoint(x,y,z)   
      sc = session().setParameter('modeling.markerStyle','cross')  #plus, cross, star
      sc = session().setParameter('modeling.markerSize','9')  # 5,7,9
      sc = uiSvc().visualize(aPoint) 

def subdetector_pictures(prefix,fm):
  ui().executeScript("DLD","OnX viewer_clear @current@ staticScene")
  Magnet_view()
  ui().synchronize()  
  wprint(prefix+'Magnet_view',fm)
  ui().executeScript("DLD","OnX viewer_clear @current@ staticScene")
  disp_CaloMuon(False) 
  Calo_view()
  BeamPipe()
  ui().synchronize()
  wprint(prefix+'Calo_view',fm)
  ui().executeScript("DLD","OnX viewer_clear @current@ staticScene")
  Muon_view()
  Magnet_view()
  ui().synchronize()
  wprint(prefix+'Muon_view',fm)
  ui().executeScript("DLD","OnX viewer_clear @current@ staticScene")
  OT_view()
  BeamPipe()
  ui().synchronize()
  wprint(prefix+'OT_view',fm)
  ui().executeScript("DLD","OnX viewer_clear @current@ staticScene")
  IT_view()
  BeamPipe()
  ui().synchronize()
  wprint(prefix+'IT_view',fm)
  ui().executeScript("DLD","OnX viewer_clear @current@ staticScene")
  Velo_viewR()
  ui().synchronize()
  wprint(prefix+'Velo_view',fm)
  ui().executeScript("DLD","OnX viewer_clear @current@ staticScene")
  TT_view()
  Style().setColor('magenta')        
  uiSvc().visualize('/Event/Raw/TT/Clusters')   
  ui().synchronize()
  wprint(prefix+'TT_view',fm)
  ui().executeScript("DLD","OnX viewer_clear @current@ staticScene")
  Style().setColor('orange')        
  uiSvc().visualize(RichDigitsLocation)     
  RichHits()          
  Rich1_view()
  BeamPipe()
  ui().synchronize()
  wprint(prefix+'Rich1_view',fm)
  ui().executeScript("DLD","OnX viewer_clear @current@ staticScene")
  Rich2_view()
  BeamPipe()
  ui().synchronize()
  wprint(prefix+'Rich2_view',fm)

def wrl_files():
  Region().clear()
  uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')
  BeamPipe()
  wprint('Magnet_view','wrl')
  wprint('Magnet_view','hiv')
  Region().clear()
  DetView_module.Muon()
  wprint('Muon_view','wrl')
  wprint('Muon_view','hiv')
  Region().clear()
  Calo_view()
  wprint('Calo_view','wrl')
  wprint('Calo_view','hiv')
  Region().clear()
  uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT') 
  wprint('OT_view','wrl')
  wprint('OT_view','hiv')
  Region().clear()
  Rich(1)     
  wprint('Rich1_view','wrl')
  wprint('Rich1_view','hiv')
  Region().clear()
  Rich(2)     
  wprint('Rich2_view','wrl')
  wprint('Rich2_view','hiv')
  Region().clear()
  uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/IT')
  wprint('IT_view','wrl')
  wprint('IT_view','hiv')
  Region().clear()
  Velo_viewR()
  wprint('Velo_viewR','wrl')
  wprint('Velo_viewR','hiv')
  Region().clear()
  Velo_viewL()
  wprint('Velo_viewL','wrl')
  wprint('Velo_viewL','hiv')
  Region().clear()
  uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VacTank')
  wprint('Velo_tank','wrl')
  wprint('Velo_tank','hiv')
  Region().clear()
  uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTa')
  uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTb')
  wprint('TT_view','wrl')
  wprint('TT_view','hiv')
  Region().clear()
  uiSvc().visualize('/dd/Structure/Infrastructure')  
  wprint('Infrastructure_view','wrl')
  wprint('Infrastructure_view','hiv')   
  Region().clear()
  session().setParameter('modeling.modeling','solid')
  uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
  uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')
  wprint('EHCal','wrl')
  wprint('EHCal','hiv')
def special():
 Camera().setPosition(0., 2000., 9000.)
 Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
 Camera().setNearFar(-2672.15,6681.7)
 Camera().setHeight(13000.)

def Velo_clusters(flag=''):
  if flag == 'dark' : 
   Style().setRGB(0.29, 0.19, 0.27)
   data_collect(da(),'VeloCluster','isR==false')
   data_visualize(da())
   Style().setRGB(0.19, 0.19, 0.19)
   data_collect(da(),'VeloCluster','isR==true')
   data_visualize(da())   
  else:
   Style().setColor('violet')
   data_collect(da(),'VeloCluster','isR==false')
   data_visualize(da())
   Style().setColor('white')
   data_collect(da(),'VeloCluster','isR==true')
   data_visualize(da())

def Velo_halo(): 
  rc = session().setParameter('modeling.what','Measurements') 
  if session().parameterValue('modeling.lineWidth')=='':
        rc = session().setParameter('modeling.lineWidth','2.')  
  rc = session().setParameter('modeling.precision','10') 
  rc = session().setParameter('modeling.userTrackRange','true')
  rc = session().setParameter('modeling.trackStartz','-1000.')
  rc = session().setParameter('modeling.trackEndz','20000.')
  Style().setRGB(0.,1.,0.2) 
  session().setParameter('modeling.projection','-ZR')     
  for t in evt['Rec/Track/Velo'] :    
    if t.nDoF() < 6 : continue  
    rc = Object_visualize(t)  
  Velo_clusters()
  session().setParameter('modeling.projection','')

import Rich_RecoPixels_Imp,Rich_RecoPhotons_Imp,Rich_TracklessRings_Imp
def RichHits():
  Rich_RecoPixels_Imp.Visualize_Rich_RecoPixels(curRegion=True)
  Rich_RecoPhotons_Imp.Visualize_Rich_RecoPhotons(curRegion=True)
  Rich_TracklessRings_Imp.Visualize_Rich_TracklessRings_All(curRegion=True)

def searchForward():
 while 1>0:
   appMgr.run(1)
   if not evt['Rec/Track/Forward'] : break
   if evt['Rec/Track/Forward'].size()>0 :
    print  evt['Rec/Track/Forward'].size()
    break

def cern_press(fm='JPEG',prefix='cern'):
  if fm not in knownformats:
    print 'PRPlot: unknown format, exit'
    print 'Known formats = ',knownformats
    return
# use vol1/Bsjpsiphi_00001620_00000004_5.dst 
  ui().executeScript("DLD","OnX viewer_clear @current@ staticScene")
  #search()
  Region().eraseEvent()
# show only tracks in Velo
  disp_tracks(2) 
# Camera positionning :
  Camera().setPosition(0.,0.,9000.)
  Camera().setOrientation(0.,0.,1.,0.)
  Camera().setHeight(86.)  
  wprint(prefix+'_xy_close',format = fm)
# switch on rulers
  ui().executeScript("DLD","Panoramix layout_rulers")
  wprint(prefix+'_xy_close_ruler',format = fm)
  Camera().setHeight(4000.)
  Region().eraseEvent()
  disp_tracks(0)  
  wprint(prefix+'_xy_far_ruler',format = fm)
  ui().executeScript("DLD","Panoramix layout_removeRulers")
  disp_tracks(0)  
  wprint(prefix+'_xy_far.jpg',format = fm)

  # display towers proportional to energy    
  disp_CaloMuon(False) 
  RichHits()                                   
  Camera().setPosition(290.929, 333.815, 10678.8)
  Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
  Camera().setNearFar(-4691.25,5368.8)
  Camera().setHeight(12420.9)
  wprint(prefix+'_event',format = fm)                                        
# series of subdetector pictures with event info
  Region().eraseEvent()
  disp_tracks(0)  
  subdetector_pictures(prefix,fm)     
  disp_CaloMuon(False) 
  RichHits() 
  Style().setColor('magenta')        
  uiSvc().visualize('/Event/Raw/TT/Clusters')   
  Style().setColor('green')        
  temp = session().parameterValue('modeling.lineWidth')
  rc = session().setParameter('modeling.lineWidth','1')  
  uiSvc().visualize('/Event/Raw/OT/Times')   
  rc = session().setParameter('modeling.lineWidth',temp)
  Style().setColor('orange')        
  uiSvc().visualize(RichDigitsLocation)     
  Camera().setPosition(1660., 7250., 9570.)
  Camera().setHeight(16000.)
  Camera().setOrientation(-0.513, 0.688, 0.513, 1.936)
  det3d_open()  
  ui().synchronize()  
  wprint(prefix+'_eventWithDetector_1',format = fm)
  ui().executeScript("DLD","OnX region_setParameter viewportRegion.backgroundColor 1 1 1")
  ui().synchronize()  
  wprint(prefix+'_eventWithDetectorWhite_1',format = fm)
  ui().executeScript("DLD","OnX region_setParameter viewportRegion.backgroundColor 0 0 0")
  Camera().setPosition(-1555.03, 2241.9, 9781.9)
  Camera().setHeight(13681.5)
  Camera().setOrientation(0.143256, 0.933725, 0.328079, 4.27028)
  Camera().setNearFar(-8091.76,13494.1)
  ui().synchronize()   
  wprint(prefix+'_eventWithDetector_2',format = fm)
  ui().executeScript("DLD","OnX region_setParameter viewportRegion.backgroundColor 1 1 1")
  ui().synchronize()  
  wprint('eventWithDetectorWhite_2',format = fm)
# before magnet
  Camera().setPosition(1455.78, 1199.53, 1123.6)                    
  Camera().setHeight(3462.22)                                       
  Camera().setOrientation(-0.0874861, 0.925369, 0.368834, 2.47157)    
  Camera().setNearFar(-7469.34,19407.5)                             
  wprint(prefix+'_eventBeforeMagnetWhite',format = fm)
# after magnet, remove upper coil by hand
  Camera().setPosition(-777.922, 1580.25, 8621)                    
  Camera().setHeight(5537.53)                                       
  Camera().setOrientation(0.11072, 0.882962, 0.456201, 3.56987)    
  Camera().setNearFar(-9174.24,14876.8)                             
  wprint(prefix+'_eventAfterMagnetWhite',format = fm)
  ui().executeScript("DLD","OnX region_setParameter viewportRegion.backgroundColor 0 0 0")
#
def barcelona_press():
#------- LHC injection test
#'PFN:castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30764/030764_0000076755.raw'
#'PFN:castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30767/030767_0000076770.raw'
#          event 1119 or 3168
# with IT
#'PFN:castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30929/030929_0000077696.raw' 
#'PFN:castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30930/030930_0000077697.raw' 
#'PFN:castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30932/030932_0000077701.raw' 
#'PFN:castor:/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30933/030933_0000077704.raw' 

  session().setParameter('modeling.modeling','wireFrame')
  uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Spd')
  session().setParameter('modeling.modeling','solid')
  #session().parameterValue('modeling.color')
  Style().setRGB(0.9, 0.51, 0)
  uiSvc().visualize('/Event/Raw/Prs/Digits')
  uiSvc().visualize('/Event/Next1/Raw/Prs/Digits')  
  Style().setColor('yellow')
  uiSvc().visualize('/Event/Raw/Spd/Digits')
  uiSvc().visualize('/Event/Next1/Raw/Spd/Digits')
  uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')
  BeamPipe()
  Camera().setPosition(3569.48, 3882.31, 5392.28)
  Camera().setHeight(13681.5)
  Camera().setOrientation(0.105847, -0.942633, -0.316607, 3.85903)
  Camera().setNearFar(-6227.82,18469.5)    
  Page().SetTitle('SPD and PRS injection Event')
  ui().synchronize()  
  wprint('injectionEventSpd.jpg')
  wprint('injectionEventSpd.ps','PSVec')

def velo_press():
#------- LHC injection test
# /castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30933/030933_0000077704.raw
# runNumber :   30933
# orbitNumber :   502403
# eventNumber :   8
# orbitNumber :   3741103
# eventNumber :   173

  while 1>0:
   appMgr.run(1) 
   if evt['Rec/Track/Velo'].size()>10: break 
# all clusters       
  Velo_clusters('dark')
# only associated clusters 
  disp_velotracks('')
  Style().setColor('green')    
  session().setParameter("Track.location",'Rec/Track/Velo')
  session().setParameter("modeling.what","clusters")
  data_collect(da(),'Track','')
  data_visualize(da())
  session().setParameter("modeling.what","no")
  uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft')
  Camera().setPosition(350.709, 858.961, 753.814)
  Camera().setHeight(410.126)
  Camera().setOrientation(0.725137, 0.21828, 0.653093, 4.54186)
  Camera().setNearFar(-6655.25, 3041.07)    
  Page().setTitle('Velo injection Event')
  ui().synchronize()  
  wprint('injectionEventVelo_near.jpg')
  wprint('injectionEventVelo_near.ps','PSVec')
  BeamPipe()    
  Camera().setPosition(-74.5703, 449.579, 556.028)
  Camera().setHeight(939.035)
  Camera().setOrientation(0.611425, 0.558558, 0.56051, 4.95116)
  Camera().setNearFar(-6655.25,3041.07)    
  Page().setTitle('Velo injection Event')
  ui().synchronize()  
  wprint('injectionEventVelo.jpg')
  wprint('injectionEventVelo.ps','PSVec')


def ecalhcalCosmic():
 Style().setRGB(0.85,0.73,0.0)    
 uiSvc().visualize('/Event/Calo/Track/Backward') 
 uiSvc().visualize('/Event/Calo/Track/Forward') 
 Style().setColor('blue')
 data_collect(da(),'HcalDigits','')
 data_visualize(da())
 Style().setColor('red')
 data_collect(da(),'EcalDigits','')
 data_visualize(da())


def ot_cosmics():
#------- some cosmic events
# only few modules 
#'/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30639/030639_0000076290.raw
# runNumber :   30639
#orbitNumber :   2941477
#eventNumber :   12548
  while 1>0: 
   appMgr.run(1)
   if evt['DAQ/ODIN'].eventNumber()==12548 : break
  Camera().setPosition(-2341.97, 3030.36, 9198.67)
  Camera().setHeight(9518.01)
  Camera().setOrientation(0.177565, 0.790985, 0.585503, 3.44405)
  Camera().setNearFar(-5250.35,11289.7)    
  BeamPipe()    
  uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')
  session().setParameter('modeling.modeling','wireFrame') 
  uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
  uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')
  session().setParameter('modeling.modeling','solid')
  disp_tracks(0,'Rec/Track/Seed','cosmic')
  ecalhcalCosmic()
  Style().setRGB(0.61,1.,0.63)    
  uiSvc().visualize('/Event/Raw/OT/Times') 
  ui().synchronize()                               
  wprint('cosmicEventOT.jpg')          
  wprint('cosmicEventOT.ps','PSVec')   

def EcalHcal_cosmics():                                                                            
#------- some cosmic events                                                                  
#'/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/PHYSICS_COSMICS/30639/030639_0000076290.raw   
# runNumber :   30639                                                                        
#eventNumber :   163, 1148 | nice hcal     
# 668, 1048, 1623, 1843, 2703 both   
                                                              
  while 1>0:                                                                                 
   appMgr.run(1)                                                                             
   if evt['DAQ/ODIN'].eventNumber()==163 : break                                           
  Camera().setPosition(728.939, 321.004, 11709.3)                                           
  Camera().setHeight(9034.1)                                                                
  Camera().setOrientation(-0.471654, 0.878708, 0.0735902, 0.60116)                             
  Camera().setNearFar(-10164.8, 16360.1)                                                      
  BeamPipe()                                                                                 
  uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')                                
  session().setParameter('modeling.modeling','wireFrame')                                    
  uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')                              
  uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')                              
  session().setParameter('modeling.modeling','solid')                                        
  ecalhcalCosmic()                                                                           
  ui().synchronize()   
  wprint('cosmicEventHcal.jpg')                                                                
  wprint('cosmicEventHcal.ps','PSVec')                                                         
  # erase event
  while 1>0:                                                                                 
   appMgr.run(1)                                                                             
   if evt['DAQ/ODIN'].eventNumber()==2703 : break     
 # newPage(title,nregions)                                       
  ecalhcalCosmic()                                                                           
  ui().synchronize()   
  wprint('cosmicEventEcalHcal.jpg')                                                                
  wprint('cosmicEventEcalHcal.ps','PSVec')                                                         

def visOTHitsX():
   Style().setRGB(0.2, 0.8, 0.9)
   data_collect(da(),'OTTime','layer==0')
   data_visualize(da())
   data_collect(da(),'OTTime','layer==3')
   data_visualize(da())
def visMuonHits(tae=''):
  Style().dontUseVisSvc()
  for i in range(1,5):    
   g = 1.-0.5*i/6.
   b = 0.5*i/6. 
   Style().setRGB(0.,g,b)
   if tae == '' : 
    data_collect(da(),'MuonCoord','station=='+str(i))
    data_visualize(da())
  if tae != '' : 
    uiSvc().visualize(tae+'Raw/Muon/Coords')  
  Style().useVisSvc()

def spdprsCosmic():
  Style().setRGB(0.9, 0.51, 0)
  uiSvc().visualize('/Event/Raw/Prs/Digits')
  Style().setColor('yellow')
  uiSvc().visualize('/Event/Raw/Spd/Digits')

def visSTclusters(tae=''):
 temp = session().parameterValue('modeling.lineWidth')
 session().setParameter('modeling.lineWidth','2.')  
 for t in  evt[tae+'Raw/TT/Clusters']:
   if t.firstChannel().station() == 1 : 
     Style().setColor('green')
   else : 
     Style().setColor('red')
   Object_visualize(t)
 Style().setColor('orange')
 uiSvc().visualize(tae+'Raw/IT/Clusters')
 session().setParameter('modeling.lineWidth',temp)  
def Muon_cosmics():                                                                            
#------- some cosmic events                                                                  
#'/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/COSMICS/31874/031874_0000080442.raw'  
#{ runNumber :   31874
#eventNumber :   87861
  while 1>0:                                                                                 
   appMgr.run(1)
   print evt['DAQ/ODIN'].eventNumber()                                                                            
   if evt['DAQ/ODIN'].eventNumber()==87861 : break                                           
  BeamPipe()                                                                                 
  uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')                                
  session().setParameter('modeling.modeling','wireFrame')                                    
  uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')                              
  uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')                              
  session().setParameter('modeling.modeling','solid')                                        
  ecalhcalCosmic()    
  spdprsCosmic()   
  rc = session().setParameter('modeling.lineWidth','3.')  
  rc = session().setParameter('modeling.userTrackRange','true')
  rc = session().setParameter('modeling.trackEndz','22000.')
  Style().setRGB(0., 0.99, 0.)
  uiSvc().visualize('/Event/Rec/Muon/Track')
  Camera().setPosition(791.658, -1444.41, 13050.6)                                           
  Camera().setHeight(10565)                                                                
  Camera().setOrientation(-0.423346, -0.879117, -0.218936, 0.994556)                             
  Camera().setNearFar(-14480.6,14179.8)                                                      
  ui().synchronize()   
  wprint('cosmicEventMuon_1.jpg')                                                                
  wprint('cosmicEventMuon_1.ps','PSVec')                                                         

#import AfterMagnetDownstream_Viewer
# still needed  ? import Minuit_trackfit
def All_cosmics():                                                                            
#------- some cosmic events                                                                  
#'/castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/COSMICS/31874/031874_0000080442.raw'  
#{ runNumber :   31874
#eventNumber :   88115
  Style().useVisSvc() 
  while 1>0:                                                                                 
   appMgr.run(1)                                                                             
   print evt['DAQ/ODIN'].eventNumber()                                                                            
   if evt['DAQ/ODIN'].eventNumber()==88115 : break       
  # if evt['DAQ/ODIN'].eventNumber()==88104 : break  
  #Minuit_trackfit.searchOrPlot(True)                                             
  BeamPipe()                                                                                 
  uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')                                
  session().setParameter('modeling.modeling','wireFrame')                                    
  uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')                              
  uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')                              
  session().setParameter('modeling.modeling','solid')                                        
  AfterMagnetDownstream_Viewer.execute()
  rc = session().setParameter('modeling.lineWidth','3.')  
  rc = session().setParameter('modeling.userTrackRange','true')
  rc = session().setParameter('modeling.trackEndz','22000.')
  Style().setRGB(0., 0.99, 0.)
  Camera().setPosition(-129.716, 2000.07, 11480.3)                                           
  Camera().setHeight(11160.2)                                                                
  Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)                             
  Camera().setNearFar(-3026.,7036.64)      
  visMuonHits()                                                
  disp_tracks(0,'Rec/Track/Seed','cosmic')  
  ui().synchronize()   
  wprint('cosmicEvent_top.jpg')                                                                
  wprint('cosmicEvent_top.ps','PSVec')                                                         
  Camera().setPosition(630.839, 450.521, 13030.8)                                           
  Camera().setHeight(11160.2)                                                                
  Camera().setOrientation(-0.340896, 0.898463, 0.276683, 1.4559)                             
  Camera().setNearFar(-8568.51,11346.2)      
  ui().synchronize()   
  wprint('cosmicEvent_3d.jpg')                                                                
  wprint('cosmicEvent_3d.ps','PSVec')                                                         
  wprint('cosmicEvent_3d.hiv','hiv')                                                         
  while 1>0:
   Minuit_trackfit.searchOrPlot(True)
   if evt['Rec/Track/Seed'].size()>0 : break
  rc = session().setParameter('modeling.lineWidth','4.')    
  rc = session().setParameter('modeling.userTrackRange','true')   
  rc = session().setParameter('modeling.trackEndz','22000.')      
  uiSvc().visualize('/Event/Rec/Track/Seed')       
  ui().synchronize()   
  wprint('cosmicEvent2_3d.hiv','hiv')         
  wprint('cosmicEvent2_3d.jpg')         
  wprint('cosmicEvent2_3d.ps','PSVec')  
# special zoom
  #wprint('cosmicTrack_spd.jpg')         
  #wprint('cosmicTrack_spd.ps','PSVec')  
 
import os
def gzipPS():
 for f in os.listdir('.') : 
   if f.find('.ps')>-1 : 
     os.system('gzip '+f)
           
# TED run 6/7 September 2008
# /castor/cern.ch/grid/lhcb/data/2008/RAW/LHCb/BEAM/32586/032586_0000081828.raw
# Muon coords
def TED_muons():
  newPage('injection event muon hits',1) 
  Camera().setPosition( -110.885, 322.793, 11959.7)
  Camera().setOrientation(0.14057, 0.969092, 0.202731, 4.25183)
  Camera().setNearFar(-11743.9,10520.3)
  Camera().setHeight(12760.6)
#focalDistance 685.157
  visMuonHits()  
  Style().useVisSvc()                                                                            
  BeamPipe()                                                                                 
  uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')                                
  ui().synchronize()   
  wprint('injectionEventMuon_3d.hiv','hiv')         
  wprint('injectionEventMuon_3d.jpg')         
  wprint('injectionEventMuon_3d.ps','PSVec')  
# with infrastructure      
  uiSvc().visualize('/dd/Structure/Infrastructure')
  uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/AfterMuon')
  uiSvc().visualize('/dd/Structure/LHCb/UpstreamRegion')  
  Camera().setPosition( 110.667, 2643.73, 11996.4)
  Camera().setOrientation(0.0185975, 0.997805, 0.0635499, 4.47517)
  Camera().setNearFar(-26799.4,62804.7)
  Camera().setHeight(26037.)
  Style().setColor('blue')
  data_collect(da(),'HcalDigits','')
  data_visualize(da())
  Style().setColor('yellow')
  uiSvc().visualize('/Event/Raw/Spd/Digits')
  Style().setColor('orange')
  uiSvc().visualize('/Event/Raw/Prs/Digits')
  Style().setColor('red')
  uiSvc().visualize('/Event/Raw/IT/Clusters')
  ui().synchronize()
  wprint('injectionEventDownstream_infra.hiv','hiv')         
  wprint('injectionEventDownstream_infra.jpg')         
  wprint('injectionEventDownstream_infra.ps','PSVec')  

def SiliconDet():
  visSTclusters()
  Velo_clusters()
  Style().useVisSvc()                                                                            
  BeamPipe()                                                                                 
  uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')                                
  Camera().setPosition(4180.61, 1336.73, 21489.7)                  
  Camera().setOrientation(-0.259899, 0.96422, 0.0522912, 0.278883)  
  Camera().setNearFar(836.71,26290.2)                             
  Camera().setHeight(5153.82)                                        
  wprint('injectionEventSilicon_3d.hiv','hiv')         
  wprint('injectionEventSilicon_3d.jpg')         
  wprint('injectionEventSilicon_3d.ps','PSVec')  

def myLoop():
  while 1 > 0 :
   appMgr.run(1)
   print evt['Raw/TT/Clusters'].size()
   if evt['Rec/Track/VeloTT'].size() > 0 : break
   if evt['Rec/Track/Velo'].size() > 0 : break

def injection_event():
  visSTclusters()
  Velo_clusters()
  rc = session().setParameter('modeling.lineWidth','2.')    
  rc = session().setParameter('modeling.userTrackRange','true')   
  rc = session().setParameter('modeling.trackEndz','22000.')      
  uiSvc().visualize('/Event/Rec/Track/Velo')       
 
def veloTrackCluster_view():
# all clusters       
  Velo_clusters('dark')
# only associated clusters 
  disp_velotracks('')
  Style().setColor('green')    
  session().setParameter("Track.location",'Rec/Track/Velo')
  session().setParameter("modeling.what","clusters")
  data_collect(da(),'Track','')
  data_visualize(da())
  session().setParameter("modeling.what","no")
  uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft')
  Page().setTitle('Velo injection Event')
  Camera().setPosition(-74.5703, 449.579, 556.028)                       
  Camera().setHeight(939.035)                                            
  Camera().setOrientation(0.611425, 0.558558, 0.56051, 4.95116)          
  Camera().setNearFar(-6655.25,3041.07)                                  
  ui().synchronize()  
  BeamPipe()    

def beam1_view():
#33062
  uiSvc().visualize('/dd/Structure/Infrastructure')
  BeamPipe()                                                                                 
  uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')                                
  hits('Prev3/')
  hits()
  hits('Next3/')

def hits(tae=''):  
  Style().setColor('blue')
  uiSvc().visualize(tae+'Raw/Hcal/Digits')
  Style().setColor('yellow')
  uiSvc().visualize(tae+'Raw/Spd/Digits')
  Style().setColor('orange')
  uiSvc().visualize(tae+'Raw/Prs/Digits')
  visSTclusters(tae)
  visMuonHits(tae)
  Style().setColor('green')        
  uiSvc().visualize(tae+'Raw/OT/Times')   
  Style().setColor('orange')        
  uiSvc().visualize(tae+RichDigitsLocation)     
  ui().synchronize()
  
def otLoop():
  while 1 > 0 :
   appMgr.run(1)
   if not evt['Raw/OT/Times'] : break
   if evt['Raw/OT/Times'].size() > 50 and evt['Raw/OT/Times'].size() <  500  : break
  
def beamgas():
 appMgr.algorithm('PatSeeding').Enable=False
 while 1>0:
  appMgr.run(1)                
  print evt['DAQ/ODIN'].eventNumber()
  if evt['DAQ/ODIN'].eventNumber() == 1873 : break 
 appMgr.algorithm('PatSeeding').execute()
 visMuonHits('/Event/Next1/')      
 visSTclusters()      
 Style().setColor('blue') 
 uiSvc().visualize('/Event/Raw/Hcal/Digits')
 Style().setColor('yellow')
 uiSvc().visualize('/Event/Raw/Spd/Digits')
 Style().setColor('orange')
 uiSvc().visualize('/Event/Raw/Prs/Digits')
 Style().setColor('green')        
 uiSvc().visualize('/Event/Raw/OT/Times')   
 disp_tracks(0,'Rec/Track/Seed','cosmic')  
 Camera().setPosition(630.839, 450.521, 13030.8)                                           
 Camera().setHeight(11160.2)                                                                
 Camera().setOrientation(-0.340896, 0.898463, 0.276683, 1.4559)                             
 Camera().setNearFar(-8568.51,11346.2)      
 ui().synchronize()   
 wprint('beam1_3track_33026_1873.jpg')                                                                
 wprint('beam1_3track_33026_1873.ps','PSVec')                                                         

def timeAlign():
  appMgr.algorithm('PatSeeding').Enable=False      
  for i in range(100):
   appMgr.run(1)
   print evt['DAQ/ODIN'].eventNumber()
   tae_mult()
def tae_mult():
   for b in ["Prev3/","Prev2/","Prev1/","","Next1/","Next2/","Next3/"]:
     print '%9s'%(b),'Muons:',evt[b+'Raw/Muon/Coords'].size(), 'Hcal:',evt[b+'Raw/Hcal/Digits'].size(),'Prs:',evt[b+'Raw/Prs/Digits'].size(),'Spd:',evt[b+'Raw/Spd/Digits'].size(), 'OT:',evt[b+'Raw/OT/Times'].size()
 
# beam passing by scrapping something
# nr 74, 102, 137, 172, ...    calo muon
# nr 1600, 1670, 1677, 1684, 1691, 1698, 1726, 1754, 1789, 1873, 1999
def movie():       
 for a in appMgr.algorithms():
  if a.find('PatSeeding')>-1: appMgr.algorithm(a).Enable=False 
# for evtnr in [74, 102, 137, 172, 193, 228, 263, 305, 333, 375, 480]:
# for evtnr in [1600, 1670, 1677, 1684, 1691, 1698, 1726, 1754, 1789, 1873, 1999]:
# 2009
 for evtnr in[97653,109358,121053,144458,156153,167858,179553,191258,202953,238048,249743,261448,273143,284848,296543,308248,319943,331648,343353,355048]:
   beam_splash(evtnr,False,ot=False)
# produce wrl event files

def movie_col():
 while 1>0:
  appMgr.run(1)
  if not evt['DAQ/ODIN'] : break
  evtnr = evt['DAQ/ODIN'].eventNumber() 
  beam_splash(evtnr,False,ot=True,prefix='collision_')

def ot_tae_display(slot):
   Tstation(slot)
   rc = session().setParameter('modeling.what','Measurements') 
   rc = session().setParameter('modeling.lineWidth','2.')  
   rc = session().setParameter('modeling.precision','10')         
   rc = session().setParameter('modeling.userTrackRange','true')
   rc = session().setParameter('modeling.trackEndz','22000.')
   rc = session().setParameter('modeling.trackStartz','-2000.')
   setColor(0., 191., 255.)   # DeepSkyBlue   
   uiSvc().visualize(slot+'Rec/Track/Best')
   session().setParameter("modeling.what","clusters");
   Style().setColor("magenta")
   session().setParameter("Track.location",slot+'Rec/Track/Best')
   data_collect(da(),"Track","")
   data_visualize(da())
   session().setParameter("modeling.what","no")
def beam_splash(evtnr,wrl=False,ot=False,prefix='beam1_evt_'):
 newPage('',1)
 Style().useVisSvc()
 if evt['DAQ/ODIN'].eventNumber() != evtnr :
  print 'search for event',evtnr,evt['DAQ/ODIN'].eventNumber()
  while 1>0:
   appMgr.run(1)                
   if evt['DAQ/ODIN'].eventNumber() == evtnr : break 
  print evt['DAQ/ODIN']
  if ot: tae_tracks()
 gpstime = evt['DAQ/ODIN'].gpsTime()
 title = gpsTimeConv(gpstime)
 name = str(gpstime)
 Page().titleVisible(True)       
 Page().setCurrentRegion(0)   
 if not ot : 
  Camera().setPosition(629.534, 448.061, 13022.3)                                           
  Camera().setHeight(11160.2)                                                                
  Camera().setOrientation(-0.0685859, 0.985195, 0.157122, 2.35581)                             
  Camera().setNearFar(-15544.7,10713)  
 else :
  Camera().setPosition(13000, 7800, 1800)                                           
  Camera().setHeight(10480.1)                                                                
  Camera().setOrientation(-0.127416, 0.96898, 0.21176, 2.16076)                             
  Camera().setNearFar(4442,28913)    
 if wrl == False : 
  BeamPipe()  
  uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')
 #  
 Page().setTitle(title+'  -50ns')
 visMuonHits('/Event/Prev2/')    
 Style().setColor('blue') 
 uiSvc().visualize('/Event/Prev2/Raw/Hcal/Digits')
 Style().setColor('yellow')
 uiSvc().visualize('/Event/Prev2/Raw/Spd/Digits')
 Style().setColor('orange')
 uiSvc().visualize('/Event/Prev2/Raw/Prs/Digits')
 if ot: 
   Tstation(slot='/Event/Prev2/')
   disp_tracks(0,'Prev2/Rec/Track/Seed','cosmic')

 overlay_runeventnr(t1=0.91,t2=0.81,t3=0.2,t4=0.1,i1=0.1,i2=0.05,i3=0.05,i4=0.05,logoonly=True)
 Page().setCurrentRegion(0)   
 ui().synchronize()   
 session().flush()
 if wrl :              
  wprint(prefix+name+'_'+str(evtnr)+'_1prev2.wrl','wrl')                                                                
 else :              
  wprint(prefix+name+'_'+str(evtnr)+'_1prev2.jpg')                                                                
#  
 EraseEventAllRegions()
 Page().setCurrentRegion(0)   
 Page().setTitle(title+'  -25ns')
 visMuonHits('/Event/Prev1/')          
 Style().setColor('blue') 
 uiSvc().visualize('/Event/Prev1/Raw/Hcal/Digits')
 Style().setColor('yellow')
 uiSvc().visualize('/Event/Prev1/Raw/Spd/Digits')
 Style().setColor('orange')
 uiSvc().visualize('/Event/Prev1/Raw/Prs/Digits')
 if ot: 
   Tstation(slot='/Event/Prev1/')
   disp_tracks(0,'Prev1/Rec/Track/Seed','cosmic')

 ui().synchronize()   
 session().flush()
 if wrl :              
  wprint(prefix+name+'_'+str(evtnr)+'_2prev1.wrl','wrl')                                                                
 else :              
  wprint(prefix+name+'_'+str(evtnr)+'_2prev1.jpg')                                                                
#  
 EraseEventAllRegions()
 Page().setCurrentRegion(0)   
 Page().setTitle(title+'    0ns')
 visMuonHits('/Event/')      
 Style().setColor('blue') 
 uiSvc().visualize('/Event/Raw/Hcal/Digits')
 Style().setColor('yellow')
 uiSvc().visualize('/Event/Raw/Spd/Digits')
 Style().setColor('orange')
 uiSvc().visualize('/Event/Raw/Prs/Digits')
 if ot: 
   Tstation(slot='/Event/')
   disp_tracks(0,'Rec/Track/Seed','cosmic')

 ui().synchronize()   
 session().flush()
 if wrl :              
  wprint(prefix+name+'_'+str(evtnr)+'_3trigger.wrl','wrl')                                                                
 else :              
  wprint(prefix+name+'_'+str(evtnr)+'_3trigger.jpg')                                                                
#  
 EraseEventAllRegions()
 Page().setCurrentRegion(0)   
 Page().setTitle(title+'  +25ns')
 visMuonHits('/Event/Next1/')      
 Style().setColor('blue') 
 uiSvc().visualize('/Event/Next1/Raw/Hcal/Digits')
 Style().setColor('yellow')
 uiSvc().visualize('/Event/Next1/Raw/Spd/Digits')
 Style().setColor('orange')
 uiSvc().visualize('/Event/Next1/Raw/Prs/Digits')
 if ot: 
   Tstation(slot='/Event/Next1/')
   disp_tracks(0,'Next1/Rec/Track/Seed','cosmic')
 
 ui().synchronize()   
 session().flush()
 if wrl :              
  wprint(prefix+name+'_'+str(evtnr)+'_4Next1.wrl','wrl')                                                                
 else :              
  wprint(prefix+name+'_'+str(evtnr)+'_4Next1.jpg')                                                                
#  
 EraseEventAllRegions()
 Page().setCurrentRegion(0)   
 Page().setTitle(title+'  +50ns')
 visMuonHits('/Event/Next2/')      
 Style().setColor('blue') 
 uiSvc().visualize('/Event/Next2/Raw/Hcal/Digits')
 Style().setColor('yellow')
 uiSvc().visualize('/Event/Next2/Raw/Spd/Digits')
 Style().setColor('orange')
 uiSvc().visualize('/Event/Next2/Raw/Prs/Digits')
 if ot: 
   Tstation(slot='/Event/Next2/')
   disp_tracks(0,'Next2/Rec/Track/Seed','cosmic')

 ui().synchronize()   
 session().flush()
 if wrl :              
  wprint(prefix+name+'_'+str(evtnr)+'_5Next2.wrl','wrl')                                                                
 else :              
  wprint(prefix+name+'_'+str(evtnr)+'_5Next2.jpg')                                                                

def tae_tracks():
  for b in ["Prev3","Next3","Prev2","Next2","Prev1","Next1",""]:
   #incSvc.fireIncident(endEv)
   #incSvc.fireIncident(begEv)
   appMgr.algorithm('PatSeeding'+b).execute()  
   appMgr.algorithm('PatSeedingFit'+b).execute()   

def beam_splash_infra(evtnr,wrl=False,ot=False):
 Style().useVisSvc()
 if evt['DAQ/ODIN'].eventNumber() != evtnr :
  while 1>0:
   appMgr.run(1)                
   if evt['DAQ/ODIN'].eventNumber() == evtnr : break 
  print evt['DAQ/ODIN']
  if ot: tae_tracks()
 gpstime = evt['DAQ/ODIN'].gpsTime()
 title = gpsTimeConv(gpstime)
 name = str(gpstime)
 Page().titleVisible(True)                              
 EraseEventAllRegions()
 Page().setTitle(title+'    0ns')
 visMuonHits('/Event/Next1/')      
 Style().setColor('blue') 
 uiSvc().visualize('/Event/Raw/Hcal/Digits')
 Style().setColor('yellow')
 uiSvc().visualize('/Event/Raw/Spd/Digits')
 Style().setColor('orange')
 uiSvc().visualize('/Event/Raw/Prs/Digits')
 if ot: ot_tae_display('/Event/Next1/')
 ui().synchronize()   
 session().flush()
 if wrl :              
  wprint('beam1_evt_'+name+'_'+str(evtnr)+'_infra.wrl','wrl')                                                                
 else :              
  wprint('beam1_evt_'+name+'_'+str(evtnr)+'_infra.jpg')                                                                


import next_event
def Velo_TED_movie(nev,nmax):
 while nev>0:
  Page().setTitle('VELO RZ')
  next_event.execute()
  if not evt['DAQ/ODIN'] : break    
  if evt['Rec/Track/Velo'].size() < nmax : continue
  nev -= 1
  session().setParameter('modeling.projection','-ZR')
  Onlinemodule.VeloStationContours()
  evtnr   = evt['DAQ/ODIN'].eventNumber() 
  runnr   = evt['DAQ/ODIN'].runNumber() 
  gpstime = evt['DAQ/ODIN'].gpsTime()
  stime = gpsTimeConv(gpstime)
  print stime
  name = str(stime)+' evtnr:'+str(evtnr)
  # Page().setTitle(name)
  overlay_runeventnr_xx(runnr,evtnr,stime)
  wprint('Velo_TED_'+str(runnr)+'_'+str(gpstime)+'_'+str(evtnr)+'_0.jpg')                                                                  
  session().setParameter('modeling.projection','-ZR')
  Style().setColor('yellow')
  session().setParameter('modeling.userTrackRange',  'true')
  session().setParameter('modeling.trackStartz',     '-250.')
  session().setParameter('modeling.trackEndz',       '850.')
  session().setParameter('modeling.lineWidth',       '2')
  session().setParameter('modeling.precision',       '50')
  uiSvc().visualize('/Event/Rec/Track/Velo')
  wprint('Velo_TED_'+str(runnr)+'_'+str(gpstime)+'_'+str(evtnr)+'_1.jpg')                                                                  

def overlay_runeventnr_xx(run,event,gpstime):
 found = False
 soPage = ui().findSoPage('Viewer')
 for n in range(soPage.getNumberOfRegions()):
  reg = soPage.getRegion(n)
  typ = reg.getTypeId()
  if typ.getName() == 'SoTextRegion' :
   soTextRegion = reg.cast_SoTextRegion()
   thisText = ' Run '+str(run)+' Event '+str(event)
   rc = soTextRegion.text.set1Value(0,gpstime)
   rc = soTextRegion.text.set1Value(1,thisText)
   print thisText
   found = True
   break
 if found == False :  
  soRegion = soPage.createRegion('SoTextRegion',0.01,0.9,0.1,0.05)
  soTextRegion = soRegion.cast_SoTextRegion()
  thisText = ' Run '+str(run)+' Event '+str(event)
  rc = soTextRegion.text.set1Value(0,gpstime)
  rc = soTextRegion.text.set1Value(1,thisText)
# LHCb logo :
  soRegion = soPage.createRegion('SoImageRegion',0.01,0.01,0.0,0.0)
  soImageRegion = soRegion.cast_SoImageRegion()
  location = os.path.expandvars('$PANORAMIXROOT/scripts/images/logo_LHCb.gif')
  rc = soImageRegion.getImage().fileName.setValue(location)
 
 ui().synchronize()
# concerning movies using ImageMagick
# convert -delay 80 -loop 0 -resize 640x480 *.jpg withRich.gif
def disp_velotracks(opt,container='Rec/Track/Velo'):
# requires -F option, full reconstruction
  rc = session().setParameter('modeling.what','Measurements') 
  rc = session().setParameter('modeling.lineWidth','2.')  
  rc = session().setParameter('modeling.precision','10')          
  rc = session().setParameter('modeling.userTrackRange','true')  
  rc = session().setParameter('modeling.trackEndz','20000.')
  rc = session().setParameter('modeling.trackStartz','-2000.')
  setColor(176.,224.,230.) 
  if opt != '' :  Style().setColor(opt)
  for t in evt[container] :    
     rc = Object_visualize(t)  
#
def setupHighResolutionPlot():
 session().setParameter('modeling.lineWidth','8.')  
 session().setParameter("modeling.precision",'50')
 ui().setParameter('Panoramix_PRViewer_input_addinfo.value','False')
 ui().setParameter('Panoramix_PRViewer_input_vishits.value','True')
 session().setParameter('modeling.markerSize','20') # somehow does not work for R cluster
# CALO ET energies
 ui().setParameter('Panoramix_Calo_input_eoret.value','yes')
# tracking cuts
 if not ui().findWidget('SoEvent_Track') :
  ui().showDialog( '$SOEVENTROOT/scripts/OnX/Track.onx' ,'SoEvent_Track')
  ui().setParameter('SoEvent_Track_input_ptmin.value','500')
  ui().setParameter('SoEvent_Track_input_xmax.value','3')

 PR_Viewer.execute(version='single')
 session().setParameter('modeling.lineWidth','10.')  

#
def makeHighResolutionPlot(name,proj='',transp=False):
# save scene as HIV and produce png file
# scene should have been prepared before
# but first remove all non-display regions
 soPage = ui().currentPage()
 for n in range(soPage.getNumberOfRegions()):
   reg = soPage.getRegion(n)
   if not reg: continue
   typ = reg.getTypeId()
   soname =  typ.getName()
   if str(soname) != 'SoDisplayRegion' :
     rc = soPage.deleteRegion(reg)
# switch off rulers
 ui().executeScript("DLD","OnX page_removeRulers")
 ui().synchronize()
 tevt = "%05d" % (evt['DAQ/ODIN'].eventNumber())
 ttext = str(evt['DAQ/ODIN'].runNumber())+'_'+tevt+'_'+name
 wprint(ttext,'hiv')          
 hivFile = ttext+'.hiv'
 tf = open(hivFile)
 tfl = tf.readlines()
 tf.close()
 of = open('temp','w') 
 k = 0
 while tfl[k].find("Separator")<0:
   of.write(tfl[k])
   k+=1
 of.write(tfl[k])
# works for top view
 of.write( "  SoDirectionalLight {\n" )
 of.write( "  direction 5 0 -1 \n" )
 of.write( "  }\n" )
 of.write( "  SoDirectionalLight {\n" )
 of.write( "  direction -5 0 1  \n" )
 of.write( "  }\n" )
 of.write( "  SoDirectionalLight {\n" )
 of.write( "  direction 1 -5 0 \n" )
 of.write( "  }\n" )
 of.write( "  SoDirectionalLight {\n" )
 of.write( "  direction -1 5 0  \n" )
 of.write( "  }\n" )
 of.write( "  SoDirectionalLight {\n" )
 of.write( "  direction -1 0 5  \n" )
 of.write( "  }\n" )
 of.write( "  SoDirectionalLight {\n" )
 of.write( "  direction 0 -1 5  \n" )
 of.write( "  }\n" )

 for n in range(k+1,len(tfl)): of.write(tfl[n])  
 of.close()
 rc = os.system("mv temp "+hivFile)

 if not transp : tb = ' -jpeg '
 else      : tb = '' 
 if not proj.find('xy')<0 or not proj.find('z')<0 :
# record camera position
       bee = Event_Tree.particle_container.keys()[0] 
       camera = {}
       camera[proj] = Event_Tree.particle_container[bee]['camera']

       temp = tb+'-hcam=%8.1F -xpos=%8.1F -ypos=%8.1F -zpos=%8.1F -xorie=%4.1F -yorie=%4.1F -zorie=%4.1F -angle=%4.3F'%(  camera[proj]['height'],  
                camera[proj]['position'][0],camera[proj]['position'][1],camera[proj]['position'][2],
                camera[proj]['orientation'][0],camera[proj]['orientation'][1],camera[proj]['orientation'][2],camera[proj]['orientation'][3])
       params = temp.replace(' ','')
       for x in ( '-h','-x','-y','-z','-a'): params = params.replace(x,' '+x)   
       print 'make '+proj+' '+params    
       os.system("~/LHCb_software/contrib/LHCb_artist/0.5/LHCb_artist/X11/distrib/LHCb_artist/0.5/bin/LHCb_artist "+params+" "+hivFile)

 elif not proj.find('3d')<0:           os.system("~/LHCb_software/contrib/LHCb_artist/0.5/LHCb_artist/X11/distrib/LHCb_artist/0.5/bin/LHCb_artist \
                   -hcam=19000. -znear=-12000. -zfar=14500. -xpos=1387.99 -ypos=-597.965 -zpos=10582.7 -xorie=-0.905421 -yorie=-0.134469 -zorie=0.402654 -angle=3.82164 \
                   "+tb+hivFile)
 else: os.system("~/LHCb_software/contrib/LHCb_artist/0.5/LHCb_artist/X11/distrib/LHCb_artist/0.5/bin/LHCb_artist "+tb+hivFile)
 if transp : 
  pngfile = hivFile.replace('.hiv','_huge_'+proj+'.png')
  rc = os.system("mv out.png "+pngfile)
 else : 
  pngfile = hivFile.replace('.hiv','_huge_'+proj+'.jpg')
  rc = os.system("mv out.jpg "+pngfile)
#
