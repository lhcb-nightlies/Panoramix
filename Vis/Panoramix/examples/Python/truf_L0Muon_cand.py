from panoramixmodule import *
from ROOT import Double
PosTool  = appMgr.toolsvc().create('MuonFastPosTool', interface='IMuonFastPosTool')
XYZPoint = gaudimodule.gbl.ROOT.Math.XYZPoint

x = Double(0.)
y = Double(0.)
z = Double(0.)
deltax = Double(0.)
deltay = Double(0.)
deltaz = Double(0.)

muons = evt['Raw/Muon/Coords']
for muoncand in evt['Trig/L0/MuonCtrl'] :
 for n in range(3) : 
  for tile in muoncand.muonTileIDs(n):
   sc = PosTool.calcTilePos(tile,x,deltax,y,deltay,z,deltaz)
   point = XYZPoint(x,y,z)
   session().setParameter('modeling.color','magenta')
   uiSvc().visualize(point)
   m = muons(tile)
   if m : 
     session().setParameter('modeling.color','blue')     
     Object_visualize(m)
     
