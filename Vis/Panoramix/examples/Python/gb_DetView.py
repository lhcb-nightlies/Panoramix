import pmx
uiSvc = pmx.uiSvc()

# Page and region layout :
page = pmx.Page()
page.setTitle('My LHCb')
page.createDisplayRegions(1,1,0)

# Setup region (a page can have multiple drawing region) :
page.setCurrentRegion(0)

region = pmx.Region() # To work on the current region.
region.setBackgroundColor('1 1 1')

style = pmx.Style()
style.setSolid()

# Camera positionning :
# Side +z at left :
camera = pmx.Camera()
camera.setHeight(16000)
camera.setPosition(1660,7250,9570)
camera.setOrientation(-0.513,0.688,0.513,1.936)

uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo')
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo2Rich1')

style.setOpened()
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/HPDPanel0')
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/HPDPanel1')
style.setClosed()

uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1EnvModuleQ0')
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1EnvModuleQ1')
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1EnvModuleQ2')
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror1EnvModuleQ3')
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2MasterTop')
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Rich1Mirror2MasterBot')

uiSvc.visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')
#  visualize_magneticField(aUI)

style.setWireFrame()
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT')
style.setSolid()
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTa')
uiSvc.visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTb')

uiSvc.visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/IT')

for i in range(1,4) :
  for j in range(0,2) :
    style.setColor('lightgrey')
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T'+str(i)+'/Ulayer/Quarter'+str(j)
    uiSvc.visualize(s)

    style.setColor('grey')
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T'+str(i)+'/Vlayer/Quarter'+str(j)
    uiSvc.visualize(s)

    style.setColor('dimgrey')
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T'+str(i)+'/X1layer/Quarter'+str(j)
    uiSvc.visualize(s)
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T'+str(i)+'/X2layer/Quarter'+str(j)
    uiSvc.visualize(s)

style.setColor('indianred')
uiSvc.visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/HPDPanel0')
uiSvc.visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/HPDPanel1')
        
style.setClosed()
uiSvc.visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2Gas/Rich2SphMirrorContainer_0')
uiSvc.visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2Gas/Rich2SphMirrorContainer_1')
uiSvc.visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2Gas/Rich2SecMirrorContainer_0')
uiSvc.visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2Gas/Rich2SecMirrorContainer_1')

style.setColor('cyan')
style.dontUseVisSvc()

style.setTransparency(0.1)

style.setColor('lightblue')
uiSvc.visualize('/dd/Structure/LHCb/DownstreamRegion/Prs')
style.setColor('orange')
uiSvc.visualize('/dd/Structure/LHCb/DownstreamRegion/Spd')
style.setColor('red')
uiSvc.visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
style.setColor('violet')
uiSvc.visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')

style.setSolid()
style.setColor('forestgreen')
for i in range(1,6) :
  for j in range(1,5) :
    s ='/dd/Structure/LHCb/DownstreamRegion/Muon/M'+str(i)+'/R'+str(j)
    uiSvc.visualize(s)

style.useVisSvc()
style.setSolid()
style.setTransparency(0)
