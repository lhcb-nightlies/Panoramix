#
#  Example script to manipulate a STL from Python.
#
#  Execute this script with :
#    OS> <setup Panoramix>
#    OS> python gb_test_stl.py
#

import gaudimodule

ss = gaudimodule.gbl.std.vector(str)()
ss.push_back('item 1')
ss.push_back('item 2')
print ss.size()
print ss[0]
print ss[1]
