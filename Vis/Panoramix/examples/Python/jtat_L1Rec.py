from panoramixmodule import *

def L1Rec_plot() :
  session.setParameter('modeling.showCurve','true')
  session.setParameter('modeling.useExtrapolator','true')

  session.setParameter('TrgTrack.location','Rec/L1/L1Tracks')
  session.setParameter('modeling.lineWidth','2') 
  session_setColor('green')
  data_collect('TrgTrack')
  data_visualize()

  session.setParameter('TrgTrack.location','Rec/L1/MuonMatchedTracks')
  session.setParameter('modeling.lineWidth','2') 
  session_setColor('blue')
  data_collect('TrgTrack')
  data_visualize()
  
  #session.setParameter('TrgTrack.location','Rec/L1/VeloSpaceTracks')
  #session.setParameter('modeling.lineWidth','2') 
  #session_setColor('cyan')
  #data_collect('TrgTrack')
  #data_visualize()
  
  session.setParameter('TrgTrack.location','Rec/L1/VttTracks')
  session.setParameter('modeling.lineWidth','2') 
  session_setColor('orange')
  data_collect('TrgTrack')
  data_visualize()

  session.setParameter('modeling.lineWidth','10') 
  session_setColor('red')
  uiSvc.visualize('/Event/Rec/Trg/Vertex2D')  
      
  session.setParameter('modeling.lineWidth','2') 
  session_setColor('white')
  data_collect('VeloCluster','isR==true')
  data_visualize()
  session_setColor('violet')
  data_collect('VeloCluster','isR==false')
  data_visualize()
      
  #session.setParameter('modeling.what','no')
  #session.setParameter('modeling.lineWidth','1') 

page = ui.currentPage()
region = page.currentRegion()
L1Rec_plot() 

session.setParameter('TrgTrack.location','Rec/L1/VeloSpaceTracks')


del region;del page
