from panoramixmodule import *
filename = 'alg_properties.txt'
print 'Dump properties of algorithms, services, etc to file: %s' %(filename)

#---Get the list of algs --------------------------------
gaudi = gaudimodule.AppMgr()
algs = gaudi.algorithms()
f=open(filename,'w')
f.write('          ALGORITHMS\n')
for a in algs :   #---For each one dump the contents of Properties
    try:    properties = gaudi.algorithm(a).properties()
    except: continue
    f.write(10*'-')
    f.write('Properties of %s' %a)
    f.write(20*'-')
    f.write('\n')
    for key in properties :
            ptype =  gaudi.algorithm(a).getInterface().getProperty(key).type()
            if ptype == 'bool' :
              if properties[key] == 1 :
                f.write('%-35s : true \n' % ( key ) )
              else :
                f.write('%-35s : false \n' % ( key ) )
            elif type(properties[key].value()) != type('') :
                try: 
                  first = True
                  for x in properties[key].value() : 
                      if first: 
                        first = False
                        f.write('%-35s : %s \n' % ( key, x ) )
                      else :   f.write('%-35s , %s \n' % (' ', x ) )
                except:   
                  f.write('%-35s : %s \n' % ( key, properties[key].value() ) )
            else :
                  f.write('%-35s : %s \n' % ( key, properties[key].value() ) )
                  
f.write(80*'+')
f.write('\n')
f.write('           SERVICES\n')
serv = gaudi.services()
for x in serv :   #---For each one dump the contents of Properties
    try:    properties = gaudi.service(x).properties()
    except: continue
    f.write(10*'-')
    f.write('Properties of %s' %x)
    f.write(20*'-')
    f.write('\n')
    for key in properties :
            if type(properties[key].value()) != type('') :
                try: 
                  first = True
                  for x in properties[key].value() : 
                      if first: 
                        first = False
                        f.write('%-35s : %s \n' % ( key, x ) )
                      else :   f.write('%-35s , %s \n' % (' ', x ) )
                except:   
                  f.write('%-35s : %s \n' % ( key, properties[key].value() ) )
            else :
                  f.write('%-35s : %s \n' % ( key, properties[key].value() ) )
f.write(40*'+')
f.write('\n')
f.write('           Application manager\n')
properties = gaudi.properties()
modules = []
for key in properties :
            if key=='Dlls' :
               modules = properties[key].value()
            if type(properties[key].value()) != type('') :
                try: 
                  first = True
                  for x in properties[key].value() : 
                      if first: 
                        first = False
                        f.write('%-35s : %s \n' % ( key, x ) )
                      else :   f.write('%-35s , %s \n' % (' ', x ) )
                except:   
                  f.write('%-35s : %s \n' % ( key, properties[key].value() ) )
            else :
                  f.write('%-35s : %s \n' % ( key, properties[key].value() ) )
f.write(40*'+')
f.write('\n')
f.write('           Tool properties, to come with next Gaudi release\n')
ready = 0

if ready != 0 :
 t='Charged.PhysDesktop.CombinedParticleMaker'
 properties = gaudimodule.iAlgTool(t).properties()
 for key in properties :
            ptype =  gaudimodule.iAlgTool(t).getInterface().getProperty(key).type()
            if ptype == 'bool' :
              if properties[key] == 1 :
                f.write('%-35s : true \n' % ( key ) )
              else :
                f.write('%-35s : false \n' % ( key ) )
            elif type(properties[key].value()) != type('') :
                try: 
                  first = True
                  for x in properties[key].value() : 
                      if first: 
                        first = False
                        f.write('%-35s : %s \n' % ( key, x ) )
                      else :   f.write('%-35s , %s \n' % (' ', x ) )
                except:   
                  f.write('%-35s : %s \n' % ( key, properties[key].value() ) )
            else :
                  f.write('%-35s : %s \n' % ( key, properties[key].value() ) )
 f.write(40*'+')
 f.write('\n')
 for m in modules :   #---For each one dump the contents of Component and Properties
                try:    properties = gaudimodule.getComponentProperties(m)
                except: continue
                print m
                for c in properties :
                    f.write(80*'-')
                    f.write('\n')
                    f.write('Properties of %s %s in library %s' % (properties[c][0], c, m))
                    f.write('\n')
                    print 'Properties of %s %s in library %s' % (properties[c][0], c, m)
                    f.write(80*'-')
                    f.write('\n')
                    for p in properties[c][1] :
                        value = properties[c][1][p]
                        if type(value) is str :
                            f.write("%-30s = '%s'" % ( c+'.'+p , value))
                            f.write('\n')
                        else :
                            f.write("%-30s = %s" % ( c+'.'+p , str(value)))
                            f.write('\n')

f.close()
webbrowser.open(filename,new = 1)
