from panoramixmodule import *

filename = 'L1_buffer.txt'
btype = ('VeloR', 'VeloPhi', 'TT', 'L0DU', 'L0Calo', 'unknown')

f=open(filename,'w')

evh  = evt['Header']
rb = evt['DAQ/L1Buffer']
print '+++ Dump L1 buffer of size %d to file: %s for run %d and event %d +++' %(rb.currentSize(),filename,evh.runNum(),evh.evtNum())

f.write('run # = %d, event # = %d, raw buffer size = %d \n' %(evh.runNum(),evh.evtNum(),rb.currentSize() ) )

raw = gaudimodule.toShortArray( rb.buffer(), rb.currentSize() )   
f.write('  nr   word    \n'  )

ii = 0
for i in range(len(raw)) :
  l1Word  = raw[i] & 0x0000FFFF
  f.write('%6d  %.4x   ' %(i, l1Word) )  

  if ii==0 :
    bankType =  l1Word>>13
    sourceID = (l1Word-(bankType<<13))>>7
    length   = (l1Word-(bankType<<13)-(sourceID<<7)) 
    if bankType > 4 or bankType < 0:
     bankType = 5
    f.write('  sourceID: %d  bankType: %s length: %d    ' %( sourceID,btype[bankType],length ) )
    ii=length-1
  else :
    if btype[bankType]=='VeloR' or btype[bankType]=='VeloPhi' :
     oneStrip =  l1Word>>14 
     l1Word   = (l1Word-(oneStrip<<14)) 
     highSignal = 'false'
     if l1Word>>13 > 0 :
      highSignal = 'true'
      l1Word = (l1Word-(1<<13)) 
     strip = l1Word     
     f.write('strip %5d size %2d highSignal %5s' %(strip, oneStrip+1, highSignal )   )   

    if btype[bankType]=='TT' :
     oneStrip =  l1Word & 0x1
     strip = (l1Word & 0x1FFE)>>1
     highSignal = 'false'
     if ((l1Word & 0x2000)>>13) > 0 :
      highSignal = 'true'
     f.write('strip %5d sizep %2d highSignal %5s' %(strip, oneStrip, highSignal )   )   

    if btype[bankType]=='L0Calo' :
      f.write(' ')   
    
    if btype[bankType]=='L0DU' :
      f.write(' ')        

    ii-=1     

  f.write('\n')


f.close()
