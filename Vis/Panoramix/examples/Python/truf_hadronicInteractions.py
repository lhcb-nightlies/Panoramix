# hadronic interaction jet
session().setParameter('modeling.projection','-ZR') 

def addDaughters(moth1):
 daughters = []
 for p in evt['MC/Particles'] :
  moth = p.mother()
  try:
   if moth.key() == moth1 : daughters.append(p)
  except:
   a = p
 return daughters

# plot all daughter particles:

moth1 = evt['MC/Particles'][2012].mother().key()
Style().setColor('green')
Object_visualize(evt['MC/Particles'][moth1])

daughters = []
daughters.extend(addDaughters(moth1))
granddaughters = []
for p in daughters :
 granddaughters.extend(addDaughters(p.key()))

Style().setColor('yellow')
for d in daughters:
 Object_visualize(d)
for d in granddaughters:
 Object_visualize(d)

Style().setColor('red')
data_collect(da(),'SceneGraph(@current@)','highlight==false')
data_filter(da(),'name','MCParticle*')
session().setParameter('modeling.what','Clusters')
data_visualize(da())


Style().setColor('blue')
# collect all clusters
alldaughters = daughters
alldaughters.extend(granddaughters)

VeloCluster = GaudiPython.gbl.LHCb.VeloCluster
MCParticle = GaudiPython.gbl.LHCb.MCParticle

allveloclusters = []
vccont = 'Raw/Velo/Clusters'
lfmc = linkedFrom(VeloCluster,MCParticle,vccont)

for d in alldaughters :
  for vc in lfmc.range(d) : 
    allveloclusters.append(vc.channelID().channelID())
#192

alltracks = []
for t in evt['Rec/Track/Best']:
 if t.type() != t.Long : continue
 matched = False
 for l in t.lhcbIDs() : 
   if not l.isVelo() : continue
   for vc in allveloclusters : 
     if l.channelID() == vc :
       matched = True 
       alltracks.append(t)
       break
   if matched : break    
#139     
allvelotracks = []
for t in evt['Rec/Track/Velo']:
 if t.type() != t.Velo : continue
 matched = False
 for l in t.lhcbIDs() : 
   if not l.isVelo() : continue
   for vc in allveloclusters : 
     if l.channelID() == vc :
       matched = True 
       allvelotracks.append(t)
       break
   if matched : break    
for t in allvelotracks:
 Object_visualize(t)

h2d = TH2F('h2d','slopes',100,-0.1,0.1,100,-0.1,0.1)
for t in alltracks:
 slopes = t.slopes()
 sx = slopes.x()
 sy = slopes.y()
 rc = h2d.Fill(sx,sy)

velo = det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']
LHCbID = GaudiPython.gbl.LHCb.LHCbID
 
hrhits = TH2F('hrhits','r vs z',100,0.,900.,125,0.,50.)
hrhits_zone = {}
for n in range(8):
 hrhits_zone[n] =  TH2F('hrhits_z'+str(n),'r vs z zone=='+str(n),100,0.,900.,125,0.,50.)

hrhits.Reset()
for n in range(8):
 hrhits_zone[n].Reset()
 
for vc in evt[vccont] : 
 if not vc.isRType() : continue
 sensor = velo.sensor(vc.channelID().sensor())
 zone = sensor.zoneOfStrip(vc.channelID().strip())
 vcid  = LHCbID(vc.channelID())
 traj  = velo.trajectory(vcid,0.)
 trajp = traj.get()
 pp    = trajp.position(0.5)
 rho = pp.rho()
 rc = hrhits.Fill(pp.z(),rho)
 zone = int(8.*(pp.phi()+3.141)/(2.*3.141)) 
 rc = hrhits_zone[zone].Fill(pp.z(),rho)
   
