from panoramixmodule import *

def execute() :  
 session().setParameter('modeling.showCurve','true')
 session().setParameter('modeling.useExtrapolator','true')
 
 # velo veloBack veloTT unique  seed match forward isDownstream
 #filter = '(unique==true&&pt>500)'
 filter = '(unique==true)'
 
 Style().setLineWidth(2) 
 Style().setColor('green')
 data_collect(da(),'Track','velo==true&&'+filter)
 data_visualize(da())
 Style().setColor('greenyellow')
 data_collect(da(),'Track','backward==true&&'+filter)
 data_visualize(da())
 Style().setColor('cyan')
 data_collect(da(),'Track','downstream==true&&'+filter)
 data_visualize(da())
 Style().setColor('blue')
 data_collect(da(),'Track','long==true&&'+filter)
 data_visualize(da())
 Style().setColor('skyblue')
 data_collect(da(),'Track','match==true&&'+filter)
 data_visualize(da())
 
 Style().setColor('white')
 data_collect(da(),'VeloCluster','isR==true')
 data_visualize(da())
 Style().setColor('violet')
 data_collect(da(),'VeloCluster','isR==false')
 data_visualize(da())
 
 session().setParameter('modeling.what','clusters')
 Style().setColor('red')
 data_collect(da(),'Track','')
 data_visualize(da())
 
 session().setParameter('modeling.what','no')
 Style().setColor('red')
 data_collect(da(),'MCParticle','(charge!=0)&&(mass>0.1)&&(mass<1000)&&(timeOfFlight>0.1)&&(bcflag>0)')
 data_visualize(da())
 
 Style().setColor('gold')
 session().setParameter('modeling.what','MCParticle')
 data_collect(da(),'Track','')
 data_visualize(da())
 
 Style().setColor('red')
 uiSvc().visualize('/Event/Rec/Vertex/Primary')  

 session().setParameter('modeling.what','no')
 Style().setLineWidth(1) 

