from panoramixmodule import *
from Configurables import LHCbApp

def BeforeMagnetRegion():
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1')
 uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT')
 
def MagnetRegion():
 uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')
#  visualize_magneticField(aUI)
 uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/PipeInMagnet')
 uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/PipeSupportsInMagnet')

def ITlayers() :
 for i in range(1,4) :
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/IT/Station'+str(i)
    for b in ['TopBox','BottomBox','ASideBox','CSideBox']:
      sb = s+'/'+b
      for l in ['LayerX1','LayerX2','LayerV','LayerU']:
       sbl = sb+'/'+l
       uiSvc().visualize(sbl)

def Tstations() :
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/IT')
 for i in range(1,4) :
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/IT/Station'+str(i)
    uiSvc().visualize(s)
 for i in range(1,4) :
  for j in range(0,2) :
   if  LHCbApp().DataType  == 'DC06':
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T'+str(i)+'/Ulayer/Quarter'+str(j)
    uiSvc().visualize(s)
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T'+str(i)+'/Vlayer/Quarter'+str(j)
    session().setParameter('modeling.color','dimgrey')
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T'+str(i)+'/X1layer/Quarter'+str(j)
    uiSvc().visualize(s)
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T'+str(i)+'/X2layer/Quarter'+str(j)
    uiSvc().visualize(s)
   else : 
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T'+str(i)+'/U/Q'+str(j)
    uiSvc().visualize(s)
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T'+str(i)+'/V/Q'+str(j)
    session().setParameter('modeling.color','dimgrey')
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T'+str(i)+'/X1/Q'+str(j)
    uiSvc().visualize(s)
    s ='/dd/Structure/LHCb/AfterMagnetRegion/T/OT/T'+str(i)+'/X2/Q'+str(j)
    uiSvc().visualize(s)
    
    
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/PipeInT')

def Rich2() :
 uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2')

def Calo():
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Prs')
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Spd')
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')

def Muon():
 Style().dontUseVisSvc()
 Style().setTransparency(0.1)
 mscolors = {1:'mediumseagreen',2:'limegreen',3:'seagreen',4:'darkgreen',5:'darkolivegreen'}
 for i in range(1,6) :
   Style().setColor(mscolors[i])
   s ='/dd/Structure/LHCb/DownstreamRegion/Muon/M'+str(i)+'/M'+str(i)+'ASide'
   uiSvc().visualize(s)
   s ='/dd/Structure/LHCb/DownstreamRegion/Muon/M'+str(i)+'/M'+str(i)+'CSide'
   uiSvc().visualize(s)
 Style().useVisSvc()
 Style().setTransparency(1.0)
