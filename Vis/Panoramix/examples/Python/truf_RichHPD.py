from panoramixmodule import *

def createPage(title): 
 Page().setTitle(title)
 Page().titleVisible(True)
 Viewer().setFrame()
 if title.find('RICH1') != -1 : 

  Page().createDisplayRegions(1,2,0)
  # Rich1 upper panel
  Page().setCurrentRegion(0)
  Region().setBackgroundColor('black')
  #region.setParameter('borderSize','1')
  # Camera positionning :
  Camera().setPosition(1154.168,-20353.951,-9486.5918)
  Camera().setHeight(747.782)
  Camera().setOrientation(-0.015347386,0.85296381,-0.52174419,  3.1033111)
  Camera().setNearFar(21561, 28855.881)

  # Rich1 lower panel
  Page().setCurrentRegion(1)
  Region().setBackgroundColor('black')
  # Camera positioning :
  Camera().setPosition(1154.168,20353.951,-9486.5918)
  Camera().setHeight(747.782)
  Camera().setOrientation(0.015347386,0.85296381,0.52174419,  3.1033111)
  Camera().setNearFar(21561, 28855.881)
 elif title.find('RICH2') != -1 :
  Page().createDisplayRegions(2,1,0)
  # Rich2 left panel
  Page().setCurrentRegion(0)
  Viewer().setFrame()
  Region().setBackgroundColor('black')
  #region.setParameter('borderSize','1')
  # Camera positioning :
  Camera().setPosition(-7494.12, 302.366, 4064.35)
  Camera().setHeight(928.81)
  Camera().setOrientation(0.011812864, 0.99987644, 0.010366189, 4.1833463)
  Camera().setNearFar(1497.88, 13573.3)
  # Rich2 right panel
  Page().setCurrentRegion(1)
  Viewer().setFrame()
  Region().setBackgroundColor('black')
  # Camera positionning :
  Camera().setPosition(7494.12, 302.366, 4064.35)
  Camera().setHeight(928.81)
  Camera().setOrientation(-0.011812864, 0.99987644, 0.010366189, -4.1833463)
  Camera().setNearFar(1497.88, 13573.3)
 else : 
 # both
  Page().createDisplayRegions(2,2,0)

  # Rich1 upper panel
  Page().setCurrentRegion(0)
  Viewer().setFrame()
  Region().setBackgroundColor('black')
  # Camera positioning :
  Camera().setPosition(1154.168, -20353.951, -9486.5918)
  Camera().setHeight(1250.)
  Camera().setOrientation(-0.015347386, 0.85296381, -0.52174419,  3.1033111)
  Camera().setNearFar(21221.51, 28855.881)

  # Rich1 lower panel
  Page().setCurrentRegion(1)
  Viewer().setFrame()
  Region().setBackgroundColor('black')
  # Camera positionning :
  Camera().setPosition(1154.168, 20353.951, -9486.5918)
  Camera().setHeight(1250.)
  Camera().setOrientation(0.015347386, 0.85296381, 0.52174419,  3.1033111)
  Camera().setNearFar(21221.51, 28855.881)
  
  # Rich2 left panel
  Page().setCurrentRegion(2)
  Viewer().setFrame()
  Region().setBackgroundColor('black')
  # Camera positioning :
  Camera().setPosition(-7497.7017, 227.00066, 4066.6152)
  Camera().setHeight(2000.)
  Camera().setOrientation( 0.011812864, 0.99987644, 0.010366189, 4.1833463)
  Camera().setNearFar(668.16394, 15711.722)
  
  # Rich2 right panel
  Page().setCurrentRegion(3)
  Viewer().setFrame()
  Region().setBackgroundColor('black')
  # Camera positioning :
  Camera().setPosition(7497.7017, 227.00066, 4066.6152)
  Camera().setHeight(2000.)
  Camera().setOrientation(-0.011812864, 0.99987644, 0.010366189, -4.1833463)
  Camera().setNearFar(668.16394, 15711.722)

 Style().useVisSvc()
 Style().setWireFrame()
 Style().setSolid()

import Rich_RecoPhotons_Imp
import Rich_RecoRings_Imp
import Rich_RecoPixels_Imp
import Rich_TracklessRings_Imp
 
def execute(flag,what='',check4page=True) : 
 reco = ' with Pixels and CK Rings'
 if what == 'trackless' :  reco = ' with pixels and standalone ring finding'
 if flag == 1 : 
   title = 'RICH1 HPD Panels'+reco
 if flag == 2 : 
   title = 'RICH2 HPD Panels'+reco
 if flag == 3 : 
   title = 'RICH HPD Panels'+reco
 curtitle = ui().currentPage().title.getValues()[0]
 if not (title == curtitle) and  check4page :     
   createPage(title)
   onlineViews[title] = 'truf_RichHPD.execute('+str(flag)+', "'+what+'")'

 Style().dontUseVisSvc()  
 Style().setWireFrame()
 
 if evt['Rec/Rich'] : 
  # Draw the Rings and pixels
  # CRJ : Not looping over regions now done internally in following methods
  if what == 'trackless' : 
   Rich_RecoPixels_Imp.Visualize_Rich_RecoPixels()
   Rich_TracklessRings_Imp.Visualize_Rich_TracklessRings_All()
   Rich_TracklessRings_Imp.Visualize_Rich_TracklessRings_Best()
   Rich_TracklessRings_Imp.Visualize_Rich_TracklessRings_Isolated() 
  elif what == 'photons' :
   Rich_RecoPhotons_Imp.Visualize_Rich_RecoPhotons()
  else :
   Rich_RecoRings_Imp.Visualize_Rich_RecoRings()
   Rich_RecoPixels_Imp.Visualize_Rich_RecoPixels()
