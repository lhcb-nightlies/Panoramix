# find nice events
from panoramixmodule import *
import panoramixmodule 
import PR_Viewer
import GaudiKernel.SystemOfUnits as units
from GaudiPython import AppMgr

def nroftracks():
  nforw = 0
  nback = 0
  nvelo = 0
  for tr in evt['Rec/Track/Best']:
    if tr.type() != tr.Velo and  tr.type() != tr.Long : continue
    nvelo+=1
    first = tr.firstState().position()
    if first.rho()>5.      : continue
    if abs(first.z())>200. : continue
    if tr.flag() == tr.Backward : nback +=1
    else : nforw += 1 
  return nforw, nback, nvelo  

def myEventLoop():
 while 1>0:
  AppMgr().run(1)
  if not evt['DAQ/ODIN']: break
  nforw, nback, nvelo  = nroftracks()
  nmuon = 0
  for am in evt['Rec/Track/Muon']: 
    if am.p()<3./units.GeV : continue
    nstation = [0,0,0,0,0]
    for mid in am.lhcbIDs():
     muonID = mid.muonID()
     nstation[muonID.station()] = 1 
    nstat = 1 
    for n in range(1,5): nstat*=nstation[n]
    if nstat > 0 : nmuon+=1
  if nforw > 8 and nback > 6 and nvelo > 4 and nmuon > 0 : break
 panoramixmodule.reco_sequence()  
 PR_Viewer.execute(version='with Velo')


#sys_import('findNiceEvents')
