from panoramixmodule import *
TrackMasterFitter    = appMgr.toolsvc().create('TrackMasterFitter',  interface='ITrackFitter')

if  ui().findWidget('Viewer_2d')  :
  ui().setCurrentWidget(ui().findWidget('Viewer_2d'))        
  Page().setCurrentRegion(0)
  session().setParameter('modeling.projection','-ZR')
  
  cl = 1
  if cl > 0 :
   Style().setColor('violet')
   data_collect(da(),'VeloCluster','isR==false')
   data_visualize(da())
  
   Style().setColor('white')
   data_collect(da(),'VeloCluster','isR==true')
   data_visualize(da())
  

  Style().setColor('blue')
  session().setParameter('modeling.projection','')

