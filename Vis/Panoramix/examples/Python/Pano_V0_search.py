from panoramixmodule import *
from GaudiPython import gbl
from ROOT import TH1F,TH3F
from LinkerInstances.eventassoc import linkedFrom, linkedTo  
MCParticle = gbl.LHCb.MCParticle
Track      = gbl.LHCb.Track

lfrommc = linkedFrom(Track,MCParticle,'Rec/Track/Best')
v0         = [310,3122]
import GaudiKernel.SystemOfUnits as units

def execute(again=False):
 appMgr = AppMgr()
 found = False
 while not found:
  if not again: appMgr.run(1)
  for mcp in evt['MC/Particles']:  
      lhcbpid = mcp.particleID()
      pid =  lhcbpid.pid()
      mom = mcp.momentum()
      eta = mcp.pseudoRapidity()
      pt = mom.pt()/units.GeV
      if abs(pid) in v0:
         decayVX = mcp.endVertices()
         if decayVX.size()==0 : continue 
         if decayVX[0].position().z()>3000. : continue
         if  eta<2 or eta>5: continue
         allcharged = True
         daughters = mcp.endVertices()[0].target().products()
         for  d in daughters:
           if d.particleID().threeCharge() == 0 : allcharged = False
         if not allcharged : continue
         print mcp.key(),  decayVX[0].position().z()
         EraseEventAllRegions()
         Style().setColor('yellow')
         Object_visualize(mcp)
         for d in mcp.endVertices()[0].target().products():
                  Style().setColor('green')
                  Object_visualize(d.target())
         found = True  
         Style().setColor('red')
         MCParticle_Clusters()
         Style().setColor('blue')
         MCParticle_Track()

def MCParticle_Clusters():
    data_collect(da(),'SceneGraph(@current@)','highlight==false')
    data_filter(da(),'name','MCParticle*')
    session().setParameter('modeling.what','Clusters')
    data_visualize(da())
def MCParticle_Track():
    data_collect(da(),'SceneGraph(@current@)','highlight==false')
    data_filter(da(),'name','MCParticle*')
    session().setParameter('modeling.what','Track')
    data_visualize(da())

def lifetime(mcp):
 lt = 0.
 evx = mcp.endVertices()
 if evx.size() > 0:
   vx = evx[0].target()
   z = vx.position().z()
   lt = vx.time() 
   print z,lt

allptime  =  TH1F('allpion_time','all pions, lifetime',100,0.,10.)
allktime  =  TH1F('allkaon_time','all kaons, lifetime',100,0.,10.)
ptime     =  TH1F('pion_time','pion from Ks, lifetime',100,0.,10.)
htime     =  TH1F('pion_hard','pion from Ks, interaction',100,0.,10.)
kstime    =  TH1F('kstime','Ks lifetime',100,0.,1.)
endVX     =  TH3F('endVX','end vertex of pions',100,-500.,9000.,100,-2500.,2500.,100,-2500.,2500.)
   
myHists   = [allptime,allktime,ptime,htime,kstime,endVX]

XYZVector      = gbl.Math.XYZVector
def pionLifetime():
 myRewind()
 while 1>0:
  appMgr.run(1)
  if not evt['MC/Particles']: break
  for mcp in evt['MC/Particles']:  
      lhcbpid = mcp.particleID()
      pid =  lhcbpid.pid()
      if pid != 310 and abs(pid) != 211 and abs(pid) != 321 : continue
      ovx   = mcp.originVertex()
      ovx_z = ovx.position().z()
      if ovx_z<-300. or ovx_z>300. : continue      
      eta = mcp.pseudoRapidity()
      if  eta<2 or eta>5: continue
      ksdvx = mcp.endVertices()
      if ksdvx.size() == 0 : continue
      dvx   = ksdvx[0].target()
      dtime = dvx.time()-ovx.time()
      ltime = dtime/mcp.gamma()  
      if pid == 310 : 
        kstime.Fill(ltime)
      else : 
        if dvx.position().z() > 9000. : 
          ltime = 99.
        elif dvx.type() != dvx.DecayVertex :
          ltime = -99.  
        if abs(pid) == 211 : allptime.Fill(ltime)
        if abs(pid) == 321 : allktime.Fill(ltime)
      if abs(pid) != 310 : continue
# look for pions from Ks
      if ksdvx[0].position().z()>3000. : continue
      daughters = ksdvx[0].target().products()
      for  d in daughters:
           if d.particleID().abspid() != 211 : continue
           vx  = d.originVertex()
           evx = d.endVertices()
           if evx.size() == 0: 
             ltime = 999.
             ptime.Fill(ltime)
           else :      
             mom = mcp.momentum()
             dvx   = evx[0].target()
             dtime = dvx.time()-vx.time()
             ltime = dtime/d.gamma()  
             epos = dvx.position()
             if dvx.type() == dvx.DecayVertex :   ptime.Fill(ltime)
             elif epos.z()<9000 : htime.Fill(ltime)
             if dvx.type() != dvx.DecayVertex : endVX.Fill(epos.z(),epos.x(),epos.y())

def myRewind():
   panorewind()
   for h in myHists: h.Reset() 
