from panoramixmodule import *
longGhost = tsvc.create('LongGhostClassification',interface='ITrackGhostClassification')
info      = gaudimodule.gbl.LHCb.GhostTrackInfo()
Enums     = getEnumNames('LHCb::GhostTrackInfo')

MCParticle = gaudimodule.gbl.LHCb.MCParticle
Track      = gaudimodule.gbl.LHCb.Track
info  = gaudimodule.gbl.LHCb.GhostTrackInfo()

ltrack2part = linkedTo(MCParticle,Track,'Rec/Track/Best')
if ltrack2part.notFound():
 print 'Ghost classification does not work, no MC linker tables exist'
else:
 found = False
 for t in evt['Rec/Track/Best']:
   if t.type() == t.Long:
# only makes sense to ask for ghost classification if no pointer to MC truth exist
     if ltrack2part.range(t).size() == 0 : 
       found = True
       longGhost.info(t,info)
       ghostid =  info.classification()
       print 'track id',t.key(),' Ghost Classification:',Enums['Classification'][ghostid]
 if not found : print 'No ghost tracks in this event'
