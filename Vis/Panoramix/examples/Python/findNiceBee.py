# find nice events
from panoramixmodule import *
import PRplot,PR_Viewer,LHCbMath,math
import GaudiKernel.SystemOfUnits as units
from GaudiPython import gbl
XYZPoint =  gbl.ROOT.Math.XYZPoint
XYZVector = gbl.ROOT.Math.XYZVector
container = 'Phys/StripBu2JpsiKDet/Particles'
ParticleID = gbl.LHCb.ParticleID
sqrt       = gbl.Math.sqrt
MCParticle = gbl.LHCb.MCParticle
Track      = gbl.LHCb.Track
LorentzVector  = gbl.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')
XYZVector      = gbl.Math.XYZVector
boost          = GaudiPython.gbl.Math.Boost

rc = session().setParameter('modeling.sizeText','20')
rc = session().setParameter('modeling.posText','10')
rc = session().setParameter('modeling.precision','50')  

def print_jpg():
  tevt = "%05d" % (evt['DAQ/ODIN'].eventNumber())
  ttext = str(evt['DAQ/ODIN'].runNumber())+'_'+tevt
  ui().setCurrentWidget(ui().findWidget('Viewer_xy')) 
  PRplot.wprint('Bee_'+ttext+'_xy.jpg',format='JPEG',quality='100',info=False)    
  PRplot.wprint('Bee_'+ttext+'_xy.ps',format='GL2PS',quality='100',info=False)    
  ui().setCurrentWidget(ui().findWidget('Viewer_yz')) 
  PRplot.wprint('Bee_'+ttext+'_yz.jpg',format='JPEG',quality='100',info=False)    
  PRplot.wprint('Bee_'+ttext+'_yz.ps',format='GL2PS',quality='100',info=False)    
  ui().setCurrentWidget(ui().findWidget('Viewer')) 
  PRplot.wprint('Bee_'+ttext+'_3.jpg',format='JPEG',quality='100',info=False)    


def myEventLoop(flag=False):
 found = False 
 while 1>0:
  appMgr.run(1)
  if not evt['DAQ/ODIN']: break
  if not evt[container]: continue
  for aBee in evt[container]: 
    mass = aBee.momentum().mass()/units.GeV
    if mass > 3.086 and mass < 3.106:    
     print mass, aBee.key() 
     found = True
  if found : break
 if found: 
   if flag :
    createPage()
    drawV0(0)
   else: 
    PR_Viewer.execute(version='with Velo')
    PV    = evt['Rec/Vertex/Primary'][0]
    pvpos = PV.position()
    overlay_v0properties(aBee,pvpos,flag)
    tevt = "%05d" % (evt['DAQ/ODIN'].eventNumber())
    ttext = str(evt['DAQ/ODIN'].runNumber())+'_'+tevt+'_'
    PRplot.wprint('Bee_'+ttext+'.jpg',format='JPEG',quality='100',info=False)    

 else:
   print 'probably end of file reached'

def overlay_v0properties(aV0,pvpos,flag):
  soPage = ui().currentPage()
  if (soPage.getNumberOfRegions() < 5 and flag or soPage.getNumberOfRegions() < 7 and not flag) : 
    if flag:  soRegion = soPage.createRegion('SoTextRegion',0.01,0.7,0.3,0.1)
    else:     soRegion = soPage.createRegion('SoTextRegion',0.51,0.2,0.3,0.1)
    soTextRegion = soRegion.cast_SoTextRegion()
    soTextRegion.verticalMargin.setValue(0)  
  if flag: reg = soPage.getRegion(4)
  else:    reg = soPage.getRegion(6)
  soTextRegion = reg.cast_SoTextRegion()
  soTextRegion.text.set1Value(0,'B mass=(%4.1f+/-%2.1f)MeV/c2'%(aV0.measuredMass(),aV0.measuredMassErr()) )
  soTextRegion.text.set1Value(1,'momentum: p=%4.2f GeV/c pt=%2.2f GeV/c'%(aV0.p()/(units.GeV),aV0.pt()/(units.GeV)) )
  v0pos = aV0.endVertex().position()
  delx = v0pos.x()-pvpos.x()
  dely = v0pos.y()-pvpos.y()
  delz = v0pos.z()-pvpos.z()
  direction = XYZVector(dely,dely,delz)
  mom = aV0.momentum()
  momdir = XYZVector(mom.x(),mom.y(),mom.z())
  cosangle =  momdir.Dot(direction)/(momdir.r()*direction.r())
  dist = math.sqrt( delx*delx+dely*dely+delz*delz )
  #soTextRegion.text.set1Value(2,'decaylength=%4.2fmm  cos(alpha)=%6.5f'%(dist,cosangle) )


def createPage(): 
 if not ui().findWidget('Viewer_v0') :     
  ui().createComponent('Viewer_v0','PageViewer','ViewerTabStack')
  ui().setCallback('Viewer_v0','collect','DLD','OnX viewer_collect @this@')
  ui().setCallback('Viewer_v0','popup','Python','from Panoramix import sys_import;sys_import("Viewer_popup")')
  ui().setParameter('Viewer_v0.popupItems','Current region\nNo highlighted\nTrack_Clusters\nTrack_Measurements\nTrack_MCParticle\nMCParticle_Track  \nParticle_MCParticle\nParticle_Daughters\nMCParticle_Clusters\nCluster_MCParticles\nMCParticle_MCHits')
 ui().setCallback('Viewer_v0','destroy','DLD','ui_remove_from_currents @this@ OnX_viewers')
 ui().setCurrentWidget(ui().findWidget('Viewer_v0')) 
 Page().setTitle('XZ and YZ projection')
 Page().titleVisible(True)
 Page().createDisplayRegions(1,2,0)
 Page().setCurrentRegion(1) 
 if not ui().findWidget('Viewer_xy') :     
  ui().createComponent('Viewer_xy','PageViewer','ViewerTabStack')
  ui().setCallback('Viewer_xy','collect','DLD','OnX viewer_collect @this@')
  ui().setCallback('Viewer_xy','popup','Python','from Panoramix import sys_import;sys_import("Viewer_popup")')
  ui().setParameter('Viewer_xy.popupItems','Current region\nNo highlighted\nTrack_Clusters\nTrack_Measurements\nTrack_MCParticle\nMCParticle_Track  \nParticle_MCParticle\nParticle_Daughters\nMCParticle_Clusters\nCluster_MCParticles\nMCParticle_MCHits')
 ui().setCallback('Viewer_xy','destroy','DLD','ui_remove_from_currents @this@ OnX_viewers')
 ui().setCurrentWidget(ui().findWidget('Viewer_xy')) 
 Page().setTitle('XY projection')
 Page().titleVisible(True)
 Page().createDisplayRegions(1,1,0)
 ui().executeScript('DLD','Panoramix layout_rulers')
 Page().setCurrentRegion(0) 
 Region().setBackgroundColor('white')
 if not ui().findWidget('Viewer_yz') :     
  ui().createComponent('Viewer_yz','PageViewer','ViewerTabStack')
  ui().setCallback('Viewer_yz','collect','DLD','OnX viewer_collect @this@')
  ui().setCallback('Viewer_yz','popup','Python','from Panoramix import sys_import;sys_import("Viewer_popup")')
  ui().setParameter('Viewer_yz.popupItems','Current region\nNo highlighted\nTrack_Clusters\nTrack_Measurements\nTrack_MCParticle\nMCParticle_Track  \nParticle_MCParticle\nParticle_Daughters\nMCParticle_Clusters\nCluster_MCParticles\nMCParticle_MCHits')
 ui().setCallback('Viewer_yz','destroy','DLD','ui_remove_from_currents @this@ OnX_viewers')
 ui().setCurrentWidget(ui().findWidget('Viewer_yz')) 
 Page().setTitle('YZ projection')
 Page().titleVisible(True)
 Page().createDisplayRegions(1,1,0)
 ui().executeScript('DLD','Panoramix layout_rulers')
 Page().setCurrentRegion(0) 
 Region().setBackgroundColor('white')

def drawV0(key,new=False):
 if new : createPage()
# take first for the moment
 PV    = evt['Rec/Vertex/Primary'][0]
 pvpos = PV.position()
 aV0 = evt[container][key]
 print aV0
 v0pos = aV0.endVertex().position()
 points = gbl.std.vector(XYZPoint)()
 points.push_back(v0pos)
 points.push_back(pvpos)
 LINES = 0
 for np in range(4):
  n = np
  if np < 2 : 
     ui().setCurrentWidget(ui().findWidget('Viewer_v0')) 
  elif np == 2     : 
     ui().setCurrentWidget(ui().findWidget('Viewer_xy'))
     n = 0 
  elif np == 3     : 
     ui().setCurrentWidget(ui().findWidget('Viewer_yz'))
     n = 0 
  Page().setCurrentRegion(n) 
  page   = OnX.session().ui().currentPage()
  height = math.sqrt( (v0pos.x()-pvpos.x())*(v0pos.x()-pvpos.x()) + (v0pos.y()-pvpos.y())*(v0pos.y()-pvpos.y())) * 2.
  region = page.getRegion(n)
  # draw_PVtracks_truncated(PV)
  draw_PVtracks_fake(PV)
  session().setParameter('modeling.showText','true')
  if np == 2 : 
   Camera().setPosition( (v0pos.x()+pvpos.x())/2.,(v0pos.y()+pvpos.y())/2.,5000.)
   Camera().setHeight(height)
   Camera().setOrientation(0,0,1,0)
   Camera().setNearFar(0.,25000.)
   tsf = region.cast_SoDisplayRegion().getTransform()
   tsf.scaleFactor.setValue(iv.SbVec3f(1.,1.,1.))  
  elif n == 0 and np == 0: 
   height = abs(v0pos.z()-pvpos.z()) * 2.
   Camera().setPosition( 0.,20000.,pvpos.z())
   Camera().setHeight(height)
   Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
   Camera().setNearFar(0.,25000.)
   tsf = region.cast_SoDisplayRegion().getTransform()
   tsf.scaleFactor.setValue(iv.SbVec3f(5.,5.,1.))  
  elif n == 1 and np == 1: 
   height = abs(v0pos.z()-pvpos.z()) * 2.
   Camera().setPosition( -1000.,0.,pvpos.z())
   Camera().setHeight(height)
   Camera().setOrientation(0, -1, 0, 1.57)
   Camera().setNearFar(0.,25000.)
   tsf = region.cast_SoDisplayRegion().getTransform()
   tsf.scaleFactor.setValue(iv.SbVec3f(5.,5.,1.))  
  elif np == 3: 
   height = abs(v0pos.z()-pvpos.z()) * 2.
   Camera().setPosition( 0.,0.,pvpos.z())
   Camera().setHeight(height)
   Camera().setOrientation(0, -1, 0, 1.57)
   Camera().setNearFar(0.,25000.)
   tsf = region.cast_SoDisplayRegion().getTransform()
   tsf.scaleFactor.setValue(iv.SbVec3f(5.,5.,1.))  
  Style().setColor('orange')
  session().setParameter('modeling.showText','false') 
  Object_visualize(aV0.endVertex()) 
  session().setParameter('modeling.showText','true')
  tmp = session().parameterValue('modeling.lineWidth')
  session().setParameter('modeling.lineWidth','3.')  
  Object_visualize(aV0) 
  uiSvc().visualize(points,LINES) 
  Style().setColor('cyan')
  session().setParameter('modeling.showText','false')
  Object_visualize(PV) 
  session().setParameter('modeling.showText','true')
  for sdaughter in aV0.daughters():
   daughter = sdaughter.target()
   if daughter.particleID().pid() == 443 : 
     PRplot.setColor(160.,184.,42.)
     Object_visualize(daughter)
     for jdaughter in daughter.daughters():
       amuon = jdaughter.target() 
       Style().setColor('magenta')
       Object_visualize(amuon)
   else:
       Style().setColor('red')
       Object_visualize(daughter)
 session().setParameter('modeling.lineWidth',tmp) 
 ui().setCurrentWidget(ui().findWidget('Viewer_v0')) 
 overlay_runeventnr(t1=0.75,t2=0.77,t3=0.2,t4=0.1,i1=0.01,i2=0.01,i3=0.0,i4=0.0,logoonly=False)
 overlay_v0properties(aV0,pvpos,True)

LorentzVector  = GaudiPython.gbl.Math.LorentzVector('ROOT::Math::PxPyPzE4D<double>')
ParticleID = GaudiPython.gbl.LHCb.ParticleID
import math
def create_LorentzVector(pid,p):
       m   = partsvc.find(ParticleID(pid)).mass()
       rsq = m*m + p.Mag2()
       E   = math.sqrt(rsq)
       lv = LorentzVector(p.x(),p.y(),p.z(),E)
       return lv
def addTracks():
  aBee = evt['Phys/BeeMonitor/Particles'][0].momentum()
  amom = evt['Rec/Track/Best'][17].momentum()
  newmom = create_LorentzVector(321,amom) 
  newPart = newmom + aBee
  return newPart

def pointing(aV0=False,pvpos=False,mom=False):
  if not aV0: 
   aV0 = evt[container][0]
  if not pvpos: 
   pvpos = evt['Rec/Vertex/Primary'][0].position()
  v0pos = aV0.endVertex().position()
  delx = v0pos.x()-pvpos.x()
  dely = v0pos.y()-pvpos.y()
  delz = v0pos.z()-pvpos.z()
  direction = XYZVector(delx,dely,delz)
  if mom == False: 
    mom = aV0.momentum()
  momdir = XYZVector(mom.x(),mom.y(),mom.z())
  cosangle =  momdir.Dot(direction)/(momdir.r()*direction.r())
  dist = math.sqrt( delx*delx+dely*dely+delz*delz )
  errPV = evt['Rec/Vertex/Primary'][0].covMatrix()
  print 'PV x,y,z,sigz,sigy,sigz',pvpos.x(),pvpos.y(),pvpos.z(),math.sqrt(errPV(0,0))*1000.,math.sqrt(errPV(1,1))*1000.,math.sqrt(errPV(2,2))*1000.
  errPV = aV0.endVertex().covMatrix()
  print 'SecVx x,y,z,sigz,sigy,sigz',v0pos.x(),v0pos.y(),v0pos.z(),math.sqrt(errPV(0,0))*1000.,math.sqrt(errPV(1,1))*1000.,math.sqrt(errPV(2,2))*1000.
  print 'cosangle = ',cosangle,' dist = ',dist

def overlayParticles():
 ui().setCurrentWidget(ui().findWidget('Viewer')) 
 aV0 = evt[container][0]
 tmp = session().parameterValue('modeling.lineWidth')
 session().setParameter('modeling.lineWidth','3.')  
 for i in range(3):
  if i==2: session().setParameter('modeling.projection','-ZR')
  Page().setCurrentRegion(i) 
  for sdaughter in aV0.daughters():
   daughter = sdaughter.target()
   if daughter.particleID().pid() == 443 : 
     for jdaughter in daughter.daughters():
       amuon = jdaughter.target() 
       Style().setColor('magenta')
       atrack = amuon.proto().track()
       Object_visualize(atrack)
   else:
       Style().setColor('red')
       atrack = daughter.proto().track()
       Object_visualize(atrack)
       print atrack 
  if i==2: session().setParameter('modeling.projection','')
 session().setParameter('modeling.lineWidth',tmp) 

def draw_PVtracks_truncated(aPV):
  rc = session().setParameter('modeling.userTrackRange','true')
  rc = session().setParameter('modeling.noStates','true')
  pvpos = aPV.position()
  text_pvz = '%5.3f'%(pvpos.z())
  print text_pvz
  rc = session().setParameter('modeling.trackStartz',text_pvz)
  rc = session().setParameter('modeling.trackEndz','1000')
  tmp = session().parameterValue('modeling.lineWidth')
  session().setParameter('modeling.lineWidth','1.')  
  Style().setColor('black')
  displayed_tracks = []
  for tr in aPV.tracks():
    atr = tr.target()
    displayed_tracks.append(atr.key())
    if atr.type() != atr.Velo and atr.type() != atr.Long and atr.type() != atr.Upstream : print atr
    if atr.checkFlag(atr.Backward):
     rc = session().setParameter('modeling.trackEndz',text_pvz)
     rc = session().setParameter('modeling.trackStartz','-100')
     Object_visualize(atr)
     rc = session().setParameter('modeling.trackStartz',text_pvz)
     rc = session().setParameter('modeling.trackEndz','1000')
    else:  
     Object_visualize(atr)
  rc = session().setParameter('modeling.userTrackRange','false')
  for tr in evt['Rec/Track/Best']:
    if tr.key() in displayed_tracks : continue
    mom = tr.momentum()
    if mom.rho() < 500: continue
    if tr.type() != tr.Long : continue
    displayed_tracks.append(tr.key())
    Object_visualize(tr)
  session().setParameter('modeling.lineWidth',tmp)  
  rc = session().setParameter('modeling.noStates','false')

def draw_PVtracks_fake(aPV):
 pvpos = aPV.position()
 LINES = 0
 XYZPoint = GaudiPython.gbl.Math.XYZPoint
 po = XYZPoint(pvpos.x(),pvpos.y(),pvpos.z())
 rc = session().setParameter('modeling.noStates','true')
 tmp = session().parameterValue('modeling.lineWidth')
 session().setParameter('modeling.lineWidth','1.')  
 Style().setColor('black')
 displayed_tracks = []
 for tr in aPV.tracks():
   ps = GaudiPython.gbl.std.vector(XYZPoint)()
   atr = tr.target()
   state = atr.firstState()
   displayed_tracks.append(atr.key())
   ps.push_back(po)
   ze = 1000.
   if atr.checkFlag(atr.Backward): ze = -1000.
   xe = pvpos.x() + state.tx() * ze
   ye = pvpos.y() + state.ty() * ze
   pe = XYZPoint(xe,ye,ze)
   ps.push_back(pe)
   uiSvc().visualize(ps,LINES)  
 rc = session().setParameter('modeling.userTrackRange','true')
 rc = session().setParameter('modeling.trackStartz','-1000.')
 rc = session().setParameter('modeling.trackEndz','1000.')
 for tr in evt['Rec/Track/Best']:
    if tr.key() in displayed_tracks : continue
    mom = tr.momentum()
    if mom.rho() < 500: continue
    if tr.type() != tr.Long : continue
    displayed_tracks.append(tr.key())
    Object_visualize(tr)
 session().setParameter('modeling.lineWidth',tmp)  
 rc = session().setParameter('modeling.noStates','false')
 rc = session().setParameter('modeling.userTrackRange','false')


def draw_PVtracks(aPV):
  rc = session().setParameter('modeling.userTrackRange','true')
  rc = session().setParameter('modeling.trackStartz','-500.')
  tmp = session().parameterValue('modeling.lineWidth')
  session().setParameter('modeling.lineWidth','1.')  
  Style().setColor('black')
  data_collect(da(),'Track','long==true&&unique==true&&pt>500')
  data_visualize(da())
  rc = session().setParameter('modeling.userTrackRange','false')
  session().setParameter('modeling.lineWidth',tmp)  


#************************************************
#*    runNumber * eventNumb * Bee_tau * nPV
#************************************************
#*   69627 *  87192657 * 4.5672788 *  2
#*   69860 *   3754057 * 3.4184768 *  1
#*   69618 *     12484 * 0.5975813 *  1
#*   69625 *  45507449 * 0.2373959 * 1
#************************************************




