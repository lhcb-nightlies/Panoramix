import pmx

da = pmx.DA()

stop_loop = 0

def next_event(i):
  pmx.ui().executeScript("DLD","Panoramix next_event")
  #print 'debug : next_event %d' % i
  da.collect('MCParticle')
  da.visualize()
  pmx.ui().synchronize()

def start_loop():
  global stop_loop
  stop_loop = 0
  i = 0
  while stop_loop == 0 :
    next_event(i)
    pmx.ui().synchronize()
    i = i + 1
  
