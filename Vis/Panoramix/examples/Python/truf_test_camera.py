#
# Example script to visualize per Track (and not full collection).
#
from panoramixmodule import *

tracklocation = 'Rec/Track/Best'
tc = evt[tracklocation]
extrap = appMgr.toolsvc().create('TrackMasterExtrapolator', interface='ITrackExtrapolator')
XYZPoint  = gaudimodule.gbl.ROOT.Math.XYZPoint
XYZVector = gaudimodule.gbl.ROOT.Math.XYZVector

session().setParameter('Track.location',tracklocation)
session().setParameter('modeling.useExtrapolator','true')
Style().setColor('blue')

Style().dontUseVisSvc()  
aPoint = XYZPoint()
aSlope = XYZVector()

t = tc[1]
print 'momentum ',t.p(),t.pt()

npoints = 100
# backwards 
zextr   = 15000.
stopz = t.firstState().z()

# forwards
#stopz  = 15000.
#zextr  = t.firstState().z()

delz  = (stopz - zextr)/(npoints-1)
extrap.position(t,zextr,aPoint)
extrap.slopes(t,zextr,aSlope)

Viewer().removeAutoClipping()
Style().useVisSvc()  
#page.clear()
#uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Velo')
#uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1')
#uiSvc().visualize('/dd/Structure/LHCb/BeforeMagnetRegion/TT')
#uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')
#uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T')
#uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/Rich2')
#uiSvc().visualize('/dd/Structure/LHCb')
Object_visualize(t)

i=0
#backwards 
while zextr > stopz :
# forwards 
#while zextr < stopz :
 x = aPoint.x()
 y = aPoint.y() 
 Camera().setPosition(x,y,zextr)
 tx = aSlope.x()
 ty = aSlope.y()
# backwards 
 Camera().lookAt(tx,ty,-1.)
# forwards Camera().lookAt(tx,ty,1.)
 if zextr < 1200 : 
   if y < 10. : 
     zextr+=delz/50.
   else :
     zextr+=delz/100.     
 if zextr < 2200 : 
   zextr+=delz/10.
 else :
   zextr+=delz
 i+=1
 extrap.position(t,zextr,aPoint)
 extrap.slopes(t,zextr,aSlope) 
 a = 10000.
 b = 0.
 c = max(y*50,10)
# Camera positionning :
 Camera().setHeight(c)
 Camera().setNearFar(a,b)
 ui().synchronize()

# print '%5i %8.2f %8.2f %8.1f  %8.3f ' %(i,x,y,zextr,delz)
print t.firstState().x(),t.firstState().y(),t.firstState().z()


#Viewer().setAutoClipping()

