#Prints out Vertex Information

from panoramixmodule import *
from math import *

print ' '
print (110*('*'))
print '*',(106*(' ')),'*' 
print '* /////////////////// ',(85*(' ')),'*'
print '* /Vertex Infomation/ ',(85*(' ')),'*'
print '* /////////////////// ',(85*(' ')),'*'

#Directories
#---------------------------------

dirPv = evt['Rec/Vertex/Primary']
dir2d = evt['Rec/Trg/Vertex2D']
dirHl = evt['Hlt/VertexReports/PV3D']

#******************************************
# Definitions
#***************************************
# Filling Arrays
#---------------------------------
def arrayValVer(dir):
    
    v = ' '
    if dir:
        v = 'Vertices'
        if dir.size() == 1:
            v = 'Vertex  '
    return v

def arrayValt(dir):
    t = []
    if dir:
        for c in dir:
            try:   
              t.append(c.technique())
            except:
              t.append(" ")  
    return t

def arrayValx(dir):
    xpos = []
    if dir:
        for c in dir:
            pos = c.position()
            xpos.append(pos.x())
    return xpos

def arrayValy(dir): 
    ypos = []
    if dir:
        for c in dir:
            pos = c.position()
            ypos.append(pos.y())
    return ypos

def arrayValz(dir):
    zpos = []
    if dir:
        for c in dir:
            pos = c.position()
            zpos.append(pos.z())
    return zpos

def arrayErr(dir, mat):
    Err = []
    if dir:
        for c in dir:
            p = c.covMatrix()
            ErrSq = p.Diagonal()[mat]
            ErrRt = sqrt(ErrSq)
            Err.append(ErrRt)
    return Err

def arrayTrack(dir):
    TrNum = []
    if dir:
        for c in dir:
            track = c.tracks()
            TrNum.append(track.size())
    return TrNum

def ArrAppend(ind, tot, name): 
    if ind < tot:
        diff = tot - ind
        for k in range(0, diff):
            name.append('-')
    return name

# Matching Definitions
#-----------------------------

def Match1(num1, num2, z1, z2):
    min = []
    fin0 = 1000
    fin1 = 1000
    count = 0
    count1 = 0
    arr = []
    arr0 = 0
    arr1 = 0
    for k in range(0, num1):
        for l in range (0, num2):
            min.append(abs(z1[k]-z2[l])) #Finds difference between all z values
    for m in range(0, (num1*num2)):
        ind0 = min[m]
        if ind0 <= fin0:
            fin0 = ind0
            point0 = m                 #Finds point in array of min difference
    for k in range(0, num1):
        for l in range (0, num2):
            if point0 == count:
                arr0 = k
            count = count+1       #Finds which vertex min applies to
    arr.append(arr0)
    if num1 > 2:                         #Allows extra matching when more than two vertices are present
        for p in range(0, (num1*num2)): 
            ind1 = min[p]
            if ind1 != fin0:    
                if ind1 <= fin1:
                    fin1 = ind1
                    point1 = p
        for q in range(0, num1):
            for r in range (0, num2):
                if point1 == count1:
                    arr1 = q                        
                count1 = count1+1
    arr.append(arr1)
    return arr

def Match2(num1, num2, z1, z2):
    min = []
    fin0 = 1000
    fin1 = 1000
    count = 0
    count1 = 0
    arr = []
    arr0 = 0
    arr1 = 0
    for k in range(0, num1):
        for l in range (0, num2):
            min.append(abs(z1[k]-z2[l])) #Finds difference between all z values
    for m in range(0, (num1*num2)):
        ind0 = min[m]
        if ind0 <= fin0:
            fin0 = ind0
            point0 = m                 #Finds point in array of min difference
    for k in range(0, num1):
        for l in range (0, num2):
            if point0 == count:
                arr0 = l
            count = count+1       #Finds which vertex min applies to
    arr.append(arr0)
    if num1 > 2:                         #Allows extra matching when more than two vertices are present
        for p in range(0, (num1*num2)): 
            ind1 = min[p]
            if ind1 != fin0:    
                if ind1 <= fin1:
                    fin1 = ind1
                    point1 = p
        for q in range(0, num1):
            for r in range (0, num2):
                if point1 == count1:
                    arr1 = r                        
                count1 = count1+1
    arr.append(arr1)
    return arr

def change(b1, a1, ch):     
    if b1 != a1:
        p = ch.pop(a1)
        ch.insert(b1,p)
    return ch

def change2(b2, a2, ch, b1, a1):
    a2New0 = a2 
    if a1 < a2:
        if b1 >= a2:
            a2New0 = a2 - 1
    
    if a1 > a2:
        if b1 <= a2:
            a2New0 = a2 + 1 
    p = ch.pop(a2New0)
    ch.insert(b2,p)
    if a2New0 < b1:
        if b2 > b1:
            a1New0 = b1 - 1
            p1 = ch.pop(a1New0)
            ch.insert(b1,p1)
    if a2New0 > b1:
        if b2 <= b1:
            a1New1 = b1 + 1
            p2 = ch.pop(a1New1)
            ch.insert(b1,p2)    
    return ch 
# Primary Vertices
#-------------------------

numPv = dirPv.size()
vPv = arrayValVer(dirPv)
tPv = arrayValt(dirPv)
xPv = arrayValx(dirPv)
yPv = arrayValy(dirPv)
zPv = arrayValz(dirPv)
errxPv = arrayErr(dirPv,0)
erryPv = arrayErr(dirPv,1)
errzPv = arrayErr(dirPv,2)
trPv = arrayTrack(dirPv)
    
#2dVertices
#---------------------
try:
 num2d = dir2d.size()
except:
 num2d = 0
v2d = arrayValVer(dir2d)
t2d = arrayValt(dir2d)
x2d = arrayValx(dir2d)
y2d = arrayValy(dir2d)
z2d = arrayValz(dir2d)
errx2d = arrayErr(dir2d,0)
erry2d = arrayErr(dir2d,1)
errz2d = arrayErr(dir2d,2)
# tr2d = arrayTrack(dir2d)   - Can't find any info for 2d track entries

#Hlt Vertices
#-----------------------------

try:
 numHl = dirHl.size()
except:
 numHl = 0

vHl = arrayValVer(dirHl)
tHl = arrayValt(dirHl)
xHl = arrayValx(dirHl)
yHl = arrayValy(dirHl)
zHl = arrayValz(dirHl)
errxHl = arrayErr(dirHl,0)
erryHl = arrayErr(dirHl,1)
errzHl = arrayErr(dirHl,2)
#trHl = arrayTrack(dirHl)

#MC Vertices - Have to use different method because info not contained in one directory
#-----------------------------  

MCVerDir = evt['MC/Vertices']
GenDir   = evt['Gen/Collisions']
keyList = []
tMc = []
xMc = []
yMc = []
zMc = []
if MCVerDir:
    for c in MCVerDir: 
         if c.isPrimary() :   
            done = 0
     # dirty hack, not sure if it is always correct       
            col = GenDir[c.key()]
            if col:
                verKey = col.key()
                for l in range(0, len(keyList)):
                    colKey = keyList[l]
                    if verKey == colKey:
                        done = 1            
                if done == 0:
                    keyList.append(verKey)
                    pType = col.processType()
                    #Hard QCD Process
                    if pType == 11:
                        pType = 'f(i)f(j) -> f(i)f(j), 11'
                    if pType == 28:
                        pType = 'f(i)g -> f(i)g, 28'
                    if pType == 53:
                        pType = 'gg -> f(k)f~(k), 53'
                    if pType == 68:
                        pType = 'gg -> gg, 68'
                    #Soft QCD Process
                    if pType == 91:
                        pType = 'Elastic scattering, 91'
                    if pType == 92:
                        pType = 'Single diffraction (XB), 92'
                    if pType == 93:
                        pType = 'Single diffraction (AX), 93'
                    if pType == 94:
                        pType = 'Double diffraction, 94'
                    #Closed Heay Flavour
                    if pType == 89:
                        pType = 'gg -> Chi(1c)g, 89'
                    tMc.append(pType)
                    pos = c.position()
                    xMc.append(pos.x())
                    yMc.append(pos.y())
                    zMc.append(pos.z())

numMc=0
numMc = len(keyList)
vMc= 'Vertices'
if numMc == 1:
    vMc = 'Vertex  '


#Numbering information  - More complicated if done as definitions
#-------------------------------------

numVer = numMc
if numPv > numVer:
    numVer = numPv
if num2d > numVer:
    numVer = num2d
if numHl > numVer:
    numVer = numHl

tHl = ArrAppend(numHl, numVer, tHl)
xHl = ArrAppend(numHl, numVer, xHl)
yHl = ArrAppend(numHl, numVer, yHl)
zHl = ArrAppend(numHl, numVer, zHl)
errxHl = ArrAppend(numHl,numVer, errxHl)
erryHl = ArrAppend(numHl,numVer, erryHl)
errzHl = ArrAppend(numHl,numVer, errzHl)
        
tPv = ArrAppend(numPv, numVer, tPv)
xPv = ArrAppend(numPv, numVer, xPv)
yPv = ArrAppend(numPv, numVer, yPv)
zPv = ArrAppend(numPv, numVer, zPv)
errxPv = ArrAppend(numPv,numVer, errxPv)
erryPv = ArrAppend(numPv,numVer, erryPv)
errzPv = ArrAppend(numPv,numVer, errzPv)
trPv = ArrAppend(numPv, numVer, trPv)

t2d = ArrAppend(num2d, numVer, t2d)
x2d = ArrAppend(num2d, numVer, x2d)
y2d = ArrAppend(num2d, numVer, y2d)
z2d = ArrAppend(num2d, numVer, z2d)
errx2d = ArrAppend(num2d,numVer, errx2d)
erry2d = ArrAppend(num2d,numVer, erry2d)
errz2d = ArrAppend(num2d,numVer, errz2d)

tMc = ArrAppend(numMc, numVer, tMc)
xMc = ArrAppend(numMc, numVer, xMc)
yMc = ArrAppend(numMc, numVer, yMc)
zMc = ArrAppend(numMc, numVer, zMc)

#Matching of Vertices
#-------------------------------------------------

chOrd = 0
if numPv == numVer:     
    #Pv and Hlt changing
    chOrd =1
    arr1 = Match1(numPv, numHl, zPv, zHl)
    arr2 = Match2(numPv, numHl, zPv, zHl)
    tHl = change(arr1[0], arr2[0], tHl)
    xHl = change(arr1[0],  arr2[0],  xHl)
    yHl = change(arr1[0],  arr2[0],  yHl)
    zHl = change(arr1[0],  arr2[0],  zHl)
    errxHl = change(arr1[0], arr2[0], errxHl)
    erryHl = change(arr1[0], arr2[0], erryHl)
    errzHl = change(arr1[0], arr2[0], errzHl)
    if numHl>2:
        tHl = change2(arr1[1], arr2[1], tHl, arr1[0], arr2[0])
        xHl = change2(arr1[1],  arr2[1], xHl, arr1[0], arr2[0])
        yHl = change2(arr1[1],  arr2[1], yHl, arr1[0], arr2[0])
        zHl = change2(arr1[1],  arr2[1], zHl, arr1[0], arr2[0]) 
        errxHl = change2(arr1[1], arr2[1], errxHl, arr1[0], arr2[0])
        erryHl = change2(arr1[1], arr2[1], erryHl, arr1[0], arr2[0])
        errzHl = change2(arr1[1], arr2[1], errzHl, arr1[0], arr2[0])

    #Pv and 2d Changing
    arr1 = Match1(numPv, num2d, zPv, z2d)
    arr2 = Match2(numPv, num2d, zPv, z2d)
    t2d = change(arr1[0], arr2[0], t2d)
    x2d = change(arr1[0],  arr2[0],  x2d)
    y2d = change(arr1[0],  arr2[0],  y2d)
    z2d = change(arr1[0],  arr2[0],  z2d)
    errx2d = change(arr1[0], arr2[0], errx2d)
    erry2d = change(arr1[0], arr2[0], erry2d)
    errz2d = change(arr1[0], arr2[0], errz2d)
    if num2d>2:
        t2d = change2(arr1[1], arr2[1], t2d, arr1[0], arr2[0])
        x2d = change2(arr1[1],  arr2[1],  x2d, arr1[0], arr2[0])
        y2d = change2(arr1[1],  arr2[1],  y2d, arr1[0], arr2[0])
        z2d = change2(arr1[1],  arr2[1],  z2d, arr1[0], arr2[0])
        errx2d = change2(arr1[1], arr2[1], errx2d, arr1[0], arr2[0])
        erry2d = change2(arr1[1], arr2[1], erry2d, arr1[0], arr2[0])
        errz2d = change2(arr1[1], arr2[1], errz2d, arr1[0], arr2[0])
    
    #Pv and Mc Changing
    arr1 = Match1(numPv, numMc, zPv, zMc)
    arr2 = Match2(numPv, numMc, zPv, zMc)
    tMc = change(arr1[0], arr2[0], tMc)
    xMc = change(arr1[0],  arr2[0],  xMc)
    yMc = change(arr1[0],  arr2[0],  yMc)
    zMc = change(arr1[0],  arr2[0],  zMc)
    if numMc>2:
        tMc = change2(arr1[1], arr2[1], tMc, arr1[0], arr2[0])
        xMc = change2(arr1[1],  arr2[1],  xMc, arr1[0], arr2[0])
        yMc = change2(arr1[1],  arr2[1],  yMc, arr1[0], arr2[0])
        zMc = change2(arr1[1],  arr2[1],  zMc, arr1[0], arr2[0])

if chOrd == 0:
    if num2d == numVer: 
        #2d and Pv Changing
        arr1 = Match1(num2d, numPv, z2d, zPv)
        arr2 = Match2(num2d, numPv, z2d, zPv)
        tPv = change(arr1[0], arr2[0], tPv)
        xPv = change(arr1[0],  arr2[0],  xPv)
        yPv = change(arr1[0],  arr2[0],  yPv)
        zPv = change(arr1[0],  arr2[0],  zPv)
        errxPv = change(arr1[0], arr2[0], errxPv)
        erryPv = change(arr1[0], arr2[0], erryPv)
        errzPv = change(arr1[0], arr2[0], errzPv)
        trPv = change(arr1[0], arr2[0], trPv)
        if numPv>2:
            tPv = change2(arr1[1], arr2[1], tPv, arr1[0], arr2[0])
            xPv = change2(arr1[1],  arr2[1], xPv, arr1[0], arr2[0])
            yPv = change2(arr1[1],  arr2[1], yPv, arr1[0], arr2[0])
            zPv = change2(arr1[1],  arr2[1], zPv, arr1[0], arr2[0])
            errxPv = change2(arr1[1], arr2[1], errxPv, arr1[0], arr2[0])
            erryPv = change2(arr1[1], arr2[1], erryPv, arr1[0], arr2[0])
            errzPv = change2(arr1[1], arr2[1], errzPv, arr1[0], arr2[0])
            trPv = change2(arr1[1], arr2[1], trPv, arr1[0], arr2[0])

        #2d and Hl Changing
        arr1 = Match1(num2d, numHl, z2d, zHl)
        arr2 = Match2(num2d, numHl, z2d, zHl)
        tHl = change(arr1[0], arr2[0], tHl)
        xHl = change(arr1[0],  arr2[0],  xHl)
        yHl = change(arr1[0],  arr2[0],  yHl)
        zHl = change(arr1[0],  arr2[0],  zHl)
        errxHl = change(arr1[0], arr2[0], errxHl)
        erryHl = change(arr1[0], arr2[0], erryHl)
        errzHl = change(arr1[0], arr2[0], errzHl)
        if numHl>2:
            tHl = change2(arr1[1], arr2[1], tHl, arr1[0], arr2[0])
            xHl = change2(arr1[1],  arr2[1],  xHl, arr1[0], arr2[0])
            yHl = change2(arr1[1],  arr2[1],  yHl, arr1[0], arr2[0])
            zHl = change2(arr1[1],  arr2[1],  zHl, arr1[0], arr2[0])
            errxHl = change2(arr1[1], arr2[1], errxHl, arr1[0], arr2[0])
            erryHl = change2(arr1[1], arr2[1], erryHl, arr1[0], arr2[0])
            errzHl = change2(arr1[1], arr2[1], errzHl, arr1[0], arr2[0])

        #2d and Mc Changing
        arr1 = Match1(num2d, numMc, z2d, zMc)
        arr2 = Match2(num2d, numMc, z2d, zMc)
        tMc = change(arr1[0], arr2[0], tMc)
        xMc = change(arr1[0],  arr2[0],  xMc)
        yMc = change(arr1[0],  arr2[0],  yMc)
        zMc = change(arr1[0],  arr2[0],  zMc)
        if numMc>2:
            tMc = change2(arr1[1], arr2[1], tMc, arr1[0], arr2[0])
            xMc = change2(arr1[1],  arr2[1],  xMc, arr1[0], arr2[0])
            yMc = change2(arr1[1],  arr2[1],  yMc, arr1[0], arr2[0])
            zMc = change2(arr1[1],  arr2[1],  zMc, arr1[0], arr2[0])
if chOrd == 0:
    if numHl == numVer:
    
        #Hl and Pv Changing
        arr1 = Match1(numHl, numPv, zHl, zPv)
        arr2 = Match2(numHl, numPv, zHl, zPv)
        tPv = change(arr1[0], arr2[0], tPv)
        xPv = change(arr1[0],  arr2[0],  xPv)
        yPv = change(arr1[0],  arr2[0],  yPv)
        zPv = change(arr1[0],  arr2[0],  zPv)
        errxPv = change(arr1[0], arr2[0], errxPv)
        erryPv = change(arr1[0], arr2[0], erryPv)
        errzPv = change(arr1[0], arr2[0], errzPv)
        trPv = change(arr1[0], arr2[0], trPv)
        if numPv>2:
            tPv = change2(arr1[1], arr2[1], tPv, arr1[0], arr2[0])
            xPv = change2(arr1[1],  arr2[1],  xPv, arr1[0], arr2[0])
            yPv = change2(arr1[1],  arr2[1],  yPv, arr1[0], arr2[0])
            zPv = change2(arr1[1],  arr2[1],  zPv, arr1[0], arr2[0])
            errxPv = change2(arr1[1], arr2[1], errxPv, arr1[0], arr2[0])
            erryPv = change2(arr1[1], arr2[1], erryPv, arr1[0], arr2[0])
            errzPv = change2(arr1[1], arr2[1], errzPv, arr1[0], arr2[0])
            trPv = change2(arr1[1], arr2[1], trPv, arr1[0], arr2[0])

        #Hl and 2d Changing
        arr1 = Match1(numHl, num2d, zHl, z2d)
        arr2 = Match2(numHl, num2d, zHl, z2d)
        t2d = change(arr1[0], arr2[0], t2d)
        x2d = change(arr1[0],  arr2[0],  x2d)
        y2d = change(arr1[0],  arr2[0],  y2d)
        z2d = change(arr1[0],  arr2[0],  z2d)
        errx2d = change(arr1[0], arr2[0], errx2d)
        erry2d = change(arr1[0], arr2[0], erry2d)
        errz2d = change(arr1[0], arr2[0], errz2d)
        if num2d>2:
            t2d = change2(arr1[1], arr2[1], t2d, arr1[0], arr2[0])
            x2d = change2(arr1[1],  arr2[1],  x2d, arr1[0], arr2[0])
            y2d = change2(arr1[1],  arr2[1],  y2d, arr1[0], arr2[0])
            z2d = change2(arr1[1],  arr2[1],  z2d, arr1[0], arr2[0])
            errx2d = change2(arr1[1], arr2[1], errx2d, arr1[0], arr2[0])
            erry2d = change2(arr1[1], arr2[1], erry2d, arr1[0], arr2[0])
            errz2d = change2(arr1[1], arr2[1], errz2d, arr1[0], arr2[0])
        
        #Hl and Mc Changing
        arr1 = Match1(numHl, numMc, zHl, zMc)
        arr2 = Match2(numHl, numMc, zHl, zMc)
        tMc = change(arr1[0], arr2[0], tMc)
        xMc = change(arr1[0],  arr2[0],  xMc)
        yMc = change(arr1[0],  arr2[0],  yMc)
        zMc = change(arr1[0],  arr2[0],  zMc)
        if numMc>2:
            tMc = change2(arr1[1], arr2[1], tMc, arr1[0], arr2[0])
            xMc = change2(arr1[1],  arr2[1],  xMc, arr1[0], arr2[0])
            yMc = change2(arr1[1],  arr2[1],  yMc, arr1[0], arr2[0])
            zMc = change2(arr1[1],  arr2[1],  zMc, arr1[0], arr2[0])


if chOrd == 0:
    if numMc == numVer: 
        #Mc and Pv Changing
        arr1 = Match1(numMc, numPv, zMc, zPv)
        arr2 = Match2(numMc, numPv, zMc, zPv)
        tPv = change(arr1[0], arr2[0], tPv)
        xPv = change(arr1[0], arr2[0], xPv)
        yPv = change(arr1[0], arr2[0], yPv)
        zPv = change(arr1[0], arr2[0], zPv)
        errxPv = change(arr1[0], arr2[0], errxPv)
        erryPv = change(arr1[0], arr2[0], erryPv)
        errzPv = change(arr1[0], arr2[0], errzPv)
        trPv = change(arr1[0], arr2[0], trPv)
        if numPv>1:         
            tPv = change2(arr1[1], arr2[1], tPv, arr1[0], arr2[0])
            xPv = change2(arr1[1], arr2[1], xPv, arr1[0], arr2[0])
            yPv = change2(arr1[1], arr2[1], yPv, arr1[0], arr2[0])
            zPv = change2(arr1[1], arr2[1], zPv, arr1[0], arr2[0])
            errxPv = change2(arr1[1], arr2[1], errxPv, arr1[0], arr2[0])
            erryPv = change2(arr1[1], arr2[1], erryPv, arr1[0], arr2[0])
            errzPv = change2(arr1[1], arr2[1], errzPv, arr1[0], arr2[0])
            trPv = change2(arr1[1], arr2[1], trPv, arr1[0], arr2[0])

        #Mc and 2d Changing
        arr1 = Match1(numMc, num2d, zMc, z2d)
        arr2 = Match2(numMc, num2d, zMc, z2d)   
        t2d = change(arr1[0], arr2[0], t2d)
        x2d = change(arr1[0], arr2[0], x2d)
        y2d = change(arr1[0], arr2[0], y2d)
        z2d = change(arr1[0], arr2[0], z2d)
        errx2d = change(arr1[0], arr2[0], errx2d)
        erry2d = change(arr1[0], arr2[0], erry2d)
        errz2d = change(arr1[0], arr2[0], errz2d)
        if num2d>1:
            t2d = change2(arr1[1], arr2[1], t2d, arr1[0], arr2[0])
            x2d = change2(arr1[1], arr2[1], x2d, arr1[0], arr2[0])
            y2d = change2(arr1[1], arr2[1], y2d, arr1[0], arr2[0])
            z2d = change2(arr1[1], arr2[1], z2d, arr1[0], arr2[0])
            errx2d = change2(arr1[1], arr2[1], errx2d, arr1[0], arr2[0])
            erry2d = change2(arr1[1], arr2[1], erry2d, arr1[0], arr2[0])
            errz2d = change2(arr1[1], arr2[1], errz2d, arr1[0], arr2[0])

        #Mc and Hl Changing
        arr1 = Match1(numMc, numHl, zMc, zHl)
        arr2 = Match2(numMc, numHl, zMc, zHl)
        tHl = change(arr1[0], arr2[0], tHl)
        xHl = change(arr1[0], arr2[0], xHl)
        yHl = change(arr1[0], arr2[0], yHl)
        zHl = change(arr1[0], arr2[0], zHl)
        errxHl = change(arr1[0], arr2[0], errxHl)
        erryHl = change(arr1[0], arr2[0], erryHl)
        errzHl = change(arr1[0], arr2[0], errzHl)
        if numHl>1:
            tHl = change2(arr1[1], arr2[1], tHl, arr1[0], arr2[0])
            xHl = change2(arr1[1], arr2[1], xHl, arr1[0], arr2[0])
            yHl = change2(arr1[1], arr2[1], yHl, arr1[0], arr2[0])
            zHl = change2(arr1[1], arr2[1], zHl, arr1[0], arr2[0])
            errxHl = change2(arr1[1], arr2[1], errxHl, arr1[0], arr2[0])
            erryHl = change2(arr1[1], arr2[1], erryHl, arr1[0], arr2[0])
            errzHl = change2(arr1[1], arr2[1], errzHl, arr1[0], arr2[0])                
#---------Start of Main Method--------------------------------------
print '*',(106*(' ')),'*'
print '*',(106*(' ')),'*'
if numVer == 0:
    print '* There are no vertices in this event',(70*(' ')),'*'

if numVer > 0:
    print '* %-1s %-7s %-2s %-2s %-1s %-2s %-7s %-2s %-2s %-10s %-7s %-1s %-2s %-2s %-34s   *'  % (numPv, 'Primary',vPv,',', num2d, '2D',v2d,',', numHl, 'Hlt Generic',vHl,', ', numMc, 'MC', vMc) 
    for k in range(0, numVer):
        print '*',(106*(' ')),'*'
        print '*',(106*(' ')),'*'
        print '* Vertex',k,(97*(' ')),'*'
        print '* ~~~~~~~~',(97*(' ')),'*'
        print '*                      Primary Vertex      2d Vertex      Hlt Generic Vertex     MC Vertex                   *'     
        print '*                      ^^^^^^^^^^^^^^      ^^^^^^^^^      ^^^^^^^^^^^^^^^^^^     ^^^^^^^^^                   *'    
        print '%-24s %-19s %-15s %-19s %-27s *'  % ('* Vertex Type   =>', tPv[k], t2d[k], tHl[k], tMc[k])                        
        print '%-24s %-19s %-15s %-19s %-27s *'  % ('* X Co-ordinate =>', str(xPv[k])[:6], str(x2d[k])[:6], str(xHl[k])[:6], str(xMc[k])[:6])                        
        print '%-24s %-19s %-15s %-19s %-27s *'  % ('* Y Co-ordinate =>', str(yPv[k])[:6], str(y2d[k])[:6], str(yHl[k])[:6], str(yMc[k])[:6])                        
        print '%-24s %-19s %-15s %-19s %-27s *'  % ('* Z Co-ordinate =>', str(zPv[k])[:6], str(z2d[k])[:6], str(zHl[k])[:6], str(zMc[k])[:6])                        
        print '%-24s %-19s %-15s %-19s %-27s *'  % ('* Error in x    =>', str(errxPv[k])[:6], str(errx2d[k])[:6], str(errxHl[k])[:6], '-')                        
        print '%-24s %-19s %-15s %-19s %-27s *'  % ('* Error in y    =>', str(erryPv[k])[:6], str(erry2d[k])[:6], str(erryHl[k])[:6], '-')
        print '%-24s %-19s %-15s %-19s %-27s *'  % ('* Error in z    =>', str(errzPv[k])[:6], str(errz2d[k])[:6], str(errzHl[k])[:6], '-')                                                
        print '%-24s %-83s *'  % ('* Nb of Tracks  =>', str(trPv[k])[:6])                                                

print '*',(106*(' ')),'*'
print (110*('*'))
