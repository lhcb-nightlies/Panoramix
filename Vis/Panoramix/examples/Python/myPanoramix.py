#Panoramix configuration script
import sys,getopt,os,time,subprocess
#---Enable Tab completion-----------------------------------------
try:
  import rlcompleter, readline
  readline.parse_and_bind( 'tab: complete' )
  readline.parse_and_bind( 'set show-all-if-ambiguous On' )
except:
  pass
#----------------------------------------------------------------------------------
def myPanoramix_usage() :
  print 'Usage:'
  print ' python myPanoramix.py [--outputlevel] <outputlevel> [--DataType] [--Full_rec] [--Phys] -u <User_opts> -f <User_file>  -x <xml_catalogue> [-c] [-h] [--L0]'
  print '   or                  [-o]            <outputlevel> [-D]    [-F]         [-H]    [-P]     -u <User_opts> -f <User_file>  -x <xml_catalogue> [-c] [-h]'
#----------------------------------------------------------------------------------
def myPanoramix_help() :
 myPanoramix_usage()
 print 'Example cases:'
 print ' python myPanoramix.py                         : read default Dst'
 print ' python myPanoramix.py    -f myFile.dst        : read myFile.dst   database tags taken from Rec/Header '
 print ' python myPanoramix.py    -f myFile.dst  -L    : read myFile.dst   latest global database tags for dataype taken from Rec/Header '
 print ' python myPanoramix.py    -f myFile.raw        : read raw data'
 print ' python myPanoramix.py -F -f myFile.dst        : run full reconstruction on myFile.dst' 
 print ' python myPanoramix.py -f none                 : no event file, only detector geometry'
 print ' python myPanoramix.py --BareConfig  1         : start with bare configuration for GUI only, read DST'
 print ' python myPanoramix.py --Cosmics -f myFile.raw  : run reconstruction for cosmics on raw data' 
 print ' python myPanoramix.py --SingleBeam             : run reconstruction with field off / Velo open' 
 print ' python myPanoramix.py --FirstCollisions       : run reconstruction with field on / Velo open' 
 print ' python myPanoramix.py -c                      : overwrite default configuration with myPanoramixConfig.py'
 print ' python myPanoramix.py -c anotherConfig        : overwrite default configuration with anotherConfig.py'
 print ' python myPanoramix.py -u  user.opts           : use an user option file loaded last'
 print ' python myPanoramix.py --qt                    : use Qt for graphics, now default'
 print ' python myPanoramix.py --om                    : use OpenMotif for graphics'

 print '-----------------------------------------------------------------------------'
 print 'Example configuration file:' 
 print ' (values will be overwritten by options specified on command line)'
 print 'Full_rec  = False'               
 print 'Hlt       = True'            
 print 'Dst       = True' 
 print 'Phys      = False'
 print 'User_opts = '''
 print 'User_file = \'$PANORAMIXDATA/Bs2JpsiPhi_MC09.dst\''
 print 'xml_catalogue  = '''
 print '-----------------------------------------------------------------------------'
 print 'The last used configuration is stored in last_settings.py ' 
#----------------------------------------------------------------------------------

config = {}
config['Full_rec']      = False               
config['Hlt']           = False            
config['L0']            = False            
config['Phys']          = False
config['User_opts']     = ''
config['User_opts_first'] = ''
config['User_file']     = ['$PANORAMIXDATA/Sel_Bsmumu_2highest.dst']
config['xml_catalogue'] = ''
config['User_config']   = ''
config['outputlevel']   = '4'
config['qt']            = True
config['om']            = False
config['Cosmics']       = False
config['SingleBeam']    = False
config['DataType']      = None
config['FEST']          = False
config['BareConfig']    = 0
config['latestTag']     = False
config['simulation']    = False
config['AutoPilot']     = False
config['Upgrade']       = False

try:
        opts, args = getopt.getopt(sys.argv[1:], "o:D:FHPu:f:x:c:hqv:sl:A", 
        ["outputlevel=","DataType=", "Fu/LHCb_data/00004827_00000033_1.dstll_rec", "Phys","User_opts=","User_file=",
         "xml_catalogue=","User_config=","help","qt","om","User_opts_first=","Cosmics","SingleBeam","FirstCollisions",
         "FEST","tcmalloc","BareConfig="])
except getopt.GetoptError:
        # print help information and exit:
        myPanoramix_usage()
        print '  Try "myPanoramix.py  -h" for more information.'  
        sys.exit()
for o, a in opts:
        if o in ("-c", "--User_config="):
            User_config = 'myPanoramixConfig.py'
            if a : User_config = a
            if User_config.find('.py') < 0 : User_config+='.py'
            config['User_config']  = User_config 
        if o in ("-h", "--help"):
            myPanoramix_help()
            sys.exit()

if config['User_config'] : 
# get configuration from a user file
  f=file(config['User_config'])
  all = f.readlines()
  f.close()
  for l in all : 
     sl = l.split('=')
     if len(sl) > 1 :
       key = sl[0].split(' ')[0]
       value = sl[1].replace('\n','')
       value = value.replace(' ','')
       value = value.replace("'","")
       if value == 'False' : value = False
       if value == 'True'  : value = True
       try : 
         x = config[key]
       except : 
         print 'myPanoramix config: key unknown',key,value
         sys.exit()
       config[key] = value

for o, a in opts:
        if o in ("-o", "--outputlevel"):
# set Output Level: 2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL         
            xx = 4 
            if type(a) == type('a'):
             if a.lower()=='verbose'  : xx = 1
             if a.lower()=='debug'    : xx = 2
             if a.lower()=='info'     : xx = 3
             if a.lower()=='warning'  : xx = 4
             if a.lower()=='error'    : xx = 5                   
            elif type(a) == type(1) : xx = int(a)
            config['outputlevel'] = xx
        if o in ("-u", "--User_opts="):
            config['User_opts'] = a
        if o in ("--BareConfig="):
            config['BareConfig'] = int(a)
        if o in ("-v", "--User_opts_first="):
            config['User_opts_first'] = a
        if o in ("-f", "--User_file="):
            config['User_file']=[]
            if a.find(',') :
             tmp = a.split(',')
            else :
             tmp = [a]   
            for y in tmp:
              x = y.replace(' ','') 
              if not x.find('eos')<0 and x.find('eoslhcb')<0: x="root://eoslhcb.cern.ch/"+x
              if x.find('PFN')==-1 and x.lower() != 'none' and x.lower()!='online': 
                config['User_file'].append('PFN:'+x)
              else : 
                config['User_file'].append(a)
        if o in ("-D", "--DataType"):
            if a.find('MC')>-1: config['simulation']  = True
            ab = a.replace('MC','')
            config['DataType'] = ab.replace('L','')
        if o in ("-L", "--LatestTag"):
            config['latestTag'] = True
        if o in ("-F", "--Full_rec"):
            config['Full_rec'] = True
        if o in ("-H", "--Hlt"):
            config['Hlt'] = False
            print 'Hlt option not anymore supported !'  
        if o in ("--L0"):
            config['L0'] = True
        if o in ("--Cosmics"):
            config['Cosmics'] = True
        if o in ("--SingleBeam","--FirstCollisions"):
            if o.find('First')==-1 :
             config['SingleBeam'] = True
            else :
             config['FirstCollisions'] = True
            config['Full_rec']    = True
        if o in ("-P", "--Phys"):
            if a == '': 
              config['Phys'] = True
            else : 
              config['Phys'] = False
        if o in ("-x", "--xml_catalogue="):
            config['xml_catalogue'] = a
        if o in ("-q", "--qt"):
            config['qt'] = True
        if o == "--om":
            config['om'] = True
            config['qt'] = False
        if o == "--FEST" :
            config['FEST'] = True
        if o == "-A" :
            config['AutoPilot'] = True
# do something with ROOT first
# this is a very nasty hack to be able to use Qt
if config['qt']:
     os.putenv('ROOTENV_NO_HOME','1') 
     try:
          tmp = open('.rootrc','r') 
          text = tmp.readlines()
          found = False
          for line in text:
              zline = line.replace(' ','') 
              if zline.find("X11.XInitThread:no")>-1 : 
                  found = True
                  break
          if not found :
             print 'Error: Using Qt requires to have "X11.XInitThread: no" in your .rootrc file'
             print '       otherwise problem with multihreading, ROOT, GAUDI, Qt'
             print 'Error: Panoramix does not know how to continue. Stop here'          
             exit()
     except IOError:
          tmp = open('.rootrc','w') 
          tmp.write("X11.XInitThread: no \n")
          tmp.close()
          from ROOT import TBrowser
          os.system('rm .rootrc')  

a  = time.time()
aa = time.clock()

from ROOT import TH1F, TBrowser 

# start Panoramix configurable
from Configurables import LHCbApp,CondDB
from LHCbConfig import addDBTags

lhcbApp  = LHCbApp()
lhcbApp.DataType     = '2015'
lhcbApp.Persistency  = 'MDF'  
lhcbApp.DDDBtag   = "default"
lhcbApp.CondDBtag = "default"

if config['DataType']=='online':
 CondDB().IgnoreHeartBeat         = True 
 CondDB().EnableRunChangeHandler  = False
 CondDB().EnableRunStampCheck = False
 config['DataType'] = '2016'
dt='' 
for f in config['User_file']: 
  if not f.lower().find('online') < 0 : 
    lhcbApp.DataType     = '2016'
    lhcbApp.Persistency  = 'MDF'  
    lhcbApp.DDDBtag   = "default"
    lhcbApp.CondDBtag = "default"
  elif config['User_file'][0] !='none' and not config['DataType']:
    print '####',config['User_file']
    fn = f.replace('PFN:','')
    try: dt = addDBTags(fn)
    except: print "extraction of database tags did not worked, use default"  
if config['DataType']: 
  lhcbApp.DataType = config['DataType'] 
  CondDB().LatestGlobalTagByDataType = config['DataType'] 
if dt=='Upgrade' : 
  config['Upgrade'] = True 
  config['DataType'] = 'Upgrade' 
  print '+++ Configure Panoramix for Upgrade geometries.'
# for what is this ? If used earlier, causes problem with subprocess
os.environ['PYTHONINSPECT'] = '1'

## apply some last-minutes fixes
import PanoramixSys.Fixes
import LHCbMath.Types
LHCbMath = LHCbMath.Types.Gaudi
from PanoramixSys.Configuration import *

# now import user opts first
if len(config['User_opts_first'])>0 : 
         if type(config['User_opts_first']) == type([]) : 
          for o in config['User_opts_first'] : importOptions(o)
         else : 
          importOptions(config['User_opts_first'])
    
# configure the logging
if config['outputlevel'] < 4 : 
 import logging
 from GaudiKernel.ProcessJobOptions import InstallRootLoggingHandler
 level = logging.INFO
 if config['outputlevel'] < 3 : level = logging.DEBUG
 if config['outputlevel'] < 2 : level = logging.VERBOSE
 InstallRootLoggingHandler("# ", level = level)
 root_logger = logging.getLogger()

#translate config to configuration slots
for c in config:
  if config[c]: PanoramixSys().setProp(c,config[c])
  if c == 'Cosmics' : 
    if config[c] : PanoramixSys().setProp('Cosmics',True)
  if config['outputlevel'] < 4 : 
    print 'conf copy: ',c,':',PanoramixSys().getProp(c)
  
# some recent annoyance with ROOT, causes long wait when typing wrong command
try:
 if os.uname()[3].find('Ubuntu')>-1:
   sys.excepthook = sys.__excepthook__
except:
 rc = 'OK' 

# now import user opts 
def user_changes():
  if len(config['User_opts'])>0 : 
         if type(config['User_opts']) == type([]) : 
          for o in config['User_opts'] : importOptions(o)
         else : 
          importOptions(config['User_opts'])

ol = int(config['outputlevel'])
appConf = ApplicationMgr(OutputLevel = ol, AppName = 'Panoramix')

# for rDSTs
rDST = False
for f in config['User_file'] : 
 if f.lower().find('.rdst') > -1 :
   rDST = True

from Configurables import ( RawBankToSTClusterAlg, RawBankToSTLiteClusterAlg)

def doTAEChanges():
 RawBankToSTClusterAlg("CreateITClusters").rawEventLocation = 'Next1/DAQ/RawEvent'
 RawBankToSTLiteClusterAlg("CreateITLiteClusters").rawEventLocation = 'Next1/DAQ/RawEvent'
 RawBankToSTClusterAlg("CreateTTClusters").rawEventLocation = 'Prev1/DAQ/RawEvent'
 RawBankToSTLiteClusterAlg("CreateTTLiteClusters").rawEventLocation = 'Prev1/DAQ/RawEvent'

def stopNuisanceOutput():
# started with Packages below DaVinci v34r0
 xx = ['CaloPIDsCaloPIDsForCaloProcessor','CaloTrackMatch','InECAL','ClusterMatch','ChargedPIDsCaloPIDsForCaloProcessor',
     'NeutralPIDsCaloPIDsForCaloProcessor','InCaloAcceptance','CaloMatch','CaloEnergy','CaloChi2','CaloDLLe',
     'CaloDLLmu','InHCAL','InBREM','InSPD','InPRS','ElectronMatch','BremMatch','EcalE','HcalE',
     'SpdE','PrsE','EcalChi22ID','BremChi22ID','ClusChi22ID','EcalPIDe','BremPIDe','HcalPIDe',
     'PrsPIDe','EcalPIDmu','HcalPIDmu','PhotonID','MergedID','PhotonFromMergedID']
 from Configurables import MessageSvc
 from Configurables import (InEcalAcceptanceAlg,PhotonMatchAlg,ElectronMatchAlg,BremMatchAlg,
                           InHcalAcceptanceAlg,InSpdAcceptanceAlg,InPrsAcceptanceAlg,InBremAcceptanceAlg,
                           Track2PrsEAlg,Track2SpdEAlg,Track2EcalEAlg,Track2HcalEAlg,EcalChi22ID,BremChi22ID,ClusChi22ID,
                           PrsPIDeAlg,BremPIDeAlg, EcalPIDeAlg,HcalPIDeAlg,EcalPIDmuAlg,HcalPIDmuAlg,CaloPhotonIdAlg)
 msg = MessageSvc()
 msg.setError += xx  # recipe from Vanya, but did not work
 GaudiSequencer('CaloPIDsCaloPIDsForCaloProcessor').OutputLevel = ol
 GaudiSequencer('ChargedPIDsCaloPIDsForCaloProcessor').OutputLevel = ol
 GaudiSequencer('NeutralPIDsCaloPIDsForCaloProcessor').OutputLevel = ol
 GaudiSequencer('InCaloAcceptance').OutputLevel = ol
 GaudiSequencer('CaloMatch').OutputLevel = ol
 GaudiSequencer('CaloEnergy').OutputLevel = ol
 GaudiSequencer('CaloChi2').OutputLevel = ol
 GaudiSequencer('CaloDLLe').OutputLevel = ol
 GaudiSequencer('CaloDLLmu').OutputLevel = ol
 GaudiSequencer('CaloTrackMatch').OutputLevel = ol
 InEcalAcceptanceAlg('InECAL').OutputLevel = ol
 InHcalAcceptanceAlg('InHCAL').OutputLevel = ol
 InSpdAcceptanceAlg('InSPD').OutputLevel = ol
 InPrsAcceptanceAlg('InPRS').OutputLevel = ol
 InBremAcceptanceAlg('InBREM').OutputLevel = ol
 PhotonMatchAlg('ClusterMatch').OutputLevel = ol
 ElectronMatchAlg('ElectronMatch').OutputLevel = ol
 BremMatchAlg('BremMatch').OutputLevel = ol
 Track2SpdEAlg('SpdE').OutputLevel = ol
 Track2PrsEAlg('PrsE').OutputLevel = ol
 Track2EcalEAlg('EcalE').OutputLevel = ol
 Track2HcalEAlg('HcalE').OutputLevel = ol
 EcalChi22ID().OutputLevel = ol
 BremChi22ID().OutputLevel = ol
 ClusChi22ID().OutputLevel = ol
 PrsPIDeAlg('PrsPIDe').OutputLevel = ol
 BremPIDeAlg('BremPIDe').OutputLevel = ol
 EcalPIDeAlg('EcalPIDe').OutputLevel = ol
 HcalPIDeAlg('HcalPIDe').OutputLevel = ol
 EcalPIDmuAlg('EcalPIDmu').OutputLevel = ol
 HcalPIDmuAlg('HcalPIDmu').OutputLevel = ol
 CaloPhotonIdAlg('PhotonID').OutputLevel = ol
 CaloPhotonIdAlg('MergedID').OutputLevel = ol
 CaloPhotonIdAlg('PhotonFromMergedID').OutputLevel = ol

appendPostConfigAction(stopNuisanceOutput)

def removeLoKiMessage():
 from Configurables import LoKiSvc
 LoKiSvc().Welcome = False
 
appendPostConfigAction(removeLoKiMessage)
if len(config['User_opts'])>0 : appendPostConfigAction(user_changes)

# if config['SingleBeam'] : appendPostConfigAction(doTAEChanges)

# Use TimingAuditor for timing, suppress printout from SequencerTimerTool
from Configurables import (AuditorSvc,SequencerTimerTool)
appConf.ExtSvc += [ 'ToolSvc', 'AuditorSvc' ]
appConf.AuditAlgorithms = True
AuditorSvc().Auditors += [ 'TimingAuditor' ] 
SequencerTimerTool().OutputLevel = INFO

import GaudiPython
# now get ROOT stuff, if done before, it screws up the getopts
# from ROOT import TCanvas,TH1F,TH2F,TBrowser,gROOT, gSystem

appMgr = GaudiPython.AppMgr()
appMgr.initialize()

from panoramixmodule import *

ufile  = PanoramixSys().getProp('User_file')
mdf    = PanoramixSys().getProp('Mdf')
online = PanoramixSys().getProp('Online')
if len(ufile) > 0 :
 if ufile[0].lower() != 'none' and ufile[0].lower() != 'default'and not online :
  sel.open(ufile)
appMgr.start()

# at exit enable all algorithms for proper finalize
def _my_Action_() :    
 from GaudiPython.Bindings import AppMgr 
 appMgr = AppMgr()  
 print '>>>> myPanoramix exit handler, enable all algorithms for proper Gaudi finalize '  
 try:
  for a in appMgr.algorithms() : 
   appMgr.algorithm(a).Enable = True
 except:
  print 'somebody killed the appMgr'  
 #appMgr.algorithm('GaudiSequencer/CaloRecoSeq').finalize()
import atexit
if not config['Full_rec'] : atexit.register ( _my_Action_ )

# stop decoding of Calo, wait for ondemand:
if "DigiSeq" in appMgr.algorithms(): appMgr.algorithm("DigiSeq").Enable = False 
# don't strip off states by default
if "OutputDSTSeq"           in appMgr.algorithms(): appMgr.algorithm("OutputDSTSeq").Enable      = False
if "PanoDstWriter"          in appMgr.algorithms(): appMgr.algorithm("PanoDstWriter").Enable     = False
if "PanoEventSequence"      in appMgr.algorithms(): appMgr.algorithm("PanoEventSequence").Enable = False
if "Pano_HltTrackConverter" in appMgr.algorithms(): appMgr.algorithm("Pano_HltTrackConverter").Enable = False

if not config['Full_rec'] :
# disable reconstruction sequence by default
   if "PanoRecoSequencer" in appMgr.algorithms(): appMgr.algorithm("PanoRecoSequencer").Enable  = False
   if "CleanUpSequence" in appMgr.algorithms(): appMgr.algorithm("CleanUpSequence").Enable = False
if not config['Phys'] :
  if "PhysSeq" in appMgr.algorithms(): appMgr.algorithm("PhysSeq").Enable  = False

# Panoramix will probably never run with files on the GRID
# disabling avoids error message about not able to load GFale
appMgr.service('IODataManager').UseGFAL = False

# try to load dictionary for FSR
GaudiPython.loaddict('LumiEvent')

#print some information about the session configuration :
if config['User_opts'] =='' : config['User_opts'] = 'None'
print '--> Your settings: DataType =',config['DataType'],'| Full reconstruction =', config['Full_rec'],'| Hlt =',config['Hlt'],'| Phys =',config['Phys'],'| User options =',config['User_opts']
print '-->                          '
# get input files:
flist = sel.properties()['Input'].value()
txt = []
if len(flist) > 0 : 
 if len(flist)==1 : 
   txt.append('--> input file: ')
 else : 
   txt.append('--> input files: ')   
for f in flist : 
 print '%s'%(f)
 
if config['User_opts'] =='None' : config['User_opts'] = ''

#store last configuration
f=file('last_settings.py','w')
for i in config : 
 if type(config[i]) == type(True) : 
   if config[i]     : f.write(i + '=' + 'True'+'\n')
   if not config[i] : f.write(i + '=' + 'False'+'\n')
 elif type(config[i]) == type([]) : 
   first = True
   for k in config[i] : 
     if first : 
       first = False
       f.write(i + '=' + k+'\n')
     else : 
       f.write(',' + k+'\n')        
 else:    
   if type(config[i]) != type('s') :  
    f.write(i + '=' + str(config[i])+'\n')
   else :
    f.write(i + '=' + config[i]+'\n')
f.close()

from Configurables import OnXSvc

if OnXSvc().File.find('Online')!=-1 : 
 from OnlineSetup import *

b = time.time()
bb = time.clock()
print 'lost time in initialization',b-a,bb-aa
# Give control to the GUI :
toui()
 
