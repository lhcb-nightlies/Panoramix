
import pmx

hsvc = pmx.histSvc()

if hsvc == None:
  print 'gb_test_plot : HistogramSvc not found.'
else:
  print 'gb_test_plot : HistogramSvc found.'
  histo = hsvc.book("gb_test_plot_histo","Gaudi histo from Python",100,-5,5)
  if histo == None:
    print "gb_test_plot : can't create histogram."
  else:
    print 'gb_test_plot : histogram created.'
    print 'gb_test_plot : fill histogram...'
    import random
    r = random.Random()
    for I in range(0,10000):
      histo.fill(r.gauss(0,1),1)
    del r
    # Plot with Vis/SoStat.
    # The below assumes that the current region
    # in the Panoramix GUI is a SoPlotterRegion.
    # See "Help/Analysis" for the creation
    # and activation of such region.
    pmx.uiSvc().visualize(histo)
