                                 
print 'gb_test_mcdecayfinder : begin'

import gaudimodule

container = 'MC/Particles'
decaystr  = '[B0,B+,B_s0]cc'

import pmx
evtSvc = pmx.evtSvc()
if evtSvc == None:
  print 'gb_test_vis_mcparticle : EventDataSvc not found.'
else:
  print evtSvc

mc = gaudimodule.Helper.dataobject(evtSvc,'/Event/'+container)
if mc == None:
  print 'gb_test_mcdecayfinder : /Event/MC/Particles not found.'
else:
  print mc

  MCDecayFinder = pmx.getTool('MCDecayFinder','IMCDecayFinder')
  print MCDecayFinder

  print 'gb_test_mcdecayfinder : MCDecayFinder.setDecay...'
  MCDecayFinder.setDecay(decaystr)

  print 'gb_test_mcdecayfinder : MCDecayFinder.hasDecay...'
  if MCDecayFinder.hasDecay(mc) :
    print 'gb_test_mcdecayfinder : has decay'
