
from Panoramix import *

#
# We define a cutting plan.
# In the cutting plan we define a grid of cells.
# The magnetic field is computed at the center of each cell.
# The cell is colored according field strength.
#

#/////////////////////////////////////////////////////////////////////////////
# Coloring parameters :
#  A red coloring gradient is applied.
#/////////////////////////////////////////////////////////////////////////////
session.setParameter('modeling.magneticField.min','0.0001')
session.setParameter('modeling.magneticField.max','0.003')
session.setParameter('modeling.magneticField.colorMin','0.3')
session.setParameter('modeling.magneticField.colorMax','1')
#session.setParameter('modeling.transparency','0.3')

#/////////////////////////////////////////////////////////////////////////////
# Cut plan in the XY plan. Cover the magnet.
#/////////////////////////////////////////////////////////////////////////////
nx = 300  # Number of cell in x axis of the grid in the plan.
ny = 225  # Number of cell in y axis of the grid in the plan.
sx = 40 # Size of a cell in x.
sy = 40 # Size of a cell in y.

posx = -(sx * nx)/2
posy = -(sy * ny)/2
posz = 4000

session.setParameter('modeling.magneticField.gridPosition','%g %g %g' % (posx,posy,posz))
session.setParameter('modeling.magneticField.gridX','1 0 0')
session.setParameter('modeling.magneticField.gridY','0 1 0')
session.setParameter('modeling.magneticField.gridCells','%d %d' % (nx,ny))
session.setParameter('modeling.magneticField.gridCellSize','%g %g' % (sx,sy))

data_collect('MagneticField')
data_visualize()

#/////////////////////////////////////////////////////////////////////////////
# Cut plan in the ZY plan. Cover the magnet.
#/////////////////////////////////////////////////////////////////////////////
nx = 250
ny = 225
posx = 0
posy = -(sy * ny)/2.
posz = 0

session.setParameter('modeling.magneticField.gridPosition','%g %g %g' % (posx,posy,posz))
session.setParameter('modeling.magneticField.gridX','0 0 1')
session.setParameter('modeling.magneticField.gridY','0 1 0')
session.setParameter('modeling.magneticField.gridCells','%d %d' % (nx,ny))
session.setParameter('modeling.magneticField.gridCellSize','%g %g' % (sx,sy))

data_collect('MagneticField')
data_visualize()


