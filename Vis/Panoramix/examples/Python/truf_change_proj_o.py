from panoramixmodule import *
session().setParameter('modeling.projection','')
session().setParameter('modeling.what','no')
data_collect(da(),'SceneGraph(@current@)','highlight==true')
data_filter(da(),'name')

if ui().findWidget('Viewer_2d') or ui().findWidget('Viewer_3d') : 
  if ui().findWidget('Viewer_3d') :
     ui().setCurrentWidget(ui().findWidget('Viewer_3d'))           
     Page().setCurrentRegion(0)  
  else : 
   ui().setCurrentWidget(ui().findWidget('Viewer_2d'))        
   page = OnX.session().ui().currentPage()
   if page.getNumberOfRegions() == 2 :
    Page().setCurrentRegion(1)
   else :
    print 'this should not happen'
    
  session().setParameter('modeling.lineWidth','3') 
  Style().setColor('green')
  data_visualize(da())

  Style().setColor('blue')
  session().setParameter('modeling.what','MCParticle')
  data_collect(da(),'SceneGraph(@current@)','highlight==false')
  data_filter(da(),'name','Track*')
  data_visualize(da())
  
  Style().setColor('red')
  session().setParameter('modeling.what','clusters')
  data_collect(da(),'SceneGraph(@current@)','highlight==false')
  data_filter(da(),'name','Track*')
  data_visualize(da())
  
  Style().setColor('green')
  data_collect(da(),'SceneGraph(@current@)','highlight==false')
  data_filter(da(),'name','Track*')
  session().setParameter('modeling.what','OTMeasurements')
  data_visualize(da())
  
  data_collect(da(),'SceneGraph(@current@)','highlight==false')
  data_filter(da(),'name','Track*')
  session().setParameter('modeling.what','STMeasurements')
  data_visualize(da())

  data_collect(da(),'SceneGraph(@current@)','highlight==false')
  data_filter(da(),'name','Track*')
  session().setParameter('modeling.what','VeloMeasurements')
  data_visualize(da())

  Style().setColor('orange')
  data_collect(da(),'SceneGraph(@current@)','highlight==false')
  data_filter(da(),'name','MCParticle*')
  session().setParameter('modeling.what','Clusters')
  data_visualize(da())
      
  session().setParameter('modeling.what','no')

