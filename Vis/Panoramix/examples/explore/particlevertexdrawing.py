import GaudiPython
import ExploreDraw as D
import ExploreLib as L

def drawParticles():
	ui=D.ui()
	vertexloc = ui.parameterValue('cb_vertex.value')
	particleloc = ui.parameterValue('cb_particle.value')
	ptcut = float( ui.parameterValue('e_ptcut.value') )
	ipcut = float( ui.parameterValue('e_ipcut.value') )
	
	particles = L.filterParticles(particleloc, vertexloc, ptcut, ipcut)
	
	D.s_drawIDs(0)
	for p in particles:
		D.d_obj(p)
	D.s_drawIDs(1)

def drawVertices():
	ui=D.ui()
	vertexloc = ui.parameterValue('cb_vertex.value')
	vertices = D.evt[vertexloc]
	print 'Drawing', len(vertices), 'vertices'
	for v in vertices:
		D.d_obj(v)
