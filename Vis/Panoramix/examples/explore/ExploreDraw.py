from panoramixmodule import *
from math import acos, asin, atan2, sqrt
from ROOT import Double, TPython

import ExploreLib as L


#---------------------------------------------------
# General methods  
#---------------------------------------------------

def top():
	ui().exit()

def sels():
 for s in TTT.triggerSelectionNames('*'): print s

#---------------------------------------------------
# Style methods  
#---------------------------------------------------

def get_par(par):
	return OnX.session().parameterValue('modeling.'+par)

def get_pars():
	return OnX.session().availableParameters()

def set_bettertransparency():
	#not working. the propper way is to use SoGLRenderAction().setTransparencyType(), but not working in python
	topsep = OnX.session().ui().currentPage().currentRegion().getTopSeparator()
	transtype = iv.SoTransparencyType()
	transtype.value.setValue(1)
	topsep.addChild(transtype)	

def s_color(r, g=0, b=0):
	if type(r) == str:
		OnX.session().setParameter('modeling.color',r)
	else:
		OnX.session().setParameter('modeling.color','%g %g %g' % (r,g,b))	

def s_transparency(transp): # 0 - opaque, 1 - ghost
	OnX.session().setParameter('modeling.transparency','%g' % transp)	
	
def s_lw(width):
	OnX.session().setParameter('modeling.lineWidth','%d' % width)		

def s_markerSize(size):
	OnX.session().setParameter('modeling.markerSize','%d' % size)		

def s_textSize(size):
	OnX.session().setParameter('modeling.sizeText','%d' % size)		
	
def s_showText(opt):
	if opt: optbool = 'true'
	else: 	optbool = 'false'
	OnX.session().setParameter('modeling.showText',optbool)

def s_opened(opt=1):
	if opt: optbool = 'true'
	else: 	optbool = 'false'
	OnX.session().setParameter('modeling.opened',optbool)

def s_modeling(mod):	 
	if   mod == 's': modstr = 'solid'
	elif mod == 'w': modstr = 'wireFrame'
	elif mod == 'b': modstr = 'boundingBox'
	else: modstr=mod
	OnX.session().setParameter('modeling.modeling',modstr)

def s_showCurve(opt=1):
	if opt: optbool = 'true'
	else: 	optbool = 'false'
	OnX.session().setParameter('modeling.showCurve',optbool)	

def s_useExtrapolator(opt=1):
	if opt: optbool = 'true'
	else: 	optbool = 'false'
	OnX.session().setParameter('modeling.useExtrapolator',optbool)

def s_useVisSvc(opt=1):
	if opt: optbool = 'true'
	else: 	optbool = 'false'	
	OnX.session().setParameter('modeling.useVisSvc',optbool)		

def s_caloet(opt=1):
	if opt: 
		optbool = 'true'
		OnX.session().setParameter('CaloScale','150')		
		OnX.session().setParameter('CaloEtVis',optbool)	
	else: 	
		optbool = 'false'
		OnX.session().setParameter('CaloScale','30')
		OnX.session().setParameter('CaloEtVis',optbool)		

def s_ottimes(opt=1):
	if opt: 
		OnX.session().setParameter("modeling.OTRawWires", "false")
		OnX.session().setParameter("modeling.OTRawDriftTimes", "true")
	else: 	
		OnX.session().setParameter("modeling.OTRawWires", "true")
		OnX.session().setParameter("modeling.OTRawDriftTimes", "false")
	
def s_static(opt=1):
	if opt: 
		OnX.session().setParameter("modeling.Static", "true")
	else: 	
		OnX.session().setParameter("modeling.Static", "false")

def s_drawIDs(opt=1):
	if opt: 
		OnX.session().setParameter("modeling.drawIDs", "true")
	else: 	
		OnX.session().setParameter("modeling.drawIDs", "false")


#---------------------------------------------------
# OpenInventor + Coin methods  
#---------------------------------------------------

def so_colortransp(material):
	colors = OnX.smanip_torgbs(get_par('color'))	
	colors = colors.split(' ')	
	if len(colors) == 3:
		r = float( colors[0] )
		g = float( colors[1] )
		b = float( colors[2] )
		color = iv.SbColor(r,g,b)
	else:		
		r,g,b = COLORS[ colors[0] ]
		color = iv.SbColor(r,g,b)
	
	transp = get_par('transparency')
	if transp == '':
		transpvalue = 0
	else:
		transpvalue = float(transp)
	
	material.diffuseColor.setValue(color)
	material.transparency.setValue(transpvalue)

#---------------------------------------------------
def so_modeling(sodrawstyle):
	modeling = get_par('modeling')
	if modeling == 'solid':
		sodrawstyle.style.setValue(0)			
	elif modeling == 'wireFrame':
		sodrawstyle.style.setValue(1)
	lw = get_par('lineWidth')
	if lw == '': lw=1		
	sodrawstyle.lineWidth.setValue(float(lw))
	
#---------------------------------------------------
def so_line(xi,yi,zi,xf,yf,zf):
	static = get_par('Static')
	if static == 'true':
		scene = OnX.session().ui().currentPage().currentRegion().cast_SoDisplayRegion().getStaticScene()	
	else:
		scene = OnX.session().ui().currentPage().currentRegion().cast_SoDisplayRegion().getDynamicScene()	
	sep = iv.SoSeparator()
	
	# def points position
	coord = iv.SoCoordinate3()
	coord.point.set1Value(0,xi,yi,zi)
	coord.point.set1Value(1,xf,yf,zf)
	sep.addChild(coord)
	
	# get current color and apply	
	mat = iv.SoMaterial()
	so_colortransp(mat)
	sep.addChild(mat)
	
	# get current modeling and apply
	drawstyle = iv.SoDrawStyle()
	so_modeling(drawstyle)
	sep.addChild(drawstyle)
	
	line = iv.SoLineSet()
	line.numVertices.setValue(2)
	sep.addChild(line)
	
	scene.addChild(sep)

#---------------------------------------------------
def so_square(x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4):
	so_line(x1,y1,z1,x2,y2,z2)
	so_line(x2,y2,z2,x3,y3,z3)
	so_line(x3,y3,z3,x4,y4,z4)
	so_line(x4,y4,z4,x1,y1,z1)

#---------------------------------------------------
def so_cube(size,x,y,z):
	static = get_par('Static')
	if static == 'true':
		scene = OnX.session().ui().currentPage().currentRegion().cast_SoDisplayRegion().getStaticScene()	
	else:
		scene = OnX.session().ui().currentPage().currentRegion().cast_SoDisplayRegion().getDynamicScene()
	sep = iv.SoSeparator()		
	
	# translate to x,y,z	
	trans = iv.SoTranslation() 	
	trans.translation.setValue(iv.SbVec3f(x,y,z) )
	sep.addChild(trans)
	
	# set better transparency type
	transptype = iv.SoTransparencyType()
	transptype.value.setValue(1)
	sep.addChild(transptype)
	
	# get current color and apply	
	mat = iv.SoMaterial()
	so_colortransp(mat)
	sep.addChild(mat)
	
	# get current modeling and apply
	drawstyle = iv.SoDrawStyle()
	so_modeling(drawstyle)
	sep.addChild(drawstyle)
	
	# create cube	
	cube = iv.SoCube()
	cube.width.setValue(size)
	cube.height.setValue(size)
	cube.depth.setValue(size)			
	sep.addChild(cube)
	
	scene.addChild(sep)

#---------------------------------------------------
def so_arrow(state):	
	ARROWSIZE = .03
	ARROWZSIZE = 700.
	
	static = get_par('Static')
	if static == 'true':
		scene = OnX.session().ui().currentPage().currentRegion().cast_SoDisplayRegion().getStaticScene()	
	else:
		scene = OnX.session().ui().currentPage().currentRegion().cast_SoDisplayRegion().getDynamicScene()
	sep = iv.SoSeparator()
	
	# get current color and apply	
	mat = iv.SoMaterial()
	so_colortransp(mat)
	sep.addChild(mat)
	
	position = state.position()
	tx = state.tx()
	ty = state.ty()		
	pod_begin = iv.SbVec3f(position.x(), position.y(), position.z())
	pod_end = iv.SbVec3f(position.x() + ARROWZSIZE*tx, position.y() + ARROWZSIZE*ty, position.z() + ARROWZSIZE)		
	arrow = HEPVis.SoArrow()
	arrow.tail.setValue(pod_begin)
	arrow.tip.setValue(pod_end)
	arrow.size.setValue(ARROWSIZE)
	arrow.conicalShaft.setValue(True)
	sep.addChild(arrow)
	
	scene.addChild(sep)
	

#---------------------------------------------------
# Search Window and his methods
#---------------------------------------------------	

def d_window(istationselector, nsubwin=2):
	s_static(0)
	zmin=Double(1.)
	zmax=Double(1.)	
	istationselector.getValidity(zmin, zmax)
	dz = (zmax - zmin) / nsubwin
	
	zi = zmin
	wi = istationselector.searchWindow(zi)
	minxi=wi.minX()
	maxxi=wi.maxX()
	minyi=wi.minY()
	maxyi=wi.maxY()	
	so_square(minxi,minyi,zi,maxxi,minyi,zi,maxxi,maxyi,zi,minxi,maxyi,zi)

	zf = zmax
	wf = istationselector.searchWindow(zf)
	minxf=wf.minX()
	maxxf=wf.maxX()
	minyf=wf.minY()
	maxyf=wf.maxY()
	so_square(minxf,minyf,zf,maxxf,minyf,zf,maxxf,maxyf,zf,minxf,maxyf,zf)
	
	for i in range(nsubwin):
		zi = zmin + i*dz		
		wi = istationselector.searchWindow(zi)
		minxi=wi.minX()
		maxxi=wi.maxX()
		minyi=wi.minY()
		maxyi=wi.maxY()		
		
		zf = zmin + (i+1)*dz
		wf = istationselector.searchWindow(zf)
		minxf=wf.minX()
		maxxf=wf.maxX()
		minyf=wf.minY()
		maxyf=wf.maxY()
		
		so_line(minxi,minyi,zi,minxf,minyf,zf)
		so_line(minxi,maxyi,zi,minxf,maxyf,zf)
		so_line(maxxi,minyi,zi,maxxf,minyf,zf)
		so_line(maxxi,maxyi,zi,maxxf,maxyf,zf)			


#---------------------------------------------------
def conf_TU(seed):
	ids = seed.lhcbIDs()
	if len(ids):
		firstid = ids[0]
		
		if firstid.isMuon():
			TU.setReco('TMuonConf')			
		
		elif firstid.isVelo():
			TU.setReco('GuidedForward')
		
		elif firstid.isCalo():
			caloid = firstid.caloID()
			
			if caloid.calo() == 2: # ele
				TU.setReco('TEleConf')
			
			elif caloid.calo() == 3: # had
				TU.setReco('THadronConf')
				
			else:
				print 'Error - conf_TU : No such caloid ', caloid
				return
				
		else:
			print 'Error - conf_TU : No window for this seed'
			return

#---------------------------------------------------
def d_sw(seed):
	conf_TU(seed)
	istationselectors = TU.view( seed )
	for iss in istationselectors:
		d_window( iss )

#---------------------------------------------------
def d_swids(seed):
	conf_TU(seed)
	ids = TU.lhcbIDsInView( seed )
	for iid in ids:
		d_lhcbid( iid )

#---------------------------------------------------
# Get selected objects
#---------------------------------------------------	

def get_selected():
	session = OnX.session()
	da = session.accessorManager() # data accessor.
	da.collect('SceneGraph','highlight==true')
	da.filter('name')
	handlerIterator = da.handlersIterator()

	import pmx
	uiSvc = pmx.uiSvc()

	#r = pmx.Region()
	#r.eraseEvent()

	selectedobjs = []

	while 1:
		handler = handlerIterator.handler()
		if handler == None: break
		accessor = handler.type()
		cls = 'LHCb::'+accessor.name()
		# print cls
		swig_p = handler.object() # void* but wrapped by SWIG
		lcg_p = uiSvc.topointer(OnX.smanip.p2sx(swig_p)) # void* but wrapped by LCG
		obj = TPython.ObjectProxy_FromVoidPtr(lcg_p,cls)
		selectedobjs.append( obj )
		handlerIterator.next()

	handlerIterator.thisown = 1
	del handlerIterator
	
	return selectedobjs

#---------------------------------------------------
def recombine(algoname):
	selobjs = get_selected()	
	Particles = GaudiPython.gbl.std.vector('const LHCb::Particle*')
	selparts = Particles()	
	for p in selobjs:
		selparts.push_back(p)	
	alg = appMgr.algorithm(algoname) 
	interf = alg._ialg
	setInput = GaudiPython.gbl.DaVinci.Utils.setInput 
	setInput ( interf , selparts ) 
	interf.sysExecute() 


#---------------------------------------------------
# Pure drawing low level methods
#---------------------------------------------------	
	
def d_tes(tes):
	'''
	Draw transient event store element '/Event/...'
	'''
	uiSvc().visualize(tes)

#---------------------------------------------------	
def d_obj(obj):
	cls = GaudiPython.getClass(obj.__class__.__name__)	
	if cls == None:
		print 'Class %s not in Gaudi namespace' % (obj.__name__)
	else :
		if str(cls).find('SmartRef') > -1:
			obj = obj.clone()
			cls = GaudiPython.getClass(obj.__class__.__name__)	
		GaudiPython.gbl.SoEvent.KO(cls).visualize(soCnvSvc(), obj)
		ui().synchronize()
		
#---------------------------------------------------	
def d_cluster(cluster):
	
	if cluster == 'velo':
		d_tes('/Event/Raw/Velo/Clusters')
		
	elif cluster == 'tt':
		d_tes('/Event/Raw/TT/Clusters')
	
	elif cluster == 'it':
		d_tes('/Event/Raw/IT/Clusters')
	
	elif cluster == 'ot':
		d_tes('/Event/Raw/OT/Times')
	
	elif cluster == 'prs':
		d_tes('/Event/Raw/Prs/Digits')
		
	elif cluster == 'spd':
		d_tes('/Event/Raw/Spd/Digits')
	
	elif cluster == 'ecal':
		d_tes('/Event/Raw/Ecal/Digits')
		
	elif cluster == 'hcal':
		d_tes('/Event/Raw/Hcal/Digits')
	
	elif cluster == 'muon':
		d_tes('/Event/Raw/Muon/Coords')
		
#---------------------------------------------------	
def d_lhcbid(lhcbid):	

	# classify lhcbid, search raw data, draw
	
	if lhcbid.isVelo():
		vcs = evt['Raw/Velo/Clusters']
		vc = vcs(lhcbid.veloID())
		d_obj(vc)
	
	elif lhcbid.isTT():
		ttcs = evt['Raw/TT/Clusters']
		ttc = ttcs(lhcbid.stID())
		d_obj(ttc)
	
	elif lhcbid.isIT():
		itcs = evt['Raw/IT/Clusters']
		itc = itcs(lhcbid.stID())
		d_obj(itc)
	
	elif lhcbid.isOT():
		ottimes = evt['Raw/OT/Times']
		ott = ottimes(lhcbid.otID())
		d_obj(ott)
	
	elif lhcbid.isCalo():
		d_caloid(lhcbid.caloID())
	
	elif lhcbid.isMuon():
		mcoords = evt['Raw/Muon/Coords']
		mc = mcoords(lhcbid.muonID())
		d_obj(mc)
	
	else:
		print 'd_id ERROR - unknown detector type :', lhcbid.detectorType()


#---------------------------------------------------	
def d_caloid(caloid):
	
	if caloid.calo() == 0:
		prsdgs = evt['Raw/Prs/Digits']
		prsdg = prsdgs( caloid )
		d_obj( prsdg )
	
	elif caloid.calo() == 1:
		spddgs = evt['Raw/Spd/Digits']
		spddg = spddgs( caloid )
		d_obj( spddg )
	
	elif caloid.calo() == 2:
		edgs = evt['Raw/Ecal/Digits']
		edg = edgs( caloid )
		d_obj( edg )
	
	elif caloid.calo() == 3:
		hdgs = evt['Raw/Hcal/Digits']
		hdg = hdgs( caloid )
		d_obj( hdg )
		
#---------------------------------------------------			

ALLCOLORDICT = {
	0: 'red',
	1: 'yellow',
	2: 'green',
	3: 'cyan',
	4: 'blue',
	5: 'pink'
	}

COLORS = {
	'red': (1,0,0),
	'yellow': (1,1,0),
	'green': (0,1,0),
	'cyan': (0,1,1),
	'blue': (0,0,1),
	'pink': (1,0,1),	
	'black': (0,0,0),
	'white': (1,1,1)
	}

def get_relativelinewidth(obj, objs, maxlw=3):
	pt = lambda x: x.pt()
	pts = map(pt, objs)
	maxpt = max(pts)
	objpt = pt(obj)
	lw = 1 + int(round(objpt/maxpt * ( maxlw - 1 )))
	return lw

def get_relativecolorintensity(obj, objs, minint=.4):
	pt = lambda x: x.pt()
	pts = map(pt, objs)
	maxpt = max(pts)
	objpt = pt(obj)
	intensity = objpt/maxpt * (1 - minint) + minint
	return intensity

#---------------------------------------------------	
def d_track(ts):

	if isinstance(ts, list):
		tssize = len(ts)
		for t in ts:
			s_lw( get_relativelinewidth(t, ts, tssize) )
			d_track(t)

	else:
		d_obj(ts)

		drawids = get_par('drawIDs')
		if drawids == 'true':
			ids = ts.lhcbIDs()
			for i in ids:
				d_lhcbid(i)	


#	for i in range(len(ts)):
#	
#		if color == '':
#			d_track(ts[i], opts)
#			continue
#	
#		# pick color
#		elif color == 'all':
#			r,g,b = COLORS[ ALLCOLORDICT[i%6] ]
#		else:
#			r,g,b = COLORS[color]
#	
#		# set intensity and color
#		value = maxattrib( ts[i] )
#		intensity = value / maxvalue
#		r *= intensity * .7 + .3 # min .4, otherwise to dark
#		g *= intensity * .7 + .3
#		b *= intensity * .7 + .3
#		#
#		s_color(r,g,b)
#		
#		# draw track
#		d_track(ts[i], opts)
		
#---------------------------------------------------			
def d_part(parts):

	if isinstance(parts, list):
		for p in parts:
			d_part(p)

	else:
		d_obj(parts)
		
		drawids = get_par('drawIDs')
		if parts.charge() != 0 and drawids == 'true':
			ids = proto.track().lhcbIDs()
			for i in ids:
				d_lhcbid(i)	

		daus = L.recursiveDaughters(parts)
		daussize = len(daus)
		for dau in daus:
			s_lw( get_relativelinewidth(dau, daus, daussize) )
			d_obj(dau)

#---------------------------------------------------			
def d_vertex(vertices):

	if isinstance(vertices, list):
		for v in vertices:
			d_vertex(v)

	else:
		d_obj(vertices)	
				
		daus = list( vertices.tracks() )
		d_track(daus)



#---------------------------------------------------			
def d_hlt(hltsummary):

	if HLTSUM.hasSelection(hltsummary):
	
		seltype = HLTSUM.selectionType(hltsummary)
		
		if seltype == 'Track':	
			sum = list( HLTSUM.selectionTracks(hltsummary) )
			d_track(sum)
			
		elif seltype == 'Vertex':
			sum = list( HLTSUM.selectionVertices(hltsummary) )
			d_vertex(sum)
			
		elif seltype == 'Particle':
			sum = list( HLTSUM.selectionParticles(hltsummary) )
			d_part(sum)
		
		else:
			print 'd_hlt - Selection type Unknown'  
			return
		
		#debug
		print hltsummary,'n of candidates:',len(sum)
		#for i in range(len(sum)):
		#	print '   ',i,'candidate - pt =', sum[i].pt()
		
	else:
	 print 'd_hlt - No selection for summary ', hltsummary

#---------------------------------------------------			
def d_tos(off, hltsum):
	tos = TTT.matchedTOSTracks(off, hltsum)
	d_track(tos, opts)


#---------------------------------------------------
# LHCb detector - drawing methods
#---------------------------------------------------	

DETECTORCOLORS = {
	'tt': (.6,.2,.55),
	'it': (1,1,0),
	'ot': (1,.7,0),
	'ecalin': (1,0,0),
	'ecalmid': (1,.5,0),
	'ecalout': (1,.3,0),
	'hcalin': (0,0,1),
	'hcalout': (0,0,.66),	
	'muon': (.3,1,.3),
	'pipe': (0,1,.95)
	}

def s_detcolor(det):
	r,g,b = DETECTORCOLORS[det]
	s_color(r,g,b)

#---------------------------------------------------	

def d_det(d):
	'''
	Draw detector element '/dd/...'
	'''
	uiSvc().visualize(d)	

#---------------------------------------------------	
def det_velo():
	for i in range(42):
		if i < 10:
			modstr = '0' + str(i)
		else:
			modstr = str(i)	
		
		if i % 2 == 0:
			det = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloLeft/Module'+modstr+'/RPhiPair'+modstr
		else:
			det = '/dd/Structure/LHCb/BeforeMagnetRegion/Velo/VeloRight/Module'+modstr+'/RPhiPair'+modstr
		d_det(det)

#---------------------------------------------------	
def det_rich1():	
	d_det('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/Aerogel')

#---------------------------------------------------	
def det_tt():
	s_detcolor('tt')
	d_det('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTa/TTaXLayer')
	d_det('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTa/TTaULayer')
	d_det('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTb/TTbXLayer')
	d_det('/dd/Structure/LHCb/BeforeMagnetRegion/TT/TTb/TTbVLayer')	

#---------------------------------------------------	
def det_magnet():
	s_useVisSvc(1)
	d_det('/dd/Structure/LHCb/MagnetRegion/Magnet')
	s_useVisSvc(0)

#---------------------------------------------------	
def det_t():
	s_detcolor('it')
	for i in range(3):
		modstr = str(i+1)
		d_det('/dd/Structure/LHCb/AfterMagnetRegion/T/IT/Station'+modstr+'/TopBox')
		d_det('/dd/Structure/LHCb/AfterMagnetRegion/T/IT/Station'+modstr+'/BottomBox')
		d_det('/dd/Structure/LHCb/AfterMagnetRegion/T/IT/Station'+modstr+'/ASideBox')
		d_det('/dd/Structure/LHCb/AfterMagnetRegion/T/IT/Station'+modstr+'/CSideBox')
	s_detcolor('ot')
	d_det('/dd/Structure/LHCb/AfterMagnetRegion/T/OT')

#---------------------------------------------------		
def det_ecal():
	#s_opened()
	s_detcolor('ecalin')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalA/EcalAInner')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalC/EcalCInner')
	s_detcolor('ecalmid')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalA/EcalAMiddle')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalC/EcalCMiddle')
	s_detcolor('ecalout')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalA/EcalAOuter')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Ecal/EcalC/EcalCOuter')
	#s_opened(0)

#---------------------------------------------------			
def det_hcal():
	#s_opened()	
	s_detcolor('hcalin')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Hcal/HcalA/HcalAInner')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Hcal/HcalC/HcalCInner')
	s_detcolor('hcalout')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Hcal/HcalA/HcalAOuter')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Hcal/HcalC/HcalCOuter')
	#s_opened(0)

#---------------------------------------------------		
def det_muon():
	s_detcolor('muon')
	s_opened(0)
	d_det('/dd/Structure/LHCb/DownstreamRegion/Muon/M1')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Muon/M2')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Muon/M3')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Muon/M4')
	d_det('/dd/Structure/LHCb/DownstreamRegion/Muon/M5')

#---------------------------------------------------		
def det_muonContours() :
	s_detcolor('muon')
	s_static()
	for m in range(5):
		station = det['/dd/Structure/LHCb/DownstreamRegion/Muon/M'+str(m+1)] 		
		muon = det['/dd/Structure/LHCb/DownstreamRegion/Muon']
		x = muon.getOuterX(m)
		y = muon.getOuterY(m)
		
		XYZPoint = GaudiPython.gbl.Math.XYZPoint
		p1 = XYZPoint(x,y,0)
		p1 = station.geometry().toGlobal(p1)
		x1=p1.x()
		y1=p1.y()
		z1=p1.z()
		p2 = XYZPoint(x,-y,0)
		p2 = station.geometry().toGlobal(p2)
		x2=p2.x()
		y2=p2.y()
		z2=p2.z()
		p3 = XYZPoint(-x,-y,0)
		p3 = station.geometry().toGlobal(p3)
		x3=p3.x()
		y3=p3.y()
		z3=p3.z()
		p4 = XYZPoint(-x,y,0)
		p4 = station.geometry().toGlobal(p4)
		x4=p4.x()
		y4=p4.y()
		z4=p4.z()
		
		so_square(x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4)

#---------------------------------------------------		
def det_pipe():
	s_detcolor('pipe')
	d_det('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1BeforeSubM')
	d_det('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1SubMaster')
	d_det('/dd/Structure/LHCb/BeforeMagnetRegion/Rich1/PipeInRich1AfterSubM')
	d_det('/dd/Structure/LHCb/MagnetRegion/PipeInMagnet')
	d_det('/dd/Structure/LHCb/AfterMagnetRegion/T/PipeInT')
	d_det('/dd/Structure/LHCb/AfterMagnetRegion/Rich2/Rich2BeamPipe')
	d_det('/dd/Structure/LHCb/DownstreamRegion/PipeDownstream')	

#---------------------------------------------------	
def det_lhcb(opt='f'):
	'''
	Show LHCb detector
	
		t - tracking
		f - full detector		
		c - calorimeters		
	'''
	
	s_modeling('w')
	
	# tracking
	if opt.find('t') > -1:	
		s_useVisSvc(1)
		det_velo()
		s_useVisSvc(0)
		det_tt()
		det_t()
		det_muonContours()
		
	# calorimeters	
	elif opt.find('c') > -1:
		s_useVisSvc(0)
		det_ecal()
		det_hcal()
		
	# full detector
	else:
		det_velo()
		det_rich1()
		det_tt()
		det_magnet()
		det_t()
		det_ecal()
		det_hcal()
		det_muonContours()

#---------------------------------------------------
# Routines
#---------------------------------------------------	

def r_offparts(ptcut, ipcut):
	particles = L.filterParticles('Phys/StdLoosePions/Particles', 'Rec/Vertex/Primary', ptcut, ipcut)
	s_drawIDs(0)
	for p in particles:
		d_obj(p)
	s_drawIDs(1)
	
def r_htrig():
	s_caloet()
	s_modeling('w')	
	det_lhcb('t')
	det_lhcb('c')
	s_modeling('s')
	s_color('yellow')
	d_cluster('hcal')
	s_lw(1)
	s_color('green')
	r_offparts(1000, .1)
	s_color('red')
	d_hlt('L0HadronDecision')
	d_hlt('Hlt1HadronSingleDecision')
	d_hlt('Hlt1HadronDiDecision')
	

#---------------------------------------------------
# Initialization
#---------------------------------------------------

evt = appMgr.evtsvc()
det = appMgr.detsvc()

# does not exist anymore, replaced by decision bank or tistos tool
# HLTSUM = appMgr.toolsvc().create('HltSummaryTool',interface='IHltConfSummaryTool')
TTT = appMgr.toolsvc().create('TriggerTisTos',interface='ITriggerTisTos')
# does not exist anymore
# TU = appMgr.toolsvc().create('HltTrackUpgradeTool',interface='ITrackUpgrade')


s_caloet()
#s_ottimes()	
s_static()
s_drawIDs()
