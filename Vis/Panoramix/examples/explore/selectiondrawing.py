import ExploreDraw as D
import ExploreLib as L

def draw():
	ui=D.ui()
	sel = ui.parameterValue('sl_sel.selection')
	parts = list( D.evt['Phys/'+sel+'/Particles'] )
	cands = ui.parameterValue('sl_selcands.selection')
	
	if cands == 'all' or cands =='': 		
		D.d_part(parts)
	
	else:
		D.d_part(parts[int(cands)])

def updateSelections():
	ui=D.ui()
	selections = D.evt['Phys/Selections']
	selstrs = L.SelectionsList(selections)
	activesel = ui.parameterValue('cb_sel.value')
	filtersels = L.findStrOnList(activesel, selstrs)
	ui.setParameter('sl_sel.items', L.StrList2Items( filtersels ) )


def updateCandidates():
	ui=D.ui()
	selname = ui.parameterValue('sl_sel.selection')
	ncands = len( D.evt['Phys/'+selname+'/Particles'] )
	candsstr = ['all']
	for i in range(ncands):
		candsstr.append( str(i) )
	ui.setParameter('sl_selcands.items', L.StrList2Items( candsstr ) )
		
