from Gaudi.Configuration import *

#does not work yet use old opts
#ApplicationMgr().ExtSvc              += ["IncidentSvc","LHCb::NetworkEvtSelector/EventSelector"]
#ApplicationMgr().Runable             = "LHCb::EventRunable/Runable"
#Runable.MEPManager                   = "LHCb::MEPManager/MEPManager"
#MEPManager.Buffers                   = []
#EventSelector().Input                  = "$NODENAME/EvtServ"
#EventPersistencySvc().CnvServices      = ["LHCb::RawDataCnvSvc/RawDataCnvSvc"]

importOptions('$PANORAMIXROOT/options/PanoramixOnline.opts') 
allConfigurables["EventSelector"].REQ1 = "EvType=2;TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;VetoMask=0,0,0,0;MaskType=ANY;UserType=VIP;Frequency=PERC;Perc=100.0";

from Configurables import OnXSvc
OnXSvc().File = "$PANORAMIXROOT/scripts/OnX/Panoramix_Online.onx"
OnXSvc().OutputToTerminal = True

