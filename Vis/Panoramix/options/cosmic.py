from Gaudi.Configuration import *
import GaudiKernel.SystemOfUnits as units

from Configurables import MagneticFieldSvc
MagneticFieldSvc().UseConstantField = True
MagneticFieldSvc().UseConditions    = False

# Velo commissioning configuration
importOptions('$PANORAMIXROOT/options/Velo-CommissioningConditions.py')

#special calo cosmic reconstruction
ApplicationMgr().TopAlg += ['CaloCosmicsTrackAlg']

from TrackSys.Configuration import *
TrackSys().setSpecialDataOption("fieldOff",True)

#PatSeeding for cosmics
#importOptions('$PATALGORITHMSROOT/options/PatSeeding.py')

from Configurables import (PatSeeding, PatSeedingTool)
PatSeeding = PatSeeding("PatSeeding")
if TrackSys().fieldOff():
   PatSeeding.addTool(PatSeedingTool, name="PatSeedingTool")
   PatSeeding.PatSeedingTool.zMagnet = 0.
   PatSeeding.PatSeedingTool.FieldOff = True
#importOptions('$PATALGORITHMSROOT/options/PatSeedingTool-Cosmics.opts')
importOptions('$PATALGORITHMSROOT/options/PatSeedingTool-FieldOff.opts')
ApplicationMgr().TopAlg += [PatSeeding]

# add Velo tracking sequence:
from Configurables import (
Tf__PatVeloRTracking, Tf__PatVeloGeneralTracking,
Tf__PatVeloTrackTool,PatForward, Tf__PatVeloRHitManager,Tf__DefaultVeloRHitManager,Tf__PatVeloPhiHitManager)

from Configurables import Tf__PatVeloGeneric
patVeloGeneric = Tf__PatVeloGeneric()
patVeloGeneric.Output = 'Rec/Track/Velo'
patVeloGeneric.FullAlignment        = True
patVeloGeneric.MinModules           = 5
patVeloGeneric.ClusterCut           = 30 
patVeloGeneric.KalmanState          = 4
patVeloGeneric.ForwardProp          = True
patVeloGeneric.MaxSkip              = 10
patVeloGeneric.CleanSeed            = True
patVeloGeneric.PrivateBest          = True
patVeloGeneric.SigmaTol             = 5
patVeloGeneric.RAliTol              = 0.05
patVeloGeneric.PAliTol              = 0.005
patVeloGeneric.ACDC                 = False  
patVeloGeneric.ConsiderOverlaps     = False
#patVeloGeneric.NoiseThreshold       = 9000
#patVeloGeneric.OutputLevel = 2

patVeloRTracking     = Tf__PatVeloRTracking("PatVeloRTracking")

patVeloTrackTool = Tf__PatVeloTrackTool('VeloTrackTool')
patVeloTrackTool.RHitManagerName  =  'VeloRHitManager'
patVeloTrackTool.PhiHitManagerName = 'VeloPhiHitManager'

veloGeneral = Tf__PatVeloGeneralTracking("PatVeloGeneralTracking")
# tools already created before
veloGeneral.TrackToolName     = 'VeloTrackTool'
veloGeneral.RHitManagerName   = 'VeloRHitManager'
veloGeneral.PhiHitManagerName = 'VeloPhiHitManager'
veloGeneral.PointErrorMin = 2*units.mm

# special for halo tracks
patVeloRTracking.ZVertexMin     = -1.E20 
patVeloRTracking.ZVertexMax     = +1.E20 

veloreco = GaudiSequencer("veloreco")
veloreco.Members += [  patVeloRTracking, veloGeneral ]
# ApplicationMgr().TopAlg += [ veloreco ]
# run for the moment only patVeloGeneric 
ApplicationMgr().TopAlg += [ patVeloGeneric ]

from Configurables import ( PatForward, PatForwardTool, PatFwdTool)
patForward = PatForward("PatForward")
PatFwdTool("PatFwdTool").withoutBField  = True;
PatForwardTool("PatForwardTool").WithoutBField = True;
ApplicationMgr().TopAlg += [ patForward ]

from Configurables import ( PatVeloTT)
importOptions('$PATVELOTTROOT/options/PatVeloTT.py')
patVeloTT = PatVeloTT("PatVeloTT")
patVeloTT.InputUsedTracksNames = []
patVeloTT.removeUsedTracks = False
patVeloTT.PatVeloTTTool.minMomentum = 5000.;
patVeloTT.PatVeloTTTool.maxPseudoChi2 = 256;
patVeloTT.maxChi2 = 256.;
patVeloTT.PatVeloTTTool.DxGroupFactor = 0.0;
patVeloTT.fitTracks = False;
# ApplicationMgr().TopAlg += [patVeloTT]




