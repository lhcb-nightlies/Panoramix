from Gaudi.Configuration import *
from Configurables import (VisualizationSvc,OnXSvc)

## Detector and Event visualization :
ApplicationMgr().ExtSvc += ["SoDetSvc","VisualizationSvc","SoEventSvc"]

## Visualization attributes 
VisualizationSvc().ColorDbLocation = "$XMLVISROOT/xml/colors.xml"

## Rich visualization :
importOptions("$SORICHRECROOT/options/SoRichRec.opts")
# waiting to be fixed by Chris Jones, Jan.2009
#importOptions("$SORICHRECROOT/options/SoRichRecMC.opts")

## Calo visualization :
ApplicationMgr().ExtSvc += [ "SoCaloSvc" ]


## Histogram stat : 
ApplicationMgr().ExtSvc += [ "SoStatSvc" ]

## The "interactivity" machinery ; OnXSvc (bridge Gaudi / OnX) :
importOptions("$ONXSVCROOT/options/OnXSvc.opts")
# OnXSvc.OutputToTerminal = false;

## The GUI XML file :
OnXSvc().File = "$PANORAMIXROOT/scripts/OnX/Panoramix.onx"
OnXSvc().Toolkit = "Qt" #"NONE"
