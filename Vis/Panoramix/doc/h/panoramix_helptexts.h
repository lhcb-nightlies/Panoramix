/**

@page panoramix_helptexts Help texts

 This section gather the various help texts (dialogs, help pulldown menu, etc...) of Panoramix. These help texts could be found in the Panoramix/&lt;version&gt;/scripts/Help directory.

@section panoramix_helptexts_Geometry Geometry
@verbinclude Geometry.help

@section panoramix_helptexts_GaudiPython GaudiPython
@verbinclude GaudiPython.help

@section panoramix_helptexts_Tutorial Tutorial
@verbinclude Tutorial.help

*/
