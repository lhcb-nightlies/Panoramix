/**

@page panoramix_navigation Navigation

  The viewing region is done with an Open Inventor examiner viewer.
 This viewer is dedicated for 3D navigation in a scene but could
 also be used for 2D viewing. The viewing region is "decorated" at 
 left, bottom, right with thumbwheels. 

@image html panoramix_3.jpg
@image latex panoramix_3.ps
 <P>
 The bottom and left thumbwheel permit to rotate the viewer camera (it is 
 the camera that is moving, not the scene). The one at the right permits
 to zoom. Holding the Ctrl key and the left button permit to pane the
 camera when moving the cursor within the viewer area. See below
 for a description of the various controls.

  Repositionning the camera with thumbwheels or key and mouse could be 
 tedious. For LHCb some predefined camera positionning are available
 through the "Camera" pulldown menu. The "Camera/dump" menu item 
 permits to dump the current camera setting. These may permit you
 to create new camera positionning menu items (see GUI customization
 section to create new menu items).

  You must be aware that the viewer has two working modes with the mouse.
 One is the "viewing" mode (the default) that permits to use the mouse
 to navigate within the scene. When in viewing mode the cursor has the
 shape of a hand.  The other mode is the "picking" mode that permits
 to someone to program its own action when a pick is issued on a graphical
 object. When in picking mode the cursor has the shape of an arrow pointing
 to the top left. 

@section panoramix_navigation_picking Picking

  When in viewing mode, you can ask the camera to point to a given 
 object. Then in vieweing mode, click on the "torche" icon at the right
 of the viewer, the cursor should pass to a cross shape. Move
 the cursor on some graphical object, clik on it. The viewer will 
 issue a camera movement to point on the choosen point.

  When in picking mode, clicking on an object will highlight it.
 If the object (or class of object) had been declared to the OnX
 type manager (or data accessor) system, some infos about the 
 data related to the graphical picked object will be displayed 
 in the message area.

@section panoramix_navigation_detector Picking and geometry navigation

  About the detector, some key sequence had been introduced in order
 to "navigate" within the geometry. For example, ask to visualize the
 Ecal with the "Detector/Ecal" menu item. Center on it with the
 "torche" icon. Pass now in picking mode. A "Ctrl click" (that is 
 to say picking on an object whilst holding the Ctrl key) will
 "open" the choosen volume to visualize the children elements.
 A "Shift click" over a child will return back to the mother.
 Note that here a dynamic edition of the Inventor graphical database is done.
 That is to say when opening/closing volumes in this way some OpenInventor
 nodes are created or deleted (it is not only a question of camera
 positionning).

  As an exercise, by using the various navigation and picking
 systems and by using the dialog editors you can try to pass,
 for example with Ecal, from a global view like :

@image html panoramix_4.jpg
@image latex panoramix_4.ps
 <P>
 to an "opened" view like :
<P>
@image html panoramix_5.jpg
@image latex panoramix_5.ps
<P>
  We can note here one more capability of the system ; being able
 to mix solid and wire frame representations of the detector.

@section panoramix_navigation_viewer_controls 3D controls

 "right mouse button" shows a popup menu. If in viewing mode, the Inventor
 viewer popup menu is shows, If in picking mode, a customizable popup menu 
 is shown.

 "right/popup/decorations"  enables/diables the Inventor "decorations" (not 
 available for all GUI).

 "decoration/pointer icon" or "popup/viewing" passes the pointer in 
 picking mode.

 "decoration/hand icon" or "popup/viewing" passes the pointer 
 in viewing mode. 
 
 "decoration/eye icon" or "popup/view all" maps the scene to the window size.

 "decoration/home icon" or "popup/home" returns to home position.

 "decoration/blue home" or "popup/Set home" set home position to be 
 the current one. Then a click on the house icon will return to this
 given position.

 "decoration/torche icon" or "popup/Seek" asks,when in viewing mode, for 
 picking and center the scene over a picked point.

 "left + ptr move" permits to rotate the scene.

 "left + middle + ptr move" permits to scale the scene.

 "ctrl + shift + midle + ptr move" permits to scale the scene.

 "ctrl + left + ptr move" permits to pane the scene.

 "middle + ptr move" permits to pane the scene.

 "Esc key (with pointer on the viewer)" toggles picking/viewing mode.

 "S key" asks for a point to center one when in seek mode.

 "Q key" must never be used when in viewing mode !

 "ctrl + shift + ptr move" permits to select an area with an elastic rectanlge.
 In viewing mode, when releasing keys, the camera setting is saved in a stack
 and a zoom is done over the selected area. The Backspace key permits 
 to return to previous camera settings. In picking mode, when releasing
 the keys, a collect of objects is done in the selected area. The 
 pick-mode-popup-menu (on right button) permit to unhighlight the selection
 if needed.

*/
