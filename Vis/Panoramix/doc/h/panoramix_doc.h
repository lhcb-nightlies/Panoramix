/**

@page panoramix_doc Download the user guide

 The user guide at the PostScript format could be retreived at ftp://ftp.lal.in2p3.fr/pub/LHCb/Panoramix-v5r0.ps.gz

 Due to firewall protections on both side (your machine and LAL ftp server 
 machine) this ftp web access may fail.

 If so, files can also be retrieved with ftp (or sftp) command line 
 program by doing :
@verbatim
     UNIX> ftp ftp.lal.in2p3.fr
     anonymous
     user name
     ftp> cd pub/LHCb
     ftp> bin
    (ftp> passive)
     ftp> get Panoramix-<version>.ps.gz
     ftp> quit
@endverbatim

@section panoramix_doc_build Building the doc from doxygen.

  The web pages and paper user guide are constructed with doxygen and latex.
 The doc sources are under Vis/Panoramix/&lt;version&gt;/doc/doxygen.

  To produce the html web pages and the Panoramix.ps, for example 
 under a UNIX csh shell, do :
@verbatim
    csh> cd <path>/Vis/Panoramix/<version>/cmt
    csh> source setup.csh 
      to have the PANORAMIXROOT env variable.
    csh> cd ../doc/doxygen
    csh> <edit the Doxyfile in order to have 
         the latex production, that is to say have
           GENERATE_HTML    = YES
           GENERATE_LATEX   = YES >
    csh> doxygen 
      to produce web pages under ../html and .tex files in ../latex
    csh> cd ../latex
    csh> cp Panoramix.tex refman.tex
      to override the default doxygen layout and have a "manual" look and feel.
    csh> make
      to produce the .dvi files.
    csh> make ps
      to produce the refman.ps
    csh> mv refman.ps Panoramix.ps
    csh> ghostview Panoramix.ps       
      to visualize the doc.
@endverbatim
  the final PostScript doc for a release is put under ../ps.

@section panoramix_doc_editor Chief editor

  The chief editor of the Panoramix user guide is Guy Barrand.

*/


