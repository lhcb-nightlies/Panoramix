/**

@page panoramix_introduction Panoramix

  The Panoramix interactive environment is the association of the Vis
 packages : OnXSvc, VisSvc, SoDet, SoEvent, SoStat, SoHepMC, SoCalo 
 and Panoramix itself. These packages relies on Gaudi, LHCb and 
 OpenScientist packages.

@section panoramix_introduction_interactivity Graphical user interface with XML

  The GUI (graphical user interface) is described in XML.

  The OnX package, coming with OpenScientist, drives the interactivity. 
 From an XML description of the GUI (Graphical User Interface), OnX creates 
 an application GUI by using various "toolkit" like Motif, Win32, GTK+, Qt. 
 It permits also to handle in the .onx XML files various scripting 
 languages (for example Python, "system") to describe comportements 
 of the GUI pieces.

@section panoramix_introduction_graphic Graphic with OpenInventor and OpenGL

  The graphic of Panoramix is handled by Open Inventor and OpenGL. 
 Today the free and open source coin3d implementation of OpenInventor 
 of the SIM company is used (see http://www.lal.in2p3.fr/SI/Coin). 
  The OpenScientist distribution comes with the Coin packages handled 
 by CMT for Xt/Motif, gtk+, Qt and Windows. The Inventor "viewers", 
 being part of the GUI, are created by OnX from a placement in the 
 application .onx XML files.

@section panoramix_introduction_scripting Scripting with Python

  The default scripting language is Python. Python binding exists
 for OpenInventor and OnX.

*/
