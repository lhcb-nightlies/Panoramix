/**

@page panoramix_starting Preparing the environment

  By principle, Panoramix is not one big program 
 put in one big package. The code of Panoramix is split into various 
 packages handled by CMT. When reading this section, it is assumed 
 that all packages had been installed properly on your computer(s). 
 Anyway, to run properly, a user must check that he accesses the right 
 set of packages.

  Panoramix uses the LHCb software, be sure to have all the necessary LHCb 
 environment variables set properly (LHCBHOME, LHCBDBASE, etc...). 
 If you log at CERN on lxplus under an LHCb
 account these variables are, in principle, set for your. 
 To dump them, do a :

@verbatim
   printenv | grep LHCB
@endverbatim

  Panoramix needs to have in CMTPATH the path toward the Gaudi, LHCb and
 OpenScientist software. For example, if working on lxplus :

@verbatim
   csh> setenv CMTPATH "$HOME/mycmt:
    /afs/cern.ch/sw/Gaudi/releases/GAUDI_<version>:
    /afs/cern.ch/lhcb/software/releases/LHCB/LHCB_<version>:/afs/cern.ch/sw/contrib"
@endverbatim

  A healthy thing to do is to check that all packages are seen correctly.
 You can do that by issuing a "cmt show uses" done in the 
 Panoramix/&lt;version&gt;/cmt directory. For example, within a csh UNIX shell :
@verbatim
   csh> cd <path>/Panoramix/<version>/cmt
   csh> source <path>/CMT/<version>/mgr/setup.csh
   csh> cmt show uses
@endverbatim
 or under a DOS prompt :
@verbatim
   DOS> cd <disk>:\<path>\Panoramix\<version>\cmt
   DOS> call <disk>:\<path>\CMT\<version>\mgr\setup.bat
   DOS> cmt show uses
@endverbatim

@section panoramix_starting_from_Gaudi Starting the display from a Gaudi application

  The Panoramix package contains the program Panoramix.exe 
 which starts a Gaudi application manager over the 
 $PANORAMIXROOT/options/Panoramix.opts file. 
 For example, in bash, to execute it and have the display started from it, do :
@verbatim
     bash> cd <to some writable directory of your own>
     bash> . <path>/Panoramix/<version>/cmt/setup.sh
     bash> panoramix
@endverbatim
 On Windows, from a DOS prompt :
@verbatim
     DOS> cd <to some writable directory of your own>
     DOS> call <path>\Panoramix\<version>\cmt\setup.bat
     DOS> panoramix
@endverbatim
 Through the job options ; the OnXSvc, So*Svc are going to be loaded. 
 The OnXSvc will create the GUI from the XML file associated to the 
 ONXFILE environment variable or from the OnXSvc.File property in 
 a the option file. The OnXSvc.Toolkit property could
 be used to specify the GUI toolkit. See Panoramix/options/Panoramix.opts
 for a commented job option file.

@section panoramix_starting_default_GUI Default GUI

  At startup the main panel should appear :

@image html panoramix_1.jpg
@image latex panoramix_1.ps

 (on the paper manual the main panel looks more
 compact. The main panel had been reduced to 490x400
 in order to fit in a page but also in order that
 GUI items remain readable).

@section panoramix_starting_first_view Visualizing the Ecal, simple navigation.

  Whence the main panel is here, click for example the "Detector/Ecal" 
 menu item to viaulize the ECAL. When the ECAL is here play with
 the thumbwheels of the viewer to visualize it. The bottom
 and left thumbwheel permit to rotate the viewer camera (it is 
 the camera that is moving, not the scene). The one at the right permits
 to zoom. Holding the Ctrl key and the left button permit to pane the
 camera when moving the cursor within the viewer area. Click in the 
 "Camera/top, etc..." menu item to have predefined
 camera positionning mapping the experiment layout.

  See the Navigation section for more.

@section panoramix_starting_from_OnX Starting the program from OnX
  Beside starting from Gaudi, it had been forseen to be able
 to start things from the GUI toolkit (or the interactive environment
 to be more general). We may fall on situations where Gaudi is going 
 to be the plugin to an interactive environment (for example a web browser).

  Then someone can start the display from the onx.exe program.
  To work with an installed version (for example
 working with the Panoramix installed at CERN) :
@verbatim
      csh> cd <to some writable directory of your own>
      csh> source <path>/Panoramix/<version>/cmt/setup.csh
      csh> onx (to start OnX with Motif GUI toolkit)
@endverbatim
 On Windows, from a DOS prompt :
@verbatim
     DOS> cd <to some writable directory of your own>
     DOS> call <path>\Panoramix\<version>\cmt\setup.bat
     DOS> onx
@endverbatim
 This startup could be usefull to test / debug the GUI creation. In this case
 the GUI is built first and Gaudi started after. The onx.exe program
 loads the Panoramix.onx GUI file (through the ONXFILE environment variable), 
 creates the GUI and initializes (through the "create" scripts) Gaudi.

@section panoramix_starting_troubles Troubleshootings

 If the main panel does not appear you are clearly in a mess. Then :
 - check that all packages are accessible by issuing a "cmt show uses".
   If not, check the Gaudi, LHCb, OpenScientist packages installation.
 - check the messages coming from Gaudi to see that all requested libraries
   are found and loaded correctly.
 - check the messages coming from Gaudi to see any kind of problem related
   to a bad access to a resource. For example accessing an event file or 
   the detector database. This oftenly came from a bad environment variable
   setup ; then checks the LHCb environment variables.
 - check the GUI creation with a "OnX startup". You may have done a mistyping
   when edited a .onx file for a GUI customization. Check the ONXFIlE 
   environment variable (it sould point to the Panoramix.onx file).
 - on UNIX, check the DISPLAY environment variable.
 - on Linux, the LD_LIBRARY_PATH environment variable to access shared libs.
   Here you can issue a "ldd" over the Panoramix.exe or some .so libraries
   to see that the related libs are access correctly at run time.
 - on Windows, the PATH environment variable to access dlls.
   In general Windows issues a clear GUI message about a faulty
   not found dll.

 If all that seems ok and the main panel still do not appear or appears
 with error messages, contact the Panoramix support for more help : 
 - G.Barrand (barrand@lal.in2p3.fr) for the GUI, visualization and 
   scripting machinery.
 - S.Ponce (Sebastien.Ponce@cern.ch) for the detector visualization. Sebastien
   can be considered as the support at CERN.
 - P.Mato (Pere.Mato@cern.ch) for Windows support at CERN.

  Note that the display is at the top of a whole chain of packages
 and that a good comportement relies on a good installation and access
 of the underlying packages.

*/
