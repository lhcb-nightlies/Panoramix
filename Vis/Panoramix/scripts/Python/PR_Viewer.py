from panoramixmodule import *
import ReFitTracks
import PRplot,Onlinemodule,time,os

dialog    = 'Panoramix_PRViewer_dialog'

def defaults():
  if not ui().findWidget(dialog) :  ui().showDialog( '$PANORAMIXROOT/examples/Python/PR_Viewer_setup.onx' ,dialog )
  if ui().findWidget(dialog) : 
    ui().findWidget(dialog).hide()
  ui().synchronize()

def setup(flag='withBeamPipe'):
 if flag.find('withBeamPipe')>-1 : 
   PRplot.Magnet_view()
 else: 
   uiSvc().visualize('/dd/Structure/LHCb/MagnetRegion/Magnet')
 session().setParameter('modeling.modeling','wireFrame') 
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')
 session().setParameter('modeling.modeling','solid')
 visMuonContour  = ui().parameterValue('Panoramix_PRViewer_input_visMuonContour.value')=='True'
 if visMuonContour:
  for i in range(1,5):  Onlinemodule.muonStationContours(i)

def dispMuonTracks():
  if evt['Rec/Track/Muon'] :  
        mucolor  = ui().parameterValue('Panoramix_PRViewer_input_mucolor.value')
        if mucolor == 'None': mucolor = 'magenta' 
        temp1 = session().parameterValue('modeling.userTrackRange')
        temp2 = session().parameterValue('modeling.trackEndz')
        temp3 = session().parameterValue('modeling.lineWidth')
        temp4 = session().parameterValue('modeling.trackStartz')
        rc = session().setParameter('modeling.userTrackRange','true')
        rc = session().setParameter('modeling.trackEndz','22000.')
        rc = session().setParameter('modeling.trackStartz','-200.')
        lw = str(float(session().parameterValue('modeling.lineWidth')) * 1.5)     
        Style().setColor(mucolor)
        lw = str(float(session().parameterValue('modeling.lineWidth')) * 1.5)     
        rc = session().setParameter('modeling.lineWidth',lw)  
        xx = Double(-1.)
        for amuon in evt['Rec/Track/Muon'] : 
         atrack =  evt['Rec/Track/Best'][amuon.key()]
         if atrack.type()==atrack.Long and amuon.info(304,xx) > 0:
          rc = Object_visualize(atrack)
        rc = session().setParameter('modeling.userTrackRange',temp1)
        rc = session().setParameter('modeling.trackEndz',temp2)
        rc = session().setParameter('modeling.lineWidth',temp3)
        rc = session().setParameter('modeling.trackStartz',temp4)
    
def createPage(title,nregions): 
 page = ui().currentPage()
 page.deleteRegions()
 Page().setTitle(title)
 Page().titleVisible(True)
 if nregions == 4: Page().createDisplayRegions(2,2,0)
 if nregions == 1: Page().createDisplayRegions(1,1,0)
 ui().currentWidget().cast_ISoViewer().setHeadlight(1)
 Page().setCurrentRegion(0) 
 setup()     
 if nregions == 4: 
   Page().setCurrentRegion(2) 
   Region().setTransformScale(1.,8.4,1.)

def track_logic(flag):
# check if Best container is there:
# ignore flag for the moment, field off 
       mucolor = ui().parameterValue('Panoramix_PRViewer_input_mucolor.value')
       ok = False 
       if evt['Rec/Track/Best'] : 
          if evt['Rec/Track/Best'].size()>0: 
            PRplot.disp_tracks(0,mucolor=mucolor)
            ok = True 
# if not try other containers
       if not ok :
        other_containers = ['Rec/Track/Seed','Rec/Track/Velo','Rec/Track/VeloTT','Rec/Track/Forward']
        for oc in other_containers:
           if evt[oc]: 
            if evt[oc].size()>0 : PRplot.disp_tracks(0,container=oc,opt2='cosmic')    
   
def draw_picture(): 
 session().setParameter('modeling.projection','')
 nregions = Page().fPage.getNumberOfRegions()
 nc = -1
 for i in range(nregions) : 
    Page().setCurrentRegion(i)      
    type = Region().fRegion.getTypeId().getName().getString()
    test = type != "SoRulerRegion" and type != "SoImageRegion" and type != "SoTextRegion"
    if not test : continue
    nc +=1
    if nc == 0:    
 # region 1: top view with Magnet, calo wireframe
     Camera().setPosition(0., 30000., 10000.)
     Camera().setHeight(14000.)
     Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
     Camera().setNearFar(1.,50000.)
     track_logic(0)
     if ui().parameterValue('Panoramix_PRViewer_input_visCalo.value')=='True' : PRplot.disp_CaloMuon(False) 
     else: PRplot.visMuonHits(tae='')
# add muon hits from all TAE 
     for b in ["/Event/Prev2/","/Event/Prev1/","/Event/Next1/","/Event/Next2/"]:
       if  evt[b+'DAQ/RawEvent'] :   PRplot.visMuonHits(b)
#
     vishits  = ui().parameterValue('Panoramix_PRViewer_input_vishits.value')=='True'
     visrichs = ui().parameterValue('Panoramix_PRViewer_input_visrichs.value')=='True'
     if vishits: 
          PRplot.visOTHitsX()
          Style().setColor('darkgreen')
          uiSvc().visualize('/Event/Raw/TT/Clusters')
          uiSvc().visualize('/Event/Raw/IT/Clusters')
     extramuons = ui().parameterValue('Panoramix_PRViewer_input_extramuons.value')
     if evt['Rec/Rich/RecoEvent/Offline'] and visrichs :  
         PRplot.RichHits()
     if not evt['Rec/Track/Muon'] and  extramuons: PRplot.disp_muonTracks()
     visMuonContour  = ui().parameterValue('Panoramix_PRViewer_input_visMuonContour.value')=='True'
     if visMuonContour:  
        for i in range(1,5):  Onlinemodule.muonStationContours(i)
     addinfo    = ui().parameterValue('Panoramix_PRViewer_input_addinfo.value')=='True'
     if not addinfo : Page().titleVisible(False)
     if nregions < 4 and addinfo :
       par={} 
       for k in range(1,9):  
        par[k] = float(ui().parameterValue('Panoramix_PRViewer_input_t'+str(k)+'.value'))
       overlay_runeventnr(par[1],par[2],par[3],par[4],par[5],par[6],par[7],par[8])
     elif nregions > 3 and addinfo : 
      overlay_runeventnr(t1=0.55,t2=0.0,t3=0.4,t4=0.09,i1=0.65,i2=0.3,i3=0.0,i4=0.0)
     ui().synchronize()
    if nc == 1 : 
 # region 2: Velo close up, xy
     rc = session().setParameter('modeling.trackStartz','-500.')
     track_logic(2)  
     Camera().setPosition(0.,0.,35000.)
     Camera().setOrientation(0.,0.,1.,0.)
     Camera().setOrientation(0.,0.,1.,0.)
     Camera().setNearFar(15000.,40000.)
     Camera().setHeight(300.)  
     ui().synchronize()
    if nc==2 : 
 # region 3: Velo rz
     VeloRZ()
     ui().synchronize()
 prefix = ''
 if os.environ.has_key('PANORAMIXPICTURES') : 
    prefix = os.environ['PANORAMIXPICTURES']+'/' 
 PRplot.wprint(prefix+'PanoramixEventDisplay_'+str(nc)+'.jpg',format='JPEG',quality='100',info=False)    
 tevt = "%05d" % (evt['DAQ/ODIN'].eventNumber())
 ttext = str(evt['DAQ/ODIN'].runNumber())+'_'+tevt+'_'
 PRplot.wprint(prefix+ttext+str(nc)+'.jpg',format='JPEG',quality='100',info=False)    
 Page().setCurrentRegion(0)   

 #PRplot.wprint(prefix+'PanoramixEventDisplay_'+str(nc)+'.ps',format='GL2PS',info=False)    

def VeloRZ():
  veloclosed = False
  mo = det['/dd/Conditions/Online/Velo/MotionSystem']
  test = mo.paramAsDouble('ResolPosLA')
  if abs(test) < 3 : veloclosed = True
  session().setParameter('modeling.projection','-ZR')
  Onlinemodule.VeloStationContours()
  Style().setColor('grey')
  data_collect(da(),'VeloCluster','isR==true')
  data_visualize(da())
  Style().setColor('yellow')
  session().setParameter('modeling.userTrackRange',  'true')
  session().setParameter('modeling.trackStartz',     '-300.')
  session().setParameter('modeling.trackEndz',       '850.')
  session().setParameter('modeling.precision',       '50')
  Camera().setPosition(300., 0., 100.)
  if veloclosed: Camera().setHeight(800.)
  else: Camera().setHeight(1400.)
  Camera().setOrientation(0., 1., 0., 0.)
  Camera().setNearFar(0.,120.)
  Viewer().removeAutoClipping()
# execute only if Rec/Header exist
  if evt['Rec/Track/Best'] :   
   rc = session().setParameter('Track.location','Rec/Track/Best')
   session().setParameter("modeling.what","VeloMeasurements")
# velo veloBack veloTT unique  seed match forward isDownstream  
   Style().setColor('green')
   data_collect(da(),'Track','velo==true&&unique==true')
   data_visualize(da())
   Style().setColor('greenyellow')
   data_collect(da(),'Track','upstream==true&&unique==true')
   data_visualize(da())
   Style().setColor('blue')
   data_collect(da(),'Track','long==true&&unique==true')
   data_visualize(da())
   if not veloclosed :
    Style().setColor('skyblue')
    data_collect(da(),'Track','downstream==true&&unique==true')
    data_visualize(da())
#
  session().setParameter("modeling.what","VeloMeasurements")
  dispMuonTracks()
  session().setParameter('modeling.projection','')

def loop_events(select=''):
 if ui().findWidget('Panoramix_Global_input_sleeptime') : 
  sleeptime    = int(ui().parameterValue('Panoramix_Global_input_sleeptime.value'))
  maxnev       = int(ui().parameterValue('Panoramix_Global_input_maxnev.value'))
 else :
  sleeptime = 60
  maxnev = -1
 session().setParameter('looping','run')
 n = maxnev
 while n != 0 : 
  uiSvc().nextEvent()
  if not physicstrigger()  : continue
  n-=1
  if not evt['DAQ/ODIN'] : 
   print 'PR_Viewer: Currently no event available, sleep '+ str(sleeptime) + 'seconds'
  else : 
   if select != '' :
    if select == 'GEC':
       if evt['Raw/OT/Times'].size()>5000: continue 
    elif not L0DU.channelDecisionByName(select): 
      print 'PR_Viewer: not triggered by ',select
   print evt['DAQ/ODIN']
   EraseEventAllRegions()
   draw_picture()
# check if loop should be stopped    
  nsteps = int(sleeptime)
  for i in range(nsteps) :   
   ui().synchronize()
   if session().parameterValue('looping') == 'stop' : 
    print 'Event loop stopped'
    n = 0
    break
   time.sleep(1)
 Page().setCurrentRegion(0) 
 uiSvc().nextEvent()
  
def execute(action='',version='single',bf='',select=''):
# check that widget exists
 if not ui().findWidget(dialog) :  defaults()
# clear regions before showing new event
 EraseEventAllRegions() 
# check if an event exist
 if not evt['DAQ/ODIN'] : 
   # try once
   uiSvc().nextEvent()
   if not evt['DAQ/ODIN'] : 
    print 'PR_Viewer: No event available'
    return 
# fit tracks if not done before to get measurements
 if appMgr.algorithm("PanoRecoSequencer").Enable  == False :
   print 'PR_Viewer: Refit tracks'
   ReFitTracks.refit()
   if 'RichRecInitOfflineSeq' in appMgr.algorithms():
    print 'PR_Viewer: make Rich Info'
    appMgr.algorithm('RichRecInitOfflineSeq').execute()

# default: single event display
# more colourful, with Velo
 if version == 'both' :
   exe_both(bf)
 else :
  curtitle = ui().currentPage().title.getValues()[0]
  title = 'LHCb Event Display '
  if version == 'single': title = 'LHCb Event Display'
  if curtitle != title or action == 'recreate':  
   if version=='single'    : createPage(title,1)
   if version=='with Velo' : createPage(title,4)
   onlineViews[title] =  'PR_Viewer.execute("","'+version+'")'
  draw_picture()
  # select = '' # for the moment  
  if action == 'loop' : loop_events(select)

def exe_both(bf):
# this is only called for looping and automatic picture updates
 if ui().findWidget('Panoramix_Global_input_sleeptime') : 
  sleeptime    = int(ui().parameterValue('Panoramix_Global_input_sleeptime.value'))
  maxnev       = int(ui().parameterValue('Panoramix_Global_input_maxnev.value'))
 else :
  sleeptime = 60
  maxnev = -1
 session().setParameter('looping','run')
 n = maxnev
 while n != 0 : 
  n-=1
  uiSvc().nextEvent()
  if not evt['DAQ/ODIN'] : 
   print 'PR_Viewer: No event available'
  else :
   print evt['DAQ/ODIN']
   if bf=='opt' :
    if not ui().findWidget('Viewer_P1') :     
     ui().createComponent('Viewer_P1','PageViewer','ViewerTabStack')
     ui().setCallback('Viewer_P1','collect','DLD','OnX viewer_collect @this@')
     ui().setCallback('Viewer_P1','destroy','DLD','ui_remove_from_currents @this@ OnX_viewers')  
    ui().setCurrentWidget(ui().findWidget('Viewer_P1')) 
   execute(action='',version='single')
   if bf=='opt' :
    if not ui().findWidget('Viewer_P2') :     
     ui().createComponent('Viewer_P2','PageViewer','ViewerTabStack')
     ui().setCallback('Viewer_P2','collect','DLD','OnX viewer_collect @this@')
     ui().setCallback('Viewer_P2','destroy','DLD','ui_remove_from_currents @this@ OnX_viewers')  
    ui().setCurrentWidget(ui().findWidget('Viewer_P2')) 
   execute(action='',version='with Velo')
# check if loop should be stopped    
# sleep in 1sec intervals
  nsteps = int(sleeptime)
  for i in range(nsteps) :   
   ui().synchronize()
   if session().parameterValue('looping') == 'stop' : 
    print 'Event loop stopped'
    n = 0
    break
   time.sleep(1)

def makewrl():
 # region 1: top view with Magnet
    Camera().setPosition(-12072.2, 14040.8, 1678.76)
    Camera().setHeight(14000.)
    Camera().setOrientation(-0.16303, -0.912314, -0.375639, 2.31207)
    Camera().setNearFar(7757.78,32808.3)
    Region().clear()
    track_logic(0)
    PRplot.visMuonHits(tae='')
    PRplot.wprint('Track_view','wrl')
    Region().clear()
    PRplot.visOTHitsX()
    PRplot.wprint('Hits_view','wrl')
    Region().clear()
    PRplot.disp_CaloMuon(False) 
    PRplot.wprint('CaloMuonhits_view','wrl')
    Region().clear()
    PRplot.RichHits()
    #PRplot.wprint('RichDigits_view','wrl')

def PanoMovie():
#  get primary vertex
    steps = 300
    dz    = 50.
 # region 1: top view with Magnet, calo wireframe
    Camera().setPosition(-12072.2, 14040.8, 1678.76)
    Camera().setHeight(14000.)
    Camera().setOrientation(-0.16303, -0.912314, -0.375639, 2.31207)
    Camera().setNearFar(7757.78,32808.3)

    prefix = '/media/Work/PanoMovie/img_'
    pvs = evt['Rec/Vertex/Primary']
    if pvs.size() < 1 :return
    pvpos = pvs[0].position()
    z  = pvpos.z()
    temp1 = session().parameterValue('modeling.userTrackRange')
    temp2 = session().parameterValue('modeling.trackEndz')
    temp3 = session().parameterValue('modeling.trackStartz')
    temp4 = session().parameterValue('modeling.lineWidth')
    rc = session().setParameter('modeling.userTrackRange','true')
    if temp4 == '' : rc = session().setParameter('modeling.lineWidth','3.')  
    rc = session().setParameter('Track.location','Rec/Track/Best')
    startz = '%7.2F'%(z)
    rc = session().setParameter('modeling.trackStartz',startz)
    for istep in range(steps):
     EraseEventAllRegions()
     Page().setCurrentRegion(0) 
     z += dz
     zstep = '%7.2F'%(z)
     rc = session().setParameter('modeling.trackEndz',zstep)
# velo veloBack veloTT unique  seed match forward isDownstream  
     if z > 900.:   rc = session().setParameter('modeling.trackEndz','900.')
     Style().setLineWidth(1.5) 
     Style().setColor('green')
     data_collect(da(),'Track','velo==true&&unique==true')
     data_visualize(da())
     rc = session().setParameter('modeling.trackEndz',zstep)     
     if z > 2500.:  rc = session().setParameter('modeling.trackEndz','2500.')
     Style().setColor('greenyellow')
     data_collect(da(),'Track','upstream==true&&unique==true')
     data_visualize(da())
     rc = session().setParameter('modeling.trackEndz',zstep)     
     if z > 12000.:   
       rc = session().setParameter('modeling.trackEndz','12000.')
       PRplot.disp_CaloMuon(False) 
     Style().setColor('blue')
     data_collect(da(),'Track','long==true&&unique==true')
     data_visualize(da())
     rc = session().setParameter('modeling.trackEndz',zstep)     
     dispMuonTracks()
     ui().synchronize()
     pict = prefix+'%(X)03d'%{'X':istep}
     PRplot.wprint(pict,format='JPEG',info=False)  
#ffmpeg -r 25 -b 1800 -i img_%03d.jpg -y -s xga -qcomp 1 -an aMovie.avi
