
import sys

# Have ROOT.py in batch mode (no ROOT GUI)
if sys.__dict__.has_key('argv'):
  sys.argv.append('Panoramix')
else:
  sys.argv = ['Panoramix']
# X11 : if the below "sys.argv.append('-b')" is commented,
# be sure to have in your job options :
#   OnXSvc.Threaded = true;
# in order to start also the OnX/Xt driver in threaded mode.
# This is needed because X11 ROOT graphic is launched in thread
# mode (XInitThread) without possible control on that.
# sys.argv.append('-b')

if sys.platform == 'win32':
  import GaudiPython  
  gaudimodule = GaudiPython 
else:
  import os
  disp = os.getenv("DISPLAY")
  import GaudiPython  
  gaudimodule = GaudiPython 
  # Restore the DISPLAY variable deleted  somewhere
  # in GaudiPython (ROOT.py ?) because of the "-b" trick.
  #if disp : os.putenv("DISPLAY",disp)

import atexit
#/////////////////////////////////////
#// Important Object getters :
#//  - the Gaudi IUserInterfaceSvc : uiSvc().
#//  - the OnX ISession : session().
#//  - the OnX IUI : ui().
#//  - the Lib data accessor : da().
#/////////////////////////////////////


# WARNING : due to Python wrapping consistency, do not use uiSvc()
#           to get OnX session or ui. Get them with :
#           import OnX
#           session = OnX.session()
#           ui = OnX.session().ui()
def uiSvc():
  svcLoc = GaudiPython.gbl.Gaudi.svcLocator()
  svc = GaudiPython.Helper.service(svcLoc, 'OnXSvc',1)
  return GaudiPython.InterfaceCast('IUserInterfaceSvc').cast(svc)

def runable():
  svcLoc = GaudiPython.gbl.Gaudi.svcLocator()
  svc = GaudiPython.Helper.service(svcLoc, 'OnXSvc',1)
  return GaudiPython.InterfaceCast('IRunable').cast(svc)

def soCnvSvc():
  svcLoc = GaudiPython.gbl.Gaudi.svcLocator()
  svc = GaudiPython.Helper.service(svcLoc,'SoConversionSvc',1)
  return GaudiPython.InterfaceCast('ISoConversionSvc').cast(svc)

def evtSvc():
  svcLoc = GaudiPython.gbl.Gaudi.svcLocator()
  svc = GaudiPython.Helper.service(svcLoc,'EventDataSvc',1)
  return GaudiPython.InterfaceCast(GaudiPython.gbl.IDataProviderSvc).cast(svc)

def partSvc():
  svcLoc = GaudiPython.gbl.Gaudi.svcLocator()
  svc = GaudiPython.Helper.service(svcLoc,'ParticlePropertySvc',1)
  return GaudiPython.InterfaceCast(GaudiPython.gbl.IParticlePropertySvc).cast(svc)

def histSvc():
  svcLoc = GaudiPython.gbl.Gaudi.svcLocator()
  svc = GaudiPython.Helper.service(svcLoc,'HistogramDataSvc',1)
  return GaudiPython.InterfaceCast(GaudiPython.gbl.IHistogramSvc).cast(svc)

def toolSvc():
  svcLoc = GaudiPython.gbl.Gaudi.svcLocator()
  svc = GaudiPython.Helper.service(svcLoc, 'ToolSvc',1)
  return GaudiPython.InterfaceCast('IToolSvc').cast(svc)

def getTool(typ,interface):
  name = typ
  parent = None
  itool = GaudiPython.Helper.tool(toolSvc(), typ, name, parent, True )
  return GaudiPython.InterfaceCast(interface).cast(itool)

import OnX

def session():
  return OnX.session()

def ui():
  return OnX.session().ui()
  
def da():
  return OnX.session().accessorManager()

def appMgr():
  return uiSvc().appMgr()

def algMgr():
  appMgr = uiSvc().appMgr()
  return GaudiPython.InterfaceCast(GaudiPython.gbl.IAlgManager).cast(appMgr)

def algorithm(aName):
  return GaudiPython.Helper.algorithm( algMgr(),aName)

def data(aName):
  svc = evtSvc()
  if not svc : return None
  return GaudiPython.Helper.dataobject(svc,aName)

#/////////////////////////////////////////////////////////////////////////////
# sys procedures :
#/////////////////////////////////////////////////////////////////////////////

def sys_import(aModule):
 import sys
 if sys.modules.has_key(aModule):
   reload(sys.modules[aModule])
 else:
   exec('import %s' % aModule)

def x(aModule):
  sys_import(aModule)
  
def sys_dump_main():
 import sys
 print dir(sys.modules['__main__'])

def sys_exists(name):
 try:
   eval(name)
   return 1
 except NameError:
   return 0

#/////////////////////////////////////////////////////////////////////////////
# session procedures :
#/////////////////////////////////////////////////////////////////////////////

# Deprecated, use the Style class.
def session_setColor(aSession,aRGB):
 aSession.setParameter('modeling.color',aRGB)

#/////////////////////////////////////////////////////////////////////////////
# Data accessor procedures :
#/////////////////////////////////////////////////////////////////////////////

# Deprecated, use the DA class.
def data_collect(aDA,aWhat,aCuts = ''):
 ss = GaudiPython.gbl.std.vector(str)()
 if aWhat == 'SceneGraph':
   ss.push_back('SceneGraph(@current@)')
 else:
   ss.push_back(aWhat)
 ss.push_back(aCuts)
 aDA.execute('collect',ss)
 del ss

def collect_scene_graphs(aDA,aViewer = '@current@',aCuts = 'highlight==true'):
 ss = GaudiPython.gbl.std.vector(str)()
 ss.push_back('SceneGraph('+aViewer+')')
 ss.push_back(aCuts)
 aDA.execute('collect',ss)
 del ss

# Deprecated, use the DA class.
def data_visualize(aDA):
 ss = GaudiPython.gbl.std.vector(str)()
 ss.push_back("where=@current@")
 aDA.execute('visualize',ss)
 del ss

# Deprecated, use the DA class.
def data_filter(aDA,aWhat,aFilter = ''):
 ss = GaudiPython.gbl.std.vector(str)()
 ss.push_back(aWhat)
 ss.push_back(aFilter)
 aDA.execute('filter',ss)
 del ss

# Deprecated, use the DA class.
def data_destroy(aDA):
 ss = GaudiPython.gbl.std.vector(str)()
 aDA.execute('destroy',ss)
 del ss

# Deprecated, use the DA class.
def data_dump(aDA,aMode = 'table'):
 ss = GaudiPython.gbl.std.vector(str)()
 ss.push_back(aMode)
 aDA.execute('dump',ss)
 del ss

# Deprecated, use the DA class.
def data_number(aDA):
 ss = GaudiPython.gbl.std.vector(str)()
 sret = aDA.execute('number',ss)
 del ss
 return sret

# Deprecated, use the DA class.
def data_set(aDA,aWhat,aValue):
 ss = GaudiPython.gbl.std.vector(str)()
 ss.push_back(aWhat)
 ss.push_back(aValue)
 aDA.execute('set',ss)
 del ss

# Deprecated, use the DA class.
def data_values(aDA,aWhat):
 ss = GaudiPython.gbl.std.vector(str)()
 ss.push_back(aWhat)
 sret = aDA.execute('values',ss)
 del ss
 return sret

# OnX Data Accessor Python front end class :
class DA:
  def __init__(self):
    self.fDA = OnX.session().accessorManager()
  def collect(self,aWhat,aCuts = ''):
    ss = GaudiPython.gbl.std.vector(str)()
    ss.push_back(aWhat)
    ss.push_back(aCuts)
    self.fDA.execute('collect',ss)
    del ss
  def collect_scene_graphs(self,aViewer = '@current@',aCuts = 'highlight==true'):
    ss = GaudiPython.gbl.std.vector(str)()
    ss.push_back('SceneGraph('+aViewer+')')
    ss.push_back(aCuts)
    self.fDA.execute('collect',ss)
    del ss
  def visualize(self):
    ss = GaudiPython.gbl.std.vector(str)()
    ss.push_back("where=@current@")
    self.fDA.execute('visualize',ss)
    del ss
  def filter(self,aWhat,aFilter = ''):
    ss = GaudiPython.gbl.std.vector(str)()
    ss.push_back(aWhat)
    ss.push_back(aFilter)
    self.fDA.execute('filter',ss)
    del ss
  def destroy(self):
    ss = GaudiPython.gbl.std.vector(str)()
    self.fDA.execute('destroy',ss)
    del ss
  def dump(self,aMode = 'table'):
    ss = GaudiPython.gbl.std.vector(str)()
    ss.push_back(aMode)
    self.fDA.execute('dump',ss)
    del ss
  def number(self):
    ss = GaudiPython.gbl.std.vector(str)()
    sret = self.fDA.execute('number',ss)
    del ss
    return sret
  def set(self,aWhat,aValue):
    ss = GaudiPython.gbl.std.vector(str)()
    ss.push_back(aWhat)
    ss.push_back(aValue)
    self.fDA.execute('set',ss)
    del ss
  def values(self,aWhat):
    ss = GaudiPython.gbl.std.vector(str)()
    ss.push_back(aWhat)
    sret = self.fDA.execute('values',ss)
    del ss
    return sret
  # shortcuts :
  def vis(self):
    self.visualize()


#/////////////////////////////////////////////////////////////////////////////
# Inventor procedures :
#/////////////////////////////////////////////////////////////////////////////
#  LCG Python wrapper does not work properly with Inventor / coin3d.
# Then, we use CoinPython which is the OpenScientist distribution
# of pivy ; a SWIG wrapping of coin3d. We also use the Python SWIG
# HEPVis wrapping coming with OpenScientist.
#  Becaue someone must be "wrapper consistent", the current SoPage
# (and then SoRegion, SoCamera) is accessed through the IUI which
# is wrapped in OnX (then wrapped by using SWIG too).
#/////////////////////////////////////////////////////////////////////////////

import CoinPython as iv
import HEPVis  # causes error, No module named HEPVis_SWIG_Python


class Viewer: # A class for common viewer manipulations.
  def __init__(self):
    self.fViewer = None
    widget = OnX.session().ui().currentWidget()
    if widget :
      self.fViewer = widget.cast_ISoViewer()
  def setFrame(self):
    self.fViewer.setFeedbackVisibility(1)
  def removeFrame(self):
    self.fViewer.setFeedbackVisibility(0)
  def setDecoration(self):
    self.fViewer.setDecoration(1)
  def removeDecoration(self):
    self.fViewer.setDecoration(0)
  def setAutoClipping(self):
    self.fViewer.setAutoClipping(1)
  def removeAutoClipping(self):
    self.fViewer.setAutoClipping(0)

class Page: # A class for common SoPage manipulations.
  def __init__(self):
    self.fPage = OnX.session().ui().currentPage()
  def setTitle(self,aTitle):
    self.fPage.title.setValue(aTitle)
  def titleVisible(self,aValue):
    self.fPage.titleVisible.setValue(aValue)
  def setCurrentRegion(self,aIndex):
    self.fPage.setCurrentRegion(aIndex)
  def currentRegion(self):
    return self.fPage.currentRegion()
  def connectCurrentRegion(self,aIndex):
    self.fPage.connectCurrentRegion(aIndex)    
  def createDisplayRegions(self,aCols,aRows,aIndex):
    self.fPage.createRegions("SoDisplayRegion",aCols,aRows,aIndex)
  def createPlotterRegion(self,aX = 0,aY = 0,aW = 0.4,aH = 0.4):
    self.fPage.createRegion("SoPlotterRegion",aX,aY,aW,aH)
  def createDisplayRegion(self,aX = 0,aY = 0,aW = 0.4,aH = 0.4):
    self.fPage.createRegion("SoDisplayRegion",aX,aY,aW,aH)

class Region: # A class for common SoRegion manipulations.
  def __init__(self):
    soPage = OnX.session().ui().currentPage()
    highlighted = soPage.getHighlightedRegion()
    if not highlighted :
      self.fRegion = None
    else:
      if soPage.isRootRegion(highlighted) == 0:
        self.fRegion = None
      else:
        self.fRegion = highlighted
  def setBackgroundColor(self,aColor):
    # aColor could be a common name like 'red', 'blue', etc...
    # or a string with RGBS, for example '1 1 0' for a yellow. 
    s = OnX.smanip.torgbs(aColor)    
    self.fRegion.color.set(s)
  def setBackgroundRGB(self,aR,aG,aB):
    self.fRegion.color.setValue(iv.SbColor(aR,aG,aB))
  def setTransformScale(self,aX,aY,aZ):
    tsf = self.fRegion.cast_SoDisplayRegion().getTransform()
    tsf.scaleFactor.setValue(iv.SbVec3f(aX,aY,aZ))
  def eraseEvent(self):
    self.fRegion.cast_SoDisplayRegion().getDynamicScene().removeAllChildren()
  def clear(self):
    self.fRegion.clear('')
  def write_hiv(self):
    node = self.fRegion.getWriteNode("scene")
    binary = 0
    altRep = 0
    HEPVis.SoTools.write(node,"out.hiv","hiv",binary,altRep)
  def write_wrl(self):
    node = self.fRegion.getWriteNode("scene")
    binary = 0
    altRep = 0
    HEPVis.SoTools.write(node,"out.wrl","wrl",binary,altRep)


class Camera:  # A class for common SoCamera manipulations.
  def __init__(self):
    self.fCamera = OnX.session().ui().currentPage().currentRegion().getCamera()
  def setHeight(self,aHeight):
    self.fCamera.height.setValue(aHeight)
  def setPosition(self,aX,aY,aZ):
    self.fCamera.position.setValue(iv.SbVec3f(aX,aY,aZ))
  def setNearFar(self,aNear,aFar):
    self.fCamera.nearDistance.setValue(aNear)
    self.fCamera.farDistance.setValue(aFar)
  def setOrientation(self,aX,aY,aZ,aAngle):
    self.fCamera.orientation.setValue(iv.SbRotation(iv.SbVec3f(aX,aY,aZ),aAngle))
  def pointAt(self,aX,aY,aZ):
    HEPVis.SoTools.pointAt(self.fCamera,iv.SbVec3f(aX,aY,aZ),iv.SbVec3f(0,1,0))
  def lookAt(self,aX,aY,aZ):
    HEPVis.SoTools.lookAt(self.fCamera,iv.SbVec3f(aX,aY,aZ),iv.SbVec3f(0,1,0))


#/////////////////////////////////////////////////////////////////////////////
# Style class :
#/////////////////////////////////////////////////////////////////////////////
class Style:
  def __init__(self):
    self.fSession = OnX.session()
  def useVisSvc(self): # Use the XML styles.
    self.fSession.setParameter('modeling.useVisSvc','true')
  def dontUseVisSvc(self): # Do not use the XML styles.
    self.fSession.setParameter('modeling.useVisSvc','false')
  # Color :
  def setColor(self,aColor):
    # aColor could be a common name like 'red', 'blue', etc...
    # or a string with RGBS, for example '1 1 0' for a yellow. 
    self.fSession.setParameter('modeling.color',aColor)
  def setRGB(self,aR,aG,aB):
    self.fSession.setParameter('modeling.color',"%g %g %g" % (aR,aG,aB))
  def setTransparency(self,aValue):
    # 0 is opaque, 1 fully transparent.
    self.fSession.setParameter('modeling.transparency',"%g" % (aValue))
  # Line :
  def setLineWidth(self,aValue):
    self.fSession.setParameter('modeling.lineWidth',"%d" % (aValue))
  # Marker :
  def setMarkerSize(self,aValue):
    self.fSession.setParameter('modeling.markerSize',"%d" % (aValue))
  def setMarkerStyle(self,aValue):
    self.fSession.setParameter('modeling.markerStyle',aValue)
  # Text :
  def setTextSize(self,aValue):
    self.fSession.setParameter('modeling.sizeText',"%d" % (aValue))
  def showText(self):
    self.fSession.setParameter('modeling.showText','true')
  def hideText(self):
    self.fSession.setParameter('modeling.showText','false')
  # Geometry :
  def setWireFrame(self):
    self.fSession.setParameter('modeling.modeling','wireFrame')
  def setSolid(self):
    self.fSession.setParameter('modeling.modeling','solid')
  def setOpened(self):
    self.fSession.setParameter('modeling.opened','true')
  def setClosed(self):
    self.fSession.setParameter('modeling.opened','false')
  def setShape(self,aValue):
    self.fSession.setParameter('modeling.shape',aValue)

#/////////////////////////////////////////////////////////////////////////////
# To start from python shell :
#/////////////////////////////////////////////////////////////////////////////
def start(aOptions = ''):  
  #  This function permits to starts a Panoramix session from
  # the python shell and gives control to the GUI.
  #  From the GUI, control is returned back to the
  # python shell by clicking in the 'File/Exit" of the
  # GUI. From the python shell someone returns to the GUI with :
  #    >>> Panoramix.runable().run()
  # or :
  #    >>> Panoramix.ui().steer()
  #
  # Usage :
  #     OS> <setup Panoramix>
  #     OS> python
  #   ( >>> import Panoramix as pmx
  #     >>> pmx.start()
  #   ( >>> pmx.start(<job options file>)  )
  #     >>> pmx.toui() # to give control to the GUI
  # or :
  #   UNIX> python -i $PANORAMIXROOT/scripts/Python/Panoramix.py
  #     >>> start()
  #   ( >>> start(<job options file>)  )
  #     >>> toui() # to give control to the GUI
  # or :
  #   UNIX> python -i $PANORAMIXROOT/scripts/Python/Panoramix.py $PANORAMIXROOT/options/Panoramix.opts
  #     >>> start()
  #     >>> toui() # to give control to the GUI
  # or :
  #   UNIX> python -i $PANORAMIXROOT/scripts/Python/Panoramix.py $PANORAMIXROOT/options/gb_test_detector.opts
  #    DOS> python -i %PANORAMIXROOT%\scripts\Python\Panoramix.py %PANORAMIXROOT%\options\gb_test_detector.opts
  #     >>> start()
  #     >>> toui() # to give control to the GUI
  #
  # WARNING :
  #  This function is intended to be executed from the python prompt
  # to create "from scratch" a Gaudi and a Panoramix session.
  # It is not advised to execute it within the Panoramix GUI
  # (from the GUI Python prompt or from a GUI Python callback) ;
  # especially if the program had not been started from Python.
  #

  jobopts = None
  if aOptions != '':
    jobopts = aOptions
  else:
    # Search a job option file passed in argument :
    import sys
    for arg in sys.argv:
      if arg.rfind('.opts') != -1 :
        jobopts = arg
        break
  if not jobopts :
    jobopts = '$PANORAMIXROOT/options/Panoramix.opts'

  outputlevel = 3 ## DEBUG 2, INFO 3, WARNING 4, ERROR 5, FATAL 6
  appMgr = GaudiPython.AppMgr(outputlevel,jobopts)
  appMgr.config()
  appMgr.initialize()

  # Restore the GaudiPython atexit :
  import atexit
  atexit.register(_atexit_)

def toui():
     exit_code = ui().steer()
     if exit_code == 999: 
       appMgr = GaudiPython.AppMgr()
       appMgr.stop()
       appMgr.finalize()
       exit()

def run(aOptions = ''):
  start(aOptions)
  toui()
  
#/////////////////////////////////////////////////////////////////////////////
# Check geom :
#/////////////////////////////////////////////////////////////////////////////
def checkGeom(aDetelem = '/dd/Geometry/MagnetRegion/Magnet/Magnet'):
  # To operate the below, it seems that at minimum we need
  # in the cmt requirement file :
  #   use DetDescChecks v* Det
  # and in the job options :
  #   ApplicationMgr.DLLs += {"DetDescChecks"};
  #   ApplicationMgr.TopAlg += { "VolumeCheckAlg" };
  #   VolumeCheckAlg.Volume = "/dd/Geometry/MagnetRegion/Magnet/Magnet";
  alg = algorithm('VolumeCheckAlg')
  if not alg :
    print 'alg VolumeCheckAlg not found.'
    return
  #print alg
  print 'Selected detector is ' + aDetelem
  alg.Volume = aDetelem
  alg.OutputLevel = 1
  alg.Shoots3D = 50
  alg.ShootsXY = 10
  alg.ShootsYZ = 0
  alg.ShootsZX = 0
  alg.initialize()
  alg.execute()
  alg.finalize()

#/////////////////////////////////////////////////////////////////////////////
# additional functions
#/////////////////////////////////////////////////////////////////////////////
def EraseEventAllRegions():             
 for region in range(Page().fPage.getNumberOfRegions()) :
      # Move to each region in turn
      Page().setCurrentRegion(region)
      rtype = Region().fRegion.getTypeId().getName().getString()
      test = rtype != "SoRulerRegion" and rtype != "SoImageRegion" and rtype != "SoTextRegion"
      if test :
        Region().eraseEvent()
 for region in range(Page().fPage.getNumberOfRegions()) :
      # go to first SoDisplayRegion
      Page().setCurrentRegion(region)
      rtype = Region().fRegion.getTypeId().getName().getString()
      if rtype == "SoDisplayRegion": break
