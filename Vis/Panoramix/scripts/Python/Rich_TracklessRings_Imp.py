from panoramixmodule import *

ring_type = 'ENN'
#ring_type = 'Markov'

def Visualize_Rich_TracklessRings_All(curRegion=False):

    Visualize_Rich_TracklessRings_Imp('/Event/Rec/Rich/'+ring_type+'/Offline/RingsAll','pink','pink',curRegion)

def Visualize_Rich_TracklessRings_Best(curRegion=False):

    #Page().setTitle('Best Trackless Rings')
    Visualize_Rich_TracklessRings_Imp('/Event/Rec/Rich/'+ring_type+'/Offline/RingsBest','red','red',curRegion)

def Visualize_Rich_TracklessRings_Isolated(curRegion=False):

    Visualize_Rich_TracklessRings_Imp('/Event/Rec/Rich/'+ring_type+'/Offline/RingsIsolated','green','green',curRegion)    

def Visualize_Rich_TracklessRings_Imp(ring_location,ring_color,center_color,curRegion=False):

    ui().echo("Visualising RICH Trackless Rings "+ring_location+" as "+ring_color+"/"+center_color)

    Style().dontUseVisSvc()

    # Save current color
    save_color = session().parameterValue('modeling.color')
    if curRegion :     
        ui().session().setParameter('modeling.RichRecRingMode','center')
        Style().setColor(center_color)
        uiSvc().visualize(ring_location);
        # Draw parts of the rings in HPD acceptance
        ui().session().setParameter('modeling.RichRecRingMode','anywhere')
        Style().setColor(ring_color)
        uiSvc().visualize(ring_location);
    else : # Draw in all regions
    # Draw in all regions
     for region in range(Page().fPage.getNumberOfRegions()) :
        
        # Move to each region in turn
        Page().setCurrentRegion(region)
    
        # Draw the centre of the rings
        ui().session().setParameter('modeling.RichRecRingMode','center')
        Style().setColor(center_color)
        uiSvc().visualize(ring_location);

        # Draw parts of the rings in HPD acceptance
        ui().session().setParameter('modeling.RichRecRingMode','anywhere')
        Style().setColor(ring_color)
        uiSvc().visualize(ring_location);

    # reset back
    Style().useVisSvc()
    Style().setColor(save_color)

def Visualize_Selected_Rich_TracklessRings(da):

    #ui().echo('Visualising selected RICH Trackless Rings')

    ring_color = 'red'

    # Save current color
    save_color = session().parameterValue('modeling.color')

    # Draw the centre of the rings
    session().setParameter('modeling.RichRecRingMode','center')
    Style().setColor(ring_color)
    data_visualize(da)

    # Draw parts of the rings in HPD acceptance
    session().setParameter('modeling.RichRecRingMode','anywhere')
    Style().setColor(ring_color)
    data_visualize(da)

    # Set drawing option back to default
    session().setParameter('modeling.RichRecRingMode','anywhere')

    # reset back
    Style().setColor(save_color)
