from Onlinemodule import *
import panoramixmodule
import DetView_module

it = det['/dd/Structure/LHCb/AfterMagnetRegion/T/IT']
XYZPoint = GaudiPython.gbl.ROOT.Math.XYZPoint
    
def createPage(title,nregions): 
 page = ui().currentPage()
 page.deleteRegions()
 Page().setTitle(title)
 Page().titleVisible(True)
 Page().createDisplayRegions(nregions,1,0)
 ui().currentWidget().cast_ISoViewer().setHeadlight(1)
 DetView_module.ITlayers()
                                      
def execute(what):
 curtitle = ui().currentPage().title.getValues()[0]
 if what == 'top/side' :
# setup top/side projections for IT
  title = 'Top                IT                 Side'
  if curtitle != title or action == 'recreate':  
   createPage(title,2)
   onlineViews[title] = 'AfterMagnetIT_Viewer.execute("'+what+'")'
 if what == 'topscaled' :
# setup top/side projections for IT
  title = 'Top View     IT     z scaled'
  if curtitle != title or action == 'recreate':  
   createPage(title,1)
   onlineViews[title] = 'AfterMagnetIT_Viewer.execute("'+what+'")'
  
 if action ==  'visualize'  : 
   ui().echo(' Visualize IT detector')
 # clear regions before showing new event
   for i in range(Page().fPage.getNumberOfRegions()) : 
    Page().setCurrentRegion(i)      
    Page().currentRegion().clear("dynamicScene")
    if i == 0: 
# Camera positioning top:
     Camera().setPosition(0., 5000., 8400.)
     Camera().setHeight(1300.)
     Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
     Camera().setNearFar(1.,100000.)
    if i == 1: 
# Camera positioning side:
     Camera().setPosition(0., 0., 8500.)
     Camera().setHeight(1800.)
     Camera().setOrientation(0.,-1.,0.,1.57)
     Camera().setNearFar(1.,100000.)
    Style().dontUseVisSvc()
    if what == 'topscale' : Region().setTransformScale(1.,1.,2.)
    for k in range(3) :  
      Style().setColor('orange')
      data_collect(da(),'STCluster','ITorTT=="IT"&&station=='+str(k+1)+'&&layer==1')
      data_visualize(da())
      data_collect(da(),'STCluster','ITorTT=="IT"&&station=='+str(k+1)+'&&layer==4')
      Style().setColor('yellow')      
      data_visualize(da())
      data_collect(da(),'STCluster','ITorTT=="IT"&&station=='+str(k+1)+'&&layer==2')
      data_visualize(da())
      data_collect(da(),'STCluster','ITorTT=="IT"&&station=='+str(k+1)+'&&layer==3')
      data_visualize(da())        
    Style().useVisSvc()
    if i == 0: 
     for ithit in evt['Raw/IT/Clusters'] : 
      channel = ithit.channelID()
      box     = it.findBox(channel)
      layer   = box.findLayer(channel)
      lhcbid  = GaudiPython.gbl.LHCb.LHCbID(channel)
      traj    = it.trajectory(lhcbid,ithit.interStripFraction())                        
      if layer.sinAngle() != 0 : 
        Style().setColor('yellow')     
      else : 
        Style().setColor('orange')     
      x =   (traj.beginPoint().x() + traj.endPoint().x() )/2.             
      y =   (traj.beginPoint().y() + traj.endPoint().y() )/2.             
      z =   (traj.beginPoint().z() + traj.endPoint().z() )/2.       
      aPoint = XYZPoint(x,y,z)   
      sc = session().setParameter('modeling.markerStyle','cross')  #plus, cross, star
      sc = session().setParameter('modeling.markerSize','9')  # 5,7,9
      sc = uiSvc().visualize(aPoint)  
# plot also tracks if any exists which are supposed to reacj IT:
    Style().setColor('blue')
    onlyIT = 'veloTTITOT==10||veloTTITOT==110||veloTTITOT==1110||veloTTITOT==1010'
    withOT = 'veloTTITOT==11||veloTTITOT==111||veloTTITOT==1011||veloTTITOT==1111'
    data_collect(da(),'Track',onlyIT+'||'+withOT)
    data_visualize(da())
    ui().synchronize()
    panoramixmodule.overlay_runeventnr()
# Dialog inputs : 
action    = ui().parameterValue('Panoramix_Global_input_action.value')

