from panoramixmodule import *

photons_color = 'maroon'

def Visualize_Rich_RecoPhotons(curRegion=False):

    #ui().echo('Visualising RICH reconstructed photon candidates')

    Style().dontUseVisSvc()

    # Save current color
    save_color = session().parameterValue('modeling.color')
    Style().setColor(photons_color)
    
    if curRegion :     uiSvc().visualize('/Event/Rec/Rich/RecoEvent/Offline/Photons')
    else : # Draw in all regions
     for region in range(Page().fPage.getNumberOfRegions()) :
        
        # Move to each region in turn
        Page().setCurrentRegion(region)

        # draw things
        uiSvc().visualize('/Event/Rec/Rich/RecoEvent/Offline/Photons')
    
    # reset back
    Style().useVisSvc()
    Style().setColor(save_color)

def Visualize_Selected_Rich_RecoPhotons(da):

    #ui().echo('Visualising associated RICH reconstructed photon candidates')
    
    # Save current color
    save_color = session().parameterValue('modeling.color')
    Style().setColor(photons_color)
    
    # draw things
    data_visualize(da)

    # reset back
    Style().setColor(save_color)
    
