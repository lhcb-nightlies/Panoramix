DEBUG = False
from panoramixmodule import *
import math,PRplot
XYZVector = GaudiPython.gbl.Math.XYZVector
XYZPoint = GaudiPython.gbl.Math.XYZPoint
clight = 299792458. # m/sec
globpoints = GaudiPython.gbl.std.vector(XYZPoint)()
dialog = 'SoEvent_Track_dialog'

from DaVinciPVTools import Tools as DVTools
relPVFinder  = getTool(DVTools.P2PVWithIPChi2, 'IRelatedPVFinder')
if not relPVFinder: relPVFinder = tsvc.create(DVTools.P2PVWithIPChi2, interface = 'IRelatedPVFinder')
DecayTreeTool  = getTool('PrintDecayTreeTool', 'IPrintDecayTreeTool')
if not  DecayTreeTool : DecayTreeTool  = tsvc.create('PrintDecayTreeTool', interface='IPrintDecayTreeTool')
poca  = getTool('TrajPoca','ITrajPoca')

MCParticle = gaudimodule.gbl.LHCb.MCParticle
Track      = gaudimodule.gbl.LHCb.Track
ltrack2mcpart     = linkedTo(MCParticle,Track,'Rec/Track/Best')   
veto = []
# for particle viewer
particle_container        = {}

colours  = ['blue',          'red',        'green',         'magenta',    'cyan',        'orange']
ccolours  = ['0. 0. 1.',      '1. 0. 0.',    '0. 1. 0.',    '1.,0.,1.',   '0. 1. 1,',    '1. 0.64 0.']
tcolours = ['0.39 0.58 0.93','0.94 0.5 0.5','yellow','0.9 0.5 0.9','0. 0.74 0.98', '0.9 0.5 0.5'] 


def drawTraj(part,spoint=None,epoint=None,col=None,lw=None):
# only for field free region, close to PV, does not work for Ks/Lambda outside Velo
 backward = False 
 ez     = 13000.
 if hasattr(part,'states'):
# track
  traj  =  gaudimodule.gbl.LHCb.TrackTraj(part)
  if not epoint: 
    backward = part.flag()==part.Backward
    if backward: epoint=XYZPoint(0.,0.,-1000.)
    else:        epoint=XYZPoint(0.,0.,1000.)
 elif part.proto() and part.particleID().abspid()!=22:
  track =  part.proto().track()  
  traj  =  gaudimodule.gbl.LHCb.TrackTraj(track)
 else: 
  lmom = part.momentum()
  mom  = XYZVector(lmom.x(),lmom.y(),lmom.z())
  xx = gaudimodule.gbl.std.pair('<double,double>')() 
  if part.particleID().abspid()!=22 : eVx = part.endVertex().position()
  else: 
    ez     = 13000.
    refP = part.referencePoint()
    lam = ( ez-refP.z() ) / lmom.pz()
    eVx = XYZPoint( refP.x()+lam*lmom.px(),refP.y()+lam*lmom.py(),ez)
  traj = gaudimodule.gbl.LHCb.LineTraj(eVx,mom,xx )
 a   = Double(0.001)
 dis = XYZVector()
# calulate closest distance to spoint
 s   = Double(0.0)
 success = poca.minimize(traj,s,spoint,dis,a)
 startz = ('%10.6F'%(traj.position(s).z())).replace(' ','')
 if hasattr(part,'particleID'):
   pname = partsvc.find(part.particleID()).name()
   print pname,', closest distance to vertex: IP=%5.2F(um), x=%5.2F, y=%5.2F, z=%5.2F(mm)'%(dis.r()/units.micrometer,traj.position(s).x(),traj.position(s).y(),traj.position(s).z())
# calulate closest distance to epoint
 if not epoint:
  if hasattr(part,'particleID'):
   if part.endVertex():                 ez = part.endVertex().position().z()
   if part.particleID().abspid() == 13: ez = 22000.
 if epoint:
  e   = Double(0.0)
  success = poca.minimize(traj,e,epoint,dis,a)
  ez = traj.position(e).z()
 endz = ('%10.6F'%(ez)).replace(' ','')
#
 if lw: 
   tmp  = session().parameterValue('modeling.lineWidth')
   session().setParameter('modeling.lineWidth',lw)
 if col: 
  tmpc = session().parameterValue('modeling.color')
  session().setParameter('modeling.color',col)

 temp1 = session().parameterValue('modeling.userTrackRange')
 temp2 = session().parameterValue('modeling.trackEndz')
 temp4 = session().parameterValue('modeling.trackStartz')
 rc = session().setParameter('modeling.userTrackRange','true')
 rc = session().setParameter('modeling.trackEndz',endz)
 rc = session().setParameter('modeling.trackStartz',startz)
 if backward:
  rc = session().setParameter('modeling.trackEndz',startz)
  rc = session().setParameter('modeling.trackStartz',endz)
 # if DEBUG: print part
 if DEBUG: print startz,endz
 Object_visualize(part)  
 if lw:  session().setParameter('modeling.lineWidth',tmp)
 if col: session().setParameter('modeling.color',tmpc)
 rc = session().setParameter('modeling.userTrackRange',temp1)
 rc = session().setParameter('modeling.trackEndz',temp2)
 rc = session().setParameter('modeling.trackStartz',temp4)
 
 
def makePartInfo(t1=0.2,t2=0.88,t3=0.2,t4=0.1,d3=False):
#  
   proj = ui().parameterValue('Panoramix_Particle_input_azoom.value')
   if proj=='stable':
      t1=0.01
      t2=0.78 
   soPage = ui().currentPage()
   soRegion = soPage.createRegion('SoTextRegion',t1,t2,t3,t4)
   soTextRegion = soRegion.cast_SoTextRegion()
   soTextRegion.verticalMargin.setValue(0)
   soTextRegion = soRegion.cast_SoTextRegion()
   for bee in particle_container:
    result = particle_container[bee]
    Bname = partsvc.find(bee.particleID()).name()
    tmp = ''
    for gd in bee.daughters():
       tmp+=' '+partsvc.find(gd.particleID()).name()
    txt =  Bname+'->'+tmp
    print txt
    soTextRegion.text.set1Value(0,txt)
    txt =  ' mass: [%5.3F +/- %5.3F] MeV/c2'%(result['mass'],result['masserror'])
    print txt
    soTextRegion.text.set1Value(1,txt)
    txt = ' decaylength: %5.3F mm'%(result['decaylength'])
    print txt
    soTextRegion.text.set1Value(2,txt)
    txt = ' tau: %5.3F ps'%(result['lifetime'])
    print txt
    soTextRegion.text.set1Value(3,txt)
    txt = ' pt: %5.3F MeV/c'%(result['pt'])
    print txt
    soTextRegion.text.set1Value(4,txt)
    soTextRegion.setPosition(t1,t2)
    soTextRegion.setSize(t3,t4)
    ui().synchronize()

def drawPVtracks(thePV,PVs,artist,close):
# this is only for PR purposes to make display nice
 pos = thePV.position()
 xv = pos.x()
 yv = pos.y()
 zv = pos.z()
 tmp  = session().parameterValue('modeling.lineWidth')
 tmpc = session().parameterValue('modeling.color')
 tmpp = session().parameterValue('modeling.precision')
 lpv = str( int(float(tmp)/20.+1) )
 session().setParameter('modeling.lineWidth',lpv)
 for at in thePV.tracks(): 
   if not at: continue # happens for mDST
   Style().setColor('lightgrey')
   if hasattr(at,'target'): atrack = at.target()
   else : atrack = at
   if not close: 
    firststate = atrack.states()[0]
    firststate.setX(xv)
    firststate.setY(yv)
    firststate.setZ(zv)
    if atrack.states().size() > 1:
     secstate = atrack.states()[1]
     tx = (secstate.x()-xv) /(secstate.z()-zv)
     ty = (secstate.y()-yv) /(secstate.z()-zv)
     firststate.setTx(tx)
     firststate.setTy(ty)
    Object_visualize(atrack)
   else:
    if not ui().findWidget(dialog) : PRplot.defaults()
    if DEBUG: print 'Tracktype',atrack.type(),atrack.flag()==atrack.Backward
    ptmin = float(ui().parameterValue('SoEvent_Track_input_ptmin.value'))
    if DEBUG: print 'ptcut',atrack.pt(),'<',ptmin
    if atrack.pt()<ptmin: continue
    xmax = float(ui().parameterValue('SoEvent_Track_input_xmax.value'))
    if DEBUG: print 'chi2cut',atrack.chi2PerDoF(),'>',xmax
    if atrack.chi2PerDoF()>xmax: continue
    drawTraj(atrack,spoint=pos,epoint=None)
 session().setParameter('modeling.lineWidth',tmp)
 session().setParameter('modeling.color',tmpc)
 session().setParameter('modeling.precision',tmpp)
#
def draw_daughters(p,nc,opos,force,close,linewidth,mclink) : 
 tmppos = session().parameterValue('modeling.posText')
 session().setParameter('modeling.lineWidth',linewidth)
 azoom  = ui().parameterValue('Panoramix_Particle_input_azoom.value')
 proto = None 
 abspid = p.particleID().abspid()
 print "working on particle ",abspid,opos.z()
 if p.daughters().size() != 0 : 
   Style().setColor(ccolours[nc])
 else : # must be a stable particle
   if tmppos == 'medium' : session().setParameter('modeling.posText','5')
   Style().setColor(tcolours[nc])
   proto = p.proto()
   if proto : veto.append(proto.key())
# set reference point
 if force or close :  
   if abspid == 13:                  Style().setRGB(1.,0.,1.) # Style().setColor('magenta')
   if abspid == 321:                 Style().setColor('red')
   if abspid == 2212:                Style().setColor('yellow')
   if abspid > 400 and abspid < 500: Style().setColor('green')
   if abspid == 121:                 Style().setColor('orange')
   if abspid == 22:                  Style().setColor('green')
# if mother particle, assume origin is PV
   # print abspid,opos, p.referencePoint()
   if opos and force: p.setReferencePoint(opos)
# else origin is endVertex of mother particle
 if abspid == 22:  p.setReferencePoint(opos)    
 if azoom != 'stable' and not close: Object_visualize(p)
 if close:                           drawTraj(p,spoint=opos,epoint=None)
 if proto and not force and not close: 
      atrack = proto.track()
      if abspid == 13:
       temp1 = session().parameterValue('modeling.userTrackRange')
       temp2 = session().parameterValue('modeling.trackEndz')
       rc = session().setParameter('modeling.userTrackRange','true')
       rc = session().setParameter('modeling.trackEndz','22000.')
       Object_visualize(atrack)   
       rc = session().setParameter('modeling.userTrackRange',temp1)
       rc = session().setParameter('modeling.trackEndz',temp2)
      else: Object_visualize(atrack)   
 session().setParameter('modeling.posText',tmppos)
 opos = False 
 if p.endVertex() : 
   evx = p.endVertex()
   Object_visualize(evx)
   opos = evx.position()
 if mclink == 'yes':
# check for link to mctruth via protoparticle to track to mcparticle
     proto = p.proto()   
     if proto : 
       t = proto.track()
       if t : 
         mcpartl = ltrack2mcpart.range(t)
         temp = session().parameterValue('modeling.color')
         Style().setColor('yellow')
         for mcp in mcpartl : 
           Object_visualize(mcp)
         Style().setColor(temp) 
 for d in p.daughters() :
    try:
     dd = d.target()
    except:
     dd = d
    draw_daughters(dd,nc,opos,force,close,linewidth,mclink)
def print_daughters(p) : 
   e        = p.momentum().E()/units.GeV
   pt       = p.pt()/units.GeV
   pid      = p.particleID() 
   ch       = pid.threeCharge()/3.
   partname = partsvc.find(pid).name()
   mass = p.measuredMass()/units.GeV
   massError = p.measuredMassErr()/units.GeV
   if massError == 0: printout = '%4i'%(p.key())+'%12s'%(partname)+'%12.2f'%(e)+'%10.2f'%(pt)+'%6i'%(ch)+'%10.3f'%(mass)+'        '
   else:              printout = '%4i'%(p.key())+'%12s'%(partname)+'%12.2f'%(e)+'%10.2f'%(pt)+'%6i'%(ch)+'%10.3f'%(mass)+'+/-%0.3f'%(massError)+'  '
   for x in p.daughters() : 
     printout +='%4i'%(x.key())
   print printout
   for d in p.daughters() :
    try:
     dd = d.target()
    except:
     dd = d
    print_daughters(dd)

def execute(artist=False):
# Dialog inputs : 
 azoom     = ui().parameterValue('Panoramix_Particle_input_azoom.value')
 scolor    = ui().parameterValue('Panoramix_Particle_input_color.value')
 linewidth = ui().parameterValue('Panoramix_Particle_input_linewidth.value')
 text      = ui().parameterValue('Panoramix_Particle_input_text.value')=='yes'
 textsize  = ui().parameterValue('Panoramix_Particle_input_textsize.value')
 textpos   = ui().parameterValue('Panoramix_Particle_input_textpos.value')
 candidate = ui().parameterValue('Panoramix_Particle_input_candidate.value')
 mclink    = ui().parameterValue('Panoramix_Particle_input_mclink.value')
 action    = ui().parameterValue('Panoramix_Particle_input_action.value')
 precision = ui().parameterValue('Panoramix_Particle_input_precision.value')
 otherPVs  = ui().parameterValue('Panoramix_Particle_input_otherPVs.value')=='yes'
 PVtracks  = ui().parameterValue('Panoramix_Particle_input_PVtracks.value')=='yes'
 force     = ui().parameterValue('Panoramix_Particle_input_force.value')=='yes'
 close     = ui().parameterValue('Panoramix_Particle_input_force.value')=='close'
 if not artist: 
   if ui().parameterValue('Panoramix_Particle_input_artistSetup.value')=='yes' : artist = True
 if artist: linewidth = '50'
 if close:  session().setParameter('modeling.noStates','true')
 if force: session().setParameter('modeling.partforce','true')
 else:     session().setParameter('modeling.partforce','false')
 session().setParameter('modeling.showCurve','true')
 session().setParameter('modeling.useExtrapolator','yes')
 page   = OnX.session().ui().currentPage()
 for n in range(page.getNumberOfRegions() ):
      region = page.getRegion(n)
      if not region: continue
      typ = region.getTypeId()
      if typ.getName() == 'SoDisplayRegion': break
 camera  = {} 

 nn = 0
 if candidate!='all' : 
   nn=int(candidate) 

 nc = -1
 for c in colours : 
  nc+=1
  if c == scolor : break
 Style().setColor(ccolours[nc])
 
 tscale = 1.
 if textpos != 'medium' :
  tscale = float(textpos)
 tmpl = str( 1+float(linewidth))
 if not artist: session().setParameter('modeling.lineWidth',tmpl)
 session().setParameter('modeling.sizeText',textsize)
 session().setParameter('modeling.posText',textpos)
 session().setParameter('modeling.precision',precision)
 if force : session().setParameter('modeling.partstartend','true')
 else     : session().setParameter('modeling.partstartend','false')

 if  text:
     session().setParameter('modeling.showText','true')
 else :     
     session().setParameter('modeling.showText','false')
 
 if action ==  'PrintPlot': 
    selection = ui().parameterValue('mainTree.selection')
    temp = selection.split('\n')
    sel  = temp[len[temp]-1]
    proj = ui().parameterValue('Panoramix_Particle_input_azoom.value')
    runnr  = evt['Rec/Header'].runNumber()
    evtnr  = evt['Rec/Header'].evtNumber()
    PRplot.wprint(sel+'-'+str(runnr)+'-'+str(evtnr)+'-'+proj+'.gif',format='GIF')
    PRplot.wprint(sel+'-'+str(runnr)+'-'+str(evtnr)+'-'+proj+'.jpg',format='JPEG',quality='100',info=False)       
 elif action !=  'setting' : 
  PVs = 'Rec/Vertex/Primary'
  particle_container.clear()
  relations  = None
  selection = ui().parameterValue('mainTree.selection')
  location  = selection.replace('\n','/')
  location  = location.replace('/Particles','')
  if location.find('Phys') < 0 and location.find('Turbo') < 0: 
   print 'no Phys or Turbo Container found on TES starting at',location
  else :   
 # check if relation table exists:
   relTable = '/'+location  + '/Particle2VertexRelations'
   rt = evt[relTable]
 #  print 'try ',relTable
   if rt:  relations = rt.relations()
 # else use Rec/Vertex and do by hand
   elif not evt[PVs]:
      print 'Warning, event tree, no container of PVs found'
   temp = location.split('/')  
   if temp[len(temp)-1].find('Particles')<0:  location  += '/Particles' 
   container = evt['/'+location]
   bestVertex={}
   firstParticle = None 
   for particle in container:
    if not firstParticle: firstParticle = particle
    pkey = particle.key()
    if relations:
     bestVertex[pkey] = relations[pkey].to()
     print 'take PV from ',relTable,' for particle ',pkey,' found PV ',bestVertex[pkey].key()
    else:
     bestVertex[pkey] = relPVFinder.relatedPV(particle, PVs)
     print 'take PV from ',PVs,' for particle ',pkey,' found PV ',bestVertex[pkey].key()
    oVx  = bestVertex[pkey].position()
   eVx  = firstParticle.endVertex().position()
   if len(bestVertex) == 0 :
     oVx = gaudimodule.gbl.Math.XYZPoint(eVx.x()+5.,eVx.y()+5.,eVx.z()+10.)
   if action ==  'visualize' and azoom != 'no' and container.size()>0 : 
    if not checkRulersPresent(): ui().executeScript('DLD','Panoramix layout_rulers')
    height = math.sqrt( (eVx.x()-oVx.x())*(eVx.x()-oVx.x()) + (eVx.y()-oVx.y())*(eVx.y()-oVx.y())) * 5.
    if azoom == 'xy-proj' : 
     Camera().setPosition(oVx.x(),oVx.y(),oVx.z())
     Camera().setHeight(height)
     Camera().setOrientation(0,1,0,3.14)
     Camera().setNearFar(-5000.,5000.)
     tsf = region.cast_SoDisplayRegion().getTransform()
     tsf.scaleFactor.setValue(iv.SbVec3f(1.,1.,1.))  
     camera  = {'position':[oVx.x(),oVx.y(),oVx.z()],'orientation':[0,1,0,3.14],'nearfar':[-5000.,5000.],'height':height}
    if azoom == 'zx-proj' : 
     height = abs(eVx.z()-oVx.z()) * 5.
     Camera().setPosition(oVx.x(),oVx.y(),oVx.z()+height*0.5)
     Camera().setHeight(height)
     Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
     Camera().setNearFar(-5000.,5000.)
     tsf = region.cast_SoDisplayRegion().getTransform()
     tsf.scaleFactor.setValue(iv.SbVec3f(5.,5.,1.))  
     camera  = {'position':[oVx.x(),oVx.y(),oVx.z()+height*0.5],'orientation':[-0.57735, -0.57735, -0.57735, 2.0943],'nearfar':[-5000.,5000.],'height':height}
    if azoom == 'zy-proj' : 
     height = abs(eVx.z()-oVx.z()) * 5.
     Camera().setPosition(oVx.x(),oVx.y(),oVx.z()+height*0.5)
     Camera().setHeight(height)
     Camera().setOrientation(0., -1., 0., 1.57)
     Camera().setNearFar(-5000.,5000.)
     tsf = region.cast_SoDisplayRegion().getTransform()
     tsf.scaleFactor.setValue(iv.SbVec3f(5.,5.,1.))  
     camera  = {'position':[oVx.x(),oVx.y(),oVx.z()+height*0.5],'orientation':[0., -1., 0., 1.57],'nearfar':[-5000.,5000.],'height':height}
   for p in container :
# temporary hack, need to figure out how to find the mother of all particles    
    if p.daughters().size() == 0 : continue
    if candidate!='all' : 
     if p.key()!=int(candidate) : continue 
    if action ==  'printTree' : 
     print 'decay tree tool',p
     DecayTreeTool.printTree(p,-1)
    if action == 'dump' :
     print '  key      name    energy[GeV]  pt[GeV]  charge   mass[GeV]      daughters'
     print_daughters(p)
    if action ==  'visualize' :
     draw_daughters(p,nc,oVx,force,close,linewidth,mclink)
     Style().setColor(tcolours[nc])
     if force or close: Style().setColor('cyan')
     if bestVertex.has_key(p.key()) : 
       thePV = bestVertex[p.key()]
       Object_visualize(thePV)
       # estimate lifetime and decay length
       prvx  = thePV.position() 
       tmp = p.endVertex()
       if tmp:
        evx   = p.endVertex().position() 
        prvxl = XYZVector(prvx.x(),prvx.y(),prvx.z())
        evxl  = XYZVector(evx.x(),evx.y(),evx.z())
        dcl   = evxl - prvxl
       # gamma c tau = decay length  , gamma c = E/m
        ltime = dcl.R() /units.m / p.momentum().E() * p.measuredMass() / clight /1.E-12
        print 'decay length: %5.2F mm      lifetime: %5.2F ps      pt: %5.2F MeV/c'%(dcl.R()/units.mm,ltime,p.pt()/units.MeV) 
        if not particle_container.has_key(p) : particle_container[p]={}
        particle_container[p]['mass']        = p.measuredMass()/units.MeV
        particle_container[p]['masserror']   = p.measuredMassErr()/units.MeV
        particle_container[p]['decaylength'] = dcl.R()/units.mm 
        particle_container[p]['lifetime']    = ltime
        particle_container[p]['pt']          = p.pt()/units.MeV
        particle_container[p]['camera']      = camera
     if PVtracks: 
          drawPVtracks(bestVertex[p.key()],PVs,artist,close)
# for better visibility
          Style().setColor('cyan')
          Object_visualize(bestVertex[p.key()])
     if otherPVs: 
      Style().setColor('seagreen')
      for aPV in evt['/Event/Rec/Vertex/Primary']:
       delz = aPV.position().z() - thePV.position().z() 
       if abs(delz)>0.5: Object_visualize(aPV)
     nc+=1
     if nc>len(colours)-1 : nc = 0
   if text: 
     overlay_runeventnr()
     makePartInfo()











