#
#  This scripts is intended to bypass on Windows the PATH limitation problem
#  when starting the Panoramix program.
#
#  Usage>
#    DOS> cd <path>\Vis\Panoramix\<version>\cmt
#    DOS> python ..\scripts\Python\pmx_run.py Panoramix_main.exe <options>
#

import sys
if sys.platform != 'win32':
  print 'pmx_run.py is for Windows platforms'
  sys.exit()

import os
environ = {}

for line in os.popen('cmt setup -bat -quiet').readlines():
	env = line[4:line.find('=')]
	val = line[line.find('=')+1:-1]
	if env : environ[env] = val

for key in environ.keys() :
    # replace all occurences of %...% by their value
    while environ[key].find('%') != -1 :
        value = environ[key]
        var = value[value.find('%')+1:value.find('%',value.find('%')+1)]
        if environ.has_key(var) :
            val = environ[var]
        else :
            val = os.getenv(var)
            if not val : val = ''
        environ[key] = value.replace('%'+var+'%',val)
    # set the environment
    os.putenv(key,environ[key])

# append path with ld_library_path
os.putenv('PATH',environ['PATH']+';'+environ['LD_LIBRARY_PATH'])
# execute command
command = '..\\'+environ['CMTCONFIG']+'\\'+sys.argv[1]+' '
for arg in sys.argv[2:]:
    command = command + arg + ' '
os.system(command)
    
