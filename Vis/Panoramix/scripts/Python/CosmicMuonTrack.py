from panoramixmodule import *
from ROOT import Double
newcont   = GaudiPython.makeClass('KeyedContainer<LHCb::Track,Containers::KeyedObjectManager<Containers::hashmap> >')
Track     = GaudiPython.gbl.LHCb.Track
LHCbID    = GaudiPython.gbl.LHCb.LHCbID
State     = GaudiPython.gbl.LHCb.State
# to force loading of dictionary for XYZTPoint
dummy = GaudiPython.gbl.MuonTrack()
XYZTPoint = GaudiPython.gbl.Gaudi.XYZTPoint
location  = '/Event/Rec/Muon/Track'
ld = {}

m_cosmicTool = tsvc.create('MuonCombRec',interface='IMuonTrackRec') 

def store_track(track):
  pos = XYZTPoint()
  t = Track()
  GaudiPython.setOwnership(t,False)
  s1 = State()
  GaudiPython.setOwnership(s1,False)
  s2 = State()
  GaudiPython.setOwnership(s2,False)
  z1 = 7000.
  track.extrap(z1,pos)   
  x1,y1 = pos.x(),pos.y()
  z2 = 20000.
  track.extrap(z2,pos)   
  x2,y2 = pos.x(),pos.y()
  s1.setX(x1)
  s1.setY(y1)
  s1.setZ(z1)
  s1.setTx( track.sx() )
  s1.setTy( track.sy() )
  s1.setQOverP(0.00001)
  t.addToStates(s1)
  s2.setX(x2)
  s2.setY(y2)
  s2.setZ(z2)
  s2.setTx( track.sx() )
  s2.setTy( track.sy() )
  s2.setQOverP(0.00001)
  t.addToStates(s2)
  for hit in track.getHits():
     tile = hit.tile()
     t.addToLhcbIDs(LHCbID(tile))   
  if not evt[location] : 
   cosmic_cont = newcont()
   GaudiPython.setOwnership(cosmic_cont,False)
   cosmic_cont.add(t)      
   evt.registerObject(location,cosmic_cont)
  else: 
   evt[location].add(t)        

def searchOrPlot(sel):   
# main entry to muon cosmic track
  while 1>0:
   if not evt['DAQ/ODIN'] : break  
   if sel : appMgr.run(1)
   muonTracks = m_cosmicTool.tracks()
   if not sel or muonTracks.size() > 0 : break 
  if evt['DAQ/ODIN']  :
   #for tr in muonTracks:
   #  store_track(tr)      
   m_cosmicTool.copyToLHCbTracks()


def visualizeCosmicMuonTrack(flag):
 location = '/Event/Rec/Muon/Track'
 if not evt[location] or flag:
# try to find a muon track
   searchOrPlot(flag)
   if evt[location] : 
    found = False
    curtitle = ui().currentPage().title.getValues()[0]
    for t in onlineViews :
     if curtitle == t : 
      cmd = onlineViews[t]
      if cmd.find('(') > 0 : 
       m = cmd[:cmd.find('.')]
       ld[m] = sys.modules[m]
       eval(cmd,ld)   
      else : 
       x(cmd)
      found = True
      break 
    if not found :
     Page().currentRegion().clear("dynamicScene")
 if evt[location] : 
    thisPage =  ui().currentPage()
    session().setParameter('modeling.userTrackRange','true')
    session().setParameter('modeling.trackStartz','5000.')
    session().setParameter('modeling.trackEndz','20000.')  
    session().setParameter('Track.location',location)
    for i in range(Page().fPage.getNumberOfRegions()) : 
     Style().setColor('green')
     Page().setCurrentRegion(i)      
     uiSvc().visualize(evt[location])
     session().setParameter('modeling.what','clusters')
     Style().setColor('yellow')
     muonloc = 'Raw/Muon/Coords'
     for b in ["Prev3","Next3","Prev2","Next2","Prev1","Next1"] :  
      if evt[b+'/DAQ/RawEvent']:
       if  evt[b+'/'+muonloc] : uiSvc().visualize(evt[b+'/'+muonloc])      
     #data_collect(da(),'Track','')
     #data_visualize(da())
     #session().setParameter('modeling.what','no')
     ui().synchronize()

