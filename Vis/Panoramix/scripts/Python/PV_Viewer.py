from Onlinemodule import *
import panoramixmodule
# Dialog inputs : 
offlinePV   = ui().parameterValue('Panoramix_PV_input_offlinePV.value')
HltPV       = ui().parameterValue('Panoramix_PV_input_HltPV.value')
MCPV        = ui().parameterValue('Panoramix_PV_input_MCPV.value')
ocolor      = ui().parameterValue('Panoramix_PV_input_ocolor.value')
hcolor      = ui().parameterValue('Panoramix_PV_input_hcolor.value')
mccolor     = ui().parameterValue('Panoramix_PV_input_mccolor.value')
action      = ui().parameterValue('Panoramix_PV_input_action.value')

def createPage(title): 
 page = ui().currentPage()
 page.deleteRegions()
 page.title.set(title)
 page.setTitleVisible(True)
 page.createRegions(1,2,0)
 ui().currentWidget().cast_ISoViewer().setHeadlight(1)
 Style().setWireFrame()
 Viewer().setFrame()
 Viewer().removeAutoClipping()
 Style().setColor('black')
                  
 # Setup region (a page can have multiple drawing region) :
 page.setCurrentRegion(0)
 ui().executeScript('DLD','OnX region_addFrame 0.1 grey')

 page.setCurrentRegion(1)
 Region().setTransformScale(10.,10.,1.)
 ui().executeScript('DLD','OnX region_addFrame 10 grey')
                    
def execute(what=""):
 curtitle = ui().currentPage().title.getValues()[0]
 title = 'PrimaryVertices'
 if curtitle != title or action == 'recreate':  
# setup xy projections for all 5 muon stations on one page
   createPage(title)
   onlineViews[title] = 'PV_Viewer'
 if action ==  'visualize':
  Page().setCurrentRegion(0)
  session().setParameter('modeling.modeling','wireFrame')
  Camera().setPosition(0., 0., 0.)
  Camera().setHeight(1.0)
  Camera().setOrientation(0., 0., 1., 0.)
  Camera().setNearFar(-150.,50.)
  Style().setColor('steelblue')
  uiSvc().visualize('/Event/Rec/Vertex/Primary')  
 #
  Style().setColor('green')
  uiSvc().visualize('/Event/Hlt2/VertexReports/PV3D')

  if evt['MC/Particles']:
   Style().setColor('yellow')
   data_collect(da(),'MCParticle','parent==\'nil\'&&charge==0')
   data_visualize(da())

  Page().setCurrentRegion(1)
  Camera().setPosition(0., 50., -10.)
  Camera().setHeight(163)
  Camera().setOrientation(-0.57735026, -0.57735026, -0.57735026,  2.0943)
  Camera().setNearFar(0.,100.)

  Style().setColor('steelblue')
  uiSvc().visualize('/Event/Rec/Vertex/Primary')  
 #
  Style().setColor('green')
  uiSvc().visualize('/Event/Hlt2/VertexReports/PV3D')
  if evt['MC/Particles']:
   Style().setColor('yellow')
   data_collect(da(),'MCParticle','parent==\'nil\'&&charge==0')
   data_visualize(da())
 elif action == 'dump':
   sys_import('jtat_VertexInfo')  

 Style().useVisSvc()
 panoramixmodule.overlay_runeventnr()

execute()


