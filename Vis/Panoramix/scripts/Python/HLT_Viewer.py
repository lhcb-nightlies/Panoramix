from panoramixmodule import *
from HLTPanoramix import *

item = ui().parameterValue('Panoramix_HLT_input_action.value')

if item == 'HLTHadron':
    ok = True
    d_lhcb('wh')
    s_modeling('s')
    d_hlt('L0TriggerHadron','red','ids')
    s_color('yellow')
    d_cluster('hcal')
    s_color('green')
    d_cluster('it')
    d_cluster('ot')
    d_hlt('HadSingleTRForward','blue','fscone')
if item == 'HLTMuon':
    ok = True
    d_lhcb('wh')
    s_modeling('s')
    d_hlt('L0TriggerMuon','red','ids')
    s_color('yellow')
    d_cluster('hcal')
    s_color('green')
    d_cluster('it')
    d_cluster('ot')
    d_hlt('HadSingleMuon','blue','fscone')
