#
#  This is used by the InputDialog palettes that uses
# Python on their ok activate callback.
#  In this case, OnX builds an "on the fly" Python script
# that wraps the user Python "activate" code with some
# Python code that gets the input of the widgets.
#  This on-the-fly script does, with OSC-15, an "import OnX",
# which have then to be reemulated with the LCG Python wrapping
# of IUI.
#  We point out that the OnX.py coming with OnX uses a SWIG wrapping
# of IUI, etc... that may clash with the LCG one coming from OnXSvc.
# The OnX.py of OnX is then not used (for long now) in Panoramix.
#  The on-the-fly Python generation has probably to be revisited
# in further version of OnX to avoid this "import OnX".
#
#  For an example of usage of Python on the ok callback of an
# input dialog see the files :
#   examples/Python/Examples.onx 
#   examples/Python/Test.onx (in fact commented out in the upper).
#   examples/Python/gb_test_dialog_py.onx
#
#   G.Barrand 9 Oct 2006.
#


import pmx

def ui_parameterValue(aWhat):
  import gaudimodule
  s = str()
  #FIXME : in the below, the second argument is not yet wrapped correctly :
  # pmx.ui().parameterValue(aWhat,s)
  s = gaudimodule.gbl.OnXSvc.Helpers.parameterValue(pmx.ui(),aWhat)
  return s

