from panoramixmodule import *

centre_point_color = 'skyblue'
ring_in_acc_color  = 'blue'
ring_out_acc_color = 'darkblue'

def Visualize_Rich_RecoRings():

    #ui().echo('Visualising RICH CK rings as predicted by reconstructed tracks')

    Style().dontUseVisSvc()

    # Save current color
    save_color = session().parameterValue('modeling.color')

    ring_location = '/Event/Rec/Rich/RecoEvent/Offline/SegmentHypoRings'

    # Draw in all regions
    for region in range(Page().fPage.getNumberOfRegions()) :
        
        # Move to each region in turn
        Page().setCurrentRegion(region)

        # Draw the centre of the rings
        ui().session().setParameter('modeling.RichRecRingMode','center')
        Style().setColor(centre_point_color)
        uiSvc().visualize(ring_location);

        # Draw parts of the rings in HPD acceptance
        ui().session().setParameter('modeling.RichRecRingMode','inside')
        Style().setColor(ring_in_acc_color)
        uiSvc().visualize(ring_location);

        # Draw parts of the rings outside HPD acceptance
        ui().session().setParameter('modeling.RichRecRingMode','outside')
        Style().setColor(ring_out_acc_color)
        uiSvc().visualize(ring_location);
    
    #data_visualize(da())
    Style().useVisSvc()

    # reset back
    ui().session().setParameter('modeling.RichRecRingMode','anywhere')
    Style().setColor(save_color)

def Visualize_Selected_Rich_RecoRings(da):

    #ui().echo('Visualising associated RICH CK rings as predicted by reconstructed tracks')

    # Save current color
    save_color = session().parameterValue('modeling.color')
        
    # Draw the centre of the rings
    session().setParameter('modeling.RichRecRingMode','center')
    Style().setColor(centre_point_color)
    data_visualize(da)
    
    # Draw parts of the rings in HPD acceptance
    session().setParameter('modeling.RichRecRingMode','inside')
    Style().setColor(ring_in_acc_color)
    data_visualize(da)
    
    # Draw parts of the rings outside HPD acceptance
    session().setParameter('modeling.RichRecRingMode','outside')
    Style().setColor(ring_out_acc_color)
    data_visualize(da)

    # Set drawing option back to default
    session().setParameter('modeling.RichRecRingMode','anywhere')
    Style().setColor(save_color)
    
