from panoramixmodule import *
import time
                                 
# Dialog inputs : 
ecalenergy    = ui().parameterValue('Panoramix_Calo_input_ecalenergy.value')
hcalenergy    = ui().parameterValue('Panoramix_Calo_input_hcalenergy.value')
spdcalenergy  = ui().parameterValue('Panoramix_Calo_input_spdcalenergy.value')
prscalenergy  = ui().parameterValue('Panoramix_Calo_input_prscalenergy.value')
ecolor    = ui().parameterValue('Panoramix_Calo_input_ecolor.value')
hcolor    = ui().parameterValue('Panoramix_Calo_input_hcolor.value')
scolor    = ui().parameterValue('Panoramix_Calo_input_scolor.value')
pcolor    = ui().parameterValue('Panoramix_Calo_input_pcolor.value')
nevents   = ui().parameterValue('Panoramix_Calo_input_nevents.value')
action    = ui().parameterValue('Panoramix_Calo_input_action.value')
# Camera positioning :
Camera().setPosition(0., 0., 20000.)
Camera().setHeight(12000.)
Camera().setOrientation(0.,0.,1.,0.)
Camera().setNearFar(1.,100000.)

if action ==  'visualize'  : 
 ui().echo(' Visualize Calo Digits')

 Style().dontUseVisSvc()
 if hcalenergy != 'n' :
  Style().setColor(hcolor)
  data_collect(da(),'HcalDigits','(e>'+hcalenergy+')')
  data_visualize(da())
 if ecalenergy != 'n' :
  Style().setColor(ecolor)
  data_collect(da(),'EcalDigits','(e>'+ecalenergy+')')
  data_visualize(da())
 if prscalenergy != 'n' :
  Style().setColor(pcolor)
  data_collect(da(),'PrsDigits','(e>'+prscalenergy+')')
  data_visualize(da())
 if spdcalenergy != 'n' :
  Style().setColor(scolor)
  data_collect(da(),'SpdDigits','(e>'+spdcalenergy+')')
  data_visualize(da())
 Style().useVisSvc()
 overlay_runeventnr()
if action ==  'onlineEcal'  : 
 ui().echo(' loop over '+str(nevents)+' events')
 # event loop
 for n in range(nevents) :
  uiSvc().nextEvent()
  x('truf_EcalView')
  ui().synchronize()
  time.sleep(1)  
if action ==  'onlineHcal'  : 
 ui().echo(' loop over '+str(nevents)+' events')
 # event loop
 for n in range(nevents) :
  uiSvc().nextEvent()
  x('truf_HcalView')
  ui().synchronize()
  time.sleep(1)  

