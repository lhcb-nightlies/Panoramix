from panoramixmodule import *

pixel_color = 'yellowgreen'

def Visualize_Rich_MCPixels():
    
    #ui().echo('Visualising all MC RICH pixels')

    Style().dontUseVisSvc()

    # Save current color
    save_color = session().parameterValue('modeling.color')
    Style().setColor(pixel_color)
     
    # Draw in all regions
    for region in range(Page().fPage.getNumberOfRegions()) :
        
        # Move to each region in turn and draw pixels
        Page().setCurrentRegion(region)
        uiSvc().visualize('/Event/Raw/Rich/Digits')
    
    # reset back
    Style().useVisSvc()
    Style().setColor(save_color)
    
def Visualize_Selected_Rich_MCPixels(da):
    
    #ui().echo('Visualising selected MC RICH pixels')

    # Save current color
    save_color = session().parameterValue('modeling.color')
    Style().setColor(pixel_color)
     
    # Draw things    
    data_visualize(da)

    # reset back
    Style().setColor(save_color)
    
