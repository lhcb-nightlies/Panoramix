from panoramixmodule import *
fitterTool = getTool('TrackMasterFitter','ITrackFitter')
if not fitterTool : fitterTool = tsvc.create('TrackMasterFitter',   interface='ITrackFitter')

def refit(tracklocation = ''):
 if tracklocation == '': tracklocation = session().parameterValue('Track.location')
 if tracklocation == '': tracklocation = 'Rec/Track/Best'
 if  evt[tracklocation]:
  for atrack in evt[tracklocation]:
   ##if atrack.fitStatus() == 1:
   ##  if atrack.type() != atrack.Velo:
       rc = fitterTool.fit(atrack)
