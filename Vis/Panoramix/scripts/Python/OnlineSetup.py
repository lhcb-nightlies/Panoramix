from panoramixmodule import *

# need one event, otherwise setup fails because of problem with configurable
if not evt['DAQ']  : 
 print '*** Setting up online menus, please be patient ***' 
 uiSvc().nextEvent()
 try: 
  test = evt['Raw/Prs/Digits']
 except:
  test = 1
 try:
  test = evt['Raw/Spd/Digits']
 except:
  test = 1
 try: 
  test = evt['Raw/Hcal/Digits']
 except:
  test = 1
 try: 
  test = evt['Raw/Ecal/Digits']
 except:
  test = 1
 try: 
   test = evt['Raw/Muon/Coords']
 except:
   test = 1
 print '++ ignore the potential decoding error messages above !' 
 print '++ happens only once; some bug in Gaudi/LHCb software; Panoramix is innocent' 

if evt['DAQ']  : 
 for dialog in onlinetopics :
  if not ui().findWidget(dialog) :  ui().showDialog(onlinetopics[dialog],dialog)

 ui().synchronize()
 for dialog in onlinetopics :
  if ui().findWidget(dialog) : ui().findWidget(dialog).hide()

else : 
 print 'Online Setup failed, no events. Try again later.'
