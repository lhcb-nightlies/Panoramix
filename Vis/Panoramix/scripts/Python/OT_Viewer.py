from panoramixmodule import *
import PRplot,Calo_Viewer

def setup():
 PRplot.Magnet_view()
 session().setParameter('modeling.modeling','wireFrame') 
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Hcal')
 #uiSvc().visualize('/dd/Structure/LHCb/AfterMagnetRegion/T/OT')
 session().setParameter('modeling.modeling','solid')
def view3d(location):
 ecolor        = session().parameterValue('Panoramix_Calo_input_ecolor')
 if ecolor == '' : Calo_Viewer.defaults()
 ecolor        = session().parameterValue('Panoramix_Calo_input_ecolor')
 hcolor        = session().parameterValue('Panoramix_Calo_input_hcolor')
 ecalenergy    = session().parameterValue('Panoramix_Calo_input_ecalenergy')
 hcalenergy    = session().parameterValue('Panoramix_Calo_input_hcalenergy')


 Style().setRGB(0.61,1.,0.63)    
 uiSvc().visualize('/Event/Raw/OT/Times') 
 Style().setRGB(0.,1.,0.2) 
 uiSvc().visualize(location) 
 Style().setRGB(0.85,0.73,0.0)    
 uiSvc().visualize('/Event/Calo/Track/Backward') 
 uiSvc().visualize('/Event/Calo/Track/Forward') 
 Style().setColor(hcolor)
 data_collect(da(),'HcalDigits','(e>'+hcalenergy+')')
 data_visualize(da())
 Style().setColor(ecolor)
 data_collect(da(),'EcalDigits','(e>'+ecalenergy+')')
 data_visualize(da())

    
def createPage(title,nregions): 
 page = ui().currentPage()
 page.deleteRegions()
 Page().setTitle(title)
 Page().titleVisible(True)
 if nregions == 6 : 
    Page().createDisplayRegions(3,2,0)
 else : 
    Page().createDisplayRegions(1,1,0)
 ui().currentWidget().cast_ISoViewer().setHeadlight(1)
                                     
def execute(what):
 curtitle = ui().currentPage().title.getValues()[0]
 action      = ui().parameterValue('Panoramix_OT_input_action.value')
 projection  = ui().parameterValue('Panoramix_OT_input_projection.value')
 if what == 'xy' :
# xy projection of 3 OT stations
  title = 'AfterMagnet OT  XY'
  if curtitle != title or action == 'recreate':  
   createPage(title,6)
   onlineViews[title] = 'Ot_XYView'
  if action ==  'visualize'  : 
   ui().echo(' Visualize OT hits')
 # clear regions before showing new event
   for i in range(Page().fPage.getNumberOfRegions()) : 
    Page().setCurrentRegion(i)      
    Page().currentRegion().clear("dynamicScene")
   # Camera positioning XY:
    Camera().setPosition(0., 0., 20000.)
    Camera().setHeight(5500.)
    Camera().setOrientation(0.,0.,1.,0.)
    Camera().setNearFar(1000.,20000.)
    Style().dontUseVisSvc()
    if i < 3 : 
     Style().setColor('red')
     data_collect(da(),'OTTime','station=='+str(i+1)+'&&layer==0')
     data_visualize(da())
     Style().setColor('magenta')
     data_collect(da(),'OTTime','station=='+str(i+1)+'&&layer==3')
     data_visualize(da())
    else : 
     Style().setColor('blue')
     data_collect(da(),'OTTime','station=='+str(i-2)+'&&layer==1')
     data_visualize(da())
     Style().setColor('cyan')
     data_collect(da(),'OTTime','station=='+str(i-2)+'&&layer==2')
     data_visualize(da())    
    Style().useVisSvc()
    if ui().parameterValue('Panoramix_PRViewer_input_addinfo.value') == 'True' : overlay_runeventnr()
    ui().synchronize()
 if what == '3d' or what == 'top' :
# all in one display
  if what == '3d' : 
   title = 'AfterMagnet OT  3d'
   if curtitle != title or action == 'recreate':  
    createPage(title,1)
    onlineViews[title] = 'OT_Viewer.execute("'+what+'")'
   # Camera positioning XY:
    Camera().setPosition(0., 0., 20000.)
    Camera().setHeight(5500.)
    Camera().setOrientation(0.,0.,1.,0.)
    Camera().setNearFar(1000.,20000.)
  if what == 'top' :
# setup top projections for OT
   title = 'Top View     OT    z scaled'
   if curtitle != title or action == 'recreate':  
    createPage(title,1)
    onlineViews[title] = 'OT_Viewer.execute("'+what+'")'
# Camera positioning top:
    Camera().setPosition(0., 5000., 50000.)
    Camera().setHeight(7000.)
    Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
    Camera().setNearFar(1.,10000.)
    Region().setTransformScale(1.,1.,6.)
#
  if action ==  'visualize' and evt['DAQ/ODIN'] : 
   ui().echo(' Visualize OT hits')
 # clear regions before showing new event
   Page().setCurrentRegion(0)      
   Page().currentRegion().clear("dynamicScene")
   Style().dontUseVisSvc()
   for i in range(6) : 
    if i < 3:     
     Style().setColor('red')
     data_collect(da(),'OTTime','station=='+str(i+1)+'&&layer==0')
     data_visualize(da())
     Style().setColor('magenta')
     data_collect(da(),'OTTime','station=='+str(i+1)+'&&layer==3')
     data_visualize(da())
    else : 
     Style().setColor('blue')
     data_collect(da(),'OTTime','station=='+str(i-2)+'&&layer==1')
     data_visualize(da())
     Style().setColor('cyan')
     data_collect(da(),'OTTime','station=='+str(i-2)+'&&layer==2')
     data_visualize(da())    
    Style().useVisSvc()
# plot also tracks if any exists which are supposed to reacj IT:
    Style().setColor('blue')
    onlyOT = 'veloTTITOT==1 ||veloTTITOT==101||veloTTITOT==1001||veloTTITOT==1011'
    withIT = 'veloTTITOT==11||veloTTITOT==111||veloTTITOT==1111||veloTTITOT==1011'
    data_collect(da(),'Track',onlyOT+'||'+withIT)
    data_visualize(da())
    ui().synchronize()
    overlay_runeventnr()

# Dialog inputs : 
def xx() : 
 action      = ui().parameterValue('Panoramix_OT_input_action.value')
 projection  = ui().parameterValue('Panoramix_OT_input_projection.value')
 if action == 'NextEvent' :
  appMgr.run(1)
  action = 'visualize' 
 execute(projection)

