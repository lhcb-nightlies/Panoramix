from panoramixmodule import *

from PanoramixSys.Configuration import *
ld = {}

def execute() : 
 nxtevt = False
 rject = 0
 session().setParameter('looping','run') 
 while nxtevt == False:
  uiSvc().nextEvent()
  bk =  evt['DAQ/ODIN']
  if bk:  
   if physicstrigger()  : 
    nxtevt = True
   else :
    rject+=1
   if rject%100 == 0: print "Rejected Lumi Events",rject
  else:
# maybe mdf 
   bk = evt['Rec/Header']
   if not bk : return
# support for mDST, automatic unpacking of stripped packed containers
  if evt['Stripping'] or evt['Turbo']: unpackStripping()
  ui().synchronize()
  if session().parameterValue('looping') == 'stop' : 
    print 'Looking for next event stopped'
    return


 if PanoramixSys().getProp('Sim') != True : print bk
 curtitle = ui().currentPage().title.getValues()[0]

 EraseEventAllRegions()
 for t in onlineViews :
  if curtitle == t :
   cmd = onlineViews[t]
   if cmd.find('(') > 0 : 
    m = cmd[:cmd.find('.')]
    ld[m] = sys.modules[m]
    eval(cmd,ld)   
   else : 
    x(cmd)
   break 
 session().flush()   

def unpackStripping():
 if appMgr.algorithm('UnpackParticlesAndVertices').InputStream=='/Initial/':
     decReports = evt['Strip/Phys/DecReports']
     if decReports : 
      lines  = []
      for d in decReports.decisionNames():
       if d.find('Stream')<0:
        tmp  = d.replace('Decision','')
        lines.append(tmp.replace('Stripping',''))
      allNodes = nodes(evt,True)
      mDSTContainer = []
      for x in allNodes:
       if not x.find('pPhys/Particles')<0:  mDSTContainer.append(x.split('/pPhys')[0])
      for s in mDSTContainer:
       for line in lines:
        print  s+'/Phys/'+line+'/Particles'
        tmp = evt[s+'/Phys/'+line+'/Particles']
       appMgr.algorithm('UnpackParticlesAndVertices').InputStream=s
       appMgr.algorithm('UnpackParticlesAndVertices').RootOnTES='MicroDST'
      appMgr.algorithm('UnpackParticlesAndVertices').execute()
# turbo lines
 decReports = evt['Hlt2/DecReports']
 if not decReports : return
 lines  = []
 for d in decReports.decisionNames():
     if not d.find('Turbo')<0 and decReports.decReport(d).decision()>0:
       tmp  = d.replace('Decision','')
       lines.append(tmp)
 for line in lines:
     # print line
     tmp = evt['Turbo/'+line+'/Particles']
     tmp = evt['Turbo/'+line+'/Vertices']
