from Onlinemodule import *
import Calo_Viewer
import panoramixmodule
    
def createPage(title,nregions): 
 page = ui().currentPage()
 page.deleteRegions()
 Page().setTitle(title)
 Page().titleVisible(True)
 Page().createDisplayRegions(2,1,0)
 ui().currentWidget().cast_ISoViewer().setHeadlight(1)
 Style().setWireFrame()
 Page().setCurrentRegion(0)
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
 Page().setCurrentRegion(1)
 uiSvc().visualize('/dd/Structure/LHCb/DownstreamRegion/Ecal')
 Style().setSolid()  
                                     
def execute(action):
 ecolor        = session().parameterValue('Panoramix_Calo_input_ecolor')
 if ecolor == '' : Calo_Viewer.defaults()
 ecolor        = session().parameterValue('Panoramix_Calo_input_ecolor')
 hcolor        = session().parameterValue('Panoramix_Calo_input_hcolor')
 scolor        = session().parameterValue('Panoramix_Calo_input_scolor')
 pcolor        = session().parameterValue('Panoramix_Calo_input_pcolor')
 ecalenergy    = session().parameterValue('Panoramix_Calo_input_ecalenergy')
 hcalenergy    = session().parameterValue('Panoramix_Calo_input_hcalenergy')
 spdcalenergy  = session().parameterValue('Panoramix_Calo_input_spdcalenergy')
 prscalenergy  = session().parameterValue('Panoramix_Calo_input_prscalenergy')
 scale         = session().parameterValue('Panoramix_Calo_input_scale')
 eoret         = session().parameterValue('Panoramix_Calo_input_eoret')
 
 curtitle = ui().currentPage().title.getValues()[0]
# setup top/side projections for downstream detectors
 title = 'Top         Downstream                 Side'
 if curtitle != title or action == 'recreate':  
   createPage(title,2)
   onlineViews[title] = 'Downstream'
 if action ==  'visualize'  : 
  ui().echo(' Visualize Downstream detectors')
 # clear regions before showing new event
  for i in range(Page().fPage.getNumberOfRegions()) : 
   Page().setCurrentRegion(i)      
   Page().currentRegion().clear("dynamicScene")
   if i == 0: 
# Camera positioning top:
    Camera().setPosition(0., 2000., 17000.)
    Camera().setHeight(11000.)
    Camera().setOrientation(-0.57735, -0.57735, -0.57735, 2.0943)
    Camera().setNearFar(1.,100000.)
   if i == 1: 
# Camera positioning side:
    Camera().setPosition(-10000., 0., 17000.)
    Camera().setHeight(11000.)
    Camera().setOrientation(0.,-1.,0.,1.57)
    Camera().setNearFar(1.,100000.)
 
# muon hits
   Style().dontUseVisSvc()
   for k in range(5): 
    muonStationContours(k)
    g = 1.-0.5*float(k)/6.
    b = 0.5*k/6. 
    Style().setRGB(0.,g,b)
    data_collect(da(),'MuonCoord','station=='+str(k))
    data_visualize(da())
   Style().useVisSvc()

# calo hits
   Style().dontUseVisSvc()
   if hcalenergy != 'n' :
    Style().setColor(hcolor)
    data_collect(da(),'HcalDigits','(e>'+hcalenergy+')')
    data_visualize(da())
   if ecalenergy != 'n' :
    Style().setColor(ecolor)
    data_collect(da(),'EcalDigits','(e>'+ecalenergy+')')
    data_visualize(da())
   if prscalenergy != 'n' :
    Style().setColor(pcolor)
    data_collect(da(),'PrsDigits','(e>'+prscalenergy+')')
    data_visualize(da())
   if spdcalenergy != 'n' :
    Style().setColor(scolor)
    data_collect(da(),'SpdDigits','(e>'+spdcalenergy+')')
    data_visualize(da())
   Style().useVisSvc()
   panoramixmodule.overlay_runeventnr()
   ui().synchronize()

# Dialog inputs : 
def whattodo() : 
 action    = ui().parameterValue('Panoramix_Global_input_action.value')
 if action == 'NextEvent' :
  appMgr.run(1)
  action = 'visualize' 
 if (action == 'visualize' and evt['DAQ/ODIN'] ) or action == 'recreate': 
  execute(action)

