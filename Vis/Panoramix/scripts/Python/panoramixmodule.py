from Panoramix import *
import GaudiKernel.SystemOfUnits as units

from Configurables import LHCbApp
from PanoramixSys.Configuration import *
import CoinPython as iv
from gaudigadgets import *
from ROOT import Double
import os,time

appMgr = GaudiPython.AppMgr()
# automatic loading fails
GaudiPython.loaddict('SoEventDict')

#access to event tag collection
def _evtcolsvc_ ( self ) :
   return self.ntuplesvc ( 'EvtTupleSvc' )
GaudiPython.AppMgr.evtcolsvc = _evtcolsvc_
# first check if factory is loaded
for s in appMgr.services() : 
  if s == 'EvtTupleSvc' : 
   etc = GaudiPython.AppMgr().evtcolsvc()
   break

sel     = appMgr.evtsel()
evt     = appMgr.evtsvc()
hist    = appMgr.histsvc()
det     = appMgr.detsvc()
partsvc = appMgr.ppSvc()
fsrsvc  = appMgr.filerecordsvc()
tsvc    = appMgr.toolsvc()

#useful tools, some not used, take only time for initialization

#TrackHerabExtrapolator    = tsvc.create('TrackHerabExtrapolator',  interface='ITrackExtrapolator')
#TrackLinearExtrapolator   = tsvc.create('TrackLinearExtrapolator', interface='ITrackExtrapolator')
#MCDecayFinder             = tsvc.create('MCDecayFinder',           interface='IMCDecayFinder')
# needs to be retrieved very early, otherwise seq fault
# but only if recosequence in place:
if  'PanoRecoSequencer' in appMgr.algorithms():
  fitterTool = getTool('TrackMasterFitter','ITrackFitter')
else:
  fitterTool = None

# implementation in Phys project
#  MCDebugTool               = tsvc.create('PrintMCDecayTreeTool',   interface='IPrintMCDecayTreeTool')
#  DecayTreeTool             = tsvc.create('PrintDecayTreeTool',     interface='IPrintDecayTreeTool')

rootSvc  = appMgr.service('RootSvc','IRootSvc')
 #example to use:
 #h1 = his['/stat/CaloPIDs/Calo/EcalPIDe/1']
 #rootSvc.visualize(h1)

# for eolas
eolas_container = []

# get rawbank key for HltRoutingBits
bt = getEnumNames('LHCb::RawBank')
bb = bt['BankType']
for rbkey in bb:
 if bb[rbkey] == 'HltRoutingBits' : break

# dictionary of online views
onlineViews = {}

from LinkerInstances.eventassoc import *  


# get nicer circles
# session().setParameter('modeling.rotationSteps','48')

#  Pick dump raw mode, gives more information than default table mode
#  session().setParameter('AccessorManager.dumpMode', 'raw')

onlinetopics = {
 'Panoramix_OT_dialog'      :  '$PANORAMIXROOT/scripts/OnX/Ot.onx'            
, 'Panoramix_Calo_dialog'    :  '$PANORAMIXROOT/scripts/OnX/Calo.onx'          
, 'Panoramix_Muon_dialog'    :  '$PANORAMIXROOT/scripts/OnX/Muon.onx'          
, 'Panoramix_Global_dialog'  :  '$PANORAMIXROOT/scripts/OnX/Online_global.onx' 
, 'Panoramix_Rich_dialog'    :  '$PANORAMIXROOT/scripts/OnX/RICH_dialog.onx'   
}

if session().parameterValue('modeling.lineWidth') == '' : 
  session().setParameter('modeling.lineWidth','1')
# for VeloPixel
session().setParameter('modeling.markerWidth','1')
#
#
# definition of functions and methods
#
def Object_visualize(aContainedObject):
  cls =  GaudiPython.getClass('LHCb::'+aContainedObject.__class__.__name__)
  if cls == None:
    cls = GaudiPython.getClass(aContainedObject.__class__.__name__)
    if cls == None:
     print 'Class %s not in Gaudi namespace' % (aContainedObject.__class__.__name__)
     return    
  GaudiPython.gbl.SoEvent.KO(cls).visualize(soCnvSvc(),aContainedObject)
  ui().synchronize()
#-----------------------------------------------------------
# fix to broken sel.rewind() :
def panorewind():
 alg_list = appMgr.algorithms()
 alg_Enable = {}
 for a in alg_list:
  alg_Enable[a] = appMgr.algorithm(a).Enable 
  appMgr.algorithm(a).Enable = False 
 sel.rewind()
 for a in alg_list:
  appMgr.algorithm(a).Enable = alg_Enable[a]
#-----------------------------------------------------------
def evtRunNumber():
 res = None
 if evt['Rec/Header']:
    res = evt['Rec/Header'].evtNumber(), evt['Rec/Header'].runNumber()
 elif evt['DAQ/ODIN']:
    res = evt['DAQ/ODIN'].eventNumber(), evt['DAQ/ODIN'].runNumber()
 return res
#
def panojump(evtnr):
 alg_list = appMgr.algorithms()
 alg_Enable = {}
 for a in alg_list:
# remember setting of algorithms
  alg_Enable[a] = appMgr.algorithm(a).Enable 
  appMgr.algorithm(a).Enable = False 
 if not evtRunNumber() : appMgr.run(1)
 if not evtRunNumber() : 
   print 'no events available' 
   return
 curnr,runnr = evtRunNumber()
 while 1>0: 
  appMgr.run(1)
  if not evtRunNumber() : 
   print 'event not found' 
   return
  enr,rnr = evtRunNumber() 
  if enr == evtnr : break
# curnr always number of event before
  curnr = enr
 if PanoramixSys().getProp('Full_rec') :
  sel.rewind()
  while 1>0: 
   appMgr.run(1)
   enr,rnr = evtRunNumber() 
   if enr == curnr: break 
 for a in alg_list:
  appMgr.algorithm(a).Enable = alg_Enable[a]
 if PanoramixSys().getProp('Full_rec'): appMgr.run(1)
#-----------------------------------------------------------
import truf_decodePUS

def tae_exist(d):
# do not run data on demand for tae slots if they don't exist
 sc = True
 test = ''
 nx = d.find('Next')
 if nx > -1:   
  test = d[nx:nx+5]
 pr = d.find('Prev')
 if pr > -1:   
  test =  d[pr:pr+5]
 if test != '' : 
  sc = False
  if evt[test+'/DAQ'] : sc = True   
 return sc  
 
def Decode_RawBuffer():
#inititate data on demand for all raw data
# there are some issues with 
# calo and muon decoding and incident svc
# for the first time, needs to run twice 

 dodnodes = appMgr.service('DataOnDemandSvc').properties()['NodeMap'].value()
 dodalgs  = appMgr.service('DataOnDemandSvc').properties()['AlgMap'].value()
 dodtools = appMgr.service('DataOnDemandSvc').properties()['NodeMappingTools'].value()
 for d in dodnodes.keys() : 
  if not tae_exist(d)    : continue
  if not d.find('Link')<0: continue
  if not d.find('Trig/L0/')<0: 
   if not evt['DAQ/ODIN']: continue
   if evt['DAQ/ODIN'].triggerConfigurationKey() == 0: continue
   sc = evt[d]      
  if not d.find('Raw/')<0 : 
    if not d.find('OT')<0 and not det['/dd/Structure/LHCb/AfterMagnetRegion/T/OT']: continue
    if not d.find('IT')<0 and not det['/dd/Structure/LHCb/AfterMagnetRegion/T/IT']: continue
    if not d.find('FT')<0 and not det['/dd/Structure/LHCb/AfterMagnetRegion/T/FT']: continue
    if not d.find('UT')<0 and not det['/dd/Structure/LHCb/BeforeMagnetRegion/UT']: continue
    if not d.find('TT')<0 and not det['/dd/Structure/LHCb/BeforeMagnetRegion/TT']: continue
    if not d.find('VP')<0 and not det['/dd/Structure/LHCb/BeforeMagnetRegion/VP']: continue
    if not d.find('VL')<0 and not det['/dd/Structure/LHCb/BeforeMagnetRegion/VL']: continue
    if not d.find('Velo')<0 and not det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']: continue
    sc = evt[d] 
 for d in dodalgs.keys() : 
  if not tae_exist(d) : continue
  if d.find('Link')>-1: continue
  if not d.find('Trig/L0/')<0: 
   if not evt['DAQ/ODIN']: continue
   if evt['DAQ/ODIN'].triggerConfigurationKey() == 0: continue
   sc = evt[d]      
  if not d.find('Raw/')<0 : 
    if not d.find('OT')<0 and not det['/dd/Structure/LHCb/AfterMagnetRegion/T/OT']: continue
    if not d.find('IT')<0 and not det['/dd/Structure/LHCb/AfterMagnetRegion/T/IT']: continue
    if not d.find('FT')<0 and not det['/dd/Structure/LHCb/AfterMagnetRegion/T/FT']: continue
    if not d.find('UT')<0 and not det['/dd/Structure/LHCb/BeforeMagnetRegion/UT']: continue
    if not d.find('TT')<0 and not det['/dd/Structure/LHCb/BeforeMagnetRegion/TT']: continue
    if not d.find('VP')<0 and not det['/dd/Structure/LHCb/BeforeMagnetRegion/VP']: continue
    if not d.find('VL')<0 and not det['/dd/Structure/LHCb/BeforeMagnetRegion/VL']: continue
    if not d.find('Velo')<0 and not det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']: continue
    sc = evt[d] 
#private decoding of PUS data in Python
 if det['/dd/Structure/LHCb/BeforeMagnetRegion/Velo']: truf_decodePUS.create_L0PusClusters() 
# for FTClusters
 if det['/dd/Structure/LHCb/AfterMagnetRegion/T/FT'] and evt['Raw/FT/RawClusters']:
  import FTSupport
  FTSupport.makeFTClusterContainer() 

#-----------------------------------------------------------
def commonParticles():
 appMgr.algorithm("PhysSeq").execute()
 dodnodes = appMgr.service('DataOnDemandSvc').properties()['NodeMap'].value()
 dodalgs  = appMgr.service('DataOnDemandSvc').properties()['AlgMap'].value()
 dt = -2*(LHCbApp().DataType == 'DC06')+1
 for d in dodnodes.keys() : 
  if d.find('Phys/')> -1 and ( d.find('DC06')*dt < 0 ) and d.find('StdLooseDplus2hhh')<0 : touch = evt[d] 
 for d in dodalgs.keys() : 
  if d.find('Phys/')> -1 and ( d.find('DC06')*dt < 0 ) and d.find('StdLooseDplus2hhh')<0 : touch = evt[d] 

#-----------------------------------------------------------
# fix a feature in output writer which makes files unreadable if somewhere
# a converter is missing 
def remove_L0_containers():
 l0 = nodes(evt,False,'Trig/L0')
 if len(l0) < 1 : return
 print 'panodstwriter: Need to remove L0 containers from TES for writing'
 for c in l0 : 
   rc = evt.unregisterObject(c)
 evt.unregisterObject('Trig/L0')  
 evt.unregisterObject('Trig')  
 
#-----------------------------------------------------------
# print event information on page plus LHCb logo
def gpsTimeConv(gpstime):
# odin gpstime in microseconds, gps time is 13sec ahead of UTC
# information from http://www.csgnetwork.com/timegpsdispcalc.html says 15sec ahead
# 20.02.2012: according to Richard, odin gpstime is not gpstime, it is posix, therefore no correction needed. 
# thanks for Marc Bettler of finding out.
# gpss = gpstime/1000000-15
 gpss = gpstime/1000000
 timeTuple = time.localtime(gpss)
# (tm_year, tm_mon, tm_day, tm_hour, tm_min, tm_sec, tm_wday, tm_yday, tm_isdst)
 sec   = "%02d" % (timeTuple[5])
 minut = "%02d" % (timeTuple[4])
 txt = str(timeTuple[2])+'.'+str(timeTuple[1])+'. '+str(timeTuple[0])+'  '+str(timeTuple[3])+':'+minut+':'+sec
 return txt

def overlay_runeventnr(t1=0.0,t2=0.0,t3=0.15,t4=0.07,i1=0.90,i2=0.00,i3=0.0,i4=0.0,logoonly=False):
  odin = evt['DAQ/ODIN']
  if not odin :
    print 'overlay_runeventnr(): No ODIN bank found'
    return  
  soPage = ui().currentPage()
  if not soPage :
    print 'overlay_runeventnr(): No Page found'
    return    
# remove all image / text regions
  found = True
  while found:
   found = False
   nall = soPage.getNumberOfRegions()
   for n in range(nall):
    reg = soPage.getRegion(n)
    if not reg: continue
    typ = reg.getTypeId()
    if typ.getName() == 'SoTextRegion' or typ.getName() == 'SoImageRegion' : 
      soPage.deleteRegion(reg)
      found = True
#
  evtnr   = odin.eventNumber() 
  runnr   = odin.runNumber() 
  bid     = odin.bunchId() 
  gpstime = odin.gpsTime()
  stime = gpsTimeConv(gpstime)
  name = str(stime)+' evtnr:'+str(evtnr)
# check for rulers
  withRuler = False
  nall = soPage.getNumberOfRegions()
  for n in range(nall):
   reg = soPage.getRegion(n)
   typ = reg.getTypeId()
   if typ.getName() == 'SoRulerRegion' :
     withRuler = True
     break
  found = False  
  for n in range(nall):
   reg = soPage.getRegion(n)
   typ = reg.getTypeId()
   thisText = 'Run '+str(runnr)+' Event '+str(evtnr)+'   bId '+str(bid)
   # print n,typ.getName()
   if typ.getName() == 'SoTextRegion' :
    soTextRegion = reg.cast_SoTextRegion()
    soTextRegion.text.set1Value(0,stime)
    soTextRegion.text.set1Value(1,thisText)
    soTextRegion.setPosition(t1,t2)
    soTextRegion.setSize(t3,t4)
    soPage.setRegionOnTop(reg)
    found = True
   if typ.getName() == 'SoImageRegion' :
     soPage.setRegionOnTop(reg)
     if logoonly: found = True
  if not found :
   # print 'Panoramix module: no info found for page layout',soPage.getNumberOfRegions(),n
   #if n>1: 1/0  
   if not logoonly:
    soRegion = soPage.createRegion('SoTextRegion',t1,t2,t3,t4)
    soTextRegion = soRegion.cast_SoTextRegion()
    soTextRegion.verticalMargin.setValue(0)
    soTextRegion.text.set1Value(0,stime)
    soTextRegion.text.set1Value(1,thisText)
#  LHCb logo :
   soRegion = soPage.createRegion('SoImageRegion',i1,i2,i3,i4)
   soImageRegion = soRegion.cast_SoImageRegion()
   location = os.path.expandvars('$PANORAMIXROOT/scripts/images/logo_LHCb.gif')
   soImageRegion.getImage().fileName.setValue(location)
  ui().synchronize()
  for n in range(soPage.getNumberOfRegions()):
   reg = soPage.getRegion(n)
   typ = reg.getTypeId()
   if typ.getName() == 'SoDisplayRegion' :
     rc = soPage.setCurrentRegion(n)
     break
#
def checkRulersPresent():
  soPage = ui().currentPage()
  if not soPage :    return    
  nall = soPage.getNumberOfRegions()
  for n in range(nall):
   reg = soPage.getRegion(n)
   typ = reg.getTypeId()
   if typ.getName() == 'SoRulerRegion' :
     return True
  return False


def reco_sequence():
   unpack = False  
   dodnodes = appMgr.service('DataOnDemandSvc').properties()['NodeMap'].value()  
   if dodnodes.has_key('/Event/Rec/ProtoP') :
     # use algs once, otherwise problems with finalize
     evt['Rec/ProtoP/Charged']
     evt['Rec/ProtoP/Neutrals']
     appMgr.algorithm('UnpackNeutrals').enable = False
     appMgr.algorithm('UnpackCharged').enable = False
     unpack = True 
   
   try: 
    rc = appMgr.algorithms().index('CleanUpSequence')
    appMgr.algorithm("CleanUpSequence").execute()
   except:
    rc = -1 
   appMgr.algorithm('PanoRecoSequencer').execute() 
   if unpack:
    appMgr.algorithm('UnpackNeutrals').enable = True
    appMgr.algorithm('UnpackCharged').enable =  True

def physicstrigger():
 if evt['DAQ/ODIN']:
  if evt['DAQ/ODIN'].triggerType()==0: return True
 rawevent = evt['DAQ/RawEvent']
 if rawevent:
  hltrb    = rawevent.banks(rbkey)
  if hltrb.size() > 0: 
   first    = hltrb[0].data()
   # print bin(first[0]),bin(first[1]),bin(first[2])
   lumionly = first[1]&0x4
  if lumionly: return False
  else: return True 
 if not rawevent : 
    rawevent = evt['pRec/RawEvent'] # probably rDST
    if rawevent :
       return True # probably micro dst
 return False

def checkRoutingBits():
  m_routingBits = GaudiPython.gbl.std.vector('unsigned int')()
  m_routingBits.push_back(46)
  m_routingBits.push_back(77)
  rawevent = evt['DAQ/RawEvent']
  hltrb    = rawevent.banks(rbkey)

def openFile():
 w = ui().findWidget("fileChooser")
 theFile = ui().callbackValue()
 print 'open file for reading',theFile
 sel.open(theFile)
 appMgr.run(1)

