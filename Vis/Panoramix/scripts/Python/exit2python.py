import OnX,sys,os
if OnX.session().printer().enabled(): 
  print 'cannot exit to Python, output to OnX is enabled'
  print 'click File / output -> terminal'
else :  
 try:
  if os.uname()[3].find('Ubuntu')>-1:
   sys.excepthook = sys.__excepthook__
 except:
   rc = 'OK' 
 OnX.session().ui().exit() 
