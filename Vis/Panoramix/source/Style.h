#ifndef Panoramix_Style_h
#define Panoramix_Style_h

#include <Lib/Interfaces/ISession.h>

// C++ Pending of the Panoramix.py Python Style class.
// To help converting a .py to C++ if needed.

namespace Panoramix {

class Style {
public:
  Style(ISession& aSession);
public:
  void useVisSvc();
  void dontUseVisSvc();
  // Color :
  void setColor(const std::string& aColor);
  void setRGB(double aR,double aG,double aB);
  void setTransparency(double aValue);
  // Line :
  void setLineWidth(int aValue);
  // Marker :
  void setMarkerSize(int aValue);
  void setMarkerStyle(const std::string& aValue);
  // Text :
  void setTextSize(int aValue);
  void showText();
  void hideText();
  // Geometry :
  void setWireFrame();
  void setSolid();
  void setOpened();
  void setClosed();
private:
  ISession& fSession;
};

}

#endif
