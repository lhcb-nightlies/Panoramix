//
//  All functions here should be OnX callbacks, that is to say
// functions with signature :
//   extern "C" {
//     void callback_without_arguments(IUI&);
//     void callback_with_arguments(IUI&,const std::vector<std::string>&);
//   }
//

// To demo SDL.

// Panoramix :
#include "Helpers.h"

// OnX :
#include <OnX/Helpers/Inventor.h>

// Inventor :
#include <Inventor/nodes/SoPerspectiveCamera.h>

// Inventor :
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoPointLight.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoRotationXYZ.h>

// HEPVis :
#include <HEPVis/SbMath.h>
#include <HEPVis/nodekits/SoRegion.h>
#include <HEPVis/nodekits/SoDisplayRegion.h>
#include <HEPVis/nodes/SoGrid.h>
#include <HEPVis/nodekits/SoDetectorExample.h>

extern "C" {

//////////////////////////////////////////////////////////////////////////////
void Panoramix_SDL_default_scene(
 Slash::UI::IUI& aUI
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  //printf("debug : Panoramix_SDL_default_scene\n");

  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  SoRegion* soRegion = soPage->currentRegion();
  if(!soRegion) return;
  if(!soRegion->isOfType(SoDisplayRegion::getClassTypeId())) return;
  SoDisplayRegion* displayRegion = (SoDisplayRegion*)soRegion;

  displayRegion->setCameraType(SoPerspectiveCamera::getClassTypeId());
  SoPerspectiveCamera* soCamera = 
    (SoPerspectiveCamera*)displayRegion->getCamera();
  soCamera->viewportMapping.setValue(SoCamera::ADJUST_CAMERA);
  float d = 20000;
  // front :
  //soCamera->position.setValue(0,0,d);
  //soCamera->orientation.setValue(SbRotation(SbVec3f(0,1,0),0));
  // back :
  soCamera->position.setValue(0,0,-d);
  soCamera->orientation.setValue(SbRotation(SbVec3f(0,1,0),FM_PI));

  soCamera->focalDistance.setValue(d);
  soCamera->nearDistance.setValue(d*0.01F);
  soCamera->farDistance.setValue(100*d);
  soCamera->heightAngle.setValue(FM_PI/4.0F);

 {Slash::UI::IWidget* widget = aUI.currentWidget();
  if(!widget) return;
  float dr = 2.0F * 3.1416F/180.0F;
  widget->setParameter("deltaRotation",Lib::smanip::tostring(dr));
  widget->setParameter("deltaTranslation","500");} //default is 1.

  SoSeparator* sep = (SoSeparator*)displayRegion->getStaticScene();
  if(!sep) return;
  sep->removeAllChildren();

  soRegion->color.setValue(0.9F,0.9F,0.9F);

  // Light at ceil :
  SoPointLight* light = new SoPointLight;
  light->location.setValue(0,100,0); //y = 100.
  sep->addChild(light);

  ///////////////////////////////////////////////
  // Floor :
  ///////////////////////////////////////////////
  SoMaterial* material = new SoMaterial;
  material->diffuseColor.setValue(0.8F,0.8F,0.8F);
  sep->addChild(material);

  SoGrid* floor = new SoGrid;
  float halfSize = 20000;
  float yground = -5000;
  floor->position.setValue(SbVec3f(-halfSize,yground,halfSize));
  floor->X.setValue(SbVec3f(1,0,0));
  floor->Y.setValue(SbVec3f(0,0,-1));
  floor->width.setValue(2.0F*halfSize);
  floor->height.setValue(2.0F*halfSize);
  sep->addChild(floor);

  SoRotationXYZ* soRotation = new SoRotationXYZ; //For animation.
  soRotation->axis.setValue(SoRotationXYZ::Y);
  sep->addChild(soRotation);

  ///////////////////////////////////////////////
  // Detector :
  ///////////////////////////////////////////////
  IUserInterfaceSvc* uiSvc = find_uiSvc(aUI.session());
  if (!uiSvc) return;

  //ISession& session = aUI.session();
  //session_setColor(session,"blue");
 
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Velo");
  //uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Velo2Rich1");
  uiSvc->visualize("/dd/Structure/LHCb/BeforeMagnetRegion/Rich1");
  uiSvc->visualize("/dd/Structure/LHCb/MagnetRegion/Magnet");
  //uiSvc->visualize("/dd/Structure/LHCb/AfterMagnetRegion/T/IT");
  //uiSvc->visualize("/dd/Structure/LHCb/AfterMagnetRegion/T/OT");
  uiSvc->visualize("/dd/Structure/LHCb/AfterMagnetRegion/Rich2");
  //uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Prs");
  //uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Spd");
  uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Ecal");
  //uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Hcal");
  //uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Muon");

  //aUI.executeScript("DLD","Panoramix visualize_LHCb");

}

} // extern "C"
