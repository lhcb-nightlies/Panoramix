
#include <Lib/Interfaces/ISession.h>
#include <Lib/Out.h>

// Gaudi :
#include <GaudiKernel/Property.h>
#include <GaudiKernel/Bootstrap.h>
#include <GaudiKernel/SmartIF.h>
#include <GaudiKernel/IAppMgrUI.h>
#include <GaudiKernel/IProperty.h>
#include <GaudiKernel/ISvcLocator.h>

// OnXSvc :
#include <OnXSvc/IUserInterfaceSvc.h>
#include "GaudiManager.h"

//////////////////////////////////////////////////////////////////////////////
GaudiManager::GaudiManager(
 ISession& aSession
,const std::string& aTxt
)
:fName("GaudiManager")
,fSession(aSession)
,fAppMgrUI(0)
//////////////////////////////////////////////////////////////////////////////
// Used in case of starting Panoramix with the OnX_onx.exe program.
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  // Create an instance of the Gaudi Application Manager
  fAppMgrUI = Gaudi::createApplicationMgr();

  // Set the necessary properties to the ApplicationManager
  SmartIF<IAppMgrUI> appMgr(fAppMgrUI);
  if(!appMgr.isValid()) {
    Lib::Out out(fSession.printer());
    out << "GaudiManager::GaudiManager : "
	<< " Fatal error while creating the ApplicationManager." << Lib::endl;
    return;
  }

  SmartIF<IProperty> propMgr(fAppMgrUI);
  if(!propMgr.isValid()) {
    Lib::Out out(fSession.printer());
    out << "GaudiManager::GaudiManager : "
	<< " the ApplicationManager is not a property manager !" << Lib::endl;
    return;
  }

  StatusCode status = propMgr->setProperty("JobOptionsPath",aTxt);
  if( status.isFailure() ) {
    Lib::Out out(fSession.printer());
    out << "GaudiManager::GaudiManager :"
	<< " can't set property JobOptionsPath." << Lib::endl;
    return;
  }

  status = propMgr->setProperty("JobOptionsType","FILE");
  if( status.isFailure() ) {
    Lib::Out out(fSession.printer());
    out << "GaudiManager::GaudiManager :"
	<< " can't set property JobOptionsType." << Lib::endl;
    return;
  }

  status = fAppMgrUI->configure();
  if( status.isFailure() ) {
    Lib::Out out(fSession.printer());
    out << "GaudiManager::GaudiManager :"
	<< " can't configure the ApplicationManager." << Lib::endl;
    return;
  }

  status = fAppMgrUI->initialize();
  if( status.isFailure() ) {
    Lib::Out out(fSession.printer());
    out << "GaudiManager::GaudiManager :"
	<< " can't initialize the ApplicationManager." << Lib::endl;
    return;
  }

  // Get OnXSvc started with property Toolkit set to "NONE".
  SmartIF<ISvcLocator> svcLoc(fAppMgrUI);

  IUserInterfaceSvc* uiSvc = 0;
  svcLoc->service("OnXSvc",uiSvc);
  if(!uiSvc) {
    Lib::Out out(fSession.printer());
    out << "OnXSvc not found." << Lib::endl;
    return;
  }

  uiSvc->setSession(&fSession);
  uiSvc->release();
}
//////////////////////////////////////////////////////////////////////////////
GaudiManager::~GaudiManager(
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  if(fAppMgrUI) fAppMgrUI->finalize();
  //?? delete fAppMgrUI;
}
//////////////////////////////////////////////////////////////////////////////
std::string GaudiManager::name() const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return fName;
}
//////////////////////////////////////////////////////////////////////////////
void* GaudiManager::cast(
 const std::string&
) const
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return 0;
}
