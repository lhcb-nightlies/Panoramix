// this :
#include "Style.h"

#include <Lib/Interfaces/ISession.h>
#include <Lib/smanip.h>

Panoramix::Style::Style(
 ISession& aSession
)
:fSession(aSession)
{
}
void Panoramix::Style::useVisSvc() { // Use the XML styles.
  fSession.setParameter("modeling.useVisSvc","true");
}
void Panoramix::Style::dontUseVisSvc() { // Do not use the XML styles.
  fSession.setParameter("modeling.useVisSvc","false");
}
// Color :
void Panoramix::Style::setColor(const std::string& aColor) {
  // aColor could be a common name like "red", "blue", etc...
  // or a string with RGBS, for example "1 1 0" for a yellow. 
  fSession.setParameter("modeling.color",aColor);
}
void Panoramix::Style::setRGB(double aR,double aG,double aB) {
  std::string s;
  if(!Lib::smanip::printf(s,128,"%g %g %g",aR,aG,aB)) return;
  fSession.setParameter("modeling.color",s);
}
void Panoramix::Style::setTransparency(double aValue) {
  // 0 is opaque, 1 fully transparent.
  std::string s;
  if(!Lib::smanip::printf(s,32,"%g",aValue)) return;
  fSession.setParameter("modeling.transparency",s);
}
// Line :
void Panoramix::Style::setLineWidth(int aValue) {
  std::string s;
  if(!Lib::smanip::printf(s,32,"%d",aValue)) return;
  fSession.setParameter("modeling.lineWidth",s);
}
// Marker :
void Panoramix::Style::setMarkerSize(int aValue) {
  std::string s;
  if(!Lib::smanip::printf(s,32,"%d",aValue)) return;
  fSession.setParameter("modeling.markerSize",s);
}
void Panoramix::Style::setMarkerStyle(const std::string& aValue) {
  fSession.setParameter("modeling.markerStyle",aValue);
}
// Text :
void Panoramix::Style::setTextSize(int aValue) {
  std::string s;
  if(!Lib::smanip::printf(s,32,"%d",aValue)) return;
  fSession.setParameter("modeling.sizeText",s);
}
void Panoramix::Style::showText() {
  fSession.setParameter("modeling.showText","true");
}
void Panoramix::Style::hideText() {
  fSession.setParameter("modeling.showText","false");
}
// Geometry :
void Panoramix::Style::setWireFrame() {
  fSession.setParameter("modeling.modeling","wireFrame");
}
void Panoramix::Style::setSolid() {
  fSession.setParameter("modeling.modeling","solid");
}
void Panoramix::Style::setOpened() {
  fSession.setParameter("modeling.opened","true");
}
void Panoramix::Style::setClosed() {
  fSession.setParameter("modeling.opened","false");
}
