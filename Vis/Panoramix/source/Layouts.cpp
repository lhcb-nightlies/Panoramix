//
//  All functions here should be OnX callbacks, that is to say
// functions with signature :
//   extern "C" {
//     void callback_without_arguments(IUI&);
//     void callback_with_arguments(IUI&,const std::vector<std::string>&);
//   }
//

// Panoramix :
#include "Helpers.h"

// Lib :
#include <Lib/smanip.h>
#include <Lib/System.h>
#include <Lib/Out.h>

// OnX :
#include <OnX/Interfaces/IUI.h>

#include <OnX/Helpers/Inventor.h>
#include <HEPVis/nodekits/SoRegion.h>
//////////////////////////////////////////////////////////////////////////////
inline SoRegion* page_currentRegion(
 SoPage& aPage
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return aPage.currentRegion();
}
//////////////////////////////////////////////////////////////////////////////
static bool page_setCurrentRegion(
 SoPage& aSoPage
,int aIndex
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  return (aSoPage.setCurrentRegion(aIndex)==TRUE?true:false);
}
//////////////////////////////////////////////////////////////////////////////
static SoRegion* page_createRegion(
 SoPage& aPage
,const std::string& aClass
,double aX
,double aY
,double aWidth
,double aHeight                           
)
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  SoRegion* soRegion = aPage.createRegion
    (aClass.c_str(),(float)aX,(float)aY,(float)aWidth,(float)aHeight);
  if(!soRegion) return 0;
  if(aPage.getNumberOfRootRegions()==1) aPage.highlightRegion(soRegion);
  return soRegion;
}
//////////////////////////////////////////////////////////////////////////////
static void page_connect_current_region(
 SoPage& aPage
,int aIndex
)
//////////////////////////////////////////////////////////////////////////////
// Connect the current region to the "aIndex" region.
// The current region will then display the scene graph of the "aIndex" region.
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  aPage.connectCurrentRegion(aIndex);
}

// Inventor :
#include <Inventor/errors/SoDebugError.h>
#include <Inventor/nodes/SoOrthographicCamera.h>
#include <Inventor/nodes/SoTransform.h>

// Waiting the OSC_16 distrib :
#include <HEPVis/SbMath.h>
#include <HEPVis/nodes/SoViewportRegion.h>
#include <HEPVis/nodekits/SoPage.h>
#include <HEPVis/nodekits/SoDisplayRegion.h>
#include <HEPVis/nodekits/SoRulerRegion.h>
#define SoUtils_SoRulerRegion SoRulerRegion
#define s_SoUtils_SoRulerRegion "SoRulerRegion"

//////////////////////////////////////////////////////////////////////////////
static void page_dressCurrentRegionWithRulers(
 SoPage& aPage
,double aRulerSize = 0.9
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  SoRegion* soRegion = page_currentRegion(aPage);
  if(!soRegion) return;
  if(!soRegion->isOfType(SoDisplayRegion::getClassTypeId())) {
    SoDebugError::postInfo("page_dressCurrentRegionWithRulers",
      "current region is not a SoDisplayRegion.");
    return;
  }

  float w = float(aRulerSize);
  float h = float(aRulerSize);

  // Visualization region :
  SoViewportRegion* vpr = soRegion->getViewportRegion();
  vpr->setPositionPercent(1-w,1-h);
  vpr->setSizePercent(w,h);

  // Horizontal ruler region :
  SoUtils_SoRulerRegion* hRuler = 
    (SoUtils_SoRulerRegion*)page_createRegion(aPage,s_SoUtils_SoRulerRegion,
                                      1-w,0,w,1-h);
  if(!hRuler) return;
  hRuler->getViewportRegion()->backgroundColor.setValue(SbColor(1,1,1));

  // Connect the display region to the ruler :
  hRuler->setMeasuredRegion(soRegion);

  // Vertical ruler region :
  SoUtils_SoRulerRegion* vRuler = 
    (SoUtils_SoRulerRegion*)page_createRegion(aPage,s_SoUtils_SoRulerRegion,
                                      0,1-h,1-w,h);
  if(!vRuler) return;
  vRuler->getViewportRegion()->backgroundColor.setValue(SbColor(1,1,1));

  // Connect the display region to the ruler :
  vRuler->setMeasuredRegion(soRegion);

  // Set display region as current :
  aPage.highlightRegion(soRegion);

}
//////////////////////////////////////////////////////////////////////////////
static void page_removeRulers(
 SoPage& aPage
) 
//////////////////////////////////////////////////////////////////////////////
// Find rulers and remove them.
// Map the first SoDisplayRegion to the page.
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  SoRegion* soRegion = page_currentRegion(aPage);
  if(!soRegion) return;
  // Map first display region to page.
  std::vector<SoRegion*> torm;
 {int number = aPage.getNumberOfRootRegions();
  for(int index=0;index<number;index++) {
    SoRegion* soRegion = aPage.getRootRegion(index);
    if(soRegion->isOfType(SoDisplayRegion::getClassTypeId())) {
      SoViewportRegion* vpr = soRegion->getViewportRegion();
      vpr->setPositionPercent(0,0);
      vpr->setSizePercent(1,1);
    } else if(soRegion->isOfType(SoUtils_SoRulerRegion::getClassTypeId())) {
      torm.push_back(soRegion);
    }
  }}
 {unsigned int number = torm.size();
  for(unsigned int index=0;index<number;index++) {
    aPage.deleteRegion(torm[index]);
  }}
}

// OnX Callbacks :

extern "C" {

//////////////////////////////////////////////////////////////////////////////
void layout_default(
 IUI& aUI
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  ISession& session = aUI.session();
  session.setParameter("modeling.color","cyan");
  session.setParameter("modeling.highlightColor","white");
  //session.setParameter("modeling.modeling","wireFrame")

  // Make the reference frame visible.
  // Note that it is global to the viewer (not per region) :
  Slash::UI::ISoViewer* soViewer = ui_SoViewer(aUI);  
  if(soViewer) {
    soViewer->setFeedbackVisibility(true);
  }

  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  soPage->deleteRegions();
  soPage->titleVisible.setValue(FALSE);
  soPage->title.setValue("Panoramix");
  soPage->createRegions("SoDisplayRegion",1,1,0);

  SoRegion* soRegion = page_currentRegion(*soPage);
  if(!soRegion) return;
  soRegion->color.setValue(SbColor(0,0,0));

  // Default camera position, behind the velo, looking toward muon chambers :
  SoCamera* soCamera = soRegion->getCamera();
  if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
    SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
    camera->height.setValue(30000);
    camera->position.setValue(0,0,-20000);
    camera->orientation.setValue(SbRotation(SbVec3f(0,1,0),FM_PI));
    camera->nearDistance.setValue(1);
    camera->farDistance.setValue(100000);
  }

}
//////////////////////////////////////////////////////////////////////////////
void layout_rec(
 IUI& aUI
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  //////////////////////////////////////////////////////////////////////////
  // Prepare the page :
  //////////////////////////////////////////////////////////////////////////
  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  soPage->deleteRegions();
  soPage->titleVisible.setValue(FALSE);
  soPage->createRegions("SoDisplayRegion",1,2,1);

  // Connect the second region to the first one :
  page_connect_current_region(*soPage,0);
  
  // First region ; top view :
  {
    page_setCurrentRegion(*soPage,0);
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(0,0,0));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(14000);
      camera->position.setValue(0,2000,9000);
      float angle = 2.0F*FM_PI/3.0F; //2.0943
      camera->orientation.setValue(SbRotation(SbVec3f(-1,-1,-1),angle));
      camera->nearDistance.setValue(1);
      camera->farDistance.setValue(100000);
    }
  }
  
  // Second region ; front view :
  {
    page_setCurrentRegion(*soPage,1);
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(0,0,0));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(14000);
      camera->position.setValue(0,0,20000);
      camera->orientation.setValue(SbRotation(SbVec3f(0,1,0),0));
      camera->nearDistance.setValue(1);
      camera->farDistance.setValue(100000);
    }
  }
  
  //////////////////////////////////////////////////////////////////////////
  // Visualize :
  //////////////////////////////////////////////////////////////////////////
  page_setCurrentRegion(*soPage,0);
  
  ISession& session = aUI.session();

  IUserInterfaceSvc* uiSvc = find_uiSvc(session);
  if(!uiSvc) return;

  session.setParameter("modeling.useVisSvc","false");
  session.setParameter("modeling.modeling","wireFrame");

  session_setColor(session,"0.8 0 0");
  uiSvc->visualize("/dd/Structure/LHCb/MagnetRegion/Magnet");
  session_setColor(session,"green");
  uiSvc->visualize("/dd/Structure/LHCb/AfterMagnetRegion/T/IT");
  session_setColor(session,"0.8 0 0");
  uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Ecal");
}

//////////////////////////////////////////////////////////////////////////////
void layout_Rich1OpticalLayout(
 IUI& aUI
 ) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  ////////////////////////////////////////////////////////////////////
  // Prepare the page :
  ////////////////////////////////////////////////////////////////////
  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  soPage->deleteRegions();
  soPage->titleVisible.setValue(FALSE);
  soPage->createRegions("SoDisplayRegion",1,1,0);

  SoRegion* soRegion = page_currentRegion(*soPage);
  if(!soRegion) return;
  soRegion->color.setValue(SbColor(0,0,0));
  SoCamera* soCamera = soRegion->getCamera();
  if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
    SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
    camera->height.setValue(3227.969F);
    camera->position.setValue(-15.612442F,23.426102F,1475.8756F);
    const float angle = 1.8510739F;
    camera->orientation.setValue
      (SbRotation(SbVec3f(-0.010145176F,-0.99955624F,-0.028006708F),angle));
    camera->nearDistance.setValue(-1270.5231F);
    camera->farDistance.setValue(1150.1384F);
  }

  ////////////////////////////////////////////////////////////////////
  // Visualize :
  ////////////////////////////////////////////////////////////////////
  
  ISession& session = aUI.session();
  session.setParameter("modeling.useVisSvc","true");
  session.setParameter("modeling.modeling","solid");
  IUserInterfaceSvc* uiSvc = find_uiSvc(aUI.session());
  if(!uiSvc) return;

  // draw RICH1
  session_setColor(session,"cyan");
  const std::string & srich = "/dd/Structure/LHCb/BeforeMagnetRegion/Rich1";
  uiSvc->visualize( srich );

}

//////////////////////////////////////////////////////////////////////////////
void layout_Rich2OpticalLayout(
 IUI& aUI
 ) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  ////////////////////////////////////////////////////////////////////
  // Prepare the page :
  ////////////////////////////////////////////////////////////////////
  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  soPage->deleteRegions();
  soPage->titleVisible.setValue(FALSE);
  soPage->createRegions("SoDisplayRegion",1,1,0);

  SoRegion* soRegion = page_currentRegion(*soPage);
  if(!soRegion) return;
  soRegion->color.setValue(SbColor(0,0,0));
  SoCamera* soCamera = soRegion->getCamera();
  if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
    SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
    camera->height.setValue(11398.284F);
    camera->position.setValue(-3344.4697F,9908.5205F,5334.5815F);
    const float angle = 2.2956514F;
    camera->orientation.setValue
      (SbRotation(SbVec3f(-0.50448138F,-0.77540749F,-0.37979186F),angle));
    camera->nearDistance.setValue(5762.4453F);
    camera->farDistance.setValue(14284.674F);
    camera->focalDistance.setValue(10019.214F);
  }

  ////////////////////////////////////////////////////////////////////
  // Visualize :
  ////////////////////////////////////////////////////////////////////
  
  ISession& session = aUI.session();
  session.setParameter("modeling.useVisSvc","true");
  session.setParameter("modeling.modeling","solid");
  IUserInterfaceSvc* uiSvc = find_uiSvc(aUI.session());
  if ( !uiSvc)  return;

  // draw RICH2
  session_setColor(session,"cyan");
  const std::string & srich = "/dd/Structure/LHCb/AfterMagnetRegion/Rich2";
  uiSvc->visualize(srich);

}

//////////////////////////////////////////////////////////////////////////////
void layout_Rich1HPDPanels(
 IUI& aUI
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  //////////////////////////////////////////////////////////////////////////
  // Prepare the page :
  //////////////////////////////////////////////////////////////////////////
  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  soPage->deleteRegions();
  soPage->titleVisible.setValue(FALSE);
  soPage->createRegions("SoDisplayRegion",1,2,0);

  // Rich1 upper panel
  {
    page_setCurrentRegion(*soPage,0);
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(0,0,0));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(747.782);
      camera->position.setValue(1154.168F,-20353.951F,-9486.5918F);
      float angle = 3.1033111F;
      camera->orientation.setValue
        (SbRotation(SbVec3f(-0.015347386F,0.85296381F,-0.52174419F),angle));
      camera->nearDistance.setValue(21561.0);
      camera->farDistance.setValue(28855.881F);
      camera->focalDistance.setValue(24983.93F);
    }
  }

  // Second region : Rich1 lower panel
  {
    page_setCurrentRegion(*soPage,1);
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(0,0,0));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(747.782);
      camera->position.setValue(1154.168F,20353.951F,-9486.5918F);
      float angle = 3.1033111F;
      camera->orientation.setValue
        (SbRotation(SbVec3f(0.015347386F,0.85296381F,0.52174419F),angle));
      camera->nearDistance.setValue(21561.0);
      camera->farDistance.setValue(28855.881F);
      camera->focalDistance.setValue(24983.93F);
    }
    page_connect_current_region(*soPage,0);
  }

  //////////////////////////////////////////////////////////////////////////
  // Visualize :
  //////////////////////////////////////////////////////////////////////////
  page_setCurrentRegion(*soPage,0);

  ISession& session = aUI.session();
  IUserInterfaceSvc* uiSvc = find_uiSvc(session);
  if ( !uiSvc ) return;
  session.setParameter("modeling.useVisSvc","true");
  session.setParameter("modeling.modeling","wireFrame");
  //session.setParameter("modeling.modeling","solid");

}

//////////////////////////////////////////////////////////////////////////////
void layout_Rich2HPDPanels(
 IUI& aUI
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  //////////////////////////////////////////////////////////////////////////
  // Prepare the page :
  //////////////////////////////////////////////////////////////////////////
  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  soPage->deleteRegions();
  soPage->titleVisible.setValue(FALSE);
  soPage->createRegions("SoDisplayRegion",2,1,0);

  // Rich2 left panel
  {
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(0,0,0));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(928.81);
      camera->position.setValue(-7494.12, 302.366, 4064.35);
      float angle = 4.1833463F;
      camera->orientation.setValue
        (SbRotation(SbVec3f(0.011812864F,0.99987644F,0.010366189F),angle));
      camera->nearDistance.setValue(1497.88);
      camera->farDistance.setValue(13573.3);
      camera->focalDistance.setValue(6963.33);
    }
  }
  // Second region : Rich2 right panel
  {
    page_setCurrentRegion(*soPage,1);
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(0,0,0));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(928.81);
      camera->position.setValue(7494.12, 302.366, 4064.35);
      float angle = -4.1833463F;
      camera->orientation.setValue
        (SbRotation(SbVec3f(-0.011812864F,0.99987644F,0.010366189F),angle));
      camera->nearDistance.setValue(1497.88);
      camera->farDistance.setValue(13573.3);
      camera->focalDistance.setValue(6963.33);
    }
    page_connect_current_region(*soPage,0);
  }

  //////////////////////////////////////////////////////////////////////////
  // Visualize :
  //////////////////////////////////////////////////////////////////////////
  page_setCurrentRegion(*soPage,0);

  ISession& session = aUI.session();
  IUserInterfaceSvc* uiSvc = find_uiSvc(session);
  if ( !uiSvc ) return;
  session.setParameter("modeling.useVisSvc","true");
  session.setParameter("modeling.modeling","wireFrame");
  //session.setParameter("modeling.modeling","solid");

}

//////////////////////////////////////////////////////////////////////////////
void layout_AllRichHPDPanels(
 IUI& aUI
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  //////////////////////////////////////////////////////////////////////////
  // Prepare the page :
  //////////////////////////////////////////////////////////////////////////
  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  soPage->deleteRegions();
  soPage->titleVisible.setValue(FALSE);
  soPage->createRegions("SoDisplayRegion",2,2,0);

  // First region : Rich1 upper panel
  {
    page_setCurrentRegion(*soPage,0);
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(0,0,0));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(1250);
      camera->position.setValue(1154.168F,-20353.951F,-9486.5918F);
      float angle = 3.1033111F;
      camera->orientation.setValue
        (SbRotation(SbVec3f(-0.015347386F,0.85296381F,-0.52174419F),angle));
      camera->nearDistance.setValue(21221.51F);
      camera->farDistance.setValue(28855.881F);
      camera->focalDistance.setValue(24983.93F);
    }
  }
  // Second region : Rich1 lower panel
  {
    page_setCurrentRegion(*soPage,1);
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(0,0,0));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(1250);
      camera->position.setValue(1154.168F,20353.951F,-9486.5918F);
      float angle = 3.1033111F;
      camera->orientation.setValue
        (SbRotation(SbVec3f(0.015347386F,0.85296381F,0.52174419F),angle));
      camera->nearDistance.setValue(21221.51F);
      camera->farDistance.setValue(28855.881F);
      camera->focalDistance.setValue(24983.93F);
    }
    page_connect_current_region(*soPage,0);
  }
  // First region : Rich2 left panel
  {
    page_setCurrentRegion(*soPage,2);
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(0,0,0));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(2000);
      camera->position.setValue(-7497.7017F,227.00066F,4066.6152F);
      float angle = 4.1833463F;
      camera->orientation.setValue
        (SbRotation(SbVec3f(0.011812864F,0.99987644F,0.010366189F),angle));
      camera->nearDistance.setValue(668.16394F);
      camera->farDistance.setValue(15711.722F);
      camera->focalDistance.setValue(6963.3296F);
    }
    page_connect_current_region(*soPage,0);
  }
  // First region : Rich2 right panel
  {
    page_setCurrentRegion(*soPage,3);
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(0,0,0));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(2000);
      camera->position.setValue(7497.7017F,227.00066F,4066.6152F);
      float angle = -4.1833463F;
      camera->orientation.setValue
        (SbRotation(SbVec3f(-0.011812864F,0.99987644F,0.010366189F),angle));
      camera->nearDistance.setValue(668.16394F);
      camera->farDistance.setValue(15711.722F);
      camera->focalDistance.setValue(6963.3296F);
    }
    page_connect_current_region(*soPage,0);
  }

  //////////////////////////////////////////////////////////////////////////
  // Visualize :
  //////////////////////////////////////////////////////////////////////////
  page_setCurrentRegion(*soPage,0);

  ISession& session = aUI.session();
  IUserInterfaceSvc* uiSvc = find_uiSvc(session);
  if ( !uiSvc ) return;
  session.setParameter("modeling.useVisSvc","true");
  session.setParameter("modeling.modeling","wireFrame");
  //session.setParameter("modeling.modeling","solid");

}

//////////////////////////////////////////////////////////////////////////////
void layout_CAD(
 IUI& aUI
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  //////////////////////////////////////////////////////////////////////////
  // Prepare the page :
  //////////////////////////////////////////////////////////////////////////
  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  soPage->deleteRegions();
  soPage->titleVisible.setValue(FALSE);
  soPage->createRegions("SoDisplayRegion",2,2,0);
  
  // First region ; side view, +z at right :
  {
    page_setCurrentRegion(*soPage,0);
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(1,1,1));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(14000);
      camera->position.setValue(-10000,0,9000);
      camera->orientation.setValue(SbRotation(SbVec3f(0,1,0),-FM_PI_2));
      camera->nearDistance.setValue(1);
      camera->farDistance.setValue(100000);
      //camera->focalDistance.setValue();
    }
  }
  
  // Second region ; front view :
  {
    page_setCurrentRegion(*soPage,1);
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(1,1,1));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(14000);
      camera->position.setValue(0,0,20000);
      camera->orientation.setValue(SbRotation(SbVec3f(0,1,0),0));
      camera->nearDistance.setValue(1);
      camera->farDistance.setValue(100000);
      //camera->focalDistance.setValue();
    }
    page_connect_current_region(*soPage,0);
  }
  
  // Third region ; top view :
  {
    page_setCurrentRegion(*soPage,2);
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(1,1,1));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(14000);
      camera->position.setValue(0,2000,9000);
      float angle = 2.0F*FM_PI/3.0F; //2.0943
      camera->orientation.setValue(SbRotation(SbVec3f(-1,-1,-1),angle));
      camera->nearDistance.setValue(1);
      camera->farDistance.setValue(100000);
    }
    page_connect_current_region(*soPage,0);
  }
  
  // Third region ; 3D view :
  {
    page_setCurrentRegion(*soPage,3);
    SoRegion* soRegion = page_currentRegion(*soPage);
    if(!soRegion) return;
    soRegion->color.setValue(SbColor(1,1,1));
    SoCamera* soCamera = soRegion->getCamera();
    if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
      SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
      camera->height.setValue(14000);
      camera->position.setValue(-2612.4268F,2074.0654F,4631.1782F);
      float angle = 2.3861356F;
      camera->orientation.setValue
        (SbRotation(SbVec3f(-0.12087149F,-0.94958508F,-0.28927207F),angle));
      camera->nearDistance.setValue(1);
      camera->farDistance.setValue(100000);
    }
    page_connect_current_region(*soPage,0);
  }
  
  //////////////////////////////////////////////////////////////////////////
  // Visualize :
  //////////////////////////////////////////////////////////////////////////
  page_setCurrentRegion(*soPage,0);
  
  ISession& session = aUI.session();

  IUserInterfaceSvc* uiSvc = find_uiSvc(session);
  if(!uiSvc) return;

  session.setParameter("modeling.useVisSvc","false");
  session.setParameter("modeling.modeling","wireFrame");

  session_setColor(session,"0.8 0 0");
  uiSvc->visualize("/dd/Structure/LHCb/MagnetRegion/Magnet");
  session_setColor(session,"green");
  uiSvc->visualize("/dd/Structure/LHCb/AfterMagnetRegion/T/IT");
  session_setColor(session,"0.8 0 0");
  uiSvc->visualize("/dd/Structure/LHCb/DownstreamRegion/Ecal");
}
//////////////////////////////////////////////////////////////////////////////
void layout_velo(
 IUI& aUI
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  ////////////////////////////////////////////////////////////////////
  // Prepare the page :
  ////////////////////////////////////////////////////////////////////
  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  soPage->deleteRegions();
  soPage->titleVisible.setValue(FALSE);
  soPage->createRegions("SoDisplayRegion",1,1,0);
  
  SoRegion* soRegion = page_currentRegion(*soPage);
  if(!soRegion) return;
  soRegion->color.setValue(SbColor(0,0,0));
  SoCamera* soCamera = soRegion->getCamera();
  if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
    SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
    camera->height.setValue(100);
    camera->position.setValue(0,0,800);
    camera->orientation.setValue(SbRotation(SbVec3f(0,1,0),0));
    camera->nearDistance.setValue(1);
    camera->farDistance.setValue(1000);
    camera->focalDistance.setValue(800);
  }
  
  // Needed to set a z slice :
  Slash::UI::ISoViewer* soViewer = ui_SoViewer(aUI);  
  if(soViewer) {
    soViewer->setAutoClipping(false);
  }
  
  aUI.echo("Camera is at z 800 looking in -z direction.");
  aUI.echo("A z slice had been set in the interval [-200,800]");
  
  ////////////////////////////////////////////////////////////////////
  // Visualize :
  ////////////////////////////////////////////////////////////////////
  
  ISession& session = aUI.session();

  session.setParameter("modeling.useVisSvc","false");
  session.setParameter("modeling.modeling","wireFrame");
  
  // Stations are in [-200, 800]
  IUserInterfaceSvc* uiSvc = find_uiSvc(aUI.session());
  if(!uiSvc) return;

  std::string svelo = "/dd/Structure/LHCb/BeforeMagnetRegion/Velo";

  int i;
  for(i=0;i<=40;i+=2) {
    std::string s;
    std::string fmt = svelo + "/VeloLeft/Module%02d";
    Lib::smanip::printf(s,128,fmt.c_str(),i);
    uiSvc->visualize(s);
  }

  for(i=1;i<=41;i+=2) {
    std::string s;
    std::string fmt = svelo + "/VeloRight/Module%02d";
    Lib::smanip::printf(s,128,fmt.c_str(),i);
    uiSvc->visualize(s);
  }
}  
//////////////////////////////////////////////////////////////////////////////
void layout_velo_rz(
 IUI& aUI
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  ////////////////////////////////////////////////////////////////////
  // Prepare the page :
  ////////////////////////////////////////////////////////////////////
  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  soPage->deleteRegions();
  soPage->titleVisible.setValue(FALSE);
  soPage->createRegions("SoDisplayRegion",1,1,0);
  
  // ZR view :
  SoRegion* soRegion = page_currentRegion(*soPage);
  if(!soRegion) return;
  soRegion->color.setValue(SbColor(0,0,0));

  // Velo has Z in [-200, 800]. 
  // Max radius of the velo is around 60.
  // After ZR projection, Z is going to be on the x axis.
  // Have a global scale in order to bring R at the
  // same scale than Z ( scaleX = 1000/60 = 100/6 = 16.66).

  SoCamera* soCamera = soRegion->getCamera();
  if(soCamera->isOfType(SoOrthographicCamera::getClassTypeId())) {
    SoOrthographicCamera* camera = (SoOrthographicCamera*)soCamera;
    camera->height.setValue(1000);
    camera->position.setValue(300,400,100);
    camera->orientation.setValue(SbRotation(SbVec3f(0,1,0),0));
    camera->nearDistance.setValue(1);
    camera->farDistance.setValue(1000);
    //camera->focalDistance.setValue(800);
  }
  
  if(!soRegion->isOfType(SoDisplayRegion::getClassTypeId())) return;
  SoDisplayRegion* displayRegion = (SoDisplayRegion*)soRegion;
  displayRegion->getTransform()->scaleFactor.setValue(SbVec3f(1,16.6F,1));

  ISession& session = aUI.session();
  session.setParameter("modeling.projection","ZR");
  
  ////////////////////////////////////////////////////////////////////
  // Visualize :
  ////////////////////////////////////////////////////////////////////
  
  session.setParameter("modeling.useVisSvc","false");
  session.setParameter("modeling.modeling","wireFrame");

  // Stations are in [-200, 800]
  //visualize_velo(aUI);

}  
//////////////////////////////////////////////////////////////////////////////
void layout_rulers(
 IUI& aUI
,const std::vector<std::string>& aArgs
) 
//////////////////////////////////////////////////////////////////////////////
// aArgs[0] ruler size (default is 0.9)
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  double rulerSize = 0.9;
  if(aArgs.size()) {
    if(!Lib::smanip::todouble(aArgs[0],rulerSize)) return;
  }
  Slash::UI::ISoViewer* soViewer = ui_SoViewer(aUI);
  if(!soViewer) return;
  soViewer->setBackgroundColor(1,1,1);
  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  page_dressCurrentRegionWithRulers(*soPage,rulerSize);
}
//////////////////////////////////////////////////////////////////////////////
void layout_removeRulers(
 IUI& aUI
) 
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  SoPage* soPage = ui_SoPage(aUI);
  if(!soPage) return;
  page_removeRulers(*soPage);
}

} // extern "C"
