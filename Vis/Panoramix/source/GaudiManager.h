#ifndef Panoramix_GaudiManager_h
#define Panoramix_GaudiManager_h

#include <Lib/Interfaces/IManager.h>
class ISession;

#include <string>

class IAppMgrUI;

class GaudiManager : public IManager {
public: //IManager
  virtual std::string name() const;
  virtual void* cast(const std::string&) const;
public:
  GaudiManager(ISession&,const std::string&);
  ~GaudiManager();
private:
  std::string fName;
  ISession& fSession;
  IAppMgrUI* fAppMgrUI;
};

#endif
