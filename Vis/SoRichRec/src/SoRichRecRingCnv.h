// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================
#ifndef   SORICHREC_SORICHRECRINGCNV_H
#define   SORICHREC_SORICHRECRINGCNV_H 1

// interfaces
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/RichDetectorType.h"
#include "Event/RichRecPointOnRing.h"

#include "RichRecInterfaces/IRichRayTraceCherenkovCone.h"

// SoRich
#include "SoRichBaseCnv.h"

// forward decalarations
template <class CNV> class CnvFactory;

/**  @class SoRichRecRingCnv  SoRichRecRingCnv.h
 *
 *   Converter for visualization of
 *   container of RichRecRing objects
 *
 *   @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *   @date    09/05/2004
 */

class SoRichRecRingCnv : public SoRichBaseCnv
{

  friend class CnvFactory<SoRichRecRingCnv>;

public:

  /// standard initialization method
  virtual StatusCode initialize ();

  /// standard finalization  method
  virtual StatusCode finalize   ();

  virtual long repSvcType() const;

  /// the only one essential method
  virtual StatusCode createRep( DataObject* /* Object */ ,
                                IOpaqueAddress*&  /* Address */ );

  /// Class ID for created object == class ID for this specific converter
  static const CLID&  classID();

  /// storage Type
  static unsigned char storageType () ;

protected:

  /// standard constructor
  SoRichRecRingCnv( ISvcLocator* svcLoc );

  /// virtual destructor
  virtual ~SoRichRecRingCnv();

private:

  /// default constructor is disabled
  SoRichRecRingCnv           (                       ) ;

  /// copy constructor is disabled
  SoRichRecRingCnv           ( const SoRichRecRingCnv& ) ;

  /// assignment is disabled
  SoRichRecRingCnv& operator=( const SoRichRecRingCnv& ) ;

private:

  /// Ray tracing tool
  const Rich::Rec::IRayTraceCherenkovCone * m_rayTrace;

};


// ============================================================================
#endif  // SORICHREC_SORICHRECRINGCNV_H
// ============================================================================













