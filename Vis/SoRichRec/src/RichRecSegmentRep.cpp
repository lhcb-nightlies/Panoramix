// $Id: RichRecSegmentRep.cpp,v 1.9 2009-05-26 07:45:44 gybarran Exp $ 
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================

#include <OnXSvc/Win32.h>

// This :
#include "RichRecSegmentRep.h"

#include <cmath>

// CLHEP
#include <GaudiKernel/Point3DTypes.h>

// Inventor
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoIndexedLineSet.h>

#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoSceneGraph.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>

// RichEvent
#include <Event/RichRecSegment.h>

// namespaces
using namespace LHCb;

// ============================================================================
/** @file RichRecSegmentRep.cpp
 *  Implementation of class RichRecSegmentRep
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */
// ============================================================================


// ============================================================================
/** standard constructor
 */
// ============================================================================
RichRecSegmentRep::RichRecSegmentRep( const std::string& Type,
                                      const Qualities & qualities,
                                      SoStyleCache* styleCache)
  : m_type        ( Type      )
  , m_qualities   ( qualities )
  , m_styleCache  ( styleCache)
  , m_coordinate  ( 0 )
  , m_icoord      ( 0 )
{ }

// ============================================================================
// destructor
// ============================================================================
RichRecSegmentRep::~RichRecSegmentRep(){}

// ============================================================================
// ============================================================================
SoSeparator* RichRecSegmentRep::begin()
{
  SoSeparator* separator = new SoSeparator();
  separator->addChild(m_styleCache->getLightModelBaseColor());
  separator->addChild(m_styleCache->getLineStyle());

  m_coordinate = new SoCoordinate3;
  separator->addChild(m_coordinate);
  m_icoord = 0;

  return separator;
}

void RichRecSegmentRep::represent( const RichRecSegment * segment
                                   ,SoSeparator* aSeparator )
{
  if ( !m_coordinate ) return;

  // Build picking string id :
  char sid[64];
  ::sprintf(sid,"RichRecSegment/0x%lx",(unsigned long)segment);

  SoSceneGraph * separator = new SoSceneGraph();
  separator->setString(sid);

  aSeparator->addChild(separator);

  separator->addChild( m_styleCache->getHighlightMaterial(qualities().color(),
                                                          qualities().hcolor()) );

  if ( markerStyle() == "line" )
  {

    // references to geometrical information
    const RichTrackSegment & seg = segment->trackSegment();
    const Gaudi::XYZPoint & ent = seg.entryPoint();
    const Gaudi::XYZPoint & mid = seg.middlePoint();
    const Gaudi::XYZPoint & ext = seg.exitPoint();

    int32_t pointn = 4;
    SbVec3f points[4];
    int32_t coordIndex[3*2];

    points[0].setValue( ent.x(), ent.y(), ent.z() );
    points[1].setValue( mid.x(), mid.y(), mid.z() );
    coordIndex[0] = m_icoord + 0;
    coordIndex[1] = m_icoord + 1;
    coordIndex[2] = SO_END_LINE_INDEX;

    points[2].setValue( mid.x(), mid.y(), mid.z() );
    points[3].setValue( ext.x(), ext.y(), ext.z() );
    coordIndex[3] = m_icoord + 2;
    coordIndex[4] = m_icoord + 3;
    coordIndex[5] = SO_END_LINE_INDEX;

    m_coordinate->point.setValues(m_icoord,pointn,points);
    m_icoord += pointn;

    SoIndexedLineSet* lineSet = new SoIndexedLineSet;
    lineSet->coordIndex.setValues(0,6,coordIndex);
    separator->addChild(lineSet);
  }
}

// ============================================================================
