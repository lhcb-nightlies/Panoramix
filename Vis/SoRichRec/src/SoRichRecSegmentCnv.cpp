// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================

#include  "OnXSvc/Win32.h"

// This :
#include  "SoRichRecSegmentCnv.h"

// Inventor :
#include <Inventor/nodes/SoSeparator.h>

// GaudiKernel
#include <GaudiKernel/IRegistry.h>
#include <GaudiKernel/CnvFactory.h>
#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiKernel/SmartDataPtr.h>
#include <GaudiKernel/DataObject.h>
#include <GaudiKernel/IToolSvc.h>

// OnXSvc
#include <OnXSvc/IUserInterfaceSvc.h>
#include <OnXSvc/ClassID.h>
#include <OnXSvc/Helpers.h>

// Lib :
#include <Lib/smanip.h>
#include <Lib/Interfaces/ISession.h>

// HEPVis :
#include <HEPVis/nodekits/SoRegion.h>

// RichEvent
#include <Event/RichRecSegment.h>

// Local
#include "RichRecSegmentRep.h"

// namespaces
using namespace LHCb;

/** @file SoRichRecSegmentCnv.cpp
 *
 *  Implementation of SoRichRecSegmentCnv class
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

// ============================================================================
// mandatory factory business
// ============================================================================
DECLARE_CONVERTER_FACTORY(SoRichRecSegmentCnv)

// ============================================================================
// standard constructor
// ============================================================================
SoRichRecSegmentCnv::SoRichRecSegmentCnv( ISvcLocator* svcLoc )
  : SoRichBaseCnv ( classID(), svcLoc )
{
  setName ( "SoRichRecSegmentCnv" );
}

// ============================================================================
/// standard desctructor
// ============================================================================
SoRichRecSegmentCnv::~SoRichRecSegmentCnv(){}

// ============================================================================
/// initialize
// ============================================================================
StatusCode SoRichRecSegmentCnv::initialize()
{
  StatusCode sc = SoRichBaseCnv::initialize();
  if ( sc.isFailure() )
    return Error("initialize: Could not initialize base class!");

  return StatusCode::SUCCESS;
}

// ============================================================================
/// finalize
// ============================================================================
StatusCode SoRichRecSegmentCnv::finalize()
{
  return SoRichBaseCnv::finalize();
}

long SoRichRecSegmentCnv::repSvcType() const
{
  return i_repSvcType();
}

// ============================================================================
/// Class ID for created object == class ID for this specific converter
// ============================================================================
const CLID& SoRichRecSegmentCnv::classID()
{
  return RichRecSegments::classID();
}

// ============================================================================
/// storage Type
// ============================================================================
unsigned char SoRichRecSegmentCnv::storageType()
{
  return So_TechnologyType;
}

// ============================================================================
// the only one essential method
// ============================================================================
StatusCode SoRichRecSegmentCnv::createRep( DataObject*         object     ,
                                           IOpaqueAddress*& /* Address */ )
{

  // Preliminary checks
  if ( 0 == object  ) return Error("createRep: DataObject* points to NULL");
  if ( 0 == uiSvc() ) return Error("createRep: IUserInterfaceSvc* points to NULL" );

  SoRegion* soRegion = uiSvc()->currentSoRegion();
  if(soRegion==0)
  { return Error("createRep: SoRegion* points to NULL" );}

  ISession* session = uiSvc()->session();
  if(session==0)
  { return Error("createRep: ISession* points to NULL" );}

  const RichRecSegments* segments = dynamic_cast<const RichRecSegments*>( object );
  if ( 0 == segments  )
  { return Error( "createRep: Wrong input data type '" +
                  System::typeinfoName(typeid(object)) + "'" ); }
  if ( segments->empty() )
  { return Debug( "Container " + object->registry()->identifier() +
                  " is empty" ); }

  // Find first non-null digit
  typedef RichRecSegments::const_iterator RiMCSegIt;
  RiMCSegIt iseg;
  for ( iseg = segments->begin(); iseg != segments->end(); ++iseg ) { if(*iseg) break; }
  if ( segments->end() == iseg )
  { return Warning("Container " + object->registry()->identifier() +
                   " contains NULL pointers !" , StatusCode::SUCCESS ) ;}

  // Colours
  double r, g, b, hr, hg, hb;
  std::string color1, color2;
  if ( !session->parameterValue( "modeling.color", color1 ) ) color1 = "red";
  Lib::smanip::torgb(color1,r,g,b);
  SbColor color(r,g,b);
  if ( !session->parameterValue( "modeling.highlightColor", color2 ) ) color2 = "red";
  Lib::smanip::torgb(color2,hr,hg,hb);
  SbColor hcolor(hr,hg,hb);

  // marker
  std::string shape;
  if ( !session->parameterValue("modeling.RichRecSegmentMode",shape) ) shape = "line";

  // create visualisation object
  Debug( "Visualizer for RichRecSegments : Style= " +shape +
         " : colors="+color1+" "+color2 );
  RichRecSegmentRep vis( shape,
                         RichRecSegmentRep::Qualities
                         ( color, hcolor, SoMarkerSet::CIRCLE_FILLED_5_5 ),
                         soRegion->styleCache());

  SoSeparator* separator = vis.begin();

  bool empty = true;
  for ( iseg = segments->begin(); iseg != segments->end(); ++iseg ) {
    if(!(*iseg)) continue;
    vis.represent(*iseg,separator);
    empty = false;
  }

  if(empty) {
    separator->unref();
  } else {
    soRegion->resetUndo();
    region_addToDynamicScene(*soRegion,separator);
  }

  return StatusCode::SUCCESS;
}
