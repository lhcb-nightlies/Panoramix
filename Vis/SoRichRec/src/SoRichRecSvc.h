#ifndef SoRich_SoRichRecSvc_h
#define SoRich_SoRichRecSvc_h

// Inheritance :
#include "GaudiKernel/Service.h"
#include "GaudiKernel/MsgStream.h"

// forward declaration 
template <class SVC> class SvcFactory;

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IToolSvc;
class IJobOptionsSvc;

class SoRichRecSvc : public Service
{
  friend class SvcFactory<SoRichRecSvc>; 
public:
  virtual StatusCode initialize();
  virtual StatusCode finalize();
  virtual ~SoRichRecSvc();
protected:
  SoRichRecSvc(const std::string& name,ISvcLocator* SvcLoc);  
private:  
  SoRichRecSvc();
  SoRichRecSvc( const SoRichRecSvc& );
  SoRichRecSvc& operator=( const SoRichRecSvc& );
private:
  inline MsgStream & msgStream() 
  {
    if ( !m_msgStream ) { m_msgStream = new MsgStream(msgSvc(), Service::name()); }
    return *m_msgStream; 
  }
  MsgStream & debug()       { return msgStream() << MSG::DEBUG;   }
  MsgStream & info()        { return msgStream() << MSG::INFO;    }
  MsgStream & error()       { return msgStream() << MSG::WARNING; }
  MsgStream & warning()     { return msgStream() << MSG::ERROR;   }
private:
  template < class SVC >
  void releaseSvc( SVC *& svc ) { if (svc) { svc->release(); svc=NULL; } }
  template < class SVC >
  StatusCode getService( const std::string & name, SVC *& svc )
  {
    releaseSvc(svc);
    const StatusCode sc = service(name,svc,true);
    if ( sc.isFailure() || !svc ) 
    {
      error() << name << " not found" << endmsg;
    } else { svc->addRef(); }
    return sc;
  }
private:
  IUserInterfaceSvc* m_uiSvc;
  ISoConversionSvc* m_soConSvc;
  IDataProviderSvc* m_evtDataSvc;
  IToolSvc* m_toolSvc;
  IJobOptionsSvc* m_joSvc;
  MsgStream* m_msgStream;
};

#endif
