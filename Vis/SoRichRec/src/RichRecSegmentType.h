#ifndef SoRichRec_RichRecSegmentType_h
#define SoRichRec_RichRecSegmentType_h

// Inheritance :
#include "RichRecTypeBase.h"

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IToolSvc;
class IJobOptionsSvc;

class RichRecSegmentType : public RichRecTypeBase<LHCb::RichRecSegment>
{
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
  virtual void visualize(Lib::Identifier aIdentifier,void*);
public:
  RichRecSegmentType(IUserInterfaceSvc*,
                     ISoConversionSvc*,
                     IDataProviderSvc*,
                     IToolSvc*,
                     IJobOptionsSvc*,
                     MsgStream&);
};

#endif
