// $Id: RichRecRingRep.cpp,v 1.21 2009-05-29 14:29:17 jonrob Exp $ 
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================

#include <OnXSvc/Win32.h>

// This :
#include "RichRecRingRep.h"

#include <cmath>

// from Gaudi
#include <GaudiKernel/IAlgTool.h>

// Inventor
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoIndexedLineSet.h>

#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoSceneGraph.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>

// RichEvent
#include <Event/RichRecRing.h>

// namespaces
using namespace LHCb;

// ============================================================================
/** @file RichRecRingRep.cpp
 *  Implementation of class RichRecRingRep
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */
// ============================================================================

// ============================================================================
/** standard constructor
 */
// ============================================================================
RichRecRingRep::RichRecRingRep( const std::string& Shape,
                                const Qualities & qualities,
                                const Rich::Rec::IRayTraceCherenkovCone * rayTrace,
                                SoStyleCache* styleCache)
  : m_shape       ( Shape     )
  , m_qualities   ( qualities )
  , m_rayTrace    ( rayTrace  )
  , m_styleCache  ( styleCache)
  , m_coordinate  ( 0 )
  , m_icoord      ( 0 )
  , m_markers     ( 0 )
  , m_lines       ( 0 )
{ }

// ============================================================================
// destructor
// ============================================================================
RichRecRingRep::~RichRecRingRep(){}

// ============================================================================
// ============================================================================

SoSeparator* RichRecRingRep::begin()
{
  SoSeparator* separator = new SoSeparator();
  separator->addChild(m_styleCache->getLightModelBaseColor());
  separator->addChild(m_styleCache->getLineStyle());

  m_markers = new SoSeparator();
  separator->addChild(m_markers);

  m_lines = new SoSeparator();
  separator->addChild(m_lines);

  m_coordinate = new SoCoordinate3;
  m_lines->addChild(m_coordinate);
  m_icoord = 0;

  return separator;
}

void RichRecRingRep::represent( const RichRecRing * ring
                                ,SoSeparator* //aSeparator
                                )
{
  // Build picking string id :
  char sid[64];
  ::sprintf(sid,"RichRecRing/0x%lx",(unsigned long)ring);

  SoHighlightMaterial* highlightMaterial =
    m_styleCache->getHighlightMaterial(qualities().color(),
                                       qualities().hcolor());

  if ( shape() == "center" || shape() == "anywhere" )
  {
    // Draw the centre point

    // new SoSeparator
    SoSceneGraph * separator = new SoSceneGraph();
    separator->setString(sid);

    m_markers->addChild(separator);

    separator->addChild(highlightMaterial);

    SbVec3f points[1];
    points[0].setValue( ring->centrePointGlobal().x(),
                        ring->centrePointGlobal().y(),
                        ring->centrePointGlobal().z() );

    // Add one So per particle
    SoCoordinate3* coordinate3 = new SoCoordinate3;
    coordinate3->point.setValues(0,1,points);
    separator->addChild( coordinate3 );

    // So marker type
    SoMarkerSet* markerSet = new SoMarkerSet;
    markerSet->numPoints = 1;
    markerSet->markerIndex = qualities().markerType();
    separator->addChild( markerSet );

  }
  if ( shape() != "center" || shape() == "anywhere" )
  {

    // do ray tracing if needed
    const LHCb::RichTraceMode traceMode ( LHCb::RichTraceMode::RespectPDTubes );
    if (!rayTrace()) return;
    const int nPoints = static_cast<int>( 5000 * ring->radius() );
    const StatusCode sc =
      rayTrace()->rayTrace( const_cast<RichRecRing*>(ring), nPoints, traceMode );
    if ( sc.isFailure() )
    {
      std::cout << "RichRecRingRep :: Problem ray-tracing ring" << std::endl;
      return;
    }

    // draw the ring (if enough points )
    if ( ring->ringPoints().size() > 2 )
    {
      // new SoSeparator
      SoSceneGraph * separator = new SoSceneGraph();
      separator->setString(sid);

      m_lines->addChild(separator);

      separator->addChild(highlightMaterial);

      // guess at number of points
      int32_t nLines  = ring->ringPoints().size();
      SbVec3f * points = new SbVec3f[2 * nLines];
      int32_t* coordIndex = new int32_t[3 * nLines];

      int nPoint(0), coordi(0);

      // loop round ring and create lines to draw
      LHCb::RichRecPointOnRing::Vector::const_iterator iPF = ring->ringPoints().begin();
      LHCb::RichRecPointOnRing::Vector::const_iterator iP1 = iPF;
      LHCb::RichRecPointOnRing::Vector::const_iterator iP2 = iPF; ++iP2;
      for ( ; iP2 != ring->ringPoints().end(); ++iP2, ++iP1 )
      {
        if ( okToDraw( (*iP1), (*iP2), ring->rich() ) )
        {
          points[nPoint].setValue( (*iP1).globalPosition().x(),
                                   (*iP1).globalPosition().y(),
                                   (*iP1).globalPosition().z() );
          coordIndex[coordi] = m_icoord + nPoint;
          ++nPoint;
          ++coordi;

          points[nPoint].setValue( (*iP2).globalPosition().x(),
                                   (*iP2).globalPosition().y(),
                                   (*iP2).globalPosition().z() );
          coordIndex[coordi] = m_icoord + nPoint;
          ++nPoint;
          ++coordi;

          coordIndex[coordi] = SO_END_LINE_INDEX;
          ++coordi;
        }
      }
      if ( okToDraw( (*iP1), (*iPF), ring->rich() ) )
      {
        points[nPoint].setValue( (*iP1).globalPosition().x(),
                                 (*iP1).globalPosition().y(),
                                 (*iP1).globalPosition().z() );
        coordIndex[coordi] = m_icoord + nPoint;
        ++nPoint;
        ++coordi;

        points[nPoint].setValue( (*iPF).globalPosition().x(),
                                 (*iPF).globalPosition().y(),
                                 (*iPF).globalPosition().z() );
        coordIndex[coordi] = m_icoord + nPoint;
        ++nPoint;
        ++coordi;

        coordIndex[coordi] = SO_END_LINE_INDEX;
        ++coordi;
      }

      if ( nPoint > 0 )
      {
        m_coordinate->point.setValues(m_icoord,nPoint,points);
        m_icoord += nPoint;
        SoIndexedLineSet* lineSet = new SoIndexedLineSet;
        lineSet->coordIndex.setValues(0,coordi,coordIndex);
        separator->addChild(lineSet);
      }
      else
      {
        //std::cout << "RichRecRingRep :: Not enough points selected for drawing..." << std::endl;
      }

      delete [] coordIndex;
      delete [] points;

    }

  }

}

bool RichRecRingRep::okToDraw( const LHCb::RichRecPointOnRing & p1,
                               const LHCb::RichRecPointOnRing & p2,
                               const Rich::DetectorType rich ) const
{
  const bool in = inHPD(p1) && inHPD(p2);
  if ( shape() == "inside" )
  {
    return ( in &&
             sameSide( p1.globalPosition(), p2.globalPosition(), rich ) );
  }
  else if ( shape() == "outside" )
  {
    return ( !in &&
             sameSide( p1.globalPosition(), p2.globalPosition(), rich ) );
  }
  else if ( shape() == "anywhere" )
  {
    return ( sameSide( p1.globalPosition(), p2.globalPosition(), rich ) );
  }
  // if get here, reject
  return false;
}

// ============================================================================
