// $Id: GaudiHelperTool.h,v 1.1 2009-07-30 14:53:17 jonrob Exp $
#ifndef GAUDIHELPERTOOL_H 
#define GAUDIHELPERTOOL_H 1

// Rich base class
#include "RichKernel/RichToolBase.h"

/** @class GaudiHelperTool GaudiHelperTool.h
 *  
 *  Helper class for 'Type' classes
 * 
 *  @author Chris Jones
 *  @date   2009-07-08
 */
class GaudiHelperTool : public Rich::ToolBase
{

public: 

  /// Standard constructor
  GaudiHelperTool( const IInterface* parent )
    : Rich::ToolBase ( "GaudiHelperTool", "GaudiHelperTool", parent ) 
  {
    this->setProperty("Context","Offline");
  }
  
  /// Destructor
  virtual ~GaudiHelperTool( ) { }

};

#endif // GAUDIHELPERTOOL_H
