// $Id: RichRecPhotonRep.h,v 1.12 2009-05-29 14:29:17 jonrob Exp $ 
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================

#ifndef  SORICHREC_RICHRECPHOTONREP_H
#define  SORICHREC_RICHRECPHOTONREP_H 1

#include <Inventor/SbColor.h>
#include "MarkerSet.h"

#include <string>

#include <GaudiKernel/Point3DTypes.h>

#include "Kernel/MemPoolAlloc.h"

class SoSeparator;
class SoStyleCache;
class SoCoordinate3;

namespace LHCb
{
  class RichRecPhoton;
}

// ============================================================================
/** @class RichRecPhotonRep  RichRecPhotonRep.h
 *
 *  Helper class(functor) which performs the real
 *  visualization of RichRecPhoton Object
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */
// ============================================================================

class RichRecPhotonRep  : public LHCb::MemPoolAlloc<RichRecPhotonRep>
{

public: // help class

  class Qualities {

  public:

    Qualities( const SbColor & color,
               const SbColor & hcolor,
               const SoMarkerSet::MarkerType mType )
      : m_color  ( color  ),
        m_hcolor ( hcolor ),
        m_mType  ( mType  ) { }

    const SbColor & color()  const { return m_color;  }
    const SbColor & hcolor() const { return m_hcolor; }
    SoMarkerSet::MarkerType markerType() const { return m_mType; }

  private:

    SbColor m_color;
    SbColor m_hcolor;
    SoMarkerSet::MarkerType m_mType;

  };

public:

  /** standard constructor
   */
  RichRecPhotonRep( const std::string & Type,
                    const Qualities & qualities,
                    SoStyleCache* styleCache );

  /** destructor
   */
  ~RichRecPhotonRep();

  /** begin a scene graph for a collection.
   *  @return the top separator.
   */
  SoSeparator* begin();

  /** represent one hit.
   *  @param hit pointer to the hit
   *  @param sep the top separator to put the sub-scene graph.
   */
  void represent(const LHCb::RichRecPhoton* photon,SoSeparator* sep);

private: // methods

  const std::string & markerStyle() const       { return m_type; }
  const Qualities & qualities() const { return m_qualities; }

  /// test if mirror reflection point is 'valid'
  inline bool mirrorPointOK( const Gaudi::XYZPoint & point ) const
  {
    return ( point.z() > 5 );
  }

private: // data

  std::string m_type;

  const Qualities m_qualities;

  SoStyleCache* m_styleCache;
  SoCoordinate3* m_coordinate;
  int            m_icoord;
};

// ============================================================================
#endif //  SORICHREC_RICHRECPHOTONREP_H
// ============================================================================











