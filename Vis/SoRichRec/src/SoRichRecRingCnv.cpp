// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================

#include <OnXSvc/Win32.h>

// This :
#include "SoRichRecRingCnv.h"

// Inventor :
#include <Inventor/nodes/SoSeparator.h>

// from Gaudi
#include <GaudiKernel/IAlgTool.h>
#include <GaudiKernel/IRegistry.h>
#include <GaudiKernel/CnvFactory.h>
#include <GaudiKernel/IDataProviderSvc.h>
#include <GaudiKernel/SmartDataPtr.h>
#include <GaudiKernel/DataObject.h>
#include <GaudiKernel/IToolSvc.h>

// OnXSvc
#include <OnXSvc/IUserInterfaceSvc.h>
#include <OnXSvc/ClassID.h>
#include <OnXSvc/Helpers.h>

// Lib :
#include <Lib/smanip.h>
#include <Lib/Interfaces/ISession.h>

// HEPVis :
#include <HEPVis/nodekits/SoRegion.h>

// RichEvent
#include <Event/RichRecRing.h>

// Local
#include "RichRecRingRep.h"

// namespaces
using namespace LHCb;

/** @file SoRichRecRingCnv.cpp
 *
 *  Implementation of SoRichRecRingCnv class
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */

// ============================================================================
// mandatory factory business
// ============================================================================
DECLARE_CONVERTER_FACTORY(SoRichRecRingCnv)

// ============================================================================
// standard constructor
// ============================================================================
SoRichRecRingCnv::SoRichRecRingCnv( ISvcLocator* svcLoc )
  : SoRichBaseCnv ( classID(), svcLoc ),
    m_rayTrace    ( 0 )
{
  setName ( "SoRichRecRingCnv" );
}

// ============================================================================
// standard desctructor
// ============================================================================
SoRichRecRingCnv::~SoRichRecRingCnv(){}

// ============================================================================
// initialize
// ============================================================================
StatusCode SoRichRecRingCnv::initialize()
{
  StatusCode sc = SoRichBaseCnv::initialize();
  if ( sc.isFailure() )
    return Error("initialize: Could not initialize base class!");

  // Acquire tools
  acquireTool( "RichRayTraceCKCone", m_rayTrace );

  return StatusCode::SUCCESS;
}

// ============================================================================
// finalize
// ============================================================================
StatusCode SoRichRecRingCnv::finalize()
{
  // release tools
  // causes a crash due to fact tools are finalised in Panoramix BEFORE services
  // TO BE FIXED
  //releaseTool( m_rayTrace );

  return SoRichBaseCnv::finalize();
}

long SoRichRecRingCnv::repSvcType() const
{
  return i_repSvcType();
}

// ============================================================================
// Class ID for created object == class ID for this specific converter
// ============================================================================
const CLID& SoRichRecRingCnv::classID()
{
  return RichRecRings::classID();
}

// ============================================================================
// storage Type
// ============================================================================
unsigned char SoRichRecRingCnv::storageType()
{
  return So_TechnologyType;
}

// ============================================================================
// the only one essential method
// ============================================================================
StatusCode SoRichRecRingCnv::createRep( DataObject*         object     ,
                                        IOpaqueAddress*& /* Address */ )
{

  // Preliminary checks
  if ( 0 == object  ) return Error("createRep: DataObject* points to NULL");
  if ( 0 == uiSvc() ) return Error("createRep: IUserInterfaceSvc* points to NULL" );

  SoRegion* soRegion = uiSvc()->currentSoRegion();
  if(soRegion==0) 
    { return Error("createRep: SoRegion* points to NULL" );}

  ISession* session = uiSvc()->session();
  if(session==0) 
    { return Error("createRep: ISession* points to NULL" );}

  const RichRecRings* rings = dynamic_cast<const RichRecRings*>( object );
  if ( 0 == rings  )
  { return Error( "createRep: Wrong input data type '" +
                  System::typeinfoName(typeid(object)) + "'" ); }
  if ( rings->empty() )
  { return Debug( "Container " + object->registry()->identifier() +
                  " is empty" ); }

  // Find first non-null digit
  typedef RichRecRings::const_iterator RiMCSegIt;
  RiMCSegIt iseg;
  for ( iseg = rings->begin(); iseg != rings->end(); ++iseg ) { if(*iseg) break; }
  if ( rings->end() == iseg )
  { return Warning("Container " + object->registry()->identifier() +
                   " contains NULL pointers !" , StatusCode::SUCCESS ) ;}

  // Colours
  double r, g, b, hr, hg, hb;
  std::string color1, color2;
  if ( !session->parameterValue( "modeling.color", color1 ) ) color1 = "red";
  Lib::smanip::torgb(color1,r,g,b);
  SbColor color(r,g,b);
  if ( !session->parameterValue( "modeling.highlightColor", color2 ) ) color2 = "red";
  Lib::smanip::torgb(color2,hr,hg,hb);
  SbColor hcolor(hr,hg,hb);

  // marker
  std::string shape;
  if ( !session->parameterValue("modeling.RichRecRingMode",shape) ) shape = "anywhere";

  // create visualisation object
  Debug( "Visualizer for RichRecRings : Style= " +shape +
         " : colors="+color1+" "+color2 );
  RichRecRingRep vis( shape,
                      RichRecRingRep::Qualities
                        ( color, hcolor, SoMarkerSet::TRIANGLE_FILLED_5_5 ),
                      m_rayTrace,
                      soRegion->styleCache());

  SoSeparator* separator = vis.begin();

  bool empty = true;
  for ( iseg = rings->begin(); iseg != rings->end(); ++iseg ) { 
    if(!(*iseg)) continue; 
    vis.represent(*iseg,separator);
    empty = false;
  }

  if(empty) {
    separator->unref();
  } else {
    soRegion->resetUndo();
    region_addToDynamicScene(*soRegion,separator);
  }

  return StatusCode::SUCCESS;
}
