// this :
#include "RichRecRingType.h"

//////////////////////////////////////////////////////////////////////////////
RichRecRingType::RichRecRingType( IUserInterfaceSvc* aUISvc,
                                  ISoConversionSvc* aSoCnvSvc,
                                  IDataProviderSvc* aDataProviderSvc,
                                  IToolSvc* aToolSvc,
                                  IJobOptionsSvc* aJoSvc,
                                  MsgStream & msgStream )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  : RichRecTypeBase<LHCb::RichRecRing>(LHCb::RichRecRings::classID(),
                                       "RichRecRing",
                                       LHCb::RichRecRingLocation::SegmentHypoRings,
                                       aUISvc,aSoCnvSvc,aDataProviderSvc,
                                       aToolSvc,aJoSvc,msgStream)
{
  addProperty("Key",Lib::Property::INTEGER);
  addProperty("centerX",Lib::Property::DOUBLE);
  addProperty("centerY",Lib::Property::DOUBLE);
  addProperty("centerZ",Lib::Property::DOUBLE);
  addProperty("MassHypo",Lib::Property::STRING);
  addProperty("Type",Lib::Property::INTEGER);
  addProperty("Alg",Lib::Property::INTEGER);
  addProperty("AssocTrackKey",Lib::Property::INTEGER);
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable RichRecRingType::value( Lib::Identifier aIdentifier,
                                      const std::string& aName,
                                      void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::RichRecRing* obj = (LHCb::RichRecRing*)aIdentifier;
  if ( obj )
  {
    const Gaudi::XYZPoint& p = obj->centrePointGlobal();
    if      ( aName == "Key" )
    {
      return Lib::Variable(printer(),(int)obj->key());
    }
    else if (aName=="centerX") 
    {
      return Lib::Variable(printer(),p.x());
    } 
    else if(aName=="centerY") 
    {
      return Lib::Variable(printer(),p.y());
    } 
    else if(aName=="centerZ") 
    {
      return Lib::Variable(printer(),p.z());
    }
    else if(aName=="MassHypo") 
    {
      return Lib::Variable(printer(),Rich::text(obj->massHypo()));
    } 
    else if(aName=="Type") 
    {
      return Lib::Variable(printer(),obj->type());
    }
    else if(aName=="Alg") 
    {
      return Lib::Variable(printer(),obj->algorithm());
    } 
    else if(aName=="AssocTrackKey") 
    {
      LHCb::RichRecSegment* s = obj->richRecSegment();
      if ( s ) return Lib::Variable(printer(),s->richRecTrack()->key());
    }
  }
  return Lib::Variable(printer());
}
//////////////////////////////////////////////////////////////////////////////
void RichRecRingType::visualize( Lib::Identifier aIdentifier,
                                 void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::RichRecRing* ring = (LHCb::RichRecRing*)aIdentifier;

  std::string value;
  fUISvc->session()->parameterValue("modeling.what",value);

  //std::cout << "In RichRecRingType::visualize " << ring << " " << value << std::endl;

  if      ( value=="RichRecoSegments" )
  {
    drawSegment( ring->richRecSegment() );
  }
  else if ( value=="RichRecoPixels"   )
  {
    drawPixels( ring );
  }
  else if ( value=="RichRecoPhotons"  )
  {
    drawPhotons( ring->richRecSegment() );
  }
  else if ( value=="RichRecoTracks" )
  {
    drawTrack( ring->richRecSegment() );
  }
  else if ( value=="RichMCSegments"  )
  {
    drawMCSegment ( ring->richRecSegment() );
  }
  else if ( value=="RichMCPixels"    )
  {
    drawMCPixels ( ring->richRecSegment() );
  }
  else if ( value=="RichMCPhotons"   )
  {
    drawMCPhotons ( ring->richRecSegment() );
  }
  else if ( value=="RichMCCKRings"   )
  {
    drawMCRings ( ring->richRecSegment() );
  }
  else
  {
    drawRing ( ring );
  }
}
