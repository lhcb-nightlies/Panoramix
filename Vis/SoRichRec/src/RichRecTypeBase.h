// $Id: RichRecTypeBase.h,v 1.6 2009-09-25 10:46:01 jonrob Exp $
#ifndef RICHRECTYPEBASE_H
#define RICHRECTYPEBASE_H 1

// Inheritance :
#include <OnXSvc/KeyedType.h>

#include <OnXSvc/Helpers.h>

#include <Lib/Out.h>

#include <Inventor/nodes/SoSeparator.h>

#include <Event/RichRecSegment.h>

#include <GaudiKernel/IToolSvc.h>

#include "RichRecSegmentRep.h"
#include "RichRecPixelRep.h"
#include "RichRecPhotonRep.h"
#include "RichRecRingRep.h"

// MC
#include "MCInterfaces/IRichRecMCTruthTool.h"
#include "Event/MCRichSegment.h"
#include "Event/MCRichOpticalPhoton.h"

#include "GaudiKernel/MsgStream.h"

#include "GaudiHelperTool.h"

/** @class RichRecTypeBase RichRecTypeBase.h
 *
 *  base class for all 'Type' classes
 *
 *  @author Chris Jones
 *  @date   2007-05-06
 */

template <class Object>
class RichRecTypeBase : public OnXSvc::KeyedType<Object>
{

public:

  /// Standard constructor
  RichRecTypeBase( const CLID &          aCLID,
                   const std::string &   aType,
                   const std::string &   aLocation,
                   IUserInterfaceSvc *   aUISvc,
                   ISoConversionSvc *    aSoCnvSvc,
                   IDataProviderSvc *    aDataProviderSvc,
                   IToolSvc*             aToolSvc,
                   IJobOptionsSvc*       /* aJoSvc */, // currently not used, but keep just in case
                   MsgStream &           msgStream )
    : OnXSvc::KeyedType<Object>( aCLID,
                                 aType,
                                 aLocation,
                                 aUISvc,
                                 aSoCnvSvc,
                                 aDataProviderSvc ),
      m_msgStream     ( msgStream ),
      m_rayTrace      (NULL),
      m_truth         (NULL),
      m_separatorSeg  (NULL),
      m_separatorRing (NULL),
      m_separatorPix  (NULL),
      m_separatorPhot (NULL),
      m_repSeg        (NULL),
      m_repRing       (NULL),
      m_repPix        (NULL),
      m_repPhot       (NULL),
      m_emptySeg      (true),
      m_emptyRing     (true),
      m_emptyPix      (true),
      m_emptyPhot     (true) 
  {
    // initialise static tool helper
    toolHelper(aToolSvc);
  }

  /// Destructor
  virtual ~RichRecTypeBase()
  {
    // remove static tool helper
    toolHelper(NULL,true);
    // FIXME : Causes a crash since the services are finalized after the tools
    //if ( m_rayTrace ) toolHelper()->releaseTool(m_rayTrace);
    //m_rayTrace = NULL;
  }

public:

  /// Begin visualization
  virtual void beginVisualize()
  {
    OnXSvc::KeyedType<Object>::beginVisualize();
    m_emptySeg  = true;
    m_emptyRing = true;
    m_emptyPix  = true;
    m_emptyPhot = true;
    photonRep();
    pixelRep();
    segmentRep();
    ringRep();
  }

  /// End visualization
  virtual void endVisualize()
  {
    endVisualize(m_emptySeg,m_separatorSeg);
    endVisualize(m_emptyRing,m_separatorRing);
    endVisualize(m_emptyPix,m_separatorPix);
    endVisualize(m_emptyPhot,m_separatorPhot);
    delete m_repSeg;    m_repSeg    = NULL;
    delete m_repRing;   m_repRing   = NULL;
    delete m_repPix;    m_repPix    = NULL;
    delete m_repPhot;   m_repPhot   = NULL;
    OnXSvc::KeyedType<Object>::endVisualize();
  }

private:

  /// Add given SoSeparator to the scene
  void endVisualize( const bool empty, SoSeparator*& sep )
  {
    if ( sep )
    {
      if ( empty ) { sep->unref(); }
      else
      {
        this -> fSoRegion -> resetUndo();
        region_addToDynamicScene(*(this->fSoRegion),sep);
      }
    }
  }

protected:

  /// Access to Message stream
  MsgStream & msgStream() const  { return m_msgStream;                 }
  /// Debug message
  MsgStream & always()    const  { return msgStream() << MSG::ALWAYS;  }
  /// Debug message
  MsgStream & verbose()   const  { return msgStream() << MSG::VERBOSE; }
  /// Debug message
  MsgStream & debug()     const  { return msgStream() << MSG::DEBUG;   }
  /// Info message
  MsgStream & info()      const  { return msgStream() << MSG::INFO;    }
  /// Warning message
  MsgStream & error()     const  { return msgStream() << MSG::WARNING; }
  /// Error message
  MsgStream & warning()   const  { return msgStream() << MSG::ERROR;   }

protected:

  /// Access on-demand the RICH MC truth tool
  const Rich::Rec::MC::IMCTruthTool * richMCTruth() const
  {
    if ( !m_truth )
    {
      toolHelper()->acquireTool("RichRecMCTruthTool",m_truth);
      if ( !m_truth ) { error() << "Failed to get RICH MCTruth tool" << endmsg; }
      else            { info()  << "Loaded RICH MCTruth tool"        << endmsg; }
    }
    return m_truth;
  }

  /// Access the static tool helper
  inline GaudiHelperTool * toolHelper( IToolSvc * aToolSvc = NULL,
                                       const bool cleanUp  = false ) const
  {
    static GaudiHelperTool * helper = new GaudiHelperTool(aToolSvc);
    if ( cleanUp ) { delete helper; helper = NULL; }
    return helper;
  }

protected:

  /// Draw a RichRecSegment
  void drawSegment( const LHCb::RichRecSegment * segment )
  {
    if ( segment )
    {
      verbose() << "Visualizing RichRecSegment " << *segment << endmsg;
      segmentRep()->represent(segment,m_separatorSeg);
      m_emptySeg = false;
    }
  }

  /// Draw a RichRecPixel
  void drawPixel( const LHCb::RichRecPixel * pixel )
  {
    if ( pixel )
    {
      verbose() << "Visualizing RichRecPixel " << *pixel << endmsg;
      pixelRep()->represent(pixel,m_separatorPix);
      m_emptyPix = false;
    }
  }

  /// Draw a RichRecRing
  void drawRing( const LHCb::RichRecRing * ring )
  {
    if ( ring )
    {
      verbose() << "Visualizing RichRecRing " << *ring << endmsg;
      ringRep()->represent(ring,m_separatorRing);
      m_emptyRing = false;
    }
  }

  /// Draw a RichRecPhoton
  void drawPhoton( const LHCb::RichRecPhoton * photon )
  {
    if ( photon )
    {
      verbose() << "Visualizing RichRecPhoton " << *photon << endmsg;
      photonRep()->represent(photon,m_separatorPhot);
      m_emptyPhot = false;
    }
  }

  /// Draw Associated Track
  void drawTrack( LHCb::Track * track )
  {
    LHCb::Tracks toDraw;
    toDraw.add( track );
    drawContainer(toDraw);
  }

  /// Draw Associated Track
  void drawTrack( const LHCb::RichRecSegment * segment )
  {
    drawTrack( (LHCb::Track*)segment->richRecTrack()->parentTrack() );
  }

  /// Draw the RichRecPixels associated to the given RichRecSegment
  void drawPixels( const LHCb::RichRecSegment * segment )
  {
    if ( segment && !segment->richRecPixels().empty() )
    {
      for ( LHCb::RichRecSegment::Pixels::const_iterator iPix = segment->richRecPixels().begin();
            iPix != segment->richRecPixels().end(); ++ iPix )
      {
        drawPixel( *iPix );
      }
    }
  }

  /// Draw the RichRecPixels associated to the given RichRecRing
  void drawPixels( const LHCb::RichRecRing * ring )
  {
    // Does the ring have any pixels associated ?
    if ( ring && !ring->richRecPixels().empty() )
    {
      for ( std::vector<LHCb::RichRecPixelOnRing>::const_iterator iPix = ring->richRecPixels().begin();
            iPix != ring->richRecPixels().end(); ++iPix )
      {
        drawPixel(iPix->pixel());
      }
    }
    // if not, use the pixels associated to the segment
    else if ( ring && ring->richRecSegment() )
    {
      drawPixels ( ring->richRecSegment() );
    }
  }

  /// Draw the RichRecPhotons associated to the given RichRecPixel
  void drawPhotons( const LHCb::RichRecPixel * pixel )
  {
    if ( pixel )
    {
      for ( LHCb::RichRecPixel::Photons::const_iterator iPhot = pixel->richRecPhotons().begin();
            iPhot != pixel->richRecPhotons().end(); ++iPhot )
      {
        drawPhoton ( *iPhot );
      }
    }
  }

  /// Draw the RichRecPhotons associated to the given RichRecSegment
  void drawPhotons( const LHCb::RichRecSegment * segment )
  {
    if ( segment )
    {
      for ( LHCb::RichRecSegment::Photons::const_iterator iPhot = segment->richRecPhotons().begin();
            iPhot != segment->richRecPhotons().end(); ++iPhot )
      {
        drawPhoton( *iPhot );
      }
    }
  }

  /// Draw the RichRecRings associated to the given RichRecSegment
  void drawRings( const LHCb::RichRecSegment * segment )
  {
    if ( segment )
    {
      const Rich::HypoData<LHCb::RichRecRing*> & rings = segment->hypothesisRings();
      drawRing( rings[Rich::Electron] );
      drawRing( rings[Rich::Muon] );
      drawRing( rings[Rich::Pion] );
      drawRing( rings[Rich::Kaon] );
      drawRing( rings[Rich::Proton] );
    }
  }

  /// Draw the RichRecSegments associated to the given RichRecPixel
  void drawSegments( const LHCb::RichRecPixel * pixel )
  {
    if ( pixel )
    {
      for ( LHCb::RichRecPixel::Tracks::const_iterator iT = pixel->richRecTracks().begin();
            iT != pixel->richRecTracks().end(); ++iT )
      {
        drawSegment( (*iT)->segmentInRad(Rich::Aerogel)  );
        drawSegment( (*iT)->segmentInRad(Rich::Rich1Gas) );
        drawSegment( (*iT)->segmentInRad(Rich::Rich2Gas) );
      }
    }
  }

  /// Draw the tracks associated to the given RichRecPixel
  void drawTracks( const LHCb::RichRecPixel * pixel )
  {
    if ( pixel )
    {
      for ( LHCb::RichRecPixel::Tracks::const_iterator iT = pixel->richRecTracks().begin();
            iT != pixel->richRecTracks().end(); ++iT )
      {
        drawTrack( (LHCb::Track*)(*iT)->parentTrack() );
      }
    }
  }

  /// Draw the RichRecRings associated to the given RichRecPixel
  void drawRings( const LHCb::RichRecPixel * pixel )
  {
    if ( pixel )
    {
      for ( LHCb::RichRecPixel::Tracks::const_iterator iT = pixel->richRecTracks().begin();
            iT != pixel->richRecTracks().end(); ++iT )
      {
        drawRings( (*iT)->segmentInRad(Rich::Aerogel)  );
        drawRings( (*iT)->segmentInRad(Rich::Rich1Gas) );
        drawRings( (*iT)->segmentInRad(Rich::Rich2Gas) );
      }
    }
  }

private:

  /// Get the RichRecTracks
  LHCb::RichRecTracks * richRecTracks() const
  {
    DataObject* dataObject(NULL);
    std::string loc;
    bool t = this->fUISvc->session()->parameterValue("RichRecTrack.location",loc);
    if ( !t || ""==loc ) loc = "/Event/Rec/Rich/RecoEvent/Offline/Tracks";
    const StatusCode sc = this->fDataProviderSvc->retrieveObject(loc,dataObject);
    return ( sc.isSuccess() ? dynamic_cast<LHCb::RichRecTracks*>(dataObject) : NULL );
  }

  /// Get the RichRecPixels
  LHCb::RichRecPixels * richRecPixels() const
  {
    DataObject* dataObject(NULL);
    std::string loc;
    bool t = this->fUISvc->session()->parameterValue("RichRecPixel.location",loc);
    if ( !t || ""==loc ) loc = "/Event/Rec/Rich/RecoEvent/Offline/Pixels";
    const StatusCode sc = this->fDataProviderSvc->retrieveObject(loc,dataObject);
    return ( sc.isSuccess() ? dynamic_cast<LHCb::RichRecPixels*>(dataObject) : NULL );
  }

  /// Draw the data container
  template < typename TYPE >
  inline void drawContainer( TYPE & objs ) const
  {
    if ( !objs.empty() )
    {
      IOpaqueAddress * addr = 0;
      StatusCode sc = this->fSoCnvSvc->createRep(&objs,addr);
      if ( sc.isSuccess() ) sc = this->fSoCnvSvc->fillRepRefs(addr,&objs);
      if ( sc.isFailure() ) error() << "Cannot visualize "
                                    << System::typeinfoName(typeid(objs)) << endmsg;
      objs.clear();
    }
  }

protected:

  /// Print a warning method if MC associations that require extended RICH MC info are requested
  void richExtendedMCMessage() const
  {
    static bool done = false;
    if ( !richMCTruth()->extendedMCAvailable() && !done )
    {
      always() << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
               << "Visualising RICH MC Information"
               << "     -> Requires extended RICH data with addition MC information stored"
               << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
               << endmsg;
      done = true;
    }
  }

  /// Draw the MC segment associated to the given RichRecSegment
  void drawMCSegment( const LHCb::RichRecSegment * segment )
  {
    if ( segment && richMCTruth() )
    {
      richExtendedMCMessage();
      // find MCRichSegment
      LHCb::MCRichSegment * mcSeg
        = const_cast<LHCb::MCRichSegment*>(richMCTruth()->mcRichSegment(segment));
      if ( mcSeg )
      {
        LHCb::MCRichSegments toDraw;
        toDraw.add(mcSeg);
        drawContainer(toDraw);
      }
    }
  }

  /// Draw the MC photons associated to the given pixel
  void drawMCPhoton( const LHCb::RichRecPhoton * photon )
  {
    if ( photon && richMCTruth() )
    {
      richExtendedMCMessage();
      // get the MC photon
      const LHCb::MCRichOpticalPhoton * mcPhot = richMCTruth()->trueOpticalPhoton(photon);
      if ( mcPhot )
      {
        LHCb::MCRichOpticalPhotons toDraw;
        toDraw.add(const_cast<LHCb::MCRichOpticalPhoton*>(mcPhot));
        drawContainer(toDraw);
      }
    }
  }

  /// Draw the MC pixels associated to the given RichRecSegment
  void drawMCPixels( const LHCb::RichRecSegment * segment )
  {
    if ( segment && richMCTruth() )
    {
      // loop over all pixels
      LHCb::RichRecPixels * pixels = richRecPixels();
      if ( pixels )
      {
        for ( LHCb::RichRecPixels::const_iterator iPix = pixels->begin();
              iPix != pixels->end(); ++iPix )
        {
          // Is this pixel MC associated to the segment ?
          if ( NULL != richMCTruth()->trueRecPhoton(segment,*iPix) )
          {
            drawPixel(*iPix);
          }
        }
      }
    }
  }

  /// Draw the MC photons associated to the given RichRecSegment
  void drawMCPhotons( const LHCb::RichRecSegment * segment )
  {
    if ( segment && richMCTruth() )
    {
      richExtendedMCMessage();
      // find MCRichSegment
      LHCb::MCRichSegment * mcSeg
        = const_cast<LHCb::MCRichSegment*>(richMCTruth()->mcRichSegment(segment));
      if ( mcSeg )
      {
        LHCb::MCRichOpticalPhotons toDraw;
        // loop over photons
        for ( SmartRefVector<LHCb::MCRichOpticalPhoton>::const_iterator iMCPhot
                = mcSeg->mcRichOpticalPhotons().begin();
              iMCPhot != mcSeg->mcRichOpticalPhotons().end(); ++iMCPhot )
        {
          toDraw.add( const_cast<LHCb::MCRichOpticalPhoton*>(&**iMCPhot) );
        }
        drawContainer(toDraw);
      }
    }
  }

  /// Draw the MC CK RIngs associated to the given RichRecSegment
  void drawMCRings( const LHCb::RichRecSegment * segment )
  {
    if ( segment && richMCTruth() )
    {
      richExtendedMCMessage();
      drawRing( richMCTruth()->mcCKRing(segment) );
    }
  }

  /// Draw the MC segments associated to the given RichRecPixel
  void drawMCSegment ( const LHCb::RichRecPixel * pixel )
  {
    if ( pixel )
    {
      for ( LHCb::RichRecPixel::Tracks::const_iterator iT = pixel->richRecTracks().begin();
            iT != pixel->richRecTracks().end(); ++iT )
      {
        drawMCSegment( (*iT)->segmentInRad(Rich::Aerogel)  );
        drawMCSegment( (*iT)->segmentInRad(Rich::Rich1Gas) );
        drawMCSegment( (*iT)->segmentInRad(Rich::Rich2Gas) );
      }
    }
  }

  /// Draw the MC photons associated to the given pixel
  void drawMCPhotons( const LHCb::RichRecPixel * pixel )
  {
    if ( pixel )
    {
      // loop over segments for this pixel
      for ( LHCb::RichRecPixel::Tracks::const_iterator iT = pixel->richRecTracks().begin();
            iT != pixel->richRecTracks().end(); ++iT )
      {
        drawMCPhotons( (*iT)->segmentInRad(Rich::Aerogel)  );
        drawMCPhotons( (*iT)->segmentInRad(Rich::Rich1Gas) );
        drawMCPhotons( (*iT)->segmentInRad(Rich::Rich2Gas) );
      }
    }
  }

  /// Draw the MC CK Rings associated to the given pixel
  void drawMCRings( const LHCb::RichRecPixel * pixel )
  {
    if ( pixel )
    {
      // loop over segments for this pixel
      for ( LHCb::RichRecPixel::Tracks::const_iterator iT = pixel->richRecTracks().begin();
            iT != pixel->richRecTracks().end(); ++iT )
      {
        drawMCRings( (*iT)->segmentInRad(Rich::Aerogel)  );
        drawMCRings( (*iT)->segmentInRad(Rich::Rich1Gas) );
        drawMCRings( (*iT)->segmentInRad(Rich::Rich2Gas) );
      }
    }
  }

private:

  /// The modeling color
  SbColor color()
  {
    float r = 0.5, g = 0.5, b = 0.5;
    this -> attribute("modeling.color",r,g,b);
    return SbColor(r,g,b);
  }
  /// The highlight color
  SbColor hcolor()
  {
    float r = 1.0, g = 1.0, b = 1.0;
    this -> attribute("modeling.highlightColor",r,g,b);
    return SbColor(r,g,b);
  }

private:

  /// Make a new RichRecPhotonRep
  void newPhotonRep()
  {
    std::string shape;
    if(!this->attribute("modeling.RichRecPhotonMode",shape)) shape = "line";
    m_repPhot = new RichRecPhotonRep(shape,
                                     RichRecPhotonRep::Qualities
                                     (color(),hcolor(),SoMarkerSet::CIRCLE_FILLED_5_5),
                                     this->fSoRegion->styleCache());
    m_separatorPhot = m_repPhot->begin();
  }
  /// Access the RichRecPhotonRep
  RichRecPhotonRep * photonRep()
  {
    if ( !m_repPhot ) newPhotonRep();
    return m_repPhot;
  }

private:

  /// Make a new RichRecSegmentRep
  void newSegmentRep()
  {
    std::string shape;
    if(!this->attribute("modeling.RichRecSegmentMode",shape)) shape = "line";
    m_repSeg = new RichRecSegmentRep(shape,
                                     RichRecSegmentRep::Qualities
                                     (color(),hcolor(),SoMarkerSet::CIRCLE_FILLED_5_5),
                                     this->fSoRegion->styleCache());
    m_separatorSeg = m_repSeg->begin();
  }
  /// Access the RichRecSegmentRep
  RichRecSegmentRep * segmentRep()
  {
    if ( !m_repSeg ) newSegmentRep();
    return m_repSeg;
  }

private:

  /// Make a new RichRecRingRep
  void newRingRep()
  {
    std::string shape;
    if(!this->attribute("modeling.RichRecRingMode",shape)) shape = "inside";
    m_repRing = new RichRecRingRep(shape,
                                   RichRecRingRep::Qualities
                                   (color(),hcolor(),SoMarkerSet::CIRCLE_FILLED_5_5),
                                   rayTrace(),
                                   this->fSoRegion->styleCache());
    m_separatorRing = m_repRing->begin();
  }
  /// Access the RichRecRingRep
  RichRecRingRep * ringRep()
  {
    if ( !m_repRing ) newRingRep();
    return m_repRing;
  }

private:

  // Make a new RichRecPixelRep
  void newPixelRep()
  {
    std::string shape;
    if(!this->attribute("modeling.RichRecPixelMode",shape)) shape = "marker";
    m_repPix = new RichRecPixelRep(shape,
                                   RichRecPixelRep::Qualities
                                   (color(),hcolor(),SoMarkerSet::CIRCLE_FILLED_5_5),
                                   this->fSoRegion->styleCache());
    m_separatorPix = m_repPix->begin();
  }
  /// Access the RichRecPixelRep
  RichRecPixelRep * pixelRep()
  {
    if ( !m_repPix ) newPixelRep();
    return m_repPix;
  }

private:

  /// Access on demand the ray tracing tool
  const Rich::Rec::IRayTraceCherenkovCone* rayTrace()
  {
    if ( !m_rayTrace )
    {
      toolHelper()->acquireTool("RichRayTraceCKCone",m_rayTrace);
      if ( !m_rayTrace )
      {
        error() << "RichRecRingType::beginVisualize :"
                << " unable to get RichRayTraceCKCone tool."
                << endmsg;
      }
    }
    return m_rayTrace;
  }

private:

  MsgStream & m_msgStream; ///< Reference to a MsgStream object

  /// Ray tracing tool
  const Rich::Rec::IRayTraceCherenkovCone * m_rayTrace;

  /// Monte Carlo truth tool
  mutable const Rich::Rec::MC::IMCTruthTool * m_truth;

  SoSeparator* m_separatorSeg;
  SoSeparator* m_separatorRing;
  SoSeparator* m_separatorPix;
  SoSeparator* m_separatorPhot;

  RichRecSegmentRep* m_repSeg;
  RichRecRingRep*    m_repRing;
  RichRecPixelRep*   m_repPix;
  RichRecPhotonRep*  m_repPhot;

  bool m_emptySeg;
  bool m_emptyRing;
  bool m_emptyPix;
  bool m_emptyPhot;

};

#endif // RICHRECTYPEBASE_H
