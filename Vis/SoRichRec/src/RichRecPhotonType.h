#ifndef SoRichRec_RichRecPhotonType_h
#define SoRichRec_RichRecPhotonType_h

// Inheritance :
#include "RichRecTypeBase.h"

class IUserInterfaceSvc;
class ISoConversionSvc;
class IDataProviderSvc;
class IToolSvc;
class IJobOptionsSvc;

class RichRecPhotonType : public RichRecTypeBase<LHCb::RichRecPhoton>
{
public: //Lib::IType
  virtual Lib::Variable value(Lib::Identifier,const std::string&,void*);
  virtual void visualize(Lib::Identifier aIdentifier,void*);
public:
  RichRecPhotonType(IUserInterfaceSvc*,
                    ISoConversionSvc*,
                    IDataProviderSvc*,
                    IToolSvc*,
                    IJobOptionsSvc*,
                    MsgStream&);
};

#endif
