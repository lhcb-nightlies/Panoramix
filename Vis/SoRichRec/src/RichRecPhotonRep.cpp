// $Id: RichRecPhotonRep.cpp,v 1.9 2009-05-26 07:45:58 gybarran Exp $ 
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================

#include <OnXSvc/Win32.h>

// This :
#include "RichRecPhotonRep.h"

#include <cmath>

// Inventor
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoIndexedLineSet.h>

#include <HEPVis/misc/SoStyleCache.h>
#include <HEPVis/nodes/SoSceneGraph.h>
#include <HEPVis/nodes/SoHighlightMaterial.h>

// RichEvent
#include "Event/RichRecPhoton.h"

// namespaces
using namespace LHCb;

// ============================================================================
/** @file RichRecPhotonRep.cpp
 *  Implementation of class RichRecPhotonRep
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */
// ============================================================================


// ============================================================================
/** standard constructor
 */
// ============================================================================
RichRecPhotonRep::RichRecPhotonRep( const std::string& Type,
                                    const Qualities & qualities,
                                    SoStyleCache* styleCache)
  : m_type        ( Type      )
  , m_qualities   ( qualities ) 
  , m_styleCache  ( styleCache)
  , m_coordinate  ( 0 )
  , m_icoord      ( 0 )
{ }

// ============================================================================
/** destructor
 */
// ============================================================================
RichRecPhotonRep::~RichRecPhotonRep(){}

// ============================================================================
// ============================================================================
SoSeparator* RichRecPhotonRep::begin()
{
  SoSeparator* separator = new SoSeparator();
  separator->addChild(m_styleCache->getLightModelBaseColor());
  separator->addChild(m_styleCache->getLineStyle());

  m_coordinate = new SoCoordinate3;
  separator->addChild(m_coordinate);
  m_icoord = 0;

  return separator;
}


void RichRecPhotonRep::represent( 
 const RichRecPhoton* photon 
,SoSeparator* aSeparator
)
{
  // Build picking string id :
  char sid[64];
  ::sprintf(sid,"RichRecPhoton/0x%lx",(unsigned long)photon);

  SoSceneGraph * separator = new SoSceneGraph();
  separator->setString(sid);

  aSeparator->addChild(separator);

  separator->addChild(
    m_styleCache->getHighlightMaterial(qualities().color(),
                                       qualities().hcolor()));

  if ( markerStyle() == "line" ) 
  {

    // references to geometrical photon data
    const RichGeomPhoton & gPhot      = photon->geomPhoton();
    const Gaudi::XYZPoint & emissPt   = gPhot.emissionPoint();
    const Gaudi::XYZPoint & mirrSp    = gPhot.sphMirReflectionPoint();
    const Gaudi::XYZPoint & mirrFl    = gPhot.flatMirReflectionPoint();
    const Gaudi::XYZPoint & detPt     = gPhot.detectionPoint();

    // ray trace the photon
    int32_t nPoints = 6;
    int32_t coordIndex[9];
    SbVec3f points[6]; // create with max possible size
    if ( mirrorPointOK(mirrSp) && mirrorPointOK(mirrFl) )
    {
      points[0].setValue( emissPt.x(), emissPt.y(), emissPt.z() );
      points[1].setValue( mirrSp.x(),  mirrSp.y(),  mirrSp.z()  );
      points[2].setValue( mirrSp.x(),  mirrSp.y(),  mirrSp.z()  );
      points[3].setValue( mirrFl.x(),  mirrFl.y(),  mirrFl.z()  );
      points[4].setValue( mirrFl.x(),  mirrFl.y(),  mirrFl.z()  );
      points[5].setValue( detPt.x(),   detPt.y(),   detPt.z()   );
      coordIndex[0] = m_icoord + 0;
      coordIndex[1] = m_icoord + 1;
      coordIndex[2] = SO_END_LINE_INDEX;
      coordIndex[3] = m_icoord + 2;
      coordIndex[4] = m_icoord + 3;
      coordIndex[5] = SO_END_LINE_INDEX;
      coordIndex[6] = m_icoord + 4;
      coordIndex[7] = m_icoord + 5;
      coordIndex[8] = SO_END_LINE_INDEX;
    }
    else if ( mirrorPointOK(mirrSp) )
    {
      points[0].setValue( emissPt.x(), emissPt.y(), emissPt.z() );
      points[1].setValue( mirrSp.x(),  mirrSp.y(),  mirrSp.z()  );
      points[2].setValue( mirrSp.x(),  mirrSp.y(),  mirrSp.z()  );
      points[3].setValue( detPt.x(),   detPt.y(),   detPt.z()   );
      nPoints = 4;
      coordIndex[0] = m_icoord + 0;
      coordIndex[1] = m_icoord + 1;
      coordIndex[2] = SO_END_LINE_INDEX;
      coordIndex[3] = m_icoord + 2;
      coordIndex[4] = m_icoord + 3;
      coordIndex[5] = SO_END_LINE_INDEX;
    }
    else if ( mirrorPointOK(mirrFl) )
    {
      points[0].setValue( emissPt.x(), emissPt.y(), emissPt.z() );
      points[1].setValue( mirrFl.x(),  mirrFl.y(),  mirrFl.z()  );
      points[2].setValue( mirrFl.x(),  mirrFl.y(),  mirrFl.z()  );
      points[3].setValue( detPt.x(),   detPt.y(),   detPt.z()   );
      nPoints = 4;
      coordIndex[0] = m_icoord + 0;
      coordIndex[1] = m_icoord + 1;
      coordIndex[2] = SO_END_LINE_INDEX;
      coordIndex[3] = m_icoord + 2;
      coordIndex[4] = m_icoord + 3;
      coordIndex[5] = SO_END_LINE_INDEX;
    }
    else
    {
      nPoints = 0;
    }

    if ( nPoints > 0 )
    {
      m_coordinate->point.setValues(m_icoord,nPoints,points);
      m_icoord += nPoints;

      SoIndexedLineSet* lineSet = new SoIndexedLineSet;
      lineSet->coordIndex.setValues(0,3*(nPoints/2),coordIndex);
      separator->addChild(lineSet);

    }

  }
}

// ============================================================================
