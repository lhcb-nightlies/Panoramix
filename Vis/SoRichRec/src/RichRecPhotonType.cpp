// this :
#include "RichRecPhotonType.h"

//////////////////////////////////////////////////////////////////////////////
RichRecPhotonType::RichRecPhotonType( IUserInterfaceSvc* aUISvc,
                                      ISoConversionSvc* aSoCnvSvc,
                                      IDataProviderSvc* aDataProviderSvc,
                                      IToolSvc* aToolSvc,
                                      IJobOptionsSvc* aJoSvc,
                                      MsgStream & msgStream )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  : RichRecTypeBase<LHCb::RichRecPhoton>( LHCb::RichRecPhotons::classID(),
                                          "RichRecPhoton",
                                          LHCb::RichRecPhotonLocation::Offline,
                                          aUISvc,aSoCnvSvc,aDataProviderSvc,aToolSvc,aJoSvc,msgStream)
{
  addProperty("Key",Lib::Property::INTEGER);
  addProperty("emissionX",Lib::Property::DOUBLE);
  addProperty("emissionY",Lib::Property::DOUBLE);
  addProperty("emissionZ",Lib::Property::DOUBLE);
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable RichRecPhotonType::value( Lib::Identifier aIdentifier,
                                        const std::string& aName,
                                        void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::RichRecPhoton* obj = (LHCb::RichRecPhoton*)aIdentifier;
  if ( obj )
  {
    const LHCb::RichGeomPhoton& gPhot = obj->geomPhoton();
    const Gaudi::XYZPoint& p = gPhot.emissionPoint();
    if     ( aName == "Key" )
    {
      return Lib::Variable(printer(),(int)obj->key());
    }
    else if(aName=="emissionX")
    {
      return Lib::Variable(printer(),p.x());
    }
    else if(aName=="emissionY")
    {
      return Lib::Variable(printer(),p.y());
    }
    else if(aName=="emissionZ")
    {
      return Lib::Variable(printer(),p.z());
    }
    else
    {
      return Lib::Variable(printer());
    }
  }
  return Lib::Variable(printer());
}
//////////////////////////////////////////////////////////////////////////////
void RichRecPhotonType::visualize( Lib::Identifier aIdentifier,
                                   void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::RichRecPhoton* photon = (LHCb::RichRecPhoton*)aIdentifier;

  std::string value;
  fUISvc->session()->parameterValue("modeling.what",value);
  if      ( value=="RichRecoSegments" )
  {
    drawSegment( photon->richRecSegment() );
  }
  else if ( value=="RichRecoPixels"   )
  {
    drawPixel( photon->richRecPixel() );
  }
  else if ( value=="RichRecoCKRings" )
  {
    drawRings( photon->richRecSegment() );
  }
  else if ( value=="RichRecoTracks" )
  {
    drawTrack( photon->richRecSegment() );
  }
  else if ( value=="RichMCSegments"  )
  {
    drawMCSegment ( photon->richRecSegment() );
  }
  else if ( value=="RichMCPhotons"   )
  {
    drawMCPhoton ( photon );
  }
  else if ( value=="RichMCCKRings"   )
  {
    drawMCRings ( photon->richRecSegment() );
  }
  else if ( value=="RichMCPixels"    )
  {
    drawMCPixels( photon->richRecSegment() );
  }
  else
  {
    drawPhoton( photon );
  }
}
//////////////////////////////////////////////////////////////////////////////
