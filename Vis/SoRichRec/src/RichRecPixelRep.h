// $Id: RichRecPixelRep.h,v 1.10 2009-05-29 14:29:17 jonrob Exp $ 
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================

#ifndef  SORICHREC_RICHRECPIXELREP_H
#define  SORICHREC_RICHRECPIXELREP_H 1

#include <Inventor/SbColor.h>
#include "MarkerSet.h"

#include <string>

#include "Kernel/MemPoolAlloc.h"

class SoSeparator;
class SoStyleCache;
class SoCoordinate3;

namespace LHCb
{
  class RichRecPixel;
}

// ============================================================================
/** @class RichRecPixelRep  RichRecPixelRep.h
 *
 *  Helper class(functor) which performs the real
 *  visualization of RichRecPixel Object
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */
// ============================================================================

class RichRecPixelRep : public LHCb::MemPoolAlloc<RichRecPixelRep>
{

public: // help class

  class Qualities {

  public:

    Qualities( const SbColor & color,
               const SbColor & hcolor,
               const SoMarkerSet::MarkerType mType )
      : m_color  ( color  ),
        m_hcolor ( hcolor ),
        m_mType  ( mType  ) { }

    const SbColor & color()  const { return m_color;  }
    const SbColor & hcolor() const { return m_hcolor; }
    SoMarkerSet::MarkerType markerType() const { return m_mType; }

  private:

    SbColor m_color;
    SbColor m_hcolor;
    SoMarkerSet::MarkerType m_mType;

  };

public:

  /** standard constructor
   */
  RichRecPixelRep( const std::string & Type,
                   const Qualities & qualities,
                   SoStyleCache* styleCache  );

  /** destructor
   */
  ~RichRecPixelRep();

  /** begin a scene graph for a collection.
   *  @return the top separator.
   */
  SoSeparator* begin();

  /** represent one hit.
   *  @param hit pointer to the hit
   *  @param sep the top separator to put the sub-scene graph.
   */
  void represent(const LHCb::RichRecPixel* pixel,SoSeparator* sep);

private: // methods

  const std::string & markerStyle() const   { return m_type; }
  const Qualities & qualities() const { return m_qualities; }

private: // data

  std::string m_type;

  const Qualities m_qualities;

  SoStyleCache* m_styleCache;
  SoCoordinate3* m_coordinate;
  int            m_icoord;
};

// ============================================================================
#endif //  SORICHREC_RICHRECPIXELREP_H
// ============================================================================











