// this :
#include "RichRecPixelType.h"

//////////////////////////////////////////////////////////////////////////////
RichRecPixelType::RichRecPixelType( IUserInterfaceSvc* aUISvc,
                                    ISoConversionSvc* aSoCnvSvc,
                                    IDataProviderSvc* aDataProviderSvc,
                                    IToolSvc* aToolSvc,
                                    IJobOptionsSvc* aJoSvc,
                                    MsgStream & msgStream )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
  : RichRecTypeBase<LHCb::RichRecPixel>( LHCb::RichRecPixels::classID(),
                                         "RichRecPixel",
                                         LHCb::RichRecPixelLocation::Offline,
                                         aUISvc,aSoCnvSvc,aDataProviderSvc,
                                         aToolSvc,aJoSvc,msgStream )
{
  addProperty("Key",  Lib::Property::INTEGER);
  addProperty("gPosX",Lib::Property::DOUBLE );
  addProperty("gPosY",Lib::Property::DOUBLE );
  addProperty("gPosZ",Lib::Property::DOUBLE );
}
//////////////////////////////////////////////////////////////////////////////
Lib::Variable RichRecPixelType::value( Lib::Identifier aIdentifier,
                                       const std::string& aName,
                                       void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::RichRecPixel* obj = (LHCb::RichRecPixel*)aIdentifier;
  if ( obj )
  {
    const Gaudi::XYZPoint & gPos = obj->globalPosition();

    if      ( aName == "Key" )
    {
      return Lib::Variable(printer(),obj->key());
    }
    else if ( aName == "gPosX" )
    {
      return Lib::Variable(printer(),gPos.x());
    }
    else if ( aName == "gPosY" )
    {
      return Lib::Variable(printer(),gPos.y());
    }
    else if ( aName == "gPosZ" )
    {
      return Lib::Variable(printer(),gPos.z());
    }
    else
    {
      return Lib::Variable(printer());
    }
  }
  return Lib::Variable(printer());
}
//////////////////////////////////////////////////////////////////////////////
void RichRecPixelType::visualize( Lib::Identifier aIdentifier,
                                  void* )
//////////////////////////////////////////////////////////////////////////////
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
{
  LHCb::RichRecPixel * pixel = (LHCb::RichRecPixel*)aIdentifier;

  std::string value;
  fUISvc->session()->parameterValue("modeling.what",value);

  //std::cout << "In RichRecPixelType::visualize " << pixel << " " << value << std::endl;

  if      ( value=="RichRecoSegments" )
  {
    drawSegments( pixel );
  }
  else if ( value=="RichRecoPhotons" )
  {
    drawPhotons( pixel );
  }
  else if ( value=="RichRecoCKRings" )
  {
    drawRings( pixel );
  }
  else if ( value=="RichRecoTracks" )
  {
    drawTracks( pixel );
  }
  else if ( value=="RichMCSegments"  )
  {
    drawMCSegment ( pixel );
  }
  else if ( value=="RichMCPhotons"   )
  {
    drawMCPhotons ( pixel );
  }
  else if ( value=="RichMCCKRings"   )
  {
    drawMCRings ( pixel );
  }
  else
  {
    drawPixel ( pixel );
  }
}
//////////////////////////////////////////////////////////////////////////////
