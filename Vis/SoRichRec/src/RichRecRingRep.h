// $Id: RichRecRingRep.h,v 1.21 2009-05-29 14:29:17 jonrob Exp $ 
// ============================================================================
// CVS tag $Name: not supported by cvs2svn $
// ============================================================================

#ifndef  SORICHREC_RICHRECRINGREP_H
#define  SORICHREC_RICHRECRINGREP_H 1

#include <Inventor/SbColor.h>
#include "MarkerSet.h"

#include <string>

#include <GaudiKernel/Point3DTypes.h>
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/RichDetectorType.h"
#include "Event/RichRecPointOnRing.h"

#include "RichRecInterfaces/IRichRayTraceCherenkovCone.h"

#include "Kernel/MemPoolAlloc.h"

class SoSeparator;
class SoStyleCache;
class SoCoordinate3;

namespace LHCb
{
  class RichRecRing;
}

// ============================================================================
/** @class RichRecRingRep  RichRecRingRep.h
 *
 *  Helper class(functor) which performs the real
 *  visualization of RichRecRing Object
 *
 *  @author  Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date    09/05/2004
 */
// ============================================================================

class RichRecRingRep  : public LHCb::MemPoolAlloc<RichRecRingRep>
{

public: // help class

  class Qualities {

  public:

    Qualities( const SbColor & color,
               const SbColor & hcolor,
               const SoMarkerSet::MarkerType mType )
      : m_color  ( color  ),
        m_hcolor ( hcolor ),
        m_mType  ( mType  ) { }

    const SbColor & color()  const { return m_color;  }
    const SbColor & hcolor() const { return m_hcolor; }
    SoMarkerSet::MarkerType markerType() const { return m_mType; }

  private:

    SbColor m_color;
    SbColor m_hcolor;
    SoMarkerSet::MarkerType m_mType;

  };

public:

  /// standard constructor
  RichRecRingRep( const std::string & Type,
                  const Qualities & qualities,
                  const Rich::Rec::IRayTraceCherenkovCone * rayTrace,
                  SoStyleCache* styleCache );

  /// destructor
  ~RichRecRingRep();

  /** begin a scene graph for a collection.
   *  @return the top separator.
   */
  SoSeparator* begin();

  /** represent one hit.
   *  @param hit pointer to the hit
   *  @param sep the top separator to put the sub-scene graph.
   */
  void represent(const LHCb::RichRecRing* ring,SoSeparator* sep);

private: // methods

  const Rich::Rec::IRayTraceCherenkovCone * rayTrace() const  { return m_rayTrace; }
  const std::string & shape() const   { return m_shape; }
  const Qualities & qualities() const { return m_qualities; }
  bool sameSide( const Gaudi::XYZPoint & p1,
                 const Gaudi::XYZPoint & p2,
                 const Rich::DetectorType rich ) const;
  bool inHPD( const LHCb::RichRecPointOnRing & p ) const;
  bool inPanel( const LHCb::RichRecPointOnRing & p ) const;
  bool okToDraw( const LHCb::RichRecPointOnRing & p1,
                 const LHCb::RichRecPointOnRing & p2,
                 const Rich::DetectorType rich ) const;

private: // data

  std::string m_shape;

  const Qualities m_qualities;

  /// pointer to ray tracing tool
  const Rich::Rec::IRayTraceCherenkovCone * m_rayTrace;

  SoStyleCache* m_styleCache;
  SoCoordinate3* m_coordinate;
  int            m_icoord;
  SoSeparator*   m_markers;
  SoSeparator*   m_lines;
};

inline bool RichRecRingRep::inHPD( const LHCb::RichRecPointOnRing & p ) const
{
  return ( p.acceptance() == LHCb::RichRecPointOnRing::InHPDTube );
}

inline bool RichRecRingRep::inPanel( const LHCb::RichRecPointOnRing & p ) const
{
  return ( p.acceptance() == LHCb::RichRecPointOnRing::InHPDPanel ||
           p.acceptance() == LHCb::RichRecPointOnRing::InHPDTube );
}

inline bool RichRecRingRep::sameSide( const Gaudi::XYZPoint & p1,
                                      const Gaudi::XYZPoint & p2,
                                      const Rich::DetectorType rich ) const
{
  return ( Rich::Rich1 == rich ? p1.y() * p2.y() > 0 : p1.x() * p2.x() > 0 );
}

// ============================================================================
#endif // SORICHREC_RICHRECRINGREP_H
// ============================================================================
